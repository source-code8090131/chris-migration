﻿namespace CHRIS.Models
{
    public class ViewResponseModel
    {
        public string responseCode { get; set; }
        public string responseMessage { get; set; }

        public int? data { get; set; }
        public decimal? data2 { get; set; }
        public string responseData { get; set; }
    }
}
