﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CHRIS.Data;
//using System.Web.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace CHRIS.Models
{
    public class AssignRoles_Custom
    {
        public CHRIS_LOGIN Entity { get; set; }
        public int?[] SelectedRoles { get; set; }
        public IEnumerable<SelectListItem> RolesList { get; set; }
    }
}