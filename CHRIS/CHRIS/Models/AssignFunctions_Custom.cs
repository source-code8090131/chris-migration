﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CHRIS.Data;
using CHRIS.Data;
//using System.Web.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace CHRIS.Models
{
    public class AssignFunctions_Custom
    {
        public CHRIS_ROLES Entity { get; set; }
        public int?[] SelectedFunctions { get; set; }
        public IEnumerable<SelectListItem> FunctionsList { get; set; }
    }
}