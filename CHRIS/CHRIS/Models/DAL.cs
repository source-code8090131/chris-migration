﻿//#define DEV
#define prod
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Text;
using System.Net;
using System.Security.Cryptography;
using CHRIS.Data;
using CHRIS.Data;

namespace CHRIS.Models
{
    public class DAL
    {

        public static string message = "";
        public static dynamic LogCurrentRequest;

        #region Encrypt N Decrypt
        public static string Decrypt(string cipherText)
        {
            string EncryptionKey = "MAKV2SPBNI99212";
            byte[] cipherBytes = Convert.FromBase64String(cipherText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6E, 0x20, 0x4D, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (System.IO.MemoryStream ms = new System.IO.MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(cipherBytes, 0, cipherBytes.Length);
                        cs.Close();
                    }
                    cipherText = Encoding.Unicode.GetString(ms.ToArray());
                }
            }
            return cipherText;
        }
        public static string Encrypt(string clearText)
        {
            string EncryptionKey = "MAKV2SPBNI99212";
            byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6E, 0x20, 0x4D, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (System.IO.MemoryStream ms = new System.IO.MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(clearBytes, 0, clearBytes.Length);
                        cs.Close();
                    }
                    clearText = Convert.ToBase64String(ms.ToArray());
                }
            }
            return clearText;
        }
        #endregion
        public static bool CheckFunctionValidity(string CName, string AName, string userID)
        {
            try
            {
                //PRCEntities context = new PRCEntities();
                CHRIS_Context context = new CHRIS_Context();
                //var context = new Import_ContextProcedures(ic);
                bool success = false;
                int FunctionId = context.CHRIS_FUNCTIONS.Where(m => m.PF_CONTROLLER == CName && m.PF_ACTION == AName).Select(m => m.PF_ID).FirstOrDefault();
                List<CHRIS_ASSIGN_LOGIN_RIGHTS> RoleId = context.CHRIS_ASSIGN_LOGIN_RIGHTS.Where(m => m.PALR_USER_ID == userID && m.PALR_STATUS == true).ToList();
                foreach (CHRIS_ASSIGN_LOGIN_RIGHTS CheckRoles in RoleId)
                {
                    var roleIsActive = context.CHRIS_ROLES.Where(m => m.PR_ID == CheckRoles.PALR_ROLE_ID && m.PR_STATUS == "true" && m.PR_ISAUTH == true).FirstOrDefault();
                    if (roleIsActive != null)
                    {
                        CHRIS_ASSIGN_FUNCTIONS CheckFunction = context.CHRIS_ASSIGN_FUNCTIONS.Where(m => m.PAF_ROLE_ID == CheckRoles.PALR_ROLE_ID && m.PAF_FUNCTION_ID == FunctionId && m.PAF_STATUS == true).FirstOrDefault();
                        if (CheckFunction == null)
                        {
                            success = false;
                        }
                        else
                        {
                            success = true;
                            return success;
                        }
                    }
                    else
                        success = false;
                }
                return success;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}