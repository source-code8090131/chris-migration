﻿using CHRIS.Data;
using CHRIS.Models;
using CHRIS.Common;


namespace CHRIS.Services
{
    public class AttritionTypeEntryService
    {
        private static AttritionTypeEntryService _instance = null;
        public static AttritionTypeEntryService getInstance()
        {
            if (_instance == null)
            {
                _instance = new AttritionTypeEntryService();
            }
            return _instance;
        }
        private AttritionTypeEntryService() { }

        #region AttritionCategoryEntry
        public List<CHRIS_SP_SETUP_ATTR_TYPE_GETALLResult> GetAllAttritionTypeEntry()
        {
            List<CHRIS_SP_SETUP_ATTR_TYPE_GETALLResult> output = new List<CHRIS_SP_SETUP_ATTR_TYPE_GETALLResult>();
            try
            {
                CHRIS_Context ic = new CHRIS_Context();
                var context = new CHRIS_ContextProcedures(ic);

                OutputParameter<int?> outParam = new OutputParameter<int?>();
                var res = context.CHRIS_SP_SETUP_ATTR_TYPE_GETALLAsync(outParam);
                res.Wait();

                output = res.Result;

                ic.Dispose();
                context = null;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return output;
        }

        //public int? AddUpdateAttritionTypeEntry(CHRIS_SP_SETUP_ATTR_TYPE_GETALLResult model)
        //{
        //    int result = 0;
        //    try
        //    {
        //        CHRIS_Context ic = new CHRIS_Context();
        //        var context = new CHRIS_ContextProcedures(ic);

        //        if (model.ID > 0)
        //        {
        //            OutputParameter<int?> ID = new OutputParameter<int?>();
        //            OutputParameter<int?> outparam1 = new OutputParameter<int?>();
        //            var res = context.CHRIS_SP_SETUP_ATTR_TYPE_UPDATEAsync(model.ATTR_CATEGORY_CODE, model.ATTR_CATEGORY_DESC, model.ID, outparam1);
        //            res.Wait();
        //            if (outparam1.Value == -10)
        //                result = res.Result;
        //            else
        //                result = 0;
        //        }
        //        else
        //        {
        //            OutputParameter<int?> ID = new OutputParameter<int?>();
        //            OutputParameter<int?> outparam3 = new OutputParameter<int?>();

        //            var res = context.CHRIS_SP_SETUP_ATTR_TYPE_ADDAsync(model.ATTR_CATEGORY_CODE, model.ATTR_CATEGORY_DESC, ID, outparam3);
        //            res.Wait();
        //            if (outparam3.Value > 0)
        //                result = res.Result;
        //            else
        //                result = 0;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }

        //    return result;
        //}


        //public ViewResponseModel DeleteAttritionTypeEntry(AttritionTypeEntryModel model)
        //{
        //    ViewResponseModel response = new ViewResponseModel();
        //    try
        //    {
        //        CHRIS_Context ic = new CHRIS_Context();
        //        var context = new CHRIS_ContextProcedures(ic);
        //        OutputParameter<int?> iD = new OutputParameter<int?>();
        //        OutputParameter<int?> oRetVal = new OutputParameter<int?>();


        //        var dtlListUpdate = context.CHRIS_SP_Setup_ATTR_CATEGORY_DELETEAsync(model.ID, oRetVal);
        //        dtlListUpdate.Wait();
        //        if (oRetVal != null && oRetVal.Value == -10)
        //        {
        //            response.responseCode = Constants.ResponseCode.success;
        //            response.responseMessage = "Record Deleted successfully";
        //        }
        //        else if (oRetVal != null && oRetVal.Value == -40)
        //        {
        //            response.responseCode = Constants.ResponseCode.error;
        //            response.responseMessage = "Record Not Found";
        //        }
        //        else
        //        {
        //            response.responseCode = Constants.ResponseCode.error;
        //            response.responseMessage = "Error Deleting Record";
        //        }

        //        ic.Dispose();
        //        context = null;
        //    }
        //    catch (Exception ex)
        //    {
        //        response.responseCode = Constants.ResponseCode.error;
        //        response.responseMessage = "Error Deleting Record";
        //    }
        //    return response;
        //}



        #endregion
    }
}
