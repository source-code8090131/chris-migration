﻿using CHRIS.Data;
using CHRIS.Models;
using CHRIS.Common;
using System.Data;
using Microsoft.AspNetCore.Http;

namespace CHRIS.Services
{
    public class BenchMarkRateEntryService
    {
        private static BenchMarkRateEntryService _instance = null;
        public static BenchMarkRateEntryService getInstance()
        {
            if (_instance == null)
            {
                _instance = new BenchMarkRateEntryService();
            }
            return _instance;
        }

       
        private BenchMarkRateEntryService() { }

        #region BenchMarkRateEntry
        public List<CHRIS_SP_BENCHMARK_MANAGERResult> btn_SearchDate(CHRIS_SP_BENCHMARK_MANAGERResult model)
        {
            List<CHRIS_SP_BENCHMARK_MANAGERResult> output = new List<CHRIS_SP_BENCHMARK_MANAGERResult>();
            try
            {
                CHRIS_Context ic = new CHRIS_Context();
                var context = new CHRIS_ContextProcedures(ic);
                DateTime BEN_DATE = Convert.ToDateTime(model.BEN_DATE);
                decimal BEN_RATE = Convert.ToDecimal(model.BEN_RATE);
                OutputParameter<int?> outParam = new OutputParameter<int?>();
                var res = context.CHRIS_SP_BENCHMARK_MANAGERAsync(model.BEN_FIN_TYPE, BEN_DATE, BEN_RATE, "List", outParam, outParam);
                res.Wait();

                output = res.Result;
                
                ic.Dispose();
                context = null;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return output;
        }


        public List<CHRIS_SP_BENCHMARK_MANAGERResult> PRC01(CHRIS_SP_BENCHMARK_MANAGERResult model)
        {
            string message = "";
            List<CHRIS_SP_BENCHMARK_MANAGERResult> output = new List<CHRIS_SP_BENCHMARK_MANAGERResult>();
            try
            {
                if (model.BEN_DATE != null)
                {
                    CHRIS_Context ic = new CHRIS_Context();
                    var context = new CHRIS_ContextProcedures(ic);
                    DateTime BEN_DATE = Convert.ToDateTime(model.BEN_DATE);
                    decimal BEN_RATE = Convert.ToDecimal(model.BEN_RATE);
                    OutputParameter<int?> outParam = new OutputParameter<int?>();
                    var res = context.CHRIS_SP_BENCHMARK_MANAGERAsync(model.BEN_FIN_TYPE, BEN_DATE, BEN_RATE, "PRC01", outParam, outParam);
                    res.Wait();

                    output = res.Result;

                    ic.Dispose();
                    context = null;
                }
                else
                {
                    message = "Select date to continue.";
                }
                    
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return output;
        }
        public List<T> ConvertDataTableToList<T>(DataTable dt)
        {
            var columnNames = dt.Columns.Cast<DataColumn>().Select(c => c.ColumnName.ToLower()).ToList();
            var properties = typeof(T).GetProperties();
            return dt.AsEnumerable().Select(row =>
            {
                var objT = Activator.CreateInstance<T>();
                foreach (var pro in properties)
                {
                    if (columnNames.Contains(pro.Name.ToLower()))
                    {
                        try
                        {
                            pro.SetValue(objT, row[pro.Name]);
                        }
                        catch (Exception ex) { }
                    }
                }
                return objT;
            }).ToList();
        }
        public List<CHRIS_SP_BENCHMARK_MANAGERResult> AddUpdateBenchMarkRateEntry(CHRIS_SP_BENCHMARK_MANAGERResult model)
        {
            List<CHRIS_SP_BENCHMARK_MANAGERResult> output = new List<CHRIS_SP_BENCHMARK_MANAGERResult>();
            int result = 0;
            try
            {
                CHRIS_Context ic = new CHRIS_Context();
                var context = new CHRIS_ContextProcedures(ic);

                if (model.ID > 0)
                {
                    OutputParameter<int?> ID = new OutputParameter<int?>();
                   // OutputParameter<int?> outparam1 = new OutputParameter<int?>();
                    DateTime BEN_DATE = Convert.ToDateTime(model.BEN_DATE);
                    decimal BEN_RATE = Convert.ToDecimal(model.BEN_RATE);
                    OutputParameter<int?> oRetVal = new OutputParameter<int?>();
                    OutputParameter<int> returnValue = new OutputParameter<int>();
                    var res = context.CHRIS_SP_BENCHMARK_MANAGERAsync(model.BEN_FIN_TYPE, BEN_DATE, BEN_RATE, "Update",ID, oRetVal, returnValue);
                    res.Wait();
                    if (res != null)
                        output = res.Result;

                }
                else
                {
                    OutputParameter<int?> ID = new OutputParameter<int?>();
                    DateTime BEN_DATE = Convert.ToDateTime(model.BEN_DATE);
                    decimal BEN_RATE = Convert.ToDecimal(model.BEN_RATE);
                    OutputParameter<int?> oRetVal = new OutputParameter<int?>();
                    OutputParameter<int> returnValue = new OutputParameter<int>();

                    var res = context.CHRIS_SP_BENCHMARK_MANAGERAsync(model.BEN_FIN_TYPE, BEN_DATE, BEN_RATE, "Save", ID, oRetVal, returnValue);
                    res.Wait();
                    if (res != null)
                        output = res.Result;
                   
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return output;
        }


        //public ViewResponseModel DeleteBenchMarkRateEntry(BenchMarkRateEntryModel model)
        //{
        //    ViewResponseModel response = new ViewResponseModel();
        //    try
        //    {
        //        CHRIS_Context ic = new CHRIS_Context();
        //        var context = new CHRIS_ContextProcedures(ic);
        //        OutputParameter<int?> iD = new OutputParameter<int?>();
        //        OutputParameter<int?> oRetVal = new OutputParameter<int?>();


        //        var dtlListUpdate = context.CHRIS_SP_BENCHMARK_DELETEAsync(model.ID, oRetVal);
        //        dtlListUpdate.Wait();
        //        if (oRetVal != null && oRetVal.Value == -10)
        //        {
        //            response.responseCode = Constants.ResponseCode.success;
        //            response.responseMessage = "Record Deleted successfully";
        //        }
        //        else if (oRetVal != null && oRetVal.Value == -40)
        //        {
        //            response.responseCode = Constants.ResponseCode.error;
        //            response.responseMessage = "Record Not Found";
        //        }
        //        else
        //        {
        //            response.responseCode = Constants.ResponseCode.error;
        //            response.responseMessage = "Error Deleting Record";
        //        }

        //        ic.Dispose();
        //        context = null;
        //    }
        //    catch (Exception ex)
        //    {
        //        response.responseCode = Constants.ResponseCode.error;
        //        response.responseMessage = "Error Deleting Record";
        //    }
        //    return response;
        //}



        #endregion
    }
}
