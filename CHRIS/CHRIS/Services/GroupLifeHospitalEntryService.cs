﻿using CHRIS.Common;
using CHRIS.Data;
using CHRIS.Models;

namespace CHRIS.Services
{
    public class GroupLifeHospitalEntryService
    {
        private static GroupLifeHospitalEntryService _instance = null;
        public static GroupLifeHospitalEntryService getInstance()
        {
            if (_instance == null)
            {
                _instance = new GroupLifeHospitalEntryService();
            }
            return _instance;
        }
        private GroupLifeHospitalEntryService() { }

        #region GroupLifeHospitalEntry
        public List<CHRIS_SP_GROUP_INSC_GETALLResult> GroupLifeHospitalEntry()
        {
            List<CHRIS_SP_GROUP_INSC_GETALLResult> output = new List<CHRIS_SP_GROUP_INSC_GETALLResult>();
            try
            {
                CHRIS_Context ic = new CHRIS_Context();
                var context = new CHRIS_ContextProcedures(ic);

                OutputParameter<int?> outParam = new OutputParameter<int?>();
                var res = context.CHRIS_SP_GROUP_INSC_GETALLAsync(outParam);
                res.Wait();

                output = res.Result;

                ic.Dispose();
                context = null;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return output;
        }

        public ViewResponseModel DeleteGroupLifeHospitalEntry(GroupLifeHospitalEntryModel model)
        {
            ViewResponseModel response = new ViewResponseModel();
            try
            {
                CHRIS_Context ic = new CHRIS_Context();
                var context = new CHRIS_ContextProcedures(ic);
                OutputParameter<int?> iD = new OutputParameter<int?>();
                OutputParameter<int?> oRetVal = new OutputParameter<int?>();


                var dtlListUpdate = context.CHRIS_SP_GROUP_INSC_DELETEAsync(model.ID, oRetVal);
                dtlListUpdate.Wait();
                if (oRetVal != null && oRetVal.Value == -10)
                {
                    response.responseCode = Constants.ResponseCode.success;
                    response.responseMessage = "Record Deleted successfully";
                }
                else if (oRetVal != null && oRetVal.Value == -40)
                {
                    response.responseCode = Constants.ResponseCode.error;
                    response.responseMessage = "Record Not Found";
                }
                else
                {
                    response.responseCode = Constants.ResponseCode.error;
                    response.responseMessage = "Error Deleting Record";
                }

                ic.Dispose();
                context = null;
            }
            catch (Exception ex)
            {
                response.responseCode = Constants.ResponseCode.error;
                response.responseMessage = "Error Deleting Record";
            }
            return response;
        }

        #endregion
    }
}
