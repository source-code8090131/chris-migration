﻿using CHRIS.Data;
using CHRIS.Models;
using CHRIS.Common;

namespace CHRIS.Services
{
    public class UserNameSetupService
    {
        private static UserNameSetupService _instance = null;
        public static UserNameSetupService getInstance()
        {
            if (_instance == null)
            {
                _instance = new UserNameSetupService();
            }
            return _instance;
        }
        private UserNameSetupService() { }

        #region UserNameSetup
        public List<CHRIS_Setup_Sp_CHRIS_USERS_GETALLResult> GetAllUserNameSetup()
        {
            List<CHRIS_Setup_Sp_CHRIS_USERS_GETALLResult> output = new List<CHRIS_Setup_Sp_CHRIS_USERS_GETALLResult>();
            try
            {
                CHRIS_Context ic = new CHRIS_Context();
                var context = new CHRIS_ContextProcedures(ic);

                OutputParameter<int?> outParam = new OutputParameter<int?>();
                var res = context.CHRIS_Setup_Sp_CHRIS_USERS_GETALLAsync(outParam);
                res.Wait();

                output = res.Result;

                ic.Dispose();
                context = null;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return output;
        }

        public int? AddUpdateUserNameSetup(CHRIS_Setup_Sp_CHRIS_USERS_GETALLResult model)
        {
            int result = 0;
            try
            {
                CHRIS_Context ic = new CHRIS_Context();
                var context = new CHRIS_ContextProcedures(ic);

                if (model.ID > 0)
                {
                    OutputParameter<int?> ID = new OutputParameter<int?>();
                    OutputParameter<int?> outparam1 = new OutputParameter<int?>();
                    var res = context.CHRIS_Setup_Sp_CHRIS_USERS_ADDAsync(model.SOEID, model.USER_NAME, model.ID, outparam1);
                    res.Wait();
                    if (outparam1.Value == -40)
                        result = res.Result;
                    else
                        result = 0;
                }
                else
                {
                    OutputParameter<int?> ID = new OutputParameter<int?>();
                    OutputParameter<int?> outparam3 = new OutputParameter<int?>();

                    var res = context.CHRIS_Setup_Sp_CHRIS_USERS_ADDAsync(model.SOEID, model.USER_NAME, model.ID, outparam3);
                    res.Wait();
                    if (outparam3.Value > 0)
                        result = res.Result;
                    else
                        result = 0;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }


        public ViewResponseModel DeleteUserNameSetup(UserNameSetupModel model)
        {
            ViewResponseModel response = new ViewResponseModel();
            try
            {
                CHRIS_Context ic = new CHRIS_Context();
                var context = new CHRIS_ContextProcedures(ic);
                OutputParameter<int?> iD = new OutputParameter<int?>();
                OutputParameter<int?> oRetVal = new OutputParameter<int?>();


                var dtlListUpdate = context.CHRIS_Setup_Sp_CHRIS_USERS_DELETEAsync(model.ID, oRetVal);
                dtlListUpdate.Wait();
                if (oRetVal != null && oRetVal.Value == -10)
                {
                    response.responseCode = Constants.ResponseCode.success;
                    response.responseMessage = "Record Deleted successfully";
                }
                else if (oRetVal != null && oRetVal.Value == -40)
                {
                    response.responseCode = Constants.ResponseCode.error;
                    response.responseMessage = "Record Not Found";
                }
                else
                {
                    response.responseCode = Constants.ResponseCode.error;
                    response.responseMessage = "Error Deleting Record";
                }

                ic.Dispose();
                context = null;
            }
            catch (Exception ex)
            {
                response.responseCode = Constants.ResponseCode.error;
                response.responseMessage = "Error Deleting Record";
            }
            return response;
        }

        #endregion
    }
}
