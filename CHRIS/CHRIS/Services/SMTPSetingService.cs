﻿using CHRIS.Common;
using CHRIS.Data;
using CHRIS.Models;
using System.Data;


namespace CHRIS.Services
{
    public class SMTPSetingService
    {
        private static SMTPSetingService _instance = null;
        public static SMTPSetingService getInstance()
        {
            if (_instance == null)
            {
                _instance = new SMTPSetingService();
            }
            return _instance;
        }


        private SMTPSetingService() { }

        #region SMTPSeting
      
        public List<CHRIS_SP_SMTP_SETTINGSResult> AddUpdateSMTPSeting(CHRIS_SP_SMTP_SETTINGSResult model)
        {
            List<CHRIS_SP_SMTP_SETTINGSResult> output = new List<CHRIS_SP_SMTP_SETTINGSResult>();
            int result = 0;
            try
            {
                CHRIS_Context ic = new CHRIS_Context();
                var context = new CHRIS_ContextProcedures(ic);
                
                OutputParameter<int?> ID = new OutputParameter<int?>();
                OutputParameter<int?> oRetVal = new OutputParameter<int?>();
                OutputParameter<int> returnValue = new OutputParameter<int>();

                var res = context.CHRIS_SP_SMTP_SETTINGSAsync(null,"Save",model.SharedFolderPath,model.Subject,model.Body, ID, oRetVal, returnValue);
                res.Wait();
                if (res != null)
                    output = res.Result;

               
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return output;
        }

        #endregion
    }
}
