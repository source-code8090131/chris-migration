﻿using CHRIS.Common;
using CHRIS.Data;
using CHRIS.Models;
using System.Data;


namespace CHRIS.Services
{
    public class MarginalTaxSetupService
    {
        private static MarginalTaxSetupService _instance = null;
        public static MarginalTaxSetupService getInstance()
        {
            if (_instance == null)
            {
                _instance = new MarginalTaxSetupService();
            }
            return _instance;
        }


        private MarginalTaxSetupService() { }

        #region MarginalTaxSetup
        public List<CHRIS_SP_MARGINAL_TAX_GETALLResult> MarginalTaxSetup()
        {
            List<CHRIS_SP_MARGINAL_TAX_GETALLResult> output = new List<CHRIS_SP_MARGINAL_TAX_GETALLResult>();
            try
            {
                CHRIS_Context ic = new CHRIS_Context();
                var context = new CHRIS_ContextProcedures(ic);

                OutputParameter<int?> outParam = new OutputParameter<int?>();
                var res = context.CHRIS_SP_MARGINAL_TAX_GETALLAsync(outParam);
                res.Wait();

                output = res.Result;

                ic.Dispose();
                context = null;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return output;
        }

        public List<T> ConvertDataTableToList<T>(DataTable dt)
        {
            var columnNames = dt.Columns.Cast<DataColumn>().Select(c => c.ColumnName.ToLower()).ToList();
            var properties = typeof(T).GetProperties();
            return dt.AsEnumerable().Select(row =>
            {
                var objT = Activator.CreateInstance<T>();
                foreach (var pro in properties)
                {
                    if (columnNames.Contains(pro.Name.ToLower()))
                    {
                        try
                        {
                            pro.SetValue(objT, row[pro.Name]);
                        }
                        catch (Exception ex) { }
                    }
                }
                return objT;
            }).ToList();
        }
        public List<CHRIS_SP_MARGINAL_TAX_MANAGERResult> AddUpdateMarginalTaxSetup(CHRIS_SP_MARGINAL_TAX_MANAGERResult model)
        {
            List<CHRIS_SP_MARGINAL_TAX_MANAGERResult> output = new List<CHRIS_SP_MARGINAL_TAX_MANAGERResult>();
            int result = 0;
            try
            {
                CHRIS_Context ic = new CHRIS_Context();
                var context = new CHRIS_ContextProcedures(ic);

                if (model.ID > 0)
                {
                    OutputParameter<int?> ID = new OutputParameter<int?>();

                    DateTime MT_DATE_FROM = Convert.ToDateTime(model.MT_DATE_FROM);
                    DateTime MT_DATE_TO = Convert.ToDateTime(model.MT_DATE_TO);
                    decimal MT_AMT_FROM = Convert.ToDecimal(model.MT_AMT_FROM);
                    decimal MT_AMT_TO = Convert.ToDecimal(model.MT_AMT_TO);
                    decimal MT_PERCENTAGE = Convert.ToDecimal(model.MT_PERCENTAGE);
                    OutputParameter<int?> oRetVal = new OutputParameter<int?>();
                    OutputParameter<int> returnValue = new OutputParameter<int>();

                    var res = context.CHRIS_SP_MARGINAL_TAX_MANAGERAsync(MT_DATE_FROM, MT_DATE_TO, MT_AMT_FROM, MT_AMT_TO,MT_PERCENTAGE, "Update", ID, oRetVal, returnValue);
                    res.Wait();
                    if (res != null)
                        output = res.Result;

                }
                else
                {
                    OutputParameter<int?> ID = new OutputParameter<int?>();
                    DateTime MT_DATE_FROM = Convert.ToDateTime(model.MT_DATE_FROM);
                    DateTime MT_DATE_TO = Convert.ToDateTime(model.MT_DATE_TO);
                    decimal MT_AMT_FROM = Convert.ToDecimal(model.MT_AMT_FROM);
                    decimal MT_AMT_TO = Convert.ToDecimal(model.MT_AMT_TO);
                    decimal MT_PERCENTAGE = Convert.ToDecimal(model.MT_PERCENTAGE);
                    OutputParameter<int?> oRetVal = new OutputParameter<int?>();
                    OutputParameter<int> returnValue = new OutputParameter<int>();

                    var res = context.CHRIS_SP_MARGINAL_TAX_MANAGERAsync(MT_DATE_FROM, MT_DATE_TO, MT_AMT_FROM, MT_AMT_TO, MT_PERCENTAGE, "Save", ID, oRetVal, returnValue);
                    res.Wait();
                    if (res != null)
                        output = res.Result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return output;
        }


        public ViewResponseModel DeleteMarginalTaxSetup(MarginalTaxSetupModel model)
        {
            ViewResponseModel response = new ViewResponseModel();
            try
            {
                CHRIS_Context ic = new CHRIS_Context();
                var context = new CHRIS_ContextProcedures(ic);
                OutputParameter<int?> oRetVal = new OutputParameter<int?>();


                var dtlListUpdate = context.CHRIS_SP_MARGINAL_TAX_DELETEAsync(model.ID, oRetVal);
                dtlListUpdate.Wait();

                if (oRetVal != null && oRetVal.Value == -10)
                {
                    response.responseCode = Constants.ResponseCode.success;
                    response.responseMessage = "Record Deleted successfully";
                }
                else if (oRetVal != null && oRetVal.Value == -40)
                {
                    response.responseCode = Constants.ResponseCode.error;
                    response.responseMessage = "Record Not Found";
                }
                else
                {
                    response.responseCode = Constants.ResponseCode.error;
                    response.responseMessage = "Error Deleting Record";
                }

                ic.Dispose();
                context = null;
            }
            catch (Exception ex)
            {
                response.responseCode = Constants.ResponseCode.error;
                response.responseMessage = "Error Deleting Record";
            }
            return response;
        }



        #endregion
    }
}
