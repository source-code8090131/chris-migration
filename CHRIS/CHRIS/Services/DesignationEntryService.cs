﻿
using CHRIS.Common;
using CHRIS.Data;
using CHRIS.Models;
using Microsoft.Data.SqlClient;
using System.Data;

namespace CHRIS.Services
{
    public class DesignationEntryService
    {
        private static DesignationEntryService _instance = null;
        public static DesignationEntryService getInstance()
        {
            if (_instance == null)
            {
                _instance = new DesignationEntryService();
            }
            return _instance;
        }


        private DesignationEntryService() { }

        #region DesignationEntry
        public List<CHRIS_SP_DESIG_GETALLResult> DesignationEntry()
        {
            List<CHRIS_SP_DESIG_GETALLResult> output = new List<CHRIS_SP_DESIG_GETALLResult>();
            try
            {
                CHRIS_Context ic = new CHRIS_Context();
                var context = new CHRIS_ContextProcedures(ic);

                OutputParameter<int?> outParam = new OutputParameter<int?>();
                var res = context.CHRIS_SP_DESIG_GETALLAsync(outParam);
                res.Wait();

                output = res.Result;

                ic.Dispose();
                context = null;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return output;
        }

        public List<CHRIS_SP_DESIG_MANAGERResult> btn_tbl_Category(CHRIS_SP_DESIG_MANAGERResult model)
        {
            List<CHRIS_SP_DESIG_MANAGERResult> output = new List<CHRIS_SP_DESIG_MANAGERResult>();
            try
            {
                CHRIS_Context ic = new CHRIS_Context();
                var context = new CHRIS_ContextProcedures(ic);
                OutputParameter<int?> ID = new OutputParameter<int?>();
                //ID._value = Convert.ToInt32(model.ID);
                OutputParameter<int?> outParam = new OutputParameter<int?>();
                OutputParameter<int?> oRetVal = new OutputParameter<int?>();

                var res = context.CHRIS_SP_DESIG_MANAGERAsync(null, null, null, null, null, null, null, null, null, null, null, null, null, null, "Lov_CAT",outParam,outParam);
                //DataTable resa = CallCHRIS_SP_DESIG_MANAGER(null,null,null,null,null,null,null,null,null,null,null,null,null,null, "Lov_CAT",ID, outParam);

                res.Wait();
                output = res.Result;

                ic.Dispose();
                context = null;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return output;
        }

        public List<CHRIS_SP_DESIG_MANAGERResult> btn_tbl_Branch(CHRIS_SP_DESIG_MANAGERResult model)
        {
            List<CHRIS_SP_DESIG_MANAGERResult> output = new List<CHRIS_SP_DESIG_MANAGERResult>();
            try
            {
                CHRIS_Context ic = new CHRIS_Context();
                var context = new CHRIS_ContextProcedures(ic);
                OutputParameter<int?> ID = new OutputParameter<int?>();

                OutputParameter<int?> outParam = new OutputParameter<int?>();
                var res = context.CHRIS_SP_DESIG_MANAGERAsync(null, null, null, null, null, null, null, null, null, null, null, null, null, null, "Lov_BRN", null, outParam);
                res.Wait();
                output = res.Result;

                ic.Dispose();
                context = null;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return output;
        }

        public List<T> ConvertDataTableToList<T>(DataTable dt)
        {
            var columnNames = dt.Columns.Cast<DataColumn>().Select(c => c.ColumnName.ToLower()).ToList();
            var properties = typeof(T).GetProperties();
            return dt.AsEnumerable().Select(row =>
            {
                var objT = Activator.CreateInstance<T>();
                foreach (var pro in properties)
                {
                    if (columnNames.Contains(pro.Name.ToLower()))
                    {
                        try
                        {
                            pro.SetValue(objT, row[pro.Name]);
                        }
                        catch (Exception ex) { }
                    }
                }
                return objT;
            }).ToList();
        }
        public List<CHRIS_SP_DESIG_MANAGERResult> AddUpdateDesignationEntry(CHRIS_SP_DESIG_MANAGERResult model)
        {
            List<CHRIS_SP_DESIG_MANAGERResult> output = new List<CHRIS_SP_DESIG_MANAGERResult>();
            int result = 0;
            try
            {
                CHRIS_Context ic = new CHRIS_Context();
                var context = new CHRIS_ContextProcedures(ic);

                if (model.ID > 0)
                {
                    OutputParameter<int?> ID = new OutputParameter<int?>();
                    decimal SP_MIN = Convert.ToDecimal(model.SP_MIN);
                    decimal SP_MID = Convert.ToDecimal(model.SP_MID);
                    decimal SP_MAX = Convert.ToDecimal(model.SP_MAX);
                    decimal SP_INCREMENT = Convert.ToDecimal(model.SP_INCREMENT);
                    DateTime SP_EFFECTIVE = Convert.ToDateTime(model.SP_EFFECTIVE);
                    OutputParameter<int?> oRetVal = new OutputParameter<int?>();
                    OutputParameter<int> returnValue = new OutputParameter<int>();

                    var res = context.CHRIS_SP_DESIG_MANAGERAsync(model.SP_DESG,model.SP_BRANCH,model.SP_LEVEL,model.SP_CATEGORY,model.SP_DESC_DG1,model.SP_DESC_DG2,model.SP_CONFIRM,SP_MIN,SP_MID,SP_MAX,SP_INCREMENT,SP_EFFECTIVE,model.SP_CURRENT,null, "Update", ID, oRetVal, returnValue);
                    res.Wait();
                    if (res != null)
                        output = res.Result;

                }
                else
                {
                    OutputParameter<int?> ID = new OutputParameter<int?>();
                    decimal SP_MIN = Convert.ToDecimal(model.SP_MIN);
                    decimal SP_MID = Convert.ToDecimal(model.SP_MID);
                    decimal SP_MAX = Convert.ToDecimal(model.SP_MAX);
                    decimal SP_INCREMENT = Convert.ToDecimal(model.SP_INCREMENT);
                    DateTime SP_EFFECTIVE = Convert.ToDateTime(model.SP_EFFECTIVE);
                    OutputParameter<int?> oRetVal = new OutputParameter<int?>();
                    OutputParameter<int> returnValue = new OutputParameter<int>();

                    var res = context.CHRIS_SP_DESIG_MANAGERAsync(model.SP_DESG, model.SP_BRANCH, model.SP_LEVEL, model.SP_CATEGORY, model.SP_DESC_DG1, model.SP_DESC_DG2, model.SP_CONFIRM, SP_MIN, SP_MID, SP_MAX, SP_INCREMENT, SP_EFFECTIVE, model.SP_CURRENT, null, "Save", ID, oRetVal, returnValue);
                    res.Wait();
                    if (res != null)
                        output = res.Result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return output;
        }

        public ViewResponseModel DeleteDesignationEntry(DesignationEntryModel model)
        {
            ViewResponseModel response = new ViewResponseModel();
            try
            {
                CHRIS_Context ic = new CHRIS_Context();
                var context = new CHRIS_ContextProcedures(ic);
                OutputParameter<int?> oRetVal = new OutputParameter<int?>();


                var dtlListUpdate = context.CHRIS_SP_DESIG_DELETEAsync(model.ID, oRetVal);
                dtlListUpdate.Wait();

                if (oRetVal != null && oRetVal.Value == -10)
                {
                    response.responseCode = Constants.ResponseCode.success;
                    response.responseMessage = "Record Deleted successfully";
                }
                else if (oRetVal != null && oRetVal.Value == -40)
                {
                    response.responseCode = Constants.ResponseCode.error;
                    response.responseMessage = "Record Not Found";
                }
                else
                {
                    response.responseCode = Constants.ResponseCode.error;
                    response.responseMessage = "Error Deleting Record";
                }

                ic.Dispose();
                context = null;
            }
            catch (Exception ex)
            {
                response.responseCode = Constants.ResponseCode.error;
                response.responseMessage = "Error Deleting Record";
            }
            return response;
        }

        public DataTable CallCHRIS_SP_DESIG_MANAGER(string SP_DESG, string SP_BRANCH, string SP_LEVEL, string SP_CATEGORY, string SP_DESC_DG1, string SP_DESC_DG2, decimal? SP_CONFIRM, decimal? SP_MIN, decimal? SP_MID, decimal? SP_MAX, decimal? SP_INCREMENT, DateTime? SP_EFFECTIVE, string SP_CURRENT, string SearchFilter, string ActionType, OutputParameter<int?> ID, OutputParameter<int?> oRetVal, OutputParameter<int> returnValue = null, CancellationToken cancellationToken = default)
        {
            DataTable resultSet = null;
            try
            {

                IConfigurationRoot configuration = new ConfigurationBuilder()
               .SetBasePath(Directory.GetCurrentDirectory())
               .AddJsonFile("appsettings.json")
               .Build();
                var connectionString = configuration.GetConnectionString("cs");
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("CHRIS_SP_DESIG_MANAGER", conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    
                    cmd.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "ID",
                        Direction = System.Data.ParameterDirection.InputOutput,
                        Value = ID?._value ?? Convert.DBNull,
                        SqlDbType = System.Data.SqlDbType.Int,
                    });
                    cmd.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "oRetVal",
                        Direction = System.Data.ParameterDirection.InputOutput,
                        Value = oRetVal?._value ?? Convert.DBNull,
                        SqlDbType = System.Data.SqlDbType.Int,
                    });

                    cmd.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "returnValue",
                        Direction = System.Data.ParameterDirection.Output,
                        SqlDbType = System.Data.SqlDbType.Int,
                    });


                    cmd.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "SP_DESG",
                        Size = 3,
                        Value = SP_DESG ?? Convert.DBNull,
                        SqlDbType = System.Data.SqlDbType.VarChar,
                    });
                    cmd.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "SP_BRANCH",
                        Size = 3,
                        Value = SP_BRANCH ?? Convert.DBNull,
                        SqlDbType = System.Data.SqlDbType.VarChar,
                    });
                    cmd.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "SP_LEVEL",
                        Precision = 3,
                        Value = SP_LEVEL ?? Convert.DBNull,
                        SqlDbType = System.Data.SqlDbType.VarChar,
                    });
                    cmd.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "SP_CATEGORY",
                        Size = 1,
                        Value = SP_CATEGORY ?? Convert.DBNull,
                        SqlDbType = System.Data.SqlDbType.VarChar,
                    });
                    cmd.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "SP_DESC_DG1",
                        Size = 20,
                        Value = SP_DESC_DG1 ?? Convert.DBNull,
                        SqlDbType = System.Data.SqlDbType.VarChar,
                    });
                    cmd.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "SP_DESC_DG2",
                        Size = 20,
                        Value = SP_DESC_DG2 ?? Convert.DBNull,
                        SqlDbType = System.Data.SqlDbType.VarChar,
                    });
                    cmd.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "SP_CONFIRM",
                        Precision = 1,
                        Value = SP_CONFIRM ?? Convert.DBNull,
                        SqlDbType = System.Data.SqlDbType.Decimal,
                    });
                    cmd.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "SP_MIN",
                        Precision = 11,
                        Value = SP_MIN ?? Convert.DBNull,
                        SqlDbType = System.Data.SqlDbType.Decimal,
                    });
                    cmd.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "SP_MID",
                        Precision = 11,
                        Value = SP_MID ?? Convert.DBNull,
                        SqlDbType = System.Data.SqlDbType.Decimal,
                    });
                    cmd.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "SP_MAX",
                        Precision = 11,
                        Value = SP_MAX ?? Convert.DBNull,
                        SqlDbType = System.Data.SqlDbType.Decimal,
                    });
                    cmd.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "SP_INCREMENT",
                        Precision = 4,
                        Value = SP_INCREMENT ?? Convert.DBNull,
                        SqlDbType = System.Data.SqlDbType.Decimal,
                    });
                    cmd.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "SP_EFFECTIVE",
                        Value = SP_EFFECTIVE ?? Convert.DBNull,
                        SqlDbType = System.Data.SqlDbType.DateTime,
                    });
                    cmd.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "SP_CURRENT",
                        Size = 1,
                        Value = SP_CURRENT ?? Convert.DBNull,
                        SqlDbType = System.Data.SqlDbType.VarChar,
                    });

                    cmd.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "SearchFilter",
                        Size = 50,
                        Value = SearchFilter ?? Convert.DBNull,
                        SqlDbType = System.Data.SqlDbType.Decimal,
                    });
                    cmd.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "ActionType",
                        Size = 50,
                        Value = ActionType ?? Convert.DBNull,
                        SqlDbType = System.Data.SqlDbType.VarChar,
                    });
                    
                    var result = cmd.ExecuteReader();

                    if (result != null && result.HasRows)
                    {
                        resultSet = new DataTable();
                        resultSet.Load(result);
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return resultSet;
        }


        #endregion
    }
}
