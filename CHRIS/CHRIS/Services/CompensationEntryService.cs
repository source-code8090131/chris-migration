﻿using CHRIS.Common;
using CHRIS.Data;
using CHRIS.Models;
using System.Data;

namespace CHRIS.Services
{
    public class CompensationEntryService
    { 
        private static CompensationEntryService _instance = null;
        public static CompensationEntryService getInstance()
        {
            if (_instance == null)
            {
                _instance = new CompensationEntryService();
            }
            return _instance;
        }

       
        private CompensationEntryService() { }

        #region CompensationEntry
        public List<CHRIS_SP_CompEnt_COMP_GETALLResult> CompensationEntry()
        {
            List<CHRIS_SP_CompEnt_COMP_GETALLResult> output = new List<CHRIS_SP_CompEnt_COMP_GETALLResult>();
            try
            {
                CHRIS_Context ic = new CHRIS_Context();
                var context = new CHRIS_ContextProcedures(ic);
                
                OutputParameter<int?> outParam = new OutputParameter<int?>();
                var res = context.CHRIS_SP_CompEnt_COMP_GETALLAsync(outParam);
                res.Wait();

                output = res.Result;
                
                ic.Dispose();
                context = null;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return output;
        }


        public List<T> ConvertDataTableToList<T>(DataTable dt)
        {
            var columnNames = dt.Columns.Cast<DataColumn>().Select(c => c.ColumnName.ToLower()).ToList();
            var properties = typeof(T).GetProperties();
            return dt.AsEnumerable().Select(row =>
            {
                var objT = Activator.CreateInstance<T>();
                foreach (var pro in properties)
                {
                    if (columnNames.Contains(pro.Name.ToLower()))
                    {
                        try
                        {
                            pro.SetValue(objT, row[pro.Name]);
                        }
                        catch (Exception ex) { }
                    }
                }
                return objT;
            }).ToList();
        }
        public List<CHRIS_SP_CompEnt_COMP_MANAGERResult> AddUpdateCompensationEntry(CHRIS_SP_CompEnt_COMP_MANAGERResult model)
        {
            List<CHRIS_SP_CompEnt_COMP_MANAGERResult> output = new List<CHRIS_SP_CompEnt_COMP_MANAGERResult>();
            int result = 0;
            try
            {
                CHRIS_Context ic = new CHRIS_Context();
                var context = new CHRIS_ContextProcedures(ic);

                if (model.ID > 0)
                {
                    OutputParameter<int?> ID = new OutputParameter<int?>();
                    decimal MIN_SAL = Convert.ToDecimal(model.MIN_SAL);
                    decimal MAX_SAL = Convert.ToDecimal(model.MAX_SAL);
                    decimal MID_SAL = Convert.ToDecimal(model.MID_SAL);
                    decimal BDGT_PRICE = Convert.ToDecimal(model.BDGT_PRICE);
                    decimal ANL_DEPC = Convert.ToDecimal(model.ANL_DEPC);
                    decimal PTRL_LTR = Convert.ToDecimal(model.PTRL_LTR);
                    decimal PTRL_RATE = Convert.ToDecimal(model.PTRL_RATE);
                    decimal INS_RATE = Convert.ToDecimal(model.INS_RATE);
                    decimal MNTNC_AMT = Convert.ToDecimal(model.MNTNC_AMT);
                    decimal DRVR_COST = Convert.ToDecimal(model.DRVR_COST);
                    decimal GRPHD_ALL = Convert.ToDecimal(model.GRPHD_ALL);
                    decimal BO_YEAR = Convert.ToDecimal(model.BO_YEAR);
                    decimal GRHD_NO = Convert.ToDecimal(0);
                    OutputParameter<int?> oRetVal = new OutputParameter<int?>();
                    OutputParameter<int> returnValue = new OutputParameter<int>();
                    var res = context.CHRIS_SP_CompEnt_COMP_MANAGERAsync(model.LVL_CODE, model.GRPHD, MIN_SAL, MAX_SAL, MID_SAL, BDGT_PRICE, ANL_DEPC, PTRL_LTR, PTRL_RATE, INS_RATE, MNTNC_AMT, DRVR_COST, GRPHD_ALL, BO_YEAR, GRHD_NO, "Save", ID, oRetVal, returnValue);
                    res.Wait();
                    if (res != null)
                        output = res.Result;

                }
                else
                {
                    OutputParameter<int?> ID = new OutputParameter<int?>();
                    decimal MIN_SAL = Convert.ToDecimal(model.MIN_SAL);
                    decimal MAX_SAL = Convert.ToDecimal(model.MAX_SAL);
                    decimal MID_SAL = Convert.ToDecimal(model.MID_SAL);
                    decimal BDGT_PRICE = Convert.ToDecimal(model.BDGT_PRICE);
                    decimal ANL_DEPC = Convert.ToDecimal(model.ANL_DEPC);
                    decimal PTRL_LTR = Convert.ToDecimal(model.PTRL_LTR);
                    decimal PTRL_RATE = Convert.ToDecimal(model.PTRL_RATE);
                    decimal INS_RATE = Convert.ToDecimal(model.INS_RATE);
                    decimal MNTNC_AMT = Convert.ToDecimal(model.MNTNC_AMT);
                    decimal DRVR_COST = Convert.ToDecimal(model.DRVR_COST);
                    decimal GRPHD_ALL = Convert.ToDecimal(model.GRPHD_ALL);
                    decimal BO_YEAR = Convert.ToDecimal(model.BO_YEAR);
                    decimal GRHD_NO = Convert.ToDecimal(0);


                    OutputParameter<int?> oRetVal = new OutputParameter<int?>();
                    OutputParameter<int> returnValue = new OutputParameter<int>();

                    var res = context.CHRIS_SP_CompEnt_COMP_MANAGERAsync(model.LVL_CODE, model.GRPHD, MIN_SAL, MAX_SAL, MID_SAL,BDGT_PRICE,ANL_DEPC, PTRL_LTR, PTRL_RATE, INS_RATE,MNTNC_AMT, DRVR_COST, GRPHD_ALL, BO_YEAR,GRHD_NO, "Save", ID, oRetVal, returnValue);
                    res.Wait();
                    if (res != null)
                        output = res.Result;

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return output;
        }


        public ViewResponseModel DeleteCompensationEntry(CompensationEntryModel model)
        {
            ViewResponseModel response = new ViewResponseModel();
            try
            {
                CHRIS_Context ic = new CHRIS_Context();
                var context = new CHRIS_ContextProcedures(ic);               
                OutputParameter<int?> oRetVal = new OutputParameter<int?>();


                var dtlListUpdate = context.CHRIS_SP_CompEnt_COMP_DELETEAsync(model.ID, oRetVal);
                dtlListUpdate.Wait();
                if (oRetVal != null && oRetVal.Value == -10)
                {
                    response.responseCode = Constants.ResponseCode.success;
                    response.responseMessage = "Record Deleted successfully";
                }
                else if (oRetVal != null && oRetVal.Value == -40)
                {
                    response.responseCode = Constants.ResponseCode.error;
                    response.responseMessage = "Record Not Found";
                }
                else
                {
                    response.responseCode = Constants.ResponseCode.error;
                    response.responseMessage = "Error Deleting Record";
                }

                ic.Dispose();
                context = null;
            }
            catch (Exception ex)
            {
                response.responseCode = Constants.ResponseCode.error;
                response.responseMessage = "Error Deleting Record";
            }
            return response;
        }



        #endregion



        #region CompensationEntry_GroupHead


        public List<CHRIS_SP_GROUP_HEAD_GROUP_HEAD_GETALLResult> CompensationEntry_GroupHead()
        {
            List<CHRIS_SP_GROUP_HEAD_GROUP_HEAD_GETALLResult> output = new List<CHRIS_SP_GROUP_HEAD_GROUP_HEAD_GETALLResult>();
            try
            {
                CHRIS_Context ic = new CHRIS_Context();
                var context = new CHRIS_ContextProcedures(ic);

                OutputParameter<int?> outParam = new OutputParameter<int?>();
                var res = context.CHRIS_SP_GROUP_HEAD_GROUP_HEAD_GETALLAsync(outParam);
                res.Wait();

                output = res.Result;

                ic.Dispose();
                context = null;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return output;
        }


       
        public List<CHRIS_SP_GROUP_HEAD_GROUP_HEAD_MANAGERResult> AddUpdateCompensationEntry_GroupHead(CHRIS_SP_GROUP_HEAD_GROUP_HEAD_MANAGERResult model)
        {
            List<CHRIS_SP_GROUP_HEAD_GROUP_HEAD_MANAGERResult> output = new List<CHRIS_SP_GROUP_HEAD_GROUP_HEAD_MANAGERResult>();
            int result = 0;
            try
            {
                CHRIS_Context ic = new CHRIS_Context();
                var context = new CHRIS_ContextProcedures(ic);
               
                if (model.ID > 0)
                {
                    OutputParameter<int?> ID = new OutputParameter<int?>();
                    decimal GRHD_NO = Convert.ToDecimal(model.GRHD_NO);
                    OutputParameter<int?> oRetVal = new OutputParameter<int?>();
                    OutputParameter<int> returnValue = new OutputParameter<int>();
                    var res = context.CHRIS_SP_GROUP_HEAD_GROUP_HEAD_MANAGERAsync(GRHD_NO,"Update", ID, oRetVal, returnValue);
                    res.Wait();
                    if (res != null)
                        output = res.Result;

                }
                else
                {
                    OutputParameter<int?> ID = new OutputParameter<int?>();
                    decimal GRHD_NO = Convert.ToDecimal(model.GRHD_NO);
                    OutputParameter<int?> oRetVal = new OutputParameter<int?>();
                    OutputParameter<int> returnValue = new OutputParameter<int>();

                    var res = context.CHRIS_SP_GROUP_HEAD_GROUP_HEAD_MANAGERAsync(GRHD_NO, "Save", ID, oRetVal, returnValue);
                    res.Wait();
                    if (res != null)
                        output = res.Result;

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return output;
        }


        public ViewResponseModel DeleteCompensationEntry_GroupHead(CompensationEntryModel model)
        {
            ViewResponseModel response = new ViewResponseModel();
            try
            {
                CHRIS_Context ic = new CHRIS_Context();
                var context = new CHRIS_ContextProcedures(ic);
                OutputParameter<int?> oRetVal = new OutputParameter<int?>();


                var dtlListUpdate = context.CHRIS_SP_GROUP_HEAD_GROUP_HEAD_DELETEAsync(model.ID, oRetVal);
                dtlListUpdate.Wait();
                if (oRetVal != null && oRetVal.Value == -10)
                {
                    response.responseCode = Constants.ResponseCode.success;
                    response.responseMessage = "Record Deleted successfully";
                }
                else if (oRetVal != null && oRetVal.Value == -40)
                {
                    response.responseCode = Constants.ResponseCode.error;
                    response.responseMessage = "Record Not Found";
                }
                else
                {
                    response.responseCode = Constants.ResponseCode.error;
                    response.responseMessage = "Error Deleting Record";
                }

                ic.Dispose();
                context = null;
            }
            catch (Exception ex)
            {
                response.responseCode = Constants.ResponseCode.error;
                response.responseMessage = "Error Deleting Record";
            }
            return response;
        }


     
        #endregion
    }
}
