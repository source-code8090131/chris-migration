﻿using CHRIS.Data;
using CHRIS.Models;
using CHRIS.Common;


namespace CHRIS.Services
{
    public class Category_EntryService
    {
        private static Category_EntryService _instance = null;
        public static Category_EntryService getInstance()
        {
            if (_instance == null)
            {
                _instance = new Category_EntryService();
            }
            return _instance;
        }
        private Category_EntryService() { }

        #region Category_Entry
        public List<CHRIS_SP_CATEGORY_GETALLResult> GetAllCategory_Entry()
        {
            List<CHRIS_SP_CATEGORY_GETALLResult> output = new List<CHRIS_SP_CATEGORY_GETALLResult>();
            try
            {
                CHRIS_Context ic = new CHRIS_Context();
                var context = new CHRIS_ContextProcedures(ic);

                OutputParameter<int?> outParam = new OutputParameter<int?>();
                var res = context.CHRIS_SP_CATEGORY_GETALLAsync(outParam);
                res.Wait();

                output = res.Result;

                ic.Dispose();
                context = null;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return output;
        }

        public int? AddUpdateCategory_Entry(CHRIS_SP_CATEGORY_GETALLResult model)
        {
            int result = 0;
            try
            {
                CHRIS_Context ic = new CHRIS_Context();
                var context = new CHRIS_ContextProcedures(ic);

                if (model.ID > 0)
                {
                    OutputParameter<int?> ID = new OutputParameter<int?>();
                    OutputParameter<int?> outparam1 = new OutputParameter<int?>();
                    var res = context.CHRIS_SP_CATEGORY_UPDATEAsync(model.SP_CAT_CODE, model.SP_DESC, model.ID, outparam1);
                    res.Wait();
                    if (outparam1.Value == -10)
                        result = res.Result;
                    else
                        result = 0;
                }
                else
                {
                    OutputParameter<int?> ID = new OutputParameter<int?>();
                    OutputParameter<int?> outparam3 = new OutputParameter<int?>();

                    var res = context.CHRIS_SP_CATEGORY_ADDAsync(model.SP_CAT_CODE, model.SP_DESC, ID, outparam3);
                    res.Wait();
                    if (outparam3.Value > 0)
                        result = res.Result;
                    else
                        result = 0;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }


        public ViewResponseModel DeleteCategory_Entry(Category_EntryModel model)
        {
            ViewResponseModel response = new ViewResponseModel();
            try
            {
                CHRIS_Context ic = new CHRIS_Context();
                var context = new CHRIS_ContextProcedures(ic);
                OutputParameter<int?> iD = new OutputParameter<int?>();
                OutputParameter<int?> oRetVal = new OutputParameter<int?>();


                var dtlListUpdate = context.CHRIS_SP_CATEGORY_DELETEAsync(model.ID, oRetVal);
                dtlListUpdate.Wait();
                if (oRetVal != null && oRetVal.Value == -10)
                {
                    response.responseCode = Constants.ResponseCode.success;
                    response.responseMessage = "Record Deleted successfully";
                }
                else if (oRetVal != null && oRetVal.Value == -40)
                {
                    response.responseCode = Constants.ResponseCode.error;
                    response.responseMessage = "Record Not Found";
                }
                else
                {
                    response.responseCode = Constants.ResponseCode.error;
                    response.responseMessage = "Error Deleting Record";
                }

                ic.Dispose();
                context = null;
            }
            catch (Exception ex)
            {
                response.responseCode = Constants.ResponseCode.error;
                response.responseMessage = "Error Deleting Record";
            }
            return response;
        }

        #endregion
    }
}
