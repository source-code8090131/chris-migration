﻿using CHRIS.Common;
using CHRIS.Data;
using CHRIS.Models;

namespace CHRIS.Services
{
    public class TIREntryService
    {
        private static TIREntryService _instance = null;
        public static TIREntryService getInstance()
        {
            if (_instance == null)
            {
                _instance = new TIREntryService();
            }
            return _instance;
        }
        private TIREntryService() { }

        #region TIREntry
        public List<CHRIS_SP_TIREnt_TIR_GETALLResult> TIREntry()
        {
            List<CHRIS_SP_TIREnt_TIR_GETALLResult> output = new List<CHRIS_SP_TIREnt_TIR_GETALLResult>();
            try
            {
                CHRIS_Context ic = new CHRIS_Context();
                var context = new CHRIS_ContextProcedures(ic);

                OutputParameter<int?> outParam = new OutputParameter<int?>();
                var res = context.CHRIS_SP_TIREnt_TIR_GETALLAsync(outParam);
                res.Wait();

                output = res.Result;

                ic.Dispose();
                context = null;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return output;
        }

        public ViewResponseModel DeleteTIREntry(TIREntryModel model)
        {
            ViewResponseModel response = new ViewResponseModel();
            try
            {
                CHRIS_Context ic = new CHRIS_Context();
                var context = new CHRIS_ContextProcedures(ic);
                OutputParameter<int?> iD = new OutputParameter<int?>();
                OutputParameter<int?> oRetVal = new OutputParameter<int?>();


                var dtlListUpdate = context.CHRIS_SP_TIREnt_TIR_DELETEAsync(model.ID, oRetVal);
                dtlListUpdate.Wait();
                if (oRetVal != null && oRetVal.Value == -10)
                {
                    response.responseCode = Constants.ResponseCode.success;
                    response.responseMessage = "Record Deleted successfully";
                }
                else if (oRetVal != null && oRetVal.Value == -40)
                {
                    response.responseCode = Constants.ResponseCode.error;
                    response.responseMessage = "Record Not Found";
                }
                else
                {
                    response.responseCode = Constants.ResponseCode.error;
                    response.responseMessage = "Error Deleting Record";
                }

                ic.Dispose();
                context = null;
            }
            catch (Exception ex)
            {
                response.responseCode = Constants.ResponseCode.error;
                response.responseMessage = "Error Deleting Record";
            }
            return response;
        }

        #endregion
    }
}
