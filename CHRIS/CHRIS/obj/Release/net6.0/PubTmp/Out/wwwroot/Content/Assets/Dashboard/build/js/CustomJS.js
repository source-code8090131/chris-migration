﻿//#region Custom Template Js 
$(window).on('load', function () {
    $('#loading').hide();
});

//$(window).on('load', function () {
//    $('#loading1').hide();
//});
$(function () {
    //$("#example1").DataTable({
    //    "responsive": true, "lengthChange": false, "autoWidth": false, "order": false
    //}).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');

   
    $(".dTable").DataTable({
        "responsive": true, "lengthChange": false, "autoWidth": false, "order": false
    }).buttons().container().appendTo('.dTable_wrapper .col-md-6:eq(0)');

    $("#UserListData").DataTable({
        "responsive": true, "lengthChange": false, "autoWidth": false, "order": false
    }).buttons().container().appendTo('#UserListData_wrapper .col-md-6:eq(0)');

    $("#CostTypesPriceTermsList").DataTable({
        "responsive": true, "lengthChange": false, "autoWidth": false, "order": false
    }).buttons().container().appendTo('#CostTypesPriceTermsList_wrapper .col-md-6:eq(0)');

    $("#RoleListData").DataTable({
        "responsive": true, "lengthChange": false, "autoWidth": false, "order": false
    }).buttons().container().appendTo('#RoleListData_wrapper .col-md-6:eq(0)');

    $("#TypesOfBusiness").DataTable({
        "responsive": true, "lengthChange": false, "autoWidth": false, "order": false
    }).buttons().container().appendTo('#TypesOfBusiness_wrapper .col-md-6:eq(0)');

    $("#DescripenciesInfo").DataTable({
        "responsive": true, "lengthChange": false, "autoWidth": false, "order": false
    }).buttons().container().appendTo('#DescripenciesInfo_wrapper .col-md-6:eq(0)');

    $("#LCTypesList").DataTable({
        "responsive": true, "lengthChange": false, "autoWidth": false, "order": false
    }).buttons().container().appendTo('#LCTypesList_wrapper .col-md-6:eq(0)');

    $("#InsurancePolicyInfo").DataTable({
        "responsive": true, "lengthChange": false, "autoWidth": false, "order": false
    }).buttons().container().appendTo('#InsurancePolicyInfo_wrapper .col-md-6:eq(0)');

    $("#HsCodeList").DataTable({
        "responsive": true, "lengthChange": false, "autoWidth": false, "order": false
    }).buttons().container().appendTo('#HsCodeList_wrapper .col-md-6:eq(0)');

    $("#HSCODESROs").DataTable({
        "responsive": true, "lengthChange": false, "autoWidth": false, "order": false
    }).buttons().container().appendTo('#HSCODESROs_wrapper .col-md-6:eq(0)');

    //Initialize Select2 Elements
    $('.select2').select2()
    //Initialize Select2 Elements
    $('.select2bs4').select2({
        theme: 'bootstrap4'
    })
});
$(document).ready(function () {
    $('[data-toggle="tooltip"]').tooltip();
});
//#endregion

//#region Browser Back Button
$("[name='BrowserBack']").click(function (button) {
    history.back();
});
//#endregion


