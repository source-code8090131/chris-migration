using Microsoft.EntityFrameworkCore;
using System.Xml.Serialization;
using CHRIS.Data;
using DocumentFormat.OpenXml.Office2016.Drawing.ChartDrawing;
using CHRIS.Data;

var builder = WebApplication.CreateBuilder(args);
builder.Services.AddDbContext<CHRIS_Context>(options => options.UseSqlServer(builder.Configuration.GetConnectionString("cs")));

// Add services to the container.
builder.Services.AddControllersWithViews();
builder.Services.AddDistributedMemoryCache();
builder.Services.AddSession();
builder.Services.AddResponseCaching();
builder.Services.AddAuthentication("auth")
            .AddCookie("auth", config =>
            {
                config.Cookie.Name = "authcookie";
                config.LoginPath = "/Login/Index";
            });
builder.Services.AddHttpContextAccessor();
var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
}
app.UseStaticFiles();
app.UseSession();
app.UseRouting();
app.UseResponseCaching();

app.UseAuthorization();

//app.MapControllerRoute(
//    name: "default",
//    pattern: "{controller=Home}/{action=Index}/{id?}");//pattern: "{controller=Setup}/{action=BranchSelect}/{id?}");//old

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Authentication}/{action=Index}/{id?}");//pattern: "{controller=Setup}/{action=BranchSelect}/{id?}");//old


app.Run();
