﻿namespace CHRIS.ReportModels
{
    public class PRREP23Model
    {
        public int? PNO { get; set; }
        public string AuthName { get; set; }
        public string AuthDescription { get; set; }
        public string AuthPlace { get; set; }
    }
}
