﻿namespace CHRIS.ReportModels
{
    public class PRREP07Model
    {
        public string Hold { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string Branch { get; set; }
        public string Segment { get; set; }
        public string Desig { get; set; }
    }
}
