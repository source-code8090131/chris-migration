﻿namespace CHRIS.ReportModels
{
    public class PRREP46Model
    {
        public string Branch { get; set; }
        public string Segment { get; set; }
        public string Desig { get; set; }
        public string FromPrNo { get; set; }
        public string EndPrNo { get; set; }
        public DateTime? From_Date { get; set; }
        public DateTime? To_Date { get; set; }
    }
}
