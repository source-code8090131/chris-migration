﻿namespace CHRIS.ReportModels
{
    public class CITI_EMP2Model
    {
        public string Branch { get; set; }
        public string FromPNo { get; set; }
        public string ToPNo { get; set; }
        public string Segment { get; set; }
        public string Department { get; set; }
        public string Level { get; set; }
        public string Desig { get; set; }
        public string Group { get; set; }
    }
}
