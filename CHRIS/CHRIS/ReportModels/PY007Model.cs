﻿namespace CHRIS.ReportModels
{
    public class PY007Model
    {
        public string Branch { get; set; }
        public string Segment { get; set; }
        public int? Month { get; set; }
        public int? Year { get; set; }
        public string Category { get; set; }
    }
}
