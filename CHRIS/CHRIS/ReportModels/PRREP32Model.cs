﻿namespace CHRIS.ReportModels
{
    public class PRREP32Model
    {
        public string Branch { get; set; }
        public string Segment { get; set; }
        public string Desig { get; set; }
        public decimal? Year { get; set; }
        public DateTime? TodayDate { get; set; }
    }
}
