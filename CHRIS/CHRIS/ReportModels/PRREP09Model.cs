﻿namespace CHRIS.ReportModels
{
    public class PRREP09Model
    {
        public string Branch { get; set; }
        public string Desig { get; set; }
        public string Segment { get; set; }
        public DateTime? TodayDate { get; set; }
    }
}
