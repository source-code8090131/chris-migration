﻿namespace CHRIS.ReportModels
{
    public class PRREP51Model
    {
        public string Branch { get; set; }
        public string Segment { get; set; }
        public string Group { get; set; }
    }
}
