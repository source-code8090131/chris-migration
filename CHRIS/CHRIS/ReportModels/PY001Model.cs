﻿namespace CHRIS.ReportModels
{
    public class PY001Model
    {
        public string Branch { get; set; }
        public DateTime? From_Date { get; set; }
        public DateTime? To_Date { get; set; }
    }
}
