﻿namespace CHRIS.ReportModels
{
    public class PY013Model
    {
        public string Name { get; set; }
        public string LastName { get; set; }
        public decimal? Rate { get; set; }
        public string InitOfficer { get; set; }
        public string SeniorOfficer { get; set; }
        public decimal? PNO { get; set; }
    }
}
