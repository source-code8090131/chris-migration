﻿namespace CHRIS.ReportModels
{
    public class PRREP08Model
    {
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string Branch { get; set; }
    }
}
