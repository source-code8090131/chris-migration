﻿namespace CHRIS.ReportModels
{
    public class PY016Model
    {
        public int? Month { get; set; }
        public decimal? FPNO { get; set; }
        public decimal? EPNO { get; set; }
    }
}
