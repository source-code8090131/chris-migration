﻿namespace CHRIS.ReportModels
{
    public class PY005Model
    {
        public string City { get; set; }
        public decimal? Month { get; set; }
        public string Segment { get; set; }
        public string Category { get; set; }
        public decimal? Year { get; set; }
    }
}
