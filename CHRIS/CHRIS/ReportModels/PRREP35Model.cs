﻿namespace CHRIS.ReportModels
{
    public class PRREP35Model
    {
        public string Branch { get; set; }
        public int? SPNO { get; set; }
        public int? TPNO { get; set; }
        public string Segment { get; set; }
        public string Department { get; set; }
        public string Level { get; set; }
        public int? Year { get; set; }
    }
}
