﻿namespace CHRIS.ReportModels
{
    public class PRREP13Model
    {
        public string Branch { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
    }
}
