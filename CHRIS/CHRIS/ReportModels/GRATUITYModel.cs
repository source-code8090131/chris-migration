﻿namespace CHRIS.ReportModels
{
    public class GRATUITYModel
    {
        public int? PNO { get; set; }
        public int? Year { get; set; }
        public string Branch { get; set; }
        public string Department { get; set; }
        public DateTime? TerminationDate { get; set; }
        public string Trustee1 { get; set; }
        public string Trustee2 { get; set; }
    }
}
