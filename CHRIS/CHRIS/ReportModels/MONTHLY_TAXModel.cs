﻿namespace CHRIS.ReportModels
{
    public class MONTHLY_TAXModel
    {
        public string Branch { get; set; }
        public DateTime? Month { get; set; }
    }
}
