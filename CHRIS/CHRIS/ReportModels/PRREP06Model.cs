﻿namespace CHRIS.ReportModels
{
    public class PRREP06Model
    {
        public DateTime? FromDate{ get; set; }
        public DateTime? EndDate { get; set; }
        public string Segment { get; set; }
        public string Branch { get; set; }
        public string Desig { get; set; }
        public string Group { get; set; }
    }
}
