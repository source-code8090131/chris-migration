﻿namespace CHRIS.ReportModels
{
    public class PRREP39Model
    {
        public string Branch { get; set; }
        public string Segment { get; set; }
        public string Desig { get; set; }
        public DateTime? Today_Date { get; set; }
    }
}
