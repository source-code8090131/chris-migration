﻿namespace CHRIS.ReportModels
{
    public class PY002Model
    {
        public string Branch { get; set; }
        public int? Month { get; set; }
        public int? Year { get; set; }
        public string Flag { get; set; }
        public string Category { get; set; }
        public string Segment { get; set; }
    }
}
