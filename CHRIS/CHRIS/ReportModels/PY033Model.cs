﻿namespace CHRIS.ReportModels
{
    public class PY033Model
    {
        public string Branch { get; set; }
        public string Segment { get; set; }
        public string Year { get; set; }
    }
}
