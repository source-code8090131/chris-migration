﻿namespace CHRIS.ReportModels
{
    public class FNREP01Model
    {
        public string Branch { get; set; }
        public DateTime? Date { get; set; }
        public string Segment { get; set; }
    }
}
