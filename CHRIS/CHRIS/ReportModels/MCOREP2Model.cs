﻿namespace CHRIS.ReportModels
{
    public class MCOREP2Model
    {
        public decimal? Balance { get; set; }
        public string Branch { get; set; }
        public string Segment { get; set; }
    }
}
