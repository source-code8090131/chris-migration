﻿namespace CHRIS.ReportModels
{
    public class PFR07NEWModel
    {
        public int? Year { get; set; }
        public int? PNO { get; set; }
        public string Branch { get; set; }
        public string Department { get; set; }
        public DateTime? TerminationDate { get; set; }
        public string Trustee1 { get; set; }
        public string Trustee2 { get; set; }
        public string Zakat { get; set; }
    }
}
