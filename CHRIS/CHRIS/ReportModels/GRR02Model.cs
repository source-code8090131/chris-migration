﻿namespace CHRIS.ReportModels
{
    public class GRR02Model
    {
        public int? FPNO { get; set; }
        public int? TPNO { get; set; }
        public string Segment { get; set; }
        public string Branch { get; set; }
        public string Department { get; set; }
    }
}
