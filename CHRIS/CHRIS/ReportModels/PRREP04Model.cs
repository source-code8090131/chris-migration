﻿namespace CHRIS.ReportModels
{
    public class PRREP04Model
    {
        public string Branch { get; set; }
        public DateTime? IssueDate { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public string NewBranch { get; set; }
        public string Segment { get; set; }
        public string Desig { get; set; }
    }
}
