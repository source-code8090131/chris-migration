﻿namespace CHRIS.ReportModels
{
    public class PY015Model
    {
        public string Branch { get; set; }
        public int? Month { get; set; }
        public int? Year { get; set; }
    }
}
