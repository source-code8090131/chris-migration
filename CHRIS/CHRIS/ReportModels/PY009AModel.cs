﻿namespace CHRIS.ReportModels
{
    public class PY009AModel
    {
        public int? Month { get; set; }
        public int? Year { get; set; }
        public string Branch { get; set; }
        public string Segment { get; set; }
    }
}
