﻿namespace CHRIS.ReportModels
{
    public class PRREP16Model
    {
        public string Branch { get; set; }
        public string Group { get; set; }
        public string Segment { get; set; }
        public DateTime? Date { get; set; }
        public DateTime? Year { get; set; }
    }
}
