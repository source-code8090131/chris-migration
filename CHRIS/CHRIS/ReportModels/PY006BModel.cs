﻿namespace CHRIS.ReportModels
{
    public class PY006BModel
    {
        public string Segment { get; set; }
        public string Branch { get; set; }
        public int? Month { get; set; }
        public int? Year { get; set; }
        public string Category { get; set; }
    }
}
