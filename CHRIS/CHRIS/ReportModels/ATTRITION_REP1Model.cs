﻿namespace CHRIS.ReportModels
{
    public class ATTRITION_REP1Model
    {
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string Category { get; set; }
        public string Type { get; set; }
        public string Branch { get; set; }
        public string Segment { get; set; }
        public string Group { get; set; }
        public string Department { get; set; }
        public string Order { get; set; }
    }
}
