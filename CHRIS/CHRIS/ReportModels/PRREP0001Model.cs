﻿namespace CHRIS.ReportModels
{
    public class PREP0001Model
    {
        public string Branch { get; set; }
        public string Segment { get; set; }
        public string Department { get; set; }
        public string Group { get; set; }
        public string Desig { get; set; }
        public string Level { get; set; }
    }
}
