﻿namespace CHRIS.ReportModels
{
    public class PRREP02Model
    {
        public string Branch { get; set; }
        public string Segment { get; set; }
        public string Department { get; set; }
        public string Group { get; set; }
        public DateTime? RunDate { get; set; }
    }
}
