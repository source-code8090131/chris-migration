﻿namespace CHRIS.ReportModels
{
    public class PY011_BModel
    {
        public string Branch { get; set; }
        public string Mode { get; set; }
        public string Segment { get; set; }
        public string Department { get; set; }
        public string Category { get; set; }
        public string FPNO { get; set; }
        public string TPNO { get; set; }
        public int? Month { get; set; }
        public int? Year { get; set; }
    }
}
