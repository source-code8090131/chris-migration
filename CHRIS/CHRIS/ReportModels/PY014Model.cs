﻿namespace CHRIS.ReportModels
{
    public class PY014Model
    {
        public string Branch { get; set; }
        public int? MonthYear { get; set; }
        public int? PNO { get; set; }
        public string Segment { get; set; }
    }
}
