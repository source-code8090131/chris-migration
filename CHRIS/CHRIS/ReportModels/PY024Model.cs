﻿namespace CHRIS.ReportModels
{
    public class PY024Model
    {
        public DateTime? RunDate { get; set; }
        public int? Month { get; set; }
        public int? Year { get; set; }
        public string Branch { get; set; }
    }
}
