﻿namespace CHRIS.ReportModels
{
    public class PY032Model
    {
        public int? Year { get; set; }
        public string Branch { get; set; }
        public string Segment { get; set; }
    }
}
