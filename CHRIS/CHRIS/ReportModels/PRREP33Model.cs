﻿namespace CHRIS.ReportModels
{
    public class PRREP33Model
    {
        public string Branch { get; set; }
        public string Segment { get; set; }
        public string Desig { get; set; }
        public int? Year { get; set; }
    }
}
