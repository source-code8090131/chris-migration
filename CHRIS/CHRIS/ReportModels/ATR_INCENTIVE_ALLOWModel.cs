﻿namespace CHRIS.ReportModels
{
    public class ATR_INCENTIVE_ALLOWModel
    {
        public string Branch { get; set; }
        public string Category { get; set; }
        public string Allow { get; set; }
        public int? EmployeeNo { get; set; }
    }
}
