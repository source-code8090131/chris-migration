﻿namespace CHRIS.ReportModels
{
    public class PY004Model
    {
        public int? Month { get; set; }
        public string City { get; set; }
        public string Segment { get; set; }
        public string Category { get; set; }
        public int? Year { get; set; }
    }
}
