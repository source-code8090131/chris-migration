﻿namespace CHRIS.ReportModels
{
    public class PRREP52Model
    {
        public string Branch { get; set; }
        public string Segment { get; set; }
        public string Group { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? EndDate { get; set; }
    }
}
