﻿namespace CHRIS.ReportModels
{
    public class PY023Model
    {
        public string City { get; set; }
        public string Segment { get; set; }
        public string Category { get; set; }
        public string Month { get; set; }
        public string Year { get; set; }
    }
}
