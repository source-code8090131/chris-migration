﻿namespace CHRIS.ReportModels
{
    public class FNREP09Model
    {
        public string Branch { get; set; }
        public string Segment { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public string Flag { get; set; }
    }
}
