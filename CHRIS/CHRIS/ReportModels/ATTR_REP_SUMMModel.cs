﻿namespace CHRIS.ReportModels
{
    public class ATTR_REP_SUMMModel
    {
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string Branch { get; set; }
        public string Segment { get; set; }
    }
}
