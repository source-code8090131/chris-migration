﻿namespace CHRIS.ReportModels
{
    public class PY017Model
    {
        public int? Start_PNo { get; set; }
        public int? End_PNo { get; set; }
        public string Branch { get; set; }
        public string Month { get; set; }
    }
}
