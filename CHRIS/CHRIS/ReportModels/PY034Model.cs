﻿namespace CHRIS.ReportModels
{
    public class PY034Model
    {
        public DateTime? DateTime { get; set; }
        public decimal? Bonus { get; set; }
    }
}
