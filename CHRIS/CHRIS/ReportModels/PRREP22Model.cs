﻿namespace CHRIS.ReportModels
{
    public class PRREP22Model
    {
        public decimal? PNO { get; set; }
        public string AuthName { get; set; }
        public string AuthDesig { get; set; }
        public string AuthPlace { get; set; }
    }
}
