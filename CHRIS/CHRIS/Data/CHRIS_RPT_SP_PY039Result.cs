﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace CHRIS.Data
{
    public partial class CHRIS_RPT_SP_PY039Result
    {
        public decimal? pr_p_no { get; set; }
        public string name { get; set; }
        public DateTime? pr_joining_date { get; set; }
        public DateTime? pr_confirm_on { get; set; }
        public string pr_segment { get; set; }
        public DateTime? un_promotion { get; set; }
        public string un_level { get; set; }
        public decimal? un_org { get; set; }
        public decimal? un_step { get; set; }
        public decimal? un_adj { get; set; }
        public decimal? un_sal_arear { get; set; }
        public decimal? un_pfund { get; set; }
        public string BRANCH { get; set; }
        public string HEADING { get; set; }
    }
}
