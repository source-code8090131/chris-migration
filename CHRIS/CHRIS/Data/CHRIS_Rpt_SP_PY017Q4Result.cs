﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace CHRIS.Data
{
    public partial class CHRIS_Rpt_SP_PY017Q4Result
    {
        public int? rec { get; set; }
        public decimal s_hr { get; set; }
        public decimal? oth_p_no { get; set; }
    }
}
