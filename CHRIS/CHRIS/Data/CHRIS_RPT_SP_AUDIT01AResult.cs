﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace CHRIS.Data
{
    public partial class CHRIS_RPT_SP_AUDIT01AResult
    {
        public string FN_TYPE { get; set; }
        public string FN_DESC { get; set; }
        public decimal? FN_MARKUP { get; set; }
        public decimal? FN_SCHEDULE { get; set; }
        public decimal? FN_LESS_AMT { get; set; }
        public decimal? FN_EX_LESS { get; set; }
        public decimal? FN_EX_GREAT { get; set; }
        public decimal? FN_ELIG { get; set; }
        public decimal? FN_C_RATIO { get; set; }
        public string FN_CONFIRM { get; set; }
        public string ftype { get; set; }
        public string usr { get; set; }
        public string stts { get; set; }
        public DateTime? run_date { get; set; }
    }
}
