﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace CHRIS.Data
{
    public partial class CHRIS_RPT_SP_GRR01Result
    {
        public decimal? p_no { get; set; }
        public string name { get; set; }
        public string pr_level { get; set; }
        public string pr_desig { get; set; }
        public DateTime? j_date { get; set; }
        public decimal? pack { get; set; }
        public string category { get; set; }
        public decimal? BASIC { get; set; }
        public decimal? grauity { get; set; }
    }
}
