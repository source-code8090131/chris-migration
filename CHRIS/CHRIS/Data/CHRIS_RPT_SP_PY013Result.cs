﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace CHRIS.Data
{
    public partial class CHRIS_RPT_SP_PY013Result
    {
        public string pr_first_name { get; set; }
        public string pr_desig { get; set; }
        public decimal? pr_p_no { get; set; }
        public string CITY { get; set; }
        public DateTime? pr_last_increment { get; set; }
        public decimal? pr_annual_present15 { get; set; }
        public decimal? PR_INC_PER { get; set; }
        public decimal? PR_ANNUAL_PRESENT { get; set; }
        public string NATIONAL1 { get; set; }
        public DateTime? PR_EFFECTIVE { get; set; }
        public decimal? RATE { get; set; }
        public decimal? PR_ANNUAL_PREVIOUS { get; set; }
        public decimal? PR_INC_AMT { get; set; }
        public decimal? inc_pr_inc_per { get; set; }
        public decimal? inc_PR_ANNUAL_PRESENT { get; set; }
        public string PR_INC_TYPE { get; set; }
        public decimal? PR_IN_NO { get; set; }
        public string PR_REMARKS { get; set; }
        public string PR_LEVEL_PRESENT { get; set; }
        public string INITOFF { get; set; }
        public string SEN { get; set; }
    }
}
