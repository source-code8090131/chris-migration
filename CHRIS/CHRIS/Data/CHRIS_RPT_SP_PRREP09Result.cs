﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace CHRIS.Data
{
    public partial class CHRIS_RPT_SP_PRREP09Result
    {
        public string BRANCH { get; set; }
        public string SEGG { get; set; }
        public string DEPT { get; set; }
        public decimal? PR_P_NO { get; set; }
        public string NAME { get; set; }
        public decimal? CONTRIB { get; set; }
        public string LVL { get; set; }
        public string FUNCT_T { get; set; }
        public string MGR { get; set; }
        public string BRN { get; set; }
        public string SEG { get; set; }
    }
}
