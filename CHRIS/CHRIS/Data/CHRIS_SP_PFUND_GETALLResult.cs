﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace CHRIS.Data
{
    public partial class CHRIS_SP_PFUND_GETALLResult
    {
        public int ID { get; set; }
        public decimal? PF_PR_P_NO { get; set; }
        public DateTime? PF_YEAR { get; set; }
        public decimal? PF_EMP_CONT { get; set; }
        public decimal? PF_BNK_CONT { get; set; }
        public decimal? PF_INTEREST { get; set; }
        public decimal? PF_PREMIUM { get; set; }
    }
}
