﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
#nullable disable
using System;
using System.Collections.Generic;

namespace CHRIS.Data;

public partial class GOALS_MASTER
{
    public decimal GO_P_NO { get; set; }

    public decimal GO_GOALS_FOR_THE_YEAR { get; set; }

    public string GO_GOAL_RCVD { get; set; }

    public DateTime? GO_GOAL_DATE { get; set; }

    public string GO_REMARK { get; set; }

    public int ID { get; set; }
}