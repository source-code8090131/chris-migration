﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
#nullable disable
using System;
using System.Collections.Generic;

namespace CHRIS.Data;

public partial class TRAINING
{
    public decimal? TR_PR_NO { get; set; }

    public string TR_LOC_OVRS { get; set; }

    public string TR_PROG_NAME { get; set; }

    public DateTime? TR_PROG_DATE { get; set; }

    public string TR_FTR_PROG_NAME { get; set; }

    public DateTime? TR_FTR_PROG_DATE { get; set; }

    public string TR_COUNTRY { get; set; }

    public int ID { get; set; }
}