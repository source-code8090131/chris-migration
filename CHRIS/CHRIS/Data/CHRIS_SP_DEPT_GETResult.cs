﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace CHRIS.Data
{
    public partial class CHRIS_SP_DEPT_GETResult
    {
        [Column("@ID")]
        public int ID { get; set; }
        [Column("@SP_SEGMENT_D")]
        public string SP_SEGMENT_D { get; set; }
        [Column("@SP_GROUP_D")]
        public string SP_GROUP_D { get; set; }
        [Column("@SP_DEPT")]
        public string SP_DEPT { get; set; }
        [Column("@SP_DESC_D1")]
        public string SP_DESC_D1 { get; set; }
        [Column("@SP_DESC_D2")]
        public string SP_DESC_D2 { get; set; }
        [Column("@SP_DEPT_HEAD")]
        public string SP_DEPT_HEAD { get; set; }
        [Column("@SP_DATE_FROM_D")]
        public DateTime? SP_DATE_FROM_D { get; set; }
        [Column("@SP_DATE_TO_D")]
        public DateTime? SP_DATE_TO_D { get; set; }
        [Column("@RC")]
        public string RC { get; set; }
        [Column("@SP_RC_APA")]
        public string SP_RC_APA { get; set; }
    }
}
