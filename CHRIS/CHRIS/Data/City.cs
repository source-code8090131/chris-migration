﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
#nullable disable
using System;
using System.Collections.Generic;

namespace CHRIS.Data;

public partial class CITY
{
    public decimal? CITY_CD { get; set; }

    public string CITY_NAME { get; set; }

    public int ID { get; set; }
}