﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace CHRIS.Data
{
    public partial class CHRIS_SP_FINANCE_APPLICATON_PROC_1Result
    {
        public double? CREDIT_RATIO { get; set; }
        public decimal? RATIO_AVAILED { get; set; }
        public double? AVAILABLE { get; set; }
        public double? FACTOR { get; set; }
        public double? AMT_Can_Avail { get; set; }
        public decimal? Cap_Amt { get; set; }
    }
}
