﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace CHRIS.Data
{
    public partial class CHRIS_RPT_SP_FNREP06_SUBResult
    {
        public double? lbal25 { get; set; }
        public double? lbal20 { get; set; }
        public double? lbal19 { get; set; }
        public double? lbal18 { get; set; }
        public double? lbal17 { get; set; }
        public double? lbal16 { get; set; }
        public double? lbal15 { get; set; }
        public double? lbal14 { get; set; }
        public double? lbal13 { get; set; }
        public double? lbal12 { get; set; }
        public double? lbal11 { get; set; }
        public double? lbal10 { get; set; }
        public double? lbal9 { get; set; }
        public double? lbal8 { get; set; }
        public double? lbal7 { get; set; }
        public double? lbal6 { get; set; }
        public double? lbal5 { get; set; }
        public double? lbal4 { get; set; }
        public double? lbal3 { get; set; }
        public double? lbal2 { get; set; }
        public double? lbal1 { get; set; }
        public string fn_fin_no { get; set; }
        public string branch { get; set; }
        public string seg { get; set; }
    }
}
