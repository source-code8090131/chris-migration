﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace CHRIS.Data
{
    public partial class CHRIS_SP_SHIFT_OVERTIME_MANAGERResult
    {
        [Column("@ID")]
        public int ID { get; set; }
        [Column("@OT_P_NO")]
        public decimal? OT_P_NO { get; set; }
        [Column("@OT_DATE")]
        public DateTime? OT_DATE { get; set; }
        [Column("@OT_SEGMENT")]
        public string OT_SEGMENT { get; set; }
        [Column("@OT_DEPT")]
        public string OT_DEPT { get; set; }
        [Column("@OT_DEFAULT")]
        public string OT_DEFAULT { get; set; }
        [Column("@OT_TIME_IN")]
        public string OT_TIME_IN { get; set; }
        [Column("@OT_TIME_OUT")]
        public string OT_TIME_OUT { get; set; }
        [Column("@OT_SINGLE_HR")]
        public decimal? OT_SINGLE_HR { get; set; }
        [Column("@OT_DOUBLE_HR")]
        public decimal? OT_DOUBLE_HR { get; set; }
        [Column("@OT_SD_WEEK")]
        public decimal? OT_SD_WEEK { get; set; }
        [Column("@OT_MEAL_CODE")]
        public string OT_MEAL_CODE { get; set; }
        [Column("@OT_MEAL_AMT")]
        public decimal? OT_MEAL_AMT { get; set; }
        [Column("@OT_CONV_CODE")]
        public string OT_CONV_CODE { get; set; }
        [Column("@OT_CONV_AMT")]
        public decimal? OT_CONV_AMT { get; set; }
        [Column("@OT_FRI_HOL")]
        public string OT_FRI_HOL { get; set; }
    }
}
