﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace CHRIS.Data
{
    public partial class CHRIS_RPT_SP_PY044Result
    {
        public decimal? pr_p_no { get; set; }
        public string name { get; set; }
        public string desig { get; set; }
        public string levl { get; set; }
        public int? DAYS { get; set; }
        public decimal? c09 { get; set; }
        public decimal c03 { get; set; }
        public decimal c05 { get; set; }
        public decimal c12 { get; set; }
        public decimal c13 { get; set; }
        public decimal ot { get; set; }
        public decimal conv_add { get; set; }
        public string wyear { get; set; }
        public DateTime? sdate { get; set; }
        public DateTime? edate { get; set; }
    }
}
