﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace CHRIS.Data
{
    public partial class CHRIS_RPT_SP_PFR02Result
    {
        public string brn { get; set; }
        public string br_name { get; set; }
        public decimal? BAL_EMP { get; set; }
        public decimal? BAL_BANK { get; set; }
        public decimal? BAL_INS { get; set; }
        public decimal? BAL_TOT { get; set; }
        public decimal? PF_PREMIUM2 { get; set; }
        public string branch_name { get; set; }
        public DateTime? RUN_DATE { get; set; }
        public decimal? pf_rate { get; set; }
        public decimal? PRESENT_EMP_BAL { get; set; }
        public decimal? PRESENT_BANK_BAL { get; set; }
    }
}
