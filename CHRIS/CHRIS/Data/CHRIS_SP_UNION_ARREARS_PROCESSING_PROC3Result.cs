﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace CHRIS.Data
{
    public partial class CHRIS_SP_UNION_ARREARS_PROCESSING_PROC3Result
    {
        public decimal? PR_IN_NO { get; set; }
        public DateTime? PR_EFFECTIVE { get; set; }
        public string PR_LEVEL_PRESENT { get; set; }
        public decimal? PR_ANNUAL_PRESENT { get; set; }
        public string pr_promoted { get; set; }
        public decimal? pr_annual_previous { get; set; }
        public string pr_level_previous { get; set; }
    }
}
