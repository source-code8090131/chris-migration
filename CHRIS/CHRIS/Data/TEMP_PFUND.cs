﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
#nullable disable
using System;
using System.Collections.Generic;

namespace CHRIS.Data;

public partial class TEMP_PFUND
{
    public decimal PF_PR_P_NO { get; set; }

    public decimal? PF_SETTLEMENT { get; set; }

    public decimal? PF_EMP_CONT { get; set; }

    public decimal? PF_INTEREST { get; set; }

    public int ID { get; set; }
}