﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace CHRIS.Data
{
    public partial class CHRIS_RPT_SP_PRREP33_SUB1Result
    {
        public int ID { get; set; }
        public decimal? pr_d_no { get; set; }
        public string dept { get; set; }
        public string eff_date { get; set; }
    }
}
