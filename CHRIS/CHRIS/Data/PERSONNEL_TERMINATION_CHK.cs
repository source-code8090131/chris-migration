﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
#nullable disable
using System;
using System.Collections.Generic;

namespace CHRIS.Data;

public partial class PERSONNEL_TERMINATION_CHK
{
    public decimal? PR_P_NO { get; set; }

    public string PR_FIRST_NAME { get; set; }

    public string PR_TERMIN_TYPE { get; set; }

    public DateTime? PR_TERMIN_DATE { get; set; }

    public string PR_REASONS { get; set; }

    public string PR_RESIG_RECV { get; set; }

    public string PR_APP_RESIG { get; set; }

    public string PR_EXIT_INTER { get; set; }

    public string PR_ID_RETURN { get; set; }

    public string PR_NOTICE { get; set; }

    public string PR_ARTONY { get; set; }

    public string PR_DELETION { get; set; }

    public string PR_SETTLE { get; set; }

    public int ID { get; set; }

    public string PR_EMAIL { get; set; }

    public string PR_UserID { get; set; }

    public string PR_UploadDate { get; set; }

    public bool? PR_User_Approved { get; set; }
}