﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace CHRIS.Data
{
    public partial class CHRIS_Rpt_Sp_Py006b_2Result
    {
        public int? PR_P_NO { get; set; }
        public decimal? CONV3Formula { get; set; }
        public decimal? CONV1Formula { get; set; }
    }
}
