﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace CHRIS.Data
{
    public partial class CHRIS_SP_FIN_BOOK_COLLAT_GETResult
    {
        [Column("@ID")]
        public int ID { get; set; }
        [Column("@FN_P_NO")]
        public decimal FN_P_NO { get; set; }
        [Column("@FN_FINANCE_NO")]
        public string FN_FINANCE_NO { get; set; }
        [Column("@FN_COLLAT_NO")]
        public string FN_COLLAT_NO { get; set; }
        [Column("@FN_PARTY1")]
        public string FN_PARTY1 { get; set; }
        [Column("@FN_PARTY2")]
        public string FN_PARTY2 { get; set; }
        [Column("@FN_COLLAT_DT")]
        public DateTime? FN_COLLAT_DT { get; set; }
        [Column("@FN_DOC_RCV_DT")]
        public DateTime? FN_DOC_RCV_DT { get; set; }
    }
}
