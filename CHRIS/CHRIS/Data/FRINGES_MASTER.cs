﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
#nullable disable
using System;
using System.Collections.Generic;

namespace CHRIS.Data;

public partial class FRINGES_MASTER
{
    public string BRF_CODE { get; set; }

    public string BRF_BRN_CODE { get; set; }

    public string BRF_DESC { get; set; }

    public decimal? BRF_YAMT { get; set; }

    public decimal? BRF_MAMT { get; set; }

    public string BRF_CATEGORY { get; set; }

    public string BRF_SF_FLAG { get; set; }

    public string BRF_CALC { get; set; }

    public string BRF_CALC_PB { get; set; }

    public string BRF_CALC_ALL1 { get; set; }

    public string BRF_CALC_ALL2 { get; set; }

    public string BRF_CALC_ALL3 { get; set; }

    public decimal? BRF_CALC_PER { get; set; }

    public string BRF_DR_AC { get; set; }

    public string BRF_DR_DESC1 { get; set; }

    public string BRF_DR_DESC2 { get; set; }

    public string BRF_DR_DESC3 { get; set; }

    public string BRF_CR_AC { get; set; }

    public string BRF_CR_DESC1 { get; set; }

    public string BRF_CR_DESC2 { get; set; }

    public string BRF_CR_DESC3 { get; set; }

    public int ID { get; set; }
}