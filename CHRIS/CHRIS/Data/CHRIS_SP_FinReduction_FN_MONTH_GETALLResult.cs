﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace CHRIS.Data
{
    public partial class CHRIS_SP_FinReduction_FN_MONTH_GETALLResult
    {
        public int ID { get; set; }
        public decimal? FN_MP_NO { get; set; }
        public string FN_M_BRANCH { get; set; }
        public string FN_TYPE { get; set; }
        public string FN_FIN_NO { get; set; }
        public DateTime? FN_MDATE { get; set; }
        public decimal? FN_DEBIT { get; set; }
        public decimal? FN_CREDIT { get; set; }
        public decimal? FN_PAY_LEFT { get; set; }
        public decimal? FN_BALANCE { get; set; }
        public decimal? FN_LOAN_BALANCE { get; set; }
        public decimal? FN_MARKUP { get; set; }
        public string FN_LIQ_FLAG { get; set; }
        public string FN_SUBTYPE { get; set; }
    }
}
