﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace CHRIS.Data
{
    public partial class CHRIS_SP_RegStHiEnt_PRE_EMP_GETALLResult
    {
        public int ID { get; set; }
        public decimal? PR_P_NO { get; set; }
        public string PR_JOB_FROM { get; set; }
        public string PR_JOB_TO { get; set; }
        public string PR_ORGANIZ { get; set; }
        public string PR_DESIG_PR { get; set; }
        public string PR_REMARKS_PR { get; set; }
        public string PR_ADD1 { get; set; }
        public string PR_ADD2 { get; set; }
        public string PR_ADD3 { get; set; }
        public string PR_LET_SNT { get; set; }
        public string PR_ANS_REC { get; set; }
        public string PR_FLAG { get; set; }
        public string PR_ADD4 { get; set; }
    }
}
