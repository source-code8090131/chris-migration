﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace CHRIS.Data
{
    public partial class CHRIS_RPT_SP_PY014_SUB1Result
    {
        public string NAME { get; set; }
        public DateTime? PR_EFFECTIVE_DATE { get; set; }
        public int? PR_CONTRIB { get; set; }
        public string SP_GROUP_D { get; set; }
        public string PR_DEPT { get; set; }
        public decimal? pr_p_no { get; set; }
        public int groupname { get; set; }
    }
}
