﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
#nullable disable
using System;
using System.Collections.Generic;

namespace CHRIS.Data;

public partial class RC
{
    public string RC_SEG { get; set; }

    public string RC_GROUP { get; set; }

    public string RC_DEPT { get; set; }

    public string RC_BRANCH { get; set; }

    public string RC1 { get; set; }

    public int ID { get; set; }
}