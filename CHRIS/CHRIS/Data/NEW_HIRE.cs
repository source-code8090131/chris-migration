﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
#nullable disable
using System;
using System.Collections.Generic;

namespace CHRIS.Data;

public partial class NEW_HIRE
{
    public string BNP_BRANCH { get; set; }

    public string BNP_SEGMENT { get; set; }

    public string BNP_GROUP { get; set; }

    public string BNP_DEPT { get; set; }

    public decimal? BNP_NO_EMP { get; set; }

    public string BNP_DESIG { get; set; }

    public string BNP_LEVEL { get; set; }

    public DateTime? BNP_DATE { get; set; }

    public int ID { get; set; }
}