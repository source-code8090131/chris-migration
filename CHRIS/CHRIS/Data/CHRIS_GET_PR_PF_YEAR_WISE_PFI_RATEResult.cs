﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace CHRIS.Data
{
    public partial class CHRIS_GET_PR_PF_YEAR_WISE_PFI_RATEResult
    {
        public int? YEAR { get; set; }
        public DateTime? PFI_YEAR { get; set; }
        public decimal? PFI_RATE { get; set; }
        public decimal? PFI_RES_RATE { get; set; }
        public int ID { get; set; }
    }
}
