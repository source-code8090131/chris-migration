﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace CHRIS.Data
{
    public partial class CHRIS_RPT_SP_GLI04Result
    {
        public string CPERSON { get; set; }
        public string ADD1 { get; set; }
        public string ADD2 { get; set; }
        public string ADD3 { get; set; }
        public string ADD_4 { get; set; }
        public string CTT_OFF { get; set; }
        public string PRE_SUBJECT { get; set; }
        public DateTime? D1 { get; set; }
        public string PRE_MC_NO { get; set; }
        public decimal? PRE_PREMIUM_AMT { get; set; }
        public decimal? MILL { get; set; }
        public decimal THOU { get; set; }
        public decimal ONES { get; set; }
        public decimal PAISA { get; set; }
        public string amt11 { get; set; }
    }
}
