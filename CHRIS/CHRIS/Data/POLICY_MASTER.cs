﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
#nullable disable
using System;
using System.Collections.Generic;

namespace CHRIS.Data;

public partial class POLICY_MASTER
{
    public string POL_POLICY_NO { get; set; }

    public string POL_BRANCH { get; set; }

    public string POL_SEGMENT { get; set; }

    public string POL_POLICY_TYPE { get; set; }

    public string POL_CTT_OFF { get; set; }

    public DateTime? POL_FROM { get; set; }

    public DateTime? POL_TO { get; set; }

    public string POL_CONTACT_PER { get; set; }

    public string POL_COMP_NAME1 { get; set; }

    public string POL_COMP_NAME2 { get; set; }

    public string POL_ADD1 { get; set; }

    public string POL_ADD2 { get; set; }

    public string POL_ADD3 { get; set; }

    public string POL_ADD4 { get; set; }

    public decimal? POL_COV_MULTI { get; set; }

    public decimal? POL_MAX_LIMIT { get; set; }

    public decimal? POL_MAX_ACC { get; set; }

    public decimal? POL_CTT_COV { get; set; }

    public decimal? POL_CTT_ACC { get; set; }

    public decimal? POL_AILMENT { get; set; }

    public decimal? POL_DELV_LIMIT { get; set; }

    public decimal? POL_ROOM_CHG { get; set; }

    public decimal? POL_OMI_LIMIT { get; set; }

    public decimal? POL_OMI_SELF { get; set; }

    public decimal? POL_PER_DEPEN { get; set; }

    public decimal? POL_NO_DEPEN { get; set; }

    public int ID { get; set; }
}