﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace CHRIS.Data
{
    public partial class CHRIS_GET_PR_INFO_FOR_SETTLEMENTResult
    {
        public decimal? PR_P_NO { get; set; }
        public string PR_BRANCH { get; set; }
        public string PR_DESIG { get; set; }
        public string PR_DEPT { get; set; }
        public string PR_FUNC_TITTLE1 { get; set; }
        public string PR_FUNC_TITTLE2 { get; set; }
        public DateTime? PR_JOINING_DATE { get; set; }
        public decimal? PR_ANNUAL_PACK { get; set; }
        public string PR_TERMIN_TYPE { get; set; }
        public DateTime? PR_TERMIN_DATE { get; set; }
        public string PR_REASONS { get; set; }
        public string PR_RESIG_RECV { get; set; }
        public decimal? GR_LIAB { get; set; }
    }
}
