﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace CHRIS.Data
{
    public partial class CHRIS_RPT_SP_FNREP41Result
    {
        public string DESIG { get; set; }
        public string level { get; set; }
        public int? counter { get; set; }
        public decimal? hous { get; set; }
        public decimal? emer { get; set; }
        public decimal? staff { get; set; }
        public decimal? car { get; set; }
        public decimal? bal { get; set; }
        public string branch { get; set; }
        public string segment { get; set; }
        public DateTime? date { get; set; }
    }
}
