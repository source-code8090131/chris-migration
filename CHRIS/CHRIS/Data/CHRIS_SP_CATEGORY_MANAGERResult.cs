﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace CHRIS.Data
{
    public partial class CHRIS_SP_CATEGORY_MANAGERResult
    {
        [Column("@ID")]
        public int ID { get; set; }
        [Column("@SP_CAT_CODE")]
        public string SP_CAT_CODE { get; set; }
        [Column("@SP_DESC")]
        public string SP_DESC { get; set; }
    }
}
