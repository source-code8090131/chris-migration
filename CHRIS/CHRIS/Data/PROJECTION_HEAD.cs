﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
#nullable disable
using System;
using System.Collections.Generic;

namespace CHRIS.Data;

public partial class PROJECTION_HEAD
{
    public string BOH_BAH_CODE { get; set; }

    public string BOH_YEAR { get; set; }

    public string BOH_SEGMENT { get; set; }

    public string BOH_GROUP { get; set; }

    public string BOH_DEPT { get; set; }

    public string BOH_YM_FLAG { get; set; }

    public decimal? BOH_YAMT { get; set; }

    public decimal? BOH_AMT1 { get; set; }

    public decimal? BOH_AMT2 { get; set; }

    public decimal? BOH_AMT3 { get; set; }

    public decimal? BOH_AMT4 { get; set; }

    public decimal? BOH_AMT5 { get; set; }

    public decimal? BOH_AMT6 { get; set; }

    public decimal? BOH_AMT7 { get; set; }

    public decimal? BOH_AMT8 { get; set; }

    public decimal? BOH_AMT9 { get; set; }

    public decimal? BOH_AMT10 { get; set; }

    public decimal? BOH_AMT11 { get; set; }

    public decimal? BOH_AMT12 { get; set; }

    public int ID { get; set; }
}