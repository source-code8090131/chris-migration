﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
#nullable disable
using System;
using System.Collections.Generic;

namespace CHRIS.Data;

public partial class LOGIN_HISTORY_DTL
{
    public string APP_CD { get; set; }

    public string PROG_NAME { get; set; }

    public double? USER_CD { get; set; }

    public DateTime LOGIN_DT { get; set; }

    public DateTime? LOGOUT_DT { get; set; }

    public string SOEID { get; set; }

    public int ID { get; set; }
}