﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace CHRIS.Data
{
    public partial class CHRIS_RPT_SP_FNREP21Result
    {
        public string FN_TYPE { get; set; }
        public string PR_DEPT { get; set; }
        public decimal? FN_BALANCE { get; set; }
        public int? NO { get; set; }
        public string CITY { get; set; }
        public string IOFLAG { get; set; }
        public DateTime? DATE { get; set; }
        public string SEGMENT { get; set; }
    }
}
