﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace CHRIS.Data
{
    public partial class CHRIS_SP_CONVERSION_TRANSFER_MANAGERResult
    {
        [Column("@ID")]
        public int ID { get; set; }
        [Column("@PR_TR_NO")]
        public decimal? PR_TR_NO { get; set; }
        [Column("@PR_TRANSFER_TYPE")]
        public decimal? PR_TRANSFER_TYPE { get; set; }
        [Column("@PR_FUNC_1")]
        public string PR_FUNC_1 { get; set; }
        [Column("@PR_FUNC_2")]
        public string PR_FUNC_2 { get; set; }
        [Column("@PR_NEW_BRANCH")]
        public string PR_NEW_BRANCH { get; set; }
        [Column("@PR_FURLOUGH")]
        public string PR_FURLOUGH { get; set; }
        [Column("@PR_DESG")]
        public string PR_DESG { get; set; }
        [Column("@PR_LEVEL")]
        public string PR_LEVEL { get; set; }
        [Column("@PR_COUNTRY")]
        public string PR_COUNTRY { get; set; }
        [Column("@PR_CITY")]
        public string PR_CITY { get; set; }
        [Column("@PR_DEPARTMENT_HC")]
        public string PR_DEPARTMENT_HC { get; set; }
        [Column("@PR_ASR_DOL")]
        public decimal? PR_ASR_DOL { get; set; }
        [Column("@PR_FAST_END_DATE")]
        public DateTime? PR_FAST_END_DATE { get; set; }
        [Column("@PR_IS_COORDINAT")]
        public string PR_IS_COORDINAT { get; set; }
        [Column("@PR_REMARKS")]
        public string PR_REMARKS { get; set; }
        [Column("@PR_FAST_CONVER")]
        public DateTime? PR_FAST_CONVER { get; set; }
        [Column("@PR_FLAG")]
        public string PR_FLAG { get; set; }
        [Column("@PR_EFFECTIVE")]
        public DateTime? PR_EFFECTIVE { get; set; }
        [Column("@PR_RENT")]
        public decimal? PR_RENT { get; set; }
        [Column("@PR_UTILITIES")]
        public decimal? PR_UTILITIES { get; set; }
        [Column("@PR_TAX_ON_TAX")]
        public decimal? PR_TAX_ON_TAX { get; set; }
        [Column("@PR_OLD_BRANCH")]
        public string PR_OLD_BRANCH { get; set; }
        [Column("@CITI_FLAG1")]
        public string CITI_FLAG1 { get; set; }
        [Column("@CITI_FLAG2")]
        public string CITI_FLAG2 { get; set; }
        [Column("@PR_NEW_ASR")]
        public decimal? PR_NEW_ASR { get; set; }
        [Column("@CURRENCY_TYPE")]
        public string CURRENCY_TYPE { get; set; }
    }
}
