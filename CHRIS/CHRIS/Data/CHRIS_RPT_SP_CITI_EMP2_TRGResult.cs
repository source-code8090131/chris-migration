﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace CHRIS.Data
{
    public partial class CHRIS_RPT_SP_CITI_EMP2_TRGResult
    {
        public decimal? TR_PR_NO { get; set; }
        public string TR_LOC_OVRS { get; set; }
        public string TR_PROG_NAME { get; set; }
        public DateTime? TR_PROG_DATE { get; set; }
        public string TR_FTR_PROG_NAME { get; set; }
        public DateTime? TR_FTR_PROG_DATE { get; set; }
        public string TR_COUNTRY { get; set; }
    }
}
