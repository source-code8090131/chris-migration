﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CHRIS.Data
{
    public class Manage_LOGIN_VIEW_CUSTOM
    {
        public int PLOG_ID { get; set; }
        public string PLOG_USER_ID { get; set; }
        public string PLOG_PASSWORD { get; set; }
        public string DOMAIN_NAME { get; set; }
    }
}