﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
#nullable disable
using System;
using System.Collections.Generic;

namespace CHRIS.Data;

public partial class HPAYROLL_DEDUC
{
    public decimal? PD_P_NO { get; set; }

    public DateTime? PD_PAY_DATE { get; set; }

    public string PD_DED_CODE { get; set; }

    public decimal? PD_DED_AMOUNT { get; set; }

    public string PD_ACCOUNT { get; set; }

    public int ID { get; set; }
}