﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace CHRIS.Data
{
    public partial class CHRIS_RPT_SP_ATR01_SUBResult
    {
        public int? F_Y { get; set; }
        public int? T_Y { get; set; }
        public int? AN_LEVEL { get; set; }
        public int? AN_CATEGORY { get; set; }
        public string AN_BRANCH { get; set; }
        public int? AN_P_NO { get; set; }
        public string C2 { get; set; }
        public string C3 { get; set; }
        public string C4 { get; set; }
        public string BRN_ADD2 { get; set; }
        public string BRN_ADD3 { get; set; }
        public string C5 { get; set; }
        public string C6 { get; set; }
        public decimal? C7 { get; set; }
        public decimal? C8 { get; set; }
        public decimal? C9 { get; set; }
        public decimal? C10 { get; set; }
        public decimal? C11 { get; set; }
        public decimal? C12 { get; set; }
        public decimal? C13 { get; set; }
        public decimal? C14 { get; set; }
        public decimal? C15 { get; set; }
        public decimal? C16 { get; set; }
        public decimal? C17 { get; set; }
        public decimal? C18 { get; set; }
        public decimal? C19 { get; set; }
        public decimal? C20 { get; set; }
        public decimal? C21 { get; set; }
        public decimal? C22 { get; set; }
        public decimal? C23 { get; set; }
        public decimal? C24 { get; set; }
        public decimal? C25 { get; set; }
        public decimal? C26 { get; set; }
        public decimal? C27 { get; set; }
        public decimal? C28 { get; set; }
        public decimal? C29 { get; set; }
        public decimal? C30 { get; set; }
        public decimal? C31 { get; set; }
        public decimal? C32 { get; set; }
        public decimal? C33 { get; set; }
        public decimal? C34 { get; set; }
        public decimal? C35 { get; set; }
        public decimal? C36 { get; set; }
        public decimal? C37 { get; set; }
        public decimal? C38 { get; set; }
        public decimal? C39 { get; set; }
        public decimal? NEW39 { get; set; }
        public decimal? C40 { get; set; }
        public decimal? C41 { get; set; }
        public decimal? C42 { get; set; }
        public decimal? C43 { get; set; }
        public string FLAG_TYPE { get; set; }
        public string S_SIG { get; set; }
        public string DESIGNATION { get; set; }
        public int? AN_FLAG { get; set; }
        public int? PAGE { get; set; }
        public int? MAXPAGE { get; set; }
        public int? SERIAL { get; set; }
        public decimal? sumC39 { get; set; }
        public decimal? sumC40 { get; set; }
    }
}
