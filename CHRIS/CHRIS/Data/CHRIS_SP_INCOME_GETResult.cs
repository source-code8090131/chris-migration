﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace CHRIS.Data
{
    public partial class CHRIS_SP_INCOME_GETResult
    {
        [Column("@ID")]
        public int ID { get; set; }
        [Column("@SP_AMT_FROM")]
        public decimal? SP_AMT_FROM { get; set; }
        [Column("@SP_AMT_TO")]
        public decimal? SP_AMT_TO { get; set; }
        [Column("@SP_REBATE")]
        public decimal? SP_REBATE { get; set; }
        [Column("@SP_PERCEN")]
        public decimal? SP_PERCEN { get; set; }
        [Column("@SP_TAX_AMT")]
        public decimal? SP_TAX_AMT { get; set; }
        [Column("@SP_SURCHARGE")]
        public decimal? SP_SURCHARGE { get; set; }
        [Column("@SP_FREBATE")]
        public decimal? SP_FREBATE { get; set; }
        [Column("@SP_FPERCEN")]
        public decimal? SP_FPERCEN { get; set; }
        [Column("@SP_FTAX_AMT")]
        public decimal? SP_FTAX_AMT { get; set; }
        [Column("@SP_FSURCHARGE")]
        public decimal? SP_FSURCHARGE { get; set; }
        [Column("@SP_ADD_TAX")]
        public decimal? SP_ADD_TAX { get; set; }
        [Column("@SP_ADD_TAX_AMT")]
        public decimal? SP_ADD_TAX_AMT { get; set; }
        [Column("@TAX_RED_PER")]
        public decimal? TAX_RED_PER { get; set; }
        [Column("@FTAX_RED_PER")]
        public decimal? FTAX_RED_PER { get; set; }
    }
}
