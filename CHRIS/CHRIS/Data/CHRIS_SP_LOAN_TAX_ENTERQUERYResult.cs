﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace CHRIS.Data
{
    public partial class CHRIS_SP_LOAN_TAX_ENTERQUERYResult
    {
        public int ID { get; set; }
        public DateTime? TAX_YEAR_FROM { get; set; }
        public DateTime? TAX_YEAR_TO { get; set; }
        public decimal? TAX_PER { get; set; }
    }
}
