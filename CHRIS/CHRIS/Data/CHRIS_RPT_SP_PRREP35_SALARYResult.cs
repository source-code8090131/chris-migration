﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace CHRIS.Data
{
    public partial class CHRIS_RPT_SP_PRREP35_SALARYResult
    {
        public DateTime? pr_effective { get; set; }
        public decimal? PR_ANNUAL_PREVIOUS { get; set; }
        public decimal? PR_INC_PER { get; set; }
        public decimal? PR_INC_AMT { get; set; }
        public decimal? PR_ANNUAL_PRESENT { get; set; }
        public decimal? PR_IN_NO { get; set; }
    }
}
