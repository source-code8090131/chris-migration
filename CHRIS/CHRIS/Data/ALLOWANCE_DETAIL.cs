﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
#nullable disable
using System;
using System.Collections.Generic;

namespace CHRIS.Data;

public partial class ALLOWANCE_DETAIL
{
    public string SP_ALLOW_CODE { get; set; }

    public decimal? SP_P_NO { get; set; }

    public string SP_REMARKS { get; set; }

    public decimal? SP_ALL_AMT { get; set; }

    public int ID { get; set; }
}