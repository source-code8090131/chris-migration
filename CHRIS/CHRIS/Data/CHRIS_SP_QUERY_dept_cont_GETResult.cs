﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace CHRIS.Data
{
    public partial class CHRIS_SP_QUERY_dept_cont_GETResult
    {
        [Column("@ID")]
        public int ID { get; set; }
        [Column("@PR_D_NO")]
        public decimal? PR_D_NO { get; set; }
        [Column("@PR_SEGMENT")]
        public string PR_SEGMENT { get; set; }
        [Column("@PR_DEPT")]
        public string PR_DEPT { get; set; }
        [Column("@PR_CONTRIB")]
        public decimal? PR_CONTRIB { get; set; }
        [Column("@PR_MENU_OPTION")]
        public decimal? PR_MENU_OPTION { get; set; }
        [Column("@PR_TYPE")]
        public string PR_TYPE { get; set; }
        [Column("@PR_EFFECTIVE_DATE")]
        public DateTime? PR_EFFECTIVE_DATE { get; set; }
    }
}
