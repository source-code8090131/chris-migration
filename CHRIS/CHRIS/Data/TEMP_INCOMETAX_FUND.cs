﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
#nullable disable
using System;
using System.Collections.Generic;

namespace CHRIS.Data;

public partial class TEMP_INCOMETAX_FUND
{
    public int PKID { get; set; }

    public decimal? HZR_PA_P_NO { get; set; }

    public decimal? HouseFinance { get; set; }

    public decimal? ZakatFinance { get; set; }

    public decimal? InvestFinance { get; set; }

    public decimal? Rebate { get; set; }

    public DateTime? FinDate { get; set; }

    public decimal? FinFields_1 { get; set; }

    public string FinFields_Text_1 { get; set; }
}