﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
#nullable disable
using System;
using System.Collections.Generic;

namespace CHRIS.Data;

public partial class SHIFT_TAB
{
    public decimal? SH_P_NO { get; set; }

    public DateTime? SH_DATE { get; set; }

    public string SH_SHIFT { get; set; }

    public decimal? SH_AMOUNT { get; set; }

    public int ID { get; set; }
}