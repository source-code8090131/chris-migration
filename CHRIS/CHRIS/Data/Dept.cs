﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
#nullable disable
using System;
using System.Collections.Generic;

namespace CHRIS.Data;

public partial class DEPT
{
    public string SP_SEGMENT_D { get; set; }

    public string SP_GROUP_D { get; set; }

    public string SP_DEPT { get; set; }

    public string SP_DESC_D1 { get; set; }

    public string SP_DESC_D2 { get; set; }

    public string SP_DEPT_HEAD { get; set; }

    public DateTime? SP_DATE_FROM_D { get; set; }

    public DateTime? SP_DATE_TO_D { get; set; }

    public string RC { get; set; }

    public string SP_RC_APA { get; set; }

    public int ID { get; set; }
}