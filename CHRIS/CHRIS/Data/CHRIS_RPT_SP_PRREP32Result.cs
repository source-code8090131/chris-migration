﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace CHRIS.Data
{
    public partial class CHRIS_RPT_SP_PRREP32Result
    {
        public decimal? pr_p_no { get; set; }
        public string name { get; set; }
        public decimal? pr_e_no { get; set; }
        public string pr_degree { get; set; }
        public string pr_collage { get; set; }
        public decimal? pr_e_year { get; set; }
    }
}
