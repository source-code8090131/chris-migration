﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace CHRIS.Data
{
    public partial class CHRIS_RPT_SP_PRREP06Result
    {
        public string PR_SEGMENT { get; set; }
        public string PR_LEVEL { get; set; }
        public string SP_GROUP_D { get; set; }
        public string PR_NEW_BRANCH { get; set; }
        public string PR_DEPT { get; set; }
        public decimal? PR_P_NO { get; set; }
        public string PR_FIRST_NAME { get; set; }
        public string PR_DESIG { get; set; }
        public DateTime? PR_JOINING_DATE { get; set; }
        public DateTime? PR_CONFIRM { get; set; }
        public decimal? FTE { get; set; }
    }
}
