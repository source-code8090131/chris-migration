﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace CHRIS.Data
{
    public partial class CHRIS_RPT_SP_PY011_SUB4Result
    {
        public decimal? P_NO { get; set; }
        public string PR_DEPT { get; set; }
        public decimal? PR_CONTRIB { get; set; }
    }
}
