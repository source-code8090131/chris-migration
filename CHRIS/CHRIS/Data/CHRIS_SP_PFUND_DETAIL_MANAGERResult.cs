﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace CHRIS.Data
{
    public partial class CHRIS_SP_PFUND_DETAIL_MANAGERResult
    {
        [Column("@ID")]
        public int ID { get; set; }
        [Column("@PFD_PR_P_NO")]
        public decimal? PFD_PR_P_NO { get; set; }
        [Column("@PFD_DATE")]
        public DateTime? PFD_DATE { get; set; }
        [Column("@PFD_PFUND")]
        public decimal? PFD_PFUND { get; set; }
        [Column("@PFD_PF_AREAR")]
        public decimal? PFD_PF_AREAR { get; set; }
        [Column("@PFD_FLAG")]
        public string PFD_FLAG { get; set; }
    }
}
