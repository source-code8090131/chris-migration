﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace CHRIS.Data
{
    public partial class CHRIS_SP_BRANCH_GETALLResult
    {
        public int ID { get; set; }
        public string BRN_CODE { get; set; }
        public string BRN_NAME { get; set; }
        public string BRN_ADD1 { get; set; }
        public string BRN_ADD2 { get; set; }
        public string BRN_ADD3 { get; set; }
        public string BRN_CR_AC { get; set; }
        public string BRN_DR_AC { get; set; }
    }
}
