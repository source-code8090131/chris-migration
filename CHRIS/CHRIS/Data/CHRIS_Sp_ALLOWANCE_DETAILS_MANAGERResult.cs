﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace CHRIS.Data
{
    public partial class CHRIS_Sp_ALLOWANCE_DETAILS_MANAGERResult
    {
        [Column("@ID")]
        public int ID { get; set; }
        [Column("@SP_ALL_CODE")]
        public string SP_ALL_CODE { get; set; }
        [Column("@SP_P_NO")]
        public decimal? SP_P_NO { get; set; }
        [Column("@SP_REMARKS")]
        public string SP_REMARKS { get; set; }
        [Column("@SP_ALL_AMT")]
        public decimal? SP_ALL_AMT { get; set; }
    }
}
