﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace CHRIS.Data
{
    public partial class CHRIS_Rpt_SP_PRREP53Result
    {
        public string BRANCH { get; set; }
        [Column("GEID #")]
        public decimal? GEID { get; set; }
        public string NAME { get; set; }
        public string GROUP { get; set; }
        public string SEGMENT { get; set; }
        public string DEPT { get; set; }
        [Column("P. NO")]
        public decimal? PNO { get; set; }
        public string DESIGNATION { get; set; }
        public string LEVEL { get; set; }
        public DateTime? DOB { get; set; }
        [Column("JOINING DATE")]
        public DateTime? JOININGDATE { get; set; }
        [Column("IN GRADE SINCE")]
        public DateTime? INGRADESINCE { get; set; }
        [Column("CURRENT SALARY")]
        public decimal? CURRENTSALARY { get; set; }
        [Column("CNIC #")]
        public string CNIC { get; set; }
        [Column("OLD NIC #")]
        public string OLDNIC { get; set; }
        [Column("ACCOUNT NO")]
        public string ACCOUNTNO { get; set; }
        [Column("NTN #")]
        public string NTN { get; set; }
        public string GENDER { get; set; }
    }
}
