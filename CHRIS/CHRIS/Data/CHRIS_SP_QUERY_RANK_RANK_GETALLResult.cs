﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace CHRIS.Data
{
    public partial class CHRIS_SP_QUERY_RANK_RANK_GETALLResult
    {
        public int ID { get; set; }
        public decimal? PR_P_NO { get; set; }
        public decimal? R_QUARTER { get; set; }
        public string R_RANK { get; set; }
        public decimal? YEAR { get; set; }
    }
}
