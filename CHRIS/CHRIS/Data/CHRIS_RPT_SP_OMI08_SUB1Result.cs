﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace CHRIS.Data
{
    public partial class CHRIS_RPT_SP_OMI08_SUB1Result
    {
        public int? POL_POLICY_NO { get; set; }
        public DateTime? PRE_PAYMENT_DATE { get; set; }
        public decimal? PRE_PREMIUM_AMT { get; set; }
    }
}
