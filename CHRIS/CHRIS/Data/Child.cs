﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
#nullable disable
using System;
using System.Collections.Generic;

namespace CHRIS.Data;

public partial class Child
{
    public decimal? PR_C_NO { get; set; }

    public string PR_CHILD_NAME { get; set; }

    public DateTime? PR_DATE_BIRTH { get; set; }

    public string PR_CH_SEX { get; set; }

    public int ID { get; set; }
}