﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace CHRIS.Data
{
    public partial class CHRIS_SP_BENCHMARK_MANAGERResult
    {
        public string BEN_FIN_TYPE { get; set; }
        public string w_description { get; set; }
        public decimal? BEN_RATE { get; set; }
        public DateTime? BEN_DATE { get; set; }
        public int ID { get; set; }
    }
}
