﻿
using System.Data;

namespace CHRIS.Common
{
    public class CommonDataManager : DataManager
    {
        /// <summary>
        /// Obsolete. SetSPName
        /// </summary>
        /// <param name="oprtnType"></param>
        protected internal override void SetSPName(OperationType oprtnType)
        {
            string spName = "";

            //switch (oprtnType)
            //{
            //    //case OperationType.Add:
            //    //    spName = StoredProcedureList.ACCOUNT_ADD;
            //    //    break;
            //    //case OperationType.Update:
            //    //    spName = StoredProcedureList.ACCOUNT_UPDATE;
            //    //    break;
            //    //case OperationType.Delete:
            //    //    spName = StoredProcedureList.ACCOUNT_DELETE;
            //    //    break;
            //    //case OperationType.Get:
            //    //    spName = StoredProcedureList.ACCOUNT_GET;
            //    //    break;
            //    //case OperationType.GetAll:
            //    //    spName = StoredProcedureList.ACCOUNT_GET_ALL;
            //    //    break;
            //    //case OperationType.GetByCode:
            //    //    spName = StoredProcedureList.ACCOUNT_GETBYCODE;
            //    //    break;
            //    //case OperationType.Duplicate:
            //    //    spName = StoredProcedureList.ACCOUNT_ISDUPLICATE;
            //    //    break;
            //    //case OperationType.NameDuplicate:
            //    //    spName = StoredProcedureList.ACCOUNT_ISNAMEDUPLICATE;
            //    //    break;

            //}

            this.spName = StoredProcedureList.SP_PREFIX + spName;
        }

        // Added by SL
        /// <summary>
        /// LoadReportParametersSQL
        /// </summary>
        /// <param name="dataTable"></param>
        /// <returns></returns>
        //public DataSet LoadReportParametersSQL(DataTable dataTable)
        //{
        //    Result m_result = new Result();
        //    m_result = ReportSQLManager.SelectDataTable(dataTable);
        //    return m_result.dstResult;
        //}

        // Added by SL
        /// <summary>
        /// GetLOVKeyCode
        /// </summary>
        /// <param name="pstrSPName"></param>
        /// <param name="pstrLOVType"></param>
        /// <returns></returns>
        public Result GetLOVKeyCode(string pstrSPName, string pstrLOVType)
        {
            DataSet dst = new DataSet();
            DataRow drw = null;

            this.spName = pstrSPName;
            dst.Tables.Add(SQLManager.GetSPParams(this.spName));

            drw = dst.Tables[this.spName].NewRow();

            if (drw.Table.Columns.Contains("LOVCodeType"))
                drw["LOVCodeType"] = pstrLOVType;

            dst.Tables[this.spName].Rows.Add(drw);
            dst.Tables[this.spName].AcceptChanges();

            return SQLManager.SelectDataSet(dst);
        }

        // Added by SL
        /// <summary>
        /// GetLOVEntityCode
        /// </summary>
        /// <param name="pstrEntityName"></param>
        /// <param name="pstrSPName"></param>
        /// <param name="pstrActionType"></param>
        /// <returns></returns>
        //public Result GetLOVEntityCode(string pstrEntityName, string pstrSPName, string pstrActionType)
        //{
        //    DataSet dst = new DataSet();
        //    BusinessEntity oEntity = RuntimeClassLoader.GetBusinessEntity(pstrEntityName);

        //    this.spName = pstrSPName;
        //    dst.Tables.Add(SQLManager.GetSPParams(this.spName));

        //    base.PopulateDataTableFromObject(oEntity, dst.Tables[this.spName], pstrActionType);

        //    //return SQLManager.ExecuteDML(dst);

        //    //drw = dst.Tables[this.spName].NewRow();

        //    //if (drw.Table.Columns.Contains("ActionType"))
        //    //    drw["ActionType"] = pstrActionType;

        //    return SQLManager.SelectDataSet(dst);
        //}

        /// <summary>
        /// PopulateDataTableFromObjectForSelect
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="dtl"></param>
        protected internal override void PopulateDataTableFromObjectForSelect(IEntityCommand entity, DataTable dtl)
        {
            throw new Exception("The method or operation is not implemented.");
        }
    }

    public class StoredProcedureList
    {
        public const string SP_PREFIX = "CIB_SP_";
    }

}
