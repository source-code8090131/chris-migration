using System;
using System.Collections.Generic;
using System.Text;
using CHRIS.Common;
using iCORE.Common;

/*****************************************************************
  Class Name:  ExceptionLogCommand 
  Class Description: <A brief summary of what is the purpose of the class>
  Created By: Shamraiz Ahmad
  Created Date: 22-08-2007
  Version No: 1.0
  Modification: <Mention the modification in the class >               
  Modified By:
  Modification Date:
  Version No: <The version No after modification>

*****************************************************************/

namespace iCORE.Common
{
    public class ExceptionLogCommand : IEntityCommand
    {
        private string _ClassName;
        private string _FunctionName;
        private Exception _Exception;
        private string _UserID;


        public string ClassName
        {
            set
            {
                this._ClassName = value;
            }
            get
            {
                return this._ClassName;
            }
        }

        public string FunctionName
        {
            set
            {
                this._FunctionName = value;
            }
            get
            {
                return this._FunctionName;
            }
        }

        public Exception Exception
        {
            set
            {
                this._Exception = value;
            }
            get
            {
                return this._Exception;
            }
        }

        public string UserID
        {
            set
            {
                this._UserID = value;
            }
            get
            {
                return this._UserID;
            }
        }
    }
}
