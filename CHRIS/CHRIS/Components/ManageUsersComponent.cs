﻿using DocumentFormat.OpenXml.InkML;
using CHRIS.Data;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using DocumentFormat.OpenXml.InkML;
using CHRIS.Models;
using CHRIS.Data;

namespace CHRIS.Components
{
    [ViewComponent]
    public class ManageUsersViewComponent : ViewComponent
    {
        CHRIS_Context context = new CHRIS_Context();

        public async Task<List<CHRIS_USER_WITH_ROLES_LIST_VIEW>> AddUserView()
        {
            List<CHRIS_USER_WITH_ROLES_LIST_VIEW> ViewData = new List<CHRIS_USER_WITH_ROLES_LIST_VIEW>();
            if (HttpContext.Session.GetString("USER_ID") != null)
            {
                if (DAL.CheckFunctionValidity("Manage_Users", "AddUser", HttpContext.Session.GetString("USER_ID").ToString()))
                {
                    string SessionUser = HttpContext.Session.GetString("USER_ID").ToString();
                    ViewData = context.CHRIS_USER_WITH_ROLES_LIST_VIEW.Where(m => m.PLOG_USER_ID != "Admin123" && m.PLOG_USER_ID != SessionUser && (m.PLOG_MAKER_ID != SessionUser || m.PLOG_ISAUTH == true)).OrderByDescending(m => m.PLOG_ID).ToList();
                }
            }

            return ViewData;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var items = await AddUserView();
            return View(items);
        }
    }
}
