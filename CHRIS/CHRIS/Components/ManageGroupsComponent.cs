﻿using DocumentFormat.OpenXml.InkML;
using CHRIS.Data;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using DocumentFormat.OpenXml.InkML;
using CHRIS.Controllers;
using CHRIS.Data;
using CHRIS.Models;
using System.Net.Mail;
using System.Runtime.CompilerServices;
using CHRIS.Data;

namespace Export.Components
{
    [ViewComponent]
    public class ManageGroupsViewComponent : ViewComponent
    {
        CHRIS_Context context = new CHRIS_Context();

        public async Task<List<CHRIS_ROLE_WITH_FUNCTIONS_LIST_VIEW>> Grid_View()
        {
            List<CHRIS_ROLE_WITH_FUNCTIONS_LIST_VIEW> ViewData = new List<CHRIS_ROLE_WITH_FUNCTIONS_LIST_VIEW>();

            if (HttpContext.Session.GetString("USER_ID") != null)
            {
                string SessionUser = HttpContext.Session.GetString("USER_ID").ToString();
                if (SessionUser == "Admin123")
                    ViewData = context.CHRIS_ROLE_WITH_FUNCTIONS_LIST_VIEW.Where(m => m.PR_MAKER_ID != SessionUser || m.PR_ISAUTH == true).OrderByDescending(m => m.PR_ID).ToList();
                else
                    ViewData = context.CHRIS_ROLE_WITH_FUNCTIONS_LIST_VIEW.Where(m => m.PR_NAME != "ADMIN" && (m.PR_MAKER_ID != SessionUser || m.PR_ISAUTH == true)).OrderByDescending(m => m.PR_ID).ToList();

            }

            return ViewData;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var items = await Grid_View();
            return View(items);
        }
    }
}
