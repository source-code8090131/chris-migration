﻿using DocumentFormat.OpenXml.InkML;
using CHRIS.Data;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using DocumentFormat.OpenXml.InkML;
using CHRIS.Controllers;
using CHRIS.Data;
using CHRIS.Models;
using System.Net.Mail;
using System.Runtime.CompilerServices;

namespace CHRIS.Components
{
    [ViewComponent]
    public class MenuViewComponent : ViewComponent
    {
        CHRIS_Context context = new CHRIS_Context();
        public async Task<List<CHRIS_FUNCTIONS>> Menu()
        {
            List<CHRIS_FUNCTIONS> menu = new List<CHRIS_FUNCTIONS>();

            if (HttpContext.Session.GetString("USER_ID") != null)
            {
                string UserID = HttpContext.Session.GetString("USER_ID");
                int?[] RoleIds = context.CHRIS_ASSIGN_LOGIN_RIGHTS.Where(m => m.PALR_USER_ID == UserID && m.PALR_STATUS == true).Select(m => m.PALR_ROLE_ID).ToArray();
                RoleIds = context.CHRIS_ROLES.Where(m => RoleIds.Contains(m.PR_ID) && m.PR_STATUS == "true" && m.PR_ISAUTH == true).Select(m => (int?)m.PR_ID).ToArray();
                int?[] FunctionIds = context.CHRIS_ASSIGN_FUNCTIONS.Where(m => RoleIds.Contains(m.PAF_ROLE_ID) && m.PAF_STATUS == true).Select(m => m.PAF_FUNCTION_ID).Distinct().ToArray();
                menu = context.CHRIS_FUNCTIONS.Where(m => FunctionIds.Contains(m.PF_ID)).Distinct().ToList();
            }

            return menu;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var items = await Menu();
            return View(items);
        }
    }
}
