﻿//#region Custom Template Js 
$(window).on('load', function () {
    $('#loading').hide();
});

//$(window).on('load', function () {
//    $('#loading1').hide();
//});

$('#FormType').each(function () {
    var CheckFormType = $(this).attr('value');
    if (CheckFormType == "Edit") {
        $('#Entity_PLOG_USER_ID').attr('readonly', true);
    }
    else {
        $('#Entity_PLOG_USER_ID').attr('readonly', false);
    }
});

$(function () {
    $("#example1").DataTable({
        "responsive": true, "lengthChange": false, "autoWidth": false, "order": false
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    $("#example2").DataTable({
        "responsive": true, "lengthChange": false, "autoWidth": false, "order": false
    }).buttons().container().appendTo('#example2_wrapper .col-md-6:eq(0)');

    $("#Reporting").DataTable({
        "responsive": true, "lengthChange": false, "autoWidth": false, "order": false,
        "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    }).buttons().container().appendTo('#Reporting_wrapper .col-md-6:eq(0)');
    //Initialize Select2 Elements
    $('.select2').select2()
    //Initialize Select2 Elements
    $('.select2bs4').select2({
        theme: 'bootstrap4'
    })
});

//#endregion

//#region Custom Date Picker
$(function () {
    $(".CustomDatePicker").datepicker();
});
//#endregion

//#region Group Role Authorize/Reject
$("[name='AuthroizeRole']").click(function (button) {
    var R_ID = $(this).attr("value");
    $.ajax
        ({
            url: '/Manage_Groups/AuthorizeGroupRole?R_ID=' + R_ID,
            type: 'POST',
            datatype: 'application/json',
            contentType: 'application/json',
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (result) {
                alert(result);
                location.reload();
            },
            error: function () {
                //alert("Something went wrong..");
            },
        });
});
$("[name='DeleteFromAuthroizeRole']").click(function (button) {
    var R_ID = $(this).attr("value");
    var check = confirm("Are You Sure your want to delete?");
    if (check == true) {
        $.ajax
            ({
                url: '/Manage_Groups/RejectAuthorizeGroupRole?R_ID=' + R_ID,
                type: 'POST',
                datatype: 'application/json',
                contentType: 'application/json',
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (result) {
                    alert(result);
                    location.reload();
                },
                error: function () {
                    //alert("Something went wrong..");
                },
            });
    }
});
//#endregion

//#region User Authorize/Reject
$("[name='AuthroizeUser']").click(function (button) {
    var LOG_ID = $(this).attr("value");
    $.ajax
        ({
            url: '/Manage_Users/AuthroizeUser?LOG_ID=' + LOG_ID,
            type: 'POST',
            datatype: 'application/json',
            contentType: 'application/json',
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (result) {
                alert(result);
                location.reload();
            },
            error: function () {
                //alert("Something went wrong..");
            },
        });
});
$("[name='DeleteFromAuthroizeUser']").click(function (button) {
    var LOG_ID = $(this).attr("value");
    var check = confirm("Are You Sure your want to delete?");
    if (check == true) {
        $.ajax
            ({
                url: '/Manage_Users/DeleteFromAuthroizeUser?LOG_ID=' + LOG_ID,
                type: 'POST',
                datatype: 'application/json',
                contentType: 'application/json',
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (result) {
                    alert(result);
                    location.reload();
                },
                error: function () {
                    //alert("Something went wrong..");
                },
            });
    }
});
    //#endregion

//#region Browser Back Button
$("[name='BrowserBack']").click(function (button) {
    history.back();
});
//#endregion

//#region Password Filed Show on Hold
$("#passtoggle").on('mousedown', function () {
    $("#GCARParams_PassHash").attr('type','text');
});
$("#passtoggle").on('mouseup', function () {
    $("#GCARParams_PassHash").attr('type', 'password');
});

$("#showhidepass").on('mousedown', function () {
    $("#SettingParms_PSS_GCAR_CERT_HASH").attr('type', 'text');
});
$("#showhidepass").on('mouseup', function () {
    $("#SettingParms_PSS_GCAR_CERT_HASH").attr('type', 'password');
});
//#endregion

