﻿using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc;

namespace CHRIS.Filters
{
    public class LoginAuthentication : Attribute, IAuthorizationFilter
    {
        public void OnAuthorization(AuthorizationFilterContext context)
        {
            if (context.HttpContext.Session.GetString("USER_ID") == null)
            {
                HandleUnauthorizedRequest(context);
            }
        }

        protected void HandleUnauthorizedRequest(AuthorizationFilterContext context)
        {
            context.Result = new RedirectToRouteResult(
                new RouteValueDictionary
                {{ "controller", "Login"},
                { "action", "Index"},
                });
        }
    
    }
}
