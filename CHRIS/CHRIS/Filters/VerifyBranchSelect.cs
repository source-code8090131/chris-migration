﻿using CHRIS.Common;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Diagnostics;
using System.Security.Claims;

namespace CHRIS.Filters
{
    public class VerifyBranchSelect : Attribute, IAuthorizationFilter
    {
        public void OnAuthorization(AuthorizationFilterContext context)
        {
            if (context.HttpContext.Session.GetString(Constants.Config.branchCode) == null)
            {
                HandleUnauthorizedRequest(context);
            }
        }
        
        protected  void HandleUnauthorizedRequest(AuthorizationFilterContext context)
        {
            context.Result = new RedirectToRouteResult(
                new RouteValueDictionary
                {{ "controller", "Setup"},
                { "action", "BranchSelect"},
                });
        }

    }
}