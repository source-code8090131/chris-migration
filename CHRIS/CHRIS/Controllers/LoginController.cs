﻿using CHRIS.Data;
using CHRIS.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using System.Security.Claims;
using System.Text;

namespace CHRIS.Controllers
{
    public class LoginController : Controller
    {
        CHRIS_Context context = new CHRIS_Context();
        public string GetIP()
        {
            // Testing the Repository // 
            string Str = "";
            Str = System.Net.Dns.GetHostName();
            IPHostEntry ipEntry = System.Net.Dns.GetHostEntry(Str);
            IPAddress[] addr = ipEntry.AddressList;
            return addr[addr.Length - 1].ToString();
        }
        //[OutputCacheAttribute(VaryByParam = "*", Duration = 0, NoStore = true)]
        [ResponseCache(Duration = 0, NoStore = true, VaryByQueryKeys = new[] { "*" })]
        // GET: Login
        public ActionResult Index()
        {
            //Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));
            Response.Cookies.Append("ASP.NET_SessionId", "");
            return View();
        }
        [HttpPost]
        public ActionResult Index(Manage_LOGIN_VIEW_CUSTOM db_table)
        {
            string message = "";
            try
            {
                byte[] bytes = Encoding.ASCII.GetBytes(GetIP());
                HttpContext.Session.Set("IP", bytes);
                CHRIS_LOGIN login = new CHRIS_LOGIN();
                login = context.CHRIS_LOGIN.Where(m => m.PLOG_USER_ID == db_table.PLOG_USER_ID && m.PLOG_ISAUTH == true && m.PLOG_USER_STATUS == "Active").FirstOrDefault();
                if (login != null)
                {
                    CHRIS_LOGIN loginview = context.CHRIS_LOGIN.Where(m => m.PLOG_USER_ID == db_table.PLOG_USER_ID).FirstOrDefault();

                    //Session["IP"] = GetIP();
                    HttpContext.Session.Set("IP", bytes);
                    if (loginview != null)
                    {
                        CHRIS_USER_WITH_ROLES_LIST_VIEW CheckRoles = context.CHRIS_USER_WITH_ROLES_LIST_VIEW.Where(m => m.PLOG_USER_ID == login.PLOG_USER_ID).FirstOrDefault();
                        if (CheckRoles != null)
                        {
                            if (CheckRoles.ASSIGNED_ROLES != null)
                            {
                                if (!loginview.PLOG_USER_ID.ToLower().Contains("admin123") && !loginview.PLOG_USER_ID.ToLower().Contains("adminc"))
                                {
                                    string adPath = "";
                                    string adurl = "";
                                    if (db_table.DOMAIN_NAME == "EUR")
                                    {
                                        adPath = "LDAP://eur.nsroot.net"; //Fully-qualified Domain Name
                                        adurl = "eur.nsroot.net";
                                    }
                                    else if (db_table.DOMAIN_NAME == "APAC")
                                    {
                                        adPath = "LDAP://apac.nsroot.net"; //Fully-qualified Domain Name
                                        adurl = "apac.nsroot.net";
                                    }
                                    else if (db_table.DOMAIN_NAME == "NAM")
                                    {
                                        adPath = "LDAP://nam.nsroot.net"; //Fully-qualified Domain Name
                                        adurl = "nam.nsroot.net";
                                    }
                                    else
                                    {
                                        message = "Select the domain to continue.";
                                        return View();
                                    }
                                    LdapAuthentication adAuth = new LdapAuthentication(adPath);

                                    bool CheckAuth = adAuth.IsAuthenticated(adurl, db_table.PLOG_USER_ID, db_table.PLOG_PASSWORD);
                                    if (CheckAuth == true)
                                    {
                                        //bool isCookiePersistent = true;
                                        bool isCookiePersistent = User.Identity.IsAuthenticated;

                                        var claims = new List<Claim>
                                        {
                                            new Claim(ClaimTypes.Version,"1"),
                                            new Claim(ClaimTypes.Name,db_table.PLOG_USER_ID),
                                            new Claim(ClaimTypes.DateOfBirth,DateTime.Now.ToString()),
                                            new Claim(ClaimTypes.Expiration,DateTime.Now.AddMinutes(60).ToString()),
                                            new Claim(ClaimTypes.IsPersistent,isCookiePersistent.ToString()),
                                            new Claim(ClaimTypes.UserData,"null")
                                        };
                                        var claimIdentity = new ClaimsIdentity(claims, db_table.PLOG_USER_ID);
                                        var claimPrinciple = new ClaimsPrincipal(claimIdentity);
                                        var authenticationProperty = new AuthenticationProperties
                                        {
                                            IsPersistent = true
                                        };
                                        HttpContext.SignInAsync(claimPrinciple, authenticationProperty);

                                        //FormsAuthenticationTicket authTicket = new FormsAuthenticationTicket
                                        //    (1, db_table.PLOG_USER_ID, DateTime.Now, DateTime.Now.AddMinutes(60), isCookiePersistent, null);
                                        //string encryptedTicket = FormsAuthentication.Encrypt(authTicket);

                                        //HttpCookie authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);


                                        //if (true == isCookiePersistent)
                                        //    authCookie.Expires = authTicket.Expiration;
                                        //Response.Cookies.Add(authCookie);

                                        byte[] UserIdbytes = Encoding.ASCII.GetBytes(loginview.PLOG_USER_ID);
                                        HttpContext.Session.Set("USER_ID", UserIdbytes);
                                        byte[] UserNamebytes = Encoding.ASCII.GetBytes(loginview.PLOG_USER_NAME);
                                        HttpContext.Session.Set("USER_NAME", UserNamebytes);
                                        return RedirectToAction("Index", "Home");
                                    }
                                    else
                                    {
                                        message = "Authentication Failed from the Domain.";
                                    }
                                }
                                else
                                {
                                    //Validate Super User
                                    CHRIS_SYSTEM_SETUP systemuser = context.CHRIS_SYSTEM_SETUP.Where(x => x.USER_ID.ToLower() == login.PLOG_USER_ID.ToLower()).FirstOrDefault();
                                    if (systemuser != null)
                                    {
                                        if (login.PLOG_USER_ID == db_table.PLOG_USER_ID && GenerateHash(db_table.PLOG_PASSWORD) == systemuser.HASH && login.PLOG_ISAUTH == true)
                                        {
                                            //bool isCookiePersistent = true;// User.Identity.IsAuthenticated;
                                            bool isCookiePersistent = HttpContext.User.Identity.IsAuthenticated;

                                            var claims = new List<Claim>
                                        {
                                            new Claim(ClaimTypes.Version,"1"),
                                            new Claim(ClaimTypes.Name,db_table.PLOG_USER_ID),
                                            new Claim(ClaimTypes.DateOfBirth,DateTime.Now.ToString()),
                                            new Claim(ClaimTypes.Expiration,DateTime.Now.AddMinutes(60).ToString()),
                                            new Claim(ClaimTypes.IsPersistent,isCookiePersistent.ToString()),
                                            new Claim(ClaimTypes.UserData,"null")
                                        };
                                            var claimIdentity = new ClaimsIdentity(claims, db_table.PLOG_USER_ID);
                                            var claimPrinciple = new ClaimsPrincipal(claimIdentity);
                                            var authenticationProperty = new AuthenticationProperties
                                            {
                                                IsPersistent = true
                                            };
                                            HttpContext.SignInAsync(claimPrinciple, authenticationProperty);

                                            //Session["USER_ID"] = loginview.PLOG_USER_ID;
                                            //Session["USER_NAME"] = loginview.PLOG_USER_NAME;

                                            byte[] UserIdbytes = Encoding.ASCII.GetBytes(loginview.PLOG_USER_ID);
                                            HttpContext.Session.Set("USER_ID", UserIdbytes);
                                            byte[] UserNamebytes = Encoding.ASCII.GetBytes(loginview.PLOG_USER_NAME);
                                            HttpContext.Session.Set("USER_NAME", UserNamebytes);
                                            return RedirectToAction("Index", "Home");
                                        }
                                        else
                                        {
                                            if (GenerateHash(db_table.PLOG_PASSWORD) != systemuser.HASH)
                                                message = "Invalid password for user : " + login.PLOG_USER_ID;
                                            else if (login.PLOG_USER_ID != db_table.PLOG_USER_ID)
                                                message = "Invalid User Id";
                                            else if (login.PLOG_ISAUTH != true)
                                                message = "Unauthorized User : " + login.PLOG_USER_ID;
                                            return View();
                                        }
                                    }
                                    else
                                    {
                                        message = "Setup Infor is not provided for the user.";
                                        return View();
                                    }
                                }
                            }
                            else
                            {
                                message = "Login Was Successfull but No Rights Assign to the User";
                                return View();
                            }
                        }
                        else
                        {
                            message = "Login Was Successfull but No Rights Assign to the User";
                            return View();
                        }
                    }
                    else
                    {
                        message = "Invalid Email : System doesnt recognize your email";
                        return View();
                    }
                }
                else
                {
                    message = "Invalid Email : System doesnt recognize your email";
                    return View();
                }
            }
            catch (Exception ex)
            {
                message += "Exception Occured " + ex.Message + Environment.NewLine;
                if (ex.InnerException != null)
                {
                    message += "Inner Exception : " + ex.InnerException.Message.ToString() + Environment.NewLine;
                    if (ex.InnerException.InnerException != null)
                    {
                        message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                    }
                }
            }
            finally
            {
                ViewBag.message = message;
                ModelState.Clear();
            }
            return View();
        }
        public ActionResult Logout()
        {
            //if (Session["USER_ID"] != null)
            if (HttpContext.Session.GetString("USER_ID") != null)
            {
                HttpContext.Session.Clear();
            }
            HttpContext.SignOutAsync();
            //FormsAuthentication.SignOut();
            //Session.Clear();
            HttpContext.Session.Clear();
            //Response.Cache.SetCacheability(HttpCacheability.NoCache);
            HttpContext.Response.Headers["Cache-Control"] = "no-cache";
            //Response.ExpiresAbsolute = DateTime.UtcNow.AddDays(-1d);
            //Response.Expires = -1500;
            HttpContext.Response.Headers["Expires"] = DateTime.UtcNow.AddDays(-1d).ToString("R");
            //Response.CacheControl = "no-Cache";
            //Response.Cache.SetCacheability(HttpCacheability.NoCache);
            //Response.Cache.SetExpires(DateTime.UtcNow.AddHours(-1));
            //Response.Cache.SetNoStore();
            Response.Headers["Cache-Control"] = "no-store";

            //Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));
            Response.Cookies.Append("ASP.NET_SessionId", "");
            return RedirectToAction("Index", "Login");
        }

        public string GenerateHash(string password)
        {
            string saltStr = "95958745874441";
            byte[] salt = Encoding.ASCII.GetBytes(saltStr); //RandomNumberGenerator.GetBytes(128 / 8); // divide by 8 to convert bits to bytes
            //Console.WriteLine($"Salt: {Convert.ToBase64String(salt)}");

            // derive a 256-bit subkey (use HMACSHA256 with 100,000 iterations)
            string hashed = Convert.ToBase64String(KeyDerivation.Pbkdf2(
                password: password!,
                salt: salt,
                prf: KeyDerivationPrf.HMACSHA256,
                iterationCount: 100000,
                numBytesRequested: 256 / 8));

            return hashed;
        }
    }
}
