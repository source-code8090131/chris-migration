﻿using CHRIS.Common;
using CHRIS.Data;
using CHRIS.Models;
using CHRIS.Services;
using DocumentFormat.OpenXml.Office2010.Excel;
using DocumentFormat.OpenXml.Wordprocessing;
using Microsoft.AspNetCore.Mvc;
using Microsoft.DotNet.Scaffolding.Shared.Messaging;
using Newtonsoft.Json;
using System.Data;
using System.Data.SqlClient;

namespace CHRIS.Controllers
{
    public class PayrollController : Controller
    {

        #region IncentiveBonusEntry

        public IActionResult IncentiveBonusEntry()
        {
            return View();
        }
        public IActionResult btn_Delete_Modal()
        {
            Dictionary<string, object> d = new Dictionary<string, object>();

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_BONUS_TAB_MANAGER_EXPLICIT", "List", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);               
                return Json(CatResul);
            }
            return Json(null);
        }

        public IActionResult tbl_IncentiveBonusEntry()
        {
            Dictionary<string, object> d = new Dictionary<string, object>();

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_BONUS_TAB_MANAGER_EXPLICIT", "ListWithNames", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                CatResul = CatResul.Replace(" [Y/N]", "");
                return Json(CatResul);
            }
            return Json(null);
        }
        public IActionResult btn_PR_P_NO_Modal()
        {
            Dictionary<string, object> d = new Dictionary<string, object>();

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_PERSONNEL_MANAGER", "Pr_P_No_Lov", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        [HttpPost]
        public JsonResult UpsertIncentiveBonusEntry(decimal? BO_P_NO, DateTime? BO_10_D, DateTime? BO_INC_D, decimal? BO_INC_AMOUNT, string BO_HL_MARKUP, string BO_10C_BONUS, string BO_INC_APP, string BO_10C_APP, string BO_10_USE, string BO_INC_USE, int? ID)
        {
            //var user = HttpContext.Session.GetString("USER_ID");
              var Date_BO_INC_D = DateTime.Parse(DateTime.Now.ToShortDateString());
            // var TIME = DateTime.Now.TimeOfDay.Hours + "" + DateTime.Now.TimeOfDay.Minutes + "" + DateTime.Now.TimeOfDay.Seconds;

            try
            {
                Dictionary<string, object> d = new Dictionary<string, object>();

                d.Add("BO_P_NO", BO_P_NO); 
		        d.Add("BO_10_D", BO_10_D);
		        d.Add("BO_INC_D", Date_BO_INC_D);
		        d.Add("BO_INC_AMOUNT", BO_INC_AMOUNT);
		        d.Add("BO_HL_MARKUP", BO_HL_MARKUP);
		        d.Add("BO_10C_BONUS", BO_10C_BONUS);
		        d.Add("BO_INC_APP", BO_INC_APP);
		        d.Add("BO_10C_APP", BO_10C_APP);
		        d.Add("BO_10_USE", BO_10_USE);
                d.Add("BO_INC_USE", BO_INC_USE);
                d.Add("ID", ID);

                CmnDataManager objCmnDataManager = new CmnDataManager();
                
                    if (ID > 0)
                    {
                        Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_BONUS_TAB_MANAGER_EXPLICIT", "Update", d);
                        if (rsltCode.isSuccessful)
                        {
                            var res = Json(rsltCode);
                            return Json("Your request is modify successfully !");
                        }
                    }
                    else
                    {
                        Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_BONUS_TAB_MANAGER_EXPLICIT", "Save", d);
                        if (rsltCode.isSuccessful)
                        {
                            var res = Json(rsltCode);
                            return Json("Your request is successfully completed !");
                        }
                    }
               
                return Json(null);
            }
            catch (Exception ex)
            {
                return Json("Something went wrong");
            }
        }


        public ViewResponseModel DeleteIncentiveBonusEntry(IncentiveBonusEntryModel model)
        {
            return IncentiveBonusEntryService.getInstance().DeleteIncentiveBonusEntry(model);
        }

        #endregion

        #region ZakatEntry

        public IActionResult ZakatEntry()
        {
            return View();
        }
        
        public IActionResult tbl_ZakatEntry()
        {
            Dictionary<string, object> d = new Dictionary<string, object>();

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_ZAKAT_MANAGER", "PersonnelLov", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);                
                return Json(CatResul);
            }
            return Json(null);
        }

        public IActionResult CheckZakatEntry(decimal? PR_P_NO)
        {
            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("PR_P_NO", PR_P_NO);
            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_ZAKAT_MANAGER", "CheckZakatEntry", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        [HttpPost]
        public JsonResult UpsertZakatEntry(decimal? PR_P_NO, string PR_NAME,decimal? PR_ZAKAT_AMT,decimal? PR_REFUND_AMT, decimal? PR_REFUND_FOR1, decimal? PR_REFUND_FOR2 ,int? ID)
        {
           
            try
            {
                Dictionary<string, object> d = new Dictionary<string, object>();


               d.Add("PR_P_NO", PR_P_NO);
               d.Add("PR_NAME", PR_NAME);
               d.Add("PR_ZAKAT_AMT", PR_ZAKAT_AMT);
               d.Add("PR_REFUND_AMT", PR_REFUND_AMT);
               d.Add("PR_REFUND_FOR1", PR_REFUND_FOR1);
               d.Add("PR_REFUND_FOR2", PR_REFUND_FOR2);
                d.Add("ID", ID);

                CmnDataManager objCmnDataManager = new CmnDataManager();

                if (PR_P_NO > 0)
                {
                    Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_ZAKAT_MANAGER", "Update", d);
                    if (rsltCode.isSuccessful)
                    {
                        var res = Json(rsltCode);
                        return Json("Your request is modify successfully !");
                    }
                }
                else
                {
                    Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_ZAKAT_MANAGER", "Save", d);
                    if (rsltCode.isSuccessful)
                    {
                        var res = Json(rsltCode);
                        return Json("Your request is successfully completed !");
                    }
                }

                return Json(null);
            }
            catch (Exception ex)
            {
                return Json("Something went wrong");
            }
        }


        public IActionResult DeleteZakatEntry(decimal? PR_P_NO)
        {
            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("PR_P_NO", PR_P_NO);
            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_ZAKAT_MANAGER", "Delete", d);
            if (rsltCode.isSuccessful)
            {               
                return Json("Record delete successfully !");
            }
            return Json(null);
        }

        #endregion

        #region SalaryAdvanceEntry

       
        public IActionResult SalaryAdvanceEntry()
        {
            return View();
        }

        public IActionResult btn_SA_P_NO_Modal()
        {
            Dictionary<string, object> d = new Dictionary<string, object>();

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_Payroll_SalAdv_SAL_ADVANCE_MANAGER", "BLK_P_PR_P_NO_LOV0", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        public IActionResult ModifySalaryAdvanceEntry()
        {
            Dictionary<string, object> d = new Dictionary<string, object>();

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_Payroll_SalAdv_SAL_ADVANCE_MANAGER", "List", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }
        public IActionResult txtSA_P_NO_Validation(decimal? SA_P_NO, DateTime? SA_DATE)
        {
            try
            {
                Dictionary<string, object> dicAdvanceSalary = new Dictionary<string, object>();

                /*----------------------To store input parameters of StoredProcedure--------------------*/
                Dictionary<string, object> dicInputParameters = new Dictionary<string, object>();

                dicAdvanceSalary.Clear();

                //-----BLK_HEAD---------16 ITEMS
                dicAdvanceSalary.Add("BLK_HEAD_W_OPTION", null);
                dicAdvanceSalary.Add("BLK_HEAD_W_CNT", null);
                dicAdvanceSalary.Add("BLK_HEAD_W_CATEGORY", null);
                dicAdvanceSalary.Add("BLK_HEAD_W_VAL1", null);
                dicAdvanceSalary.Add("BLK_HEAD_W_VAL2", null);
                dicAdvanceSalary.Add("BLK_HEAD_W_BASIC", null);
                dicAdvanceSalary.Add("BLK_HEAD_W_SUM", null);
                dicAdvanceSalary.Add("BLK_HEAD_W_YEAR", null);
                dicAdvanceSalary.Add("BLK_HEAD_W_FLAG", null);
                dicAdvanceSalary.Add("BLK_HEAD_W_YSY", null);
                dicAdvanceSalary.Add("BLK_HEAD_W_ANSWER", null);
                dicAdvanceSalary.Add("BLK_HEAD_W_ANSWER3", null);
                dicAdvanceSalary.Add("BLK_HEAD_W_TOTAL", null);
                dicAdvanceSalary.Add("BLK_HEAD_W_DIS", null);
                dicAdvanceSalary.Add("BLK_HEAD_W_USER", null);
                dicAdvanceSalary.Add("BLK_HEAD_W_LOC", null);

                //-------BLK_P-----------2 ITEMS
                dicAdvanceSalary.Add("BLK_P_PR_P_NO", null);
                dicAdvanceSalary.Add("BLK_P_PR_FIRST_NAME", null);

                //-----SAL_ADVANCE---------5 ITEMS
                dicAdvanceSalary.Add("SAL_ADVANCE_SA_P_NO", null);
                dicAdvanceSalary.Add("SAL_ADVANCE_SA_DATE", null);
                dicAdvanceSalary.Add("SAL_ADVANCE_SA_FOR_THE_MONTH1", null);
                dicAdvanceSalary.Add("SAL_ADVANCE_SA_FOR_THE_MONTH2", null);
                dicAdvanceSalary.Add("SAL_ADVANCE_SA_ADV_AMOUNT", null);

                //------PER-----------2 ITEMS
                dicAdvanceSalary.Add("PER_PR_P_NO", null);
                dicAdvanceSalary.Add("PER_PR_FIRST_NAME", null);

                //-----SAL_ADVANCE1-------5 ITEMS
                dicAdvanceSalary.Add("SAL_ADVANCE1_SA_P_NO", null);
                dicAdvanceSalary.Add("SAL_ADVANCE1_W_NAME", null);
                dicAdvanceSalary.Add("SAL_ADVANCE1_SA_FOR_THE_MONTH1", null);
                dicAdvanceSalary.Add("SAL_ADVANCE1_SA_FOR_THE_MONTH2", null);
                dicAdvanceSalary.Add("SAL_ADVANCE1_SA_ADV_AMOUNT", null);

                Result rsltSalary = new Result();
                CmnDataManager objCmnDataManager = new CmnDataManager();

                dicAdvanceSalary["SAL_ADVANCE_SA_P_NO"] = SA_P_NO;

               
                    #region Insertion

                    //system date Time
                    dicAdvanceSalary["BLK_P_W_DATE"] = DateTime.Parse(DateTime.Now.ToShortDateString());// System.DateTime.Now.Date.ToString("dd/MM/yyyy");
                    SA_DATE = DateTime.Parse(DateTime.Now.ToShortDateString()); //System.DateTime.Now.Date.ToString("dd/MM/yyyy");

                    dicInputParameters.Clear();
                    dicInputParameters.Add("SA_P_NO", SA_P_NO);
                    rsltSalary = objCmnDataManager.GetData("CHRIS_SP_Payroll_SalAdv_SAL_ADVANCE_MANAGER", "Check_Insertion", dicInputParameters);

                    if (rsltSalary.isSuccessful)
                    {
                        if (rsltSalary.dstResult.Tables.Count > 0)
                        {
                            if (rsltSalary.dstResult.Tables[0].Rows.Count > 0)
                            {
                                dicAdvanceSalary["BLK_HEAD_W_VAL1"] = rsltSalary.dstResult.Tables[0].Rows[0]["SA_FOR_THE_MONTH1"].ToString() == "" ? "00" : rsltSalary.dstResult.Tables[0].Rows[0]["SA_FOR_THE_MONTH1"];
                                dicAdvanceSalary["BLK_HEAD_W_VAL2"] = rsltSalary.dstResult.Tables[0].Rows[0]["SA_FOR_THE_MONTH2"].ToString() == "" ? "00" : rsltSalary.dstResult.Tables[0].Rows[0]["SA_FOR_THE_MONTH2"];

                                /** If Advance Has Been Taken For Both The Months **/
                                if ((Convert.ToInt32(dicAdvanceSalary["BLK_HEAD_W_VAL1"]) != 00) && (Convert.ToInt32(dicAdvanceSalary["BLK_HEAD_W_VAL2"]) != 00))
                                {
                                   return Json("VALUE IS ALREADY THERE FOR THE CURRENT MONTH");                              
                                    
                                }
                                /** If Advance is Taken For Month 1 & Not Month 2 **/
                                else if ((Convert.ToInt32(dicAdvanceSalary["BLK_HEAD_W_VAL1"]) != 00) && (Convert.ToInt32(dicAdvanceSalary["BLK_HEAD_W_VAL2"]) == 00))
                                {
                                    try
                                    {                                    
                                        dicInputParameters.Clear();
                                        dicInputParameters.Add("SA_P_NO", SA_P_NO);

                                       
                                        Result rslt = objCmnDataManager.GetData("CHRIS_SP_Payroll_SalAdv_SAL_ADVANCE_MANAGER", "execute", dicInputParameters);

                                        if (rslt.isSuccessful)
                                        {
                                            if (rslt.dstResult.Tables.Count > 0)
                                            {
                                                if (rslt.dstResult.Tables[0].Rows.Count > 0)
                                                {
                                                    
                                                    DateTime dtSA_DATE = DateTime.Parse(rslt.dstResult.Tables[0].Rows[0]["SA_DATE"].ToString());
                                                    SA_DATE = dtSA_DATE;
                                                    var CatResul = JsonConvert.SerializeObject(SA_DATE);
                                                    return Json(CatResul);
                                                }
                                            }
                                        }                                
                                    }
                                    catch (Exception ex)
                                    {
                                        return Json(null);
                                    }
                                }
                            }
                            else
                            {
                                //No data found exception
                                dicAdvanceSalary["SAL_ADVANCE_SA_DATE"] = DateTime.Parse(DateTime.Now.ToShortDateString()); ;
                                SA_DATE = DateTime.Parse(DateTime.Now.ToShortDateString());                            
                                var CatResul = JsonConvert.SerializeObject(SA_DATE);
                                return Json(CatResul);
                        }
                        }
                    }
                    #endregion
               
            }
            catch (Exception ex)
            {
                return Json(null);
            }
            return Json(null);
        }

        public IActionResult txtMonth1_Validation(string SA_FOR_THE_MONTH1)
        { 
            try
            {
                Dictionary<string, object> dicAdvanceSalary = new Dictionary<string, object>();

                /*----------------------To store input parameters of StoredProcedure--------------------*/
                Dictionary<string, object> dicInputParameters = new Dictionary<string, object>();

                dicAdvanceSalary.Clear();

                //-----BLK_HEAD---------16 ITEMS
                dicAdvanceSalary.Add("BLK_HEAD_W_OPTION", null);
                dicAdvanceSalary.Add("BLK_HEAD_W_CNT", null);
                dicAdvanceSalary.Add("BLK_HEAD_W_CATEGORY", null);
                dicAdvanceSalary.Add("BLK_HEAD_W_VAL1", null);
                dicAdvanceSalary.Add("BLK_HEAD_W_VAL2", null);
                dicAdvanceSalary.Add("BLK_HEAD_W_BASIC", null);
                dicAdvanceSalary.Add("BLK_HEAD_W_SUM", null);
                dicAdvanceSalary.Add("BLK_HEAD_W_YEAR", null);
                dicAdvanceSalary.Add("BLK_HEAD_W_FLAG", null);
                dicAdvanceSalary.Add("BLK_HEAD_W_YSY", null);
                dicAdvanceSalary.Add("BLK_HEAD_W_ANSWER", null);
                dicAdvanceSalary.Add("BLK_HEAD_W_ANSWER3", null);
                dicAdvanceSalary.Add("BLK_HEAD_W_TOTAL", null);
                dicAdvanceSalary.Add("BLK_HEAD_W_DIS", null);
                dicAdvanceSalary.Add("BLK_HEAD_W_USER", null);
                dicAdvanceSalary.Add("BLK_HEAD_W_LOC", null);

                //-------BLK_P-----------2 ITEMS
                dicAdvanceSalary.Add("BLK_P_PR_P_NO", null);
                dicAdvanceSalary.Add("BLK_P_PR_FIRST_NAME", null);

                //-----SAL_ADVANCE---------5 ITEMS
                dicAdvanceSalary.Add("SAL_ADVANCE_SA_P_NO", null);
                dicAdvanceSalary.Add("SAL_ADVANCE_SA_DATE", null);
                dicAdvanceSalary.Add("SAL_ADVANCE_SA_FOR_THE_MONTH1", null);
                dicAdvanceSalary.Add("SAL_ADVANCE_SA_FOR_THE_MONTH2", null);
                dicAdvanceSalary.Add("SAL_ADVANCE_SA_ADV_AMOUNT", null);

                //------PER-----------2 ITEMS
                dicAdvanceSalary.Add("PER_PR_P_NO", null);
                dicAdvanceSalary.Add("PER_PR_FIRST_NAME", null);

                //-----SAL_ADVANCE1-------5 ITEMS
                dicAdvanceSalary.Add("SAL_ADVANCE1_SA_P_NO", null);
                dicAdvanceSalary.Add("SAL_ADVANCE1_W_NAME", null);
                dicAdvanceSalary.Add("SAL_ADVANCE1_SA_FOR_THE_MONTH1", null);
                dicAdvanceSalary.Add("SAL_ADVANCE1_SA_FOR_THE_MONTH2", null);
                dicAdvanceSalary.Add("SAL_ADVANCE1_SA_ADV_AMOUNT", null);

                if (SA_FOR_THE_MONTH1 != "")
                {
                    dicAdvanceSalary["SAL_ADVANCE_SA_FOR_THE_MONTH1"] = SA_FOR_THE_MONTH1;
                    DateTime dtSaDate = Convert.ToDateTime(dicAdvanceSalary["SAL_ADVANCE_SA_DATE"]);

                    if (dtSaDate.Year != 1 && dtSaDate.Month != Int32.Parse(SA_FOR_THE_MONTH1))
                    {
                        if (SA_FOR_THE_MONTH1.Trim().Length == 1)
                            SA_FOR_THE_MONTH1 = SA_FOR_THE_MONTH1.Trim().PadLeft(2, '0');
                       return Json("VALUE HAS TO BE " + dtSaDate.Month + ". ADVANCE DATE IS " + dtSaDate.ToString("dd/MM/yyyy"));

                       
                    }
                    if (SA_FOR_THE_MONTH1.Trim().Length == 1)
                        SA_FOR_THE_MONTH1 = SA_FOR_THE_MONTH1.Trim().PadLeft(2, '0');

                    var CatResul = JsonConvert.SerializeObject(SA_FOR_THE_MONTH1);
                    return Json(CatResul);
                }
                else
                {
                    return (null);
                }

            }
            catch (Exception ex)
            {
                return Json(null);
            }

            return Json(null);
        }


        public IActionResult txtMonth2_Validation(decimal? SA_P_NO,string SA_FOR_THE_MONTH1, string SA_FOR_THE_MONTH2)
        {
            if (SA_FOR_THE_MONTH2 == "11")
            {
                try
                {
                    int globalNum;
                    Dictionary<string, object> dicAdvanceSalary = new Dictionary<string, object>();

                    /*----------------------To store input parameters of StoredProcedure--------------------*/
                    Dictionary<string, object> dicInputParameters = new Dictionary<string, object>();

                    dicAdvanceSalary.Clear();

                    //-----BLK_HEAD---------16 ITEMS
                    dicAdvanceSalary.Add("BLK_HEAD_W_OPTION", null);
                    dicAdvanceSalary.Add("BLK_HEAD_W_CNT", null);
                    dicAdvanceSalary.Add("BLK_HEAD_W_CATEGORY", null);
                    dicAdvanceSalary.Add("BLK_HEAD_W_VAL1", null);
                    dicAdvanceSalary.Add("BLK_HEAD_W_VAL2", null);
                    dicAdvanceSalary.Add("BLK_HEAD_W_BASIC", null);
                    dicAdvanceSalary.Add("BLK_HEAD_W_SUM", null);
                    dicAdvanceSalary.Add("BLK_HEAD_W_YEAR", null);
                    dicAdvanceSalary.Add("BLK_HEAD_W_FLAG", null);
                    dicAdvanceSalary.Add("BLK_HEAD_W_YSY", null);
                    dicAdvanceSalary.Add("BLK_HEAD_W_ANSWER", null);
                    dicAdvanceSalary.Add("BLK_HEAD_W_ANSWER3", null);
                    dicAdvanceSalary.Add("BLK_HEAD_W_TOTAL", null);
                    dicAdvanceSalary.Add("BLK_HEAD_W_DIS", null);
                    dicAdvanceSalary.Add("BLK_HEAD_W_USER", null);
                    dicAdvanceSalary.Add("BLK_HEAD_W_LOC", null);

                    //-------BLK_P-----------2 ITEMS
                    dicAdvanceSalary.Add("BLK_P_PR_P_NO", null);
                    dicAdvanceSalary.Add("BLK_P_PR_FIRST_NAME", null);

                    //-----SAL_ADVANCE---------5 ITEMS
                    dicAdvanceSalary.Add("SAL_ADVANCE_SA_P_NO", null);
                    dicAdvanceSalary.Add("SAL_ADVANCE_SA_DATE", null);
                    dicAdvanceSalary.Add("SAL_ADVANCE_SA_FOR_THE_MONTH1", null);
                    dicAdvanceSalary.Add("SAL_ADVANCE_SA_FOR_THE_MONTH2", null);
                    dicAdvanceSalary.Add("SAL_ADVANCE_SA_ADV_AMOUNT", null);

                    //------PER-----------2 ITEMS
                    dicAdvanceSalary.Add("PER_PR_P_NO", null);
                    dicAdvanceSalary.Add("PER_PR_FIRST_NAME", null);

                    //-----SAL_ADVANCE1-------5 ITEMS
                    dicAdvanceSalary.Add("SAL_ADVANCE1_SA_P_NO", null);
                    dicAdvanceSalary.Add("SAL_ADVANCE1_W_NAME", null);
                    dicAdvanceSalary.Add("SAL_ADVANCE1_SA_FOR_THE_MONTH1", null);
                    dicAdvanceSalary.Add("SAL_ADVANCE1_SA_FOR_THE_MONTH2", null);
                    dicAdvanceSalary.Add("SAL_ADVANCE1_SA_ADV_AMOUNT", null);

                    if (SA_FOR_THE_MONTH2 != "")
                    {
                        dicAdvanceSalary["SAL_ADVANCE_SA_FOR_THE_MONTH2"] = SA_FOR_THE_MONTH2;
                        DateTime dtSaDate = Convert.ToDateTime(dicAdvanceSalary["SAL_ADVANCE_SA_DATE"]);
                        if (dtSaDate.Month != 12)
                        {                            
                            if (SA_FOR_THE_MONTH2.Trim().Length == 1)
                                SA_FOR_THE_MONTH2.Trim().PadLeft(2, '0');
                               
                            try
                            {
                                #region when validate item handling

                                dicInputParameters.Clear();
                                dicInputParameters.Add("SA_P_NO", SA_P_NO);
                                dicInputParameters.Add("SA_FOR_THE_MONTH2", SA_FOR_THE_MONTH2);

                                CmnDataManager objCmnDataManager = new CmnDataManager();
                                Result rslt = objCmnDataManager.GetData("CHRIS_SP_Payroll_SalAdv_SAL_ADVANCE_MANAGER", "SALADV", dicInputParameters);

                                if (rslt.isSuccessful)
                                {
                                    if (rslt.dstResult.Tables.Count > 0)
                                    {
                                        if (rslt.dstResult.Tables[0].Rows.Count > 0)
                                        {
                                            if (rslt.dstResult.Tables[0].Rows[0]["SA_ADV_AMOUNT"] != null)
                                            {
                                                //if (this.FunctionConfig.CurrentOption != Function.Modify)
                                                //{
                                                var SA_ADV_AMOUNT = rslt.dstResult.Tables[0].Rows[0]["SA_ADV_AMOUNT"].ToString();
                                                //}
                                                dicAdvanceSalary["SAL_ADVANCE_SA_ADV_AMOUNT"] = rslt.dstResult.Tables[0].Rows[0]["SA_ADV_AMOUNT"].ToString();
                                                var global_sa_advance = double.Parse(dicAdvanceSalary["SAL_ADVANCE_SA_ADV_AMOUNT"].ToString());
                                                var CatResul = JsonConvert.SerializeObject(global_sa_advance);
                                                return Json(CatResul);
                                            }
                                        }
                                    }
                                }

                                #endregion
                            }
                            catch (Exception ex)
                            {
                                return Json(null);
                            }

                          //  }
                        }
                        else
                        {
                            if (Int32.Parse(SA_FOR_THE_MONTH2) != 1)
                            {
                                return Json(" VALUE CAN ONLY BE 11");
                            }
                            else
                            {
                                if (SA_FOR_THE_MONTH2.Trim().Length == 1)
                                    SA_FOR_THE_MONTH2.Trim().PadLeft(2, '0');

                                var CatResul = JsonConvert.SerializeObject(SA_FOR_THE_MONTH2);
                                return Json(CatResul);
                            }
                        }
                    }
                    else
                    {
                        return Json(null);
                    }
                }
                catch (Exception ex)
                {
                    return Json(null);
                }
            }
            else
            {
                return Json("VALUE CAN ONLY BE 11--");
            }           

            return Json(null);
        }

        [HttpPost]
        public JsonResult UpsertSalaryAdvanceEntry(decimal? SA_P_NO, DateTime? SA_DATE, decimal? SA_ADV_AMOUNT, string SA_FOR_THE_MONTH1, string SA_FOR_THE_MONTH2, string SA_PAYROLL_GEN, int? ID,DateTime? W_DATE)
        {
            try
            {
                Dictionary<string, object> d = new Dictionary<string, object>();

                d.Add("SA_P_NO", SA_P_NO);
                d.Add("SA_DATE", SA_DATE);
                d.Add("SA_ADV_AMOUNT", SA_ADV_AMOUNT);
                d.Add("SA_FOR_THE_MONTH1", SA_FOR_THE_MONTH1);
                d.Add("SA_FOR_THE_MONTH2", SA_FOR_THE_MONTH2);
                d.Add("SA_PAYROLL_GEN", SA_PAYROLL_GEN);
                d.Add("ID", ID);
                d.Add("W_DATE", W_DATE);

                CmnDataManager objCmnDataManager = new CmnDataManager();

                if (ID > 0)
                {
                    Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_Payroll_SalAdv_SAL_ADVANCE_MANAGER", "Update", d);
                    if (rsltCode.isSuccessful)
                    {
                        var res = Json(rsltCode);
                        return Json("Your request is modify successfully !");
                    }
                }
                else
                {
                    Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_Payroll_SalAdv_SAL_ADVANCE_MANAGER", "Save", d);
                    if (rsltCode.isSuccessful)
                    {
                        var res = Json(rsltCode);
                        return Json("Your request is successfully completed !");
                    }
                }

                return Json(null);
            }
            catch (Exception ex)
            {
                return Json("Something went wrong");
            }
        }


        #endregion

        #region LFAEntry
        public IActionResult LFAEntry()
        {
            return View();
        }
        public IActionResult btn_LF_P_NO_Modal()
        {
            Dictionary<string, object> d = new Dictionary<string, object>();

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_LFA_TAB_MANAGER", "PersonnelLov", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }
        public IActionResult VIEWLFAEntry()
        {
            Dictionary<string, object> d = new Dictionary<string, object>();
            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_LFA_TAB_MANAGER", "View_Records", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        [HttpPost]
        public JsonResult UpsertLFAEntry(decimal? LF_P_NO, DateTime? LF_DATE, decimal? LF_LEAV_BAL, decimal? LF_LFA_AMOUNT, string LF_APPROVED, string LF_PRINT_REP, int? ID)
        {

            try
            {
                Dictionary<string, object> d = new Dictionary<string, object>();

                d.Add("LF_P_NO", LF_P_NO);
                d.Add("LF_DATE", LF_DATE);
                d.Add("LF_LEAV_BAL", LF_LEAV_BAL);
                d.Add("LF_LFA_AMOUNT", LF_LFA_AMOUNT);
                d.Add("LF_APPROVED", LF_APPROVED);
                d.Add("LF_PRINT_REP", LF_PRINT_REP);
                d.Add("ID", ID);

                CmnDataManager objCmnDataManager = new CmnDataManager();

                if (ID > 0)
                {
                    Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_LFA_TAB_MANAGER", "Update", d);
                    if (rsltCode.isSuccessful)
                    {
                        var res = Json(rsltCode);
                        return Json("Your request is modify successfully !");
                    }
                }
                else
                {
                    Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_LFA_TAB_MANAGER", "Save", d);
                    if (rsltCode.isSuccessful)
                    {
                        var res = Json(rsltCode);
                        return Json("Your request is successfully completed !");
                    }
                }

                return Json(null);
            }
            catch (Exception ex)
            {
                return Json("Something went wrong");
            }
        }

        public IActionResult Check_Mofify_Validation(decimal? LF_P_NO, DateTime? LF_DATE)
        {
            /*-----------------------------To store data on form level------------------------------*/
            Dictionary<string, object> dicLFA = new Dictionary<string, object>();
            /*----------------------To store input parameters of StoredProcedure--------------------*/
            Dictionary<string, object> dicInputParameters = new Dictionary<string, object>();
            /*------------------------------For custom call in database---------------------------- */
            CmnDataManager objCmnDataManager;

            dicLFA.Clear();

            //-----BLK_HEAD---------16 ITEMS
            dicLFA.Add("BLK_HEAD_W_OPTION", null);
            dicLFA.Add("BLK_HEAD_W_CNT", null);
            dicLFA.Add("BLK_HEAD_W_CATEGORY", null);
            dicLFA.Add("BLK_HEAD_W_VAL1", null);
            dicLFA.Add("BLK_HEAD_W_VAL2", null);
            dicLFA.Add("BLK_HEAD_W_BASIC", null);
            dicLFA.Add("BLK_HEAD_W_SUM", null);
            dicLFA.Add("BLK_HEAD_W_YEAR", null);
            dicLFA.Add("BLK_HEAD_W_FLAG", null);
            dicLFA.Add("BLK_HEAD_W_YSY", null);
            dicLFA.Add("BLK_HEAD_W_ANSWER", null);
            dicLFA.Add("BLK_HEAD_W_ANSWER3", null);
            dicLFA.Add("BLK_HEAD_W_TOTAL", null);
            dicLFA.Add("BLK_HEAD_W_DIS", null);
            dicLFA.Add("BLK_HEAD_W_USER", null);
            dicLFA.Add("BLK_HEAD_W_LOC", null);

            //-------BLK_P-----------4 ITEMS
            dicLFA.Add("BLK_P_PR_P_NO", null);
            dicLFA.Add("BLK_P_PR_NEW_BRANCH", null);
            dicLFA.Add("BLK_P_PR_FIRST_NAME", null);
            dicLFA.Add("BLK_P_W_DATE", null);

            //-----LFA_TAB---------5 ITEMS
            dicLFA.Add("LFA_TAB_LF_P_NO", null);
            dicLFA.Add("LFA_TAB_LF_DATE", null);
            dicLFA.Add("LFA_TAB_LF_LEAV_BAL", null);
            //dicLFA.Add("LFA_TAB_LF_LEAV_BAL11",null);
            dicLFA.Add("LFA_TAB_LF_LFA_AMOUNT", null);
            dicLFA.Add("LFA_TAB_LF_APPROVED", null);

            //------PER-----------2 ITEMS
            dicLFA.Add("PER_PR_P_NO", null);
            dicLFA.Add("PER_PR_FIRST_NAME", null);

            //-----LFA_TAB1-------5 ITEMS
            dicLFA.Add("LFA_TAB1_LF_P_NO", null);
            dicLFA.Add("LFA_TAB1_LEAV_BAL", null);
            //dicLFA.Add("LFA_TAB1_LEAV_BAL11", null);
            dicLFA.Add("LFA_TAB1_LFA_AMOUNT", null);
            dicLFA.Add("LFA_TAB1_LF_APPROVED", null);
            dicLFA.Add("LFA_TAB1_W_NAME", null);

            dicInputParameters.Clear();
            dicInputParameters.Add("inp_pr_p_no", LF_P_NO);

            Result rsltLFA = new Result();
            objCmnDataManager = new CmnDataManager();

            #region Modification

            rsltLFA = objCmnDataManager.GetData("CHRIS_SP_LFA_TAB_MANAGER", "Check_Insertion", dicInputParameters);
            if (rsltLFA.isSuccessful)
            {
                if (rsltLFA.dstResult.Tables.Count > 0)
                {
                    if (rsltLFA.dstResult.Tables[0].Rows.Count > 0)
                    {
                       
                        DateTime dtLFdate = DateTime.Parse(Convert.ToString(rsltLFA.dstResult.Tables[0].Rows[0]["LF_DATE"]));
                        //LF_DATE = dtLFdate.ToShortDateString();
                        var CatResul = JsonConvert.SerializeObject(dtLFdate);
                        return Json(CatResul);

                    }
                    else
                    {
                       //dicLFA["BLK_HEAD_W_TOTAL"] = 0;                       
                         return Json("NO RECORD IS THERE TO BE MODIFIED");                       
                    }
                }
            }
            #endregion

            return Json(null);
        }

        public IActionResult Delete_LFAEntry(int? ID)
        {
            try
            {
                Dictionary<string, object> d = new Dictionary<string, object>();
                d.Add("ID", ID);
                CmnDataManager objCmnDataManager = new CmnDataManager();
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_LFA_TAB_MANAGER", "Delete", d);
                if (rsltCode.isSuccessful)
                {
                    return Json("Record delete successfully !");
                }
                return Json(null);
            }
            catch (Exception)
            {
                return Json(null);
            }
           
        }

        public IActionResult txtLF_P_NO_Validation(decimal? LF_P_NO, DateTime? LF_DATE)
        {
            try
            {
                /*-----------------------------To store data on form level------------------------------*/
                Dictionary<string, object> dicLFA = new Dictionary<string, object>();
                /*----------------------To store input parameters of StoredProcedure--------------------*/
                Dictionary<string, object> dicInputParameters = new Dictionary<string, object>();
                /*------------------------------For custom call in database---------------------------- */
                CmnDataManager objCmnDataManager;

                dicLFA.Clear();

                //-----BLK_HEAD---------16 ITEMS
                dicLFA.Add("BLK_HEAD_W_OPTION", null);
                dicLFA.Add("BLK_HEAD_W_CNT", null);
                dicLFA.Add("BLK_HEAD_W_CATEGORY", null);
                dicLFA.Add("BLK_HEAD_W_VAL1", null);
                dicLFA.Add("BLK_HEAD_W_VAL2", null);
                dicLFA.Add("BLK_HEAD_W_BASIC", null);
                dicLFA.Add("BLK_HEAD_W_SUM", null);
                dicLFA.Add("BLK_HEAD_W_YEAR", null);
                dicLFA.Add("BLK_HEAD_W_FLAG", null);
                dicLFA.Add("BLK_HEAD_W_YSY", null);
                dicLFA.Add("BLK_HEAD_W_ANSWER", null);
                dicLFA.Add("BLK_HEAD_W_ANSWER3", null);
                dicLFA.Add("BLK_HEAD_W_TOTAL", null);
                dicLFA.Add("BLK_HEAD_W_DIS", null);
                dicLFA.Add("BLK_HEAD_W_USER", null);
                dicLFA.Add("BLK_HEAD_W_LOC", null);

                //-------BLK_P-----------4 ITEMS
                dicLFA.Add("BLK_P_PR_P_NO", null);
                dicLFA.Add("BLK_P_PR_NEW_BRANCH", null);
                dicLFA.Add("BLK_P_PR_FIRST_NAME", null);
                dicLFA.Add("BLK_P_W_DATE", null);

                //-----LFA_TAB---------5 ITEMS
                dicLFA.Add("LFA_TAB_LF_P_NO", null);
                dicLFA.Add("LFA_TAB_LF_DATE", null);
                dicLFA.Add("LFA_TAB_LF_LEAV_BAL", null);
                //dicLFA.Add("LFA_TAB_LF_LEAV_BAL11",null);
                dicLFA.Add("LFA_TAB_LF_LFA_AMOUNT", null);
                dicLFA.Add("LFA_TAB_LF_APPROVED", null);

                //------PER-----------2 ITEMS
                dicLFA.Add("PER_PR_P_NO", null);
                dicLFA.Add("PER_PR_FIRST_NAME", null);

                //-----LFA_TAB1-------5 ITEMS
                dicLFA.Add("LFA_TAB1_LF_P_NO", null);
                dicLFA.Add("LFA_TAB1_LEAV_BAL", null);
                //dicLFA.Add("LFA_TAB1_LEAV_BAL11", null);
                dicLFA.Add("LFA_TAB1_LFA_AMOUNT", null);
                dicLFA.Add("LFA_TAB1_LF_APPROVED", null);
                dicLFA.Add("LFA_TAB1_W_NAME", null);

                dicInputParameters.Clear();
                dicInputParameters.Add("inp_pr_p_no", LF_P_NO);

                Result rsltLFA = new Result();
                objCmnDataManager = new CmnDataManager();
                               
                #region Insertion

                dicLFA["BLK_P_W_DATE"] = System.DateTime.Now.Date.ToString("dd/MM/yyyy");
                LF_DATE = DateTime.Parse(System.DateTime.Now.Date.ToString("dd/MM/yyyy")); 

                rsltLFA = objCmnDataManager.GetData("CHRIS_SP_LFA_TAB_MANAGER", "Check_Insertion", dicInputParameters);

                if (rsltLFA.isSuccessful)
                {
                    if (rsltLFA.dstResult.Tables.Count > 0)
                    {
                        if (rsltLFA.dstResult.Tables[0].Rows.Count > 0)
                        {
                            return Json("VALUE IS ALREADY THERE FOR THE CURRENT MONTH");                           
                        }
                        else
                        {
                            dicLFA["LFA_TAB_LF_DATE"] = System.DateTime.Now.Date.ToString("dd/MM/yyyy"); 
                            var CatResul = JsonConvert.SerializeObject(dicLFA);
                            return Json(CatResul);
                        }
                    }
                }
                #endregion

            }
            catch (Exception ex)
            {
                return Json(null);
            }
            return Json(null);
        }

        #endregion                

        #region SalaryEntry

        public IActionResult SalaryEntry()
        {
            return View();
        }

        public IActionResult btn_Branch_Modal()
        {
            Dictionary<string, object> d = new Dictionary<string, object>();

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_BRANCH_MANAGER", "GetBranchLov", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        #region PROC
        CmnDataManager objCmnDataManager = new CmnDataManager();
        /*-------------To store data block variables--------------*/
        Dictionary<string, object> DataBlockValues = new Dictionary<string, object>();
        /*------To store input parameters of StoredProcedure------*/
        Dictionary<string, object> dicInputParameters = new Dictionary<string, object>();

        private void InitializeDic()
        {

            //69 feilds.....no comments
            this.DataBlockValues.Clear();
            DataBlockValues.Add("W_FROM", null);
            DataBlockValues.Add("W_TO", null);
            DataBlockValues.Add("W_BRN", null);
            DataBlockValues.Add("W_MOLD", null);
            DataBlockValues.Add("W_COLD", null);
            DataBlockValues.Add("W_MEAL_AMT", null);
            DataBlockValues.Add("W_CONV_AMT", null);
            DataBlockValues.Add("W_LFA", null);
            DataBlockValues.Add("W_NEW_LFA", null);
            DataBlockValues.Add("W_MED", null);
            DataBlockValues.Add("W_NEW_MED", null);
            DataBlockValues.Add("PR_P_NO", null);
            DataBlockValues.Add("W_DATE", null);
            DataBlockValues.Add("W_DATE1", null);
            DataBlockValues.Add("W_CATE", null);
            DataBlockValues.Add("W_INCOME", null);
            DataBlockValues.Add("W_TAX_REC", null);
            DataBlockValues.Add("PR_PACK", null);
            DataBlockValues.Add("LEV", null);
            DataBlockValues.Add("ORG", null);
            DataBlockValues.Add("STEP", null);
            DataBlockValues.Add("ADJ", null);
            DataBlockValues.Add("AREAR", null);
            DataBlockValues.Add("W_MONTHS", null);
            DataBlockValues.Add("START1", null);
            DataBlockValues.Add("W_JOINING", null);
            DataBlockValues.Add("W_EFF", null);
            DataBlockValues.Add("W_PF", null);
            DataBlockValues.Add("W_SD1", null);
            DataBlockValues.Add("W_SD2", null);
            DataBlockValues.Add("W_SHR", null);
            DataBlockValues.Add("W_DHR", null);
            DataBlockValues.Add("W_MEAL", null);
            DataBlockValues.Add("W_CONV", null);
            DataBlockValues.Add("W_FOUND", null);
            DataBlockValues.Add("W_SAL_AREAR", null);
            DataBlockValues.Add("W_PROMOTED", null);
            DataBlockValues.Add("W_FORCAST_OT", null);
            DataBlockValues.Add("W_ACTUAL_OT", null);
            DataBlockValues.Add("W_AVG", null);
            DataBlockValues.Add("W_AREARS", null);
            DataBlockValues.Add("W_OT_MONTHS", null);
            DataBlockValues.Add("W_FORCAST_MONTHS", null);
            DataBlockValues.Add("W_FORCAST", null);
            DataBlockValues.Add("W_OT_ASR", null);
            DataBlockValues.Add("W_PR_DATE", null);
            DataBlockValues.Add("W_TRANS_DATE", null);
            DataBlockValues.Add("W_DEPT_FT", null);
            DataBlockValues.Add("W_TAX_PACK", null);
            DataBlockValues.Add("W_10_BONUS", null);
            DataBlockValues.Add("W_ANNUAL_BASIC", null);
            DataBlockValues.Add("W_GOVT_ALL", null);
            DataBlockValues.Add("W_AN_PACK", null);
            DataBlockValues.Add("W_P_BASIC", null);
            DataBlockValues.Add("W_DESIG", null);
            DataBlockValues.Add("W_LEVEL", null);
            DataBlockValues.Add("W_OTHER_ALL", null);
            DataBlockValues.Add("W_BASIC_AREAR", null);
            DataBlockValues.Add("W_BRANCH", null);
            DataBlockValues.Add("W_VP", null);
            DataBlockValues.Add("W_ZAKAT", null);
            DataBlockValues.Add("W_PRV_INC", null);
            DataBlockValues.Add("W_TAX_USED", null);
            DataBlockValues.Add("W_TAXABLE_INC", null);
            DataBlockValues.Add("W_PRV_TAX_PD", null);
            DataBlockValues.Add("W_ANNUAL_TAX", null);
            DataBlockValues.Add("W_ITAX", null);
            DataBlockValues.Add("W_INC_EFF", null);
            DataBlockValues.Add("W_CONF", null);

            DataBlockValues.Add("W_TAX1", null);
            DataBlockValues.Add("W_TAX2", null);
            DataBlockValues.Add("W_AREAR_TAX", null);

            DataBlockValues["W_OT_ASR"] = 0;
            DataBlockValues["W_GOVT_ALL"] = 0;
            DataBlockValues["W_ITAX"] = 0;
            DataBlockValues["W_P_BASIC"] = 0;
            DataBlockValues["W_ANNUAL_TAX"] = 0;
            DataBlockValues["W_BASIC_AREAR"] = 0;
            DataBlockValues["W_SAL_AREAR"] = 0;
            DataBlockValues["W_FORCAST"] = 0;
            DataBlockValues["W_AREAR_TAX"] = 0;

        }

        void Proc1()
        {
            DataTable dtEmployee = null;

            //PROC_1 LOCAL VARIABLES
            float wtax1 = 0;
            float wtax2 = 0;

            try
            {
                dicInputParameters.Clear();
                dicInputParameters.Add("pW_BRN", DataBlockValues["W_BRN"]);
                dicInputParameters.Add("pW_FROM", DataBlockValues["W_FROM"]);
                dicInputParameters.Add("pW_TO", DataBlockValues["W_TO"]);

                Result rslt = objCmnDataManager.GetData("CHRIS_SP_UNION_ARREARS_PROCESSING_PROC1", "", dicInputParameters);

                if (rslt.isSuccessful)
                {
                    if (rslt.dstResult != null)
                    {
                        if (rslt.dstResult.Tables.Count > 0)
                        {
                            if (rslt.dstResult.Tables[0].Rows.Count > 0)
                            {
                                dtEmployee = rslt.dstResult.Tables[0];
                                for (int EmployeeNo = 0; EmployeeNo < dtEmployee.Rows.Count; EmployeeNo++)
                                {
                                    DataBlockValues["PR_P_NO"] = dtEmployee.Rows[EmployeeNo]["pr_p_no"];
                                    DataBlockValues["W_CATE"] = dtEmployee.Rows[EmployeeNo]["pr_category"];
                                    DataBlockValues["LEV"] = dtEmployee.Rows[EmployeeNo]["pr_level"];
                                    DataBlockValues["PR_PACK"] = dtEmployee.Rows[EmployeeNo]["pr_new_annual_pack"];
                                    DataBlockValues["W_JOINING"] = dtEmployee.Rows[EmployeeNo]["pr_joining_date"];
                                    DataBlockValues["W_CONF"] = dtEmployee.Rows[EmployeeNo]["pr_conf_flag"];
                                    DataBlockValues["W_DESIG"] = dtEmployee.Rows[EmployeeNo]["pr_desig"];
                                    DataBlockValues["W_LEVEL"] = dtEmployee.Rows[EmployeeNo]["pr_level"];
                                    DataBlockValues["W_ZAKAT"] = dtEmployee.Rows[EmployeeNo]["pr_zakat_amt"];
                                    DataBlockValues["W_PRV_INC"] = dtEmployee.Rows[EmployeeNo]["pr_tax_inc"];
                                    DataBlockValues["W_PRV_TAX_PD"] = dtEmployee.Rows[EmployeeNo]["pr_tax_paid"];
                                    DataBlockValues["W_TAX_USED"] = dtEmployee.Rows[EmployeeNo]["pr_tax_used"];
                                    DataBlockValues["W_BRANCH"] = dtEmployee.Rows[EmployeeNo]["pr_new_branch"];
                                    DataBlockValues["W_PR_DATE"] = dtEmployee.Rows[EmployeeNo]["pr_promotion_date"];
                                    DataBlockValues["W_TRANS_DATE"] = dtEmployee.Rows[EmployeeNo]["pr_transfer_date"];
                                    DataBlockValues["W_AN_PACK"] = dtEmployee.Rows[EmployeeNo]["pr_new_annual_pack"];
                                    DataBlockValues["W_PROMOTED"] = null;

                                    if (DataBlockValues["W_CATE"] != DBNull.Value)
                                    {
                                        if (DataBlockValues["W_CATE"].ToString() == "C")
                                        {
                                            Proc2();
                                        }
                                        else
                                        {
                                            Proc4();
                                        }
                                    }

                                    Proc90();

                                    if (DataBlockValues["W_CATE"].ToString() == "C")
                                    {
                                        Proc97();
                                    }

                                    Proc96();

                                    Proc99();

                                    DataBlockValues["W_TAX1"] = DataBlockValues["W_ANNUAL_TAX"];

                                    Proc9();

                                    Proc99();

                                    DataBlockValues["W_TAX2"] = DataBlockValues["W_ANNUAL_TAX"];

                                    UpdateSalArear();
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        void Proc2()
        {
            try
            {
                DataTable dtPROC2 = null;
                char WFOUND = 'N';
                int sw = 0;
                DateTime w_dt = new DateTime();
                DateTime w_dt2 = DateTime.Parse(DataBlockValues["W_TO"].ToString());

                dicInputParameters.Clear();

                dicInputParameters.Add("pW_FROM", DataBlockValues["W_FROM"]);
                dicInputParameters.Add("pW_TO", DataBlockValues["W_TO"]);
                dicInputParameters.Add("pPR_P_NO", DataBlockValues["PR_P_NO"]);

                Result rslt = objCmnDataManager.GetData("CHRIS_SP_UNION_ARREARS_PROCESSING_PROC2", "", dicInputParameters);

                if (rslt.isSuccessful)
                {
                    if (rslt.dstResult != null)
                    {
                        if (rslt.dstResult.Tables.Count > 0)
                        {
                            #region Data return by Cursor query

                            if (rslt.dstResult.Tables[0].Rows.Count > 0)
                            {
                                dtPROC2 = rslt.dstResult.Tables[0];
                                int LastRecordProc2 = dtPROC2.Rows.Count - 1;//For Last record

                                for (int i = 0; i < dtPROC2.Rows.Count; i++)
                                {
                                    #region For Loop
                                    WFOUND = 'Y';
                                    if (sw == 0)
                                    {
                                        w_dt = DateTime.Parse(DataBlockValues["W_FROM"].ToString());
                                        sw = 1;
                                        DataBlockValues["PR_PACK"] = dtPROC2.Rows[i]["PR_ANNUAL_PREVIOUS"];
                                        DataBlockValues["LEV"] = dtPROC2.Rows[i]["PR_LEVEL_PREVIOUS"];
                                        DataBlockValues["W_MONTHS"] = (DateTime.Parse(dtPROC2.Rows[i]["PR_EFFECTIVE"].ToString()), w_dt);
                                        w_dt = DateTime.Parse(dtPROC2.Rows[i]["PR_EFFECTIVE"].ToString());
                                        DataBlockValues["W_EFF"] = w_dt;
                                        DataBlockValues["W_SD1"] = DataBlockValues["W_FROM"];
                                        DataBlockValues["W_SD2"] = w_dt;
                                        Proc5();
                                        //WFOUND = 'Y';
                                        continue; // loop continues to execute with the next iteration                    
                                    }

                                    DataBlockValues["W_MONTHS"] = (DateTime.Parse(dtPROC2.Rows[i]["PR_EFFECTIVE"].ToString()), w_dt);
                                    DataBlockValues["PR_PACK"] = dtPROC2.Rows[i]["PR_ANNUAL_PREVIOUS"];
                                    DataBlockValues["LEV"] = dtPROC2.Rows[i]["PR_LEVEL_PREVIOUS"];
                                    w_dt = DateTime.Parse(dtPROC2.Rows[i]["PR_EFFECTIVE"].ToString());
                                    DataBlockValues["W_EFF"] = w_dt;
                                    DataBlockValues["W_SD1"] = DataBlockValues["W_SD2"];
                                    DataBlockValues["W_SD2"] = w_dt;
                                    Proc5();
                                    #endregion
                                }

                                #region Last record
                                if (w_dt == null)
                                {
                                    w_dt = DateTime.Parse(DataBlockValues["W_FROM"].ToString());
                                }

                                if (WFOUND == 'Y')
                                {
                                    DataBlockValues["W_MONTHS"] = (w_dt2, w_dt); ;
                                    DataBlockValues["PR_PACK"] = dtPROC2.Rows[LastRecordProc2]["PR_ANNUAL_PRESENT"];
                                    DataBlockValues["LEV"] = dtPROC2.Rows[LastRecordProc2]["PR_LEVEL_PRESENT"];
                                    DataBlockValues["W_EFF"] = w_dt;
                                    DataBlockValues["W_SD1"] = DataBlockValues["W_SD2"];
                                    DataBlockValues["W_SD2"] = DataBlockValues["W_TO"];
                                    Proc5();
                                }
                                #endregion

                            }

                            #endregion

                            if (WFOUND == 'N')
                            {
                                Proc3();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        void Proc3()
        {
            try
            {
                DataTable dtPROC3 = null;
                char WFOUND = 'N';
                int sw = 0;
                DateTime w_dt = new DateTime();
                DateTime w_dt2 = DateTime.Parse(DataBlockValues["W_TO"].ToString());

                dicInputParameters.Clear();
                dicInputParameters.Add("pPR_P_NO", DataBlockValues["PR_P_NO"]);

                Result rslt = objCmnDataManager.GetData("CHRIS_SP_UNION_ARREARS_PROCESSING_PROC3", "", dicInputParameters);

                if (rslt.isSuccessful)
                {
                    if (rslt.dstResult != null)
                    {
                        if (rslt.dstResult.Tables.Count > 0)
                        {
                            #region Data return by Cursor query

                            if (rslt.dstResult.Tables[0].Rows.Count > 0)
                            {
                                dtPROC3 = rslt.dstResult.Tables[0];
                                int LastRecordProc3 = dtPROC3.Rows.Count - 1;//For Last record

                                for (int i = 0; i < dtPROC3.Rows.Count; i++)
                                {
                                    #region For Loop 

                                    WFOUND = 'Y';

                                    if (DateTime.Parse(dtPROC3.Rows[0]["pr_effective"].ToString()) < DateTime.Parse(DataBlockValues["W_FROM"].ToString()))
                                    {
                                        DataBlockValues["W_MONTHS"] = (DateTime.Parse(DataBlockValues["W_TO"].ToString()), DateTime.Parse(DataBlockValues["W_FROM"].ToString()));
                                        DataBlockValues["PR_PACK"] = dtPROC3.Rows[i]["PR_ANNUAL_PRESENT"];
                                        DataBlockValues["LEV"] = dtPROC3.Rows[i]["PR_LEVEL_PRESENT"];
                                        DataBlockValues["W_EFF"] = null;
                                        DataBlockValues["W_SD1"] = DataBlockValues["W_FROM"];
                                        DataBlockValues["W_SD2"] = DataBlockValues["W_TO"];
                                        Proc5();
                                        WFOUND = 'C';
                                        break; //looping is broken and stop
                                    }

                                    if (sw == 0)
                                    {
                                        w_dt = DateTime.Parse(DataBlockValues["W_FROM"].ToString());
                                        sw = 1;
                                        DataBlockValues["PR_PACK"] = dtPROC3.Rows[i]["PR_ANNUAL_PREVIOUS"];
                                        DataBlockValues["LEV"] = dtPROC3.Rows[i]["PR_LEVEL_PREVIOUS"];
                                        DataBlockValues["W_MONTHS"] = (DateTime.Parse(dtPROC3.Rows[i]["PR_EFFECTIVE"].ToString()), w_dt);
                                        w_dt = DateTime.Parse(dtPROC3.Rows[i]["PR_EFFECTIVE"].ToString());
                                        DataBlockValues["W_EFF"] = w_dt;
                                        Proc5();
                                        continue; // loop continues to execute with the next iteration                    

                                    }

                                    DataBlockValues["W_MONTHS"] = (DateTime.Parse(dtPROC3.Rows[i]["PR_EFFECTIVE"].ToString()), w_dt);
                                    DataBlockValues["PR_PACK"] = dtPROC3.Rows[i]["PR_ANNUAL_PREVIOUS"];
                                    DataBlockValues["LEV"] = dtPROC3.Rows[i]["PR_LEVEL_PREVIOUS"];
                                    w_dt = DateTime.Parse(dtPROC3.Rows[i]["PR_EFFECTIVE"].ToString());
                                    DataBlockValues["W_EFF"] = w_dt;
                                    DataBlockValues["W_SD1"] = DataBlockValues["W_SD2"];
                                    DataBlockValues["W_SD2"] = w_dt;
                                    Proc5();

                                    #endregion
                                } //end loop

                                #region Last record

                                if (w_dt == null)
                                {
                                    w_dt = DateTime.Parse(DataBlockValues["W_FROM"].ToString());
                                }

                                if ((dtPROC3.Rows[LastRecordProc3]["pr_promoted"] == DBNull.Value) && (WFOUND == 'Y'))
                                {
                                    DataBlockValues["W_MONTHS"] = (w_dt2, w_dt);
                                    DataBlockValues["PR_PACK"] = dtPROC3.Rows[LastRecordProc3]["PR_ANNUAL_PRESENT"];
                                    DataBlockValues["LEV"] = dtPROC3.Rows[LastRecordProc3]["PR_LEVEL_PRESENT"];
                                    DataBlockValues["W_EFF"] = w_dt;
                                    Proc5();
                                }

                                #endregion

                            }

                            #endregion

                            if (WFOUND == 'N')
                            {
                                DataBlockValues["W_FOUND"] = 1;
                                DataBlockValues["W_SD1"] = DataBlockValues["W_FROM"];
                                DataBlockValues["W_SD2"] = DataBlockValues["W_TO"];
                                DataBlockValues["W_MONTHS"] = (DateTime.Parse(DataBlockValues["W_TO"].ToString()), DateTime.Parse(DataBlockValues["W_JOINING"].ToString()));
                                if (DateTime.Parse(DataBlockValues["W_JOINING"].ToString()) < DateTime.Parse(DataBlockValues["W_FROM"].ToString()))
                                {
                                    DataBlockValues["W_MONTHS"] = 9;
                                }
                                Proc5();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        void Proc4()
        {
            try
            {
                DataTable dtPROC4 = null;
                char WFOUND = 'N';
                int sw = 0;
                DateTime w_dt = new DateTime();
                DateTime w_dt2 = new DateTime();

                dicInputParameters.Clear();

                dicInputParameters.Add("pW_FROM", DataBlockValues["W_FROM"]);
                dicInputParameters.Add("pW_TO", DataBlockValues["W_TO"]);
                dicInputParameters.Add("pPR_P_NO", DataBlockValues["PR_P_NO"]);

                Result rslt = objCmnDataManager.GetData("CHRIS_SP_UNION_ARREARS_PROCESSING_PROC4", "", dicInputParameters);

                if (rslt.isSuccessful)
                {
                    if (rslt.dstResult != null)
                    {
                        if (rslt.dstResult.Tables.Count > 0)
                        {
                            #region Data return by Cursor query

                            if (rslt.dstResult.Tables[0].Rows.Count > 0)
                            {
                                dtPROC4 = rslt.dstResult.Tables[0];
                                int LastRecordProc4 = dtPROC4.Rows.Count - 1;//For Last record

                                for (int i = 0; i < dtPROC4.Rows.Count; i++)
                                {
                                    #region For Loop

                                    WFOUND = 'Y';
                                    if (sw == 0)
                                    {
                                        w_dt = DateTime.Parse(DataBlockValues["W_FROM"].ToString());
                                        sw = 1;
                                        DataBlockValues["PR_PACK"] = dtPROC4.Rows[i]["PR_ANNUAL_PREVIOUS"];
                                        DataBlockValues["LEV"] = dtPROC4.Rows[i]["PR_LEVEL_PREVIOUS"];
                                        DataBlockValues["W_MONTHS"] = (DateTime.Parse(dtPROC4.Rows[i]["PR_EFFECTIVE"].ToString()), w_dt);
                                        w_dt = DateTime.Parse(dtPROC4.Rows[i]["PR_EFFECTIVE"].ToString());
                                        DataBlockValues["W_EFF"] = null;
                                        DataBlockValues["W_SD1"] = DataBlockValues["W_FROM"];
                                        DataBlockValues["W_SD2"] = w_dt;
                                        Proc5();
                                        //WFOUND = 'Y';
                                        continue; // loop continues to execute with the next iteration                    
                                    }
                                    DataBlockValues["W_MONTHS"] = (DateTime.Parse(dtPROC4.Rows[i]["PR_EFFECTIVE"].ToString()), w_dt);
                                    DataBlockValues["PR_PACK"] = dtPROC4.Rows[i]["PR_ANNUAL_PREVIOUS"];
                                    DataBlockValues["LEV"] = dtPROC4.Rows[i]["PR_LEVEL_PREVIOUS"];
                                    w_dt = DateTime.Parse(dtPROC4.Rows[i]["PR_EFFECTIVE"].ToString());
                                    DataBlockValues["W_EFF"] = w_dt;
                                    DataBlockValues["W_SD1"] = DataBlockValues["W_SD2"];
                                    DataBlockValues["W_SD2"] = w_dt;
                                    Proc5();

                                    #endregion
                                }

                                #region Last record

                                if (dtPROC4.Rows[LastRecordProc4]["pr_promoted"] == DBNull.Value)
                                {
                                    DataBlockValues["W_MONTHS"] = (w_dt2, w_dt); ;
                                    DataBlockValues["PR_PACK"] = dtPROC4.Rows[LastRecordProc4]["PR_ANNUAL_PRESENT"];
                                    DataBlockValues["LEV"] = dtPROC4.Rows[LastRecordProc4]["PR_LEVEL_PRESENT"];
                                    DataBlockValues["W_EFF"] = w_dt;
                                    DataBlockValues["W_SD1"] = DataBlockValues["W_SD2"];
                                    DataBlockValues["W_SD2"] = w_dt;
                                    Proc5();
                                }

                                #endregion                              
                            }

                            #endregion

                            if (WFOUND == 'N')
                            {
                                Proc3();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        void Proc5()
        {
            try
            {
                dicInputParameters.Clear();

                dicInputParameters.Add("pW_BRN", DataBlockValues["W_BRN"]);
                dicInputParameters.Add("pW_FROM", DataBlockValues["W_FROM"]);
                dicInputParameters.Add("pLEV", DataBlockValues["LEV"]);
                dicInputParameters.Add("pPR_PACK", DataBlockValues["PR_PACK"]);
                dicInputParameters.Add("pW_MONTHS", DataBlockValues["W_MONTHS"]);
                dicInputParameters.Add("pW_SD1", DataBlockValues["W_SD1"]);
                dicInputParameters.Add("pW_SD2", DataBlockValues["W_SD2"]);
                dicInputParameters.Add("pW_MOLD", DataBlockValues["W_MOLD"]);
                dicInputParameters.Add("pW_COLD", DataBlockValues["W_COLD"]);
                //dicInputParameters.Add("pORG", DataBlockValues["ORG"]);
                //dicInputParameters.Add("pSTEP", DataBlockValues["STEP"]);            
                dicInputParameters.Add("pPR_P_NO", DataBlockValues["PR_P_NO"]);
                dicInputParameters.Add("pW_TO", DataBlockValues["W_TO"]);
                //dicInputParameters.Add("pW_SHR", DataBlockValues["W_SHR"]);
                //dicInputParameters.Add("pW_DHR", DataBlockValues["W_DHR"]);                         
                dicInputParameters.Add("pW_MEAL_AMT", DataBlockValues["W_MEAL_AMT"]);
                dicInputParameters.Add("pW_CONV_AMT", DataBlockValues["W_CONV_AMT"]);

                Result rslt = objCmnDataManager.GetData("CHRIS_SP_UNION_ARREARS_PROCESSING_PROC5", "", dicInputParameters);

                if (rslt.isSuccessful)
                {
                    if (rslt.dstResult != null)
                    {
                        if (rslt.dstResult.Tables.Count > 0)
                        {
                            if (rslt.dstResult.Tables[0].Rows.Count > 0)
                            {
                                DataBlockValues["ORG"] = rslt.dstResult.Tables[0].Rows[0]["ORG"];
                                DataBlockValues["STEP"] = rslt.dstResult.Tables[0].Rows[0]["STEP"];
                                DataBlockValues["ADJ"] = rslt.dstResult.Tables[0].Rows[0]["ADJ"];
                                DataBlockValues["AREAR"] = rslt.dstResult.Tables[0].Rows[0]["AREAR"];
                                DataBlockValues["W_PF"] = rslt.dstResult.Tables[0].Rows[0]["W_PF"];
                                DataBlockValues["W_EFF"] = rslt.dstResult.Tables[0].Rows[0]["W_EFF"];
                                DataBlockValues["W_FOUND"] = rslt.dstResult.Tables[0].Rows[0]["W_FOUND"];
                                DataBlockValues["W_MEAL"] = rslt.dstResult.Tables[0].Rows[0]["W_MEAL"];
                                DataBlockValues["W_CONV"] = rslt.dstResult.Tables[0].Rows[0]["W_CONV"];
                                DataBlockValues["W_SHR"] = rslt.dstResult.Tables[0].Rows[0]["W_SHR"];
                                DataBlockValues["W_DHR"] = rslt.dstResult.Tables[0].Rows[0]["W_DHR"];
                                DataBlockValues["START1"] = rslt.dstResult.Tables[0].Rows[0]["START1"];
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        void Proc90()
        {
            try
            {
                dicInputParameters.Clear();
                dicInputParameters.Add("pPR_P_NO", DataBlockValues["PR_P_NO"]);
                dicInputParameters.Add("pW_DATE", DataBlockValues["W_DATE"]);
                dicInputParameters.Add("pW_PR_DATE", DataBlockValues["W_PR_DATE"]);

                Result rslt = objCmnDataManager.GetData("CHRIS_SP_UNION_ARREARS_PROCESSING_PROC90", "", dicInputParameters);

                if (rslt.isSuccessful)
                {
                    if (rslt.dstResult != null)
                    {
                        if (rslt.dstResult.Tables.Count > 0)
                        {
                            if (rslt.dstResult.Tables[0].Rows.Count > 0)
                            {
                                DataBlockValues["W_DESIG"] = rslt.dstResult.Tables[0].Rows[0]["W_DESIG"];
                                DataBlockValues["W_LEVEL"] = rslt.dstResult.Tables[0].Rows[0]["W_LEVEL"];
                                DataBlockValues["W_PROMOTED"] = rslt.dstResult.Tables[0].Rows[0]["W_PROMOTED"];

                                if (rslt.dstResult.Tables[0].Columns.Count > 3)
                                {
                                    DataBlockValues["W_AN_PACK"] = rslt.dstResult.Tables[0].Rows[0]["W_AN_PACK"];

                                    if (DataBlockValues["W_DESIG"].ToString() == "CTT")
                                    {
                                        DataBlockValues["W_CATE"] = "C";
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        void Proc97()
        {
            try
            {
                dicInputParameters.Clear();
                dicInputParameters.Add("pW_BRANCH", DataBlockValues["W_BRANCH"]);
                dicInputParameters.Add("pW_DESIG", DataBlockValues["W_DESIG"]);
                dicInputParameters.Add("pW_LEVEL", DataBlockValues["W_LEVEL"]);
                dicInputParameters.Add("pW_CATE", DataBlockValues["W_CATE"]);
                dicInputParameters.Add("pW_DATE", DataBlockValues["W_DATE"]);

                Result rslt = objCmnDataManager.GetData("CHRIS_SP_UNION_ARREARS_PROCESSING_PROC97", "", dicInputParameters);

                if (rslt.isSuccessful)
                {
                    if (rslt.dstResult != null)
                    {
                        if (rslt.dstResult.Tables.Count > 0)
                        {
                            if (rslt.dstResult.Tables[0].Rows.Count > 0)
                            {
                                DataBlockValues["W_GOVT_ALL"] = rslt.dstResult.Tables[0].Rows[0]["w_govt_all"];
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        void Proc96()
        {
            try
            {
                dicInputParameters.Clear();
                dicInputParameters.Add("pPR_P_NO", DataBlockValues["PR_P_NO"]);
                dicInputParameters.Add("pW_DATE", DataBlockValues["W_DATE"]);

                Result rslt = objCmnDataManager.GetData("CHRIS_SP_UNION_ARREARS_PROCESSING_PROC96", "", dicInputParameters);

                if (rslt.isSuccessful)
                {
                    if (rslt.dstResult != null)
                    {
                        if (rslt.dstResult.Tables.Count > 0)
                        {
                            if (rslt.dstResult.Tables[0].Rows.Count > 0)
                            {
                                DataBlockValues["W_INC_EFF"] = rslt.dstResult.Tables[0].Rows[0]["w_inc_eff"];
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        void Proc99()
        {
            try
            {
                dicInputParameters.Clear();
                dicInputParameters.Add("pPR_P_NO", DataBlockValues["PR_P_NO"]);
                dicInputParameters.Add("pW_DATE", DataBlockValues["W_DATE"]);
                dicInputParameters.Add("pW_TRANS_DATE", DataBlockValues["W_TRANS_DATE"]);
                dicInputParameters.Add("pW_BRANCH", DataBlockValues["W_BRANCH"]);
                dicInputParameters.Add("pW_DESIG", DataBlockValues["W_DESIG"]);
                dicInputParameters.Add("pW_LEVEL", DataBlockValues["W_LEVEL"]);
                dicInputParameters.Add("pW_CATE", DataBlockValues["W_CATE"]);
                dicInputParameters.Add("pW_AN_PACK", DataBlockValues["W_AN_PACK"]);
                dicInputParameters.Add("pW_VP", DataBlockValues["W_VP"]);
                dicInputParameters.Add("pW_ZAKAT", DataBlockValues["W_ZAKAT"]);
                dicInputParameters.Add("pW_PRV_INC", DataBlockValues["W_PRV_INC"]);
                dicInputParameters.Add("pW_TAX_USED", DataBlockValues["W_TAX_USED"]);
                dicInputParameters.Add("pW_PRV_TAX_PD", DataBlockValues["W_PRV_TAX_PD"]);
                dicInputParameters.Add("pW_PR_DATE", DataBlockValues["W_PR_DATE"]);

                dicInputParameters.Add("oW_OT_ASR", DataBlockValues["W_OT_ASR"]);
                dicInputParameters.Add("oW_GOVT_ALL", DataBlockValues["W_GOVT_ALL"]);
                dicInputParameters.Add("oW_P_BASIC", DataBlockValues["W_P_BASIC"]);
                dicInputParameters.Add("oW_ITAX", DataBlockValues["W_ITAX"]);
                dicInputParameters.Add("oW_ANNUAL_TAX", DataBlockValues["W_ANNUAL_TAX"]);
                dicInputParameters.Add("oW_BASIC_AREAR", DataBlockValues["W_BASIC_AREAR"]);
                dicInputParameters.Add("oW_SAL_AREAR", DataBlockValues["W_SAL_AREAR"]);
                dicInputParameters.Add("oW_FORCAST", DataBlockValues["W_FORCAST"]);

                Result rslt = objCmnDataManager.GetData("CHRIS_SP_UNION_ARREARS_PROCESSING_PROC99", "", dicInputParameters);

                if (rslt.isSuccessful)
                {
                    if (rslt.dstResult != null)
                    {
                        if (rslt.dstResult.Tables.Count > 0)
                        {
                            if (rslt.dstResult.Tables[0].Rows.Count > 0)
                            {
                                DataBlockValues["W_DATE1"] = rslt.dstResult.Tables[0].Rows[0]["W_DATE1"];
                                DataBlockValues["GLOBAL_W_10_AMT"] = rslt.dstResult.Tables[0].Rows[0]["GLOBAL_W_10_AMT"];
                                DataBlockValues["GLOBAL_W_INC_AMT"] = rslt.dstResult.Tables[0].Rows[0]["GLOBAL_W_INC_AMT"];
                                DataBlockValues["W_SAL_AREAR"] = rslt.dstResult.Tables[0].Rows[0]["W_SAL_AREAR"];
                                DataBlockValues["GLOBAL_FLAG"] = rslt.dstResult.Tables[0].Rows[0]["GLOBAL_FLAG"];
                                DataBlockValues["W_OT_ASR"] = rslt.dstResult.Tables[0].Rows[0]["W_OT_ASR"];
                                DataBlockValues["W_PROMOTED"] = rslt.dstResult.Tables[0].Rows[0]["W_PROMOTED"];
                                DataBlockValues["W_PR_DATE"] = rslt.dstResult.Tables[0].Rows[0]["W_PR_DATE"];
                                DataBlockValues["W_FORCAST_OT"] = rslt.dstResult.Tables[0].Rows[0]["W_FORCAST_OT"];
                                DataBlockValues["W_ACTUAL_OT"] = rslt.dstResult.Tables[0].Rows[0]["W_ACTUAL_OT"];
                                DataBlockValues["W_AVG"] = rslt.dstResult.Tables[0].Rows[0]["W_AVG"];
                                DataBlockValues["W_AREARS"] = rslt.dstResult.Tables[0].Rows[0]["W_AREARS"];
                                DataBlockValues["W_OT_MONTHS"] = rslt.dstResult.Tables[0].Rows[0]["W_OT_MONTHS"];
                                DataBlockValues["W_FORCAST_MONTHS"] = rslt.dstResult.Tables[0].Rows[0]["W_FORCAST_MONTHS"];
                                DataBlockValues["W_FORCAST"] = rslt.dstResult.Tables[0].Rows[0]["W_FORCAST"];
                                DataBlockValues["W_P_BASIC"] = rslt.dstResult.Tables[0].Rows[0]["W_P_BASIC"];
                                DataBlockValues["W_OTHER_ALL"] = rslt.dstResult.Tables[0].Rows[0]["W_OTHER_ALL"];
                                DataBlockValues["W_BASIC_AREAR"] = rslt.dstResult.Tables[0].Rows[0]["W_BASIC_AREAR"];
                                DataBlockValues["W_ANNUAL_BASIC"] = rslt.dstResult.Tables[0].Rows[0]["W_ANNUAL_BASIC"];
                                DataBlockValues["W_TAX_PACK"] = rslt.dstResult.Tables[0].Rows[0]["W_TAX_PACK"];
                                DataBlockValues["W_10_BONUS"] = rslt.dstResult.Tables[0].Rows[0]["W_10_BONUS"];
                                DataBlockValues["W_VP"] = rslt.dstResult.Tables[0].Rows[0]["W_VP"];
                                DataBlockValues["W_INCOME"] = rslt.dstResult.Tables[0].Rows[0]["W_INCOME"];
                                DataBlockValues["W_TAXABLE_INC"] = rslt.dstResult.Tables[0].Rows[0]["W_TAXABLE_INC"];
                                DataBlockValues["W_ANNUAL_TAX"] = rslt.dstResult.Tables[0].Rows[0]["W_ANNUAL_TAX"];
                                DataBlockValues["W_TAX_REC"] = rslt.dstResult.Tables[0].Rows[0]["W_TAX_REC"];
                                DataBlockValues["W_ITAX"] = rslt.dstResult.Tables[0].Rows[0]["W_ITAX"];
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        void Proc9()
        {
            try
            {
                dicInputParameters.Clear();
                dicInputParameters.Add("pPR_P_NO", DataBlockValues["PR_P_NO"]);

                Result rslt = objCmnDataManager.Execute("CHRIS_SP_UNION_ARREARS_PROCESSING_PROC9", "", dicInputParameters);

                if (rslt.isSuccessful)
                {
                    if (rslt.dstResult != null)
                    {
                        //Do not return anything
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        void UpdateSalArear()
        {
            try
            {
                dicInputParameters.Clear();
                dicInputParameters.Add("pPR_P_NO", DataBlockValues["PR_P_NO"]);
                dicInputParameters.Add("pW_FROM", DataBlockValues["W_FROM"]);
                dicInputParameters.Add("pW_TAX1", DataBlockValues["W_TAX1"]);
                dicInputParameters.Add("pW_TAX2", DataBlockValues["W_TAX2"]);
                dicInputParameters.Add("pW_JOINING", DataBlockValues["W_JOINING"]);
                dicInputParameters.Add("pW_DATE", DataBlockValues["W_DATE"]);
                dicInputParameters.Add("pW_CONF", DataBlockValues["W_CONF"]);
                dicInputParameters.Add("pW_PR_DATE", DataBlockValues["W_PR_DATE"]);
                dicInputParameters.Add("pW_PROMOTED", DataBlockValues["W_PROMOTED"]);
                dicInputParameters.Add("pW_NEW_LFA", DataBlockValues["W_NEW_LFA"]);
                dicInputParameters.Add("pW_NEW_MED", DataBlockValues["W_NEW_MED"]);
                dicInputParameters.Add("pW_LFA", DataBlockValues["W_LFA"]);
                dicInputParameters.Add("pW_MED", DataBlockValues["W_MED"]);

                Result rslt = objCmnDataManager.Execute("CHRIS_SP_UNION_ARREARS_PROCESSING_UpdateSalArear", "", dicInputParameters);

                if (rslt.isSuccessful)
                {
                    if (rslt.dstResult != null)
                    {

                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion


        [HttpPost]
        public JsonResult UpsertSalaryEntry(DateTime? W_FROM, DateTime? W_TO, string W_BRN, string W_MOLD, string W_COLD,string W_LFA, string W_MED, string W_MEAL_AMT,string W_CONV_AMT, string W_NEW_LFA, string W_NEW_MED, string W_ANS, string PR_P_NO)
        {
            try
            {
                Dictionary<string, object> d = new Dictionary<string, object>();

                d.Add("W_FROM", W_FROM);
                d.Add("W_TO", W_TO);
                d.Add("W_BRN", W_BRN);
                d.Add("W_MOLD", W_MOLD);
                d.Add("W_COLD", W_COLD);
                d.Add("W_LFA", W_LFA);
                d.Add("W_MED", W_MED);
                d.Add("W_MEAL_AMT", W_MEAL_AMT);
                d.Add("W_CONV_AMT", W_CONV_AMT);
                d.Add("W_NEW_LFA", W_NEW_LFA);
                d.Add("W_NEW_MED", W_NEW_MED);
                d.Add("W_ANS", W_ANS);
                d.Add("PR_P_NO", PR_P_NO);               

                if (W_ANS.ToUpper() == "Y")
                {
                    InitializeDic();
                    DataBlockValues["W_FROM"] = W_FROM;
                    DataBlockValues["W_TO"] = W_TO;
                    DataBlockValues["W_BRN"] = W_BRN;

                    DataBlockValues["W_MOLD"] = W_MOLD;
                    DataBlockValues["W_COLD"] = W_COLD;

                    DataBlockValues["W_CONV_AMT"] = W_CONV_AMT;
                    DataBlockValues["W_MEAL_AMT"] = W_MEAL_AMT;

                    DataBlockValues["W_LFA"] = W_LFA;
                    DataBlockValues["W_NEW_LFA"] = W_NEW_LFA;

                    DataBlockValues["W_MED"] = W_MED;
                    DataBlockValues["W_NEW_MED"] = W_NEW_MED;

                    DataBlockValues["W_DATE"] = DateTime.Now.ToShortDateString;
                    DataBlockValues["W_ANS"] = W_ANS;

                    DataBlockValues["W_VP"] = 3600;

                    try
                    {
                        Proc1();
                       
                        return Json("Process Complete");
                    }
                    catch (Exception ex)
                    {
                        return Json(null);
                    }

                }

                return Json(null);
            }
            catch (Exception ex)
            {
                return Json("Something went wrong");
            }
        }



        #endregion
        public IActionResult ShiftEntry()
        {
            return View();
        }

        #region IndividualAllowanceEntry

        public IActionResult IndividualAllowanceEntry()
        {
            return View();
        }
        public IActionResult btn_SP_ALL_CODE_modal()
        {
            Dictionary<string, object> d = new Dictionary<string, object>();

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_Sp_ALLOWANCE_MANAGER", "List", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        public IActionResult tbl_IndividualAllowanceEntry(string SP_ALL_CODE)
        {
            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("SP_ALL_CODE", SP_ALL_CODE);
            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_Sp_ALLOWANCE_DETAILS_MANAGER", "List", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        public IActionResult btn_SP_ACOUNT_NO_Modal()
        {
            Dictionary<string, object> d = new Dictionary<string, object>();
           
            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_Sp_ALLOWANCE_MANAGER", "GetAccountLov", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        public IActionResult btn_SP_P_NO_Modal1()
        {
            Dictionary<string, object> d = new Dictionary<string, object>();

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_Sp_ALLOWANCE_DETAILS_MANAGER", "GetPr_P_NoLov", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        [HttpPost]
        public JsonResult UpsertIndividualAllowanceEntry(string SP_ALL_CODE, string SP_BRANCH_A, string SP_DESC, string SP_CATEGORY_A, string SP_DESG_A, string SP_LEVEL_A, DateTime? SP_VALID_FROM, DateTime? SP_VALID_TO, Decimal? SP_ALL_AMOUNT, Decimal? SP_PER_ASR, string SP_TAX, string SP_ATT_RELATED, string SP_RELATED_LEAV, string SP_ASR_BASIC, string SP_MEAL_CONV, string SP_ALL_IND, string SP_ACOUNT_NO, string SP_FORECAST_TAX, int? ID)
        {
            try
            {
                Dictionary<string, object> d = new Dictionary<string, object>();


             d.Add("SP_ALL_CODE", SP_ALL_CODE);   
	         d.Add("SP_BRANCH_A", SP_BRANCH_A);
	         d.Add("SP_DESC", SP_DESC);
	         d.Add("SP_CATEGORY_A", SP_CATEGORY_A);
	         d.Add("SP_DESG_A", SP_DESG_A);
	         d.Add("SP_LEVEL_A", SP_LEVEL_A);
	         d.Add("SP_VALID_FROM", SP_VALID_FROM);
	         d.Add("SP_VALID_TO", SP_VALID_TO);
	         d.Add("SP_ALL_AMOUNT", SP_ALL_AMOUNT);
	         d.Add("SP_PER_ASR", SP_PER_ASR);
	         d.Add("SP_TAX", SP_TAX);
	         d.Add("SP_ATT_RELATED", SP_ATT_RELATED);
	         d.Add("SP_RELATED_LEAV", SP_RELATED_LEAV);
	         d.Add("SP_ASR_BASIC", SP_ASR_BASIC);
	         d.Add("SP_MEAL_CONV", SP_MEAL_CONV);
	         d.Add("SP_ALL_IND", SP_ALL_IND);
	         d.Add("SP_ACOUNT_NO", SP_ACOUNT_NO);
             d.Add("SP_FORECAST_TAX", SP_FORECAST_TAX);
             d.Add("ID", ID);

                CmnDataManager objCmnDataManager = new CmnDataManager();

                if (ID > 0)
                {
                    Result rsltCode = objCmnDataManager.GetData("CHRIS_Sp_ALLOWANCE_MANAGER", "Update", d);
                    if (rsltCode.isSuccessful)
                    {
                        var res = Json(rsltCode);
                        return Json("Your request is modify successfully !");
                    }
                }
                else
                {
                    Result rsltCode = objCmnDataManager.GetData("CHRIS_Sp_ALLOWANCE_MANAGER", "Save", d);
                    if (rsltCode.isSuccessful)
                    {
                        var res = Json(rsltCode);
                        return Json("Your request is successfully completed !");
                    }
                }

                return Json(null);
            }
            catch (Exception ex)
            {
                return Json("Something went wrong");
            }
        }

        [HttpPost]
        public JsonResult UpsertIndividualAllowanceEntry1(string SP_ALL_CODE, Decimal? SP_P_NO, string SP_REMARKS, Decimal? SP_ALL_AMT, int? ID)
        {
            try
            {
                Dictionary<string, object> d = new Dictionary<string, object>();


                d.Add("SP_ALL_CODE", SP_ALL_CODE);
	   			d.Add("SP_P_NO", SP_P_NO);
	   			d.Add("SP_REMARKS", SP_REMARKS);
                d.Add("SP_ALL_AMT", SP_ALL_AMT);
                d.Add("ID", ID);

                CmnDataManager objCmnDataManager = new CmnDataManager();

                if (ID > 0)
                {
                    Result rsltCode = objCmnDataManager.GetData("CHRIS_Sp_ALLOWANCE_DETAILS_MANAGER", "Update", d);
                    if (rsltCode.isSuccessful)
                    {
                        var res = Json(rsltCode);
                        return Json("Your request is modify successfully !");
                    }
                }
                else
                {
                    Result rsltCode = objCmnDataManager.GetData("CHRIS_Sp_ALLOWANCE_DETAILS_MANAGER", "Save", d);
                    if (rsltCode.isSuccessful)
                    {
                        var res = Json(rsltCode);
                        return Json("Your request is successfully completed !");
                    }
                }

                return Json(null);
            }
            catch (Exception ex)
            {
                return Json("Something went wrong");
            }
        }

        //public IActionResult DeleteIndividualAllowanceEntry(decimal? PR_P_NO)
        //{
        //    Dictionary<string, object> d = new Dictionary<string, object>();
        //    d.Add("PR_P_NO", PR_P_NO);
        //    CmnDataManager objCmnDataManager = new CmnDataManager();
        //    Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_ZAKAT_MANAGER", "Delete", d);
        //    if (rsltCode.isSuccessful)
        //    {
        //        return Json("Record delete successfully !");
        //    }
        //    return Json(null);
        //}

        #endregion
       
        #region IndividualsDeductionEntry

        public IActionResult IndividualsDeductionEntry()
        {
            return View();
        }
        public IActionResult btn_SP_ALL_CODE_Modal_D()
        {
            Dictionary<string, object> d = new Dictionary<string, object>();

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_DEDUCTION_MANAGER", "ValidDeductionCodeLov", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        public IActionResult tbl_IndividualsDeductionEntry(string SP_DED_CODE)
        {
            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("SP_DED_CODE", SP_DED_CODE);
            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_DEDUCTION_DETAILS_MANAGER", "List", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        public IActionResult btn_SP_ACOUNT_NO_Modal_D()
        {
            Dictionary<string, object> d = new Dictionary<string, object>();

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_DEDUCTION_MANAGER", "ValidAccountNumberLov", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        public IActionResult btn_SP_P_NO_D_Modal()
        {
            Dictionary<string, object> d = new Dictionary<string, object>();

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_DEDUCTION_DETAILS_MANAGER", "ValidPersonnelNumbersLOV", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        [HttpPost]
        public JsonResult UpsertIndividualsDeductionEntry(string SP_DED_CODE, string SP_BRANCH_D, string SP_DED_DESC, string SP_CATEGORY_D, string SP_DESG_D, string SP_LEVEL_D, DateTime? SP_VALID_FROM_D, DateTime? SP_VALID_TO_D, Decimal? SP_DED_AMOUNT, Decimal? SP_DED_PER, string SP_ALL_IND, string SP_ACOUNT_NO_D, int? ID)
        {
            try
            {
                Dictionary<string, object> d = new Dictionary<string, object>();

                
                d.Add("SP_DED_CODE", SP_DED_CODE);   
	            d.Add("SP_BRANCH_D", SP_BRANCH_D);
	            d.Add("SP_DED_DESC", SP_DED_DESC);
	            d.Add("SP_CATEGORY_D", SP_CATEGORY_D);
	            d.Add("SP_DESG_D", SP_DESG_D);
	            d.Add("SP_LEVEL_D", SP_LEVEL_D);
	            d.Add("SP_VALID_FROM_D", SP_VALID_FROM_D);
	            d.Add("SP_VALID_TO_D", SP_VALID_TO_D);
	            d.Add("SP_DED_AMOUNT", SP_DED_AMOUNT);
	            d.Add("SP_DED_PER", SP_DED_PER);
	            d.Add("SP_ALL_IND", SP_ALL_IND);
                d.Add("SP_ACOUNT_NO_D", SP_ACOUNT_NO_D);
                d.Add("ID", ID);

                CmnDataManager objCmnDataManager = new CmnDataManager();

                if (ID > 0)
                {
                    Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_DEDUCTION_MANAGER", "Update", d);
                    if (rsltCode.isSuccessful)
                    {
                        var res = Json(rsltCode);
                        return Json("Your request is modify successfully !");
                    }
                }
                else
                {
                    Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_DEDUCTION_MANAGER", "Save", d);
                    if (rsltCode.isSuccessful)
                    {
                        var res = Json(rsltCode);
                        return Json("Your request is successfully completed !");
                    }
                }
                return Json(null);
            }
            catch (Exception ex)
            {
                return Json("Something went wrong");
            }
        }

        [HttpPost]
        public JsonResult UpsertIndividualsDeductionEntry1(string SP_DED_CODE, Decimal? SP_P_NO, string SP_REMARKS, Decimal? SP_DED_AMT, int? ID)
        {
            try
            {
                Dictionary<string, object> d = new Dictionary<string, object>();


                d.Add("SP_DED_CODE", SP_DED_CODE);
                d.Add("SP_P_NO", SP_P_NO);
                d.Add("SP_REMARKS", SP_REMARKS);
                d.Add("SP_DED_AMT", SP_DED_AMT);
                d.Add("ID", ID);

                CmnDataManager objCmnDataManager = new CmnDataManager();

                if (ID > 0)
                {
                    Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_DEDUCTION_DETAILS_MANAGER", "Update", d);
                    if (rsltCode.isSuccessful)
                    {
                        var res = Json(rsltCode);
                        return Json("Your request is modify successfully !");
                    }
                }
                else
                {
                    Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_DEDUCTION_DETAILS_MANAGER", "Save", d);
                    if (rsltCode.isSuccessful)
                    {
                        var res = Json(rsltCode);
                        return Json("Your request is successfully completed !");
                    }
                }

                return Json(null);
            }
            catch (Exception ex)
            {
                return Json("Something went wrong");
            }
        }

        public IActionResult DeleteIndividualsDeductionEntry_ID(int? ID)
        {
            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("ID", ID);
            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_DEDUCTION_MANAGER", "Delete", d);
            if (rsltCode.isSuccessful)
            {
                return Json("Record delete successfully !");
            }
            return Json(null);
        }

        public IActionResult DeleteIndividualsDeductionEntry_ID_D(int? ID)
        {
            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("ID", ID);
            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_DEDUCTION_DETAILS_MANAGER", "Delete", d);
            if (rsltCode.isSuccessful)
            {
                return Json("Record delete successfully !");
            }
            return Json(null);
        }

        #endregion

        #region FTEEntry

        public IActionResult FTEEntry()
        {
            return View();
        }
        public IActionResult btn_PR_P_NO_FTEEntry_Modal()
        {
            Dictionary<string, object> d = new Dictionary<string, object>();

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_PERSONNEL_ZAKAT_PERSONNEL_MANAGER", "FTEPersonnelLov", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        public IActionResult tbl_FTEEntry(Decimal? PR_P_NO, DateTime? PR_TRANSFER_DATE)
        {
            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("PR_P_NO", PR_P_NO);
            d.Add("PR_TRANSFER_DATE", PR_TRANSFER_DATE);
            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_DEPT_FTE_MANAGER", "List", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        public IActionResult btn_DP_DEPT_Modal(string DP_SEG)
        {
            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("DP_SEG", DP_SEG);
            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_DEPT_FTE_MANAGER", "DeptLOV", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        [HttpPost]
        public JsonResult UpsertFTEEntry1(Decimal? PR_P_NO, string DP_SEG, string DP_DEPT, Decimal? DP_HC, Decimal? DP_FTE, int? ID)
        {
            try
            {
                Dictionary<string, object> d = new Dictionary<string, object>();

                d.Add("PR_P_NO", PR_P_NO);
                d.Add("DP_SEG ", DP_SEG);
                d.Add("DP_DEPT", DP_DEPT);
                d.Add("DP_HC", DP_HC);
                d.Add("DP_FTE", DP_FTE);
                d.Add("ID", ID);

                CmnDataManager objCmnDataManager = new CmnDataManager();

                if (ID > 0)
                {
                    Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_DEPT_FTE_MANAGER", "Update", d);
                    if (rsltCode.isSuccessful)
                    {
                        var res = Json(rsltCode);
                        return Json("Your request is modify successfully !");
                    }
                }
                else
                {
                    Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_DEPT_FTE_MANAGER", "Save", d);
                    if (rsltCode.isSuccessful)
                    {
                        var res = Json(rsltCode);
                        return Json("Your request is successfully completed !");
                    }
                }
                return Json(null);
            }
            catch (Exception ex)
            {
                return Json("Something went wrong");
            }
        }

        public IActionResult DeleteFTEEntry_ID(int? ID)
        {
            if (ID>0)
            {
                Dictionary<string, object> d = new Dictionary<string, object>();
                d.Add("ID", ID);
                CmnDataManager objCmnDataManager = new CmnDataManager();
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_DEPT_FTE_MANAGER", "Delete", d);
                if (rsltCode.isSuccessful)
                {
                    return Json("Record delete successfully !");
                }
            }
            else
            {
                return Json("Record Not Delete !");
            }
            
            return Json(null);
        }

        #region FTE Entry Processing Dialog

        #region Variables
        Double GLOBAL_PNO;
        string BLK_HEAD_W_DESIG;
        DateTime BLK_HEAD_W_TRANSFER;
        string GLOBAL_BRN;
        string GLOBAL_CAT;
        DateTime PR_PROMOTION_DATE;
        string Re_Message;

        #endregion

        #region Methods
               
        void DeleteTempTab(string MonthYear, string EmployeeType)
        {
            //To store input parameters of StoredProcedure
            Dictionary<string, object> dicInputParameters = new Dictionary<string, object>();
            dicInputParameters.Add("W_MY1", MonthYear);
            dicInputParameters.Add("W_OPT", EmployeeType);

            try
            {
                //Get detail of particular personnel
                CmnDataManager objCmnDataManager = new CmnDataManager();
                Result rslt = objCmnDataManager.Execute("CHRIS_SP_FTE_MONTH_SUMMARY_UPDATION", "DeleteTempTab", dicInputParameters);

            }
            catch (Exception ex)
            {
                Re_Message = ex.ToString();
            }
        }
        void Update_AllEmployees(string MonthYear, string EmployeeType)
        {
            try
            {
                PROC_3(MonthYear, EmployeeType);
                PROC_8(MonthYear, EmployeeType);
                PROC_9(MonthYear, EmployeeType);
                PROC_7(MonthYear, EmployeeType);
            }
            catch (Exception ex)
            {
                Re_Message = ex.ToString();
            }
        }
        void Update_RegularStaffOnly(string MonthYear, string EmployeeType)
        {
            try
            {
                PROC_3(MonthYear, EmployeeType);
                PROC_7(MonthYear, EmployeeType);               
            }
            catch (Exception ex)
            {
                Re_Message = ex.ToString();
            }
        }
        void Update_InternsOnly(string MonthYear, string EmployeeType)
        {
            try
            {
                PROC_8(MonthYear, EmployeeType);
                PROC_7(MonthYear, EmployeeType);               
            }
            catch (Exception ex)
            {
                Re_Message = ex.ToString();
            }
        }
        void Update_ContractualStaffOnly(string MonthYear, string EmployeeType)
        {
            try
            {
                PROC_9(MonthYear, EmployeeType);
                PROC_7(MonthYear, EmployeeType);
            }
            catch (Exception ex)
            {
                Re_Message = ex.ToString();
            }
        }

        #region PROC
        void PROC_3(string MonthYear, string EmployeeType)
        {
            try
            {
                //To store input parameters of StoredProcedure
                Dictionary<string, object> dicInputParameters = new Dictionary<string, object>();
                dicInputParameters.Add("W_MY", MonthYear);

                //Get detail of particular personnel
                CmnDataManager objCmnDataManager = new CmnDataManager();
                Result rslt = objCmnDataManager.GetData("CHRIS_SP_FTE_PROC_3", " ", dicInputParameters);

                if (rslt.isSuccessful)
                {
                    if (rslt.dstResult.Tables.Count > 0)
                    {
                        if (rslt.dstResult.Tables[0].Rows.Count > 0)
                        {
                            for (int i = 0; i < rslt.dstResult.Tables[0].Rows.Count; i++)
                            {
                                #region Get data
                                if (rslt.dstResult.Tables[0].Rows[i]["PR_P_NO"] != null)
                                {
                                    GLOBAL_PNO = Double.Parse(rslt.dstResult.Tables[0].Rows[i]["PR_P_NO"].ToString());
                                    // txtEmployeeNo.Text = rslt.dstResult.Tables[0].Rows[i]["PR_P_NO"].ToString();

                                }
                                if (rslt.dstResult.Tables[0].Rows[i]["PR_DESIG"] != null)
                                {
                                    BLK_HEAD_W_DESIG = rslt.dstResult.Tables[0].Rows[i]["PR_DESIG"].ToString();
                                }
                                if ((rslt.dstResult.Tables[0].Rows[i]["PR_TRANSFER_DATE"] != System.DBNull.Value) && (!String.IsNullOrEmpty(rslt.dstResult.Tables[0].Rows[i]["PR_TRANSFER_DATE"].ToString())))
                                {
                                    BLK_HEAD_W_TRANSFER = DateTime.Parse(rslt.dstResult.Tables[0].Rows[i]["PR_TRANSFER_DATE"].ToString());
                                }
                                if (rslt.dstResult.Tables[0].Rows[i]["PR_NEW_BRANCH"] != null)
                                {
                                    GLOBAL_BRN = rslt.dstResult.Tables[0].Rows[i]["PR_NEW_BRANCH"].ToString();
                                }
                                if (rslt.dstResult.Tables[0].Rows[i]["PR_CATEGORY"] != null)
                                {
                                    GLOBAL_CAT = rslt.dstResult.Tables[0].Rows[i]["PR_CATEGORY"].ToString();
                                }
                                if ((rslt.dstResult.Tables[0].Rows[i]["PR_PROMOTION_DATE"] != System.DBNull.Value) && (!String.IsNullOrEmpty(rslt.dstResult.Tables[0].Rows[i]["PR_PROMOTION_DATE"].ToString())))
                                {
                                    PR_PROMOTION_DATE = DateTime.Parse(rslt.dstResult.Tables[0].Rows[i]["PR_PROMOTION_DATE"].ToString());
                                    PROC_4(MonthYear, EmployeeType);
                                }
                                PROC_5(MonthYear, EmployeeType);

                                #endregion
                            }

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Re_Message = ex.ToString();
            }
        }
        void PROC_4(string MonthYear, string EmployeeType)
        {
            try
            {
                //To store input parameters of StoredProcedure
                Dictionary<string, object> dicInputParameters = new Dictionary<string, object>();
                dicInputParameters.Add("W_MY", MonthYear);
                dicInputParameters.Add("GLOBAL_PNO", (object)GLOBAL_PNO);

                //Get detail of particular personnel
                CmnDataManager objCmnDataManager = new CmnDataManager();
                Result rslt = objCmnDataManager.GetData("CHRIS_SP_FTE_PROC_4", " ", dicInputParameters);

                if (rslt.isSuccessful)
                {
                    if (rslt.dstResult.Tables.Count > 0)
                    {
                        if (rslt.dstResult.Tables[0].Rows.Count > 0)
                        {
                            if (rslt.dstResult.Tables[0].Rows[0]["BLK_HEAD_W_DESIG"] != null)
                            {
                                BLK_HEAD_W_DESIG = rslt.dstResult.Tables[0].Rows[0]["BLK_HEAD_W_DESIG"].ToString();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Re_Message = ex.ToString();
            }
        }
        void PROC_5(string MonthYear, string EmployeeType)
        {
            try
            {
                //To store input parameters of StoredProcedure
                Dictionary<string, object> dicInputParameters = new Dictionary<string, object>();
                dicInputParameters.Add("W_MY", MonthYear);
                dicInputParameters.Add("GLOBAL_PNO", (object)GLOBAL_PNO);
                dicInputParameters.Add("GLOBAL_BRN", (object)GLOBAL_BRN);
                dicInputParameters.Add("W_DESIG", (object)BLK_HEAD_W_DESIG);
                dicInputParameters.Add("GLOBAL_CAT", (object)GLOBAL_CAT);

                //Get detail of particular personnel
                CmnDataManager objCmnDataManager = new CmnDataManager();
                Result rslt = objCmnDataManager.GetData("CHRIS_SP_FTE_PROC_5", " ", dicInputParameters);
            }
            catch (Exception ex)
            {
                Re_Message = ex.ToString();
            }

        }
        void PROC_8(string MonthYear, string EmployeeType)
        {
            try
            {
                Dictionary<string, object> dicInputParameters = new Dictionary<string, object>();
                dicInputParameters.Add("W_MY", MonthYear);

                //Get detail of particular personnel
                CmnDataManager objCmnDataManager = new CmnDataManager();
                Result rslt = objCmnDataManager.GetData("CHRIS_SP_FTE_PROC_8", " ", dicInputParameters);

                if (rslt.isSuccessful)
                {
                    if (rslt.dstResult.Tables.Count > 0)
                    {
                        if (rslt.dstResult.Tables[0].Rows.Count > 0)
                        {
                            for (int i = 0; i < rslt.dstResult.Tables[0].Rows.Count; i++)
                            {
                                if (rslt.dstResult.Tables[0].Rows[i]["BLK_HEAD_W_DISP_PNO"] != null)
                                {
                                    EmployeeType = rslt.dstResult.Tables[0].Rows[i]["BLK_HEAD_W_DISP_PNO"].ToString();

                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Re_Message = ex.ToString();
            }
        }
        void PROC_9(string MonthYear, string EmployeeType)
        {
            try
            {
                Dictionary<string, object> dicInputParameters = new Dictionary<string, object>();
                dicInputParameters.Add("W_MY", MonthYear);

                //Get detail of particular personnel
                CmnDataManager objCmnDataManager = new CmnDataManager();
                Result rslt = objCmnDataManager.GetData("CHRIS_SP_FTE_PROC_9", " ", dicInputParameters);

                if (rslt.isSuccessful)
                {
                    if (rslt.dstResult.Tables.Count > 0)
                    {
                        if (rslt.dstResult.Tables[0].Rows.Count > 0)
                        {
                            for (int i = 0; i < rslt.dstResult.Tables[0].Rows.Count; i++)
                            {
                                if (rslt.dstResult.Tables[0].Rows[i]["BLK_HEAD_W_DISP_PNO"] != null)
                                {
                                    EmployeeType = rslt.dstResult.Tables[0].Rows[i]["BLK_HEAD_W_DISP_PNO"].ToString();

                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Re_Message = ex.ToString();
            }
        }
        void PROC_7(string MonthYear, string EmployeeType)
        {
            try
            {
                Dictionary<string, object> dicInputParameters = new Dictionary<string, object>();
                dicInputParameters.Add("W_MY", MonthYear);

                //Get detail of particular personnel
                CmnDataManager objCmnDataManager = new CmnDataManager();
                Result rslt = objCmnDataManager.GetData("CHRIS_SP_FTE_PROC_7", " ", dicInputParameters);
            }
            catch (Exception ex)
            {
                Re_Message = ex.ToString();
            }

        }
        #endregion

        #endregion

        public IActionResult Update_FTEEntry(string MonthYear,string EmployeeType)
        {
            if (MonthYear != "")
            {               
                    //To store input parameters of StoredProcedure
                    Dictionary<string, object> dicInputParameters = new Dictionary<string, object>();
                    dicInputParameters.Add("W_MY1", MonthYear);
                    dicInputParameters.Add("W_OPT", EmployeeType);

                    try
                    {
                        //Get detail of particular personnel
                        CmnDataManager objCmnDataManager = new CmnDataManager();
                        Result rslt = objCmnDataManager.GetData("CHRIS_SP_FTE_MONTH_SUMMARY_UPDATION", "UpdateMonthSummaryUpdation", dicInputParameters);

                        if (EmployeeType == "1")
                        {
                            Update_AllEmployees(MonthYear, EmployeeType);
                            DeleteTempTab(MonthYear, EmployeeType);
                        }
                        else if (EmployeeType == "2")
                        {
                            Update_RegularStaffOnly(MonthYear, EmployeeType);
                            DeleteTempTab(MonthYear, EmployeeType);
                        }
                        else if (EmployeeType == "3")
                        {
                            Update_InternsOnly(MonthYear, EmployeeType);
                            DeleteTempTab(MonthYear, EmployeeType);
                        }
                        else if (EmployeeType == "4")
                        {
                            Update_ContractualStaffOnly(MonthYear, EmployeeType);
                            DeleteTempTab(MonthYear, EmployeeType);
                        }


                    }
                    catch (Exception ex)
                    {
                        Re_Message = ex.ToString();
                        return Json(Re_Message);
                    }
                }

            Re_Message = "Process Complete...";
            return Json(Re_Message);
        }

        #endregion

        #endregion
        public IActionResult PR111()
        {
            return View();
        }

        #region CHRSP

        #region For Start btn

        private bool GenerateFile(string FilePath, DateTime? validate)
        {
            try
            {
                FilePath = FilePath;//"G:\fate.fate.txt._txt";
                DirectoryInfo info = new DirectoryInfo(FilePath);

                //Path.GetDirectoryName(filePath)
                if (info.Root.Exists)// (Directory.Exists(filePath))
                {
                    if (info.Extension != ".txt")
                    {
                        FilePath = Path.ChangeExtension(FilePath, ".txt");
                    }

                    
                    Result rslt = GetFileData(validate);

                    if (rslt.isSuccessful)
                    {
                        if (rslt.dstResult != null && rslt.dstResult.Tables.Count > 0 && rslt.dstResult.Tables[0].Rows.Count > 0)
                        {
                            TextWriter tw = new System.IO.StreamWriter(FilePath);
                            if (rslt.dstResult.Tables[0].Rows[0]["Header"] != null)
                                tw.WriteLine(rslt.dstResult.Tables[0].Rows[0]["Header"].ToString());

                            if (rslt.dstResult.Tables[1].Rows.Count > 0)
                            {
                                for (int i = 0; i < rslt.dstResult.Tables[1].Rows.Count; i++)
                                {
                                    if (rslt.dstResult.Tables[1].Rows[i]["B"] != null)
                                        tw.WriteLine(rslt.dstResult.Tables[1].Rows[i]["B"].ToString());
                                }
                            }
                            if (rslt.dstResult.Tables[2].Rows.Count > 0)
                            {
                                if (rslt.dstResult.Tables[2].Rows[0]["footer"] != null)
                                    tw.WriteLine(rslt.dstResult.Tables[2].Rows[0]["footer"].ToString());
                            }

                            tw.Close();
                            return true;
                        }
                    }

                    return true;
                }               
                else
                {
                    return false;                    
                }
            }
            catch (Exception ioExcep)
            {
                return false;
            }
        }

        private Result GetFileData(DateTime? validate)
        {        
            Result fileResult;
            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Clear();
            param.Add("VALDATE", validate);
            CmnDataManager objCmnDataManager = new CmnDataManager();
            fileResult = objCmnDataManager.GetData("CHRIS_SP_FILE_TABLE_CREATION", "", param);
            return fileResult;
        }

        #endregion
        public IActionResult CHRSP()
        {
            return View();
        }
        
        public JsonResult CHRSP_View(Decimal? TNO)
        {
            try
            {
                CmnDataManager objCmnDataManager = new CmnDataManager();
                Result rslt = objCmnDataManager.GetData("CHRIS_PR_VIEW_CHRSP", "");
                if (rslt.isSuccessful)
                {
                    if (rslt.dstResult != null && rslt.dstResult.Tables.Count > 0)
                    {
                        var CatResul1 = string.Empty;
                        var CatResul2 = string.Empty;                       
                        var CatResul3 = string.Empty;

                        if (TNO == 0)
                        {
                            if (rslt.dstResult.Tables[0].Rows.Count > 0)
                            {
                                DataTable firstTable = rslt.dstResult.Tables[0];
                                CatResul1 = JsonConvert.SerializeObject(firstTable);
                                return Json(CatResul1);
                            }
                        }
                       
                        if (TNO == 2)
                        {
                            if (rslt.dstResult.Tables[2].Rows.Count > 0)
                            {
                                //DataTable firstTable = rslt.dstResult.Tables[2];
                                //CatResul3 = JsonConvert.SerializeObject(firstTable);
                                CatResul3 = rslt.dstResult.Tables[2].Rows[0][0].ToString();
                                CatResul3 = JsonConvert.SerializeObject(CatResul3);
                                return Json(CatResul3);
                            }
                        }
                       
                        if (TNO == 1)
                        {
                            if (rslt.dstResult.Tables[1].Rows.Count > 0)
                            {
                                DataTable firstTable = rslt.dstResult.Tables[1];
                                CatResul2 = JsonConvert.SerializeObject(firstTable);
                                return Json(CatResul2);
                            }
                            else
                            {
                                string label_amt_tot = ".00";
                                string label_Amt1_tot = "000000000000000";
                                CatResul2 = "'" + label_amt_tot + "','" + label_Amt1_tot + "'";
                                CatResul2 = JsonConvert.SerializeObject(CatResul2);
                                return Json(CatResul2);
                            }
                        }
                                        
                    }
                }
            }
            catch 
            {
                return Json(null);

            }
            return Json(null);
        }

        public JsonResult CHRSP_Start(string FilePath, DateTime? validate)
        {
            try
            {
                if (FilePath.Trim() != string.Empty)
                {
                    bool fileGeneratedSuccessfully = false;
                    fileGeneratedSuccessfully = GenerateFile(FilePath,validate);
                    if (fileGeneratedSuccessfully)
                    {
                        return Json("File has been created succesfully on  " + FilePath + "  Path With Name");

                    }
                    else
                    {
                        return Json("Some Error Occured While Generating File.Please Try again Later.");
                    }
                }
                else
                {
                    return Json("Value Date or Path Must Be Entered");
                }
            }
            catch 
            {
                return Json("Some Error Occured While Generating File.Please Try again Later.");
            }           
        }

        #endregion

        public IActionResult PR008A()
        {
            return View();
        }
        
        #region SalaryUpdate

        public IActionResult SalaryUpdate()
        {
            return View();
        }
        public IActionResult btn_PR_P_NO_SalaryUpdate_Modal()
        {
            Dictionary<string, object> d = new Dictionary<string, object>();

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_PERSONNEL_MANAGER_FOR_SALARY_UPDATE", "List", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        public IActionResult btn_PR_CATEGORY_SalaryUpdate_Modal()
        {
            Dictionary<string, object> d = new Dictionary<string, object>();

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_PERSONNEL_MANAGER_FOR_SALARY_UPDATE", "PR_CATEGORY_LOV1", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        public IActionResult btn_PW_ALL_CODE_SalaryUpdate_Modal()
        {
            Dictionary<string, object> d = new Dictionary<string, object>();

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_PAYROLL_ALLOW_MANAGER_FOR_SALARY_UPDATE", "PW_ALL_CODE_LOV2", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        public IActionResult btn_PD_DED_CODE_SalaryUpdate_Modal()
        {
            Dictionary<string, object> d = new Dictionary<string, object>();

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_PAYROLL_DEDUC_MANAGER_FOR_SALARY", "PD_DED_CODE_LOV3", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        public IActionResult CHRIS_SP_PAYROLL_MANAGERFOR_SALARY_UPDATE(Decimal? PR_P_NO)
        {
            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("PR_P_NO", PR_P_NO);
            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_PAYROLL_MANAGERFOR_SALARY_UPDATE", "List", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        [HttpPost]
        public JsonResult UpsertSalaryUpdate(string SP_DED_CODE, string SP_BRANCH_D, string SP_DED_DESC, string SP_CATEGORY_D, string SP_DESG_D, string SP_LEVEL_D, DateTime? SP_VALID_FROM_D, DateTime? SP_VALID_TO_D, Decimal? SP_DED_AMOUNT, Decimal? SP_DED_PER, string SP_ALL_IND, string SP_ACOUNT_NO_D, int? ID)
        {
            try
            {
                Dictionary<string, object> d = new Dictionary<string, object>();


                d.Add("SP_DED_CODE", SP_DED_CODE);
                d.Add("SP_BRANCH_D", SP_BRANCH_D);
                d.Add("SP_DED_DESC", SP_DED_DESC);
                d.Add("SP_CATEGORY_D", SP_CATEGORY_D);
                d.Add("SP_DESG_D", SP_DESG_D);
                d.Add("SP_LEVEL_D", SP_LEVEL_D);
                d.Add("SP_VALID_FROM_D", SP_VALID_FROM_D);
                d.Add("SP_VALID_TO_D", SP_VALID_TO_D);
                d.Add("SP_DED_AMOUNT", SP_DED_AMOUNT);
                d.Add("SP_DED_PER", SP_DED_PER);
                d.Add("SP_ALL_IND", SP_ALL_IND);
                d.Add("SP_ACOUNT_NO_D", SP_ACOUNT_NO_D);
                d.Add("ID", ID);

                CmnDataManager objCmnDataManager = new CmnDataManager();

                if (ID > 0)
                {
                    Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_DEDUCTION_MANAGER", "Update", d);
                    if (rsltCode.isSuccessful)
                    {
                        var res = Json(rsltCode);
                        return Json("Your request is modify successfully !");
                    }
                }
                else
                {
                    Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_DEDUCTION_MANAGER", "Save", d);
                    if (rsltCode.isSuccessful)
                    {
                        var res = Json(rsltCode);
                        return Json("Your request is successfully completed !");
                    }
                }
                return Json(null);
            }
            catch (Exception ex)
            {
                return Json("Something went wrong");
            }
        }
        public IActionResult DeleteSalaryUpdate_ID(int? ID)
        {
            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("ID", ID);
            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_DEDUCTION_MANAGER", "Delete", d);
            if (rsltCode.isSuccessful)
            {
                return Json("Record delete successfully !");
            }
            return Json(null);
        }

        #endregion

        #region ArearUpdate
        public ActionResult ArearUpdate()
        {           
            return View();
        }
        public ActionResult tbl_ArearUpdate()
        {
            Dictionary<string, object> d = new Dictionary<string, object>();

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_SAL_AREAR_MANAGER", "EnterQuery", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        [HttpPost]
        public JsonResult UpsertArearUpdate(Decimal? SA_P_NO, DateTime? SA_DATE, Decimal? SA_SAL_AREARS, Decimal? SA_PF_AREARS, Decimal? SA_OT_AREARS, string SA_AREARS_APPROV, Decimal? SA_HOLDING_TAX, Decimal? SA_LFA, Decimal? SA_MEDICAL, int? ID)
        {
            try
            {
                Dictionary<string, object> d = new Dictionary<string, object>();

                d.Add("SA_P_NO", SA_P_NO); 
	   			d.Add("SA_DATE", SA_DATE);
	   			d.Add("SA_SAL_AREARS", SA_SAL_AREARS);
	   			d.Add("SA_PF_AREARS", SA_PF_AREARS);
	   			d.Add("SA_OT_AREARS", SA_OT_AREARS);
	   			d.Add("SA_AREARS_APPROV", SA_AREARS_APPROV);
	   			d.Add("SA_HOLDING_TAX", SA_HOLDING_TAX);
	   			d.Add("SA_LFA", SA_LFA);
                d.Add("SA_MEDICAL", SA_MEDICAL);
                d.Add("ID", ID);

                CmnDataManager objCmnDataManager = new CmnDataManager();
                if (ID > 0)
                {
                    Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_SAL_AREAR_MANAGER", "Update", d);
                    if (rsltCode.isSuccessful)
                    {
                        var res = Json(rsltCode);
                        return Json("Your request is modify successfully !");
                    }
                }
                else
                {
                    Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_SAL_AREAR_MANAGER", "Save", d);
                    if (rsltCode.isSuccessful)
                    {
                        var res = Json(rsltCode);
                        return Json("Your request is successfully completed !");
                    }
                }
                return Json(null);
            }
            catch (Exception ex)
            {
                return Json(null);
            }
        }
        public ActionResult Delete_ArearUpdate(int? ID)
        {
            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("ID", ID);
            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_SAL_AREAR_MANAGER", "Delete", d);
            if (rsltCode.isSuccessful)
            {
                var res = Json(rsltCode);
                return Json("Your request is successfully completed !");
            }
            return Json(null);
        }

        #endregion

        #region LoanUpdate
        public IActionResult LoanUpdate()
        {
            return View();
        }
        public ActionResult btn_PR_P_NO_LoanUpdate_Modal()
        {
            Dictionary<string, object> d = new Dictionary<string, object>();
            
            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_FIN_BOOKING_MPC_MANAGER_PR111", "FinNumLov", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        public ActionResult Monthly_Installment_Information(Decimal? PR_P_NO,string FN_TYPE,string FN_FINANCE_NO)
        {
            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("PR_P_NO", PR_P_NO);
            d.Add("FN_TYPE", FN_TYPE);
            d.Add("FN_FINANCE_NO", FN_FINANCE_NO);
            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_FN_MONTH_MANAGER_PR111", "List", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        [HttpPost]
        public JsonResult UpsertLoanUpdate(Decimal? FN_P_NO, string FN_BRANCH, string FN_TYPE, Decimal? FN_PAY_SCHED, Decimal? FN_FINANCE_NO, DateTime? FN_START_DATE, DateTime? FN_END_DATE, DateTime? FN_EXTRA_TIME, Decimal? FN_AMT_AVAILED, Decimal? FN_MONTHLY_DED, Decimal? FN_C_RATIO_PER, string FN_LIQUIDATE,int? ID)
        {
            try
            {
                Dictionary<string, object> d = new Dictionary<string, object>();

                d.Add("FN_P_NO", FN_P_NO);  
	            d.Add("FN_BRANCH", FN_BRANCH);
	            d.Add("FN_TYPE", FN_TYPE);
	            d.Add("FN_PAY_SCHED", FN_PAY_SCHED);
	            d.Add("FN_FINANCE_NO", FN_FINANCE_NO);
	            d.Add("FN_START_DATE", FN_START_DATE);
	            d.Add("FN_END_DATE", FN_END_DATE);
	            d.Add("FN_EXTRA_TIME", FN_EXTRA_TIME);
	            d.Add("FN_AMT_AVAILED", FN_AMT_AVAILED);
	            d.Add("FN_MONTHLY_DED", FN_MONTHLY_DED);
	            d.Add("FN_C_RATIO_PER", FN_C_RATIO_PER);
                d.Add("FN_LIQUIDATE", FN_LIQUIDATE);
                d.Add("ID", ID);

                CmnDataManager objCmnDataManager = new CmnDataManager();
                if (ID > 0)
                {
                    Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_FIN_BOOKING_MPC_MANAGER_PR111", "Update", d);
                    if (rsltCode.isSuccessful)
                    {
                        var res = Json(rsltCode);
                        return Json("Your request is modify successfully !");

                    }
                }
                else
                {
                    Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_FIN_BOOKING_MPC_MANAGER_PR111", "Save", d);
                    if (rsltCode.isSuccessful)
                    {
                        var res = Json(rsltCode);
                        return Json("Your request is successfully completed !");
                    }
                }
                return Json(null);
            }
            catch (Exception ex)
            {
                return Json(null);
            }
        }

        [HttpPost]
        public JsonResult UpsertLoanUpdate1(Decimal? FN_MP_NO, string FN_M_BRANCH, string FN_TYPE, string FN_FIN_NO, DateTime? FN_MDATE, Decimal? FN_DEBIT, Decimal? FN_CREDIT, Decimal? FN_PAY_LEFT, Decimal? FN_BALANCE, Decimal? FN_LOAN_BALANCE, Decimal? FN_MARKUP, string FN_LIQ_FLAG, string FN_SUBTYPE, int? ID)
        {
            try
            {
               Dictionary<string, object> d = new Dictionary<string, object>();

               d.Add("FN_MP_NO", FN_MP_NO); 
	           d.Add("FN_M_BRANCH", FN_M_BRANCH);
	           d.Add("FN_TYPE", FN_TYPE);
	           d.Add("FN_FIN_NO", FN_FIN_NO);
	           d.Add("FN_MDATE", FN_MDATE);
	           d.Add("FN_DEBIT", FN_DEBIT);
	           d.Add("FN_CREDIT", FN_CREDIT);
	           d.Add("FN_PAY_LEFT", FN_PAY_LEFT);
	           d.Add("FN_BALANCE", FN_BALANCE);
	           d.Add("FN_LOAN_BALANCE", FN_LOAN_BALANCE);
	           d.Add("FN_MARKUP", FN_MARKUP);
	           d.Add("FN_LIQ_FLAG", FN_LIQ_FLAG);
               d.Add("FN_SUBTYPE", FN_SUBTYPE);
                d.Add("ID", ID);

                CmnDataManager objCmnDataManager = new CmnDataManager();
                if (ID > 0)
                {
                    Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_FN_MONTH_MANAGER_PR111", "Update", d);
                    if (rsltCode.isSuccessful)
                    {
                        var res = Json(rsltCode);
                        return Json("Your request is modify successfully !");
                    }
                }
                else
                {
                    Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_FN_MONTH_MANAGER_PR111", "Save", d);
                    if (rsltCode.isSuccessful)
                    {
                        var res = Json(rsltCode);
                        return Json("Your request is successfully completed !");
                    }
                }
                return Json(null);
            }
            catch (Exception ex)
            {
                return Json(null);
            }
        }
        public ActionResult Delete_LoanUpdate(int? ID)
        {
            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("ID", ID);
            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_SAL_AREAR_MANAGER", "Delete", d);
            if (rsltCode.isSuccessful)
            {
                var res = Json(rsltCode);
                return Json("Your request is successfully completed !");
            }
            return Json(null);
        }

        #endregion              

        #region PR111_Serial_No
        public IActionResult PR111_Serial_No()
        {
            return View();
        }

        public JsonResult PR111_Serial_No1()
        {
            Dictionary<string, object> d = new Dictionary<string, object>();

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_SERIAL_NO_MANAGER", "List", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        [HttpPost]
        public JsonResult UpsertPR111_Serial_No(string PR_TYPE,Decimal? PR_SNO,int? ID)
        {
            try
            {
                Dictionary<string, object> d = new Dictionary<string, object>();

                d.Add("PR_TYPE", PR_TYPE);
                d.Add("PR_SNO", PR_SNO);
                d.Add("ID", ID);

                CmnDataManager objCmnDataManager = new CmnDataManager();
                if (ID > 0)
                {
                    Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_SERIAL_NO_MANAGER", "Update", d);
                    if (rsltCode.isSuccessful)
                    {
                        var res = Json(rsltCode);
                        return Json("Your request is modify successfully !");
                    }
                }
                else
                {
                    Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_SERIAL_NO_MANAGER", "Save", d);
                    if (rsltCode.isSuccessful)
                    {
                        var res = Json(rsltCode);
                        return Json("Your request is successfully completed !");
                    }
                }
                return Json(null);
            }
            catch (Exception ex)
            {
                return Json(null);
            }
        }

        #endregion

        #region View_Info
        public IActionResult View_Info()
        {
            return View();
        }

        public JsonResult View_Tax_Details1()
        {
            Dictionary<string, object> d = new Dictionary<string, object>();

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_PERSONNEL_MANAGER_FOR_SALARY_UPDATE", "List", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        public JsonResult View_Tax_Details(Decimal? PR_P_NO, DateTime? W_DATE, DateTime? W_JOINING_DATE, Decimal? W_AN_PACK, string W_BRANCH, string W_DESIG, string W_LEVEL, string W_CATE, Decimal? W_ZAKAT, Decimal? W_PRV_INC, string W_TAX_USED, Decimal? W_REFUND, Decimal? W_PRV_TAX_PD, Decimal? W_ITAX, Decimal? W_PF_EXEMPT, DateTime? W_SYS, Decimal? GLOBAL_W_10_AMT, Decimal? GLOBAL_W_INC_AMT, Decimal? W_VP, string SCREEN_TYPE)
        {
            Dictionary<string, object> d = new Dictionary<string, object>();

            #region  d_List
            d.Clear();

            d.Add("PR_P_NO", PR_P_NO);

            if (W_DATE != null)
            {
                d.Add("W_DATE", W_DATE);
            }
            else
            {
                W_DATE = DateTime.Now;
                d.Add("W_DATE", W_DATE);
            }

            d.Add("W_JOINING_DATE", W_JOINING_DATE);
            d.Add("W_AN_PACK", W_AN_PACK);
            d.Add("W_BRANCH", W_BRANCH);
            d.Add("W_DESIG", W_DESIG);
            d.Add("W_LEVEL", W_LEVEL);
            d.Add("W_CATE", W_CATE);
            d.Add("W_ZAKAT", W_ZAKAT);
            d.Add("W_PRV_INC", W_PRV_INC);
            d.Add("W_TAX_USED", W_TAX_USED);
            d.Add("W_REFUND", W_REFUND);
            d.Add("W_PRV_TAX_PD", W_PRV_TAX_PD);
            d.Add("W_ITAX", W_ITAX);
            d.Add("W_PF_EXEMPT", W_PF_EXEMPT);

            if (W_SYS != null)
            {
                d.Add("W_SYS", W_SYS);
            }
            else
            {
                W_SYS = DateTime.Now;
                d.Add("W_SYS", W_SYS);
            }

            d.Add("GLOBAL_W_10_AMT", GLOBAL_W_10_AMT);
            d.Add("GLOBAL_W_INC_AMT", GLOBAL_W_INC_AMT);
            d.Add("W_VP", W_VP);

            if (SCREEN_TYPE != null)
            {
                d.Add("SCREEN_TYPE", SCREEN_TYPE);
            }
            else
            {
                SCREEN_TYPE = "0";
                d.Add("SCREEN_TYPE", SCREEN_TYPE);
            }
            #endregion

            CmnDataManager objCmnDataManager = new CmnDataManager();
            SqlTransaction processTrans = null;
            Result rsltCode = objCmnDataManager.Execute("CHRIS_SP_PAYROLL_GENERATION_Z_TAX", "", d, ref processTrans, 60);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }
        #endregion
    }

}
