﻿using CHRIS.ReportModels;
using Microsoft.AspNetCore.Mvc;
using System.Net.Http.Headers;
using System.Text.Json;
using System.Text;
using CHRIS.Common;
using CHRIS.Services;
using Newtonsoft.Json;
using System.Data;
using System.Security.Cryptography.Xml;

namespace CHRIS.Controllers
{
    public class GratuityController : Controller
    {
        #region INDIVIDUAL GRATUITY REPORTS
        public IActionResult IndividualGratuityReports()
        {
            string message = "";
            try
            {

            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        [HttpPost]
        public IActionResult IndividualGratuityReports(string IGR_FROM_PNO, string IGR_TO_PNO, string IGR_BRANCH, string IGR_SEGMENT, string IGR_DEPARTMENT)
        {
            string message = "";
            try
            {
                HttpResponseMessage file = new HttpResponseMessage();
                GRR02Model entity = new GRR02Model();
                if (IGR_FROM_PNO != null)
                    entity.FPNO = Convert.ToInt32(IGR_FROM_PNO);
                else
                    entity.FPNO = null;
                if (IGR_TO_PNO != null)
                    entity.TPNO = Convert.ToInt32(IGR_TO_PNO);
                else
                    entity.TPNO = null;
                entity.Segment = IGR_SEGMENT;
                entity.Branch = IGR_BRANCH;
                entity.Department = IGR_DEPARTMENT;
                var DomainName = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetSection("CHRIS_REPORTING")["API_URL"];
                string BASE_URL = DomainName + "api/GratuityReporting/IndividualGratuityReports/";
                using (HttpClient clienFNREP08 = new HttpClient())
                {
                    using (HttpRequestMessage req310 = new HttpRequestMessage(HttpMethod.Post, BASE_URL))
                    {
                        req310.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        string dataAsJson = System.Text.Json.JsonSerializer.Serialize(entity,
                        new JsonSerializerOptions() { WriteIndented = false });
                        message += dataAsJson + Environment.NewLine;
                        var content = new StringContent(dataAsJson, Encoding.UTF8, "application/json");
                        req310.Content = content;
                        req310.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                        HttpResponseMessage response = clienFNREP08.SendAsync(req310).Result;
                        int StatusCode = (int)response.StatusCode;
                        string StatusDesc = response.StatusCode.ToString();
                        if (StatusCode == 200)
                        {
                            file = response;
                            return File(file.Content.ReadAsByteArrayAsync().Result, "application/octet-stream", "GRR02_" + DateTime.Now.ToString("dd_MM_yyyy") + ".pdf");
                        }
                        else
                        {
                            message = "Code : " + StatusCode + " | Description : " + StatusDesc;
                            return View();
                        }
                        //return responsse;
                    }
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        #endregion
        public IActionResult CHRIS_FINAL_SETTLEMENT_REPORT()
        {
            return View();
        }

        public IActionResult ActualGratuityReport()
        {
            return View();
        }
        public IActionResult GratuityLiabilityForTheYear()
        {
            return View();
        }
        public IActionResult GratuityLiabilityReport()
        {
            return View();
        }
        public IActionResult GratuityReservesReports()
        {
            return View();
        }

        #region HistoryUpdatingProcess

        public IActionResult HistoryUpdatingProcess()
        {
            return View();
        }

        public ActionResult HistoryUpdatingProcess1(string txtYesNo)
        {
            if ((txtYesNo != "Y") && (txtYesNo != "N"))
            {
                return Json("Enter [Y]es...or ..[N]o...");
            }
            else if (txtYesNo.ToUpper() == "Y")
            {
                try
                {
                    CmnDataManager objCmnDataManager = new CmnDataManager();
                    Result rsltCode = objCmnDataManager.Get("CHRIS_SP_GRATUITY_HISTORY_UPDATION_PROCESS", "");
                    if (rsltCode.isSuccessful)
                    {
                        return Json("Process Completed");
                    }
                }
                catch (Exception ex)
                {
                    return Json(ex.Message);
                }
            }
            return Json(null);
        }
       
        #endregion
        public IActionResult ResingesISAndOtherGratuity()
        {
            return View();
        }
        
    }
}
