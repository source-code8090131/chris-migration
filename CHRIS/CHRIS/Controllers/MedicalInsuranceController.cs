﻿using CHRIS.Common;
using CHRIS.Data;
using CHRIS.Services;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Data;
using System.Net.NetworkInformation;

namespace CHRIS.Controllers
{
    public class MedicalInsuranceController : Controller
    {
        public IActionResult CFEntry()
        {
            return View();
        }

        #region CFEntry_GHIDE03
        public IActionResult CFEntry_GHIDE03()
        {
            return View();
        }

        public JsonResult tbl_CFEntry_GHIDE03(string CLA_POLICY, Decimal? CLA_P_NO)
        {
            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("CLA_POLICY" , CLA_POLICY);
            d.Add("CLA_P_NO", CLA_P_NO);
            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_CLAIM_GHI_MANAGER", "List", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        public JsonResult btn_CLA_POLICY()
        {
            Dictionary<string, object> d = new Dictionary<string, object>();
          
            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_MedicalInsurance_ClaimEntry_Search_MANAGER", "POLICY", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        public JsonResult btn_CLA_P_NO()
        {
            Dictionary<string, object> d = new Dictionary<string, object>();

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_MedicalInsurance_ClaimEntry_Search_MANAGER", "PERSONNEL", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }


        [HttpPost]
        public JsonResult Upsert_CFEntry_GHIDE03(string CLA_POLICY,  Decimal? CLA_P_NO, Decimal? CLA_S_NO, DateTime? CLA_DATE, Decimal? CLA_CLAIMED_AMT,Decimal? CLA_REC_AMT, int ID)
        {
            try
            {
                Dictionary<string, object> d = new Dictionary<string, object>();

                d.Add("CLA_POLICY", CLA_POLICY);
                d.Add("CLA_P_NO",CLA_P_NO);
                d.Add("CLA_S_NO",CLA_S_NO);
                d.Add("CLA_DATE",CLA_DATE);
                d.Add("CLA_CLAIMED_AMT",CLA_CLAIMED_AMT);
                d.Add("CLA_REC_AMT",CLA_REC_AMT);
                d.Add("ID", ID);


                CmnDataManager objCmnDataManager = new CmnDataManager();
                if (ID > 0)
                {

                    Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_CLAIM_GHI_MANAGER", "Update", d);
                    if (rsltCode.isSuccessful)
                    {
                        var res = Json(rsltCode);
                        return Json("Your request is successfully Update !");
                    }
                }
                else
                {
                    Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_CLAIM_GHI_MANAGER", "Save", d);
                    if (rsltCode.isSuccessful)
                    {
                        var res = Json(rsltCode);
                        return Json("Your request is successfully Save !");
                    }
                }

                return Json(null);
            }
            catch
            {
                return Json(null);
            }
        }


        #endregion

        #region CFEntry_OMIDE03

        public IActionResult CFEntry_OMIDE03()
        {
            return View();
        }

        #region Variables

        CmnDataManager cmnDM = new CmnDataManager();


         string txt_W_OMI_LIMIT      = "";
         string txt_W_SELF           = "";
         string txt_W_SPOUSE         = "";
         string txt_W_CHILD          = "";
         string txt_PR_P_NO          = "";
         string txt_OMI_SELF         = "";
         string txt_OMI_SPOUSE       = "";
         string txt_OMI_CHILD        = "";
         string txt_W_OS_BALANCE     = "";
         string txt_W_STATUS         = "";
         string txt_OMI_CLAIM_NO     = "";
         string txt_OMI_TOTAL_CALIM1 = "";
        int s_no = 0;
        #endregion

        public JsonResult btn_OMI_P_NO_CFEnt_OMIDE03()
        {
            Dictionary<string, object> d = new Dictionary<string, object>();
            

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_MedicalInsurance_CFEntryOMI_CLAIM_MANAGER", "PERSONNEL_LOV", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        public JsonResult Tbl_Dept_CFEnt_OMIDE03(Decimal? PR_P_NO,DateTime? pr_transfer_date)
        {
            Dictionary<string, object> d = new Dictionary<string, object>();

            d.Add("PR_P_NO", PR_P_NO);
            d.Add("pr_transfer_date", pr_transfer_date);

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_Fin_Liquidate_DEPT_CONT_MANAGER", "List", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        public JsonResult btn_MONTHYEAR_LOV(string SEARCHFILTER)
        {
            Dictionary<string, object> d = new Dictionary<string, object>();

            d.Add("SEARCHFILTER", SEARCHFILTER);

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_MedicalInsurance_CFEntryOMI_CLAIM_MANAGER", "MONTHYEAR_LOV", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        public JsonResult tbl_CFEnt_OMIDE03_M(Decimal? OMI_P_NO, Decimal? OMI_MONTH, Decimal? OMI_YEAR)
        {
            Dictionary<string, object> d = new Dictionary<string, object>();

            d.Add("OMI_P_NO ", OMI_P_NO);
            d.Add("OMI_MONTH" ,OMI_MONTH);
            d.Add("OMI_YEAR " ,OMI_YEAR);
            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_MedicalInsurance_CFEntry_OMI_CLAIM_DETAIL_MANAGER", "List", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        public JsonResult PROC_CFEnt_OMIDE03(Decimal? OMI_P_NO, Decimal? OMI_MONTH, Decimal? OMI_YEAR, Decimal? OMI_CHILD, string OMI_TOTAL_CALIM1, Decimal? OMI_CLAIM_AMOUNT, string OMI_PATIENTS_NAME, DateTime? W_JOINING_DATE, DateTime? W_MARRIAGE_DATE, Decimal? OMI_CLAIM_NO, string W_YEAR, string  W_MONTH,string W_STATUS, string W_SELF, string W_SPOUSE,string W_CHILD, string W_OMI_LIMIT)
        {
            try
            {
                if (W_YEAR != string.Empty && W_MONTH != string.Empty)
                {
                    string Date = "01/" + W_MONTH + "/" + W_YEAR;

                    Result rsltLIST;
                    Dictionary<string, object> param6 = new Dictionary<string, object>();
                    param6.Add("OMI_P_NO", OMI_P_NO);
                    param6.Add("OMI_MONTH",OMI_MONTH);
                    param6.Add("OMI_YEAR", OMI_YEAR );

                    rsltLIST = cmnDM.GetData("CHRIS_MedicalInsurance_CFEntry_OMI_CLAIM_DETAIL_MANAGER", "List", param6);
                    if (rsltLIST.isSuccessful)
                    {
                        if (rsltLIST.dstResult != null && rsltLIST.dstResult.Tables[0].Rows.Count > 0)
                        {
                            //dgvOmiDetail.DataSource = rsltLIST.dstResult.Tables[0];
                            //int Total = Convert.ToInt32(dgvOmiDetail.CurrentRow.Cells["colAmount"].EditedFormattedValue) + Convert.ToInt32(txt_W_Total.Text == string.Empty ? "0" : txt_W_Total.Text);
                            //txt_W_Total.Text = Total.ToString();
                        }
                    }


                    Result rslt;
                    Dictionary<string, object> param = new Dictionary<string, object>();
                    param.Add("SEARCHFILTER", Date);
                   
                    rslt = cmnDM.GetData("CHRIS_MEDICALINSURANCE_CFENTRYOMI_CLAIM_MANAGER", "OMI_LIMIT", param);
                    if (rslt.isSuccessful)
                    {
                        if (rslt.dstResult != null && rslt.dstResult.Tables[0].Rows.Count > 0)
                        {
                            //:W_OMI_LIMIT,:W_SELF,:W_SPOUSE,:W_CHILD
                            txt_W_OMI_LIMIT = rslt.dstResult.Tables[0].Rows[0]["W_OMI_LIMIT"].ToString();
                            txt_W_SELF = rslt.dstResult.Tables[0].Rows[0]["W_SELF"].ToString();
                            txt_W_SPOUSE = rslt.dstResult.Tables[0].Rows[0]["W_SPOUSE"].ToString();
                            txt_W_CHILD = rslt.dstResult.Tables[0].Rows[0]["W_CHILD"].ToString();
                            txt_PR_P_NO = Convert.ToString(OMI_P_NO);
                        }
                        else if (rslt.dstResult.Tables[0].Rows.Count == 0)
                        {
                            txt_W_OMI_LIMIT = "0";
                            txt_W_SELF = "0";
                            txt_W_SPOUSE = "0";
                            txt_W_CHILD = "0";
                            txt_PR_P_NO = Convert.ToString(OMI_P_NO);
                        }
                    }

                    /* CALLING PROCEDURE TO PRO-RATE OMI LIMIT */
                    if (OMI_CHILD == null)
                    {
                        g_Child = "0";
                    }
                    else
                    {
                        g_Child = Convert.ToString(OMI_CHILD);
                    }


                    PROC_5(W_YEAR, W_JOINING_DATE,  W_MARRIAGE_DATE,  W_STATUS,  W_SELF,  W_SPOUSE);

                    PROC_6( OMI_P_NO,  W_YEAR,  W_JOINING_DATE,  W_CHILD);

                    int spouse = txt_W_SPOUSE == "" ? 0 : int.Parse(txt_W_SPOUSE);
                    int self = txt_W_SELF == "" ? 0 : int.Parse(txt_W_SELF);
                    int child = txt_W_CHILD == "" ? 0 : int.Parse(txt_W_CHILD);

                    txt_W_OMI_LIMIT = Convert.ToString(spouse + self + child);


                    if (rslt.isSuccessful)
                    {
                        if (rslt.dstResult != null && rslt.dstResult.Tables[0].Rows.Count == 0)
                        {
                            txt_OMI_SELF = txt_W_SELF;
                            txt_OMI_SPOUSE = txt_W_SPOUSE;
                            txt_OMI_CHILD = txt_W_CHILD;
                        }
                    }


                    PROC_8(OMI_P_NO,  OMI_YEAR);
                    /* UPDATE OMI LIMITS EVERY TIME DUE TO MARRIAGE OR BIRTH OF CHILD*/
                    PROC_7( OMI_P_NO, W_OMI_LIMIT);


                    //OMICLAIMDETAILCommand ent = (OMICLAIMDETAILCommand)(this.pnltblDetl.CurrentBusinessEntity);
                    //ent.OMI_P_NO = Convert.ToDecimal(txt_W_Pno);

                    int OS_Balance = 0;
                    OS_Balance = Convert.ToInt32(txt_W_OMI_LIMIT == string.Empty ? "0" : txt_W_OMI_LIMIT) - Convert.ToInt32(OMI_TOTAL_CALIM1 == string.Empty ? "0" : OMI_TOTAL_CALIM1);
                    txt_W_OS_BALANCE = OS_Balance.ToString();

                    Proc_1(OMI_P_NO, OMI_CLAIM_NO, OMI_MONTH, OMI_YEAR);
                }

                var res = new
                {
                    txt_W_OMI_LIMIT      = txt_W_OMI_LIMIT,
                    txt_W_SELF           = txt_W_SELF,
                    txt_W_SPOUSE         = txt_W_SPOUSE,
                    txt_W_CHILD          = txt_W_CHILD,
                    txt_PR_P_NO          = txt_PR_P_NO,
                    txt_OMI_SELF         = txt_OMI_SELF,
                    txt_OMI_SPOUSE       = txt_OMI_SPOUSE,
                    txt_OMI_CHILD        = txt_OMI_CHILD,
                    txt_W_OS_BALANCE     = txt_W_OS_BALANCE,
                    txt_W_STATUS         = txt_W_STATUS,
                    txt_OMI_CLAIM_NO     = txt_OMI_CLAIM_NO,
                    txt_OMI_TOTAL_CALIM1 = txt_OMI_TOTAL_CALIM1 ,
                    MessageBox = MessageBox,
                };

                return Json(res);

            }
            catch (Exception exp)
            {
                //LogError(this.Name, "txt_W_OMI_LIMIT_Enter", exp);
                MessageBox = exp.Message;
            }
            return Json(null);
        }

        public int MonthsBetweenInOracle(DateTime MaxDate, DateTime MinDate)
        {
            Int32 dResult = 0;

            //Last day of Maximum Date
            DateTime FirstDayOfMonth_MaxDate = new DateTime(MaxDate.Year, MaxDate.Month, 1);
            DateTime LastDayOfMonth_MaxDate = FirstDayOfMonth_MaxDate.AddMonths(1).AddDays(-1);

            //Last day of Minimum Date
            DateTime FirstDayOfMonth_MinDate = new DateTime(MinDate.Year, MinDate.Month, 1);
            DateTime LastDayOfMonth_MinDate = FirstDayOfMonth_MinDate.AddMonths(1).AddDays(-1);

            //Condition 1: Day of Maximum date != Day of Minimum date
            //Condition 2: "Last Day of Maximum date != Day of Maximum date" And "Last Day of Minimum date != Day of Minimum date"
            if ((MaxDate.Day == MinDate.Day) || (LastDayOfMonth_MaxDate.Day == MaxDate.Day && LastDayOfMonth_MinDate.Day == MinDate.Day))
            {
                return (12 * (MaxDate.Year - MinDate.Year) + MaxDate.Month - MinDate.Month);
            }
            else
            {
                int A = ((12 * (MaxDate.Year - MinDate.Year) + MaxDate.Month - MinDate.Month) - 1) * 31;
                A = A + (MaxDate.Day - 1) + (32 - MinDate.Day);

                Double D = A / 31.00;
                dResult = Convert.ToInt32(Math.Round(D, 0));
            }

            return dResult;
        }

        #region PROC
        public void Proc_1(Decimal? OMI_P_NO, Decimal? OMI_CLAIM_NO, Decimal? OMI_MONTH, Decimal? OMI_YEAR)
        {
            try
            {
                Result rslt;
                Dictionary<string, object> param = new Dictionary<string, object>();
                param.Add("OMI_P_NO", OMI_P_NO);
                param.Add("OMI_MONTH", OMI_MONTH);
                param.Add("OMI_YEAR", OMI_YEAR);
                rslt = cmnDM.GetData("CHRIS_MedicalInsurance_CFEntryOMI_CLAIM_MANAGER", "PROC_1", param);
                if (rslt.isSuccessful)
                {
                    if (rslt.dstResult != null && rslt.dstResult.Tables[0].Rows.Count > 0)
                    {
                        txt_OMI_CLAIM_NO = rslt.dstResult.Tables[0].Rows[0]["OMI_CLAIM_NO"].ToString();
                    }
                }

                txt_OMI_CLAIM_NO = txt_OMI_CLAIM_NO == string.Empty ? "1" : txt_OMI_CLAIM_NO;
            }
            catch (Exception exp)
            {                
                MessageBox = "Proc_1" + exp.Message;
            }
        }

        public bool PROC_4(string OMI_PATIENTS_NAME,Decimal? OMI_CLAIM_AMOUNT,string OMI_SPOUSE,string OMI_CHILD)
        {
            try
            {
                int str_S_no = 0;
                str_S_no = s_no;
                int amount = Convert.ToInt16(OMI_CLAIM_AMOUNT);
                string self = OMI_PATIENTS_NAME;

                /* FOR SELF CHEKING LIMITS */
                if (str_S_no == 1)
                {
                    txt_OMI_SELF = Convert.ToString(Convert.ToInt32(txt_OMI_SELF) - amount);

                    if (str_S_no == 1 && (txt_OMI_SELF) == "0")
                    {
                        MessageBox = "OMI LIMIT FOR SELF IS  " + self + " REMAINING LIMIT IS " + txt_OMI_SELF;
                        return false;
                    }
                }

                /* FOR SPOUSE CHECKING LIMITS */
                if (str_S_no == 2)
                {
                    txt_OMI_SPOUSE = Convert.ToString(Convert.ToInt32(OMI_SPOUSE) - amount);

                    if (str_S_no == 2 && (txt_OMI_SPOUSE) == "0")
                    {
                        MessageBox = "OMI LIMIT FOR SPOUSE IS " + self + " REMAINING LIMIT IS " + txt_OMI_SPOUSE;
                        return false;
                    }
                }

                /* FOR CHILDREN CHECKING LIMITS */
                if (str_S_no > 2)
                {
                    txt_OMI_CHILD = Convert.ToString(Convert.ToInt32(OMI_CHILD) - amount);

                    if (str_S_no > 2 && (txt_OMI_CHILD) == "0")
                    {
                        MessageBox = "OMI LIMIT FOR CHILDREN IS " + self + " REMAINING LIMIT IS " + txt_OMI_CHILD;
                        return false;
                    }
                }
                return true;
            }
            catch (Exception exp)
            {  
                MessageBox = "PROC_4" + exp.Message;
                return false;
            }
        }

        public void PROC_5(string W_YEAR,DateTime? W_JOINING_DATE,DateTime? W_MARRIAGE_DATE,string W_STATUS,string W_SELF,string W_SPOUSE)
        {
            try
            {
                int WW_SELF = 0;
                int WW_SPOUSE = 0;
                DateTime dtYearDate = Convert.ToDateTime("31-Dec-" + W_YEAR);
                DateTime dtJoiningDate = Convert.ToDateTime(W_JOINING_DATE);
                DateTime dtMarriageDate = Convert.ToDateTime(W_MARRIAGE_DATE);
                bool wSelfChk = true;
                bool wSpouseChk = true;
                int W_SELF1 = 0;
                int W_SPOUSE1 = 0;


                if ((Convert.ToDateTime(W_JOINING_DATE).Year) == (Convert.ToDateTime(dtYearDate).Year) &&
                    W_STATUS == "M" &&
                    (Convert.ToDateTime(W_MARRIAGE_DATE) < (Convert.ToDateTime(W_JOINING_DATE))))
                {
                    wSelfChk = int.TryParse(W_SELF, out W_SELF1);
                    wSpouseChk = int.TryParse(W_SPOUSE, out W_SPOUSE1);

                    if (wSelfChk == true && wSpouseChk == true)
                    {
                        WW_SELF = (W_SELF1 / 12) * ((MonthsBetweenInOracle(Convert.ToDateTime(dtYearDate), Convert.ToDateTime(dtJoiningDate))));
                        WW_SPOUSE = (W_SPOUSE1 / 12) * ((MonthsBetweenInOracle(Convert.ToDateTime(dtYearDate), Convert.ToDateTime(dtJoiningDate))));
                    }
                }


                /* MARRIED AFTER JOINING AND MARRIAGE IS IN CURRENT YEAR */
                if (W_STATUS == "M" &&
                    Convert.ToDateTime(W_MARRIAGE_DATE) > Convert.ToDateTime(W_JOINING_DATE) &&
                    dtMarriageDate.Year.ToString() == W_YEAR)
                {
                    wSelfChk = int.TryParse(W_SELF, out W_SELF1);
                    wSpouseChk = int.TryParse(W_SPOUSE, out W_SPOUSE1);

                    if (wSelfChk == true && wSpouseChk == true)
                    {
                        WW_SELF = W_SELF1;
                        WW_SPOUSE = (W_SPOUSE1 / 12) * ((MonthsBetweenInOracle(Convert.ToDateTime(dtYearDate), Convert.ToDateTime(dtMarriageDate))));
                    }
                }


                /* MARRIED AND JOIND BEFOR CURRENT YEAR                  */
                if (W_STATUS == "M" &&
                    ((Convert.ToDateTime(W_JOINING_DATE).Year) < (Convert.ToDateTime(dtYearDate).Year)) &&
                    ((Convert.ToDateTime(W_MARRIAGE_DATE).Year < Convert.ToDateTime(dtYearDate).Year) || W_MARRIAGE_DATE == null))
                {
                    wSelfChk = int.TryParse(W_SELF, out W_SELF1);
                    wSpouseChk = int.TryParse(W_SPOUSE, out W_SPOUSE1);

                    if (wSelfChk == true && wSpouseChk == true)
                    {
                        WW_SELF = W_SELF1;
                        WW_SPOUSE = W_SPOUSE1;//(W_SPOUSE / 12) * ((base.MonthsBetweenInOracle(Convert.ToDateTime(dtYearDate), Convert.ToDateTime(dtp_W_MARRIAGE_DATE.Value))));
                    }
                }

                /* SINGLE AND JOINED IN THE CURRENT YEAR                */
                if ((W_STATUS == "S" || W_STATUS == "D" || W_STATUS == "W") &&
                    ((Convert.ToDateTime(W_JOINING_DATE).Year) == Convert.ToDateTime(dtYearDate).Year))
                {
                    wSelfChk = int.TryParse(W_SELF, out W_SELF1);
                    wSpouseChk = int.TryParse(W_SPOUSE, out W_SPOUSE1);

                    if (wSelfChk == true && wSpouseChk == true)
                    {
                        WW_SELF = ((W_SELF1 / 12) * (MonthsBetweenInOracle(Convert.ToDateTime(dtYearDate), Convert.ToDateTime(dtJoiningDate))));
                    }
                }

                /* SINGLE AND JOINED BEFORE CURRENT YEAR                */
                if ((W_STATUS == "S" || W_STATUS == "D" || W_STATUS == "W") &&
                    ((Convert.ToDateTime(W_JOINING_DATE).Year) < Convert.ToDateTime(dtYearDate).Year))
                {
                    wSelfChk = int.TryParse(W_SELF, out W_SELF1);

                    if (wSelfChk)
                    {
                        WW_SELF = W_SELF1;
                    }
                }

                txt_W_SELF = WW_SELF == null ? "0" : WW_SELF.ToString();
                txt_W_SPOUSE = WW_SPOUSE == null ? "0" : WW_SPOUSE.ToString();

                if (txt_OMI_SPOUSE == string.Empty && WW_SPOUSE > 0)
                {                  
                    txt_OMI_SPOUSE = W_SPOUSE.ToString();
                }

            }
            catch (Exception exp)
            {
                MessageBox = "PROC_5" + exp.Message;
            }
        }

        public void PROC_6(Decimal? OMI_P_NO, string W_YEAR,DateTime? W_JOINING_DATE,string W_CHILD)
        {
            try
            {
                int ww_child = 0;
                int w_omi = 0;
                int var1 = 0;
                int var2 = 0;
                DateTime dtYearDate = Convert.ToDateTime("31-Dec-" + W_YEAR);
                DateTime dtDateBirth = new DateTime();

                Result rslt;
                Dictionary<string, object> param = new Dictionary<string, object>();
                param.Add("OMI_P_NO", OMI_P_NO);
                rslt = cmnDM.GetData("CHRIS_MEDICALINSURANCE_CFENTRYOMI_CLAIM_MANAGER", "OMI_CHILD", param);
                if (rslt.isSuccessful)
                {
                    if (rslt.dstResult != null && rslt.dstResult.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow dr in rslt.dstResult.Tables[0].Rows)
                        {
                            dtDateBirth = Convert.ToDateTime(dr["PR_DATE_BIRTH"].ToString());

                            if (dtDateBirth.Year < dtYearDate.Year)
                            {
                                if ((Convert.ToDateTime(W_JOINING_DATE).Year) < dtYearDate.Year)
                                {
                                    ww_child = ww_child + int.Parse(W_CHILD);
                                }
                                else
                                {
                                    ww_child = ((int.Parse(W_CHILD)) / 12) * (MonthsBetweenInOracle(Convert.ToDateTime(dtYearDate), Convert.ToDateTime(W_JOINING_DATE))) + ww_child;
                                }
                            }
                            else if (dtDateBirth.Year == dtYearDate.Year)
                            {
                                var1 = (MonthsBetweenInOracle(Convert.ToDateTime(dtYearDate), Convert.ToDateTime(dtDateBirth)));
                                var2 = (MonthsBetweenInOracle(Convert.ToDateTime(dtYearDate), Convert.ToDateTime(W_JOINING_DATE)));

                                if (var1 > var2)
                                {
                                    ww_child = (((int.Parse(W_CHILD)) / 12) * var2 + ww_child);
                                }
                                else if (var1 < var2)
                                {
                                    ww_child = (((int.Parse(W_CHILD)) / 12) * var1 + ww_child);
                                }
                                else if (var1 == var2)
                                {
                                    ww_child = (((int.Parse(W_CHILD)) / 12) * var1 + ww_child);
                                }
                            }
                        }
                    }
                    txt_W_CHILD = Convert.ToString((ww_child - w_omi) + w_omi);
                    txt_OMI_CHILD = txt_W_CHILD;
                }
            }
            catch (Exception exp)
            {
                MessageBox = "PROC_6" + exp.Message;
            }
        }

        public void PROC_7(Decimal? OMI_P_NO, string W_OMI_LIMIT)
        {
            try
            {
                DateTime date1 = DateTime.Now;
                string year = Convert.ToString(date1.Year);
                int DUMMY = 0;
                Result rslt;
                Result rsltIns;
                Result rsltUpd;
                Dictionary<string, object> param = new Dictionary<string, object>();
                param.Add("OMI_P_NO", OMI_P_NO);
                param.Add("OMI_YEAR", year);
                rslt = cmnDM.GetData("CHRIS_MEDICALINSURANCE_CFENTRYOMI_CLAIM_MANAGER", "PROC_7", param);
                param.Clear();
                if (rslt.isSuccessful)
                {
                    if (rslt.dstResult.Tables[0].Rows.Count == 0)
                    {
                        rsltIns = cmnDM.Execute("CHRIS_MEDICALINSURANCE_CFENTRYOMI_CLAIM_MANAGER", "PROC_7_INSERT", param);
                        param.Clear();

                        txt_OMI_SELF = txt_W_SELF;
                        txt_OMI_SPOUSE = txt_W_SPOUSE;
                        txt_OMI_CHILD = txt_W_CHILD;
                    }
                    else if (rslt.dstResult.Tables[0].Rows.Count > 0)
                    {
                        DUMMY = int.Parse(rslt.dstResult.Tables[0].Rows[0]["OMI_LIMIT"].ToString());

                        if (DUMMY != int.Parse(Convert.ToString(W_OMI_LIMIT)))
                        {
                            param.Add("OMI_TOTAL_CLAIMS", OMI_P_NO);
                            param.Add("OMI_YEAR", year);
                            rsltUpd = cmnDM.Execute("CHRIS_MEDICALINSURANCE_CFENTRYOMI_CLAIM_MANAGER", "PROC_7_UPDATE", param);
                            param.Clear();
                        }
                    }
                }
            }
            catch (Exception exp)
            {
                MessageBox = "PROC_7" + exp.Message;
            }
        }

        public void PROC_8(Decimal? OMI_P_NO, Decimal? OMI_YEAR)
        {
            try
            {
                int SELF = 0;
                int WIFE = 0;
                int SON = 0;

                /*Self*/
                Dictionary<string, object> paramSelf = new Dictionary<string, object>();
                paramSelf.Add("OMI_P_NO", OMI_P_NO);
                paramSelf.Add("OMI_YEAR", OMI_YEAR);

                Result rsltCodeS;
                CmnDataManager cmnDM = new CmnDataManager();
                rsltCodeS = cmnDM.GetData("CHRIS_MedicalInsurance_CFEntryOMI_CLAIM_MANAGER", "PROC8_SELF", paramSelf);

                if (rsltCodeS.isSuccessful && rsltCodeS.dstResult.Tables.Count > 0 && rsltCodeS.dstResult.Tables[0].Rows.Count > 0)
                {
                    SELF = Convert.ToInt32(rsltCodeS.dstResult.Tables[0].Rows[0]["SELF"].ToString() == string.Empty ? "0" : rsltCodeS.dstResult.Tables[0].Rows[0]["SELF"].ToString());
                }


                /*WIFE*/
                Dictionary<string, object> paramWife = new Dictionary<string, object>();
                paramWife.Add("OMI_P_NO", OMI_P_NO);
                paramWife.Add("OMI_YEAR", OMI_YEAR);

                Result rsltCodeW;
                rsltCodeW = cmnDM.GetData("CHRIS_MedicalInsurance_CFEntryOMI_CLAIM_MANAGER", "PROC8_WIFE", paramWife);

                if (rsltCodeW.isSuccessful && rsltCodeW.dstResult.Tables.Count > 0 && rsltCodeW.dstResult.Tables[0].Rows.Count > 0)
                {
                    WIFE = Convert.ToInt32(rsltCodeW.dstResult.Tables[0].Rows[0]["WIFE"].ToString() == string.Empty ? "0" : rsltCodeW.dstResult.Tables[0].Rows[0]["WIFE"].ToString());
                }

                /*WIFE*/
                Dictionary<string, object> paramSon = new Dictionary<string, object>();
                paramSon.Add("OMI_P_NO", OMI_P_NO);
                paramSon.Add("OMI_YEAR", OMI_YEAR);

                Result rsltCodeSon;
                rsltCodeSon = cmnDM.GetData("CHRIS_MedicalInsurance_CFEntryOMI_CLAIM_MANAGER", "PROC8_SON", paramSon);

                if (rsltCodeSon.isSuccessful && rsltCodeSon.dstResult.Tables.Count > 0 && rsltCodeSon.dstResult.Tables[0].Rows.Count > 0)
                {
                    SON = Convert.ToInt32(rsltCodeSon.dstResult.Tables[0].Rows[0]["SON"].ToString() == string.Empty ? "0" : rsltCodeSon.dstResult.Tables[0].Rows[0]["SON"].ToString());
                }

                txt_OMI_SELF = Convert.ToString((Convert.ToInt32(txt_W_SELF == string.Empty ? "0" : txt_W_SELF)) + SELF);
                txt_OMI_SPOUSE = Convert.ToString((Convert.ToInt32(txt_W_SPOUSE == string.Empty ? "0" : txt_W_SPOUSE)) + WIFE);
                txt_OMI_CHILD = Convert.ToString((Convert.ToInt32(txt_W_CHILD == string.Empty ? "0" : txt_W_CHILD)) + SON);
                txt_OMI_TOTAL_CALIM1 = Convert.ToString(SELF + WIFE + SON);

            }
            catch (Exception exp)
            {
                MessageBox = "PROC_8" + exp.Message;
            }
        }

        #endregion
        #endregion
        public IActionResult ClaimRep()
        {
            return View();
        }
        public IActionResult DsgWisOMI()
        {
            return View();
        }
        public IActionResult EmpPerInfo()
        {
            return View();
        }
        #region EnrEntry_GHIDE01
        public IActionResult EnrEntry_GHIDE01()
        {
            return View();
        }

        #endregion

        public IActionResult EnrLetG2()
        {
            return View();
        }
        public IActionResult EnrLetRepG()
        {
            return View();
        }
        public IActionResult EnrLettRep()
        {
            return View();
        }

        #region EnrollmentLetter
        
        public IActionResult EnrollmentLetter()
        {
            return View();
        }

        public JsonResult btn_PR_P_NO_Enrol_Let(Decimal? PR_P_NO)
        {
            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("PR_P_NO", PR_P_NO);

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_EnrLettPERSONNEL_MANAGER", "Personal", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        public IActionResult EnrollmentLetterPage1()
        {
            return View();
        }
        public IActionResult EnrollmentLetterPage2()
        {
            return View();
        }

        #endregion
        public IActionResult EnrStatus()
        {
            return View();
        }
        public IActionResult EnrStatusG()
        {
            return View();
        }
        public IActionResult EnrStsRepG()
        {
            return View();
        }
        public IActionResult GHosRepOff()
        {
            return View();
        }
        public IActionResult GLINSrEP()
        {
            return View();
        }
        public IActionResult GLINSrEP2()
        {
            return View();
        }
        public IActionResult GrpHospRep()
        {
            return View();
        }

        #region OMILimCalc
        public IActionResult OMILimCalc()
        {
            return View();
        }

        #region Variables

        string txt_W_Joining_Date  = "";
        string txt_W_Marriage_Date = "";

        string MessageBox = "";
        string g_Child = "";

        string txt_W_PNO = "";
        string txt_W_Name = "";
        string txt_W_Cat = "";
        string txt_W_Status = "";

        string txt_OMI_Child = "";
        string txt_W_OMI_Limit = "";
        string txt_W_Self ="";
        string txt_W_Spouse ="";
        string txt_W_Child ="";
        string txt_OMI_Total_Claim = "";
        string txt_W_OS_Balance = "";
        string txt_OMI_Spouse = "";
        string txt_OMI_Self = "";
        #endregion

        public JsonResult btn_OMILimCalc(string W_Month, string W_Year)
        {
            try
            {
                int year = Convert.ToInt32(W_Year == string.Empty ? "0" : W_Year);
                int month = Convert.ToInt32(W_Month == string.Empty ? "0" : W_Month);
                if (month == 0 && month > 12 && year == 0)
                {
                    MessageBox = "ENTER A VALID MONTH OR YEAR";
                }
                else
                {
                    Proc1(W_Month, W_Year);
                }

                if (MessageBox !="")
                {
                    return Json(MessageBox);
                }
                else
                {
                    return Json("Process Complete !!!");
                }
                
            }
            catch (Exception exp)
            {
                return Json(exp);
            }
           
            return Json(null);
         }

        #region PROC

        public void Proc1(string W_Month, string W_Year)
        {
            try
            {
                Dictionary<string, object> param = new Dictionary<string, object>();
                param.Clear();
                param.Add("OMI_MONTH", W_Month);
                param.Add("OMI_YEAR", W_Year);

                Result rsltCode;
                CmnDataManager cmnDM = new CmnDataManager();
                rsltCode = cmnDM.GetData("CHRIS_SP_OMILimCalc_OMI_CLAIM_MANAGER", "PROC1", param);

                if (rsltCode.isSuccessful && rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in rsltCode.dstResult.Tables[0].Rows)
                    {
                        txt_W_PNO  = dr["PR_P_NO"].ToString();
                        txt_W_Name = dr["NAME"].ToString();
                        txt_W_Cat  = dr["PR_CATEGORY"].ToString();
                        if (dr["PR_JOINING_DATE"].ToString() != "")
                            txt_W_Joining_Date = dr["PR_JOINING_DATE"].ToString();

                        txt_W_Status = dr["PR_MARITAL"].ToString();

                        if (dr["PR_MARRIAGE"].ToString() != "")
                            txt_W_Marriage_Date = dr["PR_MARRIAGE"].ToString();

                        string Enroll = string.Empty;

                        Dictionary<string, object> param1 = new Dictionary<string, object>();
                        param1.Add("OMI_MONTH", W_Month);

                        Result rsltCode1;
                        CmnDataManager cmnDM1 = new CmnDataManager();
                        rsltCode1 = cmnDM1.GetData("CHRIS_SP_OMILimCalc_OMI_CLAIM_MANAGER", "PNo_Validate", param1);

                        if (rsltCode1.isSuccessful && rsltCode1.dstResult.Tables.Count > 0 && rsltCode1.dstResult.Tables[0].Rows.Count > 0)
                        {
                            txt_W_Name = rsltCode1.dstResult.Tables[0].Rows[0]["NAME"].ToString();
                            Enroll = rsltCode1.dstResult.Tables[0].Rows[0]["PR_OPD"].ToString();
                            txt_W_Cat = rsltCode1.dstResult.Tables[0].Rows[0]["PR_CATEGORY"].ToString();
                            txt_W_Joining_Date = rsltCode1.dstResult.Tables[0].Rows[0]["PR_JOINING_DATE"].ToString();
                            txt_W_Status = rsltCode1.dstResult.Tables[0].Rows[0]["PR_MARITAL"].ToString();
                            txt_W_Marriage_Date = rsltCode1.dstResult.Tables[0].Rows[0]["PR_CATEGORY"].ToString();

                            if (Enroll == "N")
                            {
                                MessageBox = txt_W_PNO + " IS NOT ENROLLED IN OMI";                               
                            }

                        }
                            Proc2( W_Month,  W_Year);
                    }
                }
            }
            catch (Exception exp)
            {
                MessageBox = "Error1 '"+exp+"'";
            }
        }

        public void Proc2(string W_Month, string W_Year)
        {
            try
            {
                string pol_Date = "01/" + W_Month + "/" + W_Year;


                Dictionary<string, object> param = new Dictionary<string, object>();
                param.Add("SEARCHFILTER", pol_Date);

                Result rsltCode;
                CmnDataManager cmnDM = new CmnDataManager();
                rsltCode = cmnDM.GetData("CHRIS_SP_OMILimCalc_OMI_CLAIM_MANAGER", "PROC2", param);

                if (rsltCode.isSuccessful && rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count == 1)
                {
                    txt_W_OMI_Limit = rsltCode.dstResult.Tables[0].Rows[0]["POL_OMI_LIMIT"].ToString();
                    txt_W_Self      = rsltCode.dstResult.Tables[0].Rows[0]["POL_OMI_SELF"].ToString();
                    txt_W_Spouse    = rsltCode.dstResult.Tables[0].Rows[0]["POL_PER_DEPEN_SPOUSE"].ToString();
                    txt_W_Child     = rsltCode.dstResult.Tables[0].Rows[0]["POL_PER_DEPEN_CHILD"].ToString();
                }
                else if (rsltCode.isSuccessful && rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count == 0)
                {
                    txt_W_OMI_Limit = "0";
                }
                else if (rsltCode.isSuccessful && rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 1)
                {
                    MessageBox = "Problem in Policy_master Table Too many Rows..";
                }

                /* CALLING PROCEDURE TO PRO-RATE OMI LIMIT */
                g_Child = txt_OMI_Child  == string.Empty ? "0" : txt_OMI_Child;

                Proc5( W_Month,  W_Year);
                Proc6( W_Month,  W_Year);

                int omi_limit = Convert.ToInt32(txt_W_Self == string.Empty ? "0" : txt_W_Self) + Convert.ToInt32(txt_W_Spouse == string.Empty ? "0" : txt_W_Spouse) + Convert.ToInt32(txt_W_Child == string.Empty ? "0" : txt_W_Child);
                txt_W_OMI_Limit = omi_limit.ToString();

                /* UPDATE OMI LIMITS EVERY TIME DUE TO MARRIAGE OR BIRTH OF CHILD*/
                Proc7( W_Month,  W_Year);
                /*  END OF CALLING */
                                
                Proc8( W_Month,  W_Year);

                int os_Balance = Convert.ToInt32(txt_W_OMI_Limit == string.Empty ? "0" : txt_W_OMI_Limit) + Convert.ToInt32(txt_OMI_Total_Claim  == string.Empty ? "0" : txt_OMI_Total_Claim);
                txt_W_OS_Balance = os_Balance.ToString();
            }
            catch (Exception exp)
            {
                MessageBox = "Error2 '" + exp + "'";
            }
        }

        public void Proc5(string W_Month, string W_Year)
        {
            try
            {
                double ww_Self = 0;
                double ww_Spouse = 0;
                DateTime dt_Joining = Convert.ToDateTime( txt_W_Joining_Date);
                DateTime dt_Marriage = Convert.ToDateTime(txt_W_Marriage_Date);

                string YearEnd = "31/12/" + W_Year;
                DateTime dt_YearEnd = Convert.ToDateTime(YearEnd);

                /* JOINED IN THE CURRENT YEAR AND MARRIED AT THE TIME OF JOINING */
                if (dt_Joining.Year == Convert.ToInt32(W_Year) && txt_W_Status == "M" && Convert.ToDateTime(txt_W_Marriage_Date) < Convert.ToDateTime(txt_W_Joining_Date))
                {
                    ww_Self = ((Convert.ToInt32(txt_W_Self) / 12) * (Math.Round(Convert.ToDouble((Convert.ToDateTime(dt_YearEnd), Convert.ToDateTime(txt_W_Joining_Date))))));
                    ww_Spouse = ((Convert.ToInt32(txt_W_Spouse) / 12) * (Math.Round(Convert.ToDouble((Convert.ToDateTime(dt_YearEnd), Convert.ToDateTime(txt_W_Joining_Date))))));
                }
                else if (txt_W_Status == "M" && Convert.ToDateTime(txt_W_Marriage_Date) > Convert.ToDateTime(txt_W_Joining_Date) && dt_Marriage.Year == Convert.ToInt32(W_Year))
                {
                    /* MARRIED AFTER JOINING AND MARRIAGE IS IN CURRENT YEAR */

                    ww_Self = Convert.ToDouble(txt_W_Self);
                    ww_Spouse = ((Convert.ToInt32(txt_W_Spouse) / 12) * (Math.Round(Convert.ToDouble((Convert.ToDateTime(dt_YearEnd), Convert.ToDateTime(txt_W_Marriage_Date))))));
                }
                else if (txt_W_Status == "M" && dt_Joining.Year < Convert.ToInt32(W_Year) && (Convert.ToDateTime(txt_W_Marriage_Date) < Convert.ToDateTime(W_Year) || dt_Marriage == null))
                {
                    ww_Self = Convert.ToDouble(txt_W_Self);
                    ww_Spouse = Convert.ToDouble(txt_W_Spouse);
                }
                else if ((txt_W_Status == "S" || txt_W_Status == "D" || txt_W_Status == "W") && (dt_Joining.Year == Convert.ToInt32(W_Year)))
                {
                    /* SINGLE AND JOINED IN THE CURRENT YEAR */
                    ww_Self = ((Convert.ToInt32(txt_W_Self) / 12) * (Math.Round(Convert.ToDouble((Convert.ToDateTime(dt_YearEnd), Convert.ToDateTime(txt_W_Joining_Date))))));
                }
                else if ((txt_W_Status == "S" || txt_W_Status == "D" || txt_W_Status == "W") && dt_Joining.Year < Convert.ToInt32(W_Year))
                {
                    /* SINGLE AND JOINED BEFORE CURRENT YEAR*/
                    ww_Self = Convert.ToDouble(txt_W_Self);
                }

                txt_W_Self = ww_Self.ToString();
                txt_W_Spouse = ww_Spouse.ToString();


                if ((Convert.ToInt32(txt_OMI_Spouse == string.Empty ? "0" : txt_OMI_Spouse) == 0) && (Convert.ToInt32(txt_OMI_Spouse == string.Empty ? "0" : txt_OMI_Spouse) > 0))
                {
                    txt_OMI_Spouse = txt_W_Spouse;
                }
            }
            catch (Exception exp)
            {
                MessageBox = "Error3 '" + exp + "'";
            }
        }

        public void Proc6(string W_Month, string W_Year)
        {
            try
            {
                string YearEnd = "31/12/" + W_Year;
                DateTime dt_YearEnd = Convert.ToDateTime(YearEnd);

                Dictionary<string, object> param = new Dictionary<string, object>();
                param.Add("OMI_P_NO", txt_W_PNO);

                Result rsltCode;
                CmnDataManager cmnDM = new CmnDataManager();
                rsltCode = cmnDM.GetData("CHRIS_SP_OMILimCalc_OMI_CLAIM_MANAGER", "PROC6", param);

                if (rsltCode.isSuccessful && rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count == 1)
                {
                    double WW_CHILD = 0;
                    int W_OMI = 0;

                    W_OMI = Convert.ToInt32(g_Child);

                    foreach (DataRow dr in rsltCode.dstResult.Tables[0].Rows)
                    {
                        if (Convert.ToDateTime(rsltCode.dstResult.Tables[0].Rows[0]["PR_DATE_BIRTH"]).Year < Convert.ToInt32(W_Year))
                        {
                            WW_CHILD = WW_CHILD + Convert.ToInt32(txt_W_Child == string.Empty ? "0" : txt_W_Child);
                        }
                        else if (Convert.ToDateTime(rsltCode.dstResult.Tables[0].Rows[0]["PR_DATE_BIRTH"]).Year == Convert.ToInt32(W_Year))
                        {
                            WW_CHILD = (Convert.ToInt32(txt_W_Child) / 12) * Math.Round(Convert.ToDouble((dt_YearEnd, Convert.ToDateTime(rsltCode.dstResult.Tables[0].Rows[0]["PR_DATE_BIRTH"])))) + WW_CHILD;
                        }

                        txt_W_Child = Convert.ToString((WW_CHILD - W_OMI) + W_OMI);
                        txt_OMI_Child = txt_W_Child;
                    }
                }
            }
            catch (Exception exp)
            {
                MessageBox = "Error4 '" + exp + "'";
            }
        }

        public void Proc7(string W_Month, string W_Year)
        {
            try
            {
                Dictionary<string, object> param = new Dictionary<string, object>();
                param.Add("OMI_P_NO", txt_W_PNO);
                param.Add("OMI_YEAR", W_Year);

                Result rsltCode;
                CmnDataManager cmnDM = new CmnDataManager();
                rsltCode = cmnDM.GetData("CHRIS_SP_OMILimCalc_OMI_CLAIM_MANAGER", "PROC7", param);
                param.Clear();

                if (rsltCode.isSuccessful && rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                {
                    /*UPDATE*/

                    Dictionary<string, object> paramUp = new Dictionary<string, object>();
                    paramUp.Add("OMI_P_NO", txt_W_PNO);
                    paramUp.Add("W_Date", DateTime.Now.Year);
                    paramUp.Add("OMI_TOTAL_RECEV", txt_W_OMI_Limit);

                    Result rsltCodeUp;
                    rsltCodeUp = cmnDM.Execute("CHRIS_SP_OMILimCalc_OMI_CLAIM_MANAGER", "PROC7_UPDATE", paramUp);
                }
                else if (rsltCode.isSuccessful && rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count == 0)
                {
                    /*Insert*/

                    Dictionary<string, object> paramIns = new Dictionary<string, object>();
                    paramIns.Add("OMI_P_NO", txt_W_PNO);
                    paramIns.Add("W_Date", DateTime.Now.Year);
                    paramIns.Add("W_OMI_LIMIT", txt_W_OMI_Limit);

                    Result rsltCodeIns;
                    rsltCodeIns = cmnDM.Execute("CHRIS_SP_OMILimCalc_OMI_CLAIM_MANAGER", "PROC7_INSERT", paramIns);
                }
            }
            catch (Exception exp)
            {
                MessageBox = "Error5 '" + exp + "'";
            }
        }

        public void Proc8(string W_Month, string W_Year)
        {
            try
            {
                int SELF = 0;
                int WIFE = 0;
                int SON = 0;

                /*Self*/
                Dictionary<string, object> paramSelf = new Dictionary<string, object>();
                paramSelf.Add("OMI_P_NO", txt_W_PNO);
                paramSelf.Add("OMI_YEAR", W_Year);

                Result rsltCodeS;
                CmnDataManager cmnDM = new CmnDataManager();
                rsltCodeS = cmnDM.GetData("CHRIS_SP_OMILimCalc_OMI_CLAIM_MANAGER", "PROC8_SELF", paramSelf);

                if (rsltCodeS.isSuccessful && rsltCodeS.dstResult.Tables.Count > 0 && rsltCodeS.dstResult.Tables[0].Rows.Count > 0)
                {
                    SELF = Convert.ToInt32(rsltCodeS.dstResult.Tables[0].Rows[0]["SELF"].ToString());
                }

                /*WIFE*/
                Dictionary<string, object> paramWife = new Dictionary<string, object>();
                paramWife.Add("OMI_P_NO", txt_W_PNO);
                paramWife.Add("OMI_YEAR", W_Year);

                Result rsltCodeW;
                rsltCodeW = cmnDM.GetData("CHRIS_SP_OMILimCalc_OMI_CLAIM_MANAGER", "PROC8_WIFE", paramWife);

                if (rsltCodeW.isSuccessful && rsltCodeW.dstResult.Tables.Count > 0 && rsltCodeW.dstResult.Tables[0].Rows.Count > 0)
                {
                    WIFE = Convert.ToInt32(rsltCodeW.dstResult.Tables[0].Rows[0]["WIFE"].ToString());
                }

                /*WIFE*/
                Dictionary<string, object> paramSon = new Dictionary<string, object>();
                paramSon.Add("OMI_P_NO", txt_W_PNO);
                paramSon.Add("OMI_YEAR", W_Year);

                Result rsltCodeSon;
                rsltCodeSon = cmnDM.GetData("CHRIS_SP_OMILimCalc_OMI_CLAIM_MANAGER", "PROC8_SON", paramSon);

                if (rsltCodeSon.isSuccessful && rsltCodeSon.dstResult.Tables.Count > 0 && rsltCodeSon.dstResult.Tables[0].Rows.Count > 0)
                {
                    SON = Convert.ToInt32(rsltCodeSon.dstResult.Tables[0].Rows[0]["SON"].ToString());
                }

                txt_OMI_Self = Convert.ToString((Convert.ToInt32(txt_W_Self == string.Empty ? "0" : txt_W_Self)) + SELF);
                txt_OMI_Spouse = Convert.ToString((Convert.ToInt32(txt_W_Spouse == string.Empty ? "0" : txt_W_Spouse)) + WIFE);
                txt_OMI_Child  = Convert.ToString((Convert.ToInt32(txt_W_Child  == string.Empty ? "0" : txt_W_Child )) + SON);
                txt_OMI_Total_Claim = Convert.ToString(SELF + WIFE + SON);

            }
            catch (Exception exp)
            {
                MessageBox = "Error6 '" + exp + "'";
            }
        }

        #endregion


        #endregion

        public IActionResult OutPatMedi()
        {
            return View();
        }

        #region PayEntry
        public IActionResult PayEntry()
        {
            return View();
        }

        public JsonResult tbl_PayEntry(string SP_MONTH,string SP_YEAR)
        {
            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("SP_MONTH", SP_MONTH);
            d.Add("SP_YEAR", SP_YEAR);

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_OMI_CLAIM_MANAGER", "LoadGridByMonthYear", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        public JsonResult btn_MonthYearLOV()
        {
            Dictionary<string, object> d = new Dictionary<string, object>();

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_OMI_CLAIM_MANAGER", "MonthYearLOV", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        #endregion

        public IActionResult PayEntry_Detail()
        {
            return View();
        }

        #region PerClaimQr
        public IActionResult PerClaimQr()
        {
            return View();
        }

        #region Vaiables

        string txt_W_Omi_Limit = "";
        string txt_W_Self1= "";
        string txt_W_Spouse1= "";
        string txt_W_Child1= "";
        string txt_Omi_Total_Claim = "";
        string txt_Omi_Self = "";
        string txt_Omi_Spouse = "";
        string txt_Omi_Child = "";
        string txt_W_Os_Balance = "";

        #endregion


        public JsonResult Tbl_Dept(Decimal? PR_P_NO)
        {
            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("PR_P_NO", PR_P_NO);

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_OMIDE05_DEPT_CONT_MANAGER", "List", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        public JsonResult btn_List()
        {
            Dictionary<string, object> d = new Dictionary<string, object>();

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_PreClaim_OMI_CLAIM_MANAGER", "LIST", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }


        public JsonResult btn_LOV_PNO()
        {
            Dictionary<string, object> d = new Dictionary<string, object>();

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_PreClaim_OMI_CLAIM_MANAGER", "LOV_PNO", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        public JsonResult btn_List1(Decimal? OMI_P_NO, Decimal? OMI_YEAR)
        {
            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("OMI_P_NO", OMI_P_NO);
            d.Add("OMI_YEAR", OMI_YEAR);

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_PreClaim_OMI_CLAIM_DETAIL_MANAGER", "List", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        public JsonResult OMI_P_NO_start_PROC(string W_CAT ,Decimal? OMI_P_NO,DateTime? W_JOINING_DATE,DateTime? W_MARRIAGE_DATE,Decimal? OMI_YEAR,string W_STATUS)
        {
            try
            {
                if (OMI_P_NO != null)
                {
                    Dictionary<string, object> param = new Dictionary<string, object>();
                    param.Add("W_CAT", W_CAT);

                    Result rsltCode;
                    CmnDataManager cmnDM = new CmnDataManager();
                    rsltCode = cmnDM.GetData("CHRIS_SP_PRECLAIM_OMI_CLAIM_MANAGER", "W_OMI_LIMIT", param);

                    if (rsltCode.isSuccessful && rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                    {
                        txt_W_Omi_Limit = rsltCode.dstResult.Tables[0].Rows[0]["W_OMI_LIMIT"].ToString();
                        txt_W_Self1 = rsltCode.dstResult.Tables[0].Rows[0]["W_SELF"].ToString();
                        txt_W_Spouse1 = rsltCode.dstResult.Tables[0].Rows[0]["W_SPOUSE"].ToString();
                        txt_W_Child1 = rsltCode.dstResult.Tables[0].Rows[0]["W_CHILD"].ToString();
                    }

                    if (rsltCode.isSuccessful && rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 1)
                    {
                        txt_W_Omi_Limit = "0";
                    }
                    else if (rsltCode.isSuccessful)
                    {
                        txt_W_Omi_Limit = "0";
                    }
                   
                    //dgvOmiDetail.Rows[0].Cells[1].Value = txt_W_Year.Text;
                    //dgvOmiDetail.Rows[0].Cells[0].Value = str_Omi_Month;

                    Proc5(W_JOINING_DATE,  W_MARRIAGE_DATE, OMI_YEAR, W_STATUS);
                    Proc6(OMI_P_NO, OMI_YEAR);

                    int omi_Limit = Convert.ToInt32(txt_W_Self1 == string.Empty ? "0" : txt_W_Self1) + Convert.ToInt32(txt_W_Spouse1 == string.Empty ? "0" : txt_W_Spouse1) + Convert.ToInt32(txt_W_Child1 == string.Empty ? "0" : txt_W_Child1);
                    txt_W_Omi_Limit = omi_Limit.ToString();


                    //IF MESSAGE_CODE  = 40350  THEN
                    //   :BLKONE.OMI_SELF         := :W_SELF;
                    //   :BLKONE.OMI_SPOUSE       := :W_SPOUSE;
                    //   :BLKONE.OMI_CHILD        := :W_CHILD;
                    //END IF;

                    Proc9(OMI_P_NO, OMI_YEAR);
                    Proc8(OMI_P_NO, OMI_YEAR);

                    int os_Balance = Convert.ToInt16(txt_W_Omi_Limit == string.Empty ? "0" : txt_W_Omi_Limit) + Convert.ToInt16(txt_Omi_Total_Claim == string.Empty ? "0" : txt_Omi_Total_Claim);
                    txt_W_Os_Balance = os_Balance.ToString();

                    int Total = 0;
                    int Total_Recv = 0;

                    //Result rsltCode1;
                    //foreach (DataRow dr in rsltCode1.dstResult.Tables[0].Rows)
                    //{
                    //    Total = Total + Convert.ToInt32(dr.Cells["OMI_CLAIM_AMOUNT"].EditedFormattedValue == "" ? "0" : dr.Cells["OMI_CLAIM_AMOUNT"].Value);
                    //    Total_Recv = Total_Recv + Convert.ToInt32(dr.Cells["OMI_RECEV_AMOUNT"].EditedFormattedValue == "" ? "0" : dr.Cells["OMI_RECEV_AMOUNT"].Value);
                    //}

                    //txt_W_Total.Text = Total.ToString();
                    //txt_W_Total_Recv.Text = Total_Recv.ToString();

                    txt_W_Self1 = txt_W_Self1 == string.Empty ? "0" : txt_W_Self1;
                    txt_W_Spouse1 = txt_W_Spouse1 == string.Empty ? "0" : txt_W_Spouse1;
                    txt_W_Child1 = txt_W_Child1 == string.Empty ? "0" : txt_W_Child1;
                    txt_Omi_Self = txt_Omi_Self == string.Empty ? "0" : txt_Omi_Self;
                    txt_Omi_Spouse = txt_Omi_Spouse == string.Empty ? "0" : txt_Omi_Spouse;
                    txt_Omi_Child = txt_Omi_Child == string.Empty ? "0" : txt_Omi_Child;
                    txt_W_Omi_Limit = txt_W_Omi_Limit == string.Empty ? "0" : txt_W_Omi_Limit;
                    txt_Omi_Total_Claim = txt_Omi_Total_Claim == string.Empty ? "0" : txt_Omi_Total_Claim;
                    txt_W_Os_Balance = txt_W_Os_Balance == string.Empty ? "0" : txt_W_Os_Balance;

                   
                }

                var res = new
                {
                    txt_W_Omi_Limit     = txt_W_Omi_Limit,
                    txt_W_Self1         = txt_W_Self1,
                    txt_W_Spouse1       = txt_W_Spouse1,
                    txt_W_Child1        = txt_W_Child1,
                    txt_Omi_Total_Claim = txt_Omi_Total_Claim ,
                    txt_Omi_Self        = txt_Omi_Self,
                    txt_Omi_Spouse      = txt_Omi_Spouse,
                    txt_Omi_Child       = txt_Omi_Child,
                    txt_W_Os_Balance    = txt_W_Os_Balance,
                    MessageBox = MessageBox,
                };

                return Json(res);

            }
            catch (Exception exp)
            {    
                MessageBox = "txt_W_PNO_Validated  '" + exp + "'";

                var res = new
                {
                    MessageBox = MessageBox,
                };
                return Json(res);
            }

            return Json(null);
        }


        #region PROC
        public void Proc5(DateTime? W_JOINING_DATE, DateTime? W_MARRIAGE_DATE, Decimal? OMI_YEAR, string W_STATUS)
        {
            try
            {
                double ww_Self = 0;
                double ww_Spouse = 0;
                DateTime dt_Joining = Convert.ToDateTime(W_JOINING_DATE);
                DateTime dt_Marriage = Convert.ToDateTime(W_MARRIAGE_DATE);

                string YearEnd = "31/12/" + OMI_YEAR;
                DateTime dt_YearEnd = Convert.ToDateTime(YearEnd);

                /* JOINED IN THE CURRENT YEAR AND MARRIED AT THE TIME OF JOINING */
                if (dt_Joining.Year == Convert.ToInt32(OMI_YEAR) && W_STATUS == "M" && Convert.ToDateTime(W_MARRIAGE_DATE) < Convert.ToDateTime(W_JOINING_DATE))
                {
                    ww_Self = ((Convert.ToInt32(txt_W_Self1 == string.Empty ? "0" : txt_W_Self1) / 12) * (Math.Round(Convert.ToDouble((Convert.ToDouble(dt_YearEnd) - Convert.ToDouble(dt_Joining.Month))))));
                    ww_Spouse = ((Convert.ToInt32(txt_W_Spouse1 == string.Empty ? "0" : txt_W_Spouse1) / 12) * (Math.Round(Convert.ToDouble((Convert.ToDouble(dt_YearEnd), Convert.ToDouble(dt_Marriage.Month))))));
                }
                else if (W_STATUS == "M" && Convert.ToDateTime(W_MARRIAGE_DATE) > Convert.ToDateTime(W_JOINING_DATE) && dt_Marriage.Year == Convert.ToInt32(OMI_YEAR))
                {
                    /* MARRIED AFTER JOINING AND MARRIAGE IS IN CURRENT YEAR */

                    ww_Self = Convert.ToDouble(txt_W_Self1 == string.Empty ? "0" : txt_W_Self1);
                    ww_Spouse = ((Convert.ToInt32(txt_W_Spouse1 == string.Empty ? "0" : txt_W_Spouse1) / 12) * (Math.Round(Convert.ToDouble((Convert.ToDouble(dt_YearEnd.Month) - Convert.ToDouble(dt_Marriage.Month))))));
                }
                else if (W_STATUS == "M" && dt_Joining.Year < Convert.ToInt32(OMI_YEAR) && ((Convert.ToDateTime(W_MARRIAGE_DATE).Year) < Convert.ToInt32(OMI_YEAR) || dt_Marriage == null))
                {
                    ww_Self = Convert.ToDouble(txt_W_Self1 == string.Empty ? "0" : txt_W_Self1);
                    ww_Spouse = Convert.ToDouble(txt_W_Spouse1 == string.Empty ? "0" : txt_W_Spouse1);
                }
                else if ((W_STATUS == "S" || W_STATUS == "D" || W_STATUS == "W") && (dt_Joining.Year == Convert.ToInt32(OMI_YEAR)))
                {
                    /* SINGLE AND JOINED IN THE CURRENT YEAR */
                    
                    ww_Self = ((Convert.ToInt32(txt_W_Self1 == string.Empty ? "0" : txt_W_Self1) / 12) * (Math.Round(Convert.ToDouble((Convert.ToDateTime(dt_YearEnd), Convert.ToDateTime(W_JOINING_DATE))))));
                }
                else if ((W_STATUS == "S" || W_STATUS == "D" || W_STATUS == "W") && dt_Joining.Year < Convert.ToInt32(OMI_YEAR))
                {
                    /* SINGLE AND JOINED BEFORE CURRENT YEAR*/
                    ww_Self = Convert.ToDouble(txt_W_Self1 == string.Empty ? "0" : txt_W_Self1);
                }

                txt_W_Self1 = ww_Self.ToString();
                txt_W_Spouse1 = ww_Spouse.ToString();


                if ((Convert.ToInt32(txt_Omi_Spouse == string.Empty ? "0" : txt_Omi_Spouse) == 0) && (Convert.ToInt32(txt_Omi_Spouse == string.Empty ? "0" : txt_Omi_Spouse) > 0))
                {
                    txt_Omi_Spouse = txt_W_Spouse1;
                }
            }
            catch (Exception exp)
            {
                MessageBox = "Proc5  '" + exp + "'";
            }
        }

        public void Proc6(Decimal? OMI_P_NO,Decimal? OMI_YEAR)
        {
            try
            {
                string YearEnd = "31/12/" + OMI_YEAR;
                DateTime dt_YearEnd = Convert.ToDateTime(YearEnd);

                Dictionary<string, object> param = new Dictionary<string, object>();
                param.Add("OMI_P_NO", OMI_P_NO);

                Result rsltCode;
                CmnDataManager cmnDM = new CmnDataManager();
                rsltCode = cmnDM.GetData("CHRIS_SP_PreClaim_OMI_CLAIM_MANAGER", "PROC6", param);
                double WW_CHILD = 0;
                int W_OMI = 0;
                g_Child = "0";
                W_OMI = Convert.ToInt32(g_Child);

                if (rsltCode.isSuccessful && rsltCode.dstResult.Tables.Count > 0)
                {
                    if (rsltCode.dstResult.Tables[0].Rows.Count > 0)
                    {


                        foreach (DataRow dr in rsltCode.dstResult.Tables[0].Rows)
                        {
                            if (Convert.ToDateTime(rsltCode.dstResult.Tables[0].Rows[0]["PR_DATE_BIRTH"]).Year < Convert.ToInt32(OMI_YEAR))
                            {
                                WW_CHILD = WW_CHILD + Convert.ToInt32(txt_W_Child1 == string.Empty ? "0" : txt_W_Child1);
                            }
                            else if (Convert.ToDateTime(rsltCode.dstResult.Tables[0].Rows[0]["PR_DATE_BIRTH"]).Year == Convert.ToInt32(OMI_YEAR))
                            {
                                DateTime a = Convert.ToDateTime(rsltCode.dstResult.Tables[0].Rows[0]["PR_DATE_BIRTH"]);

                                WW_CHILD = (Convert.ToInt32(txt_W_Child1 == string.Empty ? "0" : txt_W_Child1) / 12) * (Math.Round(Convert.ToDouble((dt_YearEnd.Month)-(a.Month)))) + WW_CHILD;
                            }

                            txt_W_Child1 = Convert.ToString((WW_CHILD - W_OMI) + W_OMI);
                            txt_Omi_Child = txt_W_Child1;
                        }
                    }
                    else
                    {
                        txt_W_Child1 = Convert.ToString((WW_CHILD - W_OMI) + W_OMI);
                        txt_Omi_Child= txt_W_Child1;
                    }
                }
            }
            catch (Exception exp)
            {
                MessageBox = "Proc6  '" + exp + "'";
            }
        }

        public void Proc9(Decimal? OMI_P_NO, Decimal? OMI_YEAR)
        {
            try
            {
                DateTime PRO_DATE = new DateTime();
                int NO_MONTH = 0;
                string YearEnd = "31/12/" + OMI_YEAR;
                DateTime dt_YearEnd = Convert.ToDateTime(YearEnd);

                Dictionary<string, object> param = new Dictionary<string, object>();
                param.Add("OMI_P_NO", OMI_P_NO);
                param.Add("OMI_YEAR", OMI_YEAR);

                Result rsltCode;
                CmnDataManager cmnDM = new CmnDataManager();
                rsltCode = cmnDM.GetData("CHRIS_SP_PreClaim_OMI_CLAIM_MANAGER", "PROC9", param);

                if (rsltCode.isSuccessful && rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count == 1)
                {
                    DateTime a = Convert.ToDateTime(YearEnd);
                    PRO_DATE = Convert.ToDateTime(rsltCode.dstResult.Tables[0].Rows[0]["PR_EFFECTIVE"].ToString());
                    DateTime b = PRO_DATE;
                    // NO_MONTH = base.MonthsBetweenInOracle(Convert.ToDateTime(YearEnd), Convert.ToDateTime(PRO_DATE));
                    NO_MONTH = (a.Month)-(PRO_DATE.Month);

                    if (Convert.ToInt32(txt_W_Self1 == string.Empty ? "0" : txt_W_Self1) > 0)
                    {
                        txt_W_Self1 = Convert.ToString((Convert.ToInt32(txt_W_Self1) * NO_MONTH) / 12);
                    }

                    if (Convert.ToInt32(txt_W_Spouse1 == string.Empty ? "0" : txt_W_Spouse1) > 0)
                    {
                        txt_W_Spouse1 = Convert.ToString((Convert.ToInt32(txt_W_Spouse1) * NO_MONTH) / 12);
                    }

                    if (Convert.ToInt32(txt_W_Child1 == string.Empty ? "0" : txt_W_Child1) > 0)
                    {
                        txt_W_Child1 = Convert.ToString((Convert.ToInt32(txt_W_Child1) * NO_MONTH) / 12);
                    }

                    int Omi_Limit = Convert.ToInt32(txt_W_Child1 == string.Empty ? "0" : txt_W_Child1) + Convert.ToInt32(txt_W_Spouse1 == string.Empty ? "0" : txt_W_Spouse1) + Convert.ToInt32(txt_W_Self1 == string.Empty ? "0" : txt_W_Self1);
                    txt_W_Omi_Limit = Omi_Limit.ToString();

                }
            }
            catch (Exception exp)
            {
                MessageBox="proc9  '"+exp+"'";
            }
        }

        public void Proc8(Decimal? OMI_P_NO, Decimal? OMI_YEAR)
        {
            try
            {
                int SELF = 0;
                int WIFE = 0;
                int SON = 0;

                /*Self*/
                Dictionary<string, object> paramSelf = new Dictionary<string, object>();
                paramSelf.Add("OMI_P_NO", OMI_P_NO);
                paramSelf.Add("OMI_YEAR", OMI_YEAR);

                Result rsltCodeS;
                CmnDataManager cmnDM = new CmnDataManager();
                rsltCodeS = cmnDM.GetData("CHRIS_SP_PreClaim_OMI_CLAIM_MANAGER", "PROC8_SELF", paramSelf);

                if (rsltCodeS.isSuccessful && rsltCodeS.dstResult.Tables.Count > 0 && rsltCodeS.dstResult.Tables[0].Rows.Count > 0)
                {
                    SELF = Convert.ToInt32(rsltCodeS.dstResult.Tables[0].Rows[0]["SELF"].ToString() == string.Empty ? "0" : rsltCodeS.dstResult.Tables[0].Rows[0]["SELF"].ToString());
                }


                /*WIFE*/
                Dictionary<string, object> paramWife = new Dictionary<string, object>();
                paramWife.Add("OMI_P_NO", OMI_P_NO);
                paramWife.Add("OMI_YEAR", OMI_YEAR);

                Result rsltCodeW;
                rsltCodeW = cmnDM.GetData("CHRIS_SP_PreClaim_OMI_CLAIM_MANAGER", "PROC8_WIFE", paramWife);

                if (rsltCodeW.isSuccessful && rsltCodeW.dstResult.Tables.Count > 0 && rsltCodeW.dstResult.Tables[0].Rows.Count > 0)
                {
                    WIFE = Convert.ToInt32(rsltCodeW.dstResult.Tables[0].Rows[0]["WIFE"].ToString() == string.Empty ? "0" : rsltCodeW.dstResult.Tables[0].Rows[0]["WIFE"].ToString());
                }

                /*WIFE*/
                Dictionary<string, object> paramSon = new Dictionary<string, object>();
                paramSon.Add("OMI_P_NO", OMI_P_NO);
                paramSon.Add("OMI_YEAR", OMI_YEAR);

                Result rsltCodeSon;
                rsltCodeSon = cmnDM.GetData("CHRIS_SP_PreClaim_OMI_CLAIM_MANAGER", "PROC8_SON", paramSon);

                if (rsltCodeSon.isSuccessful && rsltCodeSon.dstResult.Tables.Count > 0 && rsltCodeSon.dstResult.Tables[0].Rows.Count > 0)
                {
                    SON = Convert.ToInt32(rsltCodeSon.dstResult.Tables[0].Rows[0]["SON"].ToString() == string.Empty ? "0" : rsltCodeSon.dstResult.Tables[0].Rows[0]["SON"].ToString());
                }

                txt_Omi_Self = Convert.ToString((Convert.ToInt32(txt_W_Self1 == string.Empty ? "0" : txt_W_Self1)) + SELF);
                txt_Omi_Spouse = Convert.ToString((Convert.ToInt32(txt_W_Spouse1 == string.Empty ? "0" : txt_W_Spouse1)) + WIFE);
                txt_Omi_Child = Convert.ToString((Convert.ToInt32(txt_W_Child1 == string.Empty ? "0" : txt_W_Child1)) + SON);
                txt_Omi_Total_Claim = Convert.ToString(SELF + WIFE + SON);

            }
            catch (Exception exp)
            {
                MessageBox = "proc8  '" + exp + "'";
            }
        }
        #endregion

        #endregion

        #region PolicyInfo_Detail
        public IActionResult PolicyInfo()
        {
            return View();
        }
        public IActionResult PolicyInfo_Detail()
        {
            return View();
        }

        public JsonResult btn_POL_POLICY_NO_Modal(string SEARCHFILTER)
        {
            //string SessionUser = HttpContext.Session.GetString("USER_ID").ToString();

            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("SEARCHFILTER", SEARCHFILTER);

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_MedicalInsurance_PolicyInfoPOLICY_MASTER_MANAGER", "W_POLICY_LOV", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        public JsonResult btn_POL_BRANCH()
        {
            //string SessionUser = HttpContext.Session.GetString("USER_ID").ToString();

            Dictionary<string, object> d = new Dictionary<string, object>();
           

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_MedicalInsurance_PolicyInfoPOLICY_MASTER_MANAGER", "Branch_LOV", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        [HttpPost]
        public JsonResult Upsert_PolicyInfo_Detail(string POL_POLICY_NO, string POL_BRANCH, string POL_SEGMENT, string POL_POLICY_TYPE, string POL_CTT_OFF, DateTime? POL_FROM, DateTime? POL_TO, string POL_CONTACT_PER, string POL_COMP_NAME1, string POL_COMP_NAME2, string POL_ADD1, string POL_ADD2, string POL_ADD3, string POL_ADD4, Decimal? POL_COV_MULTI, Decimal? POL_MAX_LIMIT, Decimal? POL_MAX_ACC, Decimal? POL_CTT_COV, Decimal? POL_CTT_ACC, Decimal? POL_AILMENT, Decimal? POL_DELV_LIMIT, Decimal? POL_ROOM_CHG, Decimal? POL_OMI_LIMIT, Decimal? POL_OMI_SELF, Decimal? POL_PER_DEPEN, Decimal? POL_NO_DEPEN, int ID)
        {
            try
            {
                Dictionary<string, object> d = new Dictionary<string, object>();

                d.Add("POL_POLICY_NO", POL_POLICY_NO);
                d.Add("POL_BRANCH" ,POL_BRANCH);
                d.Add("POL_SEGMENT" ,POL_SEGMENT);
                d.Add("POL_POLICY_TYPE" ,POL_POLICY_TYPE);
                d.Add("POL_CTT_OFF" ,POL_CTT_OFF);
                d.Add("POL_FROM" ,POL_FROM);
                d.Add("POL_TO" ,POL_TO);
                d.Add("POL_CONTACT_PER" ,POL_CONTACT_PER);
                d.Add("POL_COMP_NAME1" ,POL_COMP_NAME1);
                d.Add("POL_COMP_NAME2" ,POL_COMP_NAME2);
                d.Add("POL_ADD1" ,POL_ADD1);
                d.Add("POL_ADD2" ,POL_ADD2);
                d.Add("POL_ADD3" ,POL_ADD3);
                d.Add("POL_ADD4" ,POL_ADD4);
                d.Add("POL_COV_MULTI" ,POL_COV_MULTI);
                d.Add("POL_MAX_LIMIT" ,POL_MAX_LIMIT);
                d.Add("POL_MAX_ACC" ,POL_MAX_ACC);
                d.Add("POL_CTT_COV" ,POL_CTT_COV);
                d.Add("POL_CTT_ACC" ,POL_CTT_ACC);
                d.Add("POL_AILMENT" ,POL_AILMENT);
                d.Add("POL_DELV_LIMIT" ,POL_DELV_LIMIT);
                d.Add("POL_ROOM_CHG" ,POL_ROOM_CHG);
                d.Add("POL_OMI_LIMIT" ,POL_OMI_LIMIT);
                d.Add("POL_OMI_SELF" ,POL_OMI_SELF);
                d.Add("POL_PER_DEPEN" ,POL_PER_DEPEN);
                d.Add("POL_NO_DEPEN" ,POL_NO_DEPEN);
                d.Add("ID", ID);
               

                CmnDataManager objCmnDataManager = new CmnDataManager();
                if (ID > 0)
                {

                    Result rsltCode = objCmnDataManager.GetData("CHRIS_MedicalInsurance_PolicyInfoPOLICY_MASTER_MANAGER", "Update", d);
                    if (rsltCode.isSuccessful)
                    {
                        var res = Json(rsltCode);
                        return Json("Your request is successfully Update !");
                    }
                }
                else
                {
                    Result rsltCode = objCmnDataManager.GetData("CHRIS_MedicalInsurance_PolicyInfoPOLICY_MASTER_MANAGER", "Save", d);
                    if (rsltCode.isSuccessful)
                    {
                        var res = Json(rsltCode);
                        return Json("Your request is successfully Save !");
                    }
                }

                return Json(null);
            }
            catch
            {
                return Json(null);
            }
        }


        public IActionResult PolicyInformation()
        {
            return View();
        }

        #endregion

        #region PremInfo_GHIDE02
        public IActionResult PremInfo_GHIDE02()
        {
            return View();
        }

        public JsonResult btn_GH_POLICY_LOV()
        {
            Dictionary<string, object> d = new Dictionary<string, object>();

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_PremInfo_PREMIUM_MASTER_MANAGER", "GH_POLICY_LOV", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }
        public JsonResult tbl_PremInfo_GHIDE02()
        {
            Dictionary<string, object> d = new Dictionary<string, object>();

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_PremInfo_PREMIUM_MASTER_MANAGER", "List", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }
       
        [HttpPost]
        public JsonResult Upsert_PremInfo_GHIDE02(string PRE_POLICY, DateTime? PRE_PAYMENT_DATE, Decimal? PRE_PREMIUM_AMT, string PRE_BILL_NO, string PRE_MC_NO, string PRE_SUBJECT, int ID)
        {
            try
            {
                Dictionary<string, object> d = new Dictionary<string, object>();

                d.Add("PRE_POLICY", PRE_POLICY);
                d.Add("PRE_PAYMENT_DATE", PRE_PAYMENT_DATE);
                d.Add("PRE_PREMIUM_AMT", PRE_PREMIUM_AMT);
                d.Add("PRE_BILL_NO", PRE_BILL_NO);
                d.Add("PRE_MC_NO", PRE_MC_NO);
                d.Add("PRE_SUBJECT", PRE_SUBJECT);
                d.Add("ID", ID);


                CmnDataManager objCmnDataManager = new CmnDataManager();
                if (ID > 0)
                {

                    Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_PremInfo_PREMIUM_MASTER_MANAGER", "Update", d);
                    if (rsltCode.isSuccessful)
                    {
                        var res = Json(rsltCode);
                        return Json("Your request is successfully Update !");
                    }
                }
                else
                {
                    Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_PremInfo_PREMIUM_MASTER_MANAGER", "Save", d);
                    if (rsltCode.isSuccessful)
                    {
                        var res = Json(rsltCode);
                        return Json("Your request is successfully Save !");
                    }
                }

                return Json(null);
            }
            catch
            {
                return Json(null);
            }
        }

        public JsonResult btn_PremInfo_GHIDE02_Del(int? ID)
        {
            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("ID", ID);

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_PremInfo_PREMIUM_MASTER_MANAGER", "DELETE", d);
            if (rsltCode.isSuccessful)
            {
                return Json("Record deleted successfully.");
            }
            return Json(null);
        }



        #endregion


        #region PremInfo_GLIDE02

        public IActionResult PremInfo_GLIDE02()
        {
            return View();
        }

        public JsonResult btn_PRE_POLICY()
        {
            Dictionary<string, object> d = new Dictionary<string, object>();

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_PremInfo_PREMIUM_MASTER_MANAGER", "POLICY_LOV", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }
        public JsonResult tbl_PI_GLIDE02()
        {
            Dictionary<string, object> d = new Dictionary<string, object>();

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_PremInfo_PREMIUM_MASTER_MANAGER", "List", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }
        public JsonResult btn_W_Type(string SEARCHFILTER)
        {
            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("SEARCHFILTER", SEARCHFILTER);

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_PremInfo_PREMIUM_MASTER_MANAGER", "OCTYPE", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }


        [HttpPost]
        public JsonResult Upsert_PI_GLIDE02(string PRE_POLICY, DateTime? PRE_PAYMENT_DATE, Decimal? PRE_PREMIUM_AMT, string PRE_BILL_NO, string PRE_MC_NO, string PRE_SUBJECT, int ID)
        {
            try
            {
                Dictionary<string, object> d = new Dictionary<string, object>();

                d.Add("PRE_POLICY" ,PRE_POLICY); 
                d.Add("PRE_PAYMENT_DATE" ,PRE_PAYMENT_DATE);
	   			d.Add("PRE_PREMIUM_AMT" ,PRE_PREMIUM_AMT);
	   			d.Add("PRE_BILL_NO" ,PRE_BILL_NO);
	   			d.Add("PRE_MC_NO" ,PRE_MC_NO);
                d.Add("PRE_SUBJECT", PRE_SUBJECT);
                d.Add("ID", ID);


                CmnDataManager objCmnDataManager = new CmnDataManager();
                if (ID > 0)
                {

                    Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_PremInfo_PREMIUM_MASTER_MANAGER", "Update", d);
                    if (rsltCode.isSuccessful)
                    {
                        var res = Json(rsltCode);
                        return Json("Your request is successfully Update !");
                    }
                }
                else
                {
                    Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_PremInfo_PREMIUM_MASTER_MANAGER", "Save", d);
                    if (rsltCode.isSuccessful)
                    {
                        var res = Json(rsltCode);
                        return Json("Your request is successfully Save !");
                    }
                }

                return Json(null);
            }
            catch
            {
                return Json(null);
            }
        }


        public JsonResult btn_PremInfo_GLIDE02_Del(int? ID)
        {
            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("ID", ID);

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_PremInfo_PREMIUM_MASTER_MANAGER", "DELETE", d);
            if (rsltCode.isSuccessful)
            {
                return Json("Record deleted successfully.");
            }
            return Json(null);
        }
        #endregion

        #region PremInfo_OMIDE02

        public IActionResult PremInfo_OMIDE02()
        {
            return View();
        }

        public JsonResult btn_POLICY_LOV_OMI()
        {
            Dictionary<string, object> d = new Dictionary<string, object>();

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_PremInfo_PREMIUM_MASTER_MANAGER", "POLICY_LOV_OMI", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }
        public JsonResult tbl_PremInfo_OMIDE02()
        {
            Dictionary<string, object> d = new Dictionary<string, object>();

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_PremInfo_PREMIUM_MASTER_MANAGER", "List", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }
        public JsonResult btn_OCTYPE(string SEARCHFILTER)
        {
            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("SEARCHFILTER", SEARCHFILTER);

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_PremInfo_PREMIUM_MASTER_MANAGER", "OCTYPE", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }


        [HttpPost]
        public JsonResult Upsert_PremInfo_OMIDE02(string PRE_POLICY, DateTime? PRE_PAYMENT_DATE, Decimal? PRE_PREMIUM_AMT, string PRE_BILL_NO, string PRE_MC_NO, string PRE_SUBJECT, int ID)
        {
            try
            {
                Dictionary<string, object> d = new Dictionary<string, object>();

                d.Add("PRE_POLICY", PRE_POLICY);
                d.Add("PRE_PAYMENT_DATE" ,PRE_PAYMENT_DATE);
                d.Add("PRE_PREMIUM_AMT" ,PRE_PREMIUM_AMT);
                d.Add("PRE_BILL_NO" ,PRE_BILL_NO);
                d.Add("PRE_MC_NO" ,PRE_MC_NO);
                d.Add("PRE_SUBJECT" ,PRE_SUBJECT);
                d.Add("ID", ID);


                CmnDataManager objCmnDataManager = new CmnDataManager();
                if (ID > 0)
                {

                    Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_PremInfo_PREMIUM_MASTER_MANAGER", "Update", d);
                    if (rsltCode.isSuccessful)
                    {
                        var res = Json(rsltCode);
                        return Json("Your request is successfully Update !");
                    }
                }
                else
                {
                    Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_PremInfo_PREMIUM_MASTER_MANAGER", "Save", d);
                    if (rsltCode.isSuccessful)
                    {
                        var res = Json(rsltCode);
                        return Json("Your request is successfully Save !");
                    }
                }

                return Json(null);
            }
            catch
            {
                return Json(null);
            }
        }

        public JsonResult btn_PremInfo_OMIDE02_Del(int? ID)
        {
            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("ID", ID);

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_PremInfo_PREMIUM_MASTER_MANAGER", "DELETE", d);
            if (rsltCode.isSuccessful)
            {               
                return Json("Record deleted successfully.");
            }
            return Json(null);
        }

        #endregion

        public IActionResult PremiumLetter()
        {
            return View();
        }
        public IActionResult StMedIns()
        {
            return View();
        }
        public IActionResult YearBalRep()
        {
            return View();
        }
    }
}
