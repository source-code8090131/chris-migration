﻿using CHRIS.Common;
using CHRIS.Data;
using CHRIS.Services;
using DocumentFormat.OpenXml.Office2010.Excel;
using DocumentFormat.OpenXml.Wordprocessing;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion.Internal;
using Newtonsoft.Json;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Reflection;

namespace CHRIS.Controllers
{
    public class PersonnelController : Controller
    {
        string MessageBox = "";

        #region Confirmation Entry
        public IActionResult ConfirmationEntry()
        {
            return View();
        }
        [HttpPost]
        public JsonResult FillConfirmationEntryList(string Month)
        {
            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("month", Month);
            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_Personnel_confirmation_MANAGER", "Pr_Confirm", d);
            if (rsltCode.isSuccessful)
            {
                if (rsltCode.dstResult.Tables.Count > 0)
                {
                    if (rsltCode.dstResult.Tables[0].Rows.Count > 0)
                    {
                        DataTable firstTable = rsltCode.dstResult.Tables[0];

                        var CatResul = JsonConvert.SerializeObject(firstTable);
                        return Json(CatResul);
                    }
                    else
                        return Json("No Confirmation Due For This Month");
                }
            }
            return Json(null);
        }
        [HttpPost]
        public JsonResult AddUpdateConfirmationEntry(int Id, string PrNo, string Name, string ConfirmationDueDate, string ConfirmCheck, string ConfirmationDate, string ExpectedDate)
        {
            Dictionary<string, object> d = new Dictionary<string, object>();

            d.Add("PR_P_NO", PrNo);
            d.Add("PR_FIRST_NAME", Name);
            d.Add("PR_CONFIRM", ConfirmationDueDate);
            d.Add("PR_CLOSE_FLAG", ConfirmCheck);
            d.Add("PR_CONFIRM_ON", ConfirmationDate);
            d.Add("PR_EXPECTED", ExpectedDate);
            d.Add("PR_CONF_FLAG", null);
            d.Add("ID", Id);

            CmnDataManager objCmnDataManager = new CmnDataManager();
            if (Id > 0)
            {
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_Personnel_confirmation_MANAGER", "Update", d);
                if (rsltCode.isSuccessful)
                {
                    return Json("Data updated successfully.");
                }
                else
                    return Json("Unable to update record.");
            }
            else
            {
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_Personnel_confirmation_MANAGER", "Save", d);
                if (rsltCode.isSuccessful)
                {
                    return Json("Data inserted successfully.");
                }
                else
                    return Json("Unable to insert record.");
            }
        }
        public JsonResult DeleteConfirmationEntryRecord(int Id)
        {
            Dictionary<string, object> d = new Dictionary<string, object>();

            d.Add("ID", Id);

            CmnDataManager objCmnDataManager = new CmnDataManager();
            if (Id > 0)
            {
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_Personnel_confirmation_MANAGER", "Delete", d);
                if (rsltCode.isSuccessful)
                {
                    return Json("Data deleted successfully.");
                }
                else
                    return Json("Unable to delete record.");
            }
            return Json(null);
        }
        #endregion

        #region Regular Staff Hiring Entry
        public IActionResult RegularStaffHiringEnt()
        {
            return View();
        }
        [HttpPost]
        public JsonResult GetPRNO()
        {
            CHRIS_Context ic = new CHRIS_Context();
            var context = new CHRIS_ContextProcedures(ic);
            OutputParameter<int?> outParam = new OutputParameter<int?>();
            var res = context.CHRIS_SP_RegStHiEnt_PERSONNEL_GETALLAsync(outParam);
            res.Wait();
            return Json(res.Result);

        }
        [HttpPost]
        public JsonResult FillRegularStaffForm(string PRNO)
        {
            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("PR_P_NO", PRNO);
            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_RegStHiEnt_PERSONNEL_MANAGER", "PERSONNELSINFOEXIST", d);
            if (rsltCode.isSuccessful)
            {
                if (rsltCode.dstResult.Tables.Count > 0)
                {
                    if (rsltCode.dstResult.Tables[0].Rows.Count > 0)
                    {
                        DataTable firstTable = rsltCode.dstResult.Tables[0];

                        var CatResul = JsonConvert.SerializeObject(firstTable);
                        return Json(CatResul);
                    }
                }
            }
            return Json(null);
        }
        [HttpPost]
        public JsonResult GetBranchForRegularStaff()
        {
            Dictionary<string, object> d = new Dictionary<string, object>();

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_RegStHiEnt_PERSONNEL_MANAGER", "BRANCH", d);
            if (rsltCode.isSuccessful)
            {
                if (rsltCode.dstResult.Tables.Count > 0)
                {
                    if (rsltCode.dstResult.Tables[0].Rows.Count > 0)
                    {
                        DataTable firstTable = rsltCode.dstResult.Tables[0];
                        var CatResul = JsonConvert.SerializeObject(firstTable);
                        return Json(CatResul);
                    }
                }
            }
            return Json(null);

        }
        [HttpPost]
        public JsonResult ValidateBranchForRegularStaff(string BranchCode)
        {
            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("PR_BRANCH", BranchCode);
            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_RegStHiEnt_PERSONNEL_MANAGER", "BRANCHEXIST", d);
            if (rsltCode.isSuccessful)
            {
                if (rsltCode.dstResult.Tables.Count > 0)
                {
                    if (rsltCode.dstResult.Tables[0].Rows.Count > 0)
                    {
                        return Json("Success");
                    }
                    else
                        return Json("Fail");
                }
                else
                    return Json("Fail");
            }
            return Json(null);

        }
        [HttpPost]
        public JsonResult GetReportNoListForRegularStaff()
        {
            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("SEARCHFILTER", 0);
            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_RegStHiEnt_PERSONNEL_MANAGER", "REP_NO", d);
            if (rsltCode.isSuccessful)
            {
                if (rsltCode.dstResult.Tables.Count > 0)
                {
                    if (rsltCode.dstResult.Tables[0].Rows.Count > 0)
                    {
                        DataTable firstTable = rsltCode.dstResult.Tables[0];
                        var CatResul = JsonConvert.SerializeObject(firstTable);
                        return Json(CatResul);
                    }
                }
            }
            return Json(null);

        }
        [HttpPost]
        public JsonResult GetDesignationCodeListForRegularStaff()
        {
            Dictionary<string, object> d = new Dictionary<string, object>();
            //d.Add("SEARCHFILTER", 0);
            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_RegStHiEnt_PERSONNEL_MANAGER", "DESIGNATION", d);
            if (rsltCode.isSuccessful)
            {
                if (rsltCode.dstResult.Tables.Count > 0)
                {
                    if (rsltCode.dstResult.Tables[0].Rows.Count > 0)
                    {
                        DataTable firstTable = rsltCode.dstResult.Tables[0];
                        var CatResul = JsonConvert.SerializeObject(firstTable);
                        return Json(CatResul);
                    }
                }
            }
            return Json(null);

        }
        [HttpPost]
        public JsonResult ValidateDesignationCodeRegularStaff(string DesignationCode)
        {
            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("PR_DESIG", DesignationCode);
            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_RegStHiEnt_PERSONNEL_MANAGER", "DESIGNATION_EXIST", d);
            if (rsltCode.isSuccessful)
            {
                if (rsltCode.dstResult.Tables.Count > 0)
                {
                    if (rsltCode.dstResult.Tables[0].Rows.Count > 0)
                    {
                        return Json("Success");
                    }
                    else
                        return Json("Fail");
                }
                else
                    return Json("Fail");
            }
            return Json(null);

        }
        [HttpPost]
        public JsonResult GetLevelListForRegularStaff()
        {
            Dictionary<string, object> d = new Dictionary<string, object>();
            //d.Add("SEARCHFILTER", 0);
            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_RegStHiEnt_PERSONNEL_MANAGER", "DESG_LEVEL", d);
            if (rsltCode.isSuccessful)
            {
                if (rsltCode.dstResult.Tables.Count > 0)
                {
                    if (rsltCode.dstResult.Tables[0].Rows.Count > 0)
                    {
                        DataTable firstTable = rsltCode.dstResult.Tables[0];
                        var CatResul = JsonConvert.SerializeObject(firstTable);
                        return Json(CatResul);
                    }
                }
            }
            return Json(null);
        }
        [HttpPost]
        public JsonResult ValidateLevelRegularStaff(string CheckLevel)
        {
            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("PR_LEVEL", CheckLevel);
            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_RegStHiEnt_PERSONNEL_MANAGER", "DESG_LEVEL_EXIST", d);
            if (rsltCode.isSuccessful)
            {
                if (rsltCode.dstResult.Tables.Count > 0)
                {
                    if (rsltCode.dstResult.Tables[0].Rows.Count > 0)
                    {
                        return Json("Success");
                    }
                    else
                        return Json("Fail");
                }
                else
                    return Json("Fail");
            }
            return Json(null);

        }
        [HttpPost]
        public JsonResult GetLatestPRNO()
        {
            DataTable dtFPH = new DataTable();
            Dictionary<string, object> d = new Dictionary<string, object>();
            //d.Add("SEARCHFILTER", 0);
            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_RegStHiEnt_PERSONNEL_MANAGER", "SerialNO", d);
            if (rsltCode.isSuccessful)
            {
                if (rsltCode.dstResult.Tables.Count > 0)
                {
                    if (rsltCode.dstResult.Tables[0].Rows.Count > 0)
                    {
                        dtFPH = rsltCode.dstResult.Tables[0];
                    }
                    if (dtFPH.Rows.Count > 0)
                    {
                        string LatestPRNO = dtFPH.Rows[0]["Column1"].ToString().ToUpper();
                        return Json(LatestPRNO);
                    }
                }
            }
            return Json(null);
        }
        [HttpPost]
        public JsonResult AddUpdateRegularStaffHiringMasterData(int Id, string PrNo, string Branch, string Location, string FirstName, string LastName, string ReportName, string Desig, string Level,
            string SoeId, string GeId, string FunTitle1, string FunTitle2, string EmpType, string Segment, string Department, string JoiningDate, string Confirm, string AnnualPack, string NationTax,
            string AccountNo, string CostCode, string BankId, string Issue, string Expiry, string TaxInc, string TaxPaid, string Email, string Action)
        {
            Dictionary<string, object> d = new Dictionary<string, object>();
            if (Action == "V : View")
                return Json(null);

            d.Add("PR_P_NO", PrNo);
            d.Add("PR_BRANCH", Branch);
            d.Add("PR_FIRST_NAME", FirstName);
            d.Add("PR_LAST_NAME", LastName);
            d.Add("PR_DESIG", Desig);
            d.Add("PR_LEVEL", Level);
            d.Add("PR_CATEGORY", null);
            d.Add("PR_FUNC_TITTLE1", FunTitle1);
            d.Add("PR_FUNC_TITTLE2", FunTitle2);
            d.Add("PR_JOINING_DATE", JoiningDate);
            d.Add("PR_CONFIRM", Confirm);
            d.Add("PR_ANNUAL_PACK", AnnualPack);
            d.Add("PR_NATIONAL_TAX", NationTax);
            d.Add("PR_ACCOUNT_NO", AccountNo);
            d.Add("PR_BANK_ID", BankId);
            d.Add("PR_ID_ISSUE", Issue);
            d.Add("PR_EXPIRY", Expiry);
            d.Add("PR_TAX_INC", TaxInc);
            d.Add("PR_TAX_PAID", TaxPaid);
            d.Add("PR_TAX_USED", null);
            d.Add("PR_CLOSE_FLAG", null);
            d.Add("PR_CONFIRM_ON", null);
            d.Add("PR_EXPECTED", null);
            d.Add("PR_TERMIN_TYPE", null);
            d.Add("PR_TERMIN_DATE", null);
            d.Add("PR_REASONS", null);
            d.Add("PR_RESIG_RECV", null);
            d.Add("PR_APP_RESIG", null);
            d.Add("PR_EXIT_INTER", null);
            d.Add("PR_ID_RETURN", null);
            d.Add("PR_NOTICE", null);
            d.Add("PR_ARTONY", null);
            d.Add("PR_DELETION", null);
            d.Add("PR_SETTLE", null);
            d.Add("PR_CONF_FLAG", null);
            d.Add("PR_LAST_INCREMENT", null);
            d.Add("PR_NEXT_INCREMENT", null);
            d.Add("PR_TRANSFER_DATE", null);
            d.Add("PR_PROMOTION_DATE", null);
            d.Add("PR_NEW_BRANCH", null);
            d.Add("PR_NEW_ANNUAL_PACK", null);
            d.Add("PR_TRANSFER", null);
            d.Add("PR_MONTH_AWARD", null);
            d.Add("PR_RELIGION", null);
            d.Add("PR_ZAKAT_AMT", null);
            d.Add("PR_MED_FLAG", null);
            d.Add("PR_ATT_FLAG", null);
            d.Add("PR_PAY_FLAG", null);
            d.Add("PR_REP_NO", ReportName);
            d.Add("PR_REFUND_AMT", null);
            d.Add("PR_REFUND_FOR", null);
            d.Add("PR_NTN_CERT_REC", null);
            d.Add("PR_NTN_CARD_REC", null);
            d.Add("PR_EMP_TYPE", EmpType);
            d.Add("LOCATION", Location);
            d.Add("ID", Id);
            d.Add("PR_EMAIL", Email);
            d.Add("PR_UserID", HttpContext.Session.GetString("USER_ID").ToString());
            //d.Add("PR_SEGMENT", Segment);
            //d.Add("PR_DEPT", Department);
            //d.Add("PR_CONTRIBUTION", null);
            //d.Add("PR_COSTCODE", CostCode);
            //d.Add("PR_SOEID", SoeId);
            if (Id == 0)
                d.Add("PR_UploadDate", DateTime.Now);
            d.Add("PR_User_Approved", false);

            CmnDataManager objCmnDataManager = new CmnDataManager();
            if (Id > 0)
            {
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_RegStHiEnt_PERSONNEL_MANAGER", "Update", d);
                if (rsltCode.isSuccessful)
                {
                    return Json(null);
                }
                else
                    return Json("Unable to update record.");
            }
            else
            {
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_RegStHiEnt_PERSONNEL_MANAGER", "SAVE", d);
                if (rsltCode.isSuccessful)
                {
                    return Json(null);
                }
                else
                    return Json("Unable to insert record.");
            }
        }
        [HttpPost]
        public JsonResult ValidateFieldsChange(string PRNO, string Data, string FieldName)
        {
            try
            {
                if (FieldName == "PR_NATIONAL_TAX")
                {
                    string w_name = "";
                    Dictionary<string, object> param = new Dictionary<string, object>();
                    param.Add("PR_P_NO", PRNO);
                    param.Add("PR_NATIONAL_TAX", Data);

                    Result rsltCode;
                    CmnDataManager cmnDM = new CmnDataManager();
                    rsltCode = cmnDM.GetData("CHRIS_SP_RegStHiEnt_PERSONNEL_MANAGER", "NAT_TAX_LEAVE", param);

                    if (rsltCode.isSuccessful && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                        w_name = rsltCode.dstResult.Tables[0].Rows[0].ItemArray[0].ToString();

                    if (w_name != string.Empty)
                        return Json("This Tax Is Given To " + w_name);
                }
                else if (FieldName == "PR_ACCOUNT_NO")
                {
                    if (Data.Trim().Length > 24)
                    {
                        return Json("Account No. length cannot be greater than 24");
                    }
                    if (Data.Trim().Length > 0)
                    {
                        Dictionary<string, object> param = new Dictionary<string, object>();
                        param.Add("PR_P_NO", PRNO);
                        param.Add("PR_ACCOUNT_NO", Data);
                        Result rsltCode;
                        String PR_P_NO;
                        CmnDataManager cmnDM = new CmnDataManager();
                        rsltCode = cmnDM.GetData("CHRIS_SP_RegStHiEnt_PERSONNEL_MANAGER", "ACCOUNT_VALIDATE", param);

                        if (rsltCode.isSuccessful && rsltCode.dstResult.Tables.Count > 0)
                        {
                            PR_P_NO = rsltCode.dstResult.Tables[0].Rows[0].ItemArray[0].ToString();
                            return Json("Account number already used for Personnel Number: " + PR_P_NO);
                        }
                    }
                }
            }
            catch (Exception exp)
            {
                return Json(exp.Message);
            }
            return Json(null);
        }
        [HttpPost]
        public JsonResult AddUpdateRegularStaffHiringEmploymentHistory(string PrNo, int Id, string FromDate, string Todate, string Organization, string Destination, string Remarks)
        {
            Dictionary<string, object> d = new Dictionary<string, object>();

            d.Add("PR_P_NO", PrNo);
            if (FromDate != null && FromDate != "")
            {
                DateTime result = DateTime.ParseExact((string)FromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                d.Add("PR_JOB_FROM", result);
            }
            else
                d.Add("PR_JOB_FROM", null);
            if (Todate != null && Todate != "")
            {
                DateTime result = DateTime.ParseExact((string)Todate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                d.Add("PR_JOB_TO", result);
            }
            else
                d.Add("PR_JOB_TO", null);
            d.Add("PR_ORGANIZ", Organization);
            d.Add("PR_DESIG_PR", Destination);
            d.Add("PR_REMARKS_PR", Remarks);
            d.Add("PR_ADD1", "pr add1");
            d.Add("PR_ADD2", "pr add2");
            d.Add("PR_ADD3", "pr add3");
            d.Add("PR_LET_SNT", null);
            d.Add("PR_ANS_REC", null);
            d.Add("PR_FLAG", null);
            d.Add("PR_ADD4", "PR_ADD4");
            d.Add("ID", Id);
            CmnDataManager objCmnDataManager = new CmnDataManager();
            if (Id > 0)
            {
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_RegStHiEnt_PRE_EMP_MANAGER", "Update", d);
                if (rsltCode.isSuccessful)
                {
                    return Json(null);
                }
                else
                    return Json("Unable to update record.");
            }
            else
            {
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_RegStHiEnt_PRE_EMP_MANAGER", "Save", d);
                if (rsltCode.isSuccessful)
                {
                    return Json(null);
                }
                else
                    return Json("Unable to insert record.");
            }
        }
        [HttpPost]
        public JsonResult DeletePreviousEmploymentHistory(int Id)
        {
            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("ID", Id);

            CmnDataManager objCmnDataManager = new CmnDataManager();
            if (Id > 0)
            {
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_RegStHiEnt_PRE_EMP_MANAGER", "Delete", d);
                if (rsltCode.isSuccessful)
                {
                    return Json("Record deleted successfully.");
                }
                else
                    return Json("Unable to deleted record.");
            }
            return Json(null);
        }
        [HttpPost]
        public JsonResult GetListForRegularStaffHiringEmployementHistroy(string PrNo, string Action)
        {
            if (Action == "PreviousEmploymentHistory")
            {
                Dictionary<string, object> d = new Dictionary<string, object>();
                d.Add("PR_P_NO", PrNo);
                CmnDataManager objCmnDataManager = new CmnDataManager();
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_RegStHiEnt_PRE_EMP_MANAGER", "List", d);
                if (rsltCode.isSuccessful)
                {
                    if (rsltCode.dstResult.Tables.Count > 0)
                    {
                        if (rsltCode.dstResult.Tables[0].Rows.Count > 0)
                        {
                            DataTable firstTable = rsltCode.dstResult.Tables[0];
                            var CatResul = JsonConvert.SerializeObject(firstTable);
                            return Json(CatResul);
                        }
                    }
                }
            }
            else if (Action == "PersonnelInformationList")
            {
                Dictionary<string, object> d = new Dictionary<string, object>();
                d.Add("PR_P_NO", PrNo);
                CmnDataManager objCmnDataManager = new CmnDataManager();
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_RegStHiEnt_CHILDREN_MANAGER", "List", d);
                if (rsltCode.isSuccessful)
                {
                    if (rsltCode.dstResult.Tables.Count > 0)
                    {
                        if (rsltCode.dstResult.Tables[0].Rows.Count > 0)
                        {
                            DataTable firstTable = rsltCode.dstResult.Tables[0];
                            var CatResul = JsonConvert.SerializeObject(firstTable);
                            return Json(CatResul);
                        }
                    }
                }
            }
            else if (Action == "EducationInformationList")
            {
                Dictionary<string, object> d = new Dictionary<string, object>();
                d.Add("PR_P_NO", PrNo);
                CmnDataManager objCmnDataManager = new CmnDataManager();
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_RegStHiEnt_EDUCATION_MANAGER", "List", d);
                if (rsltCode.isSuccessful)
                {
                    if (rsltCode.dstResult.Tables.Count > 0)
                    {
                        if (rsltCode.dstResult.Tables[0].Rows.Count > 0)
                        {
                            DataTable firstTable = rsltCode.dstResult.Tables[0];
                            var CatResul = JsonConvert.SerializeObject(firstTable);
                            return Json(CatResul);
                        }
                    }
                }
            }
            return Json(null);
        }
        [HttpPost]
        public JsonResult GetRegularStaffPersonnelInfo(string PrNo)
        {
            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("PR_P_NO", PrNo);
            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_RegStHiEnt_PERSONAL_MANAGER", "List", d);
            if (rsltCode.isSuccessful)
            {
                if (rsltCode.dstResult.Tables.Count > 0)
                {
                    if (rsltCode.dstResult.Tables[0].Rows.Count > 0)
                    {
                        DataTable firstTable = rsltCode.dstResult.Tables[0];
                        var CatResul = JsonConvert.SerializeObject(firstTable);
                        return Json(CatResul);
                    }
                }
            }
            return Json(null);
        }
        [HttpPost]
        public JsonResult DeletePersonnelInfoormationData(int Id)
        {
            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("ID", Id);

            CmnDataManager objCmnDataManager = new CmnDataManager();
            if (Id > 0)
            {
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_RegStHiEnt_CHILDREN_MANAGER", "Delete", d);
                if (rsltCode.isSuccessful)
                {
                    return Json("Record deleted successfully.");
                }
                else
                    return Json("Unable to deleted record.");
            }
            return Json(null);
        }
        [HttpPost]
        public JsonResult AddUpdatePersonnelInfo(string PrNo, int Id, string ChildName, string ChildDOB)
        {
            Dictionary<string, object> d = new Dictionary<string, object>();

            d.Add("PR_P_NO", PrNo);
            d.Add("PR_CHILD_NAME", ChildName);
            d.Add("PR_DATE_BIRTH", ChildDOB);
            d.Add("PR_CH_SEX", null);
            d.Add("ID", Id);
            CmnDataManager objCmnDataManager = new CmnDataManager();
            if (Id > 0)
            {
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_RegStHiEnt_CHILDREN_MANAGER", "Update", d);
                if (rsltCode.isSuccessful)
                {
                    return Json(null);
                }
                else
                    return Json("Unable to update record.");
            }
            else
            {
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_RegStHiEnt_CHILDREN_MANAGER", "Save", d);
                if (rsltCode.isSuccessful)
                {
                    return Json(null);
                }
                else
                    return Json("Unable to insert record.");
            }
        }
        [HttpPost]
        public JsonResult AddUpdatePersonnelInformationMasterData(int Id, string PrNo, string Address1, string Address2, string Phone1, string Phone2, string EmpDOB,
            string EmpSex, string Lang1, string Lang2, string Lang3, string Lang4, string Lang5, string Lang6, string EmpCardNo, string EMpOldCardNo, string Martial, string DateOfMarriage, string GroupLife,
            string OutPatient, string GroupHosp, string OnPrime, string SpouseName, string SpouseDOB, string NoOfChilds)
        {
            Dictionary<string, object> d = new Dictionary<string, object>();

            d.Add("PR_P_NO", PrNo);
            d.Add("PR_ADD1", Address1);
            d.Add("PR_ADD2", Address2);
            d.Add("PR_PHONE1", Phone1);
            d.Add("PR_PHONE2", Phone2);
            d.Add("PR_D_BIRTH", EmpDOB);
            d.Add("PR_SEX", EmpSex);
            d.Add("PR_MARITAL", Martial);
            d.Add("PR_SPOUSE", SpouseName);
            d.Add("PR_BIRTH_SP", SpouseDOB);
            d.Add("PR_NO_OF_CHILD", NoOfChilds);
            d.Add("PR_ID_CARD_NO", EmpCardNo);
            d.Add("PR_USER_IS_PRIME", OnPrime);
            d.Add("PR_GROUP_LIFE", GroupLife);
            d.Add("PR_GROUP_HOSP", GroupHosp);
            d.Add("PR_LANG_1", Lang1);
            d.Add("PR_LANG_2", Lang2);
            d.Add("PR_LANG_3", Lang3);
            d.Add("PR_LANG_4", Lang4);
            d.Add("PR_LANG_5", Lang5);
            d.Add("PR_LANG_6", Lang6);
            d.Add("PR_OPD", OutPatient);
            d.Add("PR_MARRIAGE", DateOfMarriage);
            d.Add("PR_NATIONALITY", Id);
            d.Add("PR_PER_ADDR", Id);
            d.Add("PR_MOBILE_NO", Id);
            d.Add("PR_PLC_BIRTH", Id);
            d.Add("PR_CRNT_PHONE", Id);
            d.Add("PR_FATHER_NAME", Id);
            d.Add("PR_MOTHER_NAME", Id);
            d.Add("PR_OLD_ID_CARD_NO", EMpOldCardNo);
            d.Add("ID", Id);
            CmnDataManager objCmnDataManager = new CmnDataManager();
            if (Id > 0)
            {
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_RegStHiEnt_PERSONAL_MANAGER", "Update", d);
                if (rsltCode.isSuccessful)
                {
                    return Json(null);
                }
                else
                    return Json("Unable to update record.");
            }
            else
            {
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_RegStHiEnt_PERSONAL_MANAGER", "Save", d);
                if (rsltCode.isSuccessful)
                {
                    return Json(null);
                }
                else
                    return Json("Unable to insert record.");
            }
        }
        public JsonResult AddUpdateEducationInformationListData(int Id, string PrNo, string Year, string Degree, string Grade, string Collage, string City, string Country)
        {
            Dictionary<string, object> d = new Dictionary<string, object>();

            d.Add("PR_P_NO", PrNo);
            d.Add("PR_E_YEAR", Year);
            d.Add("PR_DEGREE", Degree);
            d.Add("PR_GRADE", Grade);
            d.Add("PR_COLLAGE", Collage);
            d.Add("PR_CITY", City);
            d.Add("PR_COUNTRY", Country);
            d.Add("PR_DEG_TYPE", null);
            d.Add("ID", Id);
            CmnDataManager objCmnDataManager = new CmnDataManager();
            if (Id > 0)
            {
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_RegStHiEnt_EDUCATION_MANAGER", "Update", d);
                if (rsltCode.isSuccessful)
                {
                    return Json(null);
                }
                else
                    return Json("Unable to update record.");
            }
            else
            {
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_RegStHiEnt_EDUCATION_MANAGER", "Save", d);
                if (rsltCode.isSuccessful)
                {
                    return Json(null);
                }
                else
                    return Json("Unable to insert record.");
            }
        }
        [HttpPost]
        public JsonResult DeleteEducationInformationData(int Id)
        {
            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("ID", Id);

            CmnDataManager objCmnDataManager = new CmnDataManager();
            if (Id > 0)
            {
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_RegStHiEnt_EDUCATION_MANAGER", "Delete", d);
                if (rsltCode.isSuccessful)
                {
                    return Json("Record deleted successfully.");
                }
                else
                    return Json("Unable to deleted record.");
            }
            return Json(null);
        }
        [HttpPost]
        public JsonResult GetAuthorizationData()
        {
            string SessionUser = HttpContext.Session.GetString("USER_ID").ToString();
            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("PR_UserID", SessionUser);
            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_RegStHiEnt_PERSONNEL_MANAGER", "GET_UNAUTHORIZED_DATA", d);
            if (rsltCode.isSuccessful)
            {
                if (rsltCode.dstResult.Tables.Count > 0)
                {
                    if (rsltCode.dstResult.Tables[0].Rows.Count > 0)
                    {
                        DataTable firstTable = rsltCode.dstResult.Tables[0];
                        var CatResul = JsonConvert.SerializeObject(firstTable);
                        return Json(CatResul);
                    }
                }
            }
            return Json(null);
        }
        [HttpPost]
        public JsonResult AuthorizeRegularStaffData(int ID)
        {
            string SessionUser = HttpContext.Session.GetString("USER_ID").ToString();
            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("PR_UserID", SessionUser);
            d.Add("ID", ID);
            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_RegStHiEnt_PERSONNEL_MANAGER", "AUTHORIZE_REGULAR_STAFF", d);
            if (rsltCode.isSuccessful)
                return Json("Record authorized successfully.");
            else
                return Json("Problem while authorizing data");
            return Json(null);
        }
        [HttpPost]
        public JsonResult DeleteRegularStaffMasterData(int Id)
        {
            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("ID", Id);

            CmnDataManager objCmnDataManager = new CmnDataManager();
            if (Id > 0)
            {
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_RegStHiEnt_PERSONNEL_MANAGER", "DELETE", d);
                if (rsltCode.isSuccessful)
                {
                    return Json("Record deleted successfully.");
                }
                else
                    return Json("Unable to deleted record.");
            }
            return Json(null);
        }
        #endregion

        #region Contractual Staff Entry
        public IActionResult ContractStaffHiringEnt()
        {
            return View();
        }
        [HttpPost]
        public JsonResult GetContractStaffPRNO()
        {
            CHRIS_Context ic = new CHRIS_Context();
            var context = new CHRIS_ContextProcedures(ic);
            OutputParameter<int?> outParam = new OutputParameter<int?>();
            var res = context.CHRIS_SP_CONTRACT_GETALLAsync(outParam);
            res.Wait();
            return Json(res.Result);

        }
        [HttpPost]
        public JsonResult FillContractualStaffForm(string PRNO)
        {
            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("PR_P_NO", PRNO);
            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_CONTRACT_MANAGER", "PR_P_NO_LOV_EXISTS", d);
            if (rsltCode.isSuccessful)
            {
                if (rsltCode.dstResult.Tables.Count > 0)
                {
                    if (rsltCode.dstResult.Tables[0].Rows.Count > 0)
                    {
                        DataTable firstTable = rsltCode.dstResult.Tables[0];

                        var CatResul = JsonConvert.SerializeObject(firstTable);
                        return Json(CatResul);
                    }
                }
            }
            return Json(null);
        }
        [HttpPost]
        public JsonResult GetContractStaffSegmentListAgainstPRNo(string PRNO)
        {
            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("PR_P_NO", PRNO);
            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_CONTR_HIRING_DEPT_CONT_MANAGER", "List", d);
            if (rsltCode.isSuccessful)
            {
                if (rsltCode.dstResult.Tables.Count > 0)
                {
                    if (rsltCode.dstResult.Tables[0].Rows.Count > 0)
                    {
                        DataTable firstTable = rsltCode.dstResult.Tables[0];
                        var CatResul = JsonConvert.SerializeObject(firstTable);
                        return Json(CatResul);
                    }
                }
            }
            return Json(null);
        }
        [HttpPost]
        public JsonResult GetLatestPrNoForContractStaff()
        {
            DataTable dtFPH = new DataTable();
            Dictionary<string, object> d = new Dictionary<string, object>();
            //d.Add("SEARCHFILTER", 0);
            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_CONTRACT_MANAGER", "SER_NO", d);
            if (rsltCode.isSuccessful)
            {
                if (rsltCode.dstResult.Tables.Count > 0)
                {
                    if (rsltCode.dstResult.Tables[0].Rows.Count > 0)
                    {
                        dtFPH = rsltCode.dstResult.Tables[0];
                    }
                    if (dtFPH.Rows.Count > 0)
                    {
                        string LatestPRNO = dtFPH.Rows[0]["SER_NO"].ToString();
                        return Json(LatestPRNO);
                    }
                }
            }
            return Json(null);
        }
        [HttpPost]
        public JsonResult FillContractStaffHiringPopUp(string A)
        {
            Dictionary<string, object> d = new Dictionary<string, object>();
            //d.Add("PR_P_NO", N);
            CmnDataManager objCmnDataManager = new CmnDataManager();
            if (A == "GetBranch")
            {
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_CONTRACT_MANAGER", "BRANCH_LOV", d);
                if (rsltCode.isSuccessful)
                {
                    if (rsltCode.dstResult.Tables.Count > 0)
                    {
                        if (rsltCode.dstResult.Tables[0].Rows.Count > 0)
                        {
                            DataTable firstTable = rsltCode.dstResult.Tables[0];

                            var CatResul = JsonConvert.SerializeObject(firstTable);
                            return Json(CatResul);
                        }
                    }
                }
            }
            else if (A == "GetDesig")
            {
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_CONTRACT_MANAGER", "DESG_LOV", d);
                if (rsltCode.isSuccessful)
                {
                    if (rsltCode.dstResult.Tables.Count > 0)
                    {
                        if (rsltCode.dstResult.Tables[0].Rows.Count > 0)
                        {
                            DataTable firstTable = rsltCode.dstResult.Tables[0];

                            var CatResul = JsonConvert.SerializeObject(firstTable);
                            return Json(CatResul);
                        }
                    }
                }
            }
            else if (A == "GetContractor")
            {
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_CONTRACT_MANAGER", "CONTRACTOR_LOV", d);
                if (rsltCode.isSuccessful)
                {
                    if (rsltCode.dstResult.Tables.Count > 0)
                    {
                        if (rsltCode.dstResult.Tables[0].Rows.Count > 0)
                        {
                            DataTable firstTable = rsltCode.dstResult.Tables[0];

                            var CatResul = JsonConvert.SerializeObject(firstTable);
                            return Json(CatResul);
                        }
                    }
                }
            }
            else if (A == "GetDepartment")
            {
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_CONTR_HIRING_DEPT_CONT_MANAGER", "DEPT_LOV", d);
                if (rsltCode.isSuccessful)
                {
                    if (rsltCode.dstResult.Tables.Count > 0)
                    {
                        if (rsltCode.dstResult.Tables[0].Rows.Count > 0)
                        {
                            DataTable firstTable = rsltCode.dstResult.Tables[0];

                            var CatResul = JsonConvert.SerializeObject(firstTable);
                            return Json(CatResul);
                        }
                    }
                }
            }
            return Json(null);
        }

        [HttpPost]
        public JsonResult AddUpdateContractStaffHiringList(int Id, decimal PrNo, string Segment, string Dept, string Contri)
        {
            Dictionary<string, object> d = new Dictionary<string, object>();

            d.Add("PR_P_NO", PrNo);
            d.Add("PR_SEGMENT", Segment);
            d.Add("PR_DEPT", Dept);
            d.Add("PR_CONTRIB", Contri);
            d.Add("PR_MENU_OPTION", null);
            d.Add("PR_TYPE", 'R');
            d.Add("PR_EFFECTIVE_DATE", null);
            d.Add("ID", Id);

            CmnDataManager objCmnDataManager = new CmnDataManager();
            if (Id > 0)
            {
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_CONTR_HIRING_DEPT_CONT_MANAGER", "Update", d);
                if (rsltCode.isSuccessful)
                {
                    return Json("Data updated successfully.");
                }
                else
                    return Json("Unable to update record.");
            }
            else
            {
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_CONTR_HIRING_DEPT_CONT_MANAGER", "Save", d);
                if (rsltCode.isSuccessful)
                {
                    return Json("Data inserted successfully.");
                }
                else
                    return Json("Unable to insert record.");
            }
        }
        [HttpPost]
        public JsonResult AddUpdateContractStaffHiringRecord(string Branch, string FirstName, string LastName, string Desig, string Contractor,
            string FromDate, string ToDate, string Address, string Phone1, string Phone2, string DOB, string Martial, string Sex, string NID
            , string MonthlySalary, int Id, string PrNo)
        {
            Dictionary<string, object> d = new Dictionary<string, object>();

            CmnDataManager objCmnDataManager = new CmnDataManager();
            if (Id > 0)
            {
                d.Add("PR_P_NO_C", PrNo);
                d.Add("PR_BRANCH", Branch);
                d.Add("PR_FIRST_NAME", FirstName);
                d.Add("PR_LAST_NAME", LastName);
                d.Add("PR_DESIG", Desig);
                d.Add("PR_CONTRACTOR", Contractor);
                d.Add("PR_FROM_DATE", FromDate);
                d.Add("PR_TO_DATE", ToDate);
                d.Add("PR_ADDRESS", Address);
                d.Add("PR_PHONE_1", Phone1);
                d.Add("PR_PHONE_2", Phone2);
                d.Add("PR_DATE_BIRTH", DOB);
                d.Add("PR_MARITAL", Martial);
                d.Add("PR_SEX", Sex);
                d.Add("PR_NID", NID);
                d.Add("PR_MONTHLY_SALARY", MonthlySalary);
                d.Add("PR_FLAG", "");
                d.Add("ID", Id);
                d.Add("PR_UserID", HttpContext.Session.GetString("USER_ID").ToString());
                d.Add("PR_User_Approved", false);
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_CONTRACT_UPDATE", "", d);
                if (rsltCode.isSuccessful)
                {
                    return Json("Data updated successfully.");
                }
                else
                    return Json("Unable to update record.");
            }
            else
            {
                d.Add("PR_BRANCH", Branch);
                d.Add("PR_FIRST_NAME", FirstName);
                d.Add("PR_LAST_NAME", LastName);
                d.Add("PR_DESIG", Desig);
                d.Add("PR_CONTRACTOR", Contractor);
                d.Add("PR_FROM_DATE", FromDate);
                d.Add("PR_TO_DATE", ToDate);
                d.Add("PR_ADDRESS", Address);
                d.Add("PR_PHONE_1", Phone1);
                d.Add("PR_PHONE_2", Phone2);
                d.Add("PR_DATE_BIRTH", DOB);
                d.Add("PR_MARITAL", Martial);
                d.Add("PR_SEX", Sex);
                d.Add("PR_NID", NID);
                d.Add("PR_MONTHLY_SALARY", MonthlySalary);
                d.Add("PR_FLAG", "");
                //d.Add("ID", null);
                d.Add("PR_P_NO_C", PrNo);
                d.Add("PR_UserID", HttpContext.Session.GetString("USER_ID").ToString());
                d.Add("PR_UploadDate", DateTime.Now);
                d.Add("PR_User_Approved", false);
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_CONTRACT_ADD", "", d);
                if (rsltCode.isSuccessful)
                {
                    return Json("Data inserted successfully.");
                }
                else
                    return Json("Unable to insert record.");
            }
        }
        [HttpPost]
        public JsonResult ValidateYearFromDOB(string FromDate, string DOB, string Action)
        {
            int DateDiff;
            Dictionary<string, object> d = new Dictionary<string, object>();

            d.Add("PR_FROM_DATE", FromDate);
            d.Add("PR_DATE_BIRTH", DOB);

            CmnDataManager objCmnDataManager = new CmnDataManager();
            if (FromDate != null && DOB != null)
            {
                if (Action == "ContractStaffHiringEnt")
                {
                    Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_CONTRACT_MANAGER", "MONTH_BETWEEN", d);
                    if (rsltCode.isSuccessful && rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                    {
                        DateDiff = int.Parse(rsltCode.dstResult.Tables[0].Rows[0].ItemArray[0].ToString());

                        if (DateDiff <= 180)
                        {
                            return Json("Age Should be 15 years Less than From Date");
                        }
                    }
                }
                else if (Action == "SumInernsHiring")
                {
                    Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_CONTRACT_MANAGER", "MONTH_BETWEEN", d);
                    if (rsltCode.isSuccessful && rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                    {
                        DateDiff = int.Parse(rsltCode.dstResult.Tables[0].Rows[0].ItemArray[0].ToString());

                        if (DateDiff <= 120)
                        {
                            return Json("Age Should be 10 years Less than From Date");
                        }
                    }
                }
            }
            return Json(null);
        }
        [HttpPost]
        public JsonResult GetAuthorizeContractStaffPRNO(string A)
        {
            CHRIS_Context cont = new CHRIS_Context();
            string SessionUser = HttpContext.Session.GetString("USER_ID").ToString();
            //string SessionUser = "Admin";
            if (A == "ContractStaffHiringEnt")
            {
                Dictionary<string, object> d = new Dictionary<string, object>();
                d.Add("PR_UserID", HttpContext.Session.GetString("USER_ID").ToString());
                CmnDataManager objCmnDataManager = new CmnDataManager();
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_CONTRACT_MANAGER", "GET_UNAUTHORIZE_CONTRACTUAL_ENTRY", d);
                if (rsltCode.isSuccessful)
                {
                    if (rsltCode.dstResult.Tables.Count > 0)
                    {
                        if (rsltCode.dstResult.Tables[0].Rows.Count > 0)
                        {
                            DataTable firstTable = rsltCode.dstResult.Tables[0];
                            var CatResul = JsonConvert.SerializeObject(firstTable);
                            return Json(CatResul);
                        }
                        else
                            return Json(null);
                    }
                    else
                        return Json(null);
                }
                else
                    return Json(null);
            }
            else if (A == "SumInernsHiring")
            {
                Dictionary<string, object> d = new Dictionary<string, object>();
                d.Add("PR_UserID", HttpContext.Session.GetString("USER_ID").ToString());
                CmnDataManager objCmnDataManager = new CmnDataManager();
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_SUMMER_INTERNS_MANAGER", "GET_UNAUTHORIZE_SUMMER_INTERNS", d);
                if (rsltCode.isSuccessful)
                {
                    if (rsltCode.dstResult.Tables.Count > 0)
                    {
                        if (rsltCode.dstResult.Tables[0].Rows.Count > 0)
                        {
                            DataTable firstTable = rsltCode.dstResult.Tables[0];
                            var CatResul = JsonConvert.SerializeObject(firstTable);
                            return Json(CatResul);
                        }
                        else
                            return Json(null);
                    }
                    else
                        return Json(null);
                }
                else
                    return Json(null);
            }
            else if (A == "TerminationEntry")
            {
                Dictionary<string, object> d = new Dictionary<string, object>();
                d.Add("PR_UserID", HttpContext.Session.GetString("USER_ID").ToString());
                CmnDataManager objCmnDataManager = new CmnDataManager();
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_TERMINATION_MANAGER", "GET_UNAUTHORIZE_TERMINATION", d);
                if (rsltCode.isSuccessful)
                {
                    if (rsltCode.dstResult.Tables.Count > 0)
                    {
                        if (rsltCode.dstResult.Tables[0].Rows.Count > 0)
                        {
                            DataTable firstTable = rsltCode.dstResult.Tables[0];
                            var CatResul = JsonConvert.SerializeObject(firstTable);
                            return Json(CatResul);
                        }
                        else
                            return Json(null);
                    }
                    else
                        return Json(null);
                }
                else
                    return Json(null);
            }
            return Json(null);
        }
        [HttpPost]
        public JsonResult FillAuthorizationContractualStaffForm(decimal? PRNO)
        {
            CHRIS_Context cont = new CHRIS_Context();
            CONTRACT_CHK GetRecord = cont.CONTRACT_CHKs.Where(m => m.PR_P_NO_C == PRNO).FirstOrDefault();
            if (GetRecord != null)
            {
                var CatResul = JsonConvert.SerializeObject(GetRecord);
                return Json("[" + CatResul + "]");
            }
            return Json(null);
        }
        [HttpPost]
        public JsonResult DeleteContractHiring(int Id)
        {
            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("ID", Id);

            CmnDataManager objCmnDataManager = new CmnDataManager();
            if (Id > 0)
            {
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_CONTRACT_MANAGER", "Delete", d);
                if (rsltCode.isSuccessful)
                {
                    return Json("Record deleted successfully.");
                }
                else
                    return Json("Unable to deleted record.");
            }
            return Json(null);
        }
        [HttpPost]
        public JsonResult FillAuthorizationSumIntersHiringForm(decimal? PRNO)
        {
            CHRIS_Context cont = new CHRIS_Context();
            SUMMER_INTERNS_CHK GetRecord = cont.SUMMER_INTERNS_CHKs.Where(m => m.PR_I_NO == PRNO).FirstOrDefault();
            if (GetRecord != null)
            {
                var CatResul = JsonConvert.SerializeObject(GetRecord);
                return Json("[" + CatResul + "]");
            }
            return Json(null);
        }
        [HttpPost]
        public JsonResult AuthorizeContractualHiringData(int ID)
        {
            string SessionUser = HttpContext.Session.GetString("USER_ID").ToString();
            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("PR_UserID", SessionUser);
            d.Add("ID", ID);
            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_CONTRACT_MANAGER", "AUTHORIZE_CONTRACTUAL_ENTRY", d);
            if (rsltCode.isSuccessful)
                return Json("Record authorized successfully.");
            else
                return Json("Problem while authorizing data");
        }
        #endregion

        #region Summer Inters Hiring
        public IActionResult SumInernsHiring()
        {
            return View();
        }
        [HttpPost]
        public JsonResult FillSumInterPrNOList(string PRNO)
        {
            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("PR_P_NO", PRNO);
            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_SUMMER_INTERNS_MANAGER", "LIST", d);
            if (rsltCode.isSuccessful)
            {
                if (rsltCode.dstResult.Tables.Count > 0)
                {
                    if (rsltCode.dstResult.Tables[0].Rows.Count > 0)
                    {
                        DataTable firstTable = rsltCode.dstResult.Tables[0];

                        var CatResul = JsonConvert.SerializeObject(firstTable);
                        return Json(CatResul);
                    }
                }
            }
            return Json(null);
        }
        [HttpPost]
        public JsonResult FillSumInterHiringForm(string PRNO)
        {
            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("PR_P_NO", PRNO);
            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_SUMMER_INTERNS_MANAGER", "PR_P_NO_LOV_EXISTS", d);
            if (rsltCode.isSuccessful)
            {
                if (rsltCode.dstResult.Tables.Count > 0)
                {
                    if (rsltCode.dstResult.Tables[0].Rows.Count > 0)
                    {
                        DataTable firstTable = rsltCode.dstResult.Tables[0];

                        var CatResul = JsonConvert.SerializeObject(firstTable);
                        return Json(CatResul);
                    }
                }
            }
            return Json(null);
        }
        [HttpPost]
        public JsonResult GetSumInterHiringListAgainstPRNo(string PRNO)
        {
            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("PR_P_NO", PRNO);
            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_SUMMER_INTERNS_DEPT_CONT_MANAGER", "%SumInernsHiring", d);
            if (rsltCode.isSuccessful)
            {
                if (rsltCode.dstResult.Tables.Count > 0)
                {
                    if (rsltCode.dstResult.Tables[0].Rows.Count > 0)
                    {
                        DataTable firstTable = rsltCode.dstResult.Tables[0];
                        var CatResul = JsonConvert.SerializeObject(firstTable);
                        return Json(CatResul);
                    }
                }
            }
            return Json(null);
        }
        [HttpPost]
        public JsonResult FillSumInterHiringPopUp(string A)
        {
            Dictionary<string, object> d = new Dictionary<string, object>();
            //d.Add("PR_P_NO", N);
            CmnDataManager objCmnDataManager = new CmnDataManager();
            if (A == "GetBranch")
            {
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_SUMMER_INTERNS_MANAGER", "BRANCH_LOV", d);
                if (rsltCode.isSuccessful)
                {
                    if (rsltCode.dstResult.Tables.Count > 0)
                    {
                        if (rsltCode.dstResult.Tables[0].Rows.Count > 0)
                        {
                            DataTable firstTable = rsltCode.dstResult.Tables[0];

                            var CatResul = JsonConvert.SerializeObject(firstTable);
                            return Json(CatResul);
                        }
                    }
                }
            }
            else if (A == "GetDepartment")
            {
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_SUMMER_INTERNS_DEPT_CONT_MANAGER", "DEPT_LOV", d);
                if (rsltCode.isSuccessful)
                {
                    if (rsltCode.dstResult.Tables.Count > 0)
                    {
                        if (rsltCode.dstResult.Tables[0].Rows.Count > 0)
                        {
                            DataTable firstTable = rsltCode.dstResult.Tables[0];

                            var CatResul = JsonConvert.SerializeObject(firstTable);
                            return Json(CatResul);
                        }
                    }
                }
            }
            return Json(null);
        }
        [HttpPost]
        public JsonResult DeleteSumInterHiringData(string PrNO)
        {
            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("PR_P_NO", PrNO);

            CmnDataManager objCmnDataManager = new CmnDataManager();
            if (PrNO != null)
            {
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_SUMMER_INTERNS_MANAGER", "DELETE", d);
                if (rsltCode.isSuccessful)
                {
                    return Json("Record deleted successfully.");
                }
                else
                    return Json("Unable to deleted record.");
            }
            return Json(null);
        }
        [HttpPost]
        public JsonResult AddUpdateSubInterHiringList(int Id, decimal PrNo, string Segment, string Dept, string Contri)
        {
            Dictionary<string, object> d = new Dictionary<string, object>();

            d.Add("PR_P_NO", PrNo);
            d.Add("PR_SEGMENT", Segment);
            d.Add("PR_DEPT", Dept);
            d.Add("PR_CONTRIB", Contri);
            d.Add("PR_MENU_OPTION", null);
            d.Add("PR_TYPE", 'I');
            d.Add("PR_EFFECTIVE_DATE", null);
            d.Add("ID", Id);

            CmnDataManager objCmnDataManager = new CmnDataManager();
            if (Id > 0)
            {
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_SUMMER_INTERNS_DEPT_CONT_MANAGER", "Update", d);
                if (rsltCode.isSuccessful)
                {
                    return Json("Data updated successfully.");
                }
                else
                    return Json("Unable to update record.");
            }
            else
            {
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_SUMMER_INTERNS_DEPT_CONT_MANAGER", "Save", d);
                if (rsltCode.isSuccessful)
                {
                    return Json("Data inserted successfully.");
                }
                else
                    return Json("Unable to insert record.");
            }
        }
        [HttpPost]
        public JsonResult GetLatestPrNoForSumIntersHiring()
        {
            DataTable dtFPH = new DataTable();
            Dictionary<string, object> d = new Dictionary<string, object>();
            //d.Add("SEARCHFILTER", 0);
            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_SUMMER_INTERNS_MANAGER", "SER_NO", d);
            if (rsltCode.isSuccessful)
            {
                if (rsltCode.dstResult.Tables.Count > 0)
                {
                    if (rsltCode.dstResult.Tables[0].Rows.Count > 0)
                    {
                        dtFPH = rsltCode.dstResult.Tables[0];
                    }
                    if (dtFPH.Rows.Count > 0)
                    {
                        string LatestPRNO = dtFPH.Rows[0]["SER_NO"].ToString();
                        return Json(LatestPRNO);
                    }
                }
            }
            return Json(null);
        }
        [HttpPost]
        public JsonResult AddUpdateSumIntersHiringRecord(string Branch, string FirstName, string LastName, string University, string Class,
            string FromDate, string ToDate, string Pre_Internship, string Recomd_To_Hire, string Stipened, string Address, string Phone1, string Phone2,
            string DOB, string Martial, string Sex, string NID, int Id, string PrNo)
        {
            Dictionary<string, object> d = new Dictionary<string, object>();

            CmnDataManager objCmnDataManager = new CmnDataManager();
            if (Id > 0)
            {
                d.Add("PR_P_NO", PrNo);
                d.Add("PR_BRANCH", Branch);
                d.Add("PR_FIRST_NAME", FirstName);
                d.Add("PR_LAST_NAME", LastName);
                d.Add("PR_UNIVERSITY", University);
                d.Add("PR_CLASS", Class);
                d.Add("PR_CON_FROM", FromDate);
                d.Add("PR_CON_TO", ToDate);
                d.Add("PR_ADDRESS", Address);
                d.Add("PR_PHONE1", Phone1);
                d.Add("PR_PHONE2", Phone2);
                d.Add("PR_DATE_BIRTH", DOB);
                d.Add("PR_MAR_STATUS", Martial);
                d.Add("PR_SEX", Sex);
                d.Add("PR_NID_CARD", NID);
                d.Add("PR_STIPENED", Stipened);
                d.Add("PR_INTERNSHIP", Pre_Internship);
                d.Add("PR_RECOM", Recomd_To_Hire);
                d.Add("PR_FLAG", null);
                d.Add("ID", Id);
                d.Add("PR_UserID", HttpContext.Session.GetString("USER_ID").ToString());
                d.Add("PR_User_Approved", false);
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_SUMMER_INTERNS_MANAGER", "UPDATE", d);
                if (rsltCode.isSuccessful)
                {
                    return Json("Data updated successfully.");
                }
                else
                    return Json("Unable to update record.");
            }
            else
            {
                d.Add("PR_P_NO", PrNo);
                d.Add("PR_BRANCH", Branch);
                d.Add("PR_FIRST_NAME", FirstName);
                d.Add("PR_LAST_NAME", LastName);
                d.Add("PR_UNIVERSITY", University);
                d.Add("PR_CLASS", Class);
                d.Add("PR_CON_FROM", FromDate);
                d.Add("PR_CON_TO", ToDate);
                d.Add("PR_ADDRESS", Address);
                d.Add("PR_PHONE1", Phone1);
                d.Add("PR_PHONE2", Phone2);
                d.Add("PR_DATE_BIRTH", DOB);
                d.Add("PR_MAR_STATUS", Martial);
                d.Add("PR_SEX", Sex);
                d.Add("PR_NID_CARD", NID);
                d.Add("PR_STIPENED", Stipened);
                d.Add("PR_INTERNSHIP", Pre_Internship);
                d.Add("PR_RECOM", Recomd_To_Hire);
                d.Add("PR_FLAG", null);
                //d.Add("ID", Id);
                d.Add("PR_UserID", HttpContext.Session.GetString("USER_ID").ToString());
                d.Add("PR_UploadDate", DateTime.Now);
                d.Add("PR_User_Approved", false);
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_SUMMER_INTERNS_MANAGER", "SAVE", d);
                if (rsltCode.isSuccessful)
                {
                    return Json("Data inserted successfully.");
                }
                else
                    return Json("Unable to insert record.");
            }
        }
        public JsonResult AuthorizeSumInternHiring(int ID)
        {
            string SessionUser = HttpContext.Session.GetString("USER_ID").ToString();
            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("PR_UserID", SessionUser);
            d.Add("ID", ID);
            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_SUMMER_INTERNS_MANAGER", "AUTHORIZE_SUMMER_INTERNS", d);
            if (rsltCode.isSuccessful)
                return Json("Record authorized successfully.");
            else
                return Json("Problem while authorizing data");
            return Json(null);
        }
        #endregion

        #region Termination Entry
        public IActionResult TerminationEntry()
        {
            return View();
        }
        [HttpPost]
        public JsonResult TerminationPrNOList(string PRNO)
        {
            Dictionary<string, object> d = new Dictionary<string, object>();
            //d.Add("PR_P_NO", PRNO);
            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_TERMINATION_MANAGER", "P_LOV", d);
            if (rsltCode.isSuccessful)
            {
                if (rsltCode.dstResult.Tables.Count > 0)
                {
                    if (rsltCode.dstResult.Tables[0].Rows.Count > 0)
                    {
                        DataTable firstTable = rsltCode.dstResult.Tables[0];

                        var CatResul = JsonConvert.SerializeObject(firstTable);
                        return Json(CatResul);
                    }
                }
            }
            return Json(null);
        }
        [HttpPost]
        public JsonResult ValidateTerminationPrNOExist(decimal PRNO)
        {
            CHRIS_Context cont = new CHRIS_Context();
            if (PRNO > 0)
            {
                Personnel GetRecord = cont.Personnel.Where(m => m.PR_P_NO == PRNO).FirstOrDefault();
                if (GetRecord != null)
                {
                    var CatResul = JsonConvert.SerializeObject(GetRecord);
                    return Json("[" + CatResul + "]");
                }
            }
            else
                return Json("Invalid data selected");
            return Json(null);
        }
        [HttpPost]
        public JsonResult AddUpdateTerminationEntry(int Id, string PrNo, string Name, string Termin_Type, string Termin_Date, string Reason, string Resig_Rev,
            string App_Resig, string Exit_Inter, string Termin_Return, string Termin_Notice, string Artony, string Deletion, string Settle)
        {
            Dictionary<string, object> d = new Dictionary<string, object>();

            CmnDataManager objCmnDataManager = new CmnDataManager();
            if (Id > 0)
            {
                d.Add("PR_P_NO", PrNo);
                d.Add("PR_TERMIN_TYPE", Termin_Type);
                d.Add("PR_TERMIN_DATE", Termin_Date);
                d.Add("PR_REASONS", Reason);
                d.Add("PR_RESIG_RECV", Resig_Rev);
                d.Add("PR_APP_RESIG", App_Resig);
                d.Add("PR_EXIT_INTER", Exit_Inter);
                d.Add("PR_ID_RETURN", Termin_Return);
                d.Add("PR_NOTICE", Termin_Notice);
                d.Add("PR_ARTONY", Artony);
                d.Add("PR_DELETION", Id);
                d.Add("PR_SETTLE", Settle);
                d.Add("ID", Id);
                d.Add("PR_UserID", HttpContext.Session.GetString("USER_ID").ToString());
                d.Add("PR_User_Approved", false);

                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_TERMINATION_MANAGER", "Update", d);
                if (rsltCode.isSuccessful)
                {
                    return Json("Data updated successfully.");
                }
                else
                    return Json("Unable to insert record.");
            }
            return Json("Problem while fetching record");
        }
        public JsonResult AuthorizeTerminationEntry(int ID)
        {
            string SessionUser = HttpContext.Session.GetString("USER_ID").ToString();
            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("PR_UserID", SessionUser);
            d.Add("ID", ID);
            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_TERMINATION_MANAGER", "AUTHORIZE_TERMINATION", d);
            if (rsltCode.isSuccessful)
                return Json("Record authorized successfully.");
            else
                return Json("Problem while authorizing data");
            return Json(null);
        }
        [HttpPost]
        public JsonResult DeleteTerminationData(int ID)
        {
            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("ID", ID);

            CmnDataManager objCmnDataManager = new CmnDataManager();
            if (ID != null)
            {
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_TERMINATION_MANAGER", "Delete", d);
                if (rsltCode.isSuccessful)
                {
                    return Json("Record deleted successfully.");
                }
                else
                    return Json("Unable to deleted record.");
            }
            return Json(null);
        }
        #endregion

        #region PERSONNEL SYSTEM INCREMENTS / PROMOTIONS (OFFICERS STAFF)
        #region Variables
        int Dummy_FLd;
        DateTime J_date;
        string PNO = "";
        string TYPE = "";
        string EFFECTIVE = "";
        string ID = "";
        string RANK = "";
        DateTime? RANKING_DATE = null;
        string INCREMENT_AMOUNT = "";
        string INCREMENT_PERCENT = "";
        string ANNUAL_PRESENT = "";
        DateTime? LAST_APPR = null;
        DateTime? NEXT_APPR = null;
        string REMARKS = "";
        string DESIG = "";
        string LEVEL = "";
        double w_inc;
        double W_min;
        double W_max;
        double NUMB1;
        double NUMB;
        double NUMB2;
        string txtPR_ANNUAL_PREVIOUS1 = "";
        string txtPR_DESIG_PREVIOUS = "";
        string txtPR_LEVEL_PREVIOUS = "";
        string txtPR_FUNCT_1_PREVIOUS = "";
        string txtPR_FUNCT_2_PREVIOUS = "";
        string txtPR_LEVEL_PRESENT1 = "";
        string txtPR_FUNCT_1_PRESENT = "";
        string txtPR_FUNCT_2_PRESENT = "";
        DateTime? dtNextApp = null;
        DateTime? dtLastApp = null;
        string Message = "";
        CmnDataManager cmnDM = new CmnDataManager();
        #endregion
        public IActionResult IncrementOfficers()
        {
            return View();
        }
        [HttpPost]
        public JsonResult IncrementOfficersPRNo()
        {
            Dictionary<string, object> d = new Dictionary<string, object>();
            //d.Add("PR_P_NO", PRNO);
            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_personnel_IncrementOfficer_MANAGER", "PR_P_NO", d);
            if (rsltCode.isSuccessful)
            {
                if (rsltCode.dstResult.Tables.Count > 0)
                {
                    if (rsltCode.dstResult.Tables[0].Rows.Count > 0)
                    {
                        DataTable firstTable = rsltCode.dstResult.Tables[0];

                        var CatResul = JsonConvert.SerializeObject(firstTable);
                        return Json(CatResul);
                    }
                }
            }
            return Json(null);
        }
        [HttpPost]
        public JsonResult ValidatePrNoOfficerStaffIncrement(string PrNo)
        {
            string PR_ANNUAL_PRESENT = "";
            string PR_DESIG_PRESENT = "";
            string PR_LEVEL_PRESENT = "";
            string PR_ANNUAL_PREVIOUS = "";
            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("PR_P_NO", PrNo);
            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rslt = objCmnDataManager.GetData("CHRIS_SP_personnel_IncrementOfficer_MANAGER", "PROC_8", d);
            if (rslt.isSuccessful)
            {
                if (rslt.dstResult != null && rslt.dstResult.Tables[0].Rows.Count > 0)
                {
                    if (rslt.dstResult.Tables[0].Rows[0].ItemArray[0] != null)
                        PR_ANNUAL_PRESENT = rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString();
                    if (rslt.dstResult.Tables[0].Rows[0].ItemArray[1] != null)
                        PR_DESIG_PRESENT = rslt.dstResult.Tables[0].Rows[0].ItemArray[1].ToString();
                    if (rslt.dstResult.Tables[0].Rows[0].ItemArray[2] != null)
                        PR_LEVEL_PRESENT = rslt.dstResult.Tables[0].Rows[0].ItemArray[2].ToString();
                    if (rslt.dstResult.Tables[0].Rows[0].ItemArray[3] != null)
                    {
                        PR_ANNUAL_PREVIOUS = rslt.dstResult.Tables[0].Rows[0].ItemArray[3].ToString();
                    }
                    var CoverLetter = new
                    {
                        PR_ANNUAL_PRESENT = PR_ANNUAL_PRESENT,
                        PR_DESIG_PRESENT = PR_DESIG_PRESENT,
                        PR_LEVEL_PRESENT = PR_LEVEL_PRESENT,
                        PR_ANNUAL_PREVIOUS = PR_ANNUAL_PREVIOUS,
                    };
                    return Json(CoverLetter);
                }
            }
            return Json(null);
        }
        [HttpPost]
        public JsonResult ValidateIfExistPrNoOfficerStaffIncrement(string PrNo, string A)
        {
            if (A != "ADD")
            {
                Result rslt;
                int count = 0;
                CmnDataManager cmnDM = new CmnDataManager();
                Dictionary<string, object> param = new Dictionary<string, object>();
                param.Add("PR_IN_NO", PrNo);
                rslt = cmnDM.GetData("CHRIS_SP_INC_PROMOTION_Increment_MANAGER", "COUNT", param);
                if (rslt.isSuccessful)
                {
                    if (rslt.dstResult != null)
                    {
                        if (rslt.dstResult.Tables[0].Rows.Count > 0)
                            Int32.TryParse(rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString(), out count);
                    }
                    if (count == 0)
                        return Json("NO DATA EXISTS FOR THE ENTERED PERSONNEL NO.");
                }
            }
            return Json(null);
        }
        [HttpPost]
        public JsonResult IncrementOfficersIncrementType(string PrNo)
        {
            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("SEARCHFILTER", PrNo);
            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_INC_PROMOTION_Increment_MANAGER", "INC_TYPE", d);
            if (rsltCode.isSuccessful)
            {
                if (rsltCode.dstResult.Tables.Count > 0)
                {
                    if (rsltCode.dstResult.Tables[0].Rows.Count > 0)
                    {
                        DataTable firstTable = rsltCode.dstResult.Tables[0];

                        var CatResul = JsonConvert.SerializeObject(firstTable);
                        return Json(CatResul);
                    }
                    else
                        return Json("No record found");
                }
                else
                    return Json("No record found");
            }
            else
                return Json("No record found");
            return Json(null);
        }
        [HttpPost]
        public JsonResult ValidateIncrementType(string A, string PNo, string Type)
        {
            if (Type != "I" && Type != "A" && Type != "P")
            {
                return Json("YOU HAVE TO ENTER I , A or P");
            }

            if (A == "DELETE" || A == "UPDATE")
            {
                Proc_6(PNo, Type);
                if (Dummy_FLd == 0)
                {
                    return Json("NO DATA EXISTS FOR THIS OPTION.");
                }
            }

            else if (A == "VIEW")
            {
                proc_1(PNo, Type);
                if (Dummy_FLd == 0)
                {
                    return Json("NO DATA EXISTS FOR THIS OPTION.");
                }
            }
            return Json(null);
        }
        void Proc_6(string PNO, string Type)
        {
            Result rslt;
            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Add("PR_IN_NO", PNO);
            param.Add("PR_INC_TYPE", Type);
            rslt = cmnDM.GetData("CHRIS_SP_INC_PROMOTION_Increment_MANAGER", "PROC_6", param);
            if (rslt.isSuccessful)
            {
                if (rslt.dstResult != null && rslt.dstResult.Tables[0].Rows.Count > 0)
                {
                    if (rslt.dstResult.Tables[0].Rows[0].ItemArray[0] != null)
                        Int32.TryParse(rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString(), out Dummy_FLd);
                }
            }

        }
        void proc_1(string PNO, string Type)
        {
            Result rslt;
            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Add("PR_IN_NO", PNO);
            param.Add("PR_INC_TYPE", Type);
            rslt = cmnDM.GetData("CHRIS_SP_INC_PROMOTION_Increment_MANAGER", "PROC_1", param);
            if (rslt.isSuccessful)
            {
                if (rslt.dstResult != null && rslt.dstResult.Tables[0].Rows.Count > 0)
                {
                    Int32.TryParse(rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString(), out Dummy_FLd);
                    //Dummy_FLd = Int32.Parse(rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString());
                }
            }

        }
        [HttpPost]
        public JsonResult ValidateEffectiveFromIncrementOfficer(string PNo, string Effective, string A, string Type)
        {
            try
            {
                DateTime dt;
                if (!string.IsNullOrEmpty(Effective))
                {
                    if (Effective.Length == 8)
                    {
                        if (!DateTime.TryParseExact(Effective, "ddMMyyyy", new CultureInfo("en-GB"), DateTimeStyles.None, out dt))
                        {
                            string message = "Date must be entered in a format like  DDMMYYYY";
                            var DataResult = new { message = message };
                            return Json(DataResult);
                        }
                        else
                            Effective = string.Format("{0}/{1}/{2}", Effective.Substring(0, 2), Effective.Substring(2, 2), Effective.Substring(4, 4));

                    }
                    else if ((Effective.Length != 8) && (!DateTime.TryParseExact(Effective, "dd/MM/yyyy", new CultureInfo("en-GB"), DateTimeStyles.None, out dt)))
                    {
                        string message = "Date must be entered in a format like  DDMMYYYY";
                        var DataResult = new { message = message };
                        return Json(DataResult);
                    }
                    else if (!DateTime.TryParse(Effective, out dt))
                    {
                        string message = "Date must be entered in a format like  DDMMYYYY";
                        var DataResult = new { message = message };
                        return Json(DataResult);
                    }

                }
                //if (this.tbtCancel.Pressed || this.tbtClose.Pressed)
                //    return;
                //if (this.A == Function.None)
                //    return;
                PR_Joining_dt(PNo);
                if (Effective != null)
                {
                    DateTime Effectv_dt = Convert.ToDateTime(Effective);

                    TimeSpan ts = Effectv_dt.Subtract(J_date);
                    int days = ts.Days;

                    if (days < 0)
                    {
                        string message = "DATE HAS TO BE GREATER THAN JOINING DATE  " + J_date.ToString("dd-MMM-yy");
                        var DataResult = new { message = message };
                        return Json(DataResult);
                    }

                    else
                    {
                        pr_effective(PNo, Effective, A, Type);

                        #region Add Mode Case
                        if (A == "ADD")
                        {
                            Result rslt1;
                            Dictionary<string, object> param1 = new Dictionary<string, object>();
                            param1.Add("PR_IN_NO", PNo);
                            param1.Add("PR_EFFECTIVE", Effective);
                            rslt1 = cmnDM.GetData("CHRIS_SP_INC_PROMOTION_Increment_MANAGER", "PR_EFFECTIVE_KeyNext_Add", param1);
                            if (rslt1.isSuccessful)
                            {
                                #region Count == 1
                                if (rslt1.dstResult != null && rslt1.dstResult.Tables[0].Rows.Count == 1)
                                {
                                    DateTime dummyDate = new DateTime();
                                    if (rslt1.dstResult.Tables[0].Rows[0].ItemArray[0] != null)
                                        if (DateTime.TryParse(rslt1.dstResult.Tables[0].Rows[0].ItemArray[0].ToString(), out dummyDate))
                                        {
                                            string message = "DATE HAS TO BE GREATER THAN  " + dummyDate.ToString("dd/MM/yyyy");
                                            var DataResult = new { message = message };
                                            return Json(DataResult);
                                        }
                                    var Data = new
                                    {
                                        message = string.Empty,
                                        PNO = PNO,
                                        TYPE = TYPE,
                                        EFFECTIVE = EFFECTIVE,
                                        ID = ID,
                                        RANK = RANK,
                                        RANKING_DATE = RANKING_DATE,
                                        INCREMENT_AMOUNT = INCREMENT_AMOUNT,
                                        INCREMENT_PERCENT = INCREMENT_PERCENT,
                                        ANNUAL_PRESENT = ANNUAL_PRESENT,
                                        LAST_APPR = LAST_APPR,
                                        NEXT_APPR = NEXT_APPR,
                                        REMARKS = REMARKS,
                                        DESIG = DESIG,
                                        LEVEL = LEVEL,
                                    };
                                    return Json(Data);
                                }
                                #endregion

                                #region Count == 0
                                else if (rslt1.dstResult != null && rslt1.dstResult.Tables[0].Rows.Count == 0)
                                {
                                    if (Type == "I")
                                    {
                                        param1 = new Dictionary<string, object>();
                                        param1.Add("PR_IN_NO", PNo);
                                        param1.Add("PR_EFFECTIVE", Effective);
                                        rslt1 = cmnDM.GetData("CHRIS_SP_INC_PROMOTION_Increment_MANAGER", "PR_EFFECTIVE_KeyNext_Add2", param1);
                                        if (rslt1.isSuccessful)
                                        {
                                            if (rslt1.dstResult != null && rslt1.dstResult.Tables[0].Rows.Count == 1)
                                            {
                                                DateTime dummyDate1 = new DateTime();
                                                if (rslt1.dstResult.Tables[0].Rows[0].ItemArray[0] != null)
                                                    DateTime.TryParse(rslt1.dstResult.Tables[0].Rows[0].ItemArray[0].ToString(), out dummyDate1);

                                                if (dummyDate1.Year != 1)
                                                {
                                                    string message = "INCREMENT IS GIVEN AFTER ONE YEAR. LAST INCREMENT WAS GIVEN ON   " + dummyDate1.ToString("dd/MM/yyyy");
                                                    var DataResult = new { message = message };
                                                    return Json(DataResult);
                                                }
                                                else
                                                {
                                                    string message = "INCREMENT IS GIVEN AFTER ONE YEAR.";
                                                    var DataResult = new { message = message };
                                                    return Json(DataResult);
                                                }
                                            }
                                            else if (rslt1.dstResult != null && rslt1.dstResult.Tables[0].Rows.Count > 1)
                                            {
                                                string message = "INVALID";
                                                var DataResult = new { message = message };
                                                return Json(DataResult);
                                            }
                                            //else if (rslt1.dstResult != null && rslt1.dstResult.Tables[0].Rows.Count== 0)
                                            //NEXT_FIELD;
                                        }
                                    }
                                    //else next feild
                                }
                                #endregion

                                #region Count > 1
                                else if (rslt1.dstResult != null && rslt1.dstResult.Tables[0].Rows.Count > 1)
                                {
                                    param1 = new Dictionary<string, object>();
                                    param1.Add("PR_IN_NO", PNo);
                                    rslt1 = cmnDM.GetData("CHRIS_SP_INC_PROMOTION_Increment_MANAGER", "PR_EFFECTIVE_KeyNext_Add3", param1);
                                    if (rslt1.isSuccessful)
                                    {
                                        if (rslt1.dstResult != null && rslt1.dstResult.Tables[0].Rows.Count > 0)
                                        {
                                            DateTime JJ = new DateTime();
                                            if (rslt1.dstResult.Tables[0].Rows[0].ItemArray[0] != null)
                                                DateTime.TryParse(rslt1.dstResult.Tables[0].Rows[0].ItemArray[0].ToString(), out JJ);
                                            string message = "DATE HAS TO BE GREATER THAN  " + JJ.ToString("dd/MM/yyyy");
                                            var DataResult = new { message = message };
                                            return Json(DataResult);
                                        }
                                    }

                                }

                                #endregion

                            }

                        }
                        else
                        {
                            var Data = new
                            {
                                message = string.Empty,
                                PNO = PNO,
                                TYPE = TYPE,
                                EFFECTIVE = EFFECTIVE,
                                ID = ID,
                                RANK = RANK,
                                RANKING_DATE = RANKING_DATE,
                                INCREMENT_AMOUNT = INCREMENT_AMOUNT,
                                INCREMENT_PERCENT = INCREMENT_PERCENT,
                                ANNUAL_PRESENT = ANNUAL_PRESENT,
                                LAST_APPR = LAST_APPR,
                                NEXT_APPR = NEXT_APPR,
                                REMARKS = REMARKS,
                                DESIG = DESIG,
                                LEVEL = LEVEL,
                            };
                            return Json(Data);
                        }

                        #endregion
                    }
                }
                else
                {

                }
                return Json(null);
            }
            catch (Exception exp)
            {
                return Json(exp.Message);
            }
        }
        void pr_effective(string PNo, string Effective, string A, string Type)
        {
            #region Add/Modify/Delete
            if (A == "VIEW" || A == "UPDATE" || A == "DELETE")
            {
                Result rslt1;
                Dictionary<string, object> param1 = new Dictionary<string, object>();
                param1.Add("PR_IN_NO", PNo);
                param1.Add("PR_INC_TYPE", Type);
                param1.Add("PR_EFFECTIVE", Effective);
                rslt1 = cmnDM.GetData("CHRIS_SP_INC_PROMOTION_Increment_MANAGER", "PR_EFFECTIVE_KeyNext", param1);
                if (rslt1.isSuccessful)
                {
                    if (rslt1.dstResult != null && rslt1.dstResult.Tables[0].Rows.Count > 0)
                    {
                        if (rslt1.dstResult.Tables[0].Rows[0].ItemArray[0] != null)
                            PNO = rslt1.dstResult.Tables[0].Rows[0].ItemArray[0].ToString();
                        if (rslt1.dstResult.Tables[0].Rows[0].ItemArray[1] != null)
                            TYPE = rslt1.dstResult.Tables[0].Rows[0].ItemArray[1].ToString();
                        if (rslt1.dstResult.Tables[0].Rows[0].ItemArray[2] != null)
                            EFFECTIVE = (rslt1.dstResult.Tables[0].Rows[0].ItemArray[2].ToString());
                        if (rslt1.dstResult.Tables[0].Rows[0].ItemArray[3] != null)
                            ID = rslt1.dstResult.Tables[0].Rows[0].ItemArray[3].ToString();
                    }

                }
                Result rslt2;
                Dictionary<string, object> param2 = new Dictionary<string, object>();
                param2.Add("PR_IN_NO", PNo);
                param2.Add("PR_INC_TYPE", Type);
                param2.Add("PR_EFFECTIVE", Effective);
                rslt2 = cmnDM.GetData("CHRIS_SP_INC_PROMOTION_Increment_MANAGER", "Execute_query", param2);
                if (rslt2.isSuccessful)
                {
                    if (rslt2.dstResult.Tables.Count > 0 && rslt2.dstResult.Tables[0].Rows.Count > 0)
                    {
                        DateTime dt = DateTime.Now;
                        if (rslt2.dstResult.Tables[0].Rows[0].ItemArray[0] != null)
                            RANK = rslt2.dstResult.Tables[0].Rows[0].ItemArray[0].ToString();

                        if (rslt2.dstResult.Tables[0].Rows[0].ItemArray[1] != DBNull.Value)
                        {
                            DateTime dttemp = new DateTime();
                            if (DateTime.TryParse(rslt2.dstResult.Tables[0].Rows[0].ItemArray[1].ToString(), out dttemp))
                                RANKING_DATE = Convert.ToDateTime(rslt2.dstResult.Tables[0].Rows[0].ItemArray[1].ToString());//Convert.ToDateTime(rslt2.dstResult.Tables[0].Rows[0].ItemArray[1].ToString() == "" ? "01/01/1900" : rslt2.dstResult.Tables[0].Rows[0].ItemArray[1].ToString());
                            else
                                RANKING_DATE = null;
                        }

                        if (rslt2.dstResult.Tables[0].Rows[0].ItemArray[2] != null)
                            INCREMENT_AMOUNT = rslt2.dstResult.Tables[0].Rows[0].ItemArray[2].ToString();

                        if (rslt2.dstResult.Tables[0].Rows[0].ItemArray[3] != null)
                            INCREMENT_PERCENT = rslt2.dstResult.Tables[0].Rows[0].ItemArray[3].ToString();//.Substring(0, 4);

                        if (rslt2.dstResult.Tables[0].Rows[0].ItemArray[4] != null)
                            ANNUAL_PRESENT = rslt2.dstResult.Tables[0].Rows[0].ItemArray[4].ToString();

                        if (rslt2.dstResult.Tables[0].Rows[0].ItemArray[5] != null && rslt2.dstResult.Tables[0].Rows[0].ItemArray[5].ToString() != string.Empty)
                        {
                            DateTime dtTemp = new DateTime();
                            DateTime.TryParse(rslt2.dstResult.Tables[0].Rows[0].ItemArray[5].ToString(), out dtTemp);
                            if (dtTemp.Year != 1)
                                LAST_APPR = dtTemp;
                        }

                        if (rslt2.dstResult.Tables[0].Rows[0].ItemArray[6] != null && rslt2.dstResult.Tables[0].Rows[0].ItemArray[6].ToString() != string.Empty)
                            NEXT_APPR = Convert.ToDateTime(rslt2.dstResult.Tables[0].Rows[0].ItemArray[6].ToString());

                        if (rslt2.dstResult.Tables[0].Rows[0].ItemArray[7] != null) //if (rslt2.dstResult.Tables[0].Rows[0].ItemArray[0] != null)
                            REMARKS = rslt2.dstResult.Tables[0].Rows[0].ItemArray[7].ToString();

                        if (rslt2.dstResult.Tables[0].Rows[0].ItemArray[8] != null) //if (rslt2.dstResult.Tables[0].Rows[0].ItemArray[0] != null)
                            DESIG = rslt2.dstResult.Tables[0].Rows[0].ItemArray[8].ToString();
                        if (rslt2.dstResult.Tables[0].Rows[0].ItemArray[9] != null) //if (rslt2.dstResult.Tables[0].Rows[0].ItemArray[0] != null)
                            LEVEL = rslt2.dstResult.Tables[0].Rows[0].ItemArray[9].ToString();
                    }
                }
            }
            #endregion
        }
        void PR_Joining_dt(string PNo)
        {
            Result rslt;
            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Add("PR_P_NO", PNo);
            rslt = cmnDM.GetData("CHRIS_SP_personnel_IncrementOfficer_MANAGER", "PR_JOINING_DATE", param);
            if (rslt.isSuccessful)
            {
                if (rslt.dstResult != null && rslt.dstResult.Tables[0].Rows.Count > 0)
                {
                    if (rslt.dstResult.Tables[0].Rows[0].ItemArray[0] != null)
                        DateTime.TryParse(rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString(), out J_date);
                }
            }

        }
        [HttpPost]
        public JsonResult ValidateIncrementAmount(string PNo, string A, string Type, string Desig, string Level, string IncrementAmount,
            string txtAnnualPresnt, string txtIncrementPer, string txtPR_ANNUAL_PRESENT)
        {
            NUMB = 0;
            NUMB1 = 0;
            NUMB2 = 0;

            #region Key Next of increment Amount

            double PR_INC_AMT = 0;
            if (A == "ADD")
            {
                w_inc = 0;
            }
            if (Type != "P")
            {
                Pr_IncAmt_kyNxt1(PNo);
                /**********************Pr_IncAmt_knNxt2();and  Pr_IncAmt_knNxt3(); were called in No data found condition
                of Pr_IncAmt_kyNxt1();Therefore its now integrated in tht condition.***********************************/

                /*if (flg == 'Y')
                //{
                //    Pr_IncAmt_knNxt2();
                //    Pr_IncAmt_knNxt3();
                }*/

                Pr_IncAmt_knNxt4(PNo, Desig, Level);
                Pr_IncAmt_knNxt5(PNo);

                if (IncrementAmount != string.Empty || IncrementAmount != ""/*&& (IncrementAmount != null) */)
                    double.TryParse(IncrementAmount, out PR_INC_AMT);
                //PR_INC_AMT = double.Parse(IncrementAmount); 
                NUMB1 = NUMB + PR_INC_AMT - w_inc;
                NUMB2 = W_max - NUMB + w_inc;

                if (A == "UPDATE")
                    txtPR_ANNUAL_PREVIOUS1 = Convert.ToString(NUMB - w_inc);

                if ((NUMB1 >= W_min) && (NUMB1 <= W_max))
                {
                    txtAnnualPresnt = NUMB1.ToString();
                    if (NUMB == 0)
                    {
                        NUMB = 1;
                        txtIncrementPer = Convert.ToString(Math.Round(((PR_INC_AMT / NUMB) * 100), 2));//.Substring(0, 4);//Convert.ToString(((PR_INC_AMT / NUMB) * 100)).Substring(0, 4);
                    }
                    else
                        txtIncrementPer = Convert.ToString(Math.Round(((PR_INC_AMT / NUMB) * 100), 2));//.Substring(0, 4);//Convert.ToString(((PR_INC_AMT / NUMB) * 100)).Substring(0, 4);
                }
                else
                {
                    Message = "INCREMENT SHOULD NOT BE >  " + NUMB2.ToString() + "  BECAUSE THE LIMIT IS  " + W_max.ToString();
                    txtPR_ANNUAL_PRESENT = NUMB1.ToString();
                    if (NUMB != 0 && PR_INC_AMT != 0.0)
                        txtIncrementPer = Convert.ToString(Math.Round(((PR_INC_AMT / NUMB) * 100), 2));
                }
            }
            #endregion

            #region Pre_text_item Of Pr_Next _app

            #region Next Appraisal
            if (Type == "I" && A == "ADD")
            {
                Result rslt;
                Dictionary<string, object> param = new Dictionary<string, object>();
                param.Add("PR_IN_NO", PNo);
                rslt = cmnDM.GetData("CHRIS_SP_INC_PROMOTION_Increment_MANAGER", "Next_appraisal", param);
                if (rslt.isSuccessful)
                {
                    if (rslt.dstResult != null && rslt.dstResult.Tables[0].Rows.Count > 0)
                    {
                        DateTime dtTemp;
                        if (DateTime.TryParse(rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString(), out dtTemp))
                        { dtNextApp = dtTemp; }
                        //dtNextApp.Value = Convert.ToDateTime(rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString());
                    }
                }

            }
            #endregion

            #region Next Increment
            Result rslt1;
            Dictionary<string, object> param1 = new Dictionary<string, object>();
            param1.Add("PR_IN_NO", PNo);
            rslt1 = cmnDM.GetData("CHRIS_SP_INC_PROMOTION_Increment_MANAGER", "Next_Increment", param1);
            if (rslt1.isSuccessful)
            {
                if (rslt1.dstResult != null && rslt1.dstResult.Tables[0].Rows.Count > 0)
                {
                    if (rslt1.dstResult.Tables[0].Rows[0].ItemArray[0] == null)
                        dtLastApp = null;
                    else
                        Pr_effective(PNo);

                }
                else
                    Pr_effective(PNo);
            }
            else
                Pr_effective(PNo);


            var ReturnData = new
            {
                Message = Message,
                txtIncrementPer = txtIncrementPer,
                dtNextApp = dtNextApp,
                dtLastApp = dtLastApp,
                txtPR_ANNUAL_PRESENT = txtPR_ANNUAL_PRESENT,
                txtPR_ANNUAL_PREVIOUS1 = txtPR_ANNUAL_PREVIOUS1,
                txtPR_DESIG_PREVIOUS = txtPR_DESIG_PREVIOUS,
                txtPR_LEVEL_PREVIOUS = txtPR_LEVEL_PREVIOUS,
                txtPR_FUNCT_1_PREVIOUS = txtPR_FUNCT_1_PREVIOUS,
                txtPR_FUNCT_2_PREVIOUS = txtPR_FUNCT_2_PREVIOUS,
                txtPR_LEVEL_PRESENT1 = txtPR_LEVEL_PRESENT1,
                txtPR_FUNCT_1_PRESENT = txtPR_FUNCT_1_PRESENT,
                txtPR_FUNCT_2_PRESENT = txtPR_FUNCT_2_PRESENT,
            };
            return Json(ReturnData);
            #endregion
            //nextincrement();
            //if (flg2 == 'Y')
            //{
            //    Pr_effective();

            //}

            #endregion
        }
        void Pr_IncAmt_kyNxt1(string PNo)
        {
            Result rslt;
            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Add("PR_IN_NO", PNo);
            rslt = cmnDM.GetData("CHRIS_SP_INC_PROMOTION_Increment_MANAGER", "Pr_IncAmt_kyNxt_1", param);
            if (rslt.isSuccessful)
            {
                if (rslt.dstResult != null && rslt.dstResult.Tables[0].Rows.Count > 0)
                {
                    if (rslt.dstResult.Tables[0].Rows[0].ItemArray[0] != null)
                        txtPR_ANNUAL_PREVIOUS1 = rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString();

                    if (rslt.dstResult.Tables[0].Rows[0].ItemArray[1] != null)
                        txtPR_DESIG_PREVIOUS = rslt.dstResult.Tables[0].Rows[0].ItemArray[1].ToString();

                    if (rslt.dstResult.Tables[0].Rows[0].ItemArray[2] != null)
                        txtPR_LEVEL_PREVIOUS = rslt.dstResult.Tables[0].Rows[0].ItemArray[2].ToString();

                    if (rslt.dstResult.Tables[0].Rows[0].ItemArray[3] != null)
                        txtPR_FUNCT_1_PREVIOUS = rslt.dstResult.Tables[0].Rows[0].ItemArray[3].ToString();

                    if (rslt.dstResult.Tables[0].Rows[0].ItemArray[4] != null)
                        txtPR_FUNCT_2_PREVIOUS = rslt.dstResult.Tables[0].Rows[0].ItemArray[4].ToString();

                    if (rslt.dstResult.Tables[0].Rows[0].ItemArray[5] != null)
                        txtPR_DESIG_PREVIOUS = rslt.dstResult.Tables[0].Rows[0].ItemArray[5].ToString();

                    if (rslt.dstResult.Tables[0].Rows[0].ItemArray[6] != null)
                        txtPR_LEVEL_PRESENT1 = rslt.dstResult.Tables[0].Rows[0].ItemArray[6].ToString();

                    if (rslt.dstResult.Tables[0].Rows[0].ItemArray[7] != null)
                        txtPR_FUNCT_1_PRESENT = rslt.dstResult.Tables[0].Rows[0].ItemArray[7].ToString();

                    if (rslt.dstResult.Tables[0].Rows[0].ItemArray[8] != null)
                        txtPR_FUNCT_2_PRESENT = rslt.dstResult.Tables[0].Rows[0].ItemArray[8].ToString();
                    // flg = 'Y';

                }
                if (rslt.dstResult != null && rslt.dstResult.Tables[0].Rows.Count == 0)
                {
                    Pr_IncAmt_knNxt2(PNo);
                    Pr_IncAmt_knNxt3(PNo);
                }
            }
        }
        void Pr_IncAmt_knNxt2(string PNo)
        {
            Result rslt;
            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Add("PR_IN_NO", PNo);
            rslt = cmnDM.GetData("CHRIS_SP_INC_PROMOTION_Increment_MANAGER", "Pr_inc_txt_2", param);
            if (rslt.isSuccessful)
            {
                if (rslt.dstResult != null && rslt.dstResult.Tables[0].Rows.Count > 0)
                {
                    if (rslt.dstResult.Tables[0].Rows[0].ItemArray[0] != null)
                        txtPR_ANNUAL_PREVIOUS1 = rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString();
                    if (rslt.dstResult.Tables[0].Rows[0].ItemArray[1] != null)
                        txtPR_DESIG_PREVIOUS = rslt.dstResult.Tables[0].Rows[0].ItemArray[1].ToString();
                    if (rslt.dstResult.Tables[0].Rows[0].ItemArray[2] != null)
                        txtPR_LEVEL_PREVIOUS = rslt.dstResult.Tables[0].Rows[0].ItemArray[2].ToString();
                    if (rslt.dstResult.Tables[0].Rows[0].ItemArray[3] != null)
                        txtPR_FUNCT_1_PREVIOUS = rslt.dstResult.Tables[0].Rows[0].ItemArray[3].ToString();
                    if (rslt.dstResult.Tables[0].Rows[0].ItemArray[4] != null)
                        txtPR_FUNCT_2_PREVIOUS = rslt.dstResult.Tables[0].Rows[0].ItemArray[4].ToString();
                }
            }

        }
        void Pr_IncAmt_knNxt3(string PNo)
        {
            Result rslt;
            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Add("PR_IN_NO", PNo);
            rslt = cmnDM.GetData("CHRIS_SP_INC_PROMOTION_Increment_MANAGER", "Pr_inc_txt_3", param);
            if (rslt.isSuccessful)
            {
                if (rslt.dstResult != null && rslt.dstResult.Tables[0].Rows.Count > 0)
                {
                    if (rslt.dstResult.Tables[0].Rows[0].ItemArray[0] != null)
                        txtPR_DESIG_PREVIOUS = rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString();
                    if (rslt.dstResult.Tables[0].Rows[0].ItemArray[1] != null)
                        txtPR_LEVEL_PREVIOUS = rslt.dstResult.Tables[0].Rows[0].ItemArray[1].ToString();
                    if (rslt.dstResult.Tables[0].Rows[0].ItemArray[2] != null)
                        txtPR_FUNCT_1_PREVIOUS = rslt.dstResult.Tables[0].Rows[0].ItemArray[2].ToString();
                    if (rslt.dstResult.Tables[0].Rows[0].ItemArray[3] != null)
                        txtPR_FUNCT_2_PREVIOUS = rslt.dstResult.Tables[0].Rows[0].ItemArray[3].ToString();
                }
            }

        }
        void Pr_IncAmt_knNxt4(string PNo, string Desig, string Level)
        {
            Result rslt;
            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Add("PR_IN_NO", PNo);
            param.Add("PR_DESIG_PRESENT", Desig);
            param.Add("PR_LEVEL_PRESENT", Level);
            rslt = cmnDM.GetData("CHRIS_SP_INC_PROMOTION_Increment_MANAGER", "Pr_inc_txt_4", param);
            if (rslt.isSuccessful)
            {
                if (rslt.dstResult != null && rslt.dstResult.Tables[0].Rows.Count > 0)
                {
                    W_min = Int32.Parse(rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString());
                    W_max = Int32.Parse(rslt.dstResult.Tables[0].Rows[0].ItemArray[1].ToString());
                }
            }

        }
        void Pr_IncAmt_knNxt5(string PNo)
        {
            Result rslt;
            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Add("PR_IN_NO", PNo);
            rslt = cmnDM.GetData("CHRIS_SP_INC_PROMOTION_Increment_MANAGER", "Pr_inc_txt_5", param);
            if (rslt.isSuccessful)
            {
                if (rslt.dstResult != null && rslt.dstResult.Tables[0].Rows.Count > 0)
                {
                    if (rslt.dstResult.Tables[0].Rows[0].ItemArray[0] != null)
                        Double.TryParse(rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString(), out NUMB);
                    //NUMB = Int32.Parse(rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString());

                }
            }

        }
        void Pr_effective(string PNo)
        {
            Result rslt;
            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Add("PR_IN_NO", PNo);
            rslt = cmnDM.GetData("CHRIS_SP_INC_PROMOTION_Increment_MANAGER", "Pr_effective", param);
            if (rslt.isSuccessful)
            {
                if (rslt.dstResult != null && rslt.dstResult.Tables[0].Rows.Count > 0)
                {
                    dtLastApp = Convert.ToDateTime(rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString());
                    //dtNextApp.Value = Int32.Parse(rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString());
                    //flg2 = 'Y';
                }
            }

        }
        [HttpPost]
        public JsonResult LoadListOfEffectiveDate(string PNo, string Type)
        {
            Result rslt;
            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Add("SearchFilter", PNo + "|" + Type);
            rslt = cmnDM.GetData("CHRIS_SP_personnel_IncrementOfficer_MANAGER", "DateLOV", param);
            if (rslt.isSuccessful)
            {
                if (rslt.dstResult != null && rslt.dstResult.Tables[0].Rows.Count > 0)
                {
                    DataTable firstTable = rslt.dstResult.Tables[0];

                    var CatResul = JsonConvert.SerializeObject(firstTable);
                    return Json(CatResul);
                }
            }
            return Json(null);
        }
        [HttpPost]
        public JsonResult AddUpdateOfficerStaffIncrementData(string PRNO, int ID, string Action, string TYPE, string EFFECTIVE, string RANKING_DATE, string RANKING, string INCREMENT_AMOUNT, string INCREMENT_PERCENT,
            string NEW_ANNUAL_PACK, string LAST_APP, string NEXT_APP, string REMARKS, string ANNUAL_PRESENT, string ANNUAL_PREVIOUS, string DESIG_PRESENT, string DESIG_PREVIOUS, string LEVEL_PREVIOUS,
            string FUNCTION1_PREVIOUS, string FUNCTION2_PREVIOUS, string LEVEL_PRESENT, string FUNCTION1_PRESENT, string FUNCTION2_PRESENT)
        {
            Dictionary<string, object> d = new Dictionary<string, object>();

            d.Add("PR_IN_NO", PRNO);
            d.Add("PR_INC_TYPE", TYPE);
            d.Add("PR_RANK", RANKING);
            d.Add("PR_RANKING_DATE", RANKING_DATE);
            d.Add("PR_EFFECTIVE", EFFECTIVE);
            d.Add("PR_INC_AMT", INCREMENT_AMOUNT);
            d.Add("PR_INC_PER", INCREMENT_PERCENT);
            d.Add("PR_LAST_APP", LAST_APP);
            d.Add("PR_NEXT_APP", NEXT_APP);
            d.Add("PR_REMARKS", REMARKS);
            d.Add("PR_ANNUAL_PRESENT", ANNUAL_PRESENT);
            d.Add("PR_DESIG_PRESENT", DESIG_PRESENT);
            d.Add("PR_LEVEL_PRESENT", LEVEL_PRESENT);
            d.Add("PR_FUNCT_1_PRESENT", FUNCTION1_PRESENT);
            d.Add("PR_FUNCT_2_PRESENT", FUNCTION2_PRESENT);
            d.Add("PR_ANNUAL_PREVIOUS", ANNUAL_PREVIOUS);
            d.Add("PR_DESIG_PREVIOUS", DESIG_PREVIOUS);
            d.Add("PR_LEVEL_PREVIOUS", LEVEL_PREVIOUS);
            d.Add("PR_FUNCT_1_PREVIOUS", FUNCTION1_PREVIOUS);
            d.Add("PR_FUNCT_2_PREVIOUS", FUNCTION2_PREVIOUS);
            d.Add("PR_CURRENT_PREVIOUS", "C");
            d.Add("PR_NO_INCR", null);
            d.Add("PR_NO_MONTHS", null);
            d.Add("PR_STEP_AMT", null);
            d.Add("PR_PROMOTED", null);
            d.Add("PR_FINAL", null);
            d.Add("ID", ID);

            CmnDataManager objCmnDataManager = new CmnDataManager();
            if (ID > 0)
            {
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_INC_PROMOTION_Increment_MANAGER", "UPDATE", d);
                if (rsltCode.isSuccessful)
                {
                    return Json("Data updated successfully.");
                }
                else
                    return Json("Unable to update record.");
            }
            else
            {
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_INC_PROMOTION_Increment_MANAGER", "SAVE", d);
                if (rsltCode.isSuccessful)
                {
                    return Json("Data inserted successfully.");
                }
                else
                    return Json("Unable to insert record.");
            }
        }
        public JsonResult DeleteIncrementOfficerRecord(string PNo, string Type, string Effective)
        {
            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("PR_IN_NO", PNo);
            d.Add("PR_INC_TYPE", Type);
            d.Add("PR_EFFECTIVE", Effective);
            CmnDataManager objCmnDataManager = new CmnDataManager();
            if (PNo != null && PNo != "")
            {
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_INC_PROMOTION_Increment_MANAGER", "DELETE", d);
                if (rsltCode.isSuccessful)
                {
                    return Json("Data deleted successfully.");
                }
                else
                    return Json("Unable to delete record.");
            }
            return Json(null);
        }
        #endregion

        #region SEGMENT OR DEPARTMENT TRANSFER
        #region Declaration
        int Sum1 = 0;
        int dCount = 0;
        bool flag = false;
        bool LastCellValidate = false;
        string pr_SegmentValue = string.Empty;
        DateTime? SD_JoiningDate = null;
        DateTime? SD_TerminDate = null;
        DateTime? SD_TransferDate = null;
        string SD_FirstName = "";
        string SD_LastName = "";
        string SD_NewBranch = "";
        string SD_CloseFlag = "";
        string SD_Transfer = "";
        string SD_Desig = "";
        string SD_Level = "";
        string SD_FunTitle1 = "";
        string SD_FunTitle2 = "";
        string SD_Message = "";
        string SD_LIST1 = "";
        string SD_LIST2 = "";
        DateTime? SD_EFFECTIVE = null;
        string SD_W_REP = "";
        string SD_W_Name = "";
        string SD_W_GOAL = "";
        DateTime? SD_dtW_GOAL_DATE = null;
        #endregion  
        public IActionResult SegmtOrDeptTrans()
        {
            return View();
        }
        [HttpPost]
        public JsonResult GetSegmentDepartmentListData(string Action)
        {
            if (Action == "PrNoList")
            {
                Dictionary<string, object> d = new Dictionary<string, object>();
                //d.Add("PR_P_NO", PRNO);
                CmnDataManager objCmnDataManager = new CmnDataManager();
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_PERSONNEL_SEGMENT_TRANSFER_MANAGER", "Pr_P_No_LOV", d);
                if (rsltCode.isSuccessful)
                {
                    if (rsltCode.dstResult.Tables.Count > 0)
                    {
                        if (rsltCode.dstResult.Tables[0].Rows.Count > 0)
                        {
                            DataTable firstTable = rsltCode.dstResult.Tables[0];

                            var CatResul = JsonConvert.SerializeObject(firstTable);
                            return Json(CatResul);
                        }
                        else
                            return Json(null);
                    }
                    else
                        return Json(null);
                }
                else
                    return Json(null);
            }
            else if (Action == "ReportingList")
            {
                Dictionary<string, object> d = new Dictionary<string, object>();
                //d.Add("PR_P_NO", PRNO);
                CmnDataManager objCmnDataManager = new CmnDataManager();
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_PERSONNEL_SEGMENT_TRANSFER_MANAGER", "W_REP_LOV", d);
                if (rsltCode.isSuccessful)
                {
                    if (rsltCode.dstResult.Tables.Count > 0)
                    {
                        if (rsltCode.dstResult.Tables[0].Rows.Count > 0)
                        {
                            DataTable firstTable = rsltCode.dstResult.Tables[0];

                            var CatResul = JsonConvert.SerializeObject(firstTable);
                            return Json(CatResul);
                        }
                        else
                            return Json(null);
                    }
                    else
                        return Json(null);
                }
                else
                    return Json(null);
            }
            else if (Action == "DepartmentList")
            {
                Dictionary<string, object> d = new Dictionary<string, object>();
                //d.Add("PR_P_NO", PRNO);
                CmnDataManager objCmnDataManager = new CmnDataManager();
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_DEPT_CONT_TRANSFER_DEPT_CONT_MANAGER", "DEPT_LOV", d);
                if (rsltCode.isSuccessful)
                {
                    if (rsltCode.dstResult.Tables.Count > 0)
                    {
                        if (rsltCode.dstResult.Tables[0].Rows.Count > 0)
                        {
                            DataTable firstTable = rsltCode.dstResult.Tables[0];

                            var CatResul = JsonConvert.SerializeObject(firstTable);
                            return Json(CatResul);
                        }
                        else
                            return Json(null);
                    }
                    else
                        return Json(null);
                }
                else
                    return Json(null);
            }
            return Json(null);
        }
        [HttpPost]
        public JsonResult ValidatePrNoOfSegmentDepartment(string PrNo, string Action)
        {
            if (PrNo != string.Empty)
            {
                Sum1 = 0;

                DataTable DtPersonal;
                DataTable DtTransfer;
                Dictionary<string, object> colsNVals = new Dictionary<string, object>();
                Dictionary<string, object> param = new Dictionary<string, object>();
                colsNVals.Clear();
                colsNVals.Add("PR_TR_NO", PrNo);


                DtPersonal = GetData("CHRIS_SP_PERSONNEL_SEGMENT_TRANSFER_MANAGER", "ExecuteQueryPersonal", colsNVals);

                if (DtPersonal != null)
                {
                    #region DataFound
                    if (DtPersonal.Rows.Count > 0)
                    {
                        if (DtPersonal.Rows[0].ItemArray[0].ToString() != string.Empty)
                        {
                            SD_JoiningDate = Convert.ToDateTime(DtPersonal.Rows[0]["pr_joining_date"].ToString());
                        }
                        else
                        {
                            SD_JoiningDate = null;

                        }
                        SD_FirstName = DtPersonal.Rows[0]["pr_name_first"].ToString();
                        SD_LastName = DtPersonal.Rows[0]["Pr_name_Last"].ToString();
                        SD_NewBranch = DtPersonal.Rows[0]["PR_NEW_BRANCH"].ToString();
                        SD_CloseFlag = DtPersonal.Rows[0]["PR_CLOSE_FLAG"].ToString();
                        SD_Transfer = DtPersonal.Rows[0]["PR_TRANSFER"].ToString();
                        if (DtPersonal.Rows[0]["PR_TERMIN_DATE"].ToString() != string.Empty)
                        {
                            SD_TerminDate = Convert.ToDateTime(DtPersonal.Rows[0]["PR_TERMIN_DATE"].ToString());
                        }
                        else
                        {
                            SD_TerminDate = null;

                        }
                        if (DtPersonal.Rows[0]["PR_TRANSFER_DATE"].ToString() != string.Empty)
                        {
                            SD_TransferDate = Convert.ToDateTime(DtPersonal.Rows[0]["PR_TRANSFER_DATE"].ToString());
                        }
                        else
                        {
                            SD_TransferDate = null;

                        }

                        SD_Desig = DtPersonal.Rows[0]["PR_DESIG"].ToString();
                        SD_Level = DtPersonal.Rows[0]["PR_LEVEL"].ToString();
                        SD_FunTitle1 = DtPersonal.Rows[0]["PR_FUNC_TITTLE1"].ToString();
                        SD_FunTitle2 = DtPersonal.Rows[0]["PR_FUNC_TITTLE2"].ToString();

                        if (DtPersonal.Rows[0]["PR_TERMIN_DATE"].ToString() == string.Empty && SD_CloseFlag != "C")
                        {
                            if (Action == "ADD")
                            {
                                #region Add
                                if (SD_Transfer == string.Empty || Convert.ToInt32(SD_Transfer) <= 5)
                                {
                                    FillGridForExistingDeptCont(PrNo, SD_Transfer, SD_TransferDate);
                                }
                                else
                                {
                                    SD_Message = "THIS PERSONNEL ID. IS NOT VALID FOR THIS OPTION";
                                    var ResultData = new { Message = SD_Message };
                                    return Json(ResultData);
                                }
                                #endregion
                            }
                            if (Action == "UPDATE" || Action == "DELETE" || Action == "VIEW")
                            {
                                #region Modify/Delete/View
                                //this.DGVDept2.Visible = false;
                                if (SD_Transfer == "1")
                                {

                                    param.Clear();
                                    param.Add("PR_TR_NO", PrNo);
                                    if (SD_TransferDate != null)
                                    {
                                        param.Add("PR_TRANSFER_DATE", SD_TransferDate);
                                    }

                                    DtTransfer = GetData("CHRIS_SP_PERSONNEL_SEGMENT_TRANSFER_MANAGER", "List", param);
                                    if (DtTransfer != null)
                                    {
                                        if (DtTransfer.Rows.Count > 0)
                                        {
                                            SetListValues(DtTransfer, PrNo);
                                        }
                                    }
                                    //iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.PersonnelTransferSegmentCommand ent = (iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.PersonnelTransferSegmentCommand)this.pnlDetail.CurrentBusinessEntity;
                                    //if (ent != null)
                                    //{
                                    //    ent.PR_EFFECTIVE = (DateTime)(this.dtpPR_EFFECTIVE.Value);
                                    //    if (PrNo != string.Empty)
                                    //    {
                                    //        ent.PR_TR_NO = Double.Parse(PrNo);
                                    //    }
                                    //    if (txtNoINCR != string.Empty)
                                    //    {
                                    //        ent.PR_TRANSFER = int.Parse(txtNoINCR);
                                    //    }
                                    //}
                                    //this.pnlDetail.LoadDependentPanels();

                                    if (Action == "DELETE")
                                    {
                                        SD_Message = "Continue The Process For More Records";
                                        var ResultData = new
                                        {
                                            Message = SD_Message,
                                            JoiningDate = SD_JoiningDate,
                                            TerminDate = SD_TerminDate,
                                            TransferDate = SD_TransferDate,
                                            FirstName = SD_FirstName,
                                            LastName = SD_LastName,
                                            NewBranch = SD_NewBranch,
                                            CloseFlag = SD_CloseFlag,
                                            Transfer = SD_Transfer,
                                            Desig = SD_Desig,
                                            Level = SD_Level,
                                            FunTitle1 = SD_FunTitle1,
                                            FunTitle2 = SD_FunTitle2,
                                            LIST1 = SD_LIST1,
                                            LIST2 = SD_LIST2,
                                            EFFECTIVE = SD_EFFECTIVE,
                                            W_REP = SD_W_REP,
                                            W_Name = SD_W_Name,
                                            W_GOAL = SD_W_GOAL,
                                            dtW_GOAL_DATE = SD_dtW_GOAL_DATE
                                        };
                                        return Json(ResultData);
                                    }
                                    else if (Action == "VIEW")
                                    {
                                        SD_Message = "Do You Want To View More Record?";
                                        var ResultData = new
                                        {
                                            Message = SD_Message,
                                            JoiningDate = SD_JoiningDate,
                                            TerminDate = SD_TerminDate,
                                            TransferDate = SD_TransferDate,
                                            FirstName = SD_FirstName,
                                            LastName = SD_LastName,
                                            NewBranch = SD_NewBranch,
                                            CloseFlag = SD_CloseFlag,
                                            Transfer = SD_Transfer,
                                            Desig = SD_Desig,
                                            Level = SD_Level,
                                            FunTitle1 = SD_FunTitle1,
                                            FunTitle2 = SD_FunTitle2,
                                            LIST1 = SD_LIST1,
                                            LIST2 = SD_LIST2,
                                            EFFECTIVE = SD_EFFECTIVE,
                                            W_REP = SD_W_REP,
                                            W_Name = SD_W_Name,
                                            W_GOAL = SD_W_GOAL,
                                            dtW_GOAL_DATE = SD_dtW_GOAL_DATE
                                        };
                                        return Json(ResultData);
                                    }
                                }
                                else
                                {
                                    SD_Message = "THIS PERSONNEL ID. IS NOT VALID FOR THIS OPTION";
                                    var ResultData = new
                                    {
                                        Message = SD_Message,
                                        JoiningDate = SD_JoiningDate,
                                        TerminDate = SD_TerminDate,
                                        TransferDate = SD_TransferDate,
                                        FirstName = SD_FirstName,
                                        LastName = SD_LastName,
                                        NewBranch = SD_NewBranch,
                                        CloseFlag = SD_CloseFlag,
                                        Transfer = SD_Transfer,
                                        Desig = SD_Desig,
                                        Level = SD_Level,
                                        FunTitle1 = SD_FunTitle1,
                                        FunTitle2 = SD_FunTitle2,
                                        LIST1 = SD_LIST1,
                                        LIST2 = SD_LIST2,
                                        EFFECTIVE = SD_EFFECTIVE,
                                        W_REP = SD_W_REP,
                                        W_Name = SD_W_Name,
                                        W_GOAL = SD_W_GOAL,
                                        dtW_GOAL_DATE = SD_dtW_GOAL_DATE
                                    };
                                    return Json(ResultData);
                                }

                                #endregion
                                var ResultData1 = new
                                {
                                    Message = SD_Message,
                                    JoiningDate = SD_JoiningDate,
                                    TerminDate = SD_TerminDate,
                                    TransferDate = SD_TransferDate,
                                    FirstName = SD_FirstName,
                                    LastName = SD_LastName,
                                    NewBranch = SD_NewBranch,
                                    CloseFlag = SD_CloseFlag,
                                    Transfer = SD_Transfer,
                                    Desig = SD_Desig,
                                    Level = SD_Level,
                                    FunTitle1 = SD_FunTitle1,
                                    FunTitle2 = SD_FunTitle2,
                                    LIST1 = SD_LIST1,
                                    LIST2 = SD_LIST2,
                                    EFFECTIVE = SD_EFFECTIVE,
                                    W_REP = SD_W_REP,
                                    W_Name = SD_W_Name,
                                    W_GOAL = SD_W_GOAL,
                                    dtW_GOAL_DATE = SD_dtW_GOAL_DATE
                                };
                                return Json(ResultData1);
                            }
                            else
                            {
                                var ResultData = new
                                {
                                    Message = SD_Message,
                                    JoiningDate = SD_JoiningDate,
                                    TerminDate = SD_TerminDate,
                                    TransferDate = SD_TransferDate,
                                    FirstName = SD_FirstName,
                                    LastName = SD_LastName,
                                    NewBranch = SD_NewBranch,
                                    CloseFlag = SD_CloseFlag,
                                    Transfer = SD_Transfer,
                                    Desig = SD_Desig,
                                    Level = SD_Level,
                                    FunTitle1 = SD_FunTitle1,
                                    FunTitle2 = SD_FunTitle2,
                                    LIST1 = SD_LIST1,
                                    LIST2 = SD_LIST2,
                                    EFFECTIVE = SD_EFFECTIVE,
                                    W_REP = SD_W_REP,
                                    W_Name = SD_W_Name,
                                    W_GOAL = SD_W_GOAL,
                                    dtW_GOAL_DATE = SD_dtW_GOAL_DATE
                                };
                                return Json(ResultData);
                            }
                        }

                    }
                    #endregion
                }
                else
                {
                    SD_Message = "Record Not Found";
                    var ResultData = new
                    {
                        Message = SD_Message,
                        JoiningDate = SD_JoiningDate,
                        TerminDate = SD_TerminDate,
                        TransferDate = SD_TransferDate,
                        FirstName = SD_FirstName,
                        LastName = SD_LastName,
                        NewBranch = SD_NewBranch,
                        CloseFlag = SD_CloseFlag,
                        Transfer = SD_Transfer,
                        Desig = SD_Desig,
                        Level = SD_Level,
                        FunTitle1 = SD_FunTitle1,
                        FunTitle2 = SD_FunTitle2,
                        LIST1 = SD_LIST1,
                        LIST2 = SD_LIST2,
                        EFFECTIVE = SD_EFFECTIVE,
                        W_REP = SD_W_REP,
                        W_Name = SD_W_Name,
                        W_GOAL = SD_W_GOAL,
                        dtW_GOAL_DATE = SD_dtW_GOAL_DATE
                    };
                    return Json(ResultData);
                }
            }
            return Json(null);
        }
        private void FillGridForExistingDeptCont(string PNo, string Transfer, DateTime? TransferData)
        {
            try
            {
                Dictionary<string, object> colsNVals = new Dictionary<string, object>();
                colsNVals.Clear();
                colsNVals.Add("PR_TR_NO", PNo);
                colsNVals.Add("PR_TRANSFER_DATE", TransferData);
                colsNVals.Add("PR_TRANSFER", Transfer);
                CmnDataManager objCmnDataManager = new CmnDataManager();
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_PERSONNEL_SEGMENT_TRANSFER_MANAGER", "DeptCont_ExistingRecord", colsNVals);
                if (rsltCode.isSuccessful)
                {
                    if (rsltCode.dstResult.Tables.Count > 0)
                    {
                        if (rsltCode.dstResult.Tables[0].Rows.Count > 0)
                        {
                            DataTable firstTable = rsltCode.dstResult.Tables[0];

                            var CatResul = JsonConvert.SerializeObject(firstTable);
                            SD_LIST1 = CatResul;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                SD_Message = ex.Message;
            }
        }
        private void SetListValues(DataTable dtTransfer, string PrNo)
        {

            SD_FunTitle1 = dtTransfer.Rows[0]["PR_FUNC_1"].ToString();
            SD_FunTitle2 = dtTransfer.Rows[0]["PR_FUNC_2"].ToString();
            if (dtTransfer.Rows[0]["PR_EFFECTIVE"].ToString() != string.Empty)
            {
                SD_EFFECTIVE = Convert.ToDateTime(dtTransfer.Rows[0]["PR_EFFECTIVE"]);
            }
            //Block 2 post query
            Dictionary<string, object> colsNVals = new Dictionary<string, object>();
            colsNVals.Clear();
            colsNVals.Add("PR_TR_NO", PrNo);
            DataTable dtBlock2PostQuery = GetData("CHRIS_SP_PERSONNEL_SEGMENT_TRANSFER_MANAGER", "Block2_PostQuery", colsNVals);
            if (dtBlock2PostQuery != null)
            {
                SD_W_REP = dtBlock2PostQuery.Rows[0]["w_rep"].ToString();
                SD_W_Name = dtBlock2PostQuery.Rows[0]["w_name"].ToString();
                SD_W_GOAL = dtBlock2PostQuery.Rows[0]["W_GOAL"].ToString();
                if (dtBlock2PostQuery.Rows[0]["W_GOAL_DATE"].ToString() != string.Empty)
                {
                    SD_dtW_GOAL_DATE = Convert.ToDateTime(dtBlock2PostQuery.Rows[0]["W_GOAL_DATE"]);
                }
            }

        }
        private DataTable GetData(string sp, string actionType, Dictionary<string, object> param)
        {
            DataTable dt = null;

            Result rsltCode;
            CmnDataManager cmnDM = new CmnDataManager();
            rsltCode = cmnDM.GetData(sp, actionType, param);

            if (rsltCode.isSuccessful)
            {
                if (rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                {
                    dt = rsltCode.dstResult.Tables[0];
                }
            }

            return dt;

        }
        [HttpPost]
        public JsonResult GetSegmentDepartUpdateList(string PNo, DateTime Effective)
        {
            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("PR_TR_NO", PNo);
            d.Add("PR_EFFECTIVE", Effective);
            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_DEPT_CONT_TRANSFER_DEPT_CONT_MANAGER", "List", d);
            if (rsltCode.isSuccessful)
            {
                if (rsltCode.dstResult.Tables.Count > 0)
                {
                    if (rsltCode.dstResult.Tables[0].Rows.Count > 0)
                    {
                        DataTable firstTable = rsltCode.dstResult.Tables[0];

                        var CatResul = JsonConvert.SerializeObject(firstTable);
                        return Json(CatResul);
                    }
                    else
                        return Json(null);
                }
                else
                    return Json(null);
            }
            else
                return Json(null);
        }
        [HttpPost]
        public JsonResult AddUpdateSegmentDepartmentListData(int Id, decimal PrNo, string Segment, string Dept, string Contri, string Effective)
        {
            Dictionary<string, object> d = new Dictionary<string, object>();

            CmnDataManager objCmnDataManager = new CmnDataManager();
            if (Id > 0)
            {
                d.Add("PR_D_NO", PrNo);
                d.Add("PR_SEGMENT", Segment);
                d.Add("PR_DEPT", Dept);
                d.Add("PR_CONTRIB", Contri);
                d.Add("PR_MENU_OPTION", null);
                d.Add("PR_TYPE", 'R');
                d.Add("PR_EFFECTIVE_DATE", Effective);
                d.Add("ID", Id);
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_DEPT_CONT_TRANSFER_DEPT_CONT_UPDATE", "", d);
                if (rsltCode.isSuccessful)
                {
                    return Json("Data updated successfully.");
                }
                else
                    return Json("Unable to update record.");
            }
            else
            {
                d.Add("PR_D_NO", PrNo);
                d.Add("PR_SEGMENT", Segment);
                d.Add("PR_DEPT", Dept);
                d.Add("PR_CONTRIB", Contri);
                d.Add("PR_MENU_OPTION", null);
                d.Add("PR_TYPE", 'R');
                d.Add("PR_EFFECTIVE", Effective);
                d.Add("ID", Id);
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_DEPT_CONT_TRANSFER_DEPT_CONT_ADD", "", d);
                if (rsltCode.isSuccessful)
                {
                    return Json("Data inserted successfully.");
                }
                else
                    return Json("Unable to insert record.");
            }
        }
        [HttpPost]
        public JsonResult AddUpdateSegmentDepartmentMasterDate(int SD_ID, decimal SD_PNO, string SD_JoiningDate, string SD_TerminDate, string SD_TransferDate, string SD_Branch,
            string SD_Desig, string SD_Level, string SD_Functional, string SD_Title, string SD_TransferType, string SD_EffectiveDate, string SD_ReportTo, string SD_GoalDate, string SD_Goal)
        {
            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("PR_TR_NO", SD_PNO);
            d.Add("PR_TRANSFER_TYPE", SD_TransferType);
            d.Add("PR_FUNC_1", SD_Functional);
            d.Add("PR_FUNC_2", SD_Title);
            d.Add("PR_NEW_BRANCH", SD_Branch);
            //d.Add("PR_FURLOUGH", null);
            d.Add("PR_DESG", SD_Desig);
            d.Add("PR_LEVEL", SD_Level);
            //d.Add("PR_COUNTRY", null);
            //d.Add("PR_CITY", null);
            //d.Add("PR_DEPARTMENT_HC", null);
            //d.Add("PR_ASR_DOL", null);
            //d.Add("PR_FAST_END_DATE", null);
            //d.Add("PR_IS_COORDINAT", null);
            //d.Add("PR_REMARKS", null);
            //d.Add("PR_FAST_CONVER", null);
            //d.Add("PR_FLAG", null);
            d.Add("PR_EFFECTIVE", SD_EffectiveDate);
            //d.Add("PR_RENT", null);
            //d.Add("PR_UTILITIES", null);
            //d.Add("PR_TAX_ON_TAX", null);
            //d.Add("PR_OLD_BRANCH", null);
            //d.Add("CITI_FLAG1", null);
            //d.Add("CITI_FLAG2", null);
            //d.Add("PR_NEW_ASR", null);
            //d.Add("CURRENCY_TYPE", null);
            d.Add("W_GOAL_DATE", SD_GoalDate);
            d.Add("W_REP", SD_ReportTo);
            d.Add("W_GOAL", SD_Goal);
            d.Add("ID", SD_ID);
            CmnDataManager objCmnDataManager = new CmnDataManager();
            if (SD_ID > 0)
            {
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_PERSONNEL_SEGMENT_TRANSFER_MANAGER", "Update", d);
                if (rsltCode.isSuccessful)
                {
                    return Json("Data updated successfully.");
                }
                else
                    return Json("Unable to update record.");
            }
            else
            {
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_PERSONNEL_SEGMENT_TRANSFER_MANAGER", "Save", d);
                if (rsltCode.isSuccessful)
                {
                    return Json("Data inserted successfully.");
                }
                else
                    return Json("Unable to insert record.");
            }
        }
        [HttpPost]
        public JsonResult ValidateSegmenDepartmenttReportTo(string RreportTo)
        {
            try
            {
                if (RreportTo != string.Empty)
                {
                    Dictionary<string, object> dicInputParameters = new Dictionary<string, object>();

                    dicInputParameters.Add("W_REP", RreportTo);

                    CmnDataManager objCmnDataManager = new CmnDataManager();
                    Result rsltW_REP = objCmnDataManager.GetData("CHRIS_SP_PERSONNEL_SEGMENT_TRANSFER_MANAGER", "ReportingTo", dicInputParameters);

                    if (rsltW_REP.isSuccessful)
                    {
                        if (rsltW_REP.dstResult != null)
                        {
                            if (rsltW_REP.dstResult.Tables.Count > 0)
                            {
                                if (rsltW_REP.dstResult.Tables[0].Rows.Count < 1)
                                    return Json("Invalid Personnel No......");
                            }
                        }
                    }
                    else
                        return Json("Invalid Personnel No......");

                }

            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
            return Json(null);
        }
        [HttpPost]
        public JsonResult DeleteSegmenDepartmentRecord(string PNO, DateTime EFFECTIVE_DATE, DateTime GOAL_DATE, string GOAL)
        {
            Dictionary<string, object> d = new Dictionary<string, object>();

            d.Add("PR_TR_NO", PNO);
            d.Add("PR_EFFECTIVE", EFFECTIVE_DATE);
            d.Add("W_GOAL_DATE", GOAL_DATE);
            d.Add("W_GOAL", GOAL);

            CmnDataManager objCmnDataManager = new CmnDataManager();
            if (PNO != null && PNO != "")
            {
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_PERSONNEL_SEGMENT_TRANSFER_MANAGER", "Delete", d);
                if (rsltCode.isSuccessful)
                {
                    return Json("Data deleted successfully.");
                }
                else
                    return Json("Unable to delete record.");
            }
            else
                return Json("No Personnel No selected.");
            return Json(null);
        }
        #endregion

        #region BRANCH TO NRANCH TRANSFER
        public IActionResult BranchManagerTransfer()
        {
            return View();
        }
        [HttpPost]
        public JsonResult GetBranchToBranchDescriptionData(string Action, string Desc)
        {
            Dictionary<string, object> d = new Dictionary<string, object>();
            if (Action == "PrNoList")
            {
                CmnDataManager objCmnDataManager = new CmnDataManager();
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_BRANCHMANAGERTRANSFER_MANAGER", "MAIN_LOV", d);
                if (rsltCode.isSuccessful)
                {
                    if (rsltCode.dstResult.Tables.Count > 0)
                    {
                        if (rsltCode.dstResult.Tables[0].Rows.Count > 0)
                        {
                            DataTable firstTable = rsltCode.dstResult.Tables[0];

                            var CatResul = JsonConvert.SerializeObject(firstTable);
                            return Json(CatResul);
                        }
                        else
                            return Json(null);
                    }
                    else
                        return Json(null);
                }
                else
                    return Json(null);
            }
            else
            {
                if (Action == "ADD")
                {
                    if (Desc == "INTER BRANCH")
                        d.Add("SearchFilter", "Inter Branch" + "|" + "A");
                    else if (Desc == "RE-LOCATED")
                        d.Add("SearchFilter", "Re-located" + "|" + "A");
                    else if (Desc == "BRANCH MANAGERS")
                        d.Add("SearchFilter", "Branch Managers" + "|" + "A");
                    else if (Desc == "LOCAL")
                        d.Add("SearchFilter", "LOCAL" + "|" + "A");
                }
                else if (Action == "UPDATE")
                {
                    if (Desc == "INTER BRANCH")
                        d.Add("SearchFilter", "Inter Branch" + "|" + "M");
                    else if (Desc == "RE-LOCATED")
                        d.Add("SearchFilter", "Re-located" + "|" + "M");
                    else if (Desc == "BRANCH MANAGERS")
                        d.Add("SearchFilter", "Branch Managers" + "|" + "M");
                    else if (Desc == "LOCAL")
                        d.Add("SearchFilter", "LOCAL" + "|" + "M");
                }
                else if (Action == "DELETE")
                {
                    if (Desc == "INTER BRANCH")
                        d.Add("SearchFilter", "Inter Branch" + "|" + "D");
                    else if (Desc == "RE-LOCATED")
                        d.Add("SearchFilter", "Re-located" + "|" + "D");
                    else if (Desc == "BRANCH MANAGERS")
                        d.Add("SearchFilter", "Branch Managers" + "|" + "D");
                    else if (Desc == "LOCAL")
                        d.Add("SearchFilter", "LOCAL" + "|" + "D");
                }
                else if (Action == "VIEW")
                {
                    if (Desc == "INTER BRANCH")
                        d.Add("SearchFilter", "Inter Branch" + "|" + "V");
                    else if (Desc == "RE-LOCATED")
                        d.Add("SearchFilter", "Re-located" + "|" + "V");
                    else if (Desc == "BRANCH MANAGERS")
                        d.Add("SearchFilter", "Branch Managers" + "|" + "V");
                    else if (Desc == "LOCAL")
                        d.Add("SearchFilter", "LOCAL" + "|" + "V");
                }
                CmnDataManager objCmnDataManager = new CmnDataManager();
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_BRANCHMANAGERTRANSFER_MANAGER", "PP_LOV", d);
                if (rsltCode.isSuccessful)
                {
                    if (rsltCode.dstResult.Tables.Count > 0)
                    {
                        if (rsltCode.dstResult.Tables[0].Rows.Count > 0)
                        {
                            DataTable firstTable = rsltCode.dstResult.Tables[0];

                            var CatResul = JsonConvert.SerializeObject(firstTable);
                            return Json(CatResul);
                        }
                        else
                            return Json(null);
                    }
                    else
                        return Json(null);
                }
                else
                    return Json(null);
            }
        }
        [HttpPost]
        public JsonResult GetBTBDataAgainstPersonnelNo(string PNo, string Action, string Desc)
        {
            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("PR_P_NO", PNo);
            if (Action == "ADD")
            {
                if (Desc == "INTER BRANCH")
                    d.Add("SearchFilter", "Inter Branch" + "|" + "A");
                else if (Desc == "RE-LOCATED")
                    d.Add("SearchFilter", "Re-located" + "|" + "A");
                else if (Desc == "BRANCH MANAGERS")
                    d.Add("SearchFilter", "Branch Managers" + "|" + "A");
                else if (Desc == "LOCAL")
                    d.Add("SearchFilter", "LOCAL" + "|" + "A");
            }
            else if (Action == "UPDATE")
            {
                if (Desc == "INTER BRANCH")
                    d.Add("SearchFilter", "Inter Branch" + "|" + "M");
                else if (Desc == "RE-LOCATED")
                    d.Add("SearchFilter", "Re-located" + "|" + "M");
                else if (Desc == "BRANCH MANAGERS")
                    d.Add("SearchFilter", "Branch Managers" + "|" + "M");
                else if (Desc == "LOCAL")
                    d.Add("SearchFilter", "LOCAL" + "|" + "M");
            }
            else if (Action == "DELETE")
            {
                if (Desc == "INTER BRANCH")
                    d.Add("SearchFilter", "Inter Branch" + "|" + "D");
                else if (Desc == "RE-LOCATED")
                    d.Add("SearchFilter", "Re-located" + "|" + "D");
                else if (Desc == "BRANCH MANAGERS")
                    d.Add("SearchFilter", "Branch Managers" + "|" + "D");
                else if (Desc == "LOCAL")
                    d.Add("SearchFilter", "LOCAL" + "|" + "D");
            }
            else if (Action == "VIEW")
            {
                if (Desc == "INTER BRANCH")
                    d.Add("SearchFilter", "Inter Branch" + "|" + "V");
                else if (Desc == "RE-LOCATED")
                    d.Add("SearchFilter", "Re-located" + "|" + "V");
                else if (Desc == "BRANCH MANAGERS")
                    d.Add("SearchFilter", "Branch Managers" + "|" + "V");
                else if (Desc == "LOCAL")
                    d.Add("SearchFilter", "LOCAL" + "|" + "V");
            }
            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_BRANCHMANAGERTRANSFER_MANAGER", "PP_NO_EXISTS", d);
            if (rsltCode.isSuccessful)
            {
                if (rsltCode.dstResult.Tables.Count > 0)
                {
                    if (rsltCode.dstResult.Tables[0].Rows.Count > 0)
                    {
                        DataTable firstTable = rsltCode.dstResult.Tables[0];

                        var CatResul = JsonConvert.SerializeObject(firstTable);
                        return Json(CatResul);
                    }
                    else
                        return Json(null);
                }
                else
                    return Json(null);
            }
            else
                return Json(null);
        }
        [HttpPost]
        public JsonResult GetBTBReadOnlyList(string PNo, string TransferDate)
        {
            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("PR_P_NO", PNo);
            d.Add("PR_TRANSFER_DATE", TransferDate);
            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_DEPT_CONT_BRANCHMANAGER_MANAGER_ReadOnly", "List", d);
            if (rsltCode.isSuccessful)
            {
                if (rsltCode.dstResult.Tables.Count > 0)
                {
                    if (rsltCode.dstResult.Tables[0].Rows.Count > 0)
                    {
                        DataTable firstTable = rsltCode.dstResult.Tables[0];

                        var CatResul = JsonConvert.SerializeObject(firstTable);
                        return Json(CatResul);
                    }
                    else
                        return Json(null);
                }
                else
                    return Json(null);
            }
            else
                return Json(null);
        }
        [HttpPost]
        public JsonResult GetRestofFields(string PNo, string TransferType)
        {
            string Branch = "";
            string City = "";
            string Function1 = "";
            string Function2 = "";
            string ID = "";
            string Rent = "";
            string Utilities = "";
            string TaxOnTax = "";
            DateTime? EffectiveDate = null;
            string Remarks = "";
            Dictionary<string, object> colsNVals = new Dictionary<string, object>();
            colsNVals.Clear();
            colsNVals.Add("PR_P_NO", PNo);
            colsNVals.Add("PR_TRANSFER_TYPE", TransferType);
            DataTable dtBlock2PostQuery = GetData("CHRIS_TRANSFER_BRANCHMANAGER_MANAGER", "GetAll_Fields", colsNVals);
            if (dtBlock2PostQuery != null)
            {
                ID = dtBlock2PostQuery.Rows[0]["ID"].ToString();
                Branch = dtBlock2PostQuery.Rows[0]["PR_NEW_BRANCH"].ToString();
                City = dtBlock2PostQuery.Rows[0]["PR_FURLOUGH"].ToString();
                Function1 = dtBlock2PostQuery.Rows[0]["PR_FUNC_1"].ToString();
                Function2 = dtBlock2PostQuery.Rows[0]["PR_FUNC_2"].ToString();
                if (dtBlock2PostQuery.Rows[0]["PR_EFFECTIVE"] != null)
                    EffectiveDate = Convert.ToDateTime(dtBlock2PostQuery.Rows[0]["PR_EFFECTIVE"].ToString());
                Remarks = dtBlock2PostQuery.Rows[0]["PR_REMARKS"].ToString();
                Rent = dtBlock2PostQuery.Rows[0]["PR_RENT"].ToString();
                Utilities = dtBlock2PostQuery.Rows[0]["PR_UTILITIES"].ToString();
                TaxOnTax = dtBlock2PostQuery.Rows[0]["PR_TAX_ON_TAX"].ToString();
                var ResultData = new
                {
                    ID = ID,
                    Branch = Branch,
                    City = City,
                    Function1 = Function1,
                    Function2 = Function2,
                    EffectiveDate = EffectiveDate,
                    Remarks = Remarks,
                    Rent = Rent,
                    Utilities = Utilities,
                    TaxOnTax = TaxOnTax
                };
                return Json(ResultData);
            }
            return Json(null);
        }
        [HttpPost]
        public JsonResult GetBTBEditableList(string PNo, string TransferDate)
        {
            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("PR_P_NO", PNo);
            d.Add("PR_EFFECTIVE", TransferDate);
            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_DEPT_CONT_BRANCHMANAGER_MANAGER", "List", d);
            if (rsltCode.isSuccessful)
            {
                if (rsltCode.dstResult.Tables.Count > 0)
                {
                    if (rsltCode.dstResult.Tables[0].Rows.Count > 0)
                    {
                        DataTable firstTable = rsltCode.dstResult.Tables[0];

                        var CatResul = JsonConvert.SerializeObject(firstTable);
                        return Json(CatResul);
                    }
                    else
                        return Json(null);
                }
                else
                    return Json(null);
            }
            else
                return Json(null);
        }
        [HttpPost]
        public JsonResult GetBTBDepartmentList(string Segment)
        {
            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("PR_SEGMENT", Segment);
            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_DEPT_CONT_BRANCHMANAGER_MANAGER", "DEPT_LOV", d);
            if (rsltCode.isSuccessful)
            {
                if (rsltCode.dstResult.Tables.Count > 0)
                {
                    if (rsltCode.dstResult.Tables[0].Rows.Count > 0)
                    {
                        DataTable firstTable = rsltCode.dstResult.Tables[0];

                        var CatResul = JsonConvert.SerializeObject(firstTable);
                        return Json(CatResul);
                    }
                    else
                        return Json(null);
                }
                else
                    return Json(null);
            }
            else
                return Json(null);
        }
        [HttpPost]
        public JsonResult AddUpdateBTBListData(int Id, decimal PrNo, string Segment, string Dept, string Contri, string Effective, string TransferType)
        {
            Dictionary<string, object> d = new Dictionary<string, object>();

            CmnDataManager objCmnDataManager = new CmnDataManager();
            if (Id > 0)
            {
                d.Add("PR_P_NO", PrNo);
                d.Add("PR_SEGMENT", Segment);
                d.Add("PR_DEPT", Dept);
                d.Add("PR_CONTRIB", Contri);
                d.Add("PR_MENU_OPTION", TransferType);
                d.Add("PR_TYPE", 'R');
                d.Add("PR_EFFECTIVE", Effective);
                d.Add("ID", Id);
                Result rsltCode = objCmnDataManager.GetData("CHRIS_DEPT_CONT_BRANCHMANAGER_MANAGER", "Update", d);
                if (rsltCode.isSuccessful)
                {
                    return Json("Data updated successfully.");
                }
                else
                    return Json("Unable to update record.");
            }
            else
            {
                d.Add("PR_P_NO", PrNo);
                d.Add("PR_SEGMENT", Segment);
                d.Add("PR_DEPT", Dept);
                d.Add("PR_CONTRIB", Contri);
                d.Add("PR_MENU_OPTION", TransferType);
                d.Add("PR_TYPE", 'R');
                d.Add("PR_EFFECTIVE", Effective);
                d.Add("ID", Id);
                Result rsltCode = objCmnDataManager.GetData("CHRIS_DEPT_CONT_BRANCHMANAGER_MANAGER", "Save", d);
                if (rsltCode.isSuccessful)
                {
                    return Json("Data inserted successfully.");
                }
                else
                    return Json("Unable to insert record.");
            }
        }
        [HttpPost]
        public JsonResult ValidateBTBEffectiveDate(string PNo, string EffectiveDate, string Action)
        {
            DateTime dtimeTransfer = new DateTime();
            DateTime dtimeTransfer_proc1 = new DateTime();
            Dictionary<string, object> objVals = new Dictionary<string, object>();
            Result rslt, rsltDptCnt, rsltTrnfr;
            DataTable dtPrsnl = new DataTable();
            DataTable dtDpt = new DataTable();
            DataTable dtTransfer = new DataTable();
            DateTime? dtFirst, dtSecond;

            if (EffectiveDate != null)
            {
                if (PNo != "")
                {

                    objVals.Clear();
                    objVals.Add("PR_P_NO", PNo);

                    rsltTrnfr = cmnDM.GetData("CHRIS_SP_BRANCHMANAGERTRANSFER_MANAGER", "TRNSFR_VAL", objVals);

                    if (rsltTrnfr.isSuccessful)
                    {
                        if (rsltTrnfr.dstResult.Tables.Count > 0)
                        {
                            dtTransfer = rsltTrnfr.dstResult.Tables[0];
                            if (dtTransfer.Rows.Count > 0)
                            {
                                dtFirst = Convert.ToDateTime(EffectiveDate);

                                if (rsltTrnfr.dstResult.Tables[0].Rows[0].ItemArray[0] != DBNull.Value /*|| rsltTrnfr.dstResult.Tables[0].Rows[0].ItemArray[0] != null*/)
                                    dtSecond = Convert.ToDateTime(rsltTrnfr.dstResult.Tables[0].Rows[0].ItemArray[0].ToString());
                                else
                                    dtSecond = null;

                                if (rsltTrnfr.dstResult.Tables[0].Rows[0].ItemArray[1] != DBNull.Value /*|| rsltTrnfr.dstResult.Tables[0].Rows[0].ItemArray[1] != null*/)
                                    DateTime.TryParse(rsltTrnfr.dstResult.Tables[0].Rows[0].ItemArray[1].ToString(), out dtimeTransfer);
                                //if (DateTime.Compare(Convert.ToDateTime(dtpTEffDate.Text), Convert.ToDateTime(dtTransfer.Rows[0]["PR_JOINING_DATE"])) < 0)
                                DateTime? CHeck = dtSecond;
                                if (dtFirst < CHeck)
                                {
                                    return Json("DATE HAS TO BE GREATER THAN THE JOINING DATE :  " + dtSecond);
                                }
                                //horrible condition.if (dtpTEffDate.Text == Convert.ToString(dtTransfer.Rows[0]["PR_JOINING_DATE"]))
                                if (dtimeTransfer.Year != 1)
                                {
                                    if (Action == "ADD")
                                    {
                                        if (dtFirst == dtimeTransfer)
                                        {
                                            return Json("RECORD FOR THIS DATE ALREADY EXISTS ");
                                        }
                                        else
                                        {
                                            #region PROC1


                                            objVals.Clear();
                                            objVals.Add("PR_P_NO", PNo);
                                            objVals.Add("PR_EFF", EffectiveDate);

                                            rsltTrnfr = cmnDM.GetData("CHRIS_SP_BRANCHMANAGERTRANSFER_MANAGER", "TRNSFR_025", objVals);

                                            if (rsltTrnfr.isSuccessful)
                                            {
                                                if (rsltTrnfr.dstResult != null && rsltTrnfr.dstResult.Tables.Count > 0)
                                                {
                                                    if (rsltTrnfr.dstResult.Tables[0].Rows.Count == 1)
                                                    {
                                                        if (rsltTrnfr.dstResult.Tables[0].Rows[0].ItemArray[0] != null)
                                                            DateTime.TryParse(rsltTrnfr.dstResult.Tables[0].Rows[0].ItemArray[0].ToString(), out dtimeTransfer_proc1);
                                                        return Json("  INAVLID DATE. DATE HAS TO BE GREATER THAN  " + dtimeTransfer_proc1.ToString("dd-MMM-yyyy").ToUpper());
                                                        //if (dtimeTransfer_proc1.Year != 1){}

                                                    }
                                                    else if (rsltTrnfr.dstResult.Tables[0].Rows.Count == 0)
                                                    {
                                                        return Json(null);
                                                    }
                                                    else if (rsltTrnfr.dstResult.Tables[0].Rows.Count > 1)
                                                    {
                                                        if (rsltTrnfr.dstResult.Tables[1].Rows.Count > 0)
                                                        {
                                                            //rsltTrnfr.dstResult.Tables[1] contains SELECT MAX(pr_effective) FROM   TRANSFER WITH (NOLOCK)WHERE  pr_tr_no = @PR_P_NO
                                                            if (rsltTrnfr.dstResult.Tables[1].Rows[0].ItemArray[0] != null)
                                                                DateTime.TryParse(rsltTrnfr.dstResult.Tables[1].Rows[0].ItemArray[0].ToString(), out dtimeTransfer_proc1);
                                                            return Json("INAVLID DATE. DATE HAS TO BE GREATER THAN  " + dtimeTransfer_proc1.ToString("dd-MMM-yyyy").ToUpper());
                                                        }
                                                    }
                                                }
                                            }
                                            #endregion
                                        }

                                    }
                                    else if (Action == "UPDATE")
                                    {
                                        if (dtFirst != dtimeTransfer)
                                        {
                                            #region PROC1
                                            objVals.Clear();
                                            objVals.Add("PR_P_NO", PNo);
                                            objVals.Add("PR_EFF", EffectiveDate);

                                            rsltTrnfr = cmnDM.GetData("CHRIS_SP_BRANCHMANAGERTRANSFER_MANAGER", "TRNSFR_025", objVals);

                                            if (rsltTrnfr.isSuccessful)
                                            {
                                                if (rsltTrnfr.dstResult != null && rsltTrnfr.dstResult.Tables.Count > 0)
                                                {
                                                    if (rsltTrnfr.dstResult.Tables[0].Rows.Count == 1)
                                                    {
                                                        if (rsltTrnfr.dstResult.Tables[0].Rows[0].ItemArray[0] != null)
                                                            DateTime.TryParse(rsltTrnfr.dstResult.Tables[0].Rows[0].ItemArray[0].ToString(), out dtimeTransfer_proc1);
                                                        return Json("  INAVLID DATE. DATE HAS TO BE GREATER THAN  " + dtimeTransfer_proc1.Date.ToShortDateString());
                                                        //if (dtimeTransfer_proc1.Year != 1){}

                                                    }
                                                    else if (rsltTrnfr.dstResult.Tables[0].Rows.Count == 0)
                                                    {
                                                        return Json(null);
                                                        //dgvTSDC.BeginEdit(false);
                                                    }
                                                    else if (rsltTrnfr.dstResult.Tables[0].Rows.Count > 1)
                                                    {
                                                        if (rsltTrnfr.dstResult.Tables[1].Rows.Count > 0)
                                                        {
                                                            //rsltTrnfr.dstResult.Tables[1] contains SELECT MAX(pr_effective) FROM   TRANSFER WITH (NOLOCK)WHERE  pr_tr_no = @PR_P_NO
                                                            if (rsltTrnfr.dstResult.Tables[1].Rows[0].ItemArray[0] != null)
                                                                DateTime.TryParse(rsltTrnfr.dstResult.Tables[1].Rows[0].ItemArray[0].ToString(), out dtimeTransfer_proc1);
                                                            return Json("  INAVLID DATE. DATE HAS TO BE GREATER THAN  " + dtimeTransfer_proc1.Date.ToShortDateString());
                                                        }
                                                    }
                                                }
                                            }
                                            #endregion
                                        }
                                        else
                                            return Json(null);
                                    }

                                }
                                return Json(null);
                            }

                        }
                    }
                }
            }
            return Json(null);
        }
        [HttpPost]
        public JsonResult AddUpdateBranchToBranchMasterData(int ID, decimal PRNO, string TRANSFER_DATE, string DESIG, string LEVEL, string FUN_TITLE1, string FUN_TITLE2, string OLD_BRANCH,
            int? TRANSFER_TYPE, string NEW_BRANCH, string CITY, string EFFECTIVE_DATE, string REMARKS, string RENT, string UTILITIES, string TAX_ON_TAX)
        {
            Dictionary<string, object> d = new Dictionary<string, object>();

            CmnDataManager objCmnDataManager = new CmnDataManager();
            if (ID > 0)
            {
                d.Add("PR_P_NO", PRNO);
                d.Add("PR_TRANSFER_TYPE", TRANSFER_TYPE);
                d.Add("PR_FUNC_1", FUN_TITLE1);
                d.Add("PR_FUNC_2", FUN_TITLE2);
                d.Add("PR_NEW_BRANCH", NEW_BRANCH);
                d.Add("PR_FURLOUGH", CITY);
                //d.Add("PR_DESG", DESIG);
                //d.Add("PR_LEVEL", LEVEL);
                //d.Add("PR_COUNTRY", null);
                //d.Add("PR_CITY", null);
                //d.Add("PR_DEPARTMENT_HC", null);
                //d.Add("PR_ASR_DOL", null);
                //d.Add("PR_FAST_END_DATE", null);
                //d.Add("PR_IS_COORDINAT", null);
                d.Add("PR_REMARKS", REMARKS);
                //d.Add("PR_FAST_CONVER", null);
                //d.Add("PR_FLAG", null);
                d.Add("PR_EFFECTIVE", EFFECTIVE_DATE);
                d.Add("PR_RENT", RENT);
                d.Add("PR_UTILITIES", UTILITIES);
                d.Add("PR_TAX_ON_TAX", TAX_ON_TAX);
                d.Add("PR_OLD_BRANCH", OLD_BRANCH);
                //d.Add("CITI_FLAG1", null);
                //d.Add("CITI_FLAG2", null);
                //d.Add("PR_NEW_ASR", null);
                //d.Add("CURRENCY_TYPE", null);
                d.Add("ID", ID);
                Result rsltCode = objCmnDataManager.GetData("CHRIS_TRANSFER_BRANCHMANAGER_MANAGER", "Update", d);
                if (rsltCode.isSuccessful)
                {
                    return Json("Data updated successfully.");
                }
                else
                    return Json("Unable to update record.");
            }
            else
            {
                d.Add("PR_P_NO", PRNO);
                if (TRANSFER_TYPE == 0)
                    d.Add("PR_TRANSFER_TYPE", 1);
                else
                {
                    TRANSFER_TYPE = TRANSFER_TYPE + 1;
                    d.Add("PR_TRANSFER_TYPE", TRANSFER_TYPE);
                }
                d.Add("PR_FUNC_1", FUN_TITLE1);
                d.Add("PR_FUNC_2", FUN_TITLE2);
                d.Add("PR_NEW_BRANCH", NEW_BRANCH);
                d.Add("PR_FURLOUGH", CITY);
                //d.Add("PR_DESG", DESIG);
                //d.Add("PR_LEVEL", LEVEL);
                //d.Add("PR_COUNTRY", null);
                //d.Add("PR_CITY", null);
                //d.Add("PR_DEPARTMENT_HC", null);
                //d.Add("PR_ASR_DOL", null);
                //d.Add("PR_FAST_END_DATE", null);
                //d.Add("PR_IS_COORDINAT", null);
                d.Add("PR_REMARKS", REMARKS);
                //d.Add("PR_FAST_CONVER", null);
                //d.Add("PR_FLAG", null);
                d.Add("PR_EFFECTIVE", EFFECTIVE_DATE);
                d.Add("PR_RENT", RENT);
                d.Add("PR_UTILITIES", UTILITIES);
                d.Add("PR_TAX_ON_TAX", TAX_ON_TAX);
                d.Add("PR_OLD_BRANCH", OLD_BRANCH);
                //d.Add("CITI_FLAG1", null);
                //d.Add("CITI_FLAG2", null);
                //d.Add("PR_NEW_ASR", null);
                //d.Add("CURRENCY_TYPE", null);
                d.Add("ID", ID);
                Result rsltCode = objCmnDataManager.GetData("CHRIS_TRANSFER_BRANCHMANAGER_MANAGER", "Save", d);
                if (rsltCode.isSuccessful)
                {
                    return Json("Data inserted successfully.");
                }
                else
                    return Json("Unable to insert record.");
            }
        }
        [HttpPost]
        public JsonResult GetBTBrachList()
        {
            Dictionary<string, object> d = new Dictionary<string, object>();
            //d.Add("PR_P_NO", PNo);
            //d.Add("PR_TRANSFER_DATE", TransferDate);
            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_TRANSFER_BRANCHMANAGER_MANAGER", "BRN_LOV", d);
            if (rsltCode.isSuccessful)
            {
                if (rsltCode.dstResult.Tables.Count > 0)
                {
                    if (rsltCode.dstResult.Tables[0].Rows.Count > 0)
                    {
                        DataTable firstTable = rsltCode.dstResult.Tables[0];

                        var CatResul = JsonConvert.SerializeObject(firstTable);
                        return Json(CatResul);
                    }
                    else
                        return Json(null);
                }
                else
                    return Json(null);
            }
            else
                return Json(null);
        }
        [HttpPost]
        public JsonResult DeleteBranchToBranchRecord(string PNO, int ID, DateTime TRANSFER_DATE)
        {
            Dictionary<string, object> d = new Dictionary<string, object>();

            d.Add("PR_P_NO", PNO);
            d.Add("ID", ID);
            d.Add("PR_TRANSFER_DATE", TRANSFER_DATE);

            CmnDataManager objCmnDataManager = new CmnDataManager();
            if (PNO != null && PNO != "")
            {
                Result rsltCode = objCmnDataManager.GetData("CHRIS_TRANSFER_BRANCHMANAGER_MANAGER", "Delete", d);
                if (rsltCode.isSuccessful)
                {
                    return Json("Data deleted successfully.");
                }
                else
                    return Json("Unable to delete record.");
            }
            else
                return Json("No Personnel No selected.");
        }
        #endregion

        #region TerminationSettHold

        public IActionResult TerminationSettHold()
        {
            return View();
        }
        [HttpPost]
        public JsonResult ValidateTerminateSetHold(string PNO)
        {
            string name = "";
            string message = "";
            string destination = "";
            string level = "";
            string category = "";
            string branch = "";
            DateTime? effectivedate = null;
            string remarks = "";
            DateTime? transferdate = null;
            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Add("PR_TR_NO", PNO);
            DataTable dtDummy = this.GetData("CHRIS_SP_TRANSFER_MANAGER", "PersonnalLOV", param);
            if (dtDummy != null)
            {
                if (dtDummy.Rows.Count > 0)
                {
                    destination = dtDummy.Rows[0].ItemArray[0].ToString();
                    level = dtDummy.Rows[0].ItemArray[1].ToString();
                    category = dtDummy.Rows[0].ItemArray[2].ToString();
                    branch = dtDummy.Rows[0].ItemArray[4].ToString();
                    if (dtDummy.Rows[0].ItemArray[6].ToString() != string.Empty)
                        effectivedate = Convert.ToDateTime(dtDummy.Rows[0].ItemArray[6].ToString());
                    else
                        effectivedate = null;
                    if (dtDummy.Rows[0].ItemArray[3].ToString() != string.Empty)
                        transferdate = Convert.ToDateTime(dtDummy.Rows[0].ItemArray[3].ToString());
                    else
                        transferdate = null;
                    remarks = dtDummy.Rows[0].ItemArray[5].ToString();
                }
                else
                {
                    message = "Invalid Personnel Entered.";
                }
            }
            else
            {
                message = "Invalid Personnel Entered.";
            }
            DataTable dtDummy1 = this.GetData("CHRIS_SP_TRANSFER_MANAGER", "PersonnelLovListExists", param);
            if (dtDummy1 != null)
            {
                if (dtDummy1.Rows.Count > 0)
                {
                    name = dtDummy1.Rows[0].ItemArray[1].ToString();
                }
                else
                {
                    message = "Invalid Personnel Entered.";
                }
            }
            else
            {
                message = "Invalid Personnel Entered.";
            }
            var FinalResult = new
            {
                name = name,
                message = message,
                destination = destination,
                level = level,
                category = category,
                branch = branch,
                effectivedate = effectivedate,
                transferdate = transferdate,
                remarks = remarks
            };
            return Json(FinalResult);
        }
        public ActionResult btn_PR_TR_NO_HRE()
        {
            Dictionary<string, object> d = new Dictionary<string, object>();

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_TRANSFER_MANAGER", "PersonnelLovList", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            else
                return Json(null);
        }
        public ActionResult TblDept1(Decimal? PR_TR_NO, DateTime? PR_EFFECTIVE)
        {
            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("PR_P_NO", PR_TR_NO);
            d.Add("PR_EFFECTIVE", PR_EFFECTIVE);
            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_DEPT_CONT_GETALL", "", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }
        public ActionResult PR_REMARKS(Decimal? PR_TR_NO)
        {
            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("PR_TR_NO", PR_TR_NO);
            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_TRANSFER_MANAGER", "PersonnalLOV", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }
        public bool CheckforTransferType(Decimal? PR_TR_NO)
        {
            MessageBox = "";
            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Add("PR_TR_NO", PR_TR_NO);
            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_TRANSFER_MANAGER", "ReleaseValidate", param);
            DataTable dtRelease = rsltCode.dstResult.Tables[0];

            if (dtRelease != null)
            {
                if (dtRelease.Rows.Count > 0)
                {
                    MessageBox = "OK";
                }
                else if (dtRelease.Rows.Count <= 0)
                {
                    MessageBox = "Invalid Selection. Hold record does not exist.";
                }
            }
            else
            {
                MessageBox = "Invalid Selection. Hold record does not exist.";
            }
            return true;

        }
        public ActionResult Flag_Validation(Decimal? PR_TR_NO)
        {
            CheckforTransferType(PR_TR_NO);

            if (MessageBox != "OK")
            {
                return Json(MessageBox);
            }
            else
            {
                return Json(null);
            }
        }
        public ActionResult Submit_TSH(Decimal? PR_TR_NO, Decimal? PR_TRANSFER_TYPE, string PR_FUNC_1, string PR_FUNC_2, string PR_NEW_BRANCH, string PR_FURLOUGH, string PR_DESG, string PR_LEVEL, string PR_COUNTRY, string PR_CITY, string PR_DEPARTMENT_HC, Decimal? PR_ASR_DOL, DateTime? PR_FAST_END_DATE, string PR_IS_COORDINAT, string PR_REMARKS, DateTime? PR_FAST_CONVER, string PR_FLAG, DateTime? PR_EFFECTIVE, Decimal? PR_RENT, Decimal? PR_UTILITIES, Decimal? PR_TAX_ON_TAX, string PR_OLD_BRANCH, string CITI_FLAG1, string CITI_FLAG2, Decimal? PR_NEW_ASR, string CURRENCY_TYPE, int? ID, string pr_flag_HR)
        {
            try
            {
                Dictionary<string, object> d = new Dictionary<string, object>();

                d.Add("PR_TR_NO", PR_TR_NO);
                d.Add("PR_TRANSFER_TYPE", PR_TRANSFER_TYPE);
                d.Add("PR_FUNC_1", PR_FUNC_1);
                d.Add("PR_FUNC_2", PR_FUNC_2);
                d.Add("PR_NEW_BRANCH", PR_NEW_BRANCH);
                d.Add("PR_FURLOUGH", PR_FURLOUGH);
                d.Add("PR_DESG", PR_DESG);
                d.Add("PR_LEVEL", PR_LEVEL);
                d.Add("PR_COUNTRY", PR_COUNTRY);
                d.Add("PR_CITY", PR_CITY);
                d.Add("PR_DEPARTMENT_HC", PR_DEPARTMENT_HC);
                d.Add("PR_ASR_DOL", PR_ASR_DOL);
                d.Add("PR_FAST_END_DATE", PR_FAST_END_DATE);
                d.Add("PR_IS_COORDINAT", PR_IS_COORDINAT);
                d.Add("PR_REMARKS", PR_REMARKS);
                d.Add("PR_FAST_CONVER", PR_FAST_CONVER);
                d.Add("PR_FLAG", PR_FLAG);
                d.Add("PR_EFFECTIVE", PR_EFFECTIVE);
                d.Add("PR_RENT", PR_RENT);
                d.Add("PR_UTILITIES", PR_UTILITIES);
                d.Add("PR_TAX_ON_TAX", PR_TAX_ON_TAX);
                d.Add("PR_OLD_BRANCH  ", PR_OLD_BRANCH);
                d.Add("CITI_FLAG1", CITI_FLAG1);
                d.Add("CITI_FLAG2", CITI_FLAG2);
                d.Add("PR_NEW_ASR", PR_NEW_ASR);
                d.Add("CURRENCY_TYPE", CURRENCY_TYPE);
                d.Add("ID", ID);
                d.Add("pr_flag_HR", pr_flag_HR);


                CmnDataManager objCmnDataManager = new CmnDataManager();

                if (ID != null)
                {
                    Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_TRANSFER_MANAGER", "Update", d);
                    if (rsltCode.isSuccessful)
                    {
                        var res = Json(rsltCode);
                        return Json("Your request is modify successfully !");
                    }
                }
                else
                {
                    Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_TRANSFER_MANAGER", "Save", d);
                    if (rsltCode.isSuccessful)
                    {
                        var res = Json(rsltCode);
                        return Json("Your request is successfully completed !");
                    }
                }
                return Json(null);
            }
            catch (Exception ex)
            {
                return Json("Something went wrong");
            }
            return Json(null);
        }
        #endregion

        public IActionResult ActualLeaveEntry()
        {
            return View();
        }
        public IActionResult ActualLeaveEntry_gridview()
        {
            return View();
        }
        public IActionResult ActualLeaveEntry_Month()
        {
            return View();
        }
        public IActionResult ActualLeaveEntry_NewCriteria()
        {
            return View();
        }
        public IActionResult ConfirmationEntry_Detail()
        {
            return View();
        }
        public IActionResult ConfirmationEntry_Month()
        {
            return View();
        }
        public IActionResult ConfirmationEntry_NewCriteria()
        {
            return View();
        }
        public IActionResult ConfirmationEntry_View()
        {
            return View();
        }
        public IActionResult FastOrISConversion()
        {
            return View();
        }
        public IActionResult FastOrISTransfer()
        {
            return View();
        }
        public IActionResult FastOrISTransfer_CityPage()
        {
            return View();
        }
        public IActionResult FastOrISTransfer_Detail()
        {
            return View();
        }
        public IActionResult FastOrISTransfer_FastEndDatePage()
        {
            return View();
        }
        public IActionResult FastOrISTransfer_TransferPage()
        {
            return View();
        }
        public IActionResult GEIDEntry()
        {
            return View();
        }
        public IActionResult General_View()
        {
            return View();
        }

        public IActionResult IncrementOfficers_Designation()
        {
            return View();
        }
        public IActionResult IncrementOfficers_Detail()
        {
            return View();
        }
        public IActionResult IncrementOfficers_Level()
        {
            return View();
        }
        public IActionResult IncrementOfficers_View()
        {
            return View();
        }
        public IActionResult IncrementOfficersClerical_Course()
        {
            return View();
        }
        public IActionResult IncremPromotCleric()
        {
            return View();
        }
        public IActionResult Leaves_Carry_Forward()
        {
            return View();
        }
        public IActionResult LeavesCarryForward()
        {
            return View();
        }
        public IActionResult LeavesScheduleEntry()
        {
            return View();
        }
        public IActionResult LettersFollowupEntry()
        {
            return View();
        }
        public IActionResult MakerChecker()
        {
            return View();
        }
        public IActionResult OnePagerPersonalEntry()
        {
            return View();
        }
        public IActionResult OnePagerPersonalEntry_CitiRef()
        {
            return View();
        }
        public IActionResult OnePagerPersonalEntry_Education()
        {
            return View();
        }
        public IActionResult OnePagerPersonalEntry_EmpHistory()
        {
            return View();
        }
        public IActionResult OnePagerPersonalEntry_Save()
        {
            return View();
        }
        public IActionResult OnePagerPersonalEntry_Training()
        {
            return View();
        }
        public IActionResult PersonalEntryChecker()
        {
            return View();
        }
        public IActionResult RankEntry()
        {
            return View();
        }
        public IActionResult Re_Hiring()
        {
            return View();
        }
        public IActionResult RegularStaffHiringEnt_BLKDEPT()
        {
            return View();
        }
        public IActionResult RegularStaffHiringEnt_BLKEDU()
        {
            return View();
        }
        public IActionResult RegularStaffHiringEnt_BLKEMP()
        {
            return View();
        }
        public IActionResult RegularStaffHiringEnt_BLKPERS()
        {
            return View();
        }
        public IActionResult RegularStaffHiringEnt_BLKREF()
        {
            return View();
        }
        public IActionResult RegularStaffHiringEnt_MC()
        {
            return View();
        }
        public IActionResult RegularStaffHiringEnt_Page2()
        {
            return View();
        }
        public IActionResult ReHiring()
        {
            return View();
        }
        public IActionResult SegmtOrDeptTrans_Detail()
        {
            return View();
        }
        public IActionResult SegmtOrDeptTrans_View()
        {
            return View();
        }
        public IActionResult ShadowSalary()
        {
            return View();
        }
        public IActionResult TerminationEntry_Detail()
        {
            return View();
        }
        public IActionResult TerminationEntry_View()
        {
            return View();
        }
        public IActionResult TerminationHold()
        {
            return View();
        }
    }
}
