﻿using CHRIS.Common;
using CHRIS.Data;
using CHRIS.Services;
using DocumentFormat.OpenXml.Bibliography;
using DocumentFormat.OpenXml.Math;
using DocumentFormat.OpenXml.Office2010.Excel;
using DocumentFormat.OpenXml.Office2016.Drawing.ChartDrawing;
using DocumentFormat.OpenXml.Office2019.Drawing.Model3D;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Newtonsoft.Json;
using System.ComponentModel;
using System.Data;

namespace CHRIS.Controllers
{
    public class FinanceController : Controller
    {


        #region FinanceApplication

        public IActionResult FinanceApplication()
        {
            return View();
        }

        public ActionResult btn_PR_P_NO_FA()
        {
            Dictionary<string, object> d = new Dictionary<string, object>();

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_PERSONNELFinanceApplicaton_MANAGER", "Pesonnel_FinanceApp", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        public ActionResult btn_FN_TYPE_FA()
        {
            Dictionary<string, object> d = new Dictionary<string, object>();

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_FIN_TYPE_MANAGER", "FinNo_FinanceApp", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }
        public ActionResult tbl_FA(Decimal? PR_P_NO)
        {
            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("PR_P_NO", PR_P_NO);
            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_PERSONNEL_FinApp_MANAGER", "LIST", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        #endregion

        #region FinanceBookingEntry not completed

        #region Fields
        //public int MARKUP_RATE_DAYS = -1;
        private DateTime NewDate = new DateTime();
        private string g_wcnt = "";
        DateTime fnStartDate;
        DateTime fnMatDate;
        DataTable dtStartDate = null;
        DataTable dtfnMatDate = null;

        string txtaviledamt = "";
        string txtBookMarkUp = "";
        string txtWBasic = "";
        string txtInstallment = "";
        string txtFN_MORTG_PROP_ADD = "";
        string txtFN_MORTG_PROP_CITY = "";
        string txtFN_MORTG_PROP_VALSA = "";
        string txtFN_MORTG_PROP_VALVR = "";
        string txtFinanceNo = "";
        string GCount = "";
        string GYear = "";
        string FinNoType = "";
        string txtfnMonthly = "";
        String ratio = "0";

        String _FinType;
        String _FinNo;
        DateTime _StartDate = new DateTime();
        DateTime _ExpiryDate = new DateTime();
        DateTime _ExtraTime = new DateTime();
        Double _InstallMentAmt;
        Double _CreditRatio;
        Double _Amt;
        string _ID;

        #endregion
        public IActionResult FinanceBookingEntry()
        {
            return View();
        }
        public ActionResult btnFN_BOOK_PR_P_NO()
        {
            Dictionary<string, object> d = new Dictionary<string, object>();

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_Finance_fin_Booking_MANAGER", "FinBookingPersonalLov", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }
        public ActionResult btnFN_BOOK_FN_TYPE()
        {
            Dictionary<string, object> d = new Dictionary<string, object>();

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_Finance_fin_Booking_MANAGER", "FinBookingTypeLov", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }
        public ActionResult btnFN_BOOK_FN_SUBTYPE(string SearchFilter)
        {
            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("SearchFilter", SearchFilter);
            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_Finance_fin_Booking_MANAGER", "FinBookingSubTypeLov", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }
        public ActionResult btnFN_BOOK_FN_MORTG_PROP_CITY()
        {
            Dictionary<string, object> d = new Dictionary<string, object>();

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_Finance_fin_Booking_MANAGER", "FinBookingCityLov", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }
        public ActionResult txtPR_P_NO_Validation(string PR_P_NO)
        {
            string txtprCategory;
            string txtPrLevel;
            string txtPrNewAnnual;
            string dtprdBirth;
            string txtfnBranch;
            string txtprLoanPack;
            string Message;

            if (PR_P_NO != string.Empty)
            {
                Dictionary<string, object> param = new Dictionary<string, object>();
                param.Add("PR_P_NO", PR_P_NO);

                CmnDataManager objCmnDataManager = new CmnDataManager();
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_Finance_fin_Booking_MANAGER", "FinBookingPersonalLovValidated", param);
                if (rsltCode.isSuccessful)
                {
                    DataTable dtPersonalFields = rsltCode.dstResult.Tables[0];
                    var CatResul = JsonConvert.SerializeObject(dtPersonalFields);

                    if (dtPersonalFields != null)
                    {
                        if (dtPersonalFields.Rows.Count > 0)
                        {
                            Result rsltCode1 = objCmnDataManager.GetData("CHRIS_SP_Finance_fin_Booking_MANAGER", "FinBookingProc_Level", param);

                            if (rsltCode.isSuccessful)
                            {
                                DataTable dtProcLevel = rsltCode1.dstResult.Tables[0];
                                if (dtProcLevel.Rows.Count > 0)
                                {
                                    if (dtProcLevel.Rows[0].ItemArray[0].ToString() != string.Empty)
                                    {
                                        txtPrLevel = Convert.ToString(dtProcLevel.Rows[0].ItemArray[0].ToString());

                                        var txtPrLevel1 = new
                                        {
                                            one = CatResul,
                                            two = txtPrLevel,
                                        };
                                        return Json(txtPrLevel1);
                                    }
                                }
                            }
                        }
                        else
                        {
                            return Json(null);
                        }

                        double PR_LOAN_PACK = 0.0;
                        double Pr_New_Annual_Pack = 0.0;
                        double tenCBonus = 0.0;
                        string txtWTenCBonus = "0.0";
                        txtPrNewAnnual = "0.0";
                        if (txtWTenCBonus != string.Empty)
                        {
                            Pr_New_Annual_Pack = double.Parse(txtPrNewAnnual == "" ? "0" : txtPrNewAnnual);
                            PR_LOAN_PACK = Pr_New_Annual_Pack + 2400;
                            tenCBonus = double.Parse(txtWTenCBonus == "" ? "0" : txtWTenCBonus);
                            PR_LOAN_PACK = PR_LOAN_PACK + ((PR_LOAN_PACK * tenCBonus) / 100);
                            txtprLoanPack = PR_LOAN_PACK.ToString();
                        }
                    }
                    else
                    {
                        return Json(null);
                    }
                }
            }
            return Json(null);
        }
        public ActionResult txtaviledamt_Validated(string CAN_BE, string FN_AMT_AVAILED, string PR_NEW_ANNUAL_PACK, string PR_CATEGORY, string FACTOR, string CREDIT_RATIO, string txtVal3, string FN_MARKUP, string txtPrLevel, string FN_BRANCH, string txtWTenCBonus)
        {
            double LoanCanAvail = 0.0;
            double.TryParse(CAN_BE, out LoanCanAvail);
            if (FN_AMT_AVAILED == "0.00")
            {
                FN_AMT_AVAILED = "";
            }

            if (FN_AMT_AVAILED == string.Empty)
            {
                MessageBox = "";
                MessageBox = "VALUE HAS TO BE ENTERED & SHOULD BE <  " + LoanCanAvail;
                var res0 = new
                {
                    MessageBox = MessageBox,
                };
                return Json(res0);
            }
            else
            {
                if (double.Parse(FN_AMT_AVAILED) > LoanCanAvail)
                {
                    MessageBox = "";
                    MessageBox = "VALUE HAS TO BE ENTERED & SHOULD BE < '" + LoanCanAvail + "'";
                    var res1 = new
                    {
                        MessageBox = MessageBox,
                    };
                    return Json(res1);
                }
                #region fields
                double AUX44;
                double AUX55;
                int AUX99;
                double wVal3;
                string Cat = "";
                double fnCreditRatioPer;
                double lLESS_RATIO_AVAILED;
                double lAVAILABLE_RATIO;
                double lFACTOR;
                double lAMT_OF_LOAN_CAN_AVAIL;
                double lTOTAL_MONTHLY_INSTALLMENT;
                double fnAvailedAmount;
                double fnMonthlyMarkup;
                double CREDIT_RATIO1;
                double lmarkup;
                double wInstallment;
                double ClericalBonus = 0.0;
                double spAllowanceAmt = 0.0;
                #endregion

                AUX99 = int.Parse(PR_NEW_ANNUAL_PACK == null ? "0" : PR_NEW_ANNUAL_PACK);
                Cat = PR_CATEGORY;

                lAMT_OF_LOAN_CAN_AVAIL = double.Parse(CAN_BE == null ? "0" : CAN_BE);
                fnAvailedAmount = double.Parse(FN_AMT_AVAILED == null ? "0" : FN_AMT_AVAILED);
                lAMT_OF_LOAN_CAN_AVAIL = lAMT_OF_LOAN_CAN_AVAIL - fnAvailedAmount;

                lFACTOR = double.Parse(FACTOR == null ? "0" : FACTOR);

                lAVAILABLE_RATIO = lFACTOR * lAMT_OF_LOAN_CAN_AVAIL / 1000;

                lAVAILABLE_RATIO = Math.Round(lAVAILABLE_RATIO, 2, MidpointRounding.AwayFromZero);

                CREDIT_RATIO1 = double.Parse(CREDIT_RATIO == null ? "0" : CREDIT_RATIO);

                lLESS_RATIO_AVAILED = CREDIT_RATIO1 - lAVAILABLE_RATIO;

                lTOTAL_MONTHLY_INSTALLMENT = fnAvailedAmount * lFACTOR / 1000;

                wVal3 = double.Parse(txtVal3 == null ? "0" : txtVal3);
                lmarkup = double.Parse(FN_MARKUP == null ? "0" : FN_MARKUP);
                fnMonthlyMarkup = fnAvailedAmount * (wVal3 / 100) * lmarkup / MARKUP_RATE_DAYS;

                txtCanBeAvail = lAMT_OF_LOAN_CAN_AVAIL.ToString();
                txtAvailable = lAVAILABLE_RATIO.ToString();
                txtRatioAvail = lLESS_RATIO_AVAILED.ToString();
                txtTotalMonthInstall = lTOTAL_MONTHLY_INSTALLMENT.ToString();
                txtBookMarkUp = fnMonthlyMarkup.ToString();

                AUX44 = AUX99 / 12;
                AUX55 = AUX99 / 12;

                if (PR_CATEGORY == "C")
                {
                    //coding pending
                    ClericalBonus = Double.Parse(txtWTenCBonus == null ? "0" : txtWTenCBonus);
                    AUX44 = (ClericalBonus / 100) + 1;
                    AUX55 = AUX55 * AUX44;

                    Dictionary<string, object> param = new Dictionary<string, object>();
                    param.Clear();
                    param.Add("category", PR_CATEGORY);
                    param.Add("level", txtPrLevel);
                    param.Add("fn_branch", FN_BRANCH);
                    DataTable dtClericalAllowance = this.GetData("CHRIS_SP_Other_fin_Booking_MANAGER", "ClericalStaffCheck", param);
                    if (dtClericalAllowance != null)
                    {
                        if (dtClericalAllowance.Rows.Count > 0)
                        {
                            spAllowanceAmt = double.Parse(dtClericalAllowance.Rows[0].ItemArray[0].ToString() == "" ? "0" : dtClericalAllowance.Rows[0].ItemArray[0].ToString());
                            AUX55 = AUX55 + spAllowanceAmt;
                        }
                    }
                    else
                    {
                        MessageBox = "";
                        MessageBox = "UPDATE THE ALLOWANCE TABLE FOR GOVT. ALLOWANCE";
                        var res0 = new
                        {
                            MessageBox = MessageBox,
                        };
                        return Json(res0);
                    }
                }

                double monthly = 0.0;
                double fnCred = 0;
                monthly = Math.Round(lTOTAL_MONTHLY_INSTALLMENT, 2);
                txtfnMonthly = monthly.ToString();

                wInstallment = lTOTAL_MONTHLY_INSTALLMENT - fnMonthlyMarkup;
                txtInstallment = wInstallment.ToString();
                fnCreditRatioPer = (Math.Round(lLESS_RATIO_AVAILED, 2) / AUX55) * 100;
                txtBookRatio = fnCreditRatioPer.ToString();

                var res = new
                {
                    txtCanBeAvail = txtCanBeAvail,
                    txtAvailable = txtAvailable,
                    txtRatioAvail = txtRatioAvail,
                    txtTotalMonthInstall = txtTotalMonthInstall,
                    txtBookMarkUp = txtBookMarkUp,
                    txtfnMonthly = txtfnMonthly,
                    txtInstallment = txtInstallment,
                    txtBookRatio = txtBookRatio,
                    MessageBox = MessageBox,
                };
                return Json(res);

            }
            return Json(null);
        }
        public ActionResult DTstartdate_Validating(DateTime? FN_START_DATE, DateTime? pr_d_birth, string fn_schedule, string PR_P_NO)
        {
            DateTime w_ysy = new DateTime();
            DateTime checkDOB = new DateTime();
            string txtfnPaySchedule = "";
            decimal DateDiff = 0;
            decimal wSum = 0;
            decimal dlschedule = 0;
            if (FN_START_DATE == null)
            {
                MessageBox = "";
                MessageBox = "DATE HAS TO BE ENTERED AND >= THE CURRENT YEAR";

                var res = new
                {
                    MessageBox = MessageBox,
                };
                return Json(res);
            }
            else
            {
                checkDOB = Convert.ToDateTime(pr_d_birth);
                if (checkDOB.Date.CompareTo(DateTime.Now.Date) != 0)
                {
                    int lschedule = 0;
                    DateTime lStartDate = Convert.ToDateTime(FN_START_DATE);
                    DateTime lPRDBirth = Convert.ToDateTime(pr_d_birth);
                    if (fn_schedule != null)
                    {
                        lschedule = int.Parse(fn_schedule);
                    }
                    //int lschedule2 = Convert.ToInt32(lschedule);
                    w_ysy = lStartDate.AddMonths(lschedule);
                    //TimeSpan ts = NewDate.Subtract(lPRDBirth);
                    //int mMonth = NewDate.Month - lPRDBirth.Month;
                    //int wSum = (12 * (NewDate.Year - lPRDBirth.Year)) + mMonth;

                    Dictionary<string, object> param = new Dictionary<string, object>();
                    param.Add("FN_START_DATE", w_ysy);
                    param.Add("FN_END_DATE", lPRDBirth);

                    Result rsltCode;
                    CmnDataManager cmnDM = new CmnDataManager();
                    rsltCode = cmnDM.GetData("CHRIS_SP_Finance_fin_Booking_MANAGER", "MONTH_BETWEEN", param);
                    if (rsltCode.isSuccessful && rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                    {
                        DateDiff = decimal.Parse(rsltCode.dstResult.Tables[0].Rows[0].ItemArray[0].ToString());

                        wSum = DateDiff;
                    }
                    dlschedule = Convert.ToDecimal(lschedule);
                    if (wSum > 696)
                    {
                        wSum = wSum - 696;
                        dlschedule = Math.Abs(dlschedule - wSum);

                    }
                    lschedule = Convert.ToInt32(Math.Round(dlschedule, 0));
                    txtfnPaySchedule = lschedule.ToString();
                    NewDate = lStartDate.AddMonths(lschedule);

                    DTFN_END_DATE = NewDate;

                    var res = new
                    {
                        txtfnPaySchedule = txtfnPaySchedule,
                        DTFN_END_DATE = DTFN_END_DATE,
                        MessageBox = MessageBox,
                    };
                    return Json(res);
                }
                else
                {
                    if (PR_P_NO != "")
                    {
                        MessageBox = "";
                        MessageBox = "Birth Date does not Exists.";

                        var res = new
                        {
                            txtfnPaySchedule = txtfnPaySchedule,
                            DTFN_END_DATE = DTFN_END_DATE,
                            MessageBox = MessageBox,
                        };
                        return Json(res);
                    }
                }
            }
            return Json(null);
        }
        public ActionResult DTFN_END_DATE_Validating(DateTime? FN_START_DATE, DateTime? FN_END_DATE)
        {
            if (FN_END_DATE != null)
            {
                DateTime dt1 = Convert.ToDateTime(FN_END_DATE);
                DateTime dt2 = Convert.ToDateTime(FN_START_DATE);
                if (DateTime.Compare(dt1.Date, dt2.Date) < 0 || DateTime.Compare(NewDate.Date, dt1.Date) < 0)
                {
                    MessageBox = "";
                    MessageBox = "DATE HAS TO BE B/W START DATE & EXPECTED END DATE I.E. " + NewDate.ToString("dd/MM/YYYY");
                    return Json(MessageBox);
                }
            }
            else
            {
                MessageBox = "";
                MessageBox = "DATE HAS TO BE B/W START DATE & EXPECTED END DATE I.E. " + NewDate.ToString("dd/MM/YYYY");
                return Json(MessageBox);
            }
            return Json(null);
        }
        public ActionResult DTPAY_GEN_DATE_Validating(DateTime? FN_START_DATE, DateTime? DTPAY_GEN_DATE, string PR_P_NO, string FN_C_RATIO, string Functions, string txtfnPaySchedule, string Repay, string FN_MARKUP, string PR_NEW_ANNUAL_PACK, string FN_TYPE, string FN_AMT_AVAILED)
        {
            bool returnValue = false;
            if (DTPAY_GEN_DATE != null)
            {
                DateTime dt1 = Convert.ToDateTime(DTPAY_GEN_DATE);
                DateTime dt2 = Convert.ToDateTime(FN_START_DATE);
                double val3 = 0.0;
                int last_Day = 0;
                double val4 = 0.0;
                if (DateTime.Compare(dt1.Date, dt2.Date) <= 0)
                {
                    MessageBox = "";
                    MessageBox = "PAYROLL DATE HAS TO BE ENTERED & SHOULD BE AFTER THE START DATE";
                }
                else
                {
                    DateTime dt5 = new DateTime();
                    dt5 = dt2.AddMonths(1);
                    if (!(dt1.Date >= dt2.Date && dt1.Date <= dt5.Date))
                    {
                        MessageBox = "";
                        MessageBox = "ENTER THE DATE WHICH IS NEAREST TO STARTDATE COMING AFTER IT";
                    }
                    else
                    {
                        if (dt1.Month == dt2.Month && dt1.Year == dt2.Year)
                        {
                            System.TimeSpan diffResult = dt1.Subtract(dt2);
                            val3 = Math.Round(diffResult.TotalDays);
                        }
                        else
                        {
                            last_Day = GetLastDayOfMonth(dt2);
                            val4 = last_Day - dt2.Day;
                            val3 = val4 + dt1.Day;
                        }

                        if (val3 > 30)
                        {
                            MessageBox = "";
                            MessageBox = "PAYROLL  DATE SHOULD BE THE ONE WHICH IS JUST AFTER START DATE";
                        }

                        txtVal3 = val3.ToString();

                        returnValue = PROC_1(PR_P_NO, FN_C_RATIO, Functions, txtfnPaySchedule, Repay, FN_MARKUP, PR_NEW_ANNUAL_PACK, FN_TYPE, FN_AMT_AVAILED);
                        if (returnValue == false)
                        {
                            var res1 = new
                            {
                                MessageBox = MessageBox,
                            };
                            return Json(res1);
                        }
                    }
                }
                var res = new
                {
                    txtCreditRatio = txtCreditRatio,
                    txtRatioAvail = txtRatioAvail,
                    txtAvailable = txtAvailable,
                    txtFactor = txtFactor,
                    txtCanBeAvail = txtCanBeAvail,
                    ratio = ratio,
                    Repay = Repay,
                    MessageBox = MessageBox,
                };
                return Json(res);
            }
            else
            {
                MessageBox = "";
                MessageBox = "PAYROLL DATE HAS TO BE ENTERED & SHOULD BE AFTER THE START DATE";
                var res = new
                {
                    MessageBox = MessageBox,
                };
                return Json(res);
            }
            return Json(null);
        }
        private bool PROC_1(string PR_P_NO, string FN_C_RATIO, string Functions, string txtfnPaySchedule, string Repay, string FN_MARKUP, string PR_NEW_ANNUAL_PACK, string FN_TYPE, string FN_AMT_AVAILED)
        {
            double lCREDIT_RATIO = 0;
            double lCREDIT_RATIO_MONTH;
            double lLESS_RATIO_AVAILED;
            double lAVAILABLE_RATIO;
            double lFACTOR;
            double lAMT_OF_LOAN_CAN_AVAIL;
            double lTOTAL_MONTHLY_INSTALLMENT;
            int CREDIT_RATIO_PER;
            int AUX1;
            double AUX2;
            double AUX3;
            int AUX4;
            int AUX5;
            int AUX7;
            double AUX8;
            double AUX9;
            double dis_repay;
            bool returnValue = false;

            AUX2 = 0;
            AUX7 = 0;

            /// Monthly Deduction in AUX2
            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Add("PR_P_NO", PR_P_NO);
            DataTable dtMonthlyDeduction = this.GetData("CHRIS_SP_Other_fin_Booking_MANAGER", "MonthlyDeduction", param);
            if (dtMonthlyDeduction != null && dtMonthlyDeduction.Rows.Count > 0)
            {
                AUX2 = double.Parse(dtMonthlyDeduction.Rows[0].ItemArray[0].ToString() == "" ? "0" : dtMonthlyDeduction.Rows[0].ItemArray[0].ToString());
            }
            //// New Annual Pack in AUX9 
            //AUX9 = double.Parse(txtPrNewAnnual.Text == "" ? "0" : txtPrNewAnnual.Text);
            AUX9 = double.Parse(PR_NEW_ANNUAL_PACK == null ? "0" : PR_NEW_ANNUAL_PACK);

            Decimal lCREDIT_RATIODecimal;
            Decimal lCREDIT_RATIODecimal2;


            DataTable dtfnBookCount = this.GetData("CHRIS_SP_Other_fin_Booking_MANAGER", "fnBookingProcCount", param);
            if (dtfnBookCount != null)
            {
                if (dtfnBookCount.Rows.Count > 0)
                {
                    lCREDIT_RATIO = (AUX9 / 100) * double.Parse(FN_C_RATIO == null ? "0" : PROC1_3(FN_TYPE));
                }
            }
            else
            {
                lCREDIT_RATIO = (AUX9 / 100) * double.Parse(FN_C_RATIO == null ? "0" : FN_C_RATIO);
            }

            lCREDIT_RATIO_MONTH = lCREDIT_RATIO / 12;

            lCREDIT_RATIO_MONTH = Math.Round(lCREDIT_RATIO_MONTH, 2, MidpointRounding.AwayFromZero);

            lLESS_RATIO_AVAILED = AUX2;

            lAVAILABLE_RATIO = lCREDIT_RATIO_MONTH - lLESS_RATIO_AVAILED;

            dis_repay = double.Parse(txtfnPaySchedule == null ? "0" : txtfnPaySchedule);

            Repay = dis_repay.ToString();

            if (FN_MARKUP != string.Empty && FN_MARKUP != "0")
            {
                AUX8 = double.Parse(FN_MARKUP == null ? "0" : FN_MARKUP) / 1200;
                AUX8 = AUX8 + 1;
                //this can be made some prob
                AUX3 = Convert.ToDouble(Math.Pow(AUX8, -double.Parse(txtfnPaySchedule == null ? "0" : txtfnPaySchedule)));

                lFACTOR = (AUX8 - 1) / (1 - AUX3) * 1000;
            }
            else
            {
                lFACTOR = 1000 / double.Parse(txtfnPaySchedule == null ? "0" : txtfnPaySchedule);
            }

            ///--------

            txtCreditRatio = lCREDIT_RATIO_MONTH.ToString();
            txtRatioAvail = lLESS_RATIO_AVAILED.ToString();
            txtAvailable = lAVAILABLE_RATIO.ToString();
            txtFactor = lFACTOR.ToString();
            lAMT_OF_LOAN_CAN_AVAIL = (lAVAILABLE_RATIO * 1000) / Math.Round(lFACTOR, 6);
            txtCanBeAvail = lAMT_OF_LOAN_CAN_AVAIL.ToString();
            if (Functions == "Modify")
            {
                lAMT_OF_LOAN_CAN_AVAIL = double.Parse(FN_AMT_AVAILED == null ? "0" : FN_AMT_AVAILED) + lAMT_OF_LOAN_CAN_AVAIL;
                txtCanBeAvail = lAMT_OF_LOAN_CAN_AVAIL.ToString();
            }

            if (lAMT_OF_LOAN_CAN_AVAIL != 0)
            {
                returnValue = true;
            }

            else
            {
                MessageBox = "";
                MessageBox = "THE CREDIT RATIO HAS BEEN FULLY AVAILED .PRESS [ENTER] TO CONT ...";
                returnValue = false;
            }
            return returnValue;
        }
        private String PROC1_3(string FN_TYPE)
        {
            if (FN_TYPE != null)
            {
                if (FN_TYPE.Length > 0)
                {
                    Dictionary<string, object> param = new Dictionary<string, object>();
                    param.Add("FinType", FN_TYPE.Substring(0, 1));
                    DataTable dt = this.GetData("CHRIS_SP_Other_fin_Booking_MANAGER", "HouseLoanEntryCheck", param);
                    if (dt != null)
                    {
                        if (dt.Rows.Count > 1)
                        {
                            MessageBox = "";
                            MessageBox = "More than One House Loan Entries";
                        }
                        else if (dt.Rows.Count.Equals(1))
                            ratio = dt.Rows[0][0].ToString();
                    }
                }
            }
            return ratio;
        }
        public ActionResult dtExtratime_Validating(DateTime? FN_EXTRA_TIME, DateTime? FN_START_DATE, DateTime? FN_END_DATE)
        {
            if (FN_EXTRA_TIME != null)
            {
                DateTime lStartDate = Convert.ToDateTime(FN_START_DATE);
                DateTime dt3 = Convert.ToDateTime(FN_EXTRA_TIME);
                DateTime ExtraTimeDate = new DateTime();
                ExtraTimeDate = lStartDate.AddMonths(12);
                DateTime dt1 = Convert.ToDateTime(FN_END_DATE);
                DateTime dt2 = Convert.ToDateTime(FN_START_DATE);
                if (DateTime.Compare(dt3.Date, dt2.Date) < 0 || DateTime.Compare(dt3.Date, dt1.Date) > 0 || DateTime.Compare(ExtraTimeDate.Date, dt3.Date) < 0)
                {
                    MessageBox = "";
                    MessageBox = "THIS DATE IS A GRACE PERIOD, HAS TO BE  WITHIN 1 YR OF START DATE";

                    var res = new
                    {
                        MessageBox = MessageBox,
                    };
                    return Json(res);
                }
            }
            else
            {
                return Json(null);
            }
            return Json(null);
        }
        public ActionResult txtFinType_Validated(string FN_TYPE, string Functions, string FN_FIN_NO, string FN_SUBTYPE, string PR_P_NO, string FN_MARKUP)
        {
            DateTime Date1;
            DateTime Date2;
            Double calc1 = 0.0;
            Double calc2 = 0.0;
            Double calc3 = 0.0;

            if (FN_TYPE != string.Empty)
            {
                Dictionary<string, object> paramSub = new Dictionary<string, object>();
                paramSub.Add("FN_TYPE", FN_TYPE);
                DataTable dtSubTypeCount = this.GetData("CHRIS_SP_Finance_fin_Booking_MANAGER", "FinSubTypeCount", paramSub);
                if (dtSubTypeCount != null)
                {
                    if (dtSubTypeCount.Rows.Count > 0)
                    {
                        //txtFinanceSubType.Focus();
                    }
                }
                else
                {
                    if (Functions == "Add")
                    {
                        if (FN_FIN_NO == null || FN_FIN_NO.Substring(1, 1) != FN_TYPE.Substring(1, 1))
                        {
                            FinanceNo_Gen(FN_TYPE.Substring(0, 1), FN_SUBTYPE, PR_P_NO, Functions);
                        }
                        var res = new
                        {
                            Functions = Functions,
                            GCount = GCount,
                            GYear = GYear,
                            FinNoType = FinNoType,
                            txtFinanceNo = txtFinanceNo,
                            MessageBox = MessageBox,
                        };
                        return Json(res);
                    }
                    else
                    {
                        dtStartDate = GetStartDateRecCount(PR_P_NO, FN_TYPE);
                        if (dtStartDate != null)
                        {
                            if (dtStartDate.Rows[0].ItemArray[0].ToString() == string.Empty)
                            {
                                MessageBox = "";
                                MessageBox = "NO RECORD FOUND FOR THIS TYPE";
                                var res = new
                                {
                                    Functions = Functions,
                                    MessageBox = MessageBox,
                                };
                                return Json(res);
                            }
                            else
                            {
                                if (Functions == "Modify" || Functions == "Delete")
                                {
                                    dtfnMatDate = GetfnMatDateCount(PR_P_NO, FN_TYPE);
                                    if (dtfnMatDate != null)
                                    {
                                        if (dtfnMatDate.Rows.Count > 0)
                                        {
                                            DateTime.TryParse(dtfnMatDate.Rows[0].ItemArray[0].ToString(), out Date1);
                                            DateTime.TryParse(dtStartDate.Rows[0].ItemArray[0].ToString(), out Date2);
                                            if (DateTime.Compare(Date1, Date2) == 0)
                                            {
                                                if (Functions == "Delete")
                                                {
                                                    FinanceBookingView(PR_P_NO, FN_TYPE);
                                                    var res = new
                                                    {
                                                        Functions = Functions,
                                                        FinType = _FinType,
                                                        FinNo = _FinNo,
                                                        StartDate = _StartDate,
                                                        ExpiryDate = _ExpiryDate,
                                                        InstallMentAmt = _InstallMentAmt,
                                                        CreditRatio = _CreditRatio,
                                                        Amt = _Amt,
                                                        ID = _ID,
                                                        MessageBox = MessageBox,
                                                    };
                                                    return Json(res);
                                                }
                                                else
                                                {
                                                    Dictionary<string, object> paramSood = new Dictionary<string, object>();
                                                    paramSood.Add("PR_P_NO", PR_P_NO);
                                                    paramSood.Add("FN_TYPE", FN_TYPE);
                                                    DataTable dtSoodFields = this.GetData("CHRIS_SP_Finance_fin_Booking_MANAGER", "FinTypeSOOD", paramSood);

                                                    //Exec Query
                                                    SetModifyfields(PR_P_NO, FN_TYPE, FN_FIN_NO);
                                                    // FinanceBookingView(PR_P_NO, FN_TYPE);
                                                    if (dtSoodFields != null)
                                                    {
                                                        if (dtSoodFields.Rows.Count > 0)
                                                        {
                                                            if (dtSoodFields.Rows[0].ItemArray[0].ToString() != string.Empty)
                                                            {
                                                                txtBookMarkUp = dtSoodFields.Rows[0].ItemArray[0].ToString();
                                                            }
                                                        }
                                                    }

                                                    txtWBasic = FN_MARKUP;

                                                    if (txtBookMarkUp != string.Empty)
                                                    {
                                                        calc1 = Double.Parse(txtBookMarkUp) * MARKUP_RATE_DAYS * 100;
                                                    }
                                                    if (txtWBasic != string.Empty && txtaviledamt != string.Empty)
                                                    {
                                                        calc2 = (Double.Parse(txtWBasic)) * (Double.Parse(txtaviledamt));
                                                    }
                                                    if (calc2 != 0.0)
                                                    {
                                                        calc3 = calc1 / calc2;
                                                        txtWBasic = calc3.ToString();
                                                    }
                                                    else
                                                    {
                                                        if (FN_TYPE != string.Empty)
                                                        {
                                                            MessageBox = "";
                                                            MessageBox = "wBasic=0.0 ... Sorry You cant Continue";
                                                            var ress = new
                                                            {
                                                                Functions = Functions,
                                                                MessageBox = MessageBox,
                                                            };
                                                            return Json(ress);
                                                        }
                                                    }

                                                    var res = new
                                                    {
                                                        Functions = Functions,
                                                        txtaviledamt = txtaviledamt,
                                                        txtInstallment = txtInstallment,
                                                        txtBookRatio = txtBookRatio,
                                                        txtFN_MORTG_PROP_ADD = txtFN_MORTG_PROP_ADD,
                                                        txtFN_MORTG_PROP_CITY = txtFN_MORTG_PROP_CITY,
                                                        txtFN_MORTG_PROP_VALSA = txtFN_MORTG_PROP_VALSA,
                                                        txtFN_MORTG_PROP_VALVR = txtFN_MORTG_PROP_VALVR,
                                                        txtWBasic = txtWBasic,
                                                        MessageBox = MessageBox,
                                                        FinNo = _FinNo,
                                                        ID = _ID,
                                                    };
                                                    //var CatResul = JsonConvert.SerializeObject(res);
                                                    //return Json(CatResul);
                                                    return Json(res);
                                                }
                                            }
                                            else
                                            {
                                                MessageBox = "";
                                                MessageBox = "YOU CANT ACCESS THIS RECORD BECAUSE PAYROLL HAS BEEN GENERATED";
                                                var res = new
                                                {
                                                    Functions = Functions,
                                                    MessageBox = MessageBox,
                                                };
                                                return Json(res);
                                            }
                                        }
                                    }
                                }
                                if (Functions == "View")
                                {
                                    FinanceBookingView(PR_P_NO, FN_TYPE);
                                    var res = new
                                    {
                                        MessageBox = MessageBox,
                                        Functions = Functions,
                                        FinType = _FinType,
                                        FinNo = _FinNo,
                                        StartDate = _StartDate,
                                        ExpiryDate = _ExpiryDate,
                                        InstallMentAmt = _InstallMentAmt,
                                        CreditRatio = _CreditRatio,
                                        Amt = _Amt,
                                        ID = _ID,
                                    };
                                    return Json(res);
                                }
                            }
                        }
                        else
                        {
                            MessageBox = "";
                            MessageBox = "NO RECORD FOUND FOR THIS TYPE";
                            var res = new
                            {
                                Functions = Functions,
                                MessageBox = MessageBox,
                            };
                            return Json(res);
                        }
                    }
                }
            }
            return Json(null);
        }
        public void FinanceNo_Gen(string FN_TYPE, string FN_SUBTYPE, string PR_P_NO, string Functions)
        {
            DataTable dt = GetfnFinanceBookingCounts(FN_TYPE);
            int g_Year = 0;
            int cnt = 0;
            int wcnt = 0;
            string finNo = "";

            g_Year = DateTime.Now.Year;

            DataTable dt1 = GetfnFinanceBookingCountForTotal(FN_TYPE, FN_SUBTYPE, PR_P_NO);

            if (dt1 != null)
            {
                if (dt1.Rows.Count > 0 && Functions == "Add")
                {
                    MessageBox = "";
                    MessageBox = "Value Already There For This Loan ..... Press Any Key To Continue";
                }
            }
            cnt = Convert.ToInt32(dt.Rows[0]["Count_Serial"].ToString() == "" ? "0" : dt.Rows[0]["Count_Serial"].ToString());
            wcnt = Convert.ToInt32(dt.Rows[0]["MaxFinNo"].ToString() == "" ? "0" : dt.Rows[0]["MaxFinNo"].ToString());

            if (cnt == 0)
            {
                wcnt = 1;
                g_wcnt = wcnt.ToString("0000");
                if (FN_TYPE != string.Empty)
                {
                    finNo = FN_TYPE + "-" + g_wcnt + "/" + g_Year.ToString().Substring(2);
                }
            }
            else
            {
                wcnt = wcnt + 1;
                g_wcnt = wcnt.ToString("0000");
                if (FN_TYPE != string.Empty)
                {
                    finNo = FN_TYPE + "-" + g_wcnt + "/" + g_Year.ToString().Substring(2);
                }
            }
            txtFinanceNo = finNo;
            fin_no_gen2(FN_TYPE, g_wcnt, g_Year.ToString().Substring(2));
        }
        private void fin_no_gen2(string FNType, string gCount, string year)
        {
            // year = year.Substring(2);
            int FNYEAR = int.Parse(year);
            double FNFINANCENo = double.Parse(gCount == "" ? "0" : gCount);

            GCount = FNFINANCENo.ToString();
            GYear = FNYEAR.ToString();
            FinNoType = FNType.ToString();
        }
        private DataTable GetfnFinanceBookingCountForTotal(string FN_TYPE, string FN_SUBTYPE, string PR_P_NO)
        {
            DataTable dt = null;
            Dictionary<string, object> colsNVals = new Dictionary<string, object>();

            Result rsltCode;

            CmnDataManager cmnDM = new CmnDataManager();
            colsNVals.Add("FinType", FN_TYPE);
            colsNVals.Add("FN_SUB_TYPE", FN_SUBTYPE);
            colsNVals.Add("PR_P_NO", PR_P_NO);

            rsltCode = cmnDM.GetData("CHRIS_SP_Other_fin_Booking_MANAGER", "FinBookingCount", colsNVals);

            if (rsltCode.isSuccessful)
            {
                if (rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                {
                    dt = rsltCode.dstResult.Tables[0];
                }
            }
            return dt;
        }
        private DataTable GetfnFinanceBookingCounts(string FN_TYPE)
        {
            DataTable dt = null;
            Dictionary<string, object> colsNVals = new Dictionary<string, object>();

            Result rsltCode;

            CmnDataManager cmnDM = new CmnDataManager();
            colsNVals.Add("FinType", FN_TYPE);

            rsltCode = cmnDM.GetData("CHRIS_SP_Other_fin_Booking_MANAGER", "FinBookingFinanceNo", colsNVals);

            if (rsltCode.isSuccessful)
            {
                if (rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                {
                    dt = rsltCode.dstResult.Tables[0];
                }
            }
            return dt;
        }
        private DataTable GetStartDateRecCount(string PR_P_NO, string FN_TYPE)
        {
            DataTable dt = null;
            Dictionary<string, object> colsNVals = new Dictionary<string, object>();

            Result rsltCode;

            colsNVals.Add("PR_P_NO", PR_P_NO);
            colsNVals.Add("FN_TYPE", FN_TYPE);
            CmnDataManager cmnDM = new CmnDataManager();

            rsltCode = cmnDM.GetData("CHRIS_SP_Finance_fin_Booking_MANAGER", "FinBookingStartDateRecCount", colsNVals);

            if (rsltCode.isSuccessful)
            {
                if (rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                {
                    dt = rsltCode.dstResult.Tables[0];
                }
            }
            return dt;
        }
        private DataTable GetfnMatDateCount(string PR_P_NO, string FN_TYPE)
        {
            DataTable dt = null;
            Dictionary<string, object> colsNVals = new Dictionary<string, object>();

            Result rsltCode;

            colsNVals.Add("PR_P_NO", PR_P_NO);
            colsNVals.Add("FN_TYPE", FN_TYPE);
            CmnDataManager cmnDM = new CmnDataManager();

            rsltCode = cmnDM.GetData("CHRIS_SP_Finance_fin_Booking_MANAGER", "FinBookingFnMatDate", colsNVals);

            if (rsltCode.isSuccessful)
            {
                if (rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                {
                    dt = rsltCode.dstResult.Tables[0];
                }
            }
            return dt;
        }
        private void SetModifyfields(string PR_P_NO, string FN_TYPE, string FN_FIN_NO)
        {
            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Add("PR_P_NO", PR_P_NO);
            param.Add("FN_TYPE", FN_TYPE);
            DataTable dtList = this.GetData("CHRIS_SP_Finance_fin_Booking_MANAGER", "FinBookingMODList", param);
            if (dtList != null)
            {
                if (dtList.Rows.Count > 0)
                {
                    FN_FIN_NO = dtList.Rows[0].ItemArray[2].ToString();

                    if (dtList.Rows[0].ItemArray[3].ToString() != string.Empty)
                    {
                        DTstartdate = Convert.ToDateTime(dtList.Rows[0].ItemArray[3].ToString());
                    }
                    txtaviledamt = dtList.Rows[0].ItemArray[4].ToString();
                    txtInstallment = dtList.Rows[0].ItemArray[5].ToString();
                    txtBookRatio = dtList.Rows[0].ItemArray[6].ToString();
                    txtFN_MORTG_PROP_ADD = dtList.Rows[0].ItemArray[8].ToString();
                    txtFN_MORTG_PROP_CITY = dtList.Rows[0].ItemArray[9].ToString();
                    txtFN_MORTG_PROP_VALSA = dtList.Rows[0].ItemArray[10].ToString();
                    txtFN_MORTG_PROP_VALVR = dtList.Rows[0].ItemArray[11].ToString();
                }
                FinanceBookingView(PR_P_NO, FN_TYPE);
            }
        }
        public void FinanceBookingView(string PR_P_NO, string FN_TYPE)
        {
            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Add("PR_P_NO", PR_P_NO);
            param.Add("FN_TYPE", FN_TYPE);
            DataTable dt = this.GetData("CHRIS_SP_Finance_fin_Booking_MANAGER", "FinBookingViewPage", param);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    _FinType = dt.Rows[0]["FN_TYPE"].ToString();
                    _FinNo = dt.Rows[0]["FN_FINANCE_NO"].ToString();
                    _StartDate = Convert.ToDateTime(dt.Rows[0]["FN_START_DATE"]);
                    _ExpiryDate = Convert.ToDateTime(dt.Rows[0]["FN_END_DATE"]);
                    _InstallMentAmt = Convert.ToDouble(dt.Rows[0]["FN_MONTHLY_DED"]);
                    _CreditRatio = Convert.ToDouble(dt.Rows[0]["FN_C_RATIO_PER"]);
                    _Amt = Convert.ToDouble(dt.Rows[0]["FN_AMT_AVAILED"]);
                    _ID = dt.Rows[0]["ID"].ToString();
                }
            }
        }
        public ActionResult txtFinanceSubType_Validated(string FN_SUBTYPE, string FN_TYPE, string Functions, string FN_FIN_NO, string PR_P_NO)
        {
            dtStartDate = null;
            dtfnMatDate = null;

            DateTime Date1;
            DateTime Date2;
            Double calc1 = 0.0;
            Double calc2 = 0.0;
            Double calc3 = 0.0;
            if (FN_SUBTYPE != null)
            {
                if (FN_TYPE != null)
                {
                    Dictionary<string, object> paramSub = new Dictionary<string, object>();
                    paramSub.Add("FN_TYPE", FN_TYPE);
                    DataTable dtSubTypeCount = this.GetData("CHRIS_SP_Finance_fin_Booking_MANAGER", "FinSubTypeCount", paramSub);
                    if (dtSubTypeCount != null)
                    {
                        if (dtSubTypeCount.Rows.Count > 0)
                        {
                            if (Functions == "Add")
                            {
                                if (FN_FIN_NO == null || FN_FIN_NO.Substring(1, 1) != FN_TYPE.Substring(1, 1))
                                {
                                    FinanceNo_Gen(FN_TYPE.Substring(0, 1), FN_SUBTYPE, PR_P_NO, Functions);
                                }
                                var res = new
                                {
                                    Functions = Functions,
                                    GCount = GCount,
                                    GYear = GYear,
                                    FinNoType = FinNoType,
                                    txtFinanceNo = txtFinanceNo,
                                    MessageBox = MessageBox,
                                };
                                return Json(res);
                            }
                            else
                            {
                                dtStartDate = GetStartDateSubTypeCount(FN_SUBTYPE, FN_TYPE, PR_P_NO);
                                if (dtStartDate != null)
                                {
                                    if (dtStartDate.Rows[0].ItemArray[0].ToString() == string.Empty)
                                    {
                                        MessageBox = "";
                                        MessageBox = "NO RECORD FOUND FOR THIS TYPE";
                                        var res = new
                                        {
                                            Functions = Functions,
                                            MessageBox = MessageBox,
                                        };
                                        return Json(res);
                                    }
                                    else
                                    {
                                        if (Functions == "Modify" || Functions == "Delete")
                                        {
                                            dtfnMatDate = GetfnMatDateSubTypeCount(FN_SUBTYPE, FN_TYPE, PR_P_NO);
                                            if (dtfnMatDate != null)
                                            {
                                                if (dtfnMatDate.Rows.Count > 0)
                                                {
                                                    DateTime.TryParse(dtfnMatDate.Rows[0].ItemArray[0].ToString(), out Date1);
                                                    DateTime.TryParse(dtStartDate.Rows[0].ItemArray[0].ToString(), out Date2);
                                                    if (DateTime.Compare(Date1, Date2) == 0)
                                                    {
                                                        if (Functions == "Delete")
                                                        {
                                                            FinanceBookingView(PR_P_NO, FN_TYPE);
                                                            var res = new
                                                            {
                                                                Functions = Functions,
                                                                FinType = _FinType,
                                                                FinNo = _FinNo,
                                                                StartDate = _StartDate,
                                                                ExpiryDate = _ExpiryDate,
                                                                InstallMentAmt = _InstallMentAmt,
                                                                CreditRatio = _CreditRatio,
                                                                Amt = _Amt,
                                                                ID = _ID,
                                                                MessageBox = MessageBox,
                                                            };
                                                            return Json(res);
                                                        }

                                                        else
                                                        {
                                                            Dictionary<string, object> paramSood = new Dictionary<string, object>();
                                                            paramSood.Add("PR_P_NO", PR_P_NO);
                                                            paramSood.Add("FN_TYPE", FN_TYPE);
                                                            paramSood.Add("FN_SUBTYPE", FN_SUBTYPE);
                                                            DataTable dtSoodFields = this.GetData("CHRIS_SP_Finance_fin_Booking_MANAGER", "FinSubTypeSOOD", paramSood);

                                                            //Exec Query
                                                            SetSubTypeModifyfields(FN_SUBTYPE, FN_TYPE, PR_P_NO);
                                                            if (dtSoodFields != null)
                                                            {
                                                                if (dtSoodFields.Rows.Count > 0)
                                                                {
                                                                    if (dtSoodFields.Rows[0].ItemArray[0].ToString() != string.Empty)
                                                                    {
                                                                        txtBookMarkUp = dtSoodFields.Rows[0].ItemArray[0].ToString();
                                                                    }
                                                                }
                                                            }

                                                            txtWBasic = txtMarkup;

                                                            if (txtBookMarkUp != null)
                                                            {
                                                                calc1 = Double.Parse(txtBookMarkUp) * MARKUP_RATE_DAYS * 100;
                                                            }
                                                            if (txtWBasic != string.Empty && txtaviledamt != string.Empty)
                                                            {
                                                                calc2 = (Double.Parse(txtWBasic)) * (Double.Parse(txtaviledamt));
                                                            }
                                                            if (calc2 != 0.0)
                                                            {
                                                                calc3 = calc1 / calc2;
                                                                txtWBasic = calc3.ToString();
                                                            }
                                                            else
                                                            {
                                                                if (FN_TYPE != string.Empty)
                                                                {
                                                                    MessageBox = "";
                                                                    MessageBox = "wBasic=0.0 ... Sorry You cant Continue";
                                                                    var ress = new
                                                                    {
                                                                        Functions = Functions,
                                                                        MessageBox = MessageBox,
                                                                    };
                                                                    return Json(ress);
                                                                }
                                                            }
                                                            var res = new
                                                            {
                                                                Functions = Functions,
                                                                txtaviledamt = txtaviledamt,
                                                                txtInstallment = txtInstallment,
                                                                txtBookRatio = txtBookRatio,
                                                                txtFN_MORTG_PROP_ADD = txtFN_MORTG_PROP_ADD,
                                                                txtFN_MORTG_PROP_CITY = txtFN_MORTG_PROP_CITY,
                                                                txtFN_MORTG_PROP_VALSA = txtFN_MORTG_PROP_VALSA,
                                                                txtFN_MORTG_PROP_VALVR = txtFN_MORTG_PROP_VALVR,
                                                                txtWBasic = txtWBasic,
                                                                MessageBox = MessageBox,
                                                                ID = _ID,
                                                            };
                                                            return Json(res);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        MessageBox = "";
                                                        MessageBox = "YOU CANT ACCESS THIS RECORD BECAUSE PAYROLL HAS BEEN GENERATED";
                                                        var res = new
                                                        {
                                                            Functions = Functions,
                                                            MessageBox = MessageBox,
                                                        };
                                                        return Json(res);
                                                    }
                                                }
                                            }
                                        }
                                        if (Functions == "View")
                                        {
                                            FinanceBookingView(PR_P_NO, FN_TYPE);
                                            var res = new
                                            {
                                                MessageBox = MessageBox,
                                                Functions = Functions,
                                                FinType = _FinType,
                                                FinNo = _FinNo,
                                                StartDate = _StartDate,
                                                ExpiryDate = _ExpiryDate,
                                                InstallMentAmt = _InstallMentAmt,
                                                CreditRatio = _CreditRatio,
                                                Amt = _Amt,
                                                ID = _ID,
                                            };
                                            return Json(res);
                                        }
                                    }
                                }
                                else
                                {
                                    MessageBox = "";
                                    MessageBox = "NO RECORD FOUND FOR THIS TYPE";
                                    var res = new
                                    {
                                        Functions = Functions,
                                        MessageBox = MessageBox,
                                    };
                                    return Json(res);
                                }
                            }
                        }
                    }
                    else
                    {
                        MessageBox = "";
                        MessageBox = "No Finance Sub. Type is available for this Finance Type...";
                        var res = new
                        {
                            Functions = Functions,
                            MessageBox = MessageBox,
                        };
                        return Json(res);
                    }
                }
            }
            return Json(null);
        }
        private void SetSubTypeModifyfields(string FN_SUBTYPE, string FN_TYPE, string PR_P_NO)
        {
            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Add("PR_P_NO", PR_P_NO);
            param.Add("FN_TYPE", FN_TYPE);
            param.Add("FN_SUBTYPE", FN_SUBTYPE);
            DataTable dtList = this.GetData("CHRIS_SP_Finance_fin_Booking_MANAGER", "FinBookingMODSubTypeList", param);
            if (dtList != null)
            {
                if (dtList.Rows.Count > 0)
                {
                    txtFinanceNo = dtList.Rows[0].ItemArray[2].ToString();

                    if (dtList.Rows[0].ItemArray[3].ToString() != string.Empty)
                    {
                        DTstartdate = Convert.ToDateTime(dtList.Rows[0].ItemArray[3].ToString());
                    }
                    txtaviledamt = dtList.Rows[0].ItemArray[4].ToString();
                    txtInstallment = dtList.Rows[0].ItemArray[5].ToString();
                    txtBookRatio = dtList.Rows[0].ItemArray[6].ToString();
                    txtFN_MORTG_PROP_ADD = dtList.Rows[0].ItemArray[8].ToString();
                    txtFN_MORTG_PROP_CITY = dtList.Rows[0].ItemArray[9].ToString();
                    txtFN_MORTG_PROP_VALSA = dtList.Rows[0].ItemArray[10].ToString();
                    txtFN_MORTG_PROP_VALVR = dtList.Rows[0].ItemArray[11].ToString();
                }
                FinanceBookingView(PR_P_NO, FN_TYPE);
            }
        }
        private DataTable GetStartDateSubTypeCount(string FN_SUBTYPE, string FN_TYPE, string PR_P_NO)
        {
            DataTable dt = null;
            Dictionary<string, object> colsNVals = new Dictionary<string, object>();

            Result rsltCode;

            colsNVals.Add("PR_P_NO", PR_P_NO);
            colsNVals.Add("FN_TYPE", FN_TYPE);
            colsNVals.Add("FN_SUBTYPE", FN_SUBTYPE);
            CmnDataManager cmnDM = new CmnDataManager();

            rsltCode = cmnDM.GetData("CHRIS_SP_Finance_fin_Booking_MANAGER", "FinBookingStartDateSubTypeCount", colsNVals);

            if (rsltCode.isSuccessful)
            {
                if (rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                {
                    dt = rsltCode.dstResult.Tables[0];
                }
            }
            return dt;
        }
        private DataTable GetfnMatDateSubTypeCount(string FN_SUBTYPE, string FN_TYPE, string PR_P_NO)
        {
            DataTable dt = null;
            Dictionary<string, object> colsNVals = new Dictionary<string, object>();

            Result rsltCode;

            colsNVals.Add("PR_P_NO", PR_P_NO);
            colsNVals.Add("FN_TYPE", FN_TYPE);
            colsNVals.Add("FN_SUBTYPE", FN_SUBTYPE);
            CmnDataManager cmnDM = new CmnDataManager();

            rsltCode = cmnDM.GetData("CHRIS_SP_Finance_fin_Booking_MANAGER", "FinBookingFnMatDateSubType", colsNVals);

            if (rsltCode.isSuccessful)
            {
                if (rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                {
                    dt = rsltCode.dstResult.Tables[0];
                }
            }
            return dt;
        }
        public ActionResult txtFN_MORTG_PROP_VALVR_PreviewKeyDown(string Functions)
        {
            if (Functions == "Add" || Functions == "Modify")
            {
                MessageBox = "";
                MessageBox = "Do you want to save this record [Y/N]..";
                var res = new
                {
                    MessageBox = MessageBox,
                };
                return Json(res);

                #region rep
                //if (dRes == DialogResult.Yes)
                //{

                //    if (dtExtratime.Value == null)
                //    {
                //        dtExtratime.Value = SaveDate;
                //    }


                //CallReport();                           
                //CallReport("AUPR", "OLD");                           
                //if (this.FunctionConfig.CurrentOption == Function.Add)
                //{
                //    SaveFinanceNumber();
                //}

                //CallReport("AUP0", "NEW");


                //    return;

                //}
                //else if (dRes == DialogResult.No)
                //{
                //    this.Reset();
                //    base.ClearForm(pnlFinanceBooking.Controls);
                //    txtOption.Focus();

                //    return;
                //}
                #endregion
            }
            return Json(null);
        }

        [HttpPost]
        public JsonResult Upsert_SAVE_UPDATE(Decimal? PR_P_NO, string FN_BRANCH, string FN_TYPE, Decimal? FN_PAY_SCHED, string FN_FIN_NO, DateTime? FN_START_DATE, DateTime? FN_END_DATE, DateTime? FN_EXTRA_TIME, Decimal? FN_AMT_AVAILED, Decimal? FN_MONTHLY_DED, Decimal? FN_C_RATIO_PER, string FN_LIQUIDATE, string EXCP_FLAG, string EXCP_REM, string FN_SUBTYPE, string FN_MORTG_PROP_ADD, string FN_MORTG_PROP_CITY, Decimal? FN_MORTG_PROP_VALSA, Decimal? FN_MORTG_PROP_VALVR, string FinNo_Year, string FinNo_Type, string FinNo_Count, int? ID)
        {
            try
            {
                Dictionary<string, object> d = new Dictionary<string, object>();

                d.Add("PR_P_NO", PR_P_NO);
                d.Add("FN_BRANCH", FN_BRANCH);
                d.Add("FN_TYPE", FN_TYPE);
                d.Add("FN_PAY_SCHED", FN_PAY_SCHED);
                d.Add("FN_FIN_NO", FN_FIN_NO);
                d.Add("FN_START_DATE", FN_START_DATE);
                d.Add("FN_END_DATE", FN_END_DATE);
                d.Add("FN_EXTRA_TIME", FN_EXTRA_TIME);
                d.Add("FN_AMT_AVAILED", FN_AMT_AVAILED);
                d.Add("FN_MONTHLY_DED", FN_MONTHLY_DED);
                d.Add("FN_C_RATIO_PER", FN_C_RATIO_PER);
                d.Add("FN_LIQUIDATE", FN_LIQUIDATE);
                d.Add("EXCP_FLAG", EXCP_FLAG);
                d.Add("EXCP_REM", EXCP_REM);
                d.Add("FN_SUBTYPE", FN_SUBTYPE);
                d.Add("FN_MORTG_PROP_ADD", FN_MORTG_PROP_ADD);
                d.Add("FN_MORTG_PROP_CITY", FN_MORTG_PROP_CITY);
                d.Add("FN_MORTG_PROP_VALSA", FN_MORTG_PROP_VALSA);
                d.Add("FN_MORTG_PROP_VALVR", FN_MORTG_PROP_VALVR);
                d.Add("FinNo_Year", FinNo_Year);
                d.Add("FinNo_Type", FinNo_Type);
                d.Add("FinNo_Count", FinNo_Count);
                d.Add("ID", ID);

                CmnDataManager objCmnDataManager = new CmnDataManager();

                if (ID > 0)
                {
                    Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_Finance_fin_Booking_MANAGER", "Update", d);
                    if (rsltCode.isSuccessful)
                    {
                        var res = Json(rsltCode);
                        return Json("Your request is modify successfully !");
                    }
                }
                else
                {
                    Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_Finance_fin_Booking_MANAGER", "Save", d);
                    if (rsltCode.isSuccessful)
                    {
                        var res = Json(rsltCode);
                        return Json("Your request is successfully completed !");
                    }
                }
                return Json(null);
            }
            catch (Exception ex)
            {
                return Json("Something went wrong");
            }
        }

        [HttpPost]
        public JsonResult Delete_FBEnt(int? ID)
        {
            try
            {
                Dictionary<string, object> d = new Dictionary<string, object>();

                d.Add("ID", ID);
                CmnDataManager objCmnDataManager = new CmnDataManager();

                if (ID != null)
                {
                    Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_Finance_fin_Booking_MANAGER", "Delete", d);
                    if (rsltCode.isSuccessful)
                    {
                        var res = Json(rsltCode);
                        return Json("Your request is Deleted successfully !");
                    }
                }
                else
                {
                    return Json(null);
                }
            }
            catch (Exception ex)
            {
                return Json("Something went wrong");
            }
            return Json(null);
        }

        #endregion

        #region FinanceIntsallmentEntryFor

        public IActionResult FinanceIntsallmentEntryFor()
        {
            return View();
        }

        public ActionResult btn_PR_P_NO_FN_IntsalEntryFor()
        {
            Dictionary<string, object> d = new Dictionary<string, object>();

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_fin_Booking_MANAGER", "FinPersonnel", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        public ActionResult MaxDate_Check(Decimal? PR_P_NO, string FN_TYPE, string FN_FIN_NO)
        {
            Dictionary<string, object> d = new Dictionary<string, object>();
            DateTime Max_date = DateTime.Now;
            d.Add("PR_P_NO", PR_P_NO);
            d.Add("FN_TYPE", FN_TYPE);
            d.Add("FN_FIN_NO", FN_FIN_NO);
            CmnDataManager objCmnDataManager = new CmnDataManager();

            Result rslt = objCmnDataManager.GetData("CHRIS_SP_Fn_Month_MANAGER_FinanceInstallment", "Max_datechk", d);
            if (rslt.isSuccessful && rslt.dstResult.Tables[0].Rows.Count > 0)
            {

                Max_date = DateTime.Parse(rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString());
                return Json(Max_date);

            }
            return Json(null);
        }

        public ActionResult FN_DEBIT_Check(Decimal? FN_DEBIT, Decimal? FN_CREDIT, Decimal? FN_MARKUP, Decimal? FN_BALANCE, Decimal? FN_PAY_LEFT, Decimal? dl, Decimal? pay)
        {
            # region case 1

            if (FN_DEBIT != 0)
            {
                if (FN_DEBIT > 0)
                {

                    if (pay == 0)
                    {
                        FN_PAY_LEFT = pay - 1;
                    }
                    else if (pay == -1)
                    {
                        FN_PAY_LEFT = -1;
                    }
                    else if (pay > -1)
                    {
                        FN_PAY_LEFT = pay - 1;
                    }

                    FN_BALANCE = FN_DEBIT + dl;

                    var FN_DEBIT_Check = new
                    {
                        one = FN_BALANCE,
                        two = FN_PAY_LEFT,
                        three = FN_MARKUP,
                        four = FN_CREDIT,

                    };
                    return Json(FN_DEBIT_Check);
                }
            }
            #endregion

            return Json(null);
        }
        public ActionResult FN_CREDIT_Check(Decimal? FN_DEBIT, Decimal? FN_CREDIT, Decimal? FN_MARKUP, Decimal? FN_BALANCE, Decimal? FN_PAY_LEFT, Decimal? dl, Decimal? pay)
        {
            # region case 2
            if (FN_CREDIT != 0)
            {

                if (FN_CREDIT > 0)
                {
                    if (pay == 0)
                    {
                        FN_PAY_LEFT = pay - 1;
                    }
                    else if (pay == -1)
                    {
                        FN_PAY_LEFT = -1;
                    }
                    else if (pay > -1)
                    {
                        FN_PAY_LEFT = pay - 1;
                    }

                    FN_BALANCE = dl - FN_CREDIT;

                    var FN_CREDIT_Check = new
                    {
                        one = FN_BALANCE,
                        two = FN_PAY_LEFT,
                        three = FN_MARKUP,
                        four = FN_CREDIT,

                    };
                    return Json(FN_CREDIT_Check);
                }
            }

            #endregion

            return Json(null);
        }

        public ActionResult DGVDetail_Finance(Decimal? PR_P_NO, string FN_TYPE, string FN_FIN_NO)
        {
            Dictionary<string, object> d = new Dictionary<string, object>();

            d.Add("PR_P_NO", PR_P_NO);
            d.Add("FN_TYPE", FN_TYPE);
            d.Add("FN_FIN_NO", FN_FIN_NO);
            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_Fn_Month_MANAGER_FinanceInstallment", "List", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);


            }
            return Json(null);
        }

        [HttpPost]
        public JsonResult UpsertFinanceIntsallmentEntryFor(Decimal? PR_P_NO, string FN_BRANCH, string FN_TYPE, Decimal? FN_PAY_SCHED, string FN_FIN_NO, DateTime? FN_START_DATE, DateTime? FN_END_DATE, DateTime? FN_EXTRA_TIME, Decimal? FN_AMT_AVAILED, Decimal? FN_MONTHLY_DED, Decimal? FN_C_RATIO_PER, string FN_LIQUIDATE, string EXCP_FLAG, string EXCP_REM, string FN_SUBTYPE, string FN_MORTG_PROP_ADD, string FN_MORTG_PROP_CITY, Decimal? FN_MORTG_PROP_VALSA, Decimal? FN_MORTG_PROP_VALVR, int? ID)
        {
            try
            {
                Dictionary<string, object> d = new Dictionary<string, object>();

                d.Add("PR_P_NO", PR_P_NO);
                d.Add("FN_BRANCH", FN_BRANCH);
                d.Add("FN_TYPE", FN_TYPE);
                d.Add("FN_PAY_SCHED", FN_PAY_SCHED);
                d.Add("FN_FIN_NO", FN_FIN_NO);
                d.Add("FN_START_DATE", FN_START_DATE);
                d.Add("FN_END_DATE", FN_END_DATE);
                d.Add("FN_EXTRA_TIME", FN_EXTRA_TIME);
                d.Add("FN_AMT_AVAILED", FN_AMT_AVAILED);
                d.Add("FN_MONTHLY_DED", FN_MONTHLY_DED);
                d.Add("FN_C_RATIO_PER", FN_C_RATIO_PER);
                d.Add("FN_LIQUIDATE", FN_LIQUIDATE);
                d.Add("EXCP_FLAG", EXCP_FLAG);
                d.Add("EXCP_REM", EXCP_REM);
                d.Add("FN_SUBTYPE", FN_SUBTYPE);
                d.Add("FN_MORTG_PROP_ADD", FN_MORTG_PROP_ADD);
                d.Add("FN_MORTG_PROP_CITY", FN_MORTG_PROP_CITY);
                d.Add("FN_MORTG_PROP_VALSA", FN_MORTG_PROP_VALSA);
                d.Add("FN_MORTG_PROP_VALVR", FN_MORTG_PROP_VALVR);
                d.Add("ID", ID);

                CmnDataManager objCmnDataManager = new CmnDataManager();

                if (ID > 0)
                {
                    Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_fin_Booking_MANAGER", "Update", d);
                    if (rsltCode.isSuccessful)
                    {
                        var res = Json(rsltCode);
                        return Json("Your request is modify successfully !");
                    }
                }
                else
                {
                    Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_fin_Booking_MANAGER", "Save", d);
                    if (rsltCode.isSuccessful)
                    {
                        var res = Json(rsltCode);
                        return Json("Your request is successfully completed !");
                    }
                }
                return Json(null);
            }
            catch (Exception ex)
            {
                return Json("Something went wrong");
            }
        }
        [HttpPost]
        public JsonResult UpsertDGVDetail_Finance(Decimal? PR_P_NO, string FN_M_BRANCH, string FN_TYPE, string FN_FIN_NO, DateTime? FN_MDATE, Decimal? FN_DEBIT, Decimal? FN_CREDIT, Decimal? FN_PAY_LEFT, Decimal? FN_BALANCE, string FN_LOAN_BALANCE, Decimal? FN_MARKUP, string FN_LIQ_FLAG, string FN_SUBTYPE, int? ID)
        {
            try
            {
                Dictionary<string, object> d = new Dictionary<string, object>();

                d.Add("PR_P_NO", PR_P_NO);
                d.Add("FN_M_BRANCH", FN_M_BRANCH);
                d.Add("FN_TYPE", FN_TYPE);
                d.Add("FN_FIN_NO", FN_FIN_NO);
                d.Add("FN_MDATE", FN_MDATE);
                d.Add("FN_DEBIT", FN_DEBIT);
                d.Add("FN_CREDIT", FN_CREDIT);
                d.Add("FN_PAY_LEFT", FN_PAY_LEFT);
                d.Add("FN_BALANCE", FN_BALANCE);
                d.Add("FN_LOAN_BALANCE", FN_LOAN_BALANCE);
                d.Add("FN_MARKUP", FN_MARKUP);
                d.Add("FN_LIQ_FLAG", FN_LIQ_FLAG);
                d.Add("FN_SUBTYPE", FN_SUBTYPE);
                d.Add("ID", ID);

                CmnDataManager objCmnDataManager = new CmnDataManager();

                if (ID > 0)
                {
                    Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_Fn_Month_MANAGER_FinanceInstallment", "Update", d);
                    if (rsltCode.isSuccessful)
                    {
                        var res = Json(rsltCode);
                        return Json("Your request is modify successfully !");
                    }
                }
                else
                {
                    Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_Fn_Month_MANAGER_FinanceInstallment", "Save", d);
                    if (rsltCode.isSuccessful)
                    {
                        var res = Json(rsltCode);
                        return Json("Your request is successfully completed !");
                    }
                }
                return Json(null);
            }
            catch (Exception ex)
            {
                return Json("Something went wrong");
            }
        }

        #endregion

        #region FinanceQuery

        public IActionResult FinanceQuery()
        {
            return View();
        }

        public ActionResult btn_FN_PR_P_NO()
        {
            Dictionary<string, object> d = new Dictionary<string, object>();

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_FINANCEQUERY_MANAGER", "LOV_PR_P_NO", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        public ActionResult btn_FN_TYPE()
        {
            Dictionary<string, object> d = new Dictionary<string, object>();

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_FINANCEQUERY_MANAGER", "LOV_FinType", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        public ActionResult tbl_dgvFnMoths(Decimal? PR_P_NO)
        {
            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("PR_P_NO", PR_P_NO);
            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_FNQUERY_MONTH_MANAGER", "List", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }


        #region Events


        #region Fields

        double _ratio = 0;
        int _pppp = 0;
        int _cnting = 0;
        int _w_all = 0;
        //private string _cat = "";
        string _level = "";
        string _level1 = "";
        double _W_10_C = 0;
        double _min_cap = 0;
        string MessageBox = "";
        string txtCreditRatio = "";
        string txtRatioAvail = "";
        string txtAvailable = "";
        string txtCanBeAvail = "";
        string txtFactor = "";
        string txtCreditRatioPer = "";
        string txtTotalMonthInstall = "";
        string txtMinCap = "";
        string txtLongPkg = "";

        #endregion

        private DataTable GetData(string sp, string actionType, Dictionary<string, object> param)
        {
            DataTable dt = null;
            Result rsltCode;
            CmnDataManager cmnDM = new CmnDataManager();
            rsltCode = cmnDM.GetData(sp, actionType, param);

            if (rsltCode.isSuccessful)
            {
                if (rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                {
                    dt = rsltCode.dstResult.Tables[0];
                }
            }
            return dt;
        }

        public ActionResult dgvFnMoths_RowLeave(string PR_P_NO, string FN_FIN_NO, string FN_LIQ_FLAG, string FN_C_Ratio, string PR_NEW_ANNUAL_PACK, string FN_Markup, string FN_Schedule, string PR_CATEGORY, string PR_LEVEL, string PR_BRANCH, string FN_Type, string MIN_CAP)
        {

            Dictionary<string, object> param = new Dictionary<string, object>();

            param.Add("PR_P_NO", PR_P_NO);
            param.Add("FN_FIN_NO", FN_FIN_NO);
            param.Add("FN_LIQ_FLAG", FN_LIQ_FLAG);

            Result rsltCode;
            CmnDataManager cmnDM = new CmnDataManager();
            rsltCode = cmnDM.Execute("CHRIS_SP_FN_MONTH_MANAGER", "UpdateFIN_Booking", param);

            if (rsltCode.isSuccessful)
            {
                PROC_1(PR_P_NO, FN_FIN_NO, FN_LIQ_FLAG, FN_C_Ratio, PR_NEW_ANNUAL_PACK, FN_Markup, FN_Schedule, PR_CATEGORY, PR_LEVEL, PR_BRANCH, FN_Type, MIN_CAP);
                param.Remove("FN_LIQ_FLAG");
                param.Add("FN_LIQ_FLAG", "");
                rsltCode = cmnDM.Execute("CHRIS_SP_FN_MONTH_MANAGER", "UpdateFIN_Booking", param);

                var res = new
                {
                    txtCreditRatio = txtCreditRatio,
                    txtRatioAvail = txtRatioAvail,
                    txtAvailable = txtAvailable,
                    txtCanBeAvail = txtCanBeAvail,
                    txtFactor = txtFactor,
                    txtCreditRatioPer = txtCreditRatioPer,
                    txtTotalMonthInstall = txtTotalMonthInstall,
                    txtMinCap = txtMinCap,
                    txtLongPkg = txtLongPkg,
                    MessageBox = MessageBox,
                };
                return Json(res);
            }
            return Json(null);
        }

        private void PROC_1(string PR_P_NO, string FN_FIN_NO, string FN_LIQ_FLAG, string FN_C_Ratio, string PR_NEW_ANNUAL_PACK, string FN_Markup, string FN_Schedule, string PR_CATEGORY, string PR_LEVEL, string PR_BRANCH, string FN_Type, string MIN_CAP)
        {

            _ratio = Convert.ToDouble(FN_C_Ratio);
            double min_cap1 = 0;
            double totalMonthlyInstall = 0;
            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Add("PR_P_NO", PR_P_NO);
            param.Add("PR_NEW_ANNUAL_PACK", PR_NEW_ANNUAL_PACK);
            param.Add("RATIO", FN_C_Ratio == "" ? "0" : FN_C_Ratio);
            param.Add("MARKUP", FN_Markup == "" ? "0" : FN_Markup);
            param.Add("SCHEDULE", FN_Schedule == "" ? "0" : FN_Schedule);

            param.Add("CAT", PR_CATEGORY);
            //param.Add("W_ALL", _w_all);
            param.Add("LEVEL", PR_LEVEL);
            param.Add("BRANCH", PR_BRANCH);
            param.Add("W_DATE", DateTime.Now);

            param.Add("W_10_C", _W_10_C);
            //param.Add("MIN_CAP", min_cap1);
            //param.Add("MIN_CAP", FN_Schedule == "" ? "0" : FN_Schedule);
            param.Add("LOAN_PKG", MIN_CAP == "" ? "0" : MIN_CAP);
            // param.Add("TOT_MONTHLY_INSTALL", totalMonthlyInstall);
            param.Add("TYPE", FN_Type);

            DataTable dt = this.GetData("CHRIS_SP_FINANCEQUERY_PROC_1", "", param);

            if (dt != null && dt.Rows.Count > 0)
            {
                if (dt.Rows[0]["CREDIT_RATIO"].ToString() == "SP007")
                {

                    MessageBox = "UPDATE THE ALLOWANCE TABLE FOR GOVT. ALLOWANCE";

                    return;
                }
                txtCreditRatio = dt.Rows[0]["CREDIT_RATIO"].ToString();
                txtRatioAvail = dt.Rows[0]["RATIO_AVAILED"].ToString();
                txtAvailable = dt.Rows[0]["AVAILABLE"].ToString();
                txtCanBeAvail = dt.Rows[0]["CAN_BE"].ToString();
                txtFactor = dt.Rows[0]["FACTOR"].ToString();
                txtCreditRatioPer = dt.Rows[0]["CREDIT_RATIO_PER"].ToString();
                txtTotalMonthInstall = dt.Rows[0]["TOT_MONTHLY_INSTALL"].ToString();

                _min_cap = double.Parse(dt.Rows[0]["MIN_CAP"].ToString() == "" ? "0" : dt.Rows[0]["MIN_CAP"].ToString());
                txtMinCap = dt.Rows[0]["MIN_CAP"].ToString();

                txtLongPkg = dt.Rows[0]["loan_pkg"].ToString();

                this.PROC_3();
            }
        }
        private void PROC_3()
        {
            double creditRatio = double.Parse(this.txtCreditRatio == "" ? "0" : this.txtCreditRatio);
            double ratioAvailed = double.Parse(this.txtRatioAvail == "" ? "0" : this.txtRatioAvail);
            double available = double.Parse(this.txtAvailable == "" ? "0" : this.txtAvailable);
            double canBe = double.Parse(this.txtCanBeAvail == "" ? "0" : this.txtCanBeAvail);
            double factor = double.Parse(this.txtFactor == "" ? "0" : this.txtFactor);
            double creditRatioPer = double.Parse(this.txtCreditRatioPer == "" ? "0" : this.txtCreditRatioPer);
            double monthlyInstall = double.Parse(this.txtTotalMonthInstall == "" ? "0" : this.txtTotalMonthInstall);

            if (creditRatio < 0) { txtCreditRatio = "0"; }
            if (ratioAvailed < 0) { txtRatioAvail = "0"; }
            if (available < 0) { txtAvailable = "0"; }
            if (canBe < 0) { txtCanBeAvail = "0"; }
            if (factor < 0) { txtFactor = "0"; }
            if (creditRatioPer < 0) { txtCreditRatioPer = "0"; }
            if (monthlyInstall < 0) { txtTotalMonthInstall = "0"; }

        }
        #endregion


        #endregion

        #region FinanceTypeEntry

        public IActionResult FinanceTypeEntry()
        {
            return View();
        }

        public ActionResult btn_FN_TYPE_FnTypeEnt()
        {
            Dictionary<string, object> d = new Dictionary<string, object>();

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_FIN_TYPE_MANAGER", "FinanceTypeLov", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        public ActionResult Fn_Sub_Type_Ent(string FN_TYPE)
        {
            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("FN_TYPE", FN_TYPE);
            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_FIN_SUBTYPE_MANAGER", "List", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        [HttpPost]
        public JsonResult UpsertFnTypeEnt(string FN_TYPE, string FN_DESC, Decimal? FN_ELIG, Decimal? FN_MARKUP, string FN_CONFIRM, Decimal? FN_C_RATIO, Decimal? FN_SCHEDULE, Decimal? FN_LESS_AMT, Decimal? FN_EX_LESS, Decimal? FN_EX_GREAT, int? ID)
        {
            try
            {
                Dictionary<string, object> d = new Dictionary<string, object>();

                d.Add("FN_TYPE", FN_TYPE);
                d.Add("FN_DESC", FN_DESC);
                d.Add("FN_ELIG", FN_ELIG);
                d.Add("FN_MARKUP", FN_MARKUP);
                d.Add("FN_CONFIRM", FN_CONFIRM);
                d.Add("FN_C_RATIO", FN_C_RATIO);
                d.Add("FN_SCHEDULE", FN_SCHEDULE);
                d.Add("FN_LESS_AMT", FN_LESS_AMT);
                d.Add("FN_EX_LESS", FN_EX_LESS);
                d.Add("FN_EX_GREAT", FN_EX_GREAT);
                d.Add("ID", ID);

                CmnDataManager objCmnDataManager = new CmnDataManager();

                if (ID > 0)
                {
                    Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_FIN_TYPE_MANAGER", "Update", d);
                    if (rsltCode.isSuccessful)
                    {
                        var res = Json(rsltCode);
                        return Json("Your request is modify successfully !");
                    }
                }
                else
                {
                    Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_FIN_TYPE_MANAGER", "Save", d);
                    if (rsltCode.isSuccessful)
                    {
                        var res = Json(rsltCode);
                        return Json("Your request is successfully completed !");
                    }
                }
                return Json(null);
            }
            catch (Exception ex)
            {
                return Json("Something went wrong");
            }
        }

        [HttpPost]
        public JsonResult UpsertFnSubTypeEnt(string FN_TYPE, string FN_SUBTYPE, string FN_SUBDESC, int? ID)
        {
            try
            {
                Dictionary<string, object> d = new Dictionary<string, object>();

                d.Add("FN_TYPE", FN_TYPE);
                d.Add("FN_SUBTYPE", FN_SUBTYPE);
                d.Add("FN_SUBDESC", FN_SUBDESC);
                d.Add("ID", ID);

                CmnDataManager objCmnDataManager = new CmnDataManager();

                if (ID > 0)
                {
                    Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_FIN_SUBTYPE_MANAGER", "Update", d);
                    if (rsltCode.isSuccessful)
                    {
                        var res = Json(rsltCode);
                        return Json("Your request is modify successfully !");
                    }
                }
                else
                {
                    Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_FIN_SUBTYPE_MANAGER", "Save", d);
                    if (rsltCode.isSuccessful)
                    {
                        var res = Json(rsltCode);
                        return Json("Your request is successfully completed !");
                    }
                }
                return Json(null);
            }
            catch (Exception ex)
            {
                return Json("Something went wrong");
            }
        }

        #endregion

        #region FinEnhancementEntry

        #region Fields

        Double DDebit = 0;
        Double CCredit = 0;
        Double OPayLeft = 0;
        Double BBal = 0;
        Double wBBal = 0;
        Double Sym1;
        Double Sym2;
        Double Sym3;
        DateTime lPayGenDate = new DateTime();
        int MARKUP_RATE_DAYS = -1;

        DateTime DTstartdate = new DateTime();
        DateTime DTFN_END_DATE = new DateTime();
        DateTime? dtExtratime = new DateTime?();
        DateTime DTPAY_GEN_DATE = new DateTime();
        DateTime dtEnhancementDate = new DateTime();
        string txtfnBalance = "0";
        string txtFnPAyLeft = "0";
        string W_Bal = "0";
        string txtENHAN_AMT = "0";



        double lCREDIT_RATIO;
        double lCREDIT_RATIO_MONTH;
        int lCOUNTER;
        double lLESS_RATIO_AVAILED;
        double lAVAILABLE_RATIO;
        double lFACTOR = 0;
        double lAMT_OF_LOAN_CAN_AVAIL = 0;
        double lTOTAL_MONTHLY_INSTALLMENT;
        int CREDIT_RATIO_PER;
        int AUX1;
        double AUX2;
        double AUX3;
        int AUX4;
        int AUX5;
        int AUX7;
        double AUX8;
        double AUX9;
        double dis_repay;


        //string txtCreditRatio1 = "0";
        //string txtRatioAvail1 = "0";
        //string txtAvailable1= "0";
        //string txtFactor1 = "0";
        //string txtCanBeAvail1 = "0";
        //-----------PROC 1------------
        double EnhancementAmt = 0.0;
        double lAmountofLoanCanAvail = 0.0;
        double AUX44 = 0.0;
        double AUX55 = 0.0;
        double AUX99 = 0.0;
        //double lFACTOR = 0.0;
        //double lAVAILABLE_RATIO = 0.0;
        double lLESS_RATIO_AVAILed = 0.0;
        // double lCREDIT_RATIO = 0.0;
        double lTOT_MONTH_INST = 0.0;
        double lfnBalance = 0.0;
        double lFiN_MARKUP = 0.0;
        double lMARKUP = 0.0;
        double WVal3 = 0.0;
        double lFiN_CREDIT = 0.0;
        double lTOT_INST = 0.0;
        double lMarkupRec = 0.0;
        bool returnValue = true;
        //String ratio = "0";

        string txtVal3 = "";
        string txtfnMarkup = "";
        string txtMarkup = "";
        string txtFIN_CREDIT = "";
        string txtBookRatio = "";
        string txtTOT_INST = "";
        string txtMARKUP_REC = "";


        string txtFnMonthDebit = "";
        string txtWTenCBonus = "";
        string txtFnMonthCredit = "";
        string txtFnMonthMarkup = "";


        double aux99;
        int AUXX1;
        int AUXX2;
        double w_Bal1 = 0.0;
        double FinMarkup;
        int val3;
        double finBal;
        double tot_month_inst;
        double lFactor;
        double fnCredit;
        double lamt_of_loan_can_avail;
        double lENHAN_AMT;
        double lAvailableRatio;

        double LMarkup;
        double lFinMarkup;
        double lwwval5;
        double lFinCredit;
        double lFnCRatioBooking;
        double llFinMarkup;
        double ClericalBonus;
        double spAllowanceAmt;


        #endregion
        public IActionResult FinEnhancementEntry()
        {
            return View();
        }

        public ActionResult btn_PR_P_NO_FEE()
        {
            Dictionary<string, object> d = new Dictionary<string, object>();

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_Fin_EnhancementFN_Month_MANAGER", "PersonalLov", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        public ActionResult btn_FN_TYPE_FEE(string SearchFilter)
        {
            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("SearchFilter", SearchFilter);
            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_Fin_EnhancementFN_Month_MANAGER", "FinTypeLov", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        public ActionResult txtENHAN_AMT_PreviewKeyDown(string Functions, string wval5, string pr_category, string pr_level, string fnBranch, string FinanceNo, string PR_P_NO, string FinType, string PrNewAnnual, string txtVal3, string W_Bal, string txtfnBalance, string txtENHAN_AMT, string txtfnMarkup)
        {
            string lFinType = "";
            if (txtENHAN_AMT == "0.00")
            {

            }

            if (wval5 == null)
            {
                wval5 = "0";
            }

            if (txtENHAN_AMT != string.Empty)
            {
                if (Functions != "View" && Functions != "Delete")
                {

                    if (txtENHAN_AMT != string.Empty && txtCanBeAvail != string.Empty && txtCanBeAvail != "INFINITY")
                    {
                        if (double.Parse(txtENHAN_AMT) > double.Parse(txtCanBeAvail))
                        {
                            MessageBox = "";
                            MessageBox = "VALUE HAS TO BE ENTERED & SHOULD BE <  " + txtCanBeAvail;
                        }
                    }
                }

                aux99 = double.Parse(PrNewAnnual == "" ? "0" : PrNewAnnual);
                if (Functions == "Add")
                {
                    w_Bal1 = 0.0;
                }
                else
                {
                    if (W_Bal != string.Empty)
                    {
                        w_Bal1 = double.Parse(W_Bal);
                    }
                }
                val3 = Convert.ToInt32(txtVal3 == "" ? "0" : txtVal3);
                finBal = double.Parse(txtfnBalance == "" ? "0" : txtfnBalance);
                lFactor = double.Parse(txtFactor == "" ? "0" : txtFactor);
                lENHAN_AMT = double.Parse(txtENHAN_AMT == "" ? "0" : txtENHAN_AMT);
                llFinMarkup = double.Parse(txtfnMarkup == "" ? "0" : txtfnMarkup);
                //----   * Markup Pending


                FinMarkup = ((finBal - w_Bal1) * (val3 / 100) * llFinMarkup) / MARKUP_RATE_DAYS;

                tot_month_inst = (finBal - w_Bal1) * (lFactor / 1000);

                fnCredit = tot_month_inst - FinMarkup;

                lamt_of_loan_can_avail = double.Parse((txtCanBeAvail == "INFINITY" || txtCanBeAvail == "") ? "0" : txtCanBeAvail);

                lamt_of_loan_can_avail = lamt_of_loan_can_avail - lENHAN_AMT;

                lCREDIT_RATIO = double.Parse(txtCreditRatio == "" ? "0" : txtCreditRatio);

                lAvailableRatio = lamt_of_loan_can_avail * lFactor / 1000;
                lLESS_RATIO_AVAILed = lCREDIT_RATIO - lAvailableRatio;

                finBal = (lENHAN_AMT + finBal - w_Bal1);
                LMarkup = double.Parse(txtfnMarkup == "" ? "0" : txtfnMarkup);
                lTOT_MONTH_INST = tot_month_inst + ((lENHAN_AMT * lFactor) / 1000);
                lwwval5 = double.Parse(wval5 == "" ? "0" : wval5);
                lFinMarkup = FinMarkup + (finBal * (lwwval5 / 100) * LMarkup) / MARKUP_RATE_DAYS;
                txtFnMonthDebit = lENHAN_AMT.ToString();
                AUX55 = aux99 / 12;

                if (pr_category == "C")
                {
                    //coding pending

                    ClericalBonus = Double.Parse(txtWTenCBonus == "" ? "0" : txtWTenCBonus);
                    AUX44 = (ClericalBonus / 100) + 1;
                    AUX55 = AUX55 * AUX44;

                    Dictionary<string, object> param = new Dictionary<string, object>();
                    param.Clear();
                    param.Add("category", pr_category);
                    param.Add("level", pr_level);
                    param.Add("fn_branch", fnBranch);
                    DataTable dtClericalAllowance = this.GetData("CHRIS_SP_Other_fin_Booking_MANAGER", "ClericalStaffCheck", param);
                    if (dtClericalAllowance != null)
                    {
                        if (dtClericalAllowance.Rows.Count > 0)
                        {
                            spAllowanceAmt = double.Parse(dtClericalAllowance.Rows[0].ItemArray[0].ToString() == "" ? "0" : dtClericalAllowance.Rows[0].ItemArray[0].ToString());
                            AUX55 = AUX55 + spAllowanceAmt;
                        }
                    }

                    else
                    {
                        MessageBox = "";
                        MessageBox = "UPDATE THE ALLOWANCE TABLE FOR GOVT. ALLOWANCE";

                        //CHRIS_Setup_AllowanceEnter AllowanceDialog = new CHRIS_Setup_AllowanceEnter(null, null);
                        //AllowanceDialog.ShowDialog();
                    }
                }

                lFinCredit = (lTOT_MONTH_INST - lFinMarkup);
                txtFIN_CREDIT = lFinCredit.ToString();
                txtFnMonthCredit = "0";
                txtFnMonthMarkup = "0";
                lFnCRatioBooking = (lLESS_RATIO_AVAILed / AUX55) * 100;
                txtBookRatio = lFnCRatioBooking.ToString();
                //lFinType = txtFinType.Text.Replace("\\", @"\");
                //SETValues
                txtMarkup = lFinMarkup.ToString();

                txtRatioAvail = lLESS_RATIO_AVAILed.ToString();

                txtAvailable = lAvailableRatio.ToString();

                txtCanBeAvail = lamt_of_loan_can_avail.ToString();

                txtTotalMonthInstall = lTOT_MONTH_INST.ToString();

                txtfnBalance = finBal.ToString();

                Dictionary<string, object> paramMonth = new Dictionary<string, object>();
                paramMonth.Add("FN_FINANCE_NO", FinanceNo);
                paramMonth.Add("PR_P_NO", PR_P_NO);


                paramMonth.Add("FinType", FinType.Replace("\\", @"\"));
                DataTable dtFinPayMonth = this.GetData("CHRIS_SP_Other_fin_Booking_MANAGER", "fnMonthValues", paramMonth);
                if (dtFinPayMonth != null)
                {
                    if (dtFinPayMonth.Rows.Count > 0)
                    {
                        txtTOT_INST = dtFinPayMonth.Rows[0].ItemArray[0].ToString();
                        txtMARKUP_REC = dtFinPayMonth.Rows[0].ItemArray[1].ToString();
                    }
                }
                if (Functions == "Add" || Functions == "Modify")
                {
                    MessageBox = "";
                    MessageBox = "Do you want to save this record ...";

                    var res = new
                    {
                        //txtRatioAvail = txtRatioAvail,
                        //txtAvailable = txtAvailable,
                        //txtCanBeAvail = txtCanBeAvail,                        
                        //txtTotalMonthInstall = txtTotalMonthInstall,
                        //txtfnBalance = txtfnBalance,
                        //txtFnMonthMarkup = txtFnMonthMarkup,
                        //txtENHAN_AMT = txtENHAN_AMT,
                        //txtFnMonthDebit = txtFnMonthDebit,
                        //txtFnMonthCredit = txtFnMonthCredit,
                        //txtMarkup = txtMarkup,
                        //txtFIN_CREDIT = txtFIN_CREDIT,
                        //txtBookRatio = txtBookRatio,
                        //txtTOT_INST = txtTOT_INST,
                        //txtMARKUP_REC = txtMARKUP_REC,
                        MessageBox = MessageBox,

                    };
                    return Json(res);

                    //                   , MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    //if (dRes == DialogResult.Yes)
                    //{
                    //    txtpersonal.Text = txtPersonnalNo.Text;
                    //    fnfinNo.Text = txtFinanceNo.Text;

                    //    // CallReport("AUPR", "OLD");
                    //    base.DoToolbarActions(this.Controls, "Save");


                    //    //  CallReport("AUP0", "NEW");
                    //    this.Reset();
                    //    base.ClearForm(this.pnlFinanceEnhancement.Controls);
                    //    this.Cancel();
                    //    txtOption.Focus();
                    //    //call save
                    //    return;
                    //}
                    //else if (dRes == DialogResult.No)
                    //{
                    //    this.Reset();
                    //    base.ClearForm(this.pnlFinanceEnhancement.Controls);
                    //    this.Cancel();
                    //    txtOption.Focus();

                    //    return;
                    //}
                }
            }
            return Json(null);
        }

        public ActionResult dtPayG_Validating(DateTime? FN_MDATE, DateTime? dtPayG)
        {
            string wval5 = "";
            double val3 = 0.0;
            int last_Day = 0;
            double val4 = 0.0;
            if (dtPayG != null)

            {
                DateTime dtDate1 = Convert.ToDateTime(FN_MDATE);//dat2
                DateTime dtDate2 = Convert.ToDateTime(dtPayG);
                if (DateTime.Compare(dtDate1, dtDate2) >= 0)
                {
                    MessageBox = "";
                    MessageBox = "DATE HAS TO BE GREATER THAN ENHANCEMENT DATE";
                    var res = new
                    {
                        MessageBox = MessageBox,
                        wval5 = wval5,
                    };
                    return Json(res);
                }

                else
                {
                    DateTime dt5 = new DateTime();
                    dt5 = dtDate1.AddMonths(1);// dat2

                    if (dtDate2.Month == dtDate1.Month && dtDate2.Year == dtDate1.Year)
                    {
                        System.TimeSpan diffResult = dtDate2.Subtract(dtDate1);
                        val3 = Math.Round(diffResult.TotalDays);
                    }
                    else
                    {
                        last_Day = GetLastDayOfMonth(dtDate1);
                        val4 = last_Day - dtDate1.Day;
                        val3 = val4 + dtDate2.Day;
                    }

                    if (val3 > 30)
                    {
                        MessageBox = "";
                        MessageBox = "DATE HAS TO BE WITHIN THE MONTH AFTER ENHANCEMENT DATE";
                        var res1 = new
                        {
                            MessageBox = MessageBox,
                            wval5 = wval5,
                        };
                        return Json(res1);
                    }

                    wval5 = val3.ToString();
                    var res2 = new
                    {
                        MessageBox = MessageBox,
                        wval5 = wval5,
                    };
                    return Json(res2);
                }
            }
            return Json(null);
        }

        public ActionResult dtEnhancementDate_Validating(DateTime? FN_MDATE, DateTime? DTPAY_GEN_DATE, string pr_transfer, Decimal? PR_P_NO, string FN_TYPE, string PR_NEW_ANNUAL_PACK, string FN_C_RATIO, string finMarkup, string FN_PAY_LEFT, string Factor)
        {
            string txtVal3 = "";
            double val3 = 0.0;
            int last_Day = 0;
            double val4 = 0.0;
            int transferCount = 0;
            if (pr_transfer != string.Empty)
            {
                int.TryParse(pr_transfer, out transferCount);
            }
            if (FN_MDATE != null)
            {
                DateTime dtDate1 = Convert.ToDateTime(DTPAY_GEN_DATE);//dat2
                DateTime dtDate2 = Convert.ToDateTime(FN_MDATE);
                if (DateTime.Compare(dtDate2.Date, dtDate1.Date) <= 0 && transferCount < 6)
                {
                    MessageBox = "";
                    MessageBox = "DATE HAS TO BE GREATER THAN PAYROLL DATE";
                }
                else
                {
                    DateTime dt5 = new DateTime();
                    dt5 = dtDate1.AddMonths(1);// dat2

                    if ((!(dtDate2 >= dtDate1 && dtDate2 <= dt5)) && transferCount < 6)
                    {
                        MessageBox = "";
                        MessageBox = "DATE HAS TO BE THE WITHIN THE MONTH AFTER PAYROLL DATE";
                    }
                    else
                    {
                        if (dtDate2.Month == dtDate1.Month && dtDate2.Year == dtDate1.Year)
                        {
                            System.TimeSpan diffResult = dtDate2.Subtract(dtDate1);
                            val3 = Math.Round(diffResult.TotalDays);
                        }
                        else
                        {
                            last_Day = GetLastDayOfMonth(dtDate1);
                            val4 = last_Day - dtDate1.Day;
                            val3 = val4 + dtDate2.Day;
                        }
                    }

                    if (val3 > 30 && transferCount < 6)
                    {
                        MessageBox = "";
                        MessageBox = "DATE HAS TO BE WITHIN THE MONTH AFTER PAYROLL DATE";
                    }

                    txtVal3 = val3.ToString();


                    PROC_1(PR_P_NO, FN_TYPE, PR_NEW_ANNUAL_PACK, FN_C_RATIO, finMarkup, FN_PAY_LEFT, Factor);
                }
                var res = new
                {
                    MessageBox = MessageBox,
                    txtVal3 = txtVal3,
                };
                return Json(res);
            }
            else
            {
                MessageBox = "";
                MessageBox = "DATE HAS TO BE GREATER THAN PAYROLL DATE";
                var res = new
                {
                    MessageBox = MessageBox,
                    txtVal3 = txtVal3,
                };
                return Json(res);
            }

            return Json(null);
        }

        private DataTable GetDataObject(string sp, string actionType)
        {
            DataTable dt = null;

            Result rsltCode;
            CmnDataManager cmnDM = new CmnDataManager();
            rsltCode = cmnDM.GetData(sp, actionType);

            if (rsltCode.isSuccessful)
            {
                if (rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                {
                    dt = rsltCode.dstResult.Tables[0];
                }
            }
            return dt;
        }
        public ActionResult fn_type_FEE_Check(Decimal? PR_P_NO, string FN_FIN_NO, string FN_TYPE, string pr_transfer, string PR_NEW_ANNUAL_PACK, string FN_C_RATIO, string finMarkup, string FN_PAY_LEFT, string txtVal3, string Factor, string Functions)
        {
            if (FN_TYPE != string.Empty)
            {
                DateTime StartDate = new DateTime();
                DateTime EndDate = new DateTime();
                DateTime ExtraTime = new DateTime();

                bool returnValue = true;
                Dictionary<string, object> paramDummy = new Dictionary<string, object>();
                paramDummy.Add("PR_P_NO", PR_P_NO);
                paramDummy.Add("FN_FIN_NO", FN_FIN_NO);
                paramDummy.Add("FN_TYPE", FN_TYPE);
                DataTable dtDummy = this.GetData("CHRIS_SP_Fin_EnhancementFN_Month_MANAGER", "FinFnMonthDummy", paramDummy);
                if (dtDummy != null)
                {
                    if (dtDummy.Rows.Count > 0)
                    {
                        Dictionary<string, object> param = new Dictionary<string, object>();
                        param.Add("PR_P_NO", PR_P_NO);
                        param.Add("FN_FIN_NO", FN_FIN_NO);

                        DataTable dtFinFnMonth = this.GetData("CHRIS_SP_Fin_EnhancementFN_Month_MANAGER", "FinFnMonthQuery", param);
                        if (dtFinFnMonth != null)
                        {
                            if (dtFinFnMonth.Rows.Count == 1)
                            {
                                bool aSym1 = Double.TryParse(dtFinFnMonth.Rows[0].ItemArray[0].ToString(), out Sym1);
                                bool aSym2 = Double.TryParse(dtFinFnMonth.Rows[0].ItemArray[1].ToString(), out Sym2);
                                bool aSym3 = Double.TryParse(dtFinFnMonth.Rows[0].ItemArray[2].ToString(), out Sym3);
                                if (Functions == "View" || Functions == "Modify" || Functions == "Delete")
                                {
                                    if (Sym1 != 0 || Sym2 == 0 || Sym3 <= 0)
                                    {
                                        MessageBox = "";
                                        MessageBox = " ENHANCEMENT HAS NOT BEEN DONE LATELY .YOU CANT " + Functions;
                                    }
                                }
                            }
                            if (Functions == "View" || Functions == "Modify" || Functions == "Delete" || Functions == "Add")
                            {
                                if (dtFinFnMonth.Rows.Count > 1)
                                {
                                    MessageBox = "";
                                    MessageBox = "TOO MANY ROWS .....";
                                    var res1 = new
                                    {
                                        MessageBox = MessageBox,
                                    };
                                    return Json(res1);
                                }
                            }
                        }

                        Dictionary<string, object> paramBooking = new Dictionary<string, object>();
                        paramBooking.Add("FN_FIN_NO", FN_FIN_NO);
                        DataTable dtFinFnBooking = this.GetData("CHRIS_SP_Fin_EnhancementFN_Month_MANAGER", "FinFnMonthBookingValues", paramBooking);
                        if (dtFinFnBooking != null)
                        {
                            if (dtFinFnBooking.Rows.Count > 0)
                            {
                                if (dtFinFnBooking.Rows[0].ItemArray[0].ToString() != string.Empty)
                                {
                                    if (DateTime.TryParse(dtFinFnBooking.Rows[0].ItemArray[0].ToString(), out StartDate))
                                    {
                                        DTstartdate = StartDate;
                                    }
                                }
                                if (dtFinFnBooking.Rows[0].ItemArray[1].ToString() != String.Empty)
                                {

                                    if (DateTime.TryParse(dtFinFnBooking.Rows[0].ItemArray[1].ToString(), out EndDate))
                                    {
                                        DTFN_END_DATE = EndDate;
                                    }
                                }
                                if (dtFinFnBooking.Rows[0].ItemArray[2].ToString() != string.Empty)
                                {
                                    if (DateTime.TryParse(dtFinFnBooking.Rows[0].ItemArray[2].ToString(), out ExtraTime))
                                    {
                                        dtExtratime = ExtraTime;
                                    }
                                }
                                else
                                {
                                    dtExtratime = null;
                                }
                            }

                            if (Functions == "Add")
                            {
                                //Dictionary<string, object> param2 = new Dictionary<string, object>();
                                //param2.Add("FN_FIN_NO", txtFinanceNo.Text);
                                DataTable dt2 = GetDataObject("CHRIS_SP_Fin_EnhancementFN_Month_MANAGER", "FinPayGenDate");
                                if (dt2 != null)
                                {
                                    if (dt2.Rows.Count > 0)
                                    {
                                        if (dt2.Rows[0].ItemArray[0].ToString() != string.Empty)
                                        {
                                            lPayGenDate = Convert.ToDateTime(dt2.Rows[0].ItemArray[0].ToString());
                                            DTPAY_GEN_DATE = lPayGenDate;
                                        }
                                    }
                                }

                                Dictionary<string, object> param1 = new Dictionary<string, object>();
                                param1.Add("FN_FIN_NO", FN_FIN_NO);
                                param1.Add("PR_P_NO", PR_P_NO);
                                param1.Add("FN_TYPE", FN_TYPE);
                                DataTable dt1 = GetData("CHRIS_SP_Fin_EnhancementFN_Month_MANAGER", "FinMonthValues", param1);

                                if (dt1 != null)
                                {
                                    if (dt1.Rows.Count > 0)
                                    {

                                        //lFnMdate = null;

                                        if (dt1.Rows[0]["lFN_DEBIT"].ToString() != string.Empty)
                                        {
                                            DDebit = double.Parse(dt1.Rows[0]["lFN_DEBIT"].ToString());
                                        }
                                        if (dt1.Rows[0]["lFN_CREDIT"].ToString() != string.Empty)
                                        {
                                            CCredit = double.Parse(dt1.Rows[0]["lFN_CREDIT"].ToString());
                                        }
                                        if (dt1.Rows[0]["FN_PAY_LEFT"].ToString() != string.Empty)
                                        {
                                            OPayLeft = double.Parse(dt1.Rows[0]["FN_PAY_LEFT"].ToString());
                                        }

                                        if (dt1.Rows[0]["FN_BALANCE"].ToString() != string.Empty)
                                        {
                                            BBal = double.Parse(dt1.Rows[0]["FN_BALANCE"].ToString());
                                        }
                                        txtfnBalance = BBal.ToString();
                                        txtFnPAyLeft = OPayLeft.ToString();
                                    }
                                }
                            }

                            else
                            {
                                Dictionary<string, object> param1 = new Dictionary<string, object>();
                                param1.Add("FN_FIN_NO", FN_FIN_NO);
                                param1.Add("PR_P_NO", PR_P_NO);
                                param1.Add("FN_TYPE", FN_TYPE);
                                DataTable dt1 = this.GetData("CHRIS_SP_Fin_EnhancementFN_Month_MANAGER", "FinMonthValuesDMV", param1);

                                if (dt1 != null)
                                {
                                    if (dt1.Rows.Count > 0)
                                    {

                                        //lFnMdate = null;

                                        if (dt1.Rows[0]["fn_debit"].ToString() != string.Empty)
                                        {
                                            DDebit = double.Parse(dt1.Rows[0]["fn_debit"].ToString());
                                        }
                                        if (dt1.Rows[0]["FN_BALANCE"].ToString() != string.Empty)
                                        {
                                            BBal = double.Parse(dt1.Rows[0]["FN_BALANCE"].ToString());
                                        }
                                        if (dt1.Rows[0]["FN_PAY_LEFT"].ToString() != string.Empty)
                                        {
                                            OPayLeft = double.Parse(dt1.Rows[0]["FN_PAY_LEFT"].ToString());
                                        }

                                        if (dt1.Rows[0]["fn_debit"].ToString() != string.Empty)
                                        {
                                            wBBal = double.Parse(dt1.Rows[0]["fn_debit"].ToString());
                                        }
                                        txtfnBalance = BBal.ToString();
                                        txtFnPAyLeft = OPayLeft.ToString();
                                        W_Bal = wBBal.ToString();
                                        txtENHAN_AMT = DDebit.ToString();

                                    }
                                }
                                DataTable dt2 = this.GetDataObject("CHRIS_SP_Fin_EnhancementFN_Month_MANAGER", "FinPayGenDate");
                                if (dt2 != null)
                                {
                                    if (dt2.Rows.Count > 0)
                                    {
                                        if (dt2.Rows[0].ItemArray[0].ToString() != string.Empty)
                                        {
                                            lPayGenDate = Convert.ToDateTime(dt2.Rows[0].ItemArray[0].ToString());
                                            DTPAY_GEN_DATE = lPayGenDate;
                                        }
                                    }
                                }

                                Dictionary<string, object> paramMatDate = new Dictionary<string, object>();
                                paramMatDate.Add("FN_FIN_NO", FN_FIN_NO);
                                paramMatDate.Add("PR_P_NO", PR_P_NO);
                                paramMatDate.Add("FN_TYPE", FN_TYPE);
                                DataTable dtFinPayMatDate = this.GetData("CHRIS_SP_Fin_EnhancementFN_Month_MANAGER", "FinPayMatDate", paramMatDate);
                                if (dtFinPayMatDate != null)
                                {
                                    if (dtFinPayMatDate.Rows.Count > 0)
                                    {
                                        if (dtFinPayMatDate.Rows[0].ItemArray[0].ToString() != string.Empty)
                                        {

                                            lPayGenDate = Convert.ToDateTime(dtFinPayMatDate.Rows[0].ItemArray[0].ToString());
                                        }
                                        dtEnhancementDate = lPayGenDate;
                                    }
                                }

                                EnhancementDateValidate(PR_P_NO, FN_TYPE, dtEnhancementDate, pr_transfer, DTPAY_GEN_DATE, PR_NEW_ANNUAL_PACK, FN_C_RATIO, finMarkup, FN_PAY_LEFT, txtVal3, Factor);

                                Proc_2(Functions, PR_P_NO, FN_FIN_NO, FN_TYPE);
                                if (returnValue == false)
                                {
                                    return Json(null);
                                }

                            }
                        }
                    }
                }

                var res = new
                {
                    txtCreditRatio = txtCreditRatio,
                    txtRatioAvail = txtRatioAvail,
                    txtAvailable = txtAvailable,
                    txtCanBeAvail = txtCanBeAvail,
                    txtFactor = txtFactor,
                    txtCreditRatioPer = txtCreditRatioPer,
                    txtTotalMonthInstall = txtTotalMonthInstall,
                    txtMinCap = txtMinCap,
                    txtLongPkg = txtLongPkg,
                    MessageBox = MessageBox,

                    txtfnBalance = txtfnBalance,
                    txtFnPAyLeft = txtFnPAyLeft,
                    W_Bal = W_Bal,
                    txtENHAN_AMT = txtENHAN_AMT,
                    DTPAY_GEN_DATE = DTPAY_GEN_DATE,
                    dtEnhancementDate = dtEnhancementDate,
                    DTstartdate = DTstartdate,
                    DTFN_END_DATE = DTFN_END_DATE,
                    dtExtratime = dtExtratime,
                    txtVal3 = txtVal3,
                    lFiN_MARKUP = lFiN_MARKUP,
                    lFiN_CREDIT = lFiN_CREDIT,
                    txtMarkup = txtMarkup,
                    txtFIN_CREDIT = txtFIN_CREDIT,
                    txtBookRatio = txtBookRatio,
                    txtTOT_INST = txtTOT_INST,
                    txtMARKUP_REC = txtMARKUP_REC,

                };
                return Json(res);
            }
            else
            {
                MessageBox = "";
                MessageBox = "THIS TYPE IS NOT VALID ";
                var res = new
                {
                    MessageBox = MessageBox,
                };
                return Json(res);
            }

            return Json(null);
        }
        private void EnhancementDateValidate(Decimal? PR_P_NO, string FN_TYPE, DateTime? dtEnhancementDate, string pr_transfer, DateTime? DTPAY_GEN_DATE, string PR_NEW_ANNUAL_PACK, string FN_C_RATIO, string finMarkup, string FN_PAY_LEFT, string txtVal3, string Factor)
        {
            double val3 = 0;
            int last_Day = 0;
            double val4 = 0;
            int transferCount = 0;
            if (pr_transfer != string.Empty)
            {
                int.TryParse(pr_transfer, out transferCount);
            }
            if (dtEnhancementDate != null)
            {
                DateTime dtDate1 = Convert.ToDateTime(DTPAY_GEN_DATE);//dat2
                DateTime dtDate2 = Convert.ToDateTime(dtEnhancementDate);
                if (DateTime.Compare(dtDate2.Date, dtDate1.Date) <= 0 && transferCount < 6)
                {
                    MessageBox = "";
                    MessageBox = "DATE HAS TO BE GREATER THAN PAYROLL DATE";
                    return;
                }
                else
                {
                    DateTime dt5 = new DateTime();
                    dt5 = dtDate1.AddMonths(1);// dat2

                    if ((!(dtDate2 >= dtDate1 && dtDate2 <= dt5)) && transferCount < 6)
                    {
                        MessageBox = "";
                        MessageBox = "DATE HAS TO BE THE WITHIN THE MONTH AFTER PAYROLL DATE";
                        return;
                    }
                    else
                    {
                        if (dtDate2.Month == dtDate1.Month && dtDate2.Year == dtDate1.Year)
                        {
                            System.TimeSpan diffResult = dtDate2.Subtract(dtDate1);
                            val3 = Math.Round(diffResult.TotalDays);
                        }
                        else
                        {
                            last_Day = GetLastDayOfMonth(dtDate1);
                            val4 = last_Day - dtDate1.Day;
                            val3 = val4 + dtDate2.Day;
                        }
                    }
                    if (val3 > 30 && transferCount < 6)
                    {
                        MessageBox = "";
                        MessageBox = "DATE HAS TO BE WITHIN THE MONTH AFTER PAYROLL DATE";
                        return;
                    }

                    txtVal3 = val3.ToString();
                    PROC_1(PR_P_NO, FN_TYPE, PR_NEW_ANNUAL_PACK, FN_C_RATIO, finMarkup, FN_PAY_LEFT, Factor);
                }
            }
            else
            {
                MessageBox = "";
                MessageBox = "DATE HAS TO BE GREATER THAN PAYROLL DATE";
                return;
            }
        }
        private int GetLastDayOfMonth(DateTime dtDate)
        {

            // set return value to the last day of the month 

            // for any date passed in to the method 

            // create a datetime variable set to the passed in date 

            DateTime dtTo = dtDate;

            // overshoot the date by a month 

            dtTo = dtTo.AddMonths(1);

            // remove all of the days in the next month 

            // to get bumped down to the last day of the 

            // previous month 

            dtTo = dtTo.AddDays(-(dtTo.Day));

            // return the last day of the month 

            return dtTo.Day;
        }
        public bool Proc_2(string Functions, Decimal? PR_P_NO, string FN_FIN_NO, string FN_TYPE)
        {

            if (Functions != "View" && Functions != "Delete")
            {
                if (txtENHAN_AMT == string.Empty)
                {
                    return returnValue = false;
                }
                else
                {
                    if (txtENHAN_AMT != string.Empty)
                    {
                        double.TryParse(txtENHAN_AMT, out EnhancementAmt);
                    }
                    if (txtCanBeAvail != string.Empty)
                    {
                        double.TryParse(txtCanBeAvail, out lAmountofLoanCanAvail);
                    }
                    if (EnhancementAmt > lAmountofLoanCanAvail)
                    {
                        return returnValue = false;
                    }
                }
            }

            if (Functions == "Add")
            {
                if (txtENHAN_AMT != string.Empty)
                {
                    double.TryParse(txtENHAN_AMT, out EnhancementAmt);
                }
                if (txtCanBeAvail != string.Empty)
                {
                    double.TryParse(txtCanBeAvail, out lAmountofLoanCanAvail);
                }
                lAmountofLoanCanAvail = lAmountofLoanCanAvail - EnhancementAmt;
                if (txtFactor != string.Empty)
                {
                    double.TryParse(txtFactor, out lFACTOR);
                }
                lAVAILABLE_RATIO = (lAmountofLoanCanAvail * lFACTOR) / 1000;
                if (txtCreditRatio != string.Empty)
                {
                    double.TryParse(txtCreditRatio, out lCREDIT_RATIO);
                }
                lLESS_RATIO_AVAILed = lCREDIT_RATIO - lAVAILABLE_RATIO;
                if (txtfnBalance != string.Empty)
                {
                    double.TryParse(txtfnBalance, out lfnBalance);
                }
                lTOT_MONTH_INST = (lfnBalance * lFACTOR) / 1000;
                if (txtVal3 != string.Empty)
                {
                    double.TryParse(txtVal3, out WVal3);
                }
                if (txtfnMarkup != string.Empty)
                {
                    double.TryParse(txtfnMarkup, out lMARKUP);
                }
                lFiN_MARKUP = (lfnBalance * (WVal3 / 100) * lMARKUP) / MARKUP_RATE_DAYS;
                lFiN_CREDIT = lTOT_MONTH_INST - lFiN_MARKUP;
                txtMarkup = "0";
                txtFIN_CREDIT = "0";
            }

            Dictionary<string, object> paramsFin = new Dictionary<string, object>();
            paramsFin.Add("FN_FIN_NO", FN_FIN_NO);
            paramsFin.Add("PR_P_NO", PR_P_NO);

            DataTable dtFinBook = this.GetData("CHRIS_SP_Fin_EnhancementFN_Month_MANAGER", "FinProc2", paramsFin);
            if (dtFinBook != null)
            {
                if (dtFinBook.Rows.Count > 0)
                {
                    if (dtFinBook.Rows[0].ItemArray[0].ToString() != string.Empty)
                    {
                        txtBookRatio = dtFinBook.Rows[0].ItemArray[0].ToString();
                    }
                }
            }

            if (Functions == "View" || Functions == "Delete")
            {
                DataTable dtFinMonthlyDed = this.GetData("CHRIS_SP_Fin_EnhancementFN_Month_MANAGER", "FinBookMonthlyDed", paramsFin);
                if (dtFinMonthlyDed != null)
                {
                    if (dtFinMonthlyDed.Rows.Count > 0)
                    {
                        if (dtFinMonthlyDed.Rows[0].ItemArray[0].ToString() != string.Empty)
                        {
                            if (double.TryParse(dtFinMonthlyDed.Rows[0].ItemArray[0].ToString(), out lTOT_MONTH_INST))
                            {
                                txtTotalMonthInstall = lTOT_MONTH_INST.ToString();
                            }
                        }
                    }
                }
            }

            Dictionary<string, object> Installparams = new Dictionary<string, object>();
            Installparams.Add("FN_FIN_NO", FN_FIN_NO);
            Installparams.Add("PR_P_NO", PR_P_NO);
            Installparams.Add("FN_TYPE", FN_TYPE);

            DataTable dtFinInstll = this.GetData("CHRIS_SP_Fin_EnhancementFN_Month_MANAGER", "FinMonthlyInstall", Installparams);
            if (dtFinInstll != null)
            {
                if (dtFinInstll.Rows.Count > 0)
                {
                    if (dtFinInstll.Rows[0].ItemArray[0].ToString() != string.Empty)
                    {
                        if (double.TryParse(dtFinInstll.Rows[0].ItemArray[0].ToString(), out lTOT_INST))
                        {
                            txtTOT_INST = lTOT_INST.ToString();
                        }
                    }

                    if (dtFinInstll.Rows[0].ItemArray[1].ToString() != string.Empty)
                    {
                        if (double.TryParse(dtFinInstll.Rows[0].ItemArray[1].ToString(), out lMarkupRec))
                        {
                            txtMARKUP_REC = lMarkupRec.ToString();
                        }
                    }
                }
            }
            //if (Function == "Modify")
            //{
            //    dtEnhancementDate();
            //}
            if (Functions == "Delete")
            {
                Dictionary<string, object> paramDelete = new Dictionary<string, object>();

                paramDelete.Add("FN_FIN_NO", FN_FIN_NO);
                paramDelete.Add("PR_P_NO", PR_P_NO);

                Result rsltCode;
                CmnDataManager cmnDM = new CmnDataManager();
                rsltCode = cmnDM.Execute("CHRIS_SP_Fin_EnhancementFN_Month_DELETE_ALL", "DELETE_ALL", paramDelete);
                return returnValue;
            }
            return returnValue;
        }
        private void PROC_1(Decimal? PR_P_NO, string FN_TYPE, string PR_NEW_ANNUAL_PACK, string FN_C_RATIO, string finMarkup, string FN_PAY_LEFT, string Factor)
        {
            AUX2 = 0;
            AUX7 = 0;

            /// Monthly Deduction in AUX2
            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Add("PR_P_NO", PR_P_NO);
            CmnDataManager objCmnDataManager = new CmnDataManager();

            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_Other_fin_Booking_MANAGER", "MonthlyDeduction", param);
            DataTable dtMonthlyDeduction = rsltCode.dstResult.Tables[0];
            if (dtMonthlyDeduction != null && dtMonthlyDeduction.Rows.Count > 0)
            {
                AUX2 = double.Parse(dtMonthlyDeduction.Rows[0].ItemArray[0].ToString() == "" ? "0" : dtMonthlyDeduction.Rows[0].ItemArray[0].ToString());
            }

            AUX9 = double.Parse(PR_NEW_ANNUAL_PACK == "" ? "0" : PR_NEW_ANNUAL_PACK);

            Result rsltCode1 = objCmnDataManager.GetData("CHRIS_SP_Other_fin_Booking_MANAGER", "fnBookingProcCount", param);
            DataTable dtfnBookCount = rsltCode1.dstResult.Tables[0];

            if (dtfnBookCount != null)
            {
                if (dtfnBookCount.Rows.Count > 1)
                {
                    PROC_3(FN_TYPE);
                }
            }

            lCREDIT_RATIO = (AUX9 / 100) * double.Parse(FN_C_RATIO == "" ? "0" : FN_C_RATIO);
            lCREDIT_RATIO_MONTH = lCREDIT_RATIO / 12;

            lCREDIT_RATIO_MONTH = Math.Round(lCREDIT_RATIO_MONTH, 2, MidpointRounding.AwayFromZero);
            lLESS_RATIO_AVAILED = AUX2;

            lAVAILABLE_RATIO = lCREDIT_RATIO_MONTH - lLESS_RATIO_AVAILED;

            if (finMarkup != string.Empty && finMarkup != "0")
            {
                AUX8 = double.Parse(finMarkup == "" ? "0" : finMarkup) / 1200;
                AUX8 = AUX8 + 1;
                AUX3 = Convert.ToDouble(Math.Pow(AUX8, -double.Parse(FN_PAY_LEFT == "" ? "0" : FN_PAY_LEFT)));

                lFACTOR = (AUX8 - 1) / (1 - AUX3) * 1000;
            }
            else
            {
                if (Factor != "0")
                {
                    if (FN_PAY_LEFT != string.Empty)
                    {
                        lFACTOR = 1000 / double.Parse(FN_PAY_LEFT == "" ? "0" : FN_PAY_LEFT);
                    }
                }
            }

            Factor = lFACTOR.ToString();
            txtCreditRatio = lCREDIT_RATIO_MONTH.ToString();
            txtRatioAvail = lLESS_RATIO_AVAILED.ToString();
            txtAvailable = lAVAILABLE_RATIO.ToString();
            txtFactor = lFACTOR.ToString();
            if (Factor != "0")
            {
                lAMT_OF_LOAN_CAN_AVAIL = (lAVAILABLE_RATIO * 1000) / Math.Round(lFACTOR, 6);
            }
            txtCanBeAvail = lAMT_OF_LOAN_CAN_AVAIL.ToString();
            if (lAMT_OF_LOAN_CAN_AVAIL != 0)
            {

            }
            else
            {
                MessageBox = "";
                MessageBox = "AVAILABLE RATIO = 0 THEREFORE ENHENCEMENT NOT POSSIBLE";

                return;
            }
        }
        private void PROC_3(string FN_TYPE)
        {
            if (FN_TYPE != string.Empty)
            {
                if (FN_TYPE.Length > 0)
                {
                    Dictionary<string, object> param = new Dictionary<string, object>();
                    param.Add("FinType", FN_TYPE.Substring(0, 1));
                    CmnDataManager objCmnDataManager = new CmnDataManager();
                    Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_Other_fin_Booking_MANAGER", "HouseLoanEntryCheck", param);
                    DataTable dt = rsltCode.dstResult.Tables[0];

                    if (dt != null)
                    {
                        if (dt.Rows.Count > 1)
                        {
                            MessageBox = "";
                            MessageBox = "More than One House Loan Entries";
                            return;
                        }
                    }
                }
            }
        }

        [HttpPost]
        public JsonResult FinEnhancementEntry_Save(Decimal? PR_P_NO, string FN_M_BRANCH, string FN_TYPE, string FN_FIN_NO, DateTime? FN_MDATE, Decimal? FN_DEBIT, Decimal? FN_CREDIT, Decimal? FN_PAY_LEFT, Decimal? FN_BALANCE, Decimal? FN_LOAN_BALANCE, Decimal? FN_MARKUP, string FN_LIQ_FLAG, string FN_SUBTYPE, Decimal? FN_C_RATIO_PER, Decimal? TOT_MONTHLY_INSTALL, int? ID)
        {
            try
            {
                Dictionary<string, object> d = new Dictionary<string, object>();

                d.Add("PR_P_NO", PR_P_NO);
                d.Add("FN_M_BRANCH", FN_M_BRANCH);
                d.Add("FN_TYPE", FN_TYPE);
                d.Add("FN_FIN_NO", FN_FIN_NO);
                d.Add("FN_MDATE", FN_MDATE);
                d.Add("FN_DEBIT", FN_DEBIT);
                d.Add("FN_CREDIT", FN_CREDIT);
                d.Add("FN_PAY_LEFT", FN_PAY_LEFT);
                d.Add("FN_BALANCE", FN_BALANCE);
                d.Add("FN_LOAN_BALANCE", FN_LOAN_BALANCE);
                d.Add("FN_MARKUP", FN_MARKUP);
                d.Add("FN_LIQ_FLAG", FN_LIQ_FLAG);
                d.Add("FN_SUBTYPE", FN_SUBTYPE);
                d.Add("FN_C_RATIO_PER", FN_C_RATIO_PER);
                d.Add("TOT_MONTHLY_INSTALL", TOT_MONTHLY_INSTALL);
                d.Add("ID", ID);

                CmnDataManager objCmnDataManager = new CmnDataManager();

                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_Fin_EnhancementFN_Month_MANAGER", "Save", d);
                if (rsltCode.isSuccessful)
                {
                    //var res = Json(rsltCode);
                    return Json("Your request is Save successfully !");
                }

                return Json(null);
            }
            catch (Exception ex)
            {
                return Json("Something went wrong");
            }
        }

        #endregion

        #region FinLiquidationEntry

        public IActionResult FinLiquidationEntry()
        {
            return View();
        }

        public ActionResult btn_PR_P_NO_FLE()
        {
            Dictionary<string, object> d = new Dictionary<string, object>();

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_Fin_Liquidation_FN_MONTH_MANAGER", "FinPersonalLov", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        public ActionResult btn_FN_TYPE_FLE(string SearchFilter)
        {
            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("SearchFilter", SearchFilter);
            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_Fin_Liquidation_FN_MONTH_MANAGER", "FinTypeLov", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        public ActionResult TBL_dtGrdDept_FLE(Decimal? PR_P_NO, DateTime? pr_transfer_date)
        {
            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("pr_transfer_date", pr_transfer_date);
            d.Add("PR_P_NO", PR_P_NO);
            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_Fin_Liquidate_DEPT_CONT_MANAGER", "List", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }
        public ActionResult fn_type_FLE_Check(Decimal? PR_P_NO, string FN_TYPE, string FN_FIN_NO, string InstRec, Double? InstLeft, string txtfndebit, string txtBal, string txtfnliqFlag, double FN_CREDIT)
        {

            string MessageBox = "";
            Double Sym1;
            Double Sym2;
            Double Sym3;
            DateTime lPayGenDate;
            Double lFnCredt;
            Double lfnMonthMarkup;
            Double lMarkup;
            Double lFnInstRec;
            Double lInstLeft = 0;
            Double lfnMarkupRecv;
            DateTime DTPAY_GEN_DATE = DateTime.Now;


            if (FN_TYPE != string.Empty)
            {
                Dictionary<string, object> param = new Dictionary<string, object>();
                param.Add("PR_P_NO", PR_P_NO);
                param.Add("FN_FIN_NO", FN_FIN_NO);
                CmnDataManager objCmnDataManager = new CmnDataManager();

                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_Fin_Liquidation_FN_MONTH_MANAGER", "FinFnMonthQuery", param);
                DataTable dt = rsltCode.dstResult.Tables[0];
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        bool aSym1 = Double.TryParse(dt.Rows[0].ItemArray[0].ToString(), out Sym1);
                        bool aSym2 = Double.TryParse(dt.Rows[0].ItemArray[1].ToString(), out Sym2);
                        bool aSym3 = Double.TryParse(dt.Rows[0].ItemArray[2].ToString(), out Sym3);

                        if (Sym1 == 0 || Sym2 != 0 || Sym3 != 0)
                        {
                            MessageBox = "ADJUSTMENT HAS NOT BEEN DONE LATELY";
                        }


                        if (Sym1 == 0 || Sym2 == 0 || Sym3 != 0)
                        {
                            MessageBox = "WARNING:PAYROLL HAS NOT BEEN GENERATED AFTER LAST ACTION ON THIS LOAN";
                        }
                    }
                }
                if (MessageBox == "")
                {
                    Dictionary<string, object> param2 = new Dictionary<string, object>();
                    param2.Add("FN_FIN_NO", FN_FIN_NO);
                    Result rsltCode1 = objCmnDataManager.GetData("CHRIS_SP_Fin_Liquidation_FN_MONTH_MANAGER", "FinPayGenDate", param2);
                    DataTable dt2 = rsltCode1.dstResult.Tables[0];

                    if (dt2 != null)
                    {
                        if (dt2.Rows.Count > 0)
                        {
                            if (dt2.Rows[0].ItemArray[0].ToString() != string.Empty)
                            {
                                if (DateTime.TryParse(dt2.Rows[0].ItemArray[0].ToString(), out lPayGenDate))
                                {
                                    DTPAY_GEN_DATE = lPayGenDate;
                                }
                            }

                        }

                    }

                    Dictionary<string, object> param1 = new Dictionary<string, object>();
                    param1.Add("FN_FIN_NO", FN_FIN_NO);
                    param1.Add("PR_P_NO", PR_P_NO);
                    param1.Add("FN_TYPE", FN_TYPE);
                    Result rsltCode2 = objCmnDataManager.GetData("CHRIS_SP_Fin_Liquidation_FN_MONTH_MANAGER", "FinMonthValues", param1);
                    DataTable dt1 = rsltCode2.dstResult.Tables[0];

                    Result rsltCode3 = objCmnDataManager.GetData("CHRIS_SP_Fin_Liquidation_FN_MONTH_MANAGER", "TotalInstallments", param1);
                    DataTable dtInstallments = rsltCode3.dstResult.Tables[0];

                    if (dt1 != null)
                    {
                        Double Installments = 0;
                        if (dtInstallments != null)
                        {
                            if (dtInstallments.Rows.Count > 0)
                            {
                                double.TryParse(dtInstallments.Rows[0]["FN_PAY_SCHED"].ToString(), out Installments);
                            }
                        }


                        if (dt1.Rows.Count > 0)
                        {
                            //lFnMdate = null;

                            txtfndebit = "0";
                            if (dt1.Rows[0]["lFN_CREDIT"].ToString() != string.Empty)
                            {
                                FN_CREDIT = Convert.ToDouble(double.Parse(dt1.Rows[0]["lFN_CREDIT"].ToString()).ToString());
                            }
                            txtBal = "0";
                            if (dt1.Rows[0]["lINST_LEFT"].ToString() != string.Empty)
                            {
                                lInstLeft = Convert.ToDouble(double.Parse(dt1.Rows[0]["lINST_LEFT"].ToString() == "" ? "0" : dt1.Rows[0]["lINST_LEFT"].ToString()));

                            }
                            InstLeft = lInstLeft;
                            if (InstRec != string.Empty)
                            {
                                lFnInstRec = double.Parse(InstRec == "" ? "0" : InstRec);

                                lFnInstRec = lFnInstRec - lInstLeft;
                                // lFnInstRec = Installments - lInstLeft;
                            }
                            lFnInstRec = Installments - lInstLeft;

                            InstRec = lFnInstRec.ToString();
                        }
                    }

                    txtfnliqFlag = "L";
                }


                var res = new
                {
                    MessageBox = MessageBox,
                    DTPAY_GEN_DATE = DTPAY_GEN_DATE,
                    txtfndebit = txtfndebit,
                    FN_CREDIT = FN_CREDIT,
                    txtBal = txtBal,
                    InstLeft = InstLeft,
                    InstRec = InstRec,
                    txtfnliqFlag = txtfnliqFlag,
                };
                return Json(res);

            }
            return Json(null);
        }
        public ActionResult FN_MDATE_FLE_Check(DateTime? Start_Date, DateTime? End_Date, DateTime? ExtraTime, DateTime? FN_MDATE, Decimal? FN_CREDIT, Decimal? finMarkup, DateTime? DTPAY_GEN_DATE, Decimal? PR_P_NO, string FN_TYPE, string FN_FIN_NO)
        {

            DateTime? lPayGenDate;
            Double lFnCredt;
            Double lfnMonthMarkup;
            Double lMarkup;
            Double lFnInstRec;
            Double lInstLeft;
            Double lfnMarkupRecv;
            Double txtMarkupLeft;
            string txtfnMarkup = "0";
            string txtfnCredit = "0";
            int MARKUP_RATE_DAYS = -1;
            double val3 = 0;
            if (FN_MDATE != null)
            {
                DateTime Date1 = Convert.ToDateTime(FN_MDATE);
                DateTime Date2 = Convert.ToDateTime(DTPAY_GEN_DATE);

                if (DateTime.Compare(Date1, Date2) <= 0)
                {
                    return Json("DATE HAS TO BE THE DATE AFTER PAYROLL .....");

                }
                else
                {
                    System.TimeSpan diffResult = Date1.Subtract(Date2);
                    val3 = Math.Round(diffResult.TotalDays);
                    val3 = val3 + 1;
                    lMarkup = double.Parse(txtfnMarkup == "" ? "0" : txtfnMarkup);
                    lFnCredt = double.Parse(txtfnCredit == "" ? "0" : txtfnCredit);
                    lfnMonthMarkup = (lFnCredt * (val3 / 100) * lMarkup) / MARKUP_RATE_DAYS;
                    lfnMonthMarkup = Math.Round(lfnMonthMarkup, 8);
                    txtMarkupLeft = lfnMonthMarkup;

                    Dictionary<string, object> param5 = new Dictionary<string, object>();
                    param5.Add("FN_FIN_NO", FN_FIN_NO);
                    param5.Add("PR_P_NO", PR_P_NO);
                    param5.Add("FN_TYPE", FN_TYPE);
                    CmnDataManager objCmnDataManager = new CmnDataManager();

                    Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_Fin_Liquidation_FN_MONTH_MANAGER", "FinMarkUpReceived", param5);
                    if (rsltCode.isSuccessful)
                    {

                        DataTable firstTable = rsltCode.dstResult.Tables[0];
                        if (firstTable.Rows.Count > 0)
                        {
                            lfnMarkupRecv = double.Parse(firstTable.Rows[0].ItemArray[0].ToString() == "" ? "0" : firstTable.Rows[0].ItemArray[0].ToString());
                            var txtMarkUpRec = lfnMarkupRecv.ToString();

                            var res = new
                            {
                                txtMarkUpRec = txtMarkUpRec,
                                txtMarkupLeft = txtMarkupLeft,
                            };

                            return Json(res);
                        }
                    }
                }
            }

            else
            {
                return Json("DATE HAS TO BE THE DATE AFTER PAYROLL .....");
            }
            return Json(null);
        }

        [HttpPost]
        public JsonResult UpsertFLE(Decimal? PR_P_NO, string FN_M_BRANCH, string FN_TYPE, string FN_FIN_NO, DateTime? FN_MDATE, Decimal? FN_DEBIT, Decimal? FN_CREDIT, Decimal? FN_PAY_LEFT, Decimal? FN_BALANCE, Decimal? FN_LOAN_BALANCE, Decimal? FN_MARKUP, string FN_LIQ_FLAG, string FN_SUBTYPE, int? ID)
        {
            try
            {
                Dictionary<string, object> d = new Dictionary<string, object>();

                d.Add("PR_P_NO", PR_P_NO);
                d.Add("FN_M_BRANCH", FN_M_BRANCH);
                d.Add("FN_TYPE", FN_TYPE);
                d.Add("FN_FIN_NO", FN_FIN_NO);
                d.Add("FN_MDATE", FN_MDATE);
                d.Add("FN_DEBIT", FN_DEBIT);
                d.Add("FN_CREDIT", FN_CREDIT);
                d.Add("FN_PAY_LEFT", FN_PAY_LEFT);
                d.Add("FN_BALANCE", FN_BALANCE);
                d.Add("FN_LOAN_BALANCE", FN_LOAN_BALANCE);
                d.Add("FN_MARKUP", FN_MARKUP);
                d.Add("FN_LIQ_FLAG", FN_LIQ_FLAG);
                d.Add("FN_SUBTYPE", FN_SUBTYPE);
                d.Add("ID", ID);

                CmnDataManager objCmnDataManager = new CmnDataManager();

                if (ID > 0)
                {
                    Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_FIN_INSURANCE_MANAGER", "Update", d);
                    if (rsltCode.isSuccessful)
                    {
                        var res = Json(rsltCode);
                        return Json("Your request is modify successfully !");
                    }
                }
                else
                {
                    Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_FIN_INSURANCE_MANAGER", "Save", d);
                    if (rsltCode.isSuccessful)
                    {
                        var res = Json(rsltCode);
                        return Json("Your request is successfully completed !");
                    }
                }
                return Json(null);
            }
            catch (Exception ex)
            {
                return Json("Something went wrong");
            }
        }

        [HttpPost]
        public JsonResult UpsertTBL_dtGrdDept(Decimal? PR_P_NO, string PR_SEGMENT, string PR_DEPT, Decimal? PR_CONTRIB, string PR_MENU_OPTION, string PR_TYPE, DateTime? PR_EFFECTIVE_DATE, int? ID)
        {
            try
            {
                Dictionary<string, object> d = new Dictionary<string, object>();

                d.Add("PR_P_NO", PR_P_NO);
                d.Add("PR_SEGMENT", PR_SEGMENT);
                d.Add("PR_DEPT", PR_DEPT);
                d.Add("PR_CONTRIB", PR_CONTRIB);
                d.Add("PR_MENU_OPTION", PR_MENU_OPTION);
                d.Add("PR_TYPE", PR_TYPE);
                d.Add("PR_EFFECTIVE_DATE", PR_EFFECTIVE_DATE);
                d.Add("ID", ID);

                CmnDataManager objCmnDataManager = new CmnDataManager();

                if (ID > 0)
                {
                    Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_Fin_Liquidate_DEPT_CONT_MANAGER", "Update", d);
                    if (rsltCode.isSuccessful)
                    {
                        var res = Json(rsltCode);
                        return Json("Your request is modify successfully !");
                    }
                }
                else
                {
                    Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_Fin_Liquidate_DEPT_CONT_MANAGER", "Save", d);
                    if (rsltCode.isSuccessful)
                    {
                        var res = Json(rsltCode);
                        return Json("Your request is successfully completed !");
                    }
                }
                return Json(null);
            }
            catch (Exception ex)
            {
                return Json("Something went wrong");
            }
        }


        #endregion

        #region FinReductionEntry
        string wval5 = "";
        public IActionResult FinReductionEntry()
        {
            return View();
        }
        public ActionResult PR_P_NO_FRE_btn()
        {
            Dictionary<string, object> d = new Dictionary<string, object>();

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_FinReduction_FN_MONTH_MANAGER", "PersonalLov", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }
        public ActionResult FN_TYPE_FRE_btn(string SearchFilter)
        {
            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("SearchFilter", SearchFilter);
            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_FinReduction_FN_MONTH_MANAGER", "FinTypeLov", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }
        public ActionResult txtFinType_Validated_FRE(string PR_P_NO, string FN_FIN_NO, string FN_TYPE, string pr_transfer, string Functions, string PR_NEW_ANNUAL_PACK, string FN_C_RATIO, string finMarkup, string FN_PAY_LEFT, string FN_CREDIT, string CAN_BE, string Factor, string CreditRatio, string FN_BALANCE, string txtVal3)
        {
            if (FN_TYPE != string.Empty)
            {
                DateTime StartDate = new DateTime();
                DateTime EndDate = new DateTime();
                DateTime ExtraTime = new DateTime();
                DateTime PayGenDate = new DateTime();
                DateTime EnhancementDate = new DateTime();
                bool returnValue = true;
                Dictionary<string, object> paramDummy = new Dictionary<string, object>();
                paramDummy.Add("PR_P_NO", PR_P_NO);
                paramDummy.Add("FN_FIN_NO", FN_FIN_NO);
                paramDummy.Add("FN_TYPE", FN_TYPE);
                DataTable dtDummy = this.GetData("CHRIS_SP_FinReduction_FN_MONTH_MANAGER", "FinFnMonthDummy", paramDummy);
                if (dtDummy != null)
                {
                    if (dtDummy.Rows.Count > 0)
                    {
                        Dictionary<string, object> param = new Dictionary<string, object>();
                        param.Add("PR_P_NO", PR_P_NO);
                        param.Add("FN_FIN_NO", FN_FIN_NO);

                        DataTable dtFinFnMonth = this.GetData("CHRIS_SP_FinReduction_FN_MONTH_MANAGER", "FinFnMonthQuery", param);
                        if (dtFinFnMonth != null)
                        {
                            if (dtFinFnMonth.Rows.Count > 1)
                            {
                                MessageBox = "";
                                MessageBox = "TOO MANY ROWS .....";
                                var res = new
                                {
                                    MessageBox = MessageBox,
                                };
                                return Json(res);
                            }
                        }

                        Dictionary<string, object> paramBooking = new Dictionary<string, object>();
                        paramBooking.Add("FN_FIN_NO", FN_FIN_NO);
                        DataTable dtFinFnBooking = this.GetData("CHRIS_SP_FinReduction_FN_MONTH_MANAGER", "FinFnMonthBookingValues", paramBooking);
                        if (dtFinFnBooking != null)
                        {
                            if (dtFinFnBooking.Rows.Count > 0)
                            {
                                if (dtFinFnBooking.Rows[0].ItemArray[0].ToString() != string.Empty)
                                {
                                    if (DateTime.TryParse(dtFinFnBooking.Rows[0].ItemArray[0].ToString(), out StartDate))
                                    {
                                        DTstartdate = StartDate;
                                    }
                                }
                                if (dtFinFnBooking.Rows[0].ItemArray[1].ToString() != String.Empty)
                                {
                                    if (DateTime.TryParse(dtFinFnBooking.Rows[0].ItemArray[1].ToString(), out EndDate))
                                    {
                                        DTFN_END_DATE = EndDate;
                                    }
                                }
                                if (dtFinFnBooking.Rows[0].ItemArray[2].ToString() != string.Empty)
                                {
                                    if (DateTime.TryParse(dtFinFnBooking.Rows[0].ItemArray[2].ToString(), out ExtraTime))
                                    {
                                        dtExtratime = ExtraTime;
                                    }
                                }
                                else
                                {
                                    dtExtratime = null;
                                }
                            }
                        }// need to change as enhancement

                        if (Functions == "Add")
                        {
                            Dictionary<string, object> param2 = new Dictionary<string, object>();
                            param2.Add("PR_P_NO", PR_P_NO);
                            param2.Add("FN_FIN_NO", FN_FIN_NO);
                            DataTable dt2 = this.GetData("CHRIS_SP_FinReduction_FN_MONTH_MANAGER", "FinPayGenDate", param2);
                            if (dt2 != null)
                            {
                                if (dt2.Rows.Count > 0)
                                {
                                    if (dt2.Rows[0].ItemArray[0].ToString() != string.Empty)
                                    {
                                        lPayGenDate = Convert.ToDateTime(dt2.Rows[0].ItemArray[0].ToString());
                                        DTPAY_GEN_DATE = lPayGenDate;
                                    }
                                }
                            }

                            Dictionary<string, object> param1 = new Dictionary<string, object>();
                            param1.Add("FN_FIN_NO", FN_FIN_NO);
                            param1.Add("PR_P_NO", PR_P_NO);
                            param1.Add("FN_TYPE", FN_TYPE);
                            DataTable dt1 = this.GetData("CHRIS_SP_FinReduction_FN_MONTH_MANAGER", "FinMonthValues", param1);

                            if (dt1 != null)
                            {
                                if (dt1.Rows.Count > 0)
                                {
                                    //lFnMdate = null;

                                    if (dt1.Rows[0]["lFN_DEBIT"].ToString() != string.Empty)
                                    {
                                        DDebit = double.Parse(dt1.Rows[0]["lFN_DEBIT"].ToString());
                                    }
                                    if (dt1.Rows[0]["lFN_CREDIT"].ToString() != string.Empty)
                                    {
                                        CCredit = double.Parse(dt1.Rows[0]["lFN_CREDIT"].ToString());
                                    }
                                    if (dt1.Rows[0]["FN_PAY_LEFT"].ToString() != string.Empty)
                                    {
                                        OPayLeft = double.Parse(dt1.Rows[0]["FN_PAY_LEFT"].ToString());
                                    }
                                    if (dt1.Rows[0]["FN_BALANCE"].ToString() != string.Empty)
                                    {
                                        BBal = double.Parse(dt1.Rows[0]["FN_BALANCE"].ToString());
                                    }
                                    txtfnBalance = BBal.ToString();
                                    txtFnPAyLeft = OPayLeft.ToString();
                                }
                            }

                            var res = new
                            {
                                DTstartdate = DTstartdate,
                                DTFN_END_DATE = DTFN_END_DATE,
                                dtExtratime = dtExtratime,
                                DTPAY_GEN_DATE = DTPAY_GEN_DATE,
                                DDebit = DDebit,
                                CCredit = CCredit,
                                OPayLeft = OPayLeft,
                                BBal = BBal,
                                txtfnBalance = txtfnBalance,
                                txtFnPAyLeft = txtFnPAyLeft,
                                MessageBox = MessageBox,
                            };
                            return Json(res);
                        }
                        else
                        {
                            Dictionary<string, object> param1 = new Dictionary<string, object>();
                            param1.Add("FN_FIN_NO", FN_FIN_NO);
                            param1.Add("PR_P_NO", PR_P_NO);
                            param1.Add("FN_TYPE", FN_TYPE);
                            DataTable dt1 = this.GetData("CHRIS_SP_FinReduction_FN_MONTH_MANAGER", "FinMonthValuesDMV", param1);

                            if (dt1 != null)
                            {
                                if (dt1.Rows.Count > 0)
                                {
                                    //lFnMdate = null;

                                    if (dt1.Rows[0]["fn_credit"].ToString() != string.Empty)
                                    {
                                        DDebit = double.Parse(dt1.Rows[0]["fn_credit"].ToString());
                                    }
                                    if (dt1.Rows[0]["FN_BALANCE"].ToString() != string.Empty)
                                    {
                                        BBal = double.Parse(dt1.Rows[0]["FN_BALANCE"].ToString());
                                    }
                                    if (dt1.Rows[0]["FN_PAY_LEFT"].ToString() != string.Empty)
                                    {
                                        OPayLeft = double.Parse(dt1.Rows[0]["FN_PAY_LEFT"].ToString());
                                    }
                                    if (dt1.Rows[0]["fn_credit"].ToString() != string.Empty)
                                    {
                                        wBBal = double.Parse(dt1.Rows[0]["fn_credit"].ToString());
                                    }

                                    txtfnBalance = BBal.ToString();
                                    txtFnPAyLeft = OPayLeft.ToString();
                                    W_Bal = wBBal.ToString();
                                    txtENHAN_AMT = DDebit.ToString();
                                }
                            }

                            Dictionary<string, object> param2 = new Dictionary<string, object>();
                            param2.Clear();
                            param2.Add("PR_P_NO", PR_P_NO);
                            param2.Add("FN_TYPE", FN_TYPE);
                            param2.Add("FN_FIN_NO", FN_FIN_NO);
                            DataTable dtPayGenDate = this.GetData("CHRIS_SP_FinReduction_FN_MONTH_MANAGER", "FinPayGenDateANother", param2);
                            if (dtPayGenDate != null)
                            {
                                if (dtPayGenDate.Rows.Count > 0)
                                {
                                    if (dtPayGenDate.Rows[0].ItemArray[0].ToString() != string.Empty)
                                    {
                                        lPayGenDate = Convert.ToDateTime(dtPayGenDate.Rows[0].ItemArray[0].ToString());
                                        DTPAY_GEN_DATE = lPayGenDate;
                                    }
                                }
                            }

                            Dictionary<string, object> paramNew = new Dictionary<string, object>();
                            paramNew.Clear();
                            paramNew.Add("PR_P_NO", PR_P_NO);
                            paramNew.Add("FN_TYPE", FN_TYPE);
                            paramNew.Add("FN_FIN_NO", FN_FIN_NO);

                            DataTable dtFinPayMatDate = this.GetData("CHRIS_SP_FinReduction_FN_MONTH_MANAGER", "FinPayMatDate", paramNew);
                            if (dtFinPayMatDate != null)
                            {
                                if (dtFinPayMatDate.Rows.Count > 0)
                                {
                                    if (dtFinPayMatDate.Rows[0].ItemArray[0].ToString() != string.Empty)
                                    {
                                        if (dtFinPayMatDate.Rows[0].ItemArray[0].ToString() != string.Empty)
                                        {
                                            lPayGenDate = Convert.ToDateTime(dtFinPayMatDate.Rows[0].ItemArray[0].ToString());
                                            dtEnhancementDate = lPayGenDate;
                                        }
                                    }
                                }
                            }

                            PayGenDate = Convert.ToDateTime(DTPAY_GEN_DATE);
                            EnhancementDate = Convert.ToDateTime(dtEnhancementDate);
                            if (DateTime.Compare(EnhancementDate.Date, PayGenDate.Date) < 0)
                            {
                                MessageBox = "";
                                MessageBox = "NO. REDUCTION HAS BEEN DONE LATELY .....";
                                var res1 = new
                                {
                                    MessageBox = MessageBox,
                                };
                                return Json(res1);
                            }

                            EnhancementDateValidate(pr_transfer, Functions, PR_P_NO, PR_NEW_ANNUAL_PACK, FN_C_RATIO, finMarkup, FN_PAY_LEFT, FN_TYPE);

                            returnValue = Proc_21(PR_P_NO, FN_TYPE, FN_FIN_NO, Functions, FN_CREDIT, CAN_BE, Factor, CreditRatio, FN_BALANCE, txtVal3, finMarkup);

                            if (returnValue == false)
                            {
                                return Json(null);
                            }

                            var res = new
                            {
                                DTstartdate = DTstartdate,
                                DTFN_END_DATE = DTFN_END_DATE,
                                dtExtratime = dtExtratime,
                                DTPAY_GEN_DATE = DTPAY_GEN_DATE,
                                DDebit = DDebit,
                                CCredit = CCredit,
                                OPayLeft = OPayLeft,
                                BBal = BBal,
                                txtfnBalance = txtfnBalance,
                                txtFnPAyLeft = txtFnPAyLeft,
                                MessageBox = MessageBox,
                                txtVal3 = txtVal3,
                                txtTotalMonthInstall = txtTotalMonthInstall,
                                txtMarkup = txtMarkup,
                                txtFIN_CREDIT = txtFIN_CREDIT,
                                txtBookRatio = txtBookRatio,
                                txtTOT_INST = txtTOT_INST,
                                txtMARKUP_REC = txtMARKUP_REC,
                                txtFactor = txtFactor,
                                txtCreditRatio = txtCreditRatio,
                                txtRatioAvail = txtRatioAvail,
                                txtAvailable = txtAvailable,
                                txtCanBeAvail = txtCanBeAvail,
                                W_Bal = W_Bal,
                            };
                            return Json(res);
                        }
                    }
                }
            }
            else
            {
                return Json(null);
            }
            return Json(null);
        }
        public ActionResult dtEnhancementDate_Validating_FRE(DateTime? FN_MDATE, DateTime? DTPAY_GEN_DATE, string pr_transfer, string PR_P_NO, string PR_NEW_ANNUAL_PACK, string FN_C_RATIO, string finMarkup, string FN_PAY_LEFT, string FN_TYPE)
        {
            double val3 = 0;
            int last_Day = 0;
            double val4 = 0;
            int transferCount = 0;
            if (FN_MDATE != null)
            {
                DateTime dtDate1 = Convert.ToDateTime(DTPAY_GEN_DATE);//dat2
                DateTime dtDate2 = Convert.ToDateTime(FN_MDATE);
                if (pr_transfer != string.Empty)
                {
                    int.TryParse(pr_transfer, out transferCount);
                }
                if (DateTime.Compare(dtDate2.Date, dtDate1.Date) <= 0 && transferCount < 6)
                {
                    MessageBox = "";
                    MessageBox = "DATE HAS TO BE GREATER THAN PAYROLL DATE";
                    var res3 = new
                    {
                        MessageBox = MessageBox,
                    };
                    return Json(res3);
                }
                else
                {
                    DateTime dt5 = new DateTime();
                    dt5 = dtDate1.AddMonths(1);// dat2

                    if ((!(dtDate2 >= dtDate1 && dtDate2 <= dt5)) && transferCount < 6)
                    {
                        MessageBox = "";
                        MessageBox = "0- DATE HAS TO BE JUST AFTER THE PAYROLL DATE " + transferCount.ToString();
                        var res2 = new
                        {
                            MessageBox = MessageBox,
                        };
                        return Json(res2);
                    }
                    else
                    {
                        if (dtDate2.Month == dtDate1.Month && dtDate2.Year == dtDate1.Year)
                        {
                            System.TimeSpan diffResult = dtDate2.Subtract(dtDate1);
                            val3 = Math.Round(diffResult.TotalDays);
                        }
                        else
                        {
                            last_Day = GetLastDayOfMonth(dtDate1);
                            val4 = last_Day - dtDate1.Day;
                            val3 = val4 + dtDate2.Day;
                        }
                    }

                    if (val3 > 30 && transferCount < 6)
                    {
                        MessageBox = "";
                        MessageBox = "1- DATE HAS TO BE JUST AFTER PAYROLL GENERATION DATE " + transferCount.ToString();
                        var res1 = new
                        {
                            MessageBox = MessageBox,
                        };
                        return Json(res1);
                    }

                    txtVal3 = val3.ToString();
                    PROC_11(PR_P_NO, PR_NEW_ANNUAL_PACK, FN_C_RATIO, finMarkup, FN_PAY_LEFT, FN_TYPE);
                }
                var res = new
                {
                    txtVal3 = txtVal3,
                    txtFactor = txtFactor,
                    txtCreditRatio = txtCreditRatio,
                    txtRatioAvail = txtRatioAvail,
                    txtAvailable = txtAvailable,
                    txtCanBeAvail = txtCanBeAvail,
                };
                return Json(res);
            }
            else
            {
                MessageBox = "";
                MessageBox = "DATE HAS TO BE GREATER THAN PAYROLL DATE";
                var res = new
                {
                    MessageBox = MessageBox,
                };
                return Json(res);
            }
            return Json(null);
        }
        private void EnhancementDateValidate(string pr_transfer, string Functions, string PR_P_NO, string PR_NEW_ANNUAL_PACK, string FN_C_RATIO, string finMarkup, string FN_PAY_LEFT, string FN_TYPE)
        {
            double val3 = 0.0;
            int last_Day = 0;
            double val4 = 0.0;
            int transferCount = 0;
            if (pr_transfer != string.Empty)
            {
                int.TryParse(pr_transfer, out transferCount);
            }
            if (dtEnhancementDate != null)
            {
                DateTime dtDate1 = Convert.ToDateTime(DTPAY_GEN_DATE);
                DateTime dtDate2 = Convert.ToDateTime(dtEnhancementDate);
                if (DateTime.Compare(dtDate2.Date, dtDate1.Date) <= 0 && transferCount < 6)
                {
                    MessageBox = "";
                    MessageBox = "DATE HAS TO BE GREATER THAN PAYROLL DATE";

                }
                else
                {
                    DateTime dt5 = new DateTime();
                    dt5 = dtDate1.AddMonths(1);// dat2

                    if ((!(dtDate2 >= dtDate1 && dtDate2 <= dt5)) && transferCount < 6)
                    {
                        MessageBox = "";
                        MessageBox = "0- DATE HAS TO BE JUST AFTER THE PAYROLL DATE  '" + transferCount.ToString() + "' ";

                    }
                    else
                    {
                        if (dtDate2.Month == dtDate1.Month && dtDate2.Year == dtDate1.Year)
                        {
                            System.TimeSpan diffResult = dtDate2.Subtract(dtDate1);
                            val3 = Math.Round(diffResult.TotalDays);
                        }
                        else
                        {
                            last_Day = GetLastDayOfMonth(dtDate1);
                            val4 = last_Day - dtDate1.Day;
                            val3 = val4 + dtDate2.Day;
                        }
                    }

                    if (val3 > 30 && transferCount < 6)
                    {
                        MessageBox = "";
                        MessageBox = "1- DATE HAS TO BE JUST AFTER PAYROLL GENERATION DATE '" + transferCount.ToString() + "'";

                    }
                    txtVal3 = val3.ToString();
                    PROC_11(PR_P_NO, PR_NEW_ANNUAL_PACK, FN_C_RATIO, finMarkup, FN_PAY_LEFT, FN_TYPE);
                }
            }
            else
            {
                MessageBox = "";
                MessageBox = "DATE HAS TO BE GREATER THAN PAYROLL DATE";

            }
        }
        public bool Proc_21(string PR_P_NO, string FN_TYPE, string FN_FIN_NO, string Functions, string FN_CREDIT, string CAN_BE, string Factor, string CreditRatio, string FN_BALANCE, string txtVal3, string finMarkup)
        {
            double EnhancementAmt = 0.0;
            double lAmountofLoanCanAvail = 0.0;
            double AUX44 = 0.0;
            double AUX55 = 0.0;
            double AUX99 = 0.0;
            double lFACTOR = 0.0;
            double lAVAILABLE_RATIO = 0.0;
            double lLESS_RATIO_AVAILed = 0.0;
            double lCREDIT_RATIO = 0.0;
            double lTOT_MONTH_INST = 0.0;
            double lfnBalance = 0.0;
            double lFiN_MARKUP = 0.0;
            double lMARKUP = 0.0;
            double WVal3 = 0.0;
            double lFiN_CREDIT = 0.0;
            double lTOT_INST = 0.0;
            double lMarkupRec = 0.0;
            bool returnValue = true;


            if (Functions != "View" && Functions != "Delete")
            {
                if (FN_CREDIT == string.Empty)
                {
                    return returnValue = false;
                }
                else
                {
                    if (FN_CREDIT != string.Empty)
                    {
                        double.TryParse(FN_CREDIT, out EnhancementAmt);
                    }
                    if (CAN_BE != string.Empty)
                    {
                        double.TryParse(CAN_BE, out lAmountofLoanCanAvail);
                    }
                    if (EnhancementAmt > lAmountofLoanCanAvail)
                    {
                        return returnValue = false;
                    }
                }
            }

            if (Functions == "Add")
            {

                // :enhan_amt := 0 - abs(:enhan_amt);
                if (FN_CREDIT != string.Empty)
                {
                    double.TryParse(FN_CREDIT, out EnhancementAmt);
                }
                if (CAN_BE != string.Empty)
                {
                    double.TryParse(CAN_BE, out lAmountofLoanCanAvail);
                }
                lAmountofLoanCanAvail = lAmountofLoanCanAvail - EnhancementAmt;
                if (Factor != string.Empty)
                {
                    double.TryParse(Factor, out lFACTOR);
                }
                lAVAILABLE_RATIO = (lAmountofLoanCanAvail * lFACTOR) / 1000;
                if (CreditRatio != string.Empty)
                {
                    double.TryParse(CreditRatio, out lCREDIT_RATIO);
                }
                lLESS_RATIO_AVAILed = lCREDIT_RATIO - lAVAILABLE_RATIO;
                if (FN_BALANCE != string.Empty)
                {

                    double.TryParse(FN_BALANCE, out lfnBalance);
                }
                lTOT_MONTH_INST = (lfnBalance * lFACTOR) / 1000;
                if (txtVal3 != string.Empty)
                {
                    double.TryParse(txtVal3, out WVal3);
                }
                if (finMarkup != string.Empty)
                {
                    double.TryParse(finMarkup, out lMARKUP);
                }
                lFiN_MARKUP = (lfnBalance * (WVal3 / 100) * lMARKUP) / MARKUP_RATE_DAYS;
                lFiN_CREDIT = lTOT_MONTH_INST - lFiN_MARKUP;
                txtMarkup = "0";
                txtFIN_CREDIT = "0";
            }

            Dictionary<string, object> paramsFin = new Dictionary<string, object>();
            paramsFin.Add("FN_FIN_NO", FN_FIN_NO);
            paramsFin.Add("PR_P_NO", PR_P_NO);

            DataTable dtFinBook = this.GetData("CHRIS_SP_FinReduction_FN_MONTH_MANAGER", "FinProc2", paramsFin);
            if (dtFinBook != null)
            {
                if (dtFinBook.Rows.Count > 0)
                {
                    if (dtFinBook.Rows[0].ItemArray[0].ToString() != string.Empty)
                    {
                        txtBookRatio = dtFinBook.Rows[0].ItemArray[0].ToString();
                    }
                }
            }

            if (Functions == "View" || Functions == "Delete")
            {
                DataTable dtFinMonthlyDed = this.GetData("CHRIS_SP_FinReduction_FN_MONTH_MANAGER", "FinBookMonthlyDed", paramsFin);
                if (dtFinMonthlyDed != null)
                {
                    if (dtFinMonthlyDed.Rows.Count > 0)
                    {
                        if (dtFinMonthlyDed.Rows[0].ItemArray[0].ToString() != string.Empty)
                        {
                            if (double.TryParse(dtFinMonthlyDed.Rows[0].ItemArray[0].ToString(), out lTOT_MONTH_INST))
                            {
                                txtTotalMonthInstall = lTOT_MONTH_INST.ToString();
                            }
                        }
                    }
                }
            }

            Dictionary<string, object> Installparams = new Dictionary<string, object>();
            Installparams.Add("FN_FIN_NO", FN_FIN_NO);
            Installparams.Add("PR_P_NO", PR_P_NO);
            Installparams.Add("FN_TYPE", FN_TYPE);

            DataTable dtFinInstll = this.GetData("CHRIS_SP_FinReduction_FN_MONTH_MANAGER", "FinMonthlyInstall", Installparams);
            if (dtFinInstll != null)
            {
                if (dtFinInstll.Rows.Count > 0)
                {
                    if (dtFinInstll.Rows[0].ItemArray[0].ToString() != string.Empty)
                    {
                        if (double.TryParse(dtFinInstll.Rows[0].ItemArray[0].ToString(), out lTOT_INST))
                        {
                            txtTOT_INST = lTOT_INST.ToString();
                        }
                    }

                    if (dtFinInstll.Rows[0].ItemArray[1].ToString() != string.Empty)
                    {
                        if (double.TryParse(dtFinInstll.Rows[0].ItemArray[1].ToString(), out lMarkupRec))
                        {
                            txtMARKUP_REC = lMarkupRec.ToString();
                        }
                    }
                }
            }
            if (Functions == "Modify")
            {
                //dtEnhancementDate.Focus();
            }
            if (Functions == "Delete")
            {
                MessageBox = "";
                MessageBox = "DO YOU WANT TO DELETE THE RECORD [Y]es [N]o";

                return returnValue;
            }
            //if (Functions == "View")
            //{
            //    //MessageBox = "";
            //    //MessageBox = "DO YOU WANT TO VIEW MORE RECORDS [Y]es [N]o";
            //}
            return returnValue;
        }
        private void PROC_11(string PR_P_NO, string PR_NEW_ANNUAL_PACK, string FN_C_RATIO, string finMarkup, string FN_PAY_LEFT, string FN_TYPE)
        {

            double lCREDIT_RATIO;
            double lCREDIT_RATIO_MONTH;
            int lCOUNTER;
            double lLESS_RATIO_AVAILED;
            double lAVAILABLE_RATIO;
            double lFACTOR;
            double lAMT_OF_LOAN_CAN_AVAIL;
            double lTOTAL_MONTHLY_INSTALLMENT;
            int CREDIT_RATIO_PER;
            int AUX1;
            double AUX2;
            double AUX3;
            int AUX4;
            int AUX5;
            int AUX7;
            double AUX8;
            double AUX9;
            double dis_repay;
            double partCalc = 0.0;
            AUX2 = 0;
            AUX7 = 0;
            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Add("PR_P_NO", PR_P_NO);
            DataTable dtMonthlyDeduction = this.GetData("CHRIS_SP_Other_fin_Booking_MANAGER", "MonthlyDeduction", param);

            if (dtMonthlyDeduction != null && dtMonthlyDeduction.Rows.Count > 0)
            {
                AUX2 = double.Parse(dtMonthlyDeduction.Rows[0].ItemArray[0].ToString() == "" ? "0" : dtMonthlyDeduction.Rows[0].ItemArray[0].ToString());
            }

            AUX9 = double.Parse(PR_NEW_ANNUAL_PACK == "" ? "0" : PR_NEW_ANNUAL_PACK);

            DataTable dtfnBookCount = this.GetData("CHRIS_SP_Other_fin_Booking_MANAGER", "fnBookingProcCount", param);
            if (dtfnBookCount != null)
            {
                if (dtfnBookCount.Rows.Count > 0)
                {
                    lCREDIT_RATIO = (AUX9 / 100) * double.Parse(FN_C_RATIO == "" ? "0" : PROC_33(FN_TYPE));
                }
            }
            else
            {
                lCREDIT_RATIO = (AUX9 / 100) * double.Parse(FN_C_RATIO == "" ? "0" : FN_C_RATIO);
            }

            lCREDIT_RATIO = AUX9 / 100 * double.Parse(FN_C_RATIO == "" ? "0" : FN_C_RATIO);
            lCREDIT_RATIO_MONTH = lCREDIT_RATIO / 12;

            lCREDIT_RATIO_MONTH = Math.Round(lCREDIT_RATIO_MONTH, 2, MidpointRounding.AwayFromZero);

            lLESS_RATIO_AVAILED = AUX2;

            lAVAILABLE_RATIO = lCREDIT_RATIO_MONTH - lLESS_RATIO_AVAILED;

            if (finMarkup != string.Empty && finMarkup != "0")
            {
                AUX8 = double.Parse(finMarkup) / 1200;
                AUX8 = AUX8 + 1;
                AUX3 = Convert.ToDouble(Math.Pow(AUX8, -double.Parse(FN_PAY_LEFT == "" ? "0" : FN_PAY_LEFT)));
                lFACTOR = (AUX8 - 1) / (1 - AUX3) * 1000;
            }
            else
            {
                lFACTOR = 1000 / double.Parse(FN_PAY_LEFT == "" ? "0" : FN_PAY_LEFT);
            }

            txtFactor = lFACTOR.ToString();
            txtCreditRatio = lCREDIT_RATIO_MONTH.ToString();
            txtRatioAvail = lLESS_RATIO_AVAILED.ToString();
            txtAvailable = lAVAILABLE_RATIO.ToString();
            txtFactor = lFACTOR.ToString();
            partCalc = (1000 / Math.Round(lFACTOR, 4));
            lAMT_OF_LOAN_CAN_AVAIL = (Math.Round(lAVAILABLE_RATIO, 2) * partCalc);
            txtCanBeAvail = lAMT_OF_LOAN_CAN_AVAIL.ToString();

            if (lAMT_OF_LOAN_CAN_AVAIL != 0)
            {

            }
            else
            {
                MessageBox = "";
                MessageBox = "BALANCE = 0 THEREFORE REDUCTION NOT ALLOWED";

            }
        }
        private String PROC_33(string FN_TYPE)
        {

            if (FN_TYPE != string.Empty)
            {
                if (FN_TYPE.Length > 0)
                {
                    Dictionary<string, object> param = new Dictionary<string, object>();
                    param.Add("FinType", FN_TYPE.Substring(0, 1));
                    DataTable dt = this.GetData("CHRIS_SP_Other_fin_Booking_MANAGER", "HouseLoanEntryCheck", param);
                    if (dt != null)
                    {
                        if (dt.Rows.Count > 1)
                        {
                            MessageBox = "";
                            MessageBox = "More than One House Loan Entries";

                        }
                        else if (dt.Rows.Count.Equals(1))
                            ratio = dt.Rows[0][0].ToString();
                    }
                }
            }
            return ratio;
        }
        public JsonResult FRE_DELETE(string FN_FIN_NO, string PR_P_NO)
        {

            Dictionary<string, object> paramDelete = new Dictionary<string, object>();

            paramDelete.Add("FN_FIN_NO", FN_FIN_NO);
            paramDelete.Add("PR_P_NO", PR_P_NO);
            // paramDelete.Add("FN_TYPE", txtFinType.Text);
            Result rsltCode;
            CmnDataManager cmnDM = new CmnDataManager();
            rsltCode = cmnDM.Execute("CHRIS_SP_Fin_ReductionFN_Month_DELETE_ALL", "DELETE_ALL", paramDelete);

            if (rsltCode.isSuccessful)
            {
                MessageBox = "";
                MessageBox = "Record Deleted Successfully";
                var res = new
                {
                    MessageBox = MessageBox,
                };
                return Json(res);
            }

            return Json(null);
        }
        public ActionResult Pay_Gen_Validating_FRE(DateTime? Pay_Gen, DateTime? FN_MDATE)
        {
            double val3 = 0;
            int last_Day = 0;
            double val4 = 0;
            if (Pay_Gen != null)
            {

                DateTime dtDate1 = Convert.ToDateTime(FN_MDATE);//dat2
                DateTime dtDate2 = Convert.ToDateTime(Pay_Gen);
                if (DateTime.Compare(dtDate1.Date, dtDate2.Date) >= 0)
                {
                    MessageBox = "";
                    MessageBox = "DATE HAS TO BE GREATER THAN REDUCTION DATE";
                    var res = new
                    {
                        MessageBox = MessageBox,
                    };
                    return Json(res);
                }
                else
                {
                    DateTime dt5 = new DateTime();
                    dt5 = dtDate1.AddMonths(1);// dat2
                    if (!(dtDate2.Date >= dtDate1.Date && dtDate2.Date <= dt5.Date))
                    {
                        MessageBox = "";
                        MessageBox = "DATE HAS TO BE THE WITHIN THE MONTH AFTER REDUCTION DATE";
                        var res1 = new
                        {
                            MessageBox = MessageBox,
                        };
                        return Json(res1);
                    }
                    else
                    {
                        if (dtDate2.Month == dtDate1.Month && dtDate2.Year == dtDate1.Year)
                        {
                            System.TimeSpan diffResult = dtDate2.Subtract(dtDate1);
                            val3 = Math.Round(diffResult.TotalDays);
                        }
                        else
                        {
                            last_Day = GetLastDayOfMonth(dtDate1);
                            val4 = last_Day - dtDate1.Day;
                            val3 = val4 + dtDate2.Day;
                        }
                    }
                    if (val3 > 30)
                    {
                        MessageBox = "";
                        MessageBox = "DATE HAS TO BE WITHIN THE MONTH AFTER REDUCTION DATE";
                        var res2 = new
                        {
                            MessageBox = MessageBox,
                        };
                        return Json(res2);
                    }
                    wval5 = val3.ToString();
                    var res3 = new
                    {
                        wval5 = wval5,
                        MessageBox = MessageBox,
                    };
                    return Json(res3);
                }
            }
            return Json(null);
        }
        public ActionResult FN_CREDIT_Validate_FRE(string Functions, string W_Bal, string FN_CREDIT, string FN_BALANCE, string PR_NEW_ANNUAL_PACK, string PR_P_NO, string FN_FINANCE_NO, string FN_TYPE, string txtVal3, string Factor, string finMarkup, string CAN_BE, string CreditRatio, string pr_category, string pr_level, string FN_M_BRANCH, string txtWTenCBonus, string wval5)
        {
            string saveMessageBox = "";
            string lFinType = "";
            double Balance = 0.0;
            double w_Bal = 0.0;
            if (Functions == "Add")
            {
                w_Bal = 0.0;
            }
            else
            {
                if (W_Bal != string.Empty)
                {
                    w_Bal = double.Parse(W_Bal);
                }
            }
            if (FN_CREDIT != string.Empty)
            {
                if (FN_BALANCE != string.Empty)
                {
                    double.TryParse(FN_BALANCE, out Balance);
                    Balance = Balance + w_Bal;
                }

                if (double.Parse(FN_CREDIT) > Balance)
                {
                    MessageBox = "";
                    MessageBox = "REDUCTION AMOUNT HAS TO BE LESS THAN BALANCE ";
                    var res = new
                    {
                        MessageBox = MessageBox,
                    };
                    return Json(res);
                }

                else
                {
                    #region Fileds

                    double AUX44;
                    double AUX55;
                    double aux99;
                    int AUXX1;
                    int AUXX2;

                    double FinMarkup;
                    double val3;
                    double finBal;
                    double tot_month_inst;
                    double lFactor;
                    double fnCredit;
                    double lamt_of_loan_can_avail;
                    double lENHAN_AMT;
                    double lAvailableRatio;
                    double lCREDIT_RATIO;
                    double lLESS_RATIO_AVAILed;
                    double lTOT_MONTH_INST;
                    double LMarkup;
                    double lFinMarkup;
                    double lwwval5;
                    double lFinCredit;
                    double lFnCRatioBooking;
                    double llFinMarkup;
                    double ClericalBonus;
                    double spAllowanceAmt;
                    #endregion

                    aux99 = double.Parse(PR_NEW_ANNUAL_PACK == "" ? "0" : PR_NEW_ANNUAL_PACK);

                    val3 = double.Parse(txtVal3 == "" ? "0" : txtVal3);
                    finBal = double.Parse(FN_BALANCE == "" ? "0" : FN_BALANCE);
                    lFactor = double.Parse(Factor == "" ? "0" : Factor);
                    lENHAN_AMT = double.Parse(FN_CREDIT == "" ? "0" : FN_CREDIT);
                    llFinMarkup = double.Parse(finMarkup == "" ? "0" : finMarkup);
                    //----   * Markup Pending


                    FinMarkup = ((finBal + w_Bal) * (val3 / 100) * llFinMarkup) / MARKUP_RATE_DAYS;

                    tot_month_inst = (finBal + w_Bal) * (lFactor / 1000);

                    fnCredit = tot_month_inst - FinMarkup;

                    lamt_of_loan_can_avail = double.Parse((CAN_BE == "INFINITY" || CAN_BE == "") ? "0" : CAN_BE);

                    lamt_of_loan_can_avail = lamt_of_loan_can_avail + lENHAN_AMT;

                    lCREDIT_RATIO = double.Parse(CreditRatio == "" ? "0" : CreditRatio);

                    lAvailableRatio = lamt_of_loan_can_avail * lFactor / 1000;
                    lLESS_RATIO_AVAILed = lCREDIT_RATIO - lAvailableRatio;

                    finBal = (finBal + w_Bal) - lENHAN_AMT;

                    LMarkup = double.Parse(finMarkup == "" ? "0" : finMarkup);


                    lTOT_MONTH_INST = ((finBal * lFactor) / 1000);

                    lwwval5 = double.Parse(wval5 == "" ? "0" : wval5);
                    lFinMarkup = (finBal * (lwwval5 / 100) * LMarkup) / MARKUP_RATE_DAYS;
                    txtFnMonthDebit = lENHAN_AMT.ToString();
                    AUX55 = aux99 / 12;

                    if (pr_category == "C")
                    {
                        //coding pending

                        ClericalBonus = Double.Parse(txtWTenCBonus == "" ? "0" : txtWTenCBonus);
                        AUX44 = (ClericalBonus / 100) + 1;
                        AUX55 = AUX55 * AUX44;

                        Dictionary<string, object> param = new Dictionary<string, object>();
                        param.Clear();
                        param.Add("category", pr_category);
                        param.Add("level", pr_level);
                        param.Add("fn_branch", FN_M_BRANCH);
                        DataTable dtClericalAllowance = this.GetData("CHRIS_SP_Other_fin_Booking_MANAGER", "ClericalStaffCheck", param);
                        if (dtClericalAllowance != null)
                        {
                            if (dtClericalAllowance.Rows.Count > 0)
                            {
                                spAllowanceAmt = double.Parse(dtClericalAllowance.Rows[0].ItemArray[0].ToString() == "" ? "0" : dtClericalAllowance.Rows[0].ItemArray[0].ToString());
                                AUX55 = AUX55 + spAllowanceAmt;
                            }
                        }
                        else
                        {
                            MessageBox = "";
                            MessageBox = "UPDATE THE ALLOWANCE TABLE FOR GOVT. ALLOWANCE";
                            var res = new
                            {
                                MessageBox = MessageBox,
                            };
                            return Json(res);
                        }
                    }

                    lFinCredit = (lTOT_MONTH_INST - lFinMarkup);
                    txtFIN_CREDIT = lFinCredit.ToString();
                    txtFnMonthCredit = "0";
                    txtFnMonthMarkup = "0";
                    lFnCRatioBooking = (lLESS_RATIO_AVAILed / AUX55) * 100;
                    txtBookRatio = lFnCRatioBooking.ToString();
                    //lFinType = txtFinType.Text.Replace("\\", @"\");
                    //SETValues
                    txtMarkup = lFinMarkup.ToString();

                    txtRatioAvail = lLESS_RATIO_AVAILed.ToString();

                    txtAvailable = lAvailableRatio.ToString();

                    txtCanBeAvail = lamt_of_loan_can_avail.ToString();

                    txtTotalMonthInstall = lTOT_MONTH_INST.ToString();

                    FN_BALANCE = finBal.ToString();


                    txtFnMonthDebit = "0";
                    txtFnMonthMarkup = "0";

                    Dictionary<string, object> paramMonth = new Dictionary<string, object>();
                    paramMonth.Add("FN_FINANCE_NO", FN_FINANCE_NO);
                    paramMonth.Add("PR_P_NO", PR_P_NO);


                    paramMonth.Add("FinType", FN_TYPE.Replace("\\", @"\"));
                    DataTable dtFinPayMonth = this.GetData("CHRIS_SP_Other_fin_Booking_MANAGER", "fnMonthValues", paramMonth);
                    if (dtFinPayMonth != null)
                    {
                        if (dtFinPayMonth.Rows.Count > 0)
                        {
                            txtTOT_INST = dtFinPayMonth.Rows[0].ItemArray[0].ToString();
                            txtMARKUP_REC = dtFinPayMonth.Rows[0].ItemArray[1].ToString();
                        }
                    }

                    if (Functions == "Add" || Functions == "Modify")
                    {
                        saveMessageBox = "";
                        saveMessageBox = "Do you want to save this record [Y/N]..";
                        var res = new
                        {
                            saveMessageBox = saveMessageBox,
                        };
                        return Json(res);
                    }

                    if (saveMessageBox != "")
                    {
                        var res = new
                        {
                            txtFIN_CREDIT = txtFIN_CREDIT,
                            txtBookRatio = txtBookRatio,
                            txtMarkup = txtMarkup,
                            txtRatioAvail = txtRatioAvail,
                            txtAvailable = txtAvailable,
                            txtCanBeAvail = txtCanBeAvail,
                            txtTotalMonthInstall = txtTotalMonthInstall,
                            FN_BALANCE = FN_BALANCE,
                            txtFnMonthDebit = txtFnMonthDebit,
                            txtFnMonthMarkup = txtFnMonthMarkup,
                            txtTOT_INST = txtTOT_INST,
                            txtMARKUP_REC = txtMARKUP_REC,
                            MessageBox = MessageBox,
                            saveMessageBox = saveMessageBox,
                        };
                        return Json(res);
                    }
                    else
                    {
                        return Json(null);
                    }

                }
            }

            return Json(null);
        }

        [HttpPost]
        public JsonResult Upsert_FRE(string Functions, Decimal? PR_P_NO, string FN_M_BRANCH, string FN_TYPE, string FN_FIN_NO, DateTime? FN_MDATE, Decimal? FN_DEBIT, Decimal? FN_CREDIT, Decimal? FN_PAY_LEFT, Decimal? FN_BALANCE, Decimal? FN_LOAN_BALANCE, Decimal? FN_MARKUP, string FN_LIQ_FLAG, string FN_SUBTYPE, Decimal? FN_C_RATIO_PER, Decimal? TOT_MONTHLY_INSTALL, int? ID)
        {
            try
            {
                Dictionary<string, object> d = new Dictionary<string, object>();

                d.Add("PR_P_NO", PR_P_NO);
                d.Add("FN_M_BRANCH", FN_M_BRANCH);
                d.Add("FN_TYPE", FN_TYPE);
                d.Add("FN_FIN_NO", FN_FIN_NO);
                d.Add("FN_MDATE", FN_MDATE);
                d.Add("FN_DEBIT", FN_DEBIT);
                d.Add("FN_CREDIT", FN_CREDIT);
                d.Add("FN_PAY_LEFT", FN_PAY_LEFT);
                d.Add("FN_BALANCE", FN_BALANCE);
                d.Add("FN_LOAN_BALANCE", FN_LOAN_BALANCE);
                d.Add("FN_MARKUP", FN_MARKUP);
                d.Add("FN_LIQ_FLAG", FN_LIQ_FLAG);
                d.Add("FN_SUBTYPE", FN_SUBTYPE);
                d.Add("FN_C_RATIO_PER", FN_C_RATIO_PER);
                d.Add("TOT_MONTHLY_INSTALL", TOT_MONTHLY_INSTALL);
                d.Add("ID", ID);

                CmnDataManager objCmnDataManager = new CmnDataManager();

                if (Functions == "Modify")
                {
                    Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_FinReduction_FN_MONTH_MANAGER", "Update", d);
                    if (rsltCode.isSuccessful)
                    {
                        var res = Json(rsltCode);
                        return Json("Your request is modify successfully !");
                    }
                }
                else if (Functions == "Add")
                {
                    Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_FinReduction_FN_MONTH_MANAGER", "Save", d);
                    if (rsltCode.isSuccessful)
                    {
                        var res = Json(rsltCode);
                        return Json("Your request is successfully completed !");
                    }
                }
                return Json(null);
            }
            catch (Exception ex)
            {
                return Json("Something went wrong");
            }
        }

        #endregion

        #region LiquidatedFinanceQuery

        public IActionResult LiquidatedFinanceQuery()
        {
            return View();
        }

        public ActionResult btn_LFQ_PR_P_NO()
        {
            Dictionary<string, object> d = new Dictionary<string, object>();

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_LIQUID_FIANANCE_QUERY_MANAGER", "Personal_Lov", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        public ActionResult tbl_DGVDetail_LFQ(Decimal? PR_P_NO)
        {
            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("PR_P_NO", PR_P_NO);
            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_LIQUID_FIANANCE_QUERY_MANAGER", "List", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }


        #endregion

        #region MortgagePropertyCollateralEntry

        public IActionResult MortgagePropertyCollateralEntry()
        {
            return View();
        }

        public ActionResult btn_PR_P_NO_MPCE()
        {
            Dictionary<string, object> d = new Dictionary<string, object>();

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_FIN_BOOKING_MPC_MANAGER", "PersonnelNo_MortageProperty", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        public ActionResult btn_FN_FIN_NO_MPCE()
        {
            Dictionary<string, object> d = new Dictionary<string, object>();

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_FIN_BOOKING_MPC_MANAGER", "FinNo_MortageProperty", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        public ActionResult btn_FN_COLLAT_NO_MPCE()
        {
            Dictionary<string, object> d = new Dictionary<string, object>();

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_FIN_BOOK_COLLAT_MANAGER", "Lov_list", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        // [HttpPost]
        // public JsonResult UpsertMPCE(Decimal? PR_P_NO, string FN_BRANCH, string FN_TYPE, Decimal? FN_PAY_SCHED, string FN_FIN_NO, DateTime? FN_START_DATE, DateTime? FN_END_DATE, DateTime? FN_EXTRA_TIME, Decimal? FN_AMT_AVAILED, Decimal? FN_MONTHLY_DED, Decimal? FN_C_RATIO_PER, string FN_LIQUIDATE, string EXCP_FLAG, string EXCP_REM, string FN_SUBTYPE, string FN_MORTG_PROP_ADD, string FN_MORTG_PROP_CITY, Decimal? FN_MORTG_PROP_VALSA, Decimal? FN_MORTG_PROP_VALVR, int? ID)
        // {
        //     try
        //     {
        //         Dictionary<string, object> d = new Dictionary<string, object>();

        //         d.Add("PR_P_NO", PR_P_NO);   
        //d.Add("FN_BRANCH", FN_BRANCH);
        //d.Add("FN_TYPE", FN_TYPE);
        //d.Add("FN_PAY_SCHED", FN_PAY_SCHED);
        //d.Add("FN_FIN_NO", FN_FIN_NO);
        //d.Add("FN_START_DATE", FN_START_DATE);
        //d.Add("FN_END_DATE", FN_END_DATE);
        //d.Add("FN_EXTRA_TIME", FN_EXTRA_TIME);
        //d.Add("FN_AMT_AVAILED", FN_AMT_AVAILED);
        //d.Add("FN_MONTHLY_DED", FN_MONTHLY_DED);
        //d.Add("FN_C_RATIO_PER", FN_C_RATIO_PER);
        //d.Add("FN_LIQUIDATE", FN_LIQUIDATE);
        //d.Add("EXCP_FLAG", EXCP_FLAG);
        //d.Add("EXCP_REM", EXCP_REM);
        //d.Add("FN_SUBTYPE", FN_SUBTYPE);
        //d.Add("FN_MORTG_PROP_ADD", FN_MORTG_PROP_ADD);
        //d.Add("FN_MORTG_PROP_CITY", FN_MORTG_PROP_CITY);
        //d.Add("FN_MORTG_PROP_VALSA", FN_MORTG_PROP_VALSA);
        //         d.Add("FN_MORTG_PROP_VALVR", FN_MORTG_PROP_VALVR);
        //         d.Add("ID", ID);

        //         CmnDataManager objCmnDataManager = new CmnDataManager();

        //         if (ID > 0)
        //         {
        //             Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_FIN_BOOKING_MPC_MANAGER", "Update", d);
        //             if (rsltCode.isSuccessful)
        //             {
        //                 var res = Json(rsltCode);
        //                 return Json("Your request is modify successfully !");
        //             }
        //         }
        //         else
        //         {
        //             Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_FIN_BOOKING_MPC_MANAGER", "Save", d);
        //             if (rsltCode.isSuccessful)
        //             {
        //                 var res = Json(rsltCode);
        //                 return Json("Your request is successfully completed !");
        //             }
        //         }
        //         return Json(null);
        //     }
        //     catch (Exception ex)
        //     {
        //         return Json("Something went wrong");
        //     }
        // }

        [HttpPost]
        public JsonResult UpsertMPCE1(Decimal? PR_P_NO, string FN_FIN_NO, string FN_COLLAT_NO, string FN_PARTY1, string FN_PARTY2, DateTime? FN_COLLAT_DT, DateTime? FN_DOC_RCV_DT, int? ID)
        {
            try
            {
                Dictionary<string, object> d = new Dictionary<string, object>();

                d.Add("PR_P_NO", PR_P_NO);
                d.Add("FN_FIN_NO", FN_FIN_NO);
                d.Add("FN_COLLAT_NO ", FN_COLLAT_NO);
                d.Add("FN_PARTY1", FN_PARTY1);
                d.Add("FN_PARTY2", FN_PARTY2);
                d.Add("FN_COLLAT_DT", FN_COLLAT_DT);
                d.Add("FN_DOC_RCV_DT", FN_DOC_RCV_DT);
                d.Add("ID", ID);

                CmnDataManager objCmnDataManager = new CmnDataManager();

                if (ID > 0)
                {
                    Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_FIN_BOOK_COLLAT_MANAGER", "Update", d);
                    if (rsltCode.isSuccessful)
                    {
                        var res = Json(rsltCode);
                        return Json("Your request is modify successfully !");
                    }
                }
                else
                {
                    Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_FIN_BOOK_COLLAT_MANAGER", "Save", d);
                    if (rsltCode.isSuccessful)
                    {
                        var res = Json(rsltCode);
                        return Json("Your request is successfully completed !");
                    }
                }
                return Json(null);
            }
            catch (Exception ex)
            {
                return Json("Something went wrong");
            }
        }

        #endregion

        #region SelfInsuraneEntry

        public IActionResult SelfInsuraneEntry()
        {
            return View();
        }

        public ActionResult btn_PR_P_NO_SelfInsE()
        {
            Dictionary<string, object> d = new Dictionary<string, object>();

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_FN_INSURANCE_MANAGER", "PersonalLov", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        [HttpPost]
        public JsonResult UpsertSelfInsuraneEntry(Decimal? FNI_P_NO, string FNI_COMP, string FNI_POLICY, string FNI_TYPE, Decimal? FNI_INS_AMT, Decimal? FNI_ANNUAL_RED, Decimal? FNI_ANNUAL_PRE, DateTime? FNI_PAY_DATE, DateTime? FNI_LAST_PAY, string FNI_NOMINATION, string FNI_ASSIG_BANK, int? ID)
        {
            try
            {
                Dictionary<string, object> d = new Dictionary<string, object>();

                d.Add("FNI_P_NO", FNI_P_NO);
                d.Add("FNI_COMP", FNI_COMP);
                d.Add("FNI_POLICY", FNI_POLICY);
                d.Add("FNI_TYPE", FNI_TYPE);
                d.Add("FNI_INS_AMT", FNI_INS_AMT);
                d.Add("FNI_ANNUAL_RED", FNI_ANNUAL_RED);
                d.Add("FNI_ANNUAL_PRE", FNI_ANNUAL_PRE);
                d.Add("FNI_PAY_DATE", FNI_PAY_DATE);
                d.Add("FNI_LAST_PAY", FNI_LAST_PAY);
                d.Add("FNI_NOMINATION", FNI_NOMINATION);
                d.Add("FNI_ASSIG_BANK", FNI_ASSIG_BANK);
                d.Add("ID", ID);

                CmnDataManager objCmnDataManager = new CmnDataManager();

                if (ID > 0)
                {
                    Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_FN_INSURANCE_MANAGER", "Update", d);
                    if (rsltCode.isSuccessful)
                    {
                        var res = Json(rsltCode);
                        return Json("Your request is modify successfully !");
                    }
                }
                else
                {
                    Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_FN_INSURANCE_MANAGER", "Save", d);
                    if (rsltCode.isSuccessful)
                    {
                        var res = Json(rsltCode);
                        return Json("Your request is successfully completed !");
                    }
                }
                return Json(null);
            }
            catch (Exception ex)
            {
                return Json("Something went wrong");
            }
        }

        #endregion

        #region TransferFinanceType

        public IActionResult TransferFinanceType()
        {
            return View();
        }

        public ActionResult btn_FN_TF_Type()
        {
            Dictionary<string, object> d = new Dictionary<string, object>();

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_FN_TRANSFER_FinanceType_MANAGER", "Fin_No", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        public ActionResult btn_FNT_NEW_TYPE(string SearchFilter)
        {
            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("SearchFilter", SearchFilter);
            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_FN_TRANSFER_FinanceType_MANAGER", "NewType", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        [HttpPost]
        public JsonResult UpsertTransferFinanceType(string FNT_FIN_NO, Decimal? FNT_P_NO, DateTime? FNT_DATE, string FNT_OLD_TYPE, string FNT_NEW_TYPE, Decimal? FNT_ADJ_AMT, int? ID)
        {
            try
            {
                Dictionary<string, object> d = new Dictionary<string, object>();

                d.Add("FNT_FIN_NO", FNT_FIN_NO);
                d.Add("FNT_P_NO", FNT_P_NO);
                d.Add("FNT_DATE", FNT_DATE);
                d.Add("FNT_OLD_TYPE", FNT_OLD_TYPE);
                d.Add("FNT_NEW_TYPE", FNT_NEW_TYPE);
                d.Add("FNT_ADJ_AMT", FNT_ADJ_AMT);
                d.Add("ID", ID);

                CmnDataManager objCmnDataManager = new CmnDataManager();

                if (ID > 0)
                {
                    Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_FN_TRANSFER_FinanceType_MANAGER", "Update", d);
                    if (rsltCode.isSuccessful)
                    {
                        var res = Json(rsltCode);
                        return Json("Your request is modify successfully !");
                    }
                }
                else
                {
                    Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_FN_TRANSFER_FinanceType_MANAGER", "Save", d);
                    if (rsltCode.isSuccessful)
                    {
                        var res = Json(rsltCode);
                        return Json("Your request is successfully completed !");
                    }
                }
                return Json(null);
            }
            catch (Exception ex)
            {
                return Json("Something went wrong");
            }
        }

        #endregion

        #region VehicleInsuranceEntry

        public IActionResult VehicleInsuranceEntry()
        {
            return View();
        }

        public ActionResult btn_PR_P_NO_VIE()
        {
            Dictionary<string, object> d = new Dictionary<string, object>();

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_FIN_INSURANCE_MANAGER", "PR_No_Lov", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        public ActionResult btn_FN_FINANCE_NO_VIE()
        {
            Dictionary<string, object> d = new Dictionary<string, object>();

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_FIN_INSURANCE_MANAGER", "Fin_Lov", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        public ActionResult Fin_desc(string fn_type)
        {
            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("fn_type", fn_type);
            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_FIN_INSURANCE_MANAGER", "Fin_desc", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        public ActionResult Execute_Query(string FN_FINANCE_NO)
        {
            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("FN_FINANCE_NO", FN_FINANCE_NO);
            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_FIN_INSURANCE_MANAGER", "Execute_Query", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        [HttpPost]
        public JsonResult UpsertVehicleInsuranceEntry(Decimal? PR_P_NO, string FN_FINANCE_NO, string FN_POLICY_NO, string FN_INSUR_COMP, DateTime? FN_POLICY_ISS_DATE, DateTime? FN_POLICY_EXP_DATE, string FN_VEHC_REG_NO, string FN_VEHC_ENG_NO, string FN_VEHC_CHS_NO, string FN_VEHC_MAKE, string FN_VEHC_MODEL, int? ID)
        {
            try
            {
                Dictionary<string, object> d = new Dictionary<string, object>();

                d.Add("PR_P_NO", PR_P_NO);
                d.Add("FN_FINANCE_NO", FN_FINANCE_NO);
                d.Add("FN_POLICY_NO", FN_POLICY_NO);
                d.Add("FN_INSUR_COMP", FN_INSUR_COMP);
                d.Add("FN_POLICY_ISS_DATE", FN_POLICY_ISS_DATE);
                d.Add("FN_POLICY_EXP_DATE", FN_POLICY_EXP_DATE);
                d.Add("FN_VEHC_REG_NO", FN_VEHC_REG_NO);
                d.Add("FN_VEHC_ENG_NO", FN_VEHC_ENG_NO);
                d.Add("FN_VEHC_CHS_NO", FN_VEHC_CHS_NO);
                d.Add("FN_VEHC_MAKE", FN_VEHC_MAKE);
                d.Add("FN_VEHC_MODEL", FN_VEHC_MODEL);
                d.Add("ID", ID);

                CmnDataManager objCmnDataManager = new CmnDataManager();

                if (ID > 0)
                {
                    Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_FIN_INSURANCE_MANAGER", "Update", d);
                    if (rsltCode.isSuccessful)
                    {
                        var res = Json(rsltCode);
                        return Json("Your request is modify successfully !");
                    }
                }
                else
                {
                    Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_FIN_INSURANCE_MANAGER", "Save", d);
                    if (rsltCode.isSuccessful)
                    {
                        var res = Json(rsltCode);
                        return Json("Your request is successfully completed !");
                    }
                }
                return Json(null);
            }
            catch (Exception ex)
            {
                return Json("Something went wrong");
            }
        }

        #endregion

        public IActionResult View_FinanceBooking()
        {
            return View();
        }
    }
}
