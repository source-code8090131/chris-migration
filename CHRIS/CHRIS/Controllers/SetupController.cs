﻿using CHRIS.Common;
using CHRIS.Data;
using CHRIS.Models;
using CHRIS.Services;
using DocumentFormat.OpenXml.Office2010.Excel;
using DocumentFormat.OpenXml.Office2016.Excel;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Data;
using System.Runtime.CompilerServices;

namespace CHRIS.Controllers
{
    public class SetupController : Controller
    {
        public IActionResult AccFileList()
        {
            return View();
        }

        #region AccountEntry

        public ActionResult AccountEntry()
        {
            return View();
        }

        public JsonResult tbl_UNAUTH_AccountEntry()
        {
            string SessionUser = HttpContext.Session.GetString("USER_ID").ToString();

            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("MakerId", SessionUser);

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_AccEnt_ACCOUNT_MANAGER", "GET_UNAUTHORIZE_AccountEntry", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        public JsonResult tbl_AUTH_AccountEntry()
        {
            string SessionUser = HttpContext.Session.GetString("USER_ID").ToString();

            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("MakerId", SessionUser);

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_AccEnt_ACCOUNT_MANAGER", "List", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        public JsonResult AuthorizationFor_AccountEntry(string Action, int ID)
        {
            string SessionUser = HttpContext.Session.GetString("USER_ID").ToString();
            Dictionary<string, object> d = new Dictionary<string, object>();
            if (Action == "Authorize")
            {
                d.Add("CheckerkID", SessionUser);
                d.Add("UpdatedDatetime", DateTime.Now);
                d.Add("ID", ID);
                CmnDataManager objCmnDataManager = new CmnDataManager();
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_AccEnt_ACCOUNT_MANAGER", "AUTHORIZE_AccountEntry", d);
                if (rsltCode.isSuccessful)
                    return Json("Record authorized successfully.");
                else
                    return Json("Problem while authorizing data");
            }
            else if (Action == "Reject")
            {
                d.Add("ID", ID);
                CmnDataManager objCmnDataManager = new CmnDataManager();
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_AccEnt_ACCOUNT_MANAGER", "REJECT_AccountEntry", d);
                if (rsltCode.isSuccessful)
                    return Json("Record rejected successfully.");
                else
                    return Json("Problem while recjecting data");

            }
            return Json(null);
        }


        [HttpPost]
        public JsonResult UpsertAccountEntry(string SP_ACC_NO, string SP_ACC_DESC, string SP_ACC_DESC1, string SP_ACC_TYPE, string SP_ACC_GROUP, Decimal? SP_ACC_AMOUNT, int ID)
        {
            try
            {
                Dictionary<string, object> d = new Dictionary<string, object>();

                d.Add("SP_ACC_NO", SP_ACC_NO);
                d.Add("SP_ACC_DESC", SP_ACC_DESC);
                d.Add("SP_ACC_DESC1", SP_ACC_DESC1);
                d.Add("SP_ACC_TYPE", SP_ACC_TYPE);
                d.Add("SP_ACC_GROUP", SP_ACC_GROUP);
                d.Add("SP_ACC_AMOUNT", SP_ACC_AMOUNT);
                d.Add("ID", ID);
                d.Add("MakerId", HttpContext.Session.GetString("USER_ID").ToString());
                d.Add("AddedDatetime", DateTime.Now);
                d.Add("IsAuth", false);

                CmnDataManager objCmnDataManager = new CmnDataManager();
                if (ID > 0)
                {

                    Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_AccEnt_ACCOUNT_MANAGER", "Update", d);
                    if (rsltCode.isSuccessful)
                    {
                        var res = Json(rsltCode);
                        return Json(res);
                    }
                }
                else
                {
                    Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_AccEnt_ACCOUNT_MANAGER", "Save", d);
                    if (rsltCode.isSuccessful)
                    {
                        var res = Json(rsltCode);
                        return Json(res);
                    }
                }

                return Json(null);
            }
            catch
            {
                return Json(null);
            }
        }

        public ViewResponseModel DeleteAccountEntry(AccountEntryModel model)
        {
            return AccountEntryService.getInstance().DeleteAccountEntry(model);
        }

        #endregion

        #region AllowanceEnter

        public ActionResult AllowanceEnter()
        {
            return View();
        }

        public JsonResult tbl_UNAUTH_AllowanceEnter()
        {
            string SessionUser = HttpContext.Session.GetString("USER_ID").ToString();

            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("MakerId", SessionUser);

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_SETUP_ALLOWANCE_MANAGER", "GET_UNAUTHORIZE_ALLOWANCE", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        public JsonResult tbl_AUTH_AllowanceEnter()
        {
            string SessionUser = HttpContext.Session.GetString("USER_ID").ToString();

            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("MakerId", SessionUser);

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_SETUP_ALLOWANCE_MANAGER", "List", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        public JsonResult AuthorizationFor_AllowanceEnter(string Action, int ID)
        {
            string SessionUser = HttpContext.Session.GetString("USER_ID").ToString();
            Dictionary<string, object> d = new Dictionary<string, object>();
            if (Action == "Authorize")
            {
                d.Add("CheckerkID", SessionUser);
                d.Add("UpdatedDatetime", DateTime.Now);
                d.Add("ID", ID);
                CmnDataManager objCmnDataManager = new CmnDataManager();
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_SETUP_ALLOWANCE_MANAGER", "AUTHORIZE_ALLOWANCE", d);
                if (rsltCode.isSuccessful)
                    return Json("Record authorized successfully.");
                else
                    return Json("Problem while authorizing data");
            }
            else if (Action == "Reject")
            {
                d.Add("ID", ID);
                CmnDataManager objCmnDataManager = new CmnDataManager();
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_SETUP_ALLOWANCE_MANAGER", "REJECT_ALLOWANCE", d);
                if (rsltCode.isSuccessful)
                    return Json("Record rejected successfully.");
                else
                    return Json("Problem while recjecting data");

            }
            return Json(null);
        }

        public JsonResult btn_tbl_Branch_A()
        {

            Dictionary<string, object> d = new Dictionary<string, object>();

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_SETUP_ALLOWANCE_MANAGER", "BranchCode_LOV", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];


                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        public JsonResult btn_tbl_Designation_Modal()
        {

            Dictionary<string, object> d = new Dictionary<string, object>();

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_SETUP_ALLOWANCE_MANAGER", "Designation_LOV", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];


                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        public JsonResult btn_tbl_Level_Modal(string SEARCHFILTER)
        {

            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("SEARCHFILTER", SEARCHFILTER);

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_SETUP_ALLOWANCE_MANAGER", "LEVEL_LOV", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];


                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        public JsonResult btn_tbl_Category_Modal()
        {

            Dictionary<string, object> d = new Dictionary<string, object>();

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_SETUP_ALLOWANCE_MANAGER", "CATGORY_LOV", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];


                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        public JsonResult btn_tbl_Acc_Desc_Modal()
        {

            Dictionary<string, object> d = new Dictionary<string, object>();

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_SETUP_ALLOWANCE_MANAGER", "Account_LOV", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];


                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        [HttpPost]
        public JsonResult UpsertAllowanceEnter(string? SP_ALL_CODE, string? SP_BRANCH_A, string? SP_DESC, string? SP_CATEGORY_A, string? SP_DESG_A, string? SP_LEVEL_A, DateTime? SP_VALID_FROM, DateTime? SP_VALID_TO, decimal? SP_ALL_AMOUNT, decimal? SP_PER_ASR, string? SP_TAX, string? SP_ATT_RELATED, string? SP_RELATED_LEAV, string? SP_ASR_BASIC, string? SP_MEAL_CONV, string? SP_ALL_IND, string? SP_ACOUNT_NO, string? SP_FORECAST_TAX, int ID)
        {
            try
            {
                Dictionary<string, object> d = new Dictionary<string, object>();

                d.Add("SP_ALL_CODE", SP_ALL_CODE);
                d.Add("SP_BRANCH_A", SP_BRANCH_A);
                d.Add("SP_DESC", SP_DESC);
                d.Add("SP_CATEGORY_A", SP_CATEGORY_A);
                d.Add("SP_DESG_A", SP_DESG_A);
                d.Add("SP_LEVEL_A", SP_LEVEL_A);
                d.Add("SP_VALID_FROM", SP_VALID_FROM);
                d.Add("SP_VALID_TO", SP_VALID_TO);
                d.Add("SP_ALL_AMOUNT", SP_ALL_AMOUNT);
                d.Add("SP_PER_ASR", SP_PER_ASR);
                d.Add("SP_TAX", SP_TAX);
                d.Add("SP_ATT_RELATED", SP_ATT_RELATED);
                d.Add("SP_RELATED_LEAV", SP_RELATED_LEAV);
                d.Add("SP_ASR_BASIC", SP_ASR_BASIC);
                d.Add("SP_MEAL_CONV", SP_MEAL_CONV);
                d.Add("SP_ALL_IND", SP_ALL_IND);
                d.Add("SP_ACOUNT_NO", SP_ACOUNT_NO);
                d.Add("SP_FORECAST_TAX", SP_FORECAST_TAX);
                d.Add("ID", ID);
                d.Add("MakerId", HttpContext.Session.GetString("USER_ID").ToString());
                d.Add("AddedDatetime", DateTime.Now);
                d.Add("IsAuth", false);

                CmnDataManager objCmnDataManager = new CmnDataManager();
                if (ID > 0)
                {

                    Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_SETUP_ALLOWANCE_MANAGER", "Update", d);
                    if (rsltCode.isSuccessful)
                    {

                        var res = Json(rsltCode);

                        return Json(res);

                    }
                }
                else
                {
                    Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_SETUP_ALLOWANCE_MANAGER", "Save", d);
                    if (rsltCode.isSuccessful)
                    {

                        var res = Json(rsltCode);

                        return Json(res);

                    }
                }

                return Json(null);
            }
            catch (Exception ex)
            {

                throw;
            }
        }


        public ViewResponseModel DeleteAllowanceEnter(AllowanceEnterModel model)
        {
            return AllowanceEnterService.getInstance().DeleteAllowanceEnter(model);
        }

        #endregion
        public IActionResult AllowanceEnter_Details()
        {
            return View();
        }
        public IActionResult AllowanceEntry_MasterDetail()
        {
            return View();
        }

        #region AttritionCategoryEntry
        public ActionResult AttritionCategoryEntry()
        {
            return View();
        }

        public JsonResult tbl_UNAUTH_ATT_CAT_ENT()
        {
            string SessionUser = HttpContext.Session.GetString("USER_ID").ToString();

            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("MakerId", SessionUser);

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_Setup_ATTR_CATEGORY_MANAGER", "GET_UNAUTHORIZE_ATTR_CATE", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        public JsonResult tbl_AUTH_ATT_CAT_ENT()
        {
            string SessionUser = HttpContext.Session.GetString("USER_ID").ToString();

            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("MakerId", SessionUser);

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_Setup_ATTR_CATEGORY_MANAGER", "List", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        public JsonResult AuthorizationFor_ATT_CAT_ENT(string Action, int ID)
        {
            string SessionUser = HttpContext.Session.GetString("USER_ID").ToString();
            Dictionary<string, object> d = new Dictionary<string, object>();
            if (Action == "Authorize")
            {
                d.Add("CheckerkID", SessionUser);
                d.Add("UpdatedDatetime", DateTime.Now);
                d.Add("ID", ID);
                CmnDataManager objCmnDataManager = new CmnDataManager();
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_Setup_ATTR_CATEGORY_MANAGER", "AUTHORIZE_ATTR_CATE", d);
                if (rsltCode.isSuccessful)
                    return Json("Record authorized successfully.");
                else
                    return Json("Problem while authorizing data");
            }
            else if (Action == "Reject")
            {
                d.Add("ID", ID);
                CmnDataManager objCmnDataManager = new CmnDataManager();
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_Setup_ATTR_CATEGORY_MANAGER", "REJECT_ATTR_CATE", d);
                if (rsltCode.isSuccessful)
                    return Json("Record rejected successfully.");
                else
                    return Json("Problem while recjecting data");

            }
            return Json(null);
        }


        [HttpPost]
        public ActionResult UpsertAttritionCategoryEntry(Decimal? ATTR_CATEGORY_CODE, string ATTR_CATEGORY_DESC, int? ID)
        {
            try
            {
                Dictionary<string, object> d = new Dictionary<string, object>();

                d.Add("ATTR_CATEGORY_CODE", ATTR_CATEGORY_CODE);
                d.Add("ATTR_CATEGORY_DESC", ATTR_CATEGORY_DESC);
                d.Add("MakerId", HttpContext.Session.GetString("USER_ID").ToString());
                d.Add("AddedDatetime", DateTime.Now);
                d.Add("IsAuth", false);
                d.Add("ID", ID);

                CmnDataManager objCmnDataManager = new CmnDataManager();
                if (ID > 0)
                {

                    Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_Setup_ATTR_CATEGORY_MANAGER", "Update", d);
                    if (rsltCode.isSuccessful)
                    {
                        return Json("Your request is successfully completed !");
                    }
                }
                else
                {
                    Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_Setup_ATTR_CATEGORY_MANAGER", "Save", d);
                    if (rsltCode.isSuccessful)
                    {
                        return Json("Your request is successfully completed !");
                    }
                }

                return Json(null);
            }
            catch
            {
                return Json(null);
            }
        }

        public ViewResponseModel DeleteAttritionCategoryEntry(AttritionCategoryEntryModel model)
        {
            return AttritionCategoryEntryService.getInstance().DeleteAttritionCategoryEntry(model);
        }

        #endregion

        #region AttritionTypeEntry
        public ActionResult AttritionTypeEntry()
        {
            var AttritionTypeEntryoutput = AttritionTypeEntryService.getInstance().GetAllAttritionTypeEntry();
            return View(AttritionTypeEntryoutput);
        }



        #endregion

        #region BenchMarkRateEntry
        public IActionResult BenchMarkRateEntry()
        {
            return View();
        }
        public ActionResult btn_SearchDate(DateTime? BEN_DATE)
        {
            #region Variables
            Dictionary<string, object> colsNVals = new Dictionary<string, object>();
            DataTable dtBMR = new DataTable();
            Result rsltMain;
            CmnDataManager cmnDM = new CmnDataManager();
            Double refVar = 0;
            string MessageBox = "";
            #endregion

            if (BEN_DATE != null)
            {

                colsNVals.Clear();
                colsNVals.Add("BEN_DATE", BEN_DATE);

                rsltMain = cmnDM.GetData("CHRIS_SP_BENCHMARK_MANAGER", "List", colsNVals);
                if (rsltMain.isSuccessful)
                {
                    if (rsltMain.dstResult.ExtendedProperties.Count > 0 || rsltMain.dstResult.Tables.Count > 0)
                    {
                        dtBMR = rsltMain.dstResult.Tables[0];
                        if (dtBMR.Rows.Count > 0)
                        {
                            var CatResul = JsonConvert.SerializeObject(dtBMR);
                            return Json(CatResul);
                        }
                        else
                        {
                            // colsNVals.Clear();
                            rsltMain = cmnDM.GetData("CHRIS_SP_BENCHMARK_MANAGER", "PRC01", colsNVals);
                            if (rsltMain.isSuccessful)
                            {
                                dtBMR = rsltMain.dstResult.Tables[0];
                                if (dtBMR.Rows.Count > 0)
                                {
                                    var CatResul = JsonConvert.SerializeObject(dtBMR);
                                    return Json(CatResul);
                                }
                                else
                                {
                                    return Json(null);
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                return Json(null);
            }

            return Json(null);
        }
        [HttpPost]
        public ActionResult UpsertBenchMarkRateEntry(int? ID, string BEN_FIN_TYPE, DateTime? BEN_DATE, string BEN_RATE)
        {
            try
            {
                Dictionary<string, object> d = new Dictionary<string, object>();

                d.Add("BEN_RATE", BEN_RATE);
                d.Add("BEN_DATE", BEN_DATE);
                d.Add("BEN_FIN_TYPE", BEN_FIN_TYPE);
                d.Add("ID", ID);
                d.Add("MakerId", HttpContext.Session.GetString("USER_ID").ToString());
                d.Add("AddedDatetime", DateTime.Now);
                d.Add("UpdatedDatetime", DateTime.Now);
                d.Add("IsAuth", false);
                CmnDataManager objCmnDataManager = new CmnDataManager();
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_BENCHMARK_MANAGER", "Update", d);
                if (rsltCode.isSuccessful)
                {
                    var res = Json(rsltCode);
                    return Json("Your request is modify successfully !");
                }
                return Json(null);
            }
            catch (Exception ex)
            {
                return Json(ex.Message + Environment.NewLine + " Something went wrong");
            }
        }
        [HttpPost]
        public JsonResult AuthorizationForBenchmarkData(string Action, int ID)
        {
            string SessionUser = HttpContext.Session.GetString("USER_ID").ToString();
            Dictionary<string, object> d = new Dictionary<string, object>();
            if (Action == "Authorize")
            {
                d.Add("CheckerkID", SessionUser);
                d.Add("UpdatedDatetime", DateTime.Now);
                d.Add("ID", ID);
                CmnDataManager objCmnDataManager = new CmnDataManager();
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_BENCHMARK_MANAGER", "AUTHORIZE_BENCHMARK", d);
                if (rsltCode.isSuccessful)
                    return Json("Record authorized successfully.");
                else
                    return Json("Problem while authorizing data");
            }
            else if (Action == "Reject")
            {
                d.Add("ID", ID);
                CmnDataManager objCmnDataManager = new CmnDataManager();
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_BENCHMARK_MANAGER", "REJECT_BENCHMARK", d);
                if (rsltCode.isSuccessful)
                    return Json("Record rejected successfully.");
                else
                    return Json("Problem while recjecting data");

            }
            return Json(null);
        }
        [HttpPost]
        public JsonResult GetBenchmarkUnAuthorizedData()
        {
            string SessionUser = HttpContext.Session.GetString("USER_ID").ToString();
            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("MakerId", SessionUser);
            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_BENCHMARK_MANAGER", "GET_UNAUTHORIZE_BENCHMARK", d);
            if (rsltCode.isSuccessful)
            {
                if (rsltCode.dstResult.Tables.Count > 0)
                {
                    if (rsltCode.dstResult.Tables[0].Rows.Count > 0)
                    {
                        DataTable firstTable = rsltCode.dstResult.Tables[0];
                        var CatResul = JsonConvert.SerializeObject(firstTable);
                        return Json(CatResul);
                    }
                }
            }
            return Json(null);
        }
        public ActionResult DeleteDescripencies(int? ID)
        {
            try
            {
                Dictionary<string, object> d = new Dictionary<string, object>();

                d.Add("ID", ID);

                CmnDataManager objCmnDataManager = new CmnDataManager();

                if (ID > 0)
                {
                    Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_BENCHMARK_MANAGER", "Delete", d);
                    if (rsltCode.isSuccessful)
                    {
                        var res = Json(rsltCode);
                        return Json("Your request is successfully Deleted !");
                    }
                }
                else
                {
                    return Json(null);
                }
                return Json(null);
            }
            catch (Exception ex)
            {
                return Json("Something went wrong");
            }
            return Json(null);
        }
        #endregion

        #region BranchSetup
        public ActionResult BranchSetup()
        {
            return View();
        }

        public JsonResult tbl_UNAUTH_BranchSetup()
        {
            string SessionUser = HttpContext.Session.GetString("USER_ID").ToString();

            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("MakerId", SessionUser);

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_BRANCH_MANAGER", "GET_UNAUTHORIZE_BRANCH", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        public JsonResult tbl_AUTH_BranchSetup()
        {
            string SessionUser = HttpContext.Session.GetString("USER_ID").ToString();

            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("MakerId", SessionUser);

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_BRANCH_MANAGER", "List", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        public JsonResult AuthorizationFor_BranchSetup(string Action, int ID)
        {
            string SessionUser = HttpContext.Session.GetString("USER_ID").ToString();
            Dictionary<string, object> d = new Dictionary<string, object>();
            if (Action == "Authorize")
            {
                d.Add("CheckerkID", SessionUser);
                d.Add("UpdatedDatetime", DateTime.Now);
                d.Add("ID", ID);
                CmnDataManager objCmnDataManager = new CmnDataManager();
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_BRANCH_MANAGER", "AUTHORIZE_BRANCH", d);
                if (rsltCode.isSuccessful)
                    return Json("Record authorized successfully.");
                else
                    return Json("Problem while authorizing data");
            }
            else if (Action == "Reject")
            {
                d.Add("ID", ID);
                CmnDataManager objCmnDataManager = new CmnDataManager();
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_BRANCH_MANAGER", "REJECT_BRANCH", d);
                if (rsltCode.isSuccessful)
                    return Json("Record rejected successfully.");
                else
                    return Json("Problem while recjecting data");

            }
            return Json(null);
        }

        [HttpPost]
        public ActionResult UpsertBranchSetup(string BRN_CODE, string BRN_NAME, string BRN_ADD1, string BRN_ADD2, string BRN_ADD3, string BRN_CR_AC, string BRN_DR_AC, int? ID)
        {
            try
            {
                Dictionary<string, object> d = new Dictionary<string, object>();

                d.Add("BRN_CODE", BRN_CODE);
                d.Add("BRN_NAME", BRN_NAME);
                d.Add("BRN_ADD1", BRN_ADD1);
                d.Add("BRN_ADD2", BRN_ADD2);
                d.Add("BRN_ADD3", BRN_ADD3);
                d.Add("BRN_CR_AC", BRN_CR_AC);
                d.Add("BRN_DR_AC", BRN_DR_AC);
                d.Add("MakerId", HttpContext.Session.GetString("USER_ID").ToString());
                d.Add("AddedDatetime", DateTime.Now);
                d.Add("IsAuth", false);
                d.Add("ID", ID);

                CmnDataManager objCmnDataManager = new CmnDataManager();
                if (ID > 0)
                {

                    Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_BRANCH_MANAGER", "Update", d);
                    if (rsltCode.isSuccessful)
                    {
                        return Json("Your request is successfully completed !");
                    }
                }
                else
                {
                    Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_BRANCH_MANAGER", "Save", d);
                    if (rsltCode.isSuccessful)
                    {
                        return Json("Your request is successfully completed !");
                    }
                }

                return Json(null);
            }
            catch
            {
                return Json(null);
            }
        }

        public ViewResponseModel DeleteBranchSetup(BranchSetupModel model)
        {
            return BranchSetupService.getInstance().DeleteBranchSetup(model);
        }

        #endregion

        #region Category_Entry

        public IActionResult Category_Entry()
        {
            return View();
        }

        public JsonResult tbl_UNAUTH_Cat_Ent()
        {
            string SessionUser = HttpContext.Session.GetString("USER_ID").ToString();

            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("MakerId", SessionUser);

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_CATEGORY_MANAGER", "GET_UNAUTHORIZE_SP_CATEGORY", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        public JsonResult AuthorizationFor_Cat_Ent(string Action, int ID)
        {
            string SessionUser = HttpContext.Session.GetString("USER_ID").ToString();
            Dictionary<string, object> d = new Dictionary<string, object>();
            if (Action == "Authorize")
            {
                d.Add("CheckerkID", SessionUser);
                d.Add("UpdatedDatetime", DateTime.Now);
                d.Add("ID", ID);
                CmnDataManager objCmnDataManager = new CmnDataManager();
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_CATEGORY_MANAGER", "AUTHORIZE_SP_CATEGORY", d);
                if (rsltCode.isSuccessful)
                    return Json("Record authorized successfully.");
                else
                    return Json("Problem while authorizing data");
            }
            else if (Action == "Reject")
            {
                d.Add("ID", ID);
                CmnDataManager objCmnDataManager = new CmnDataManager();
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_CATEGORY_MANAGER", "REJECT_SP_CATEGORY", d);
                if (rsltCode.isSuccessful)
                    return Json("Record rejected successfully.");
                else
                    return Json("Problem while recjecting data");

            }
            return Json(null);
        }

        public JsonResult tbl_AUTH_Cat_Ent()
        {
            string SessionUser = HttpContext.Session.GetString("USER_ID").ToString();

            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("MakerId", SessionUser);

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_CATEGORY_MANAGER", "List", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        [HttpPost]
        public ActionResult UpsertCategory_Entry(string SP_CAT_CODE, string SP_DESC, int ID)
        {
            try
            {
                Dictionary<string, object> d = new Dictionary<string, object>();

                d.Add("SP_CAT_CODE", SP_CAT_CODE);
                d.Add("SP_DESC", SP_DESC);
                d.Add("MakerId", HttpContext.Session.GetString("USER_ID").ToString());
                d.Add("AddedDatetime", DateTime.Now);
                d.Add("IsAuth", false);
                d.Add("ID", ID);

                CmnDataManager objCmnDataManager = new CmnDataManager();
                if (ID > 0)
                {

                    Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_CATEGORY_MANAGER", "Update", d);
                    if (rsltCode.isSuccessful)
                    {
                        return Json("Your request is successfully completed !");
                    }
                }
                else
                {
                    Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_CATEGORY_MANAGER", "Save", d);
                    if (rsltCode.isSuccessful)
                    {
                        return Json("Your request is successfully completed !");
                    }
                }

                return Json(null);
            }
            catch
            {
                return Json(null);
            }

        }

        public JsonResult DeleteCategory_Entry(int ID)
        {
            //string SessionUser = HttpContext.Session.GetString("USER_ID").ToString();

            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("ID", ID);

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_CATEGORY_MANAGER", "Delete", d);
            if (rsltCode.isSuccessful)
            {

                return Json("Record deleted successfully");
            }
            return Json(null);
        }


        #endregion
        public IActionResult CategoryEntry()
        {
            return View();
        }
        public IActionResult CategoryFileList()
        {
            return View();
        }

        #region CompensationEntry

        public ActionResult CompensationEntry()
        {
            return View();
        }

        public JsonResult tbl_CompensationEntry()
        {
            string SessionUser = HttpContext.Session.GetString("USER_ID").ToString();

            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("MakerId", SessionUser);
            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_CompEnt_COMP_MANAGER", "List", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        public JsonResult tbl_UNAUTH_CompensationEntry()
        {
            string SessionUser = HttpContext.Session.GetString("USER_ID").ToString();

            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("MakerId", SessionUser);

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_CompEnt_COMP_MANAGER", "GET_UNAUTHORIZE_COMP", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        public JsonResult AuthorizationFor_CompensationEntry(string Action, int ID)
        {
            string SessionUser = HttpContext.Session.GetString("USER_ID").ToString();
            Dictionary<string, object> d = new Dictionary<string, object>();
            if (Action == "Authorize")
            {
                d.Add("CheckerkID", SessionUser);
                d.Add("UpdatedDatetime", DateTime.Now);
                d.Add("ID", ID);
                CmnDataManager objCmnDataManager = new CmnDataManager();
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_CompEnt_COMP_MANAGER", "AUTHORIZE_COMP", d);
                if (rsltCode.isSuccessful)
                    return Json("Record authorized successfully.");
                else
                    return Json("Problem while authorizing data");
            }
            else if (Action == "Reject")
            {
                d.Add("ID", ID);
                CmnDataManager objCmnDataManager = new CmnDataManager();
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_CompEnt_COMP_MANAGER", "REJECT_COMP", d);
                if (rsltCode.isSuccessful)
                    return Json("Record rejected successfully.");
                else
                    return Json("Problem while recjecting data");

            }
            return Json(null);
        }

        public JsonResult btn_GRHD_NO()
        {
            Dictionary<string, object> d = new Dictionary<string, object>();

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_GROUP_HEAD_GROUP_HEAD_MANAGER", "Group_Head", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        public JsonResult tbl_AUTH_comp_Ent_GH()
        {
            string SessionUser = HttpContext.Session.GetString("USER_ID").ToString();

            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("MakerId", SessionUser);

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_GROUP_HEAD_GROUP_HEAD_MANAGER", "List", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        public JsonResult AuthorizationFor_comp_Ent_GH(string Action, int ID)
        {
            string SessionUser = HttpContext.Session.GetString("USER_ID").ToString();
            Dictionary<string, object> d = new Dictionary<string, object>();
            if (Action == "Authorize")
            {
                d.Add("CheckerkID", SessionUser);
                d.Add("UpdatedDatetime", DateTime.Now);
                d.Add("ID", ID);
                CmnDataManager objCmnDataManager = new CmnDataManager();
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_GROUP_HEAD_GROUP_HEAD_MANAGER", "AUTHORIZE_GROUP_HEAD", d);
                if (rsltCode.isSuccessful)
                    return Json("Record authorized successfully.");
                else
                    return Json("Problem while authorizing data");
            }
            else if (Action == "Reject")
            {
                d.Add("ID", ID);
                CmnDataManager objCmnDataManager = new CmnDataManager();
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_GROUP_HEAD_GROUP_HEAD_MANAGER", "REJECT_GROUP_HEAD", d);
                if (rsltCode.isSuccessful)
                    return Json("Record rejected successfully.");
                else
                    return Json("Problem while recjecting data");

            }
            return Json(null);
        }

        public JsonResult tbl_UNAUTH_comp_Ent_GH()
        {
            string SessionUser = HttpContext.Session.GetString("USER_ID").ToString();

            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("MakerId", SessionUser);

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_GROUP_HEAD_GROUP_HEAD_MANAGER", "GET_UNAUTHORIZE_GROUP_HEAD", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        public JsonResult Group_Exist(decimal? GRHD_NO)
        {
            string message = "";

            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("GRHD_NO", GRHD_NO);
            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_GROUP_HEAD_GROUP_HEAD_MANAGER", "Group_Exist", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                if (firstTable.Rows.Count > 0)
                {
                    message = "Group Head Code Already Exist Enter Unique GRoup Head Code";
                    var res = new
                    {
                        message = message,
                    };
                    return Json(res);

                }
                else
                {
                    var res1 = new
                    {
                        message = message,
                    };
                    return Json(res1);
                }


            }
            return Json(null);
        }

        [HttpPost]
        public ActionResult UpsertCompensationEntry(string LVL_CODE, string GRPHD, Decimal? MIN_SAL, Decimal? MAX_SAL, Decimal? MID_SAL, Decimal? BDGT_PRICE, Decimal? ANL_DEPC, Decimal? PTRL_LTR, Decimal? PTRL_RATE, Decimal? INS_RATE, Decimal? MNTNC_AMT, Decimal? DRVR_COST, Decimal? GRPHD_ALL, Decimal? BO_YEAR, int ID)
        {
            try
            {
                Dictionary<string, object> d = new Dictionary<string, object>();

                d.Add("LVL_CODE", LVL_CODE);
                d.Add("GRPHD", GRPHD);
                d.Add("MIN_SAL", MIN_SAL);
                d.Add("MAX_SAL", MAX_SAL);
                d.Add("MID_SAL", MID_SAL);
                d.Add("BDGT_PRICE", BDGT_PRICE);
                d.Add("ANL_DEPC", ANL_DEPC);
                d.Add("PTRL_LTR", PTRL_LTR);
                d.Add("PTRL_RATE", PTRL_RATE);
                d.Add("INS_RATE", INS_RATE);
                d.Add("MNTNC_AMT", MNTNC_AMT);
                d.Add("DRVR_COST", DRVR_COST);
                d.Add("GRPHD_ALL", GRPHD_ALL);
                d.Add("BO_YEAR", BO_YEAR);
                d.Add("ID", ID);
                d.Add("MakerId", HttpContext.Session.GetString("USER_ID").ToString());
                d.Add("AddedDatetime", DateTime.Now);
                d.Add("IsAuth", false);

                CmnDataManager objCmnDataManager = new CmnDataManager();
                if (ID > 0)
                {

                    Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_CompEnt_COMP_MANAGER", "Update", d);
                    if (rsltCode.isSuccessful)
                    {
                        var res = Json(rsltCode);
                        return Json(res);
                    }
                }
                else
                {
                    Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_CompEnt_COMP_MANAGER", "Save", d);
                    if (rsltCode.isSuccessful)
                    {
                        var res = Json(rsltCode);
                        return Json(res);
                    }
                }
                return Json(null);
            }
            catch
            {
                return Json(null);
            }
        }

        [HttpPost]
        public ActionResult Submit_GroupHead(decimal? GRHD_NO, int ID)
        {
            try
            {
                Dictionary<string, object> d = new Dictionary<string, object>();

                d.Add("GRHD_NO", GRHD_NO);
                d.Add("ID", ID);
                d.Add("MakerId", HttpContext.Session.GetString("USER_ID").ToString());
                d.Add("AddedDatetime", DateTime.Now);
                d.Add("IsAuth", false);

                CmnDataManager objCmnDataManager = new CmnDataManager();
                if (ID > 0)
                {

                    Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_GROUP_HEAD_GROUP_HEAD_MANAGER", "Update", d);
                    if (rsltCode.isSuccessful)
                    {

                        var res = Json(rsltCode);

                        return Json(res);

                    }
                }
                else
                {
                    Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_GROUP_HEAD_GROUP_HEAD_MANAGER", "Save", d);
                    if (rsltCode.isSuccessful)
                    {

                        var res = Json(rsltCode);

                        return Json(res);

                    }
                }

                return Json(null);
            }
            catch
            {
                return Json(null);
            }
        }

        public ViewResponseModel DeleteCompensationEntry(CompensationEntryModel model)
        {
            return CompensationEntryService.getInstance().DeleteCompensationEntry(model);
        }

        #endregion

        #region CompensationEntry_GroupHead

        public ActionResult CompensationEntry_GroupHead()
        {
            return View();
        }

        public ViewResponseModel DeleteCompensationEntry_GroupHead(CompensationEntryModel model)
        {
            return CompensationEntryService.getInstance().DeleteCompensationEntry_GroupHead(model);
        }

        #endregion

        #region ContractorCodeEntry

        public ActionResult ContractorCodeEntry()
        {
            return View();
        }

        public JsonResult AuthorizationFor_Contract_Code_Ent(string Action, int ID)
        {
            string SessionUser = HttpContext.Session.GetString("USER_ID").ToString();
            Dictionary<string, object> d = new Dictionary<string, object>();
            if (Action == "Authorize")
            {
                d.Add("CheckerkID", SessionUser);
                d.Add("UpdatedDatetime", DateTime.Now);
                d.Add("ID", ID);
                CmnDataManager objCmnDataManager = new CmnDataManager();
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_ConCod_CONTRACTER_MANAGER", "AUTHORIZE_Contract_Code_Ent", d);
                if (rsltCode.isSuccessful)
                    return Json("Record authorized successfully.");
                else
                    return Json("Problem while authorizing data");
            }
            else if (Action == "Reject")
            {
                d.Add("ID", ID);
                CmnDataManager objCmnDataManager = new CmnDataManager();
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_ConCod_CONTRACTER_MANAGER", "REJECT_Contract_Code_Ent", d);
                if (rsltCode.isSuccessful)
                    return Json("Record rejected successfully.");
                else
                    return Json("Problem while recjecting data");

            }
            return Json(null);
        }

        public JsonResult tbl_UNAUTH_Contract_Code_Ent()
        {
            string SessionUser = HttpContext.Session.GetString("USER_ID").ToString();

            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("MakerId", SessionUser);

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_ConCod_CONTRACTER_MANAGER", "GET_UNAUTHORIZE_Contract_Code_Ent", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        public JsonResult tbl_AUTH_Contract_Code_Ent()
        {
            string SessionUser = HttpContext.Session.GetString("USER_ID").ToString();

            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("MakerId", SessionUser);

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_ConCod_CONTRACTER_MANAGER", "List", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        [HttpPost]

        public JsonResult UpsertContractorCodeEntry(string SP_CONTRA_CODE, string SP_CONTRA_NAME, string SP_CONTRA_COMP_NAME, string SP_CONTRA_ADD1, string SP_CONTRA_ADD2, string SP_CONTRA_ADD3, string SP_CONTRA_PHONE1, string SP_CONTRA_PHONE2, DateTime SP_CONTRA_AGRE_DATE, DateTime SP_CONTRA_EXP_DATE, decimal SP_CONTRA_NID, int ID)
        {
            try
            {
                Dictionary<string, object> d = new Dictionary<string, object>();

                d.Add("SP_CONTRA_CODE", SP_CONTRA_CODE);
                d.Add("SP_CONTRA_NAME", SP_CONTRA_NAME);
                d.Add("SP_CONTRA_COMP_NAME", SP_CONTRA_COMP_NAME);
                d.Add("SP_CONTRA_ADD1", SP_CONTRA_ADD1);
                d.Add("SP_CONTRA_ADD2", SP_CONTRA_ADD2);
                d.Add("SP_CONTRA_ADD3", SP_CONTRA_ADD3);
                d.Add("SP_CONTRA_PHONE1", SP_CONTRA_PHONE1);
                d.Add("SP_CONTRA_PHONE2", SP_CONTRA_PHONE2);
                d.Add("SP_CONTRA_AGRE_DATE", SP_CONTRA_AGRE_DATE);
                d.Add("SP_CONTRA_EXP_DATE", SP_CONTRA_EXP_DATE);
                d.Add("SP_CONTRA_NID", SP_CONTRA_NID);
                d.Add("ID", ID);




                CmnDataManager objCmnDataManager = new CmnDataManager();
                if (ID > 0)
                {

                    Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_ConCod_CONTRACTER_MANAGER", "Update", d);
                    if (rsltCode.isSuccessful)
                    {


                        var res = Json(rsltCode);


                        return Json(res);

                    }
                }
                else
                {
                    Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_ConCod_CONTRACTER_MANAGER", "Save", d);
                    if (rsltCode.isSuccessful)
                    {

                        var res = Json(rsltCode);

                        return Json(res);

                    }
                }

                return Json(null);
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public ViewResponseModel DeleteContractorCodeEntry(ContractorCodeEntryModel model)
        {
            return ContractorCodeEntryService.getInstance().DeleteContractorCodeEntry(model);
        }

        #endregion
        public IActionResult DedDetailEntry()
        {
            return View();
        }

        #region DeductionEntry

        public ActionResult DeductionEntry()
        {
            return View();
        }

        public JsonResult AuthorizationFor_Deduct_Ent(string Action, int ID)
        {
            string SessionUser = HttpContext.Session.GetString("USER_ID").ToString();
            Dictionary<string, object> d = new Dictionary<string, object>();
            if (Action == "Authorize")
            {
                d.Add("CheckerkID", SessionUser);
                d.Add("UpdatedDatetime", DateTime.Now);
                d.Add("ID", ID);
                CmnDataManager objCmnDataManager = new CmnDataManager();
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_DeducEnt_DEDUCTION_MANAGER", "AUTHORIZE_Deduct_Ent", d);
                if (rsltCode.isSuccessful)
                    return Json("Record authorized successfully.");
                else
                    return Json("Problem while authorizing data");
            }
            else if (Action == "Reject")
            {
                d.Add("ID", ID);
                CmnDataManager objCmnDataManager = new CmnDataManager();
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_DeducEnt_DEDUCTION_MANAGER", "REJECT_Deduct_Ent", d);
                if (rsltCode.isSuccessful)
                    return Json("Record rejected successfully.");
                else
                    return Json("Problem while recjecting data");

            }
            return Json(null);
        }

        public JsonResult tbl_UNAUTH_Deduct_Ent()
        {
            string SessionUser = HttpContext.Session.GetString("USER_ID").ToString();

            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("MakerId", SessionUser);

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_DeducEnt_DEDUCTION_MANAGER", "GET_UNAUTHORIZE_Deduct_Ent", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        public JsonResult tbl_AUTH_Deduct_Ent()
        {
            string SessionUser = HttpContext.Session.GetString("USER_ID").ToString();

            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("MakerId", SessionUser);

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_DeducEnt_DEDUCTION_MANAGER", "List", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        public JsonResult btn_tbl_Branch_DED()
        {

            Dictionary<string, object> d = new Dictionary<string, object>();

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_DeducEnt_DEDUCTION_MANAGER", "BRANCH_LOV", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];


                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        public JsonResult btn_modal_Desig_DED()
        {

            Dictionary<string, object> d = new Dictionary<string, object>();

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_DeducEnt_DEDUCTION_MANAGER", "DESG_LOV", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];


                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        public JsonResult btn_modal_LEVEL_DED(string SEARCHFILTER)
        {

            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("SEARCHFILTER", SEARCHFILTER);

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_DeducEnt_DEDUCTION_MANAGER", "LEVEL_LOV", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];


                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        public JsonResult btn_Modal_SP_CATEGORY_D()
        {

            Dictionary<string, object> d = new Dictionary<string, object>();

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_DeducEnt_DEDUCTION_MANAGER", "CATGORY_LOV", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];


                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        public JsonResult btn_Modal_SP_ACOUNT_NO_D()
        {

            Dictionary<string, object> d = new Dictionary<string, object>();

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_DeducEnt_DEDUCTION_MANAGER", "ACCOUNT_LOV", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];


                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        public JsonResult btn_modal_SP_DED_CODE()
        {

            Dictionary<string, object> d = new Dictionary<string, object>();

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_DeducEnt_DEDUCTION_MANAGER", "DUC_DETAIL_LOV1", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];


                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        public JsonResult DedDetailEnt_List(string SP_DED_CODE)
        {

            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("SP_DED_CODE", SP_DED_CODE);

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_DedDetailEnt_DEDUCTION_DETAILS_MANAGER", "List", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];


                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        [HttpPost]
        public JsonResult UpsertDeductionEntry(string SP_DED_CODE, string SP_BRANCH_D, string SP_DED_DESC, string SP_CATEGORY_D, string SP_DESG_D, string SP_LEVEL_D, DateTime? SP_VALID_FROM_D, DateTime? SP_VALID_TO_D, decimal? SP_DED_AMOUNT, decimal? SP_DED_PER, string SP_ALL_IND, string SP_ACOUNT_NO_D, int? ID)
        {
            try
            {
                Dictionary<string, object> d = new Dictionary<string, object>();

                d.Add("SP_DED_CODE", SP_DED_CODE);
                d.Add("SP_BRANCH_D", SP_BRANCH_D);
                d.Add("SP_DED_DESC", SP_DED_DESC);
                d.Add("SP_CATEGORY_D", SP_CATEGORY_D);
                d.Add("SP_DESG_D", SP_DESG_D);
                d.Add("SP_LEVEL_D", SP_LEVEL_D);
                d.Add("SP_VALID_FROM_D", SP_VALID_FROM_D);
                d.Add("SP_VALID_TO_D", SP_VALID_TO_D);
                d.Add("SP_DED_AMOUNT", SP_DED_AMOUNT);
                d.Add("SP_DED_PER", SP_DED_PER);
                d.Add("SP_ALL_IND", SP_ALL_IND);
                d.Add("SP_ACOUNT_NO_D", SP_ACOUNT_NO_D);
                d.Add("ID", ID);
                d.Add("MakerId", HttpContext.Session.GetString("USER_ID").ToString());
                d.Add("AddedDatetime", DateTime.Now);
                d.Add("IsAuth", false);

                CmnDataManager objCmnDataManager = new CmnDataManager();
                if (ID > 0)
                {

                    Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_DeducEnt_DEDUCTION_MANAGER", "Update", d);
                    if (rsltCode.isSuccessful)
                    {
                        var res = Json(rsltCode);
                        return Json(res);
                    }
                }
                else
                {
                    Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_DeducEnt_DEDUCTION_MANAGER", "Save", d);
                    if (rsltCode.isSuccessful)
                    {
                        var res = Json(rsltCode);
                        return Json(res);
                    }
                }
                return Json(null);
            }
            catch
            {
                return Json(null);
            }
        }

        public ViewResponseModel DeleteDeductionEntry(DeductionEntryModel model)
        {
            return DeductionEntryService.getInstance().DeleteDeductionEntry(model);
        }

        #endregion
        public IActionResult DeductionEntry_MasterDetail()
        {
            return View();
        }

        #region DepartmentEntry

        public IActionResult DepartmentEntry()
        {
            return View();
        }

        public JsonResult AuthorizationFor_Dep_Ent(string Action, int ID)
        {
            string SessionUser = HttpContext.Session.GetString("USER_ID").ToString();
            Dictionary<string, object> d = new Dictionary<string, object>();
            if (Action == "Authorize")
            {
                d.Add("CheckerkID", SessionUser);
                d.Add("UpdatedDatetime", DateTime.Now);
                d.Add("ID", ID);
                CmnDataManager objCmnDataManager = new CmnDataManager();
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_DEPT_MANAGER", "AUTHORIZE_DEPT", d);
                if (rsltCode.isSuccessful)
                    return Json("Record authorized successfully.");
                else
                    return Json("Problem while authorizing data");
            }
            else if (Action == "Reject")
            {
                d.Add("ID", ID);
                CmnDataManager objCmnDataManager = new CmnDataManager();
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_DEPT_MANAGER", "REJECT_DEPT", d);
                if (rsltCode.isSuccessful)
                    return Json("Record rejected successfully.");
                else
                    return Json("Problem while recjecting data");

            }
            return Json(null);
        }

        public JsonResult tbl_UNAUTH_Dep_Ent()
        {
            string SessionUser = HttpContext.Session.GetString("USER_ID").ToString();

            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("MakerId", SessionUser);

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_DEPT_MANAGER", "GET_UNAUTHORIZE_DEPT", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        public JsonResult tbl_AUTH_Dep_Ent()
        {
            string SessionUser = HttpContext.Session.GetString("USER_ID").ToString();

            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("MakerId", SessionUser);

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_DEPT_MANAGER", "List", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }


        public JsonResult Delete_Dep_Ent(int ID)
        {
            //string SessionUser = HttpContext.Session.GetString("USER_ID").ToString();

            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("ID", ID);

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_DEPT_MANAGER", "Delete", d);
            if (rsltCode.isSuccessful)
            {

                return Json("Record deleted successfully");
            }
            return Json(null);
        }


        public JsonResult Submit_DepartmentEntry(string SP_SEGMENT_D, string SP_GROUP_D, string SP_DEPT, string SP_COST_CENTER, string SP_DESC_D1, string SP_DESC_D2, string SP_DEPT_HEAD, DateTime? SP_DATE_FROM_D, DateTime? SP_DATE_TO_D, string RC, string SP_RC_APA, int? ID)
        {
            string MessageBox = "";

            if (SP_DATE_FROM_D != null && SP_DATE_TO_D != null && Convert.ToString(SP_DATE_FROM_D) != string.Empty && Convert.ToString(SP_DATE_TO_D) != string.Empty)
            {

                if (DateTime.Compare(Convert.ToDateTime(SP_DATE_FROM_D), Convert.ToDateTime(SP_DATE_TO_D)) > 0)
                {
                    MessageBox = "";
                    MessageBox = " 'To' Date Must be Greater than 'From' Date";
                    var res = new
                    {
                        MessageBox = MessageBox,
                    };
                    return Json(res);
                }


                if (Convert.ToString(SP_DESC_D1) != "" && Convert.ToString(SP_DEPT_HEAD) != "")
                {
                    Dictionary<string, object> dicInputParameters = new Dictionary<string, object>();

                    dicInputParameters.Clear();
                    dicInputParameters.Add("SP_DESC_D1", SP_DESC_D1);
                    dicInputParameters.Add("SP_DEPT_HEAD", SP_DEPT_HEAD);

                    //VALIDATING DUPLICATING RECORDS//
                    CmnDataManager objCmnDataManager = new CmnDataManager();
                    Result rslt = objCmnDataManager.GetData("CHRIS_SP_DEPT_MANAGER", "GetByParam", dicInputParameters);
                    if (rslt.isSuccessful)
                    {
                        if (Convert.ToDecimal(rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString()) >= 1)
                        {
                            MessageBox = "";
                            MessageBox = "Record Already Exists";
                            var res = new
                            {
                                MessageBox = MessageBox,
                            };
                            return Json(res);
                        }
                    }
                }

                if (MessageBox == "")
                {
                    MessageBox = "";
                    MessageBox = "Do You Want to Save Changes.";
                    var res = new
                    {
                        MessageBox = MessageBox,
                    };
                    return Json(res);
                }
            }
            return Json(null);
        }

        [HttpPost]
        public ActionResult Save_DepartmentEntry(string SP_SEGMENT_D, string SP_GROUP_D, string SP_DEPT, string SP_COST_CENTER, string SP_DESC_D1, string SP_DESC_D2, string SP_DEPT_HEAD, DateTime? SP_DATE_FROM_D, DateTime? SP_DATE_TO_D, string RC, string SP_RC_APA, int? ID)
        {
            string MessageBox = "";

            try
            {
                Dictionary<string, object> d = new Dictionary<string, object>();

                d.Add("SP_SEGMENT_D", SP_SEGMENT_D);
                d.Add("SP_GROUP_D", SP_GROUP_D);
                d.Add("SP_DEPT", SP_DEPT);
                d.Add("SP_COST_CENTER", SP_COST_CENTER);
                d.Add("SP_DESC_D1", SP_DESC_D1);
                d.Add("SP_DESC_D2", SP_DESC_D2);
                d.Add("SP_DEPT_HEAD", SP_DEPT_HEAD);
                d.Add("SP_DATE_FROM_D", SP_DATE_FROM_D);
                d.Add("SP_DATE_TO_D", SP_DATE_TO_D);
                d.Add("RC", RC);
                d.Add("SP_RC_APA", SP_RC_APA);
                d.Add("ID", ID);
                d.Add("MakerId", HttpContext.Session.GetString("USER_ID").ToString());
                d.Add("AddedDatetime", DateTime.Now);
                d.Add("IsAuth", false);

                CmnDataManager objCmnDataManager = new CmnDataManager();
                if (ID > 0)
                {
                    Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_DEPT_MANAGER", "Update", d);
                    if (rsltCode.isSuccessful)
                    {

                        MessageBox = "";
                        MessageBox = "Your request is modify successfully !";
                        var res = new
                        {
                            MessageBox = MessageBox,
                        };
                        return Json(res);
                    }
                }
                else
                {
                    Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_DEPT_MANAGER", "Save", d);
                    if (rsltCode.isSuccessful)
                    {

                        MessageBox = "";
                        MessageBox = "Your request is successfully completed !";
                        var res = new
                        {
                            MessageBox = MessageBox,
                        };
                        return Json(res);
                    }
                }
                return Json(null);
            }
            catch
            {
                return Json(null);
            }
        }

        public JsonResult btnUploadPath_Click(string SelectFile_DEP_ENT)
        {
            try
            {
                string MessageBox = "";
                string filePath = string.Empty;
                string fileExt = string.Empty;

                if (SelectFile_DEP_ENT != "")
                {
                    filePath = SelectFile_DEP_ENT;
                    fileExt = Path.GetExtension(filePath);
                    if (fileExt.CompareTo(".xlsx") == 0 || fileExt.CompareTo(".xls") == 0)
                    {
                        SelectFile_DEP_ENT = filePath;
                        var res = new
                        {
                            SelectFile_DEP_ENT = SelectFile_DEP_ENT,
                            MessageBox = MessageBox,
                        };
                        return Json(res);
                    }
                    else
                    {
                        MessageBox = "";
                        MessageBox = "Please choose .xlsx and .xls file only.";
                        var res1 = new
                        {
                            SelectFile_DEP_ENT = SelectFile_DEP_ENT,
                            MessageBox = MessageBox,
                        };
                        return Json(res1);
                    }
                }
            }
            catch
            {
                return Json(null);
            }

            return Json(null);
        }

        #endregion
        public IActionResult DeptCostCenterEntry()
        {
            return View();
        }

        #region DesignationEntry
        public ActionResult DesignationEntry()
        {
            return View();
        }

        public JsonResult AuthorizationFor_DesigEntry(string Action, int ID)
        {
            string SessionUser = HttpContext.Session.GetString("USER_ID").ToString();
            Dictionary<string, object> d = new Dictionary<string, object>();
            if (Action == "Authorize")
            {
                d.Add("CheckerkID", SessionUser);
                d.Add("UpdatedDatetime", DateTime.Now);
                d.Add("ID", ID);
                CmnDataManager objCmnDataManager = new CmnDataManager();
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_DESIG_MANAGER", "AUTHORIZE_DESIG", d);
                if (rsltCode.isSuccessful)
                    return Json("Record authorized successfully.");
                else
                    return Json("Problem while authorizing data");
            }
            else if (Action == "Reject")
            {
                d.Add("ID", ID);
                CmnDataManager objCmnDataManager = new CmnDataManager();
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_DESIG_MANAGER", "REJECT_DESIG", d);
                if (rsltCode.isSuccessful)
                    return Json("Record rejected successfully.");
                else
                    return Json("Problem while recjecting data");

            }
            return Json(null);
        }

        public JsonResult tbl_UNAUTH_DesigEntry()
        {
            string SessionUser = HttpContext.Session.GetString("USER_ID").ToString();

            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("MakerId", SessionUser);

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_DESIG_MANAGER", "GET_UNAUTHORIZE_DESIG", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        public JsonResult tbl_AUTH_DesigEntry()
        {
            string SessionUser = HttpContext.Session.GetString("USER_ID").ToString();

            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("MakerId", SessionUser);

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_DESIG_MANAGER", "List", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        [HttpPost]
        public JsonResult btn_tbl_Category()
        {
            Dictionary<string, object> d = new Dictionary<string, object>();

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_DESIG_MANAGER", "Lov_CAT", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        [HttpPost]
        public JsonResult btn_tbl_Branch()
        {
            Dictionary<string, object> d = new Dictionary<string, object>();

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_DESIG_MANAGER", "Lov_BRN", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        [HttpPost]
        public ActionResult UpsertDesignationEntry(string SP_DESG, string SP_BRANCH, string SP_LEVEL, string SP_CATEGORY, string SP_DESC_DG1, string SP_DESC_DG2, decimal? SP_CONFIRM, decimal? SP_MIN, decimal? SP_MID, decimal? SP_MAX, decimal? SP_INCREMENT, DateTime? SP_EFFECTIVE, string? SP_CURRENT, int ID)
        {

            try
            {
                Dictionary<string, object> d = new Dictionary<string, object>();

                d.Add("SP_DESG", SP_DESG);
                d.Add("SP_BRANCH", SP_BRANCH);
                d.Add("SP_LEVEL", SP_LEVEL);
                d.Add("SP_CATEGORY", SP_CATEGORY);
                d.Add("SP_DESC_DG1", SP_DESC_DG1);
                d.Add("SP_DESC_DG2", SP_DESC_DG2);
                d.Add("SP_CONFIRM", SP_CONFIRM);
                d.Add("SP_MIN", SP_MIN);
                d.Add("SP_MID", SP_MID);
                d.Add("SP_MAX", SP_MAX);
                d.Add("SP_INCREMENT", SP_INCREMENT);
                d.Add("SP_EFFECTIVE", SP_EFFECTIVE);
                d.Add("SP_CURRENT", SP_CURRENT);
                d.Add("ID", ID);
                d.Add("MakerId", HttpContext.Session.GetString("USER_ID").ToString());
                d.Add("AddedDatetime", DateTime.Now);
                d.Add("IsAuth", false);

                CmnDataManager objCmnDataManager = new CmnDataManager();
                if (ID > 0)
                {

                    Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_DESIG_MANAGER", "Update", d);
                    if (rsltCode.isSuccessful)
                    {
                        var res = Json(rsltCode);
                        return Json("Your request is modify successfully !");
                    }
                }
                else
                {
                    Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_DESIG_MANAGER", "Save", d);
                    if (rsltCode.isSuccessful)
                    {
                        var res = Json(rsltCode);
                        return Json("Your request is successfully completed !");
                    }
                }
                return Json(null);
            }
            catch
            {
                return Json(null);
            }
        }

        public ViewResponseModel DeleteDesignationEntry(DesignationEntryModel model)
        {
            return DesignationEntryService.getInstance().DeleteDesignationEntry(model);
        }

        #endregion

        #region GlobalParameterSetup

        public IActionResult GlobalParameterSetup()
        {
            return View();
        }

        public JsonResult AuthorizationFor_GPS(string Action, int ID)
        {
            string SessionUser = HttpContext.Session.GetString("USER_ID").ToString();
            Dictionary<string, object> d = new Dictionary<string, object>();
            if (Action == "Authorize")
            {
                d.Add("CheckerkID", SessionUser);
                d.Add("UpdatedDatetime", DateTime.Now);
                d.Add("ID", ID);
                CmnDataManager objCmnDataManager = new CmnDataManager();
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_COM_GLOBAL_PARAM_MAPPING_MANAGER", "AUTHORIZE_GPM", d);
                if (rsltCode.isSuccessful)
                    return Json("Record authorized successfully.");
                else
                    return Json("Problem while authorizing data");
            }
            else if (Action == "Reject")
            {
                d.Add("ID", ID);
                CmnDataManager objCmnDataManager = new CmnDataManager();
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_COM_GLOBAL_PARAM_MAPPING_MANAGER", "REJECT_GPM", d);
                if (rsltCode.isSuccessful)
                    return Json("Record rejected successfully.");
                else
                    return Json("Problem while recjecting data");

            }
            return Json(null);
        }

        public JsonResult tbl_UNAUTH_GPS()
        {
            string SessionUser = HttpContext.Session.GetString("USER_ID").ToString();

            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("MakerId", SessionUser);


            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_COM_GLOBAL_PARAM_MAPPING_MANAGER", "GET_UNAUTHORIZE_GPM", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        public JsonResult tbl_AUTH_GPS()
        {
            string SessionUser = HttpContext.Session.GetString("USER_ID").ToString();

            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("MakerId", SessionUser);
            d.Add("GLOBAL_PARAM_ID", 0);

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_COM_GLOBAL_PARAM_MAPPING_MANAGER", "List", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }


        [HttpPost]
        public JsonResult GlobalParameterSetup1(int GLOBAL_PARAM_ID)
        {
            Dictionary<string, object> d = new Dictionary<string, object>();

            d.Add("GLOBAL_PARAM_ID", GLOBAL_PARAM_ID);

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_COM_GLOBAL_PARAM_MAPPING_MANAGER", "List", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];

                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);

            }
            return Json(null);
        }


        [HttpPost]
        public ActionResult UpsertGlobalParameterSetup(string SOE_ID, int GLOBAL_PARAM_ID, string GLOBAL_PARAM_VALUE, int ID)
        {
            try
            {
                Dictionary<string, object> d = new Dictionary<string, object>();

                d.Add("SOE_ID", SOE_ID);
                d.Add("GLOBAL_PARAM_ID", GLOBAL_PARAM_ID);
                d.Add("GLOBAL_PARAM_VALUE", GLOBAL_PARAM_VALUE);
                d.Add("ID", ID);
                d.Add("MakerId", HttpContext.Session.GetString("USER_ID").ToString());
                d.Add("AddedDatetime", DateTime.Now);
                d.Add("IsAuth", false);

                CmnDataManager objCmnDataManager = new CmnDataManager();
                if (ID > 0)
                {

                    Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_COM_GLOBAL_PARAM_MAPPING_MANAGER", "Update", d);
                    if (rsltCode.isSuccessful)
                    {
                        var res = Json(rsltCode);
                        return Json(res);
                    }
                }
                else
                {
                    Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_COM_GLOBAL_PARAM_MAPPING_MANAGER", "Save", d);
                    if (rsltCode.isSuccessful)
                    {
                        var res = Json(rsltCode);
                        return Json(res);
                    }
                }
                return Json(null);
            }
            catch
            {

                return Json(null);
            }
        }

        public JsonResult Delete_GPS(int? ID)
        {
            string SessionUser = HttpContext.Session.GetString("USER_ID").ToString();

            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("ID", ID);

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_COM_GLOBAL_PARAM_MAPPING_MANAGER", "Delete", d);
            if (rsltCode.isSuccessful)
            {
                return Json("Record Delete Successfully");
            }
            return Json(null);
        }

        #endregion

        #region GroupEntry
        public ActionResult GroupEntry()
        {
            return View();
        }

        public JsonResult AuthorizationFor_GroupEntry(string Action, int ID)
        {
            string SessionUser = HttpContext.Session.GetString("USER_ID").ToString();
            Dictionary<string, object> d = new Dictionary<string, object>();
            if (Action == "Authorize")
            {
                d.Add("CheckerkID", SessionUser);
                d.Add("UpdatedDatetime", DateTime.Now);
                d.Add("ID", ID);
                CmnDataManager objCmnDataManager = new CmnDataManager();
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_SETUP_GROUP1_MANAGER", "AUTHORIZE_GroupEntry", d);
                if (rsltCode.isSuccessful)
                    return Json("Record authorized successfully.");
                else
                    return Json("Problem while authorizing data");
            }
            else if (Action == "Reject")
            {
                d.Add("ID", ID);
                CmnDataManager objCmnDataManager = new CmnDataManager();
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_SETUP_GROUP1_MANAGER", "REJECT_GroupEntry", d);
                if (rsltCode.isSuccessful)
                    return Json("Record rejected successfully.");
                else
                    return Json("Problem while recjecting data");

            }
            return Json(null);
        }

        public JsonResult tbl_UNAUTH_GroupEntry()
        {
            string SessionUser = HttpContext.Session.GetString("USER_ID").ToString();

            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("MakerId", SessionUser);


            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_SETUP_GROUP1_MANAGER", "GET_UNAUTHORIZE_GroupEntry", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        public JsonResult tbl_AUTH_GroupEntry()
        {
            string SessionUser = HttpContext.Session.GetString("USER_ID").ToString();

            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("MakerId", SessionUser);

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_SETUP_GROUP1_MANAGER", "List", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        [HttpPost]
        public ActionResult UpsertGroupEntry(int? ID, string SP_SEGMENT, string SP_GROUP_CODE, string SP_DESC_1, string SP_DESC_2, string SP_GROUP_HEAD, DateTime? SP_DATE_FROM, DateTime? SP_DATE_TO)
        {
            try
            {
                Dictionary<string, object> d = new Dictionary<string, object>();

                d.Add("ID", ID);
                d.Add("SP_SEGMENT", SP_SEGMENT);
                d.Add("SP_GROUP_CODE", SP_GROUP_CODE);
                d.Add("SP_DESC_1", SP_DESC_1);
                d.Add("SP_DESC_2", SP_DESC_2);
                d.Add("SP_GROUP_HEAD", SP_GROUP_HEAD);
                d.Add("SP_DATE_FROM", SP_DATE_FROM);
                d.Add("SP_DATE_TO", SP_DATE_TO);
                d.Add("MakerId", HttpContext.Session.GetString("USER_ID").ToString());
                d.Add("AddedDatetime", DateTime.Now);
                d.Add("IsAuth", false);

                CmnDataManager objCmnDataManager = new CmnDataManager();
                if (ID > 0)
                {

                    Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_SETUP_GROUP1_MANAGER", "Update", d);
                    if (rsltCode.isSuccessful)
                    {
                        var res = Json(rsltCode);
                        return Json("Your request is successfully Updated !");
                    }
                }
                else
                {
                    Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_SETUP_GROUP1_MANAGER", "Save", d);
                    if (rsltCode.isSuccessful)
                    {
                        var res = Json(rsltCode);
                        return Json("Your request is successfully Save !");
                    }
                }
                return Json(null);
            }
            catch
            {

                return Json(null);
            }
        }

        public ViewResponseModel DeleteGroupEntry(GroupEntryModel model)
        {
            return GroupEntryServices.getInstance().DeleteGroupEntry(model);
        }

        #endregion

        #region GroupLifeHospitalEntry

        public IActionResult GroupLifeHospitalEntry()
        {
            return View();
        }

        public JsonResult tbl_UNAUTH_GLH_Ent()
        {
            string SessionUser = HttpContext.Session.GetString("USER_ID").ToString();

            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("MakerId", SessionUser);

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_GROUP_INSC_MANAGER", "GET_UNAUTHORIZE_GROUP_INSC", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        public JsonResult tbl_AUTH_GLH_Ent()
        {
            string SessionUser = HttpContext.Session.GetString("USER_ID").ToString();

            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("MakerId", SessionUser);

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_GROUP_INSC_MANAGER", "List", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        [HttpPost]
        public JsonResult UpsertGroupLifeHospitalEntry(string SP_TYPE_CODE, string SP_NAME_1, string SP_NAME_2, string SP_STAFF_TYPE, DateTime SP_RENEWAL, string SP_POLICY_NO, decimal SP_L_COVERAGE, decimal SP_H_COVERAGE, string SP_DESIG_IN, string SP_LEVEL_IN, decimal SP_MULT_PACK, int ID)
        {
            try
            {
                Dictionary<string, object> d = new Dictionary<string, object>();
                d.Add("SP_TYPE_CODE", SP_TYPE_CODE);
                d.Add("SP_NAME_1", SP_NAME_1);
                d.Add("SP_NAME_2", SP_NAME_2);
                d.Add("SP_STAFF_TYPE", SP_STAFF_TYPE);
                d.Add("SP_RENEWAL", SP_RENEWAL);
                d.Add("SP_POLICY_NO", SP_POLICY_NO);
                d.Add("SP_L_COVERAGE", SP_L_COVERAGE);
                d.Add("SP_H_COVERAGE", SP_H_COVERAGE);
                d.Add("SP_DESIG_IN", SP_DESIG_IN);
                d.Add("SP_LEVEL_IN", SP_LEVEL_IN);
                d.Add("SP_MULT_PACK", SP_MULT_PACK);
                d.Add("MakerId", HttpContext.Session.GetString("USER_ID").ToString());
                d.Add("AddedDatetime", DateTime.Now);
                d.Add("IsAuth", false);
                d.Add("ID", ID);

                CmnDataManager objCmnDataManager = new CmnDataManager();
                if (ID > 0)
                {

                    Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_GROUP_INSC_MANAGER", "Update", d);
                    if (rsltCode.isSuccessful)
                    {
                        var res = Json(rsltCode);
                        return Json(res);
                    }
                }
                else
                {
                    Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_GROUP_INSC_MANAGER", "Save", d);
                    if (rsltCode.isSuccessful)
                    {
                        var res = Json(rsltCode);
                        return Json(res);
                    }
                }

                return Json(null);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        [HttpPost]
        public JsonResult AuthorizationFor_GLH_Ent(string Action, int ID)
        {
            string SessionUser = HttpContext.Session.GetString("USER_ID").ToString();
            Dictionary<string, object> d = new Dictionary<string, object>();
            if (Action == "Authorize")
            {
                d.Add("CheckerkID", SessionUser);
                d.Add("UpdatedDatetime", DateTime.Now);
                d.Add("ID", ID);
                CmnDataManager objCmnDataManager = new CmnDataManager();
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_GROUP_INSC_MANAGER", "AUTHORIZE_GROUP_INSC", d);
                if (rsltCode.isSuccessful)
                    return Json("Record authorized successfully.");
                else
                    return Json("Problem while authorizing data");
            }
            else if (Action == "Reject")
            {
                d.Add("ID", ID);
                CmnDataManager objCmnDataManager = new CmnDataManager();
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_GROUP_INSC_MANAGER", "REJECT_GROUP_INSC", d);
                if (rsltCode.isSuccessful)
                    return Json("Record rejected successfully.");
                else
                    return Json("Problem while recjecting data");

            }
            return Json(null);
        }

        public ViewResponseModel DeleteGroupLifeHospitalEntry(GroupLifeHospitalEntryModel model)
        {
            return GroupLifeHospitalEntryService.getInstance().DeleteGroupLifeHospitalEntry(model);
        }

        #endregion
        public IActionResult HolFileList()
        {
            return View();
        }

        #region IncomeTaxSlabsEntry
        public IActionResult IncomeTaxSlabsEntry()
        {
            return View();
        }

        public JsonResult AuthorizationFor_ITSE(string Action, int ID)
        {
            string SessionUser = HttpContext.Session.GetString("USER_ID").ToString();
            Dictionary<string, object> d = new Dictionary<string, object>();
            if (Action == "Authorize")
            {
                d.Add("CheckerkID", SessionUser);
                d.Add("UpdatedDatetime", DateTime.Now);
                d.Add("ID", ID);
                CmnDataManager objCmnDataManager = new CmnDataManager();
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_INCOME_MANAGER", "AUTHORIZE_ITSE", d);
                if (rsltCode.isSuccessful)
                    return Json("Record authorized successfully.");
                else
                    return Json("Problem while authorizing data");
            }
            else if (Action == "Reject")
            {
                d.Add("ID", ID);
                CmnDataManager objCmnDataManager = new CmnDataManager();
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_INCOME_MANAGER", "REJECT_ITSE", d);
                if (rsltCode.isSuccessful)
                    return Json("Record rejected successfully.");
                else
                    return Json("Problem while recjecting data");

            }
            return Json(null);
        }

        public JsonResult tbl_UNAUTH_ITSE()
        {
            string SessionUser = HttpContext.Session.GetString("USER_ID").ToString();

            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("MakerId", SessionUser);

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_INCOME_MANAGER", "GET_UNAUTHORIZE_ITSE", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        public JsonResult tbl_AUTH_ITSE()
        {
            string SessionUser = HttpContext.Session.GetString("USER_ID").ToString();

            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("MakerId", SessionUser);

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_INCOME_MANAGER", "List", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }


        [HttpPost]
        public ActionResult UpsertITSE(Decimal? SP_AMT_FROM, Decimal? SP_AMT_TO, Decimal? SP_REBATE, Decimal? SP_PERCEN, Decimal? SP_TAX_AMT, Decimal? SP_SURCHARGE, Decimal? SP_FREBATE, Decimal? SP_FPERCEN, Decimal? SP_FTAX_AMT, Decimal? SP_FSURCHARGE, Decimal? SP_ADD_TAX, Decimal? SP_ADD_TAX_AMT, Decimal? TAX_RED_PER, Decimal? FTAX_RED_PER, int? ID)
        {

            try
            {
                Dictionary<string, object> d = new Dictionary<string, object>();

                d.Add("SP_AMT_FROM", SP_AMT_FROM);
                d.Add("SP_AMT_TO", SP_AMT_TO);
                d.Add("SP_REBATE", SP_REBATE);
                d.Add("SP_PERCEN", SP_PERCEN);
                d.Add("SP_TAX_AMT", SP_TAX_AMT);
                d.Add("SP_SURCHARGE", SP_SURCHARGE);
                d.Add("SP_FREBATE", SP_FREBATE);
                d.Add("SP_FPERCEN", SP_FPERCEN);
                d.Add("SP_FTAX_AMT", SP_FTAX_AMT);
                d.Add("SP_FSURCHARGE", SP_FSURCHARGE);
                d.Add("SP_ADD_TAX", SP_ADD_TAX);
                d.Add("SP_ADD_TAX_AMT", SP_ADD_TAX_AMT);
                d.Add("TAX_RED_PER", TAX_RED_PER);
                d.Add("FTAX_RED_PER", FTAX_RED_PER);
                d.Add("ID", ID);
                d.Add("MakerId", HttpContext.Session.GetString("USER_ID").ToString());
                d.Add("AddedDatetime", DateTime.Now);
                d.Add("IsAuth", false);

                CmnDataManager objCmnDataManager = new CmnDataManager();
                if (ID > 0)
                {

                    Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_INCOME_MANAGER", "Update", d);
                    if (rsltCode.isSuccessful)
                    {
                        return Json("Your request is modify successfully !");
                    }
                }
                else
                {
                    Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_INCOME_MANAGER", "Save", d);
                    if (rsltCode.isSuccessful)
                    {
                        return Json("Your request is successfully completed !");
                    }
                }
                return Json(null);
            }
            catch (Exception ex)
            {
                return Json(null);
            }
        }

        public ActionResult DeleteITSE(int? ID)
        {
            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("ID", ID);
            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_INCOME_MANAGER", "Delete", d);
            if (rsltCode.isSuccessful)
            {

                return Json("Your request is deleted completed !");

            }
            return Json(null);
        }

        #endregion

        #region LoanSubsidyBenchMark

        public ActionResult LoanSubsidyBenchMark()
        {
            return View();
        }

        public JsonResult tbl_AUTH_LSBM()
        {
            string SessionUser = HttpContext.Session.GetString("USER_ID").ToString();

            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("MakerId", SessionUser);

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_LOAN_TAX_MANAGER", "List", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        public JsonResult tbl_UNAUTH_LSBM()
        {
            string SessionUser = HttpContext.Session.GetString("USER_ID").ToString();

            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("MakerId", SessionUser);

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_LOAN_TAX_MANAGER", "GET_UNAUTHORIZE_LSBM", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        [HttpPost]
        public JsonResult AuthorizationFor_LSBM(string Action, int ID)
        {
            string SessionUser = HttpContext.Session.GetString("USER_ID").ToString();
            Dictionary<string, object> d = new Dictionary<string, object>();
            if (Action == "Authorize")
            {
                d.Add("CheckerkID", SessionUser);
                d.Add("UpdatedDatetime", DateTime.Now);
                d.Add("ID", ID);
                CmnDataManager objCmnDataManager = new CmnDataManager();
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_LOAN_TAX_MANAGER", "AUTHORIZE_LSBM", d);
                if (rsltCode.isSuccessful)
                    return Json("Record authorized successfully.");
                else
                    return Json("Problem while authorizing data");
            }
            else if (Action == "Reject")
            {
                d.Add("ID", ID);
                CmnDataManager objCmnDataManager = new CmnDataManager();
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_LOAN_TAX_MANAGER", "REJECT_LSBM", d);
                if (rsltCode.isSuccessful)
                    return Json("Record rejected successfully.");
                else
                    return Json("Problem while recjecting data");

            }
            return Json(null);
        }


        [HttpPost]
        public ActionResult UpsertLoanSubsidyBenchMark(DateTime? TAX_YEAR_FROM, DateTime? TAX_YEAR_TO, decimal? TAX_PER, int ID)
        {

            try
            {
                Dictionary<string, object> d = new Dictionary<string, object>();

                d.Add("TAX_YEAR_FROM", TAX_YEAR_FROM);
                d.Add("TAX_YEAR_TO", TAX_YEAR_TO);
                d.Add("TAX_PER", TAX_PER);
                d.Add("MakerId", HttpContext.Session.GetString("USER_ID").ToString());
                d.Add("AddedDatetime", DateTime.Now);
                d.Add("IsAuth", false);
                d.Add("ID", ID);

                CmnDataManager objCmnDataManager = new CmnDataManager();
                if (ID > 0)
                {

                    Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_LOAN_TAX_MANAGER", "Update", d);
                    if (rsltCode.isSuccessful)
                    {

                        var res = Json(rsltCode);
                        return Json(res);

                    }
                }
                else
                {
                    Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_LOAN_TAX_MANAGER", "Save", d);
                    if (rsltCode.isSuccessful)
                    {

                        var res = Json(rsltCode);
                        return Json(res);

                    }
                }
                return Json(null);
            }
            catch (Exception ex)
            {

                throw;
            }
        }


        public ViewResponseModel DeleteLoanSubsidyBenchMark(LoanSubsidyBenchMarkModel model)
        {
            return LoanSubsidyBenchMarkService.getInstance().DeleteLoanSubsidyBenchMark(model);
        }

        #endregion

        #region MarginalTaxSetup

        public ActionResult MarginalTaxSetup()
        {
            var MarginalTaxSetup = MarginalTaxSetupService.getInstance().MarginalTaxSetup();
            return View(MarginalTaxSetup);
        }

        [HttpPost]
        public JsonResult UpsertMarginalTaxSetup(DateTime? MT_DATE_FROM, DateTime? MT_DATE_TO, decimal? MT_AMT_FROM, decimal? MT_AMT_TO, decimal? MT_PERCENTAGE, int? ID)
        {
            try
            {
                Dictionary<string, object> d = new Dictionary<string, object>();

                d.Add("MT_DATE_FROM", MT_DATE_FROM);
                d.Add("MT_DATE_TO", MT_DATE_TO);
                d.Add("MT_AMT_FROM", MT_AMT_FROM);
                d.Add("MT_AMT_TO", MT_AMT_TO);
                d.Add("MT_PERCENTAGE", MT_PERCENTAGE);
                d.Add("ID", ID);
                d.Add("MakerId", HttpContext.Session.GetString("USER_ID").ToString());
                d.Add("AddedDatetime", DateTime.Now);
                d.Add("IsAuth", false);
                CmnDataManager objCmnDataManager = new CmnDataManager();
                if (ID > 0)
                {
                    Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_MARGINAL_TAX_MANAGER", "Update", d);
                    if (rsltCode.isSuccessful)
                    {
                        var res = Json(rsltCode);
                        return Json(res);
                    }
                }
                else
                {
                    Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_MARGINAL_TAX_MANAGER", "Save", d);
                    if (rsltCode.isSuccessful)
                    {
                        // DataTable firstTable = rsltCode.dstResult.Tables[0];
                        var res = Json(rsltCode);
                        //var CatResul = JsonConvert.SerializeObject(firstTable);
                        return Json(res);
                    }
                }
                return Json(null);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }
        public ViewResponseModel DeleteMarginalTaxSetup(MarginalTaxSetupModel model)
        {
            return MarginalTaxSetupService.getInstance().DeleteMarginalTaxSetup(model);
        }
        [HttpPost]
        public JsonResult AuthorizationForMarginTax(string Action, int ID)
        {
            string SessionUser = HttpContext.Session.GetString("USER_ID").ToString();
            Dictionary<string, object> d = new Dictionary<string, object>();
            if (Action == "Authorize")
            {
                d.Add("CheckerkID", SessionUser);
                d.Add("UpdatedDatetime", DateTime.Now);
                d.Add("ID", ID);
                CmnDataManager objCmnDataManager = new CmnDataManager();
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_MARGINAL_TAX_MANAGER", "AUTHORIZE_MARGIN_TAX", d);
                if (rsltCode.isSuccessful)
                    return Json("Record authorized successfully.");
                else
                    return Json("Problem while authorizing data");
            }
            else if (Action == "Reject")
            {
                d.Add("ID", ID);
                CmnDataManager objCmnDataManager = new CmnDataManager();
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_MARGINAL_TAX_MANAGER", "REJECT_MARGIN_TAX", d);
                if (rsltCode.isSuccessful)
                    return Json("Record rejected successfully.");
                else
                    return Json("Problem while recjecting data");

            }
            return Json(null);
        }
        [HttpPost]
        public JsonResult GetMarginTaxUnAuthorizedData()
        {
            string SessionUser = HttpContext.Session.GetString("USER_ID").ToString();
            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("MakerId", SessionUser);
            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_MARGINAL_TAX_MANAGER", "GET_UNAUTHORIZE_MARGIN_TAX", d);
            if (rsltCode.isSuccessful)
            {
                if (rsltCode.dstResult.Tables.Count > 0)
                {
                    if (rsltCode.dstResult.Tables[0].Rows.Count > 0)
                    {
                        DataTable firstTable = rsltCode.dstResult.Tables[0];
                        var CatResul = JsonConvert.SerializeObject(firstTable);
                        return Json(CatResul);
                    }
                }
            }
            return Json(null);
        }
        #endregion

        #region SetupHolidayEntry

        public IActionResult SetupHolidayEntry()
        {
            return View();
        }
        public JsonResult tbl_UNAUTH_SH_Ent()
        {
            string SessionUser = HttpContext.Session.GetString("USER_ID").ToString();

            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("MakerId", SessionUser);

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_HOLIDAY_MANAGER", "GET_UNAUTHORIZE_SP_SH", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        public JsonResult AuthorizationFor_SH_Ent(string Action, int ID)
        {
            string SessionUser = HttpContext.Session.GetString("USER_ID").ToString();
            Dictionary<string, object> d = new Dictionary<string, object>();
            if (Action == "Authorize")
            {
                d.Add("CheckerkID", SessionUser);
                d.Add("UpdatedDatetime", DateTime.Now);
                d.Add("ID", ID);
                CmnDataManager objCmnDataManager = new CmnDataManager();
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_HOLIDAY_MANAGER", "AUTHORIZE_SP_SH", d);
                if (rsltCode.isSuccessful)
                    return Json("Record authorized successfully.");
                else
                    return Json("Problem while authorizing data");
            }
            else if (Action == "Reject")
            {
                d.Add("ID", ID);
                CmnDataManager objCmnDataManager = new CmnDataManager();
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_HOLIDAY_MANAGER", "REJECT_SP_SH", d);
                if (rsltCode.isSuccessful)
                    return Json("Record rejected successfully.");
                else
                    return Json("Problem while recjecting data");

            }
            return Json(null);
        }

        public JsonResult tbl_AUTH_SH_Ent()
        {
            string SessionUser = HttpContext.Session.GetString("USER_ID").ToString();

            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("MakerId", SessionUser);

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_HOLIDAY_MANAGER", "List", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        [HttpPost]
        public ActionResult UpsertSetupHolidayEntry(DateTime? SP_HOL_DATE, string SP_H_DESC, int ID)
        {
            try
            {
                Dictionary<string, object> d = new Dictionary<string, object>();

                d.Add("SP_HOL_DATE", SP_HOL_DATE);
                d.Add("SP_H_DESC", SP_H_DESC);
                d.Add("MakerId", HttpContext.Session.GetString("USER_ID").ToString());
                d.Add("AddedDatetime", DateTime.Now);
                d.Add("IsAuth", false);
                d.Add("ID", ID);

                CmnDataManager objCmnDataManager = new CmnDataManager();
                if (ID > 0)
                {

                    Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_HOLIDAY_MANAGER", "Update", d);
                    if (rsltCode.isSuccessful)
                    {
                        return Json("Your request is successfully completed !");
                    }
                }
                else
                {
                    Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_HOLIDAY_MANAGER", "Save", d);
                    if (rsltCode.isSuccessful)
                    {
                        return Json("Your request is successfully completed !");
                    }
                }

                return Json(null);
            }
            catch
            {
                return Json(null);
            }
        }

        public ViewResponseModel Delete_SH_Ent(SetupHolidayEntryModel model)
        {
            return SetupHolidayEntryService.getInstance().DeleteSetupHolidayEntry(model);
        }

        #endregion

        #region SMTPSeting

        public ActionResult SMTPSeting()
        {
            return View();
        }

        public JsonResult tbl_AUTH_SMTPSeting()
        {
            string SessionUser = HttpContext.Session.GetString("USER_ID").ToString();

            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("MakerId", SessionUser);

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_SMTP_SETTINGS", "List", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        public JsonResult tbl_UNAUTH_SMTPSeting()
        {
            string SessionUser = HttpContext.Session.GetString("USER_ID").ToString();

            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("MakerId", SessionUser);

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_SMTP_SETTINGS", "GET_UNAUTHORIZE_SMTP_Settings", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        [HttpPost]
        public JsonResult AuthorizationFor_SMTPSeting(string Action, int ID)
        {
            string SessionUser = HttpContext.Session.GetString("USER_ID").ToString();
            Dictionary<string, object> d = new Dictionary<string, object>();
            if (Action == "Authorize")
            {
                d.Add("CheckerkID", SessionUser);
                d.Add("UpdatedDatetime", DateTime.Now);
                d.Add("ID", ID);
                CmnDataManager objCmnDataManager = new CmnDataManager();
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_SMTP_SETTINGS", "AUTHORIZE_SMTP_Settings", d);
                if (rsltCode.isSuccessful)
                    return Json("Record authorized successfully.");
                else
                    return Json("Problem while authorizing data");
            }
            else if (Action == "Reject")
            {
                d.Add("ID", ID);
                CmnDataManager objCmnDataManager = new CmnDataManager();
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_SMTP_SETTINGS", "REJECT_SMTP_Settings", d);
                if (rsltCode.isSuccessful)
                    return Json("Record rejected successfully.");
                else
                    return Json("Problem while recjecting data");

            }
            return Json(null);
        }


        [HttpPost]
        public JsonResult UpsertSMTPSeting(int? ID, string SharedFolderPath, string Subject, string Body, string ProfileName)
        {

            try
            {
                Dictionary<string, object> d = new Dictionary<string, object>();
                d.Add("ProfileName", ProfileName);
                d.Add("Body", Body);
                d.Add("Subject", Subject);
                d.Add("SharedFolderPath", SharedFolderPath);
                d.Add("ID", ID);
                d.Add("MakerId", HttpContext.Session.GetString("USER_ID").ToString());
                d.Add("CheckerkID", null);
                d.Add("AddedDatetime", DateTime.Now);
                d.Add("UpdatedDatetime", null);
                d.Add("IsAuth", false);

                CmnDataManager objCmnDataManager = new CmnDataManager();

                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_SMTP_SETTINGS", "Save", d);
                if (rsltCode.isSuccessful)
                {

                    var res = Json(rsltCode);

                    return Json(res);

                }

                return Json(null);
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        #endregion
        public IActionResult TaxableIncomeUpload()
        {
            return View();
        }
        public IActionResult TestPayroll_EmployeeDialog()
        {
            return View();
        }
        public IActionResult TestPayroll_ProcessDialog()
        {
            return View();
        }
        public IActionResult TestPayrollProcess()
        {
            return View();
        }

        #region TIREntry

        public ActionResult TIREntry()
        {
            return View();
        }

        public JsonResult AuthorizationFor_TIREntry(string Action, int ID)
        {
            string SessionUser = HttpContext.Session.GetString("USER_ID").ToString();
            Dictionary<string, object> d = new Dictionary<string, object>();
            if (Action == "Authorize")
            {
                d.Add("CheckerkID", SessionUser);
                d.Add("UpdatedDatetime", DateTime.Now);
                d.Add("ID", ID);
                CmnDataManager objCmnDataManager = new CmnDataManager();
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_TIREnt_TIR_MANAGER", "AUTHORIZE_TIR", d);
                if (rsltCode.isSuccessful)
                    return Json("Record authorized successfully.");
                else
                    return Json("Problem while authorizing data");
            }
            else if (Action == "Reject")
            {
                d.Add("ID", ID);
                CmnDataManager objCmnDataManager = new CmnDataManager();
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_TIREnt_TIR_MANAGER", "REJECT_TIR", d);
                if (rsltCode.isSuccessful)
                    return Json("Record rejected successfully.");
                else
                    return Json("Problem while recjecting data");

            }
            return Json(null);
        }

        public JsonResult tbl_UNAUTH_TIREntry()
        {
            string SessionUser = HttpContext.Session.GetString("USER_ID").ToString();

            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("MakerId", SessionUser);

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_TIREnt_TIR_MANAGER", "GET_UNAUTHORIZE_TIR", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        public JsonResult tbl_AUTH_TIREntry()
        {
            string SessionUser = HttpContext.Session.GetString("USER_ID").ToString();

            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("MakerId", SessionUser);

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_TIREnt_TIR_MANAGER", "List", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }


        [HttpPost]
        public JsonResult btn_LevelModal()
        {

            Dictionary<string, object> d = new Dictionary<string, object>();

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_TIREnt_TIR_MANAGER", "LEVEL_LOV", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        [HttpPost]

        public JsonResult UpsertTIREntrySP_YEAR(decimal SP_YEAR)
        {
            try
            {
                Dictionary<string, object> d = new Dictionary<string, object>();

                d.Add("SP_YEAR", SP_YEAR);

                CmnDataManager objCmnDataManager = new CmnDataManager();
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_TIREnt_TIR_MANAGER", "W_YEAR", d);
                if (rsltCode.isSuccessful)
                {
                    DataTable firstTable = rsltCode.dstResult.Tables[0];

                    //var res = Json(rsltCode.dstResult.Tables);

                    var CatResul = JsonConvert.SerializeObject(firstTable);
                    return Json(CatResul);
                }
                return Json(null);
            }
            catch (Exception ex)
            {

                throw;
            }
        }


        [HttpPost]

        public JsonResult UpsertTIREntry(decimal SP_YEAR, decimal SP_TIR_PER, string SP_LEVEL, decimal SP_RANK, decimal SP_MIN_PER, decimal SP_MAX_PER, decimal SP_AVERAGE, decimal SP_LOCAL_TIR, int ID)
        {
            try
            {
                Dictionary<string, object> d = new Dictionary<string, object>();

                d.Add("SP_YEAR", SP_YEAR);
                d.Add("SP_TIR_PER", SP_TIR_PER);
                d.Add("SP_LEVEL", SP_LEVEL);
                d.Add("SP_RANK", SP_RANK);
                d.Add("SP_MIN_PER", SP_MIN_PER);
                d.Add("SP_MAX_PER", SP_MAX_PER);
                d.Add("SP_AVERAGE", SP_AVERAGE);
                d.Add("SP_LOCAL_TIR", SP_LOCAL_TIR);
                d.Add("ID", ID);
                d.Add("MakerId", HttpContext.Session.GetString("USER_ID").ToString());
                d.Add("AddedDatetime", DateTime.Now);
                d.Add("IsAuth", false);
                d.Add("ID", ID);


                CmnDataManager objCmnDataManager = new CmnDataManager();
                if (ID > 0)
                {

                    Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_TIREnt_TIR_MANAGER", "Update", d);
                    if (rsltCode.isSuccessful)
                    {
                        var res = Json(rsltCode);
                        return Json(res);
                    }
                }
                else
                {
                    Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_TIREnt_TIR_MANAGER", "Save", d);
                    if (rsltCode.isSuccessful)
                    {
                        var res = Json(rsltCode);
                        return Json(res);
                    }
                }

                return Json(null);
            }
            catch (Exception ex)
            {

                throw;
            }
        }


        public ViewResponseModel DeleteTIREntry(TIREntryModel model)
        {
            return TIREntryService.getInstance().DeleteTIREntry(model);
        }

        #endregion

        #region UserNameSetup
        public ActionResult UserNameSetup()
        {
            var UserNameSetupoutput = UserNameSetupService.getInstance().GetAllUserNameSetup();
            return View(UserNameSetupoutput);
        }

        [HttpPost]
        public ActionResult UpsertUserNameSetup(CHRIS_Setup_Sp_CHRIS_USERS_GETALLResult input)
        {
            try
            {
                var res = UserNameSetupService.getInstance().AddUpdateUserNameSetup(input);
                if (res == -1)
                    Response.StatusCode = 200;
                else
                    Response.StatusCode = 400;

                return Json(res);
            }
            catch (Exception ex)
            {
                Response.StatusCode = 400;
                return Json(ex.Message);
            }
        }


        public ViewResponseModel DeleteUserNameSetup(UserNameSetupModel model)
        {
            return UserNameSetupService.getInstance().DeleteUserNameSetup(model);
        }

        #endregion
    }
}
