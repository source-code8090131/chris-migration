﻿using CHRIS.Common;
using CHRIS.Services;
using iCORE.Common;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Data;
using System.Data.SqlClient;

namespace CHRIS.Controllers
{
    public class ProcessingController : Controller
    {


        #region CBonusApproval_Bonus
        public IActionResult CBonusApproval()
        {
            return View();
        }
      
        public IActionResult CBonusApproval_Bonus()
        {
            return View();
        }

        public JsonResult btn_Cal_CBAB(string w_year,Decimal? w_10_c)
        {

            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("w_year", w_year);
            d.Add("w_10_c", w_10_c);


            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_BonusApproval_Manager", "list", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }
        public IActionResult CBonusApproval_BonusPercentDialog()
        {
            return View();
        }
        public IActionResult CBonusApproval_BonusSaveRecordDialog()
        {
            return View();
        }
        public IActionResult CBonusApproval_BonusYearDialog()
        {
            return View();
        }

        #endregion

        #region FinancePayrollHistoryUpdationProcess
        public IActionResult FinancePayrollHistoryUpdationProcess()
        {
            return View();
        }

        public JsonResult btn_PRO_FPHUP()
        {

            Dictionary<string, object> d = new Dictionary<string, object>();
           
            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_FINANCEPAYROLLHISTORY_MANAGER", "", d);
            if (rsltCode.isSuccessful)
            {                
                return Json("SUCCESSFULL END OF PROGRAM");
            }
            return Json(null);
        }

        public IActionResult FPHistoryAnswerDialog()
        {
            return View();
        }
        public IActionResult FPHistoryFinanceDialog()
        {
            return View();
        }
        #endregion

        #region IncrementFinalizationProcess
        public IActionResult IFProcessAnswerDialog()
        {
            return View();
        }
        public IActionResult IncrementFinalizationProcess()
        {
            return View();
        }

        public JsonResult btn_INC_FP()
        {
            DataTable dtIFP = new DataTable();
            Result rsltIFP, rsltIFPDetail;
            CmnDataManager cmnDM = new CmnDataManager();
            Dictionary<string, object> colsNVals = new Dictionary<string, object>();

            string txtReply = "YES";
            string MessageBox = "";
            if (txtReply == "YES" || txtReply == "NO")
            {
                rsltIFP = cmnDM.GetData("CHRIS_SP_INCREMENT_FINALIZATION_MANAGER", "Initial", colsNVals);

                if (txtReply == "YES")
                {
                    if (rsltIFP.isSuccessful)
                    {
                        if (rsltIFP.dstResult.Tables.Count > 0 && rsltIFP.dstResult.Tables[0].Rows.Count > 0)
                        {
                            MessageBox = "FINALIZATION PROCESS HAS ALREADY BEEN DONE";
                           
                        }
                        else if (rsltIFP.dstResult.Tables.Count > 0 && rsltIFP.dstResult.Tables[0].Rows.Count == 0)
                        {
                            rsltIFP = cmnDM.GetData("CHRIS_SP_INCREMENT_FINALIZATION_MANAGER", "GetTermFlag", colsNVals);
                            if (rsltIFP.dstResult.Tables.Count > 0 && rsltIFP.dstResult.Tables[0].Rows[0].ItemArray[0].ToString() != "0")
                            {
                                MessageBox = "Processing Is In Progress On Other Terminal .......!";
                              
                            }
                            else
                            {
                                rsltIFP = cmnDM.GetData("CHRIS_SP_INCREMENT_FINALIZATION_MANAGER", "UpdateTermFlag", colsNVals);
                            }

                            rsltIFP = cmnDM.Execute("CHRIS_SP_INCREMENT_FINALIZATION_MANAGER", "UpdateIncPromo", colsNVals, 900000);
                        }
                        var res = new
                        {
                            MessageBox = MessageBox,
                        };

                        return Json(res);
                    }
                }
                else
                {
                    MessageBox = "Something Went Wrong....!";
                    var res = new
                    {
                        MessageBox = MessageBox,
                    };

                    return Json(res);
                }
            }
            return Json(null);
        }

        #endregion

        #region LeavesYearEnd
        public IActionResult LeavesDialog()
        {
            return View();
        }

        public IActionResult LeavesYearEnd()
        {
            return View();
        }

        public JsonResult btn_LeavesYearEnd()
        {
            string MessageBox = "";

            try
            {              

                Dictionary<String, Object> paramWithVals = new Dictionary<String, Object>();
                Result rsltCode;
                DataSet dsResult = new DataSet();

                rsltCode = cmnDM.Execute("CHRIS_SP_LEAVES_MANAGER", "Delete", paramWithVals);
                if (rsltCode.isSuccessful)
                {
                    if (rsltCode.hstOutParams["Table0.ORETVAL0"].ToString() == "-40")
                    {
                        MessageBox = "Processing Is In Progress On Other Terminal .......!";
                          return Json(MessageBox);
                    }
                    else
                    {
                        MessageBox = "Processing Complete ...";
                          return Json(MessageBox);
                    }
                }
                else
                {
                    MessageBox = "Something went wrong";
                    return Json(MessageBox);
                }
            }
            catch 
            {
                MessageBox = "Something went wrong";
                return Json(MessageBox);
            }
            MessageBox = "Something went wrong";
            return Json(MessageBox);
        }

        #endregion

        #region MonthEndProcess

        public IActionResult MonthEndProcess()
        {
            return View();
        }

        public ActionResult Submit_MonthEndProcess()
        {            
           string TE_YEAR = "";
           string TE_P_NO = "";
           string TE_MONTH = "";
           string TE_BRANCH = "";
           string TE_DESIG = "";
           string TE_CATEGORY = "";
           string TE_EMPLOYEE_TYPE = "";
           string TE_BASIC = "";
           string TE_HOUSE = "";
           string TE_CONV = "";
           string TE_PFUND = "";
           string TE_OT_HRS = "";
           string TE_SEGMENT = "";
           string TE_GROUP = "";
           string TE_DEPT = "";
           string TE_CONTRIB_TOTAL = "";
           string TE_OT_COST = "";
           string TE_ATT_AWD = "";
           string TE_MEAL = "";
           string TE_OTHERS = "";
           string MessageBox = "";
           
            Result rsltMain, rsltMonth, rsltMnthSumm;
            CmnDataManager cmnDM = new CmnDataManager();
            Dictionary<string, object> colsNVals = new Dictionary<string, object>();
            try
            {

                DataTable dtMEP = new DataTable();

                //CALLING THE MONTHEND INITIAL//
                rsltMain = cmnDM.GetData("CHRIS_SP_MONTHENDPROCESS_MAIN", "Initial", colsNVals);
                if (rsltMain.isSuccessful)
                {
                    if (rsltMain.dstResult.Tables.Count > 0)
                    {
                        if (rsltMain.dstResult.Tables[0].Rows.Count > 0)
                        {
                            MessageBox = "Processing Is In Progress On Other Terminal .......!";
                            return Json(MessageBox);
                        }
                    }

                    rsltMain = cmnDM.Execute("CHRIS_SP_MONTHENDPROCESS_MAIN", "UpdateTermFlag", colsNVals);
                }
                //CALLING THE P_MONTH PROCEDURES [CONTAIN 5 SPs]//
                rsltMonth = cmnDM.GetData("CHRIS_SP_MONTHENDPROCESS_P_MONTH", "", colsNVals);
               // var res = null;
                if (rsltMonth.isSuccessful)
                {
                    //MessageBox.Show("count of Month =   " + rsltMonth.dstResult.Tables.Count); 
                    rsltMnthSumm = cmnDM.GetData("CHRIS_SP_MONTHENDPROCESS_P_MONTH_SUMMARY", "", colsNVals);
                    if (rsltMnthSumm.isSuccessful)
                    {
                        if (rsltMnthSumm.dstResult.Tables.Count > 0)
                        {
                            dtMEP = rsltMnthSumm.dstResult.Tables[0];
                            if (dtMEP.Rows.Count > 0)
                            {

                                //for (int j = 0; j < dtMEP.Rows.Count; j++)
                                foreach (DataRow drw in dtMEP.Rows)
                                {

                                    TE_YEAR = (drw.ItemArray[22].ToString().Equals(string.Empty)) ? "0" : drw.ItemArray[22].ToString();
                                    TE_P_NO = (drw.ItemArray[0].ToString().Equals(string.Empty)) ? "0" : drw.ItemArray[0].ToString();
                                    TE_MONTH = (drw.ItemArray[1].ToString().Equals(string.Empty)) ? "0" : drw.ItemArray[1].ToString();
                                    TE_BRANCH = (drw.ItemArray[2].ToString().Equals(string.Empty)) ? "0" : drw.ItemArray[2].ToString();
                                    TE_DESIG = (drw.ItemArray[6].ToString().Equals(string.Empty)) ? "0" : drw.ItemArray[6].ToString();
                                    TE_CATEGORY = (drw.ItemArray[7].ToString().Equals(string.Empty)) ? "0" : drw.ItemArray[7].ToString();
                                    TE_EMPLOYEE_TYPE = (drw.ItemArray[8].ToString().Equals(string.Empty)) ? "0" : drw.ItemArray[8].ToString();
                                    TE_BASIC = (drw.ItemArray[11].ToString().Equals(string.Empty)) ? "0" : drw.ItemArray[11].ToString();
                                    TE_HOUSE = (drw.ItemArray[13].ToString().Equals(string.Empty)) ? "0" : drw.ItemArray[13].ToString();
                                    TE_CONV = (drw.ItemArray[14].ToString().Equals(string.Empty)) ? "0" : drw.ItemArray[14].ToString();
                                    TE_PFUND = (drw.ItemArray[12].ToString().Equals(string.Empty)) ? "0" : drw.ItemArray[12].ToString();
                                    TE_OT_HRS = (drw.ItemArray[15].ToString().Equals(string.Empty)) ? "0" : drw.ItemArray[15].ToString();
                                    TE_SEGMENT = (drw.ItemArray[3].ToString().Equals(string.Empty)) ? "0" : drw.ItemArray[3].ToString();
                                    TE_GROUP = (drw.ItemArray[4].ToString().Equals(string.Empty)) ? "0" : drw.ItemArray[4].ToString();
                                    TE_DEPT = (drw.ItemArray[5].ToString().Equals(string.Empty)) ? "0" : drw.ItemArray[5].ToString();
                                    TE_CONTRIB_TOTAL = (drw.ItemArray[10].ToString().Equals(string.Empty)) ? "0" : drw.ItemArray[10].ToString();
                                    TE_OT_COST = (drw.ItemArray[16].ToString().Equals(string.Empty)) ? "0" : drw.ItemArray[16].ToString();
                                    TE_ATT_AWD = (drw.ItemArray[9].ToString().Equals(string.Empty)) ? "0" : drw.ItemArray[9].ToString();
                                    TE_MEAL = (drw.ItemArray[17].ToString().Equals(string.Empty)) ? "0" : drw.ItemArray[17].ToString();
                                    TE_OTHERS = (drw.ItemArray[18].ToString().Equals(string.Empty)) ? "0" : drw.ItemArray[18].ToString();
                                   

                                }

                            }
                            else
                            {
                                TE_YEAR = "0";
                                TE_P_NO = "0";
                                TE_MONTH = "0";
                                TE_BRANCH = "0";
                                TE_DESIG = "0";
                                TE_CATEGORY = "0";
                                TE_EMPLOYEE_TYPE = "0";
                                TE_BASIC = "0";
                                TE_HOUSE = "0";
                                TE_CONV = "0";
                                TE_PFUND = "0";
                                TE_OT_HRS = "0";
                                TE_SEGMENT = "0";
                                TE_GROUP = "0";
                                TE_DEPT = "0";
                                TE_CONTRIB_TOTAL = "0";
                                TE_OT_COST = "0";
                                TE_ATT_AWD = "0";
                                TE_MEAL = "0";
                                TE_OTHERS = "0";                               
                            }
                            
                        }                        
                    }
                    else
                    {
                        throw new Exception(rsltMnthSumm.message);
                    }                    
                }
                var res = new
                {
                    TE_YEAR = TE_YEAR,
                    TE_P_NO = TE_P_NO,
                    TE_MONTH = TE_MONTH,
                    TE_BRANCH = TE_BRANCH,
                    TE_DESIG = TE_DESIG,
                    TE_CATEGORY = TE_CATEGORY,
                    TE_EMPLOYEE_TYPE = TE_EMPLOYEE_TYPE,
                    TE_BASIC = TE_BASIC,
                    TE_HOUSE = TE_HOUSE,
                    TE_CONV = TE_CONV,
                    TE_PFUND = TE_PFUND,
                    TE_OT_HRS = TE_OT_HRS,
                    TE_SEGMENT = TE_SEGMENT,
                    TE_GROUP = TE_GROUP,
                    TE_DEPT = TE_DEPT,
                    TE_CONTRIB_TOTAL = TE_CONTRIB_TOTAL,
                    TE_OT_COST = TE_OT_COST,
                    TE_ATT_AWD = TE_ATT_AWD,
                    TE_MEAL = TE_MEAL,
                    TE_OTHERS = TE_OTHERS,
                };
                rsltMain = cmnDM.Execute("CHRIS_SP_MONTHENDPROCESS_MAIN", "UpdateTermFlagToNull", colsNVals);
                return Json(res);
            }
            catch (Exception exp)
            {
                Logging("", "CHRIS_Processing_MonthEndProcessDetail_Shown" + exp);
                rsltMain = cmnDM.Execute("CHRIS_SP_MONTHENDPROCESS_MAIN", "UpdateTermFlagToNull", colsNVals);
            }
            return Json(null);
        }
       
        


        public IActionResult MonthEndProcessDetail()
        {
            return View();
        }
        #endregion

        #region PayrollGeneration
        public IActionResult PayrollGeneration()
        {
            return View();
        }       

        #region Feilds
        DateTime? Sys_Date= new DateTime?();
        string slTB_Transport_Vp = "";
        string MessageBox = "";
        string slTB_PF_Exempt = "";
        string slTB_Reply_For_Generation = "";
        string slTB_TotalTaxableAmount = "";
        string slTB_LoanTaxAmount = "";
        //*******dtPersonnels will contain personnel records,fetched via cursor in oracle.*******
        DataTable dtPersonnels = null;
        //******* not included in current logic.*******
        DataTable dtEmpPayRollDetail = null;
        CmnDataManager cmnDM = new CmnDataManager();
        //*******for Pre-processing like term-set.*******
        Result rslt;
        //******* used for main processing.*******
        Result flowRslt;
        //*******Payroll processing status,"Failure" will abort processing.******* 
        public enum FormMode { Processing, Failure, NotStarted, Successfull, PreReqPassed };
        private FormMode processState;
        //ds used for data manipulation and management while processing. 
        Dictionary<string, object> dicEmpPayRollDetail = new Dictionary<string, object>();
        Dictionary<string, object> paramList = new Dictionary<string, object>();
        DataManager DM = new CommonDataManager();

        /********payroll form once entered in "Processing" mode ,will use this SQL trans,and if any
         error/exception occurs whole process will be aborted and payroll mode will be "Failure" ******/
        SqlTransaction processTrans = null;
        SqlConnection proCon = new SqlConnection();
        // private static XMS.DATAOBJECTS.ConnectionBean connbean = null;
        private String GLOBAL_W_10_AMT;
        private String GLOBAL_W_INC_AMT;
        private String GLOBAL_PERKS;

        bool SetFieldStatue = true;
        public int MARKUP_RATE_DAYS = -1;
        #endregion
      
        public void Logging(string typeName, string functionName)
        {
            // code starts for creating log file starts here this code will be appended to main menu file
            string moduleName = "Payroll";

            string complete_path = "C:/iCORE-Logs/" + moduleName + "-Logs/";
            string filename = DateTime.Now.Date.Day + "_" + DateTime.Now.Date.Month + "_" + DateTime.Now.Date.Year + ".txt";
            string path = complete_path + filename;
            bool bool_icorelogs = false; bool bool_icorelogs_rps_logs = false;

            if (System.IO.Directory.Exists("C:/iCORE-Logs"))
            {
                bool_icorelogs = true;
            }
            else
            {
                System.IO.DirectoryInfo dr = System.IO.Directory.CreateDirectory("C:/Payroll-Logs");
                bool_icorelogs = true;
            }

            if (bool_icorelogs)
            {
                if (System.IO.Directory.Exists(complete_path))
                {
                    bool_icorelogs_rps_logs = true;
                }
                else
                {
                    System.IO.DirectoryInfo dr = System.IO.Directory.CreateDirectory(complete_path);
                    bool_icorelogs_rps_logs = true;
                }
            }

            if (bool_icorelogs_rps_logs)
            {
                System.IO.FileStream fs = new System.IO.FileStream(path, System.IO.FileMode.Append, System.IO.FileAccess.Write);

                // Writing log begins            
                System.IO.StreamWriter writer = new System.IO.StreamWriter(fs);
                writer.WriteLine("---------------------------------------------------------------");

                writer.WriteLine("Time of logging: " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString());
                writer.WriteLine("Class/Form/Report Name: " + typeName);
                writer.WriteLine("Function Name: " + functionName);

                writer.WriteLine("--------------------------------------------------------------- ");
                writer.Close();
                //Writing log ends
                fs.Close();
                //System.Windows.Forms.MessageBox.Show("Error found! check the log at: " + path );
            }

            // code for creating log file ends here
        }
        public ActionResult Save(string target)
        {
            try
            {
                if (target != string.Empty)
                {
                    if (target.ToUpper() == "YES")
                    {
                        if (processState == FormMode.Successfull)
                            processTrans.Commit();
                        TermSet_Clear();
                       

                    }
                    else if (target.ToUpper() == "NO")
                    {
                        if (processState == FormMode.Successfull)
                            processTrans.Rollback();
                        TermSet_Clear();
                        
                    }
                    else
                    {
                        
                    }
                }
            }
            catch (Exception exce)
            {
                if (processTrans != null)
                    processTrans.Rollback();
                //if (conn.State != ConnectionState.Closed)
                //    conn.Close();
               // base.LogException(this.Name, "slTB_FinalAnswer_Validating", exce);
            }
            return Json(null);
        }
        private void TermSet_Clear()
        {
            this.rslt = cmnDM.Get("CHRIS_SP_TERM_MANAGER", "UpdateToNull");
            if (this.rslt.isSuccessful)
            {
                
            }
        }
        public ActionResult TB_Reply_For_Generation_YES_NO(string TB_Reply_For_Generation,DateTime? Sys_Date,string TB_No)
        {
            if (TB_Reply_For_Generation != string.Empty)
            {
                if (TB_Reply_For_Generation.ToUpper() == "YES")
                {
                    PreProcessing(Sys_Date, TB_No);

                    return Json(MessageBox);

                }
                else if (TB_Reply_For_Generation.ToUpper() == "NO")
                {
                    return Json(null);
                }
                else
                {
                    return Json("Enter YES Or NO");

                }
            }
            return Json(null);
         }
        private void PreProcessing(DateTime? Sys_Date, string TB_No)
        {
            if (!IsPayRollAlreadyGenerated(Sys_Date))
            {
                TermSet();                
                StartProcessing(Sys_Date, TB_No);                
            }
            else
            {
                return;
            }
        }
        private bool IsPayRollAlreadyGenerated(DateTime? Sys_Date)
        {
            
            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Clear();
            param.Add("W_Date", Sys_Date);
            CmnDataManager objCmnDataManager = new CmnDataManager();

             rslt = objCmnDataManager.GetData("CHRIS_SP_TERM_MANAGER", "IsPayRollGenerated", param);
            if (rslt.isSuccessful)
            {
                if (rslt.dstResult.Tables[0].Rows.Count == 0)
                {
                   
                    return false;
                }
                else
                {                   
                    DateTime dt = Convert.ToDateTime(Sys_Date);
                    MessageBox = "PAYROLL HAS ALREADY BEEN GENERATED FOR " + dt.ToString("MMM-yyyy").ToUpper();
                    return true;
                }
            }
            MessageBox = rslt.message;
            return true;
        }
        private void TermSet()
        {
            //*************will check if processing is going on another terminal.************
           

           CmnDataManager objCmnDataManager = new CmnDataManager();

            rslt = objCmnDataManager.Get("CHRIS_SP_TERM_MANAGER", "GetTermStatus");
            if (rslt.isSuccessful)
            {
                if (rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString() == "0")
                {
                    rslt = objCmnDataManager.Get("CHRIS_SP_TERM_MANAGER", "UpdateToProcess");
                    if (rslt.isSuccessful)
                    {                        
                        
                    }
                }
                else
                {                   
                    MessageBox = "Processing Is In Progress On Other Terminal .......!";
                }
            }
            else
            {
                MessageBox = "Processing Is In Progress On Other Terminal .......!";
            }
        }
        private void StartProcessing(DateTime? Sys_Date, string TB_No)
        {
            GetAllEmployees();
            InitializeList();
            InitiateProcess(Sys_Date, TB_No);            
        }    
        private void InitializeList()
        {
            dicEmpPayRollDetail.Clear();
            dicEmpPayRollDetail.Add("PR_P_NO", null);
            dicEmpPayRollDetail.Add("W_AN_PACK", null);
            dicEmpPayRollDetail.Add("W_CATE", null);
            dicEmpPayRollDetail.Add("W_CONF_FLAG", null);
            dicEmpPayRollDetail.Add("W_DESIG", null);
            dicEmpPayRollDetail.Add("W_LEVEL", null);
            dicEmpPayRollDetail.Add("W_ZAKAT", null);
            dicEmpPayRollDetail.Add("W_PRV_INC", null);
            dicEmpPayRollDetail.Add("W_PRV_TAX_PD", null);
            dicEmpPayRollDetail.Add("W_TAX_USED", null);
            dicEmpPayRollDetail.Add("W_TRANS_DATE", null);
            dicEmpPayRollDetail.Add("W_PR_MONTH", null);
            dicEmpPayRollDetail.Add("W_JOINING_DATE", null);
            dicEmpPayRollDetail.Add("W_BRANCH", null);
            dicEmpPayRollDetail.Add("W_PR_DATE", null);
            dicEmpPayRollDetail.Add("W_REFUND", null);
            dicEmpPayRollDetail.Add("W_ATT_FLAG", null);
            dicEmpPayRollDetail.Add("W_PAY_FLAG", null);
            dicEmpPayRollDetail.Add("W_CONFIRM_ON", null);


            dicEmpPayRollDetail.Add("W_ASR", 0.00);
            dicEmpPayRollDetail.Add("W_HRENT", 0.00);
            dicEmpPayRollDetail.Add("W_CONV", 0.00);
            dicEmpPayRollDetail.Add("W_GRAT", 0.00);
            dicEmpPayRollDetail.Add("W_GOVT_ALL", 0.00);
            dicEmpPayRollDetail.Add("MDAYS", 0.00);
            dicEmpPayRollDetail.Add("W_DAYS", 0.00);
            dicEmpPayRollDetail.Add("W_DIVISOR", 0.00);
            dicEmpPayRollDetail.Add("WW_BASIC", 0.00);
            dicEmpPayRollDetail.Add("WW_CONV", 0.00);
            dicEmpPayRollDetail.Add("WW_HOUSE", 0.00);
            dicEmpPayRollDetail.Add("W_FLAG", null);
            dicEmpPayRollDetail.Add("W_DEPT_FT", null);
            dicEmpPayRollDetail.Add("W_SW_PAY", 1.00);

            dicEmpPayRollDetail.Add("WW_DATE", null);
            dicEmpPayRollDetail.Add("W_DATE", null);

            dicEmpPayRollDetail.Add("W_SAL_ADV", 0.00);
            dicEmpPayRollDetail.Add("W_PROMOTED", null);
            dicEmpPayRollDetail.Add("W_PFUND", 0.00);
            dicEmpPayRollDetail.Add("GLOBAL_FLAG", null);
            //dicEmpPayRollDetail.Add("W_PR_MONTH", null);
            dicEmpPayRollDetail.Add("GLOBAL_PERKS", GLOBAL_PERKS);
            dicEmpPayRollDetail.Add("W_OT_ASR", 0.00);
            //proc_6
            dicEmpPayRollDetail.Add("W_PA_DATE", null);
            dicEmpPayRollDetail.Add("W_INC_MONTHS", 0.00);
            dicEmpPayRollDetail.Add("W_BASIC_AREAR", 0.00);
            dicEmpPayRollDetail.Add("W_HOUSE_AREAR", 0.00);
            dicEmpPayRollDetail.Add("W_CONV_AREAR", 0.00);
            dicEmpPayRollDetail.Add("W_PF_AREAR", 0.00);
            dicEmpPayRollDetail.Add("GLOBAL_W_FOUND", null);
            //Sal_advance
            dicEmpPayRollDetail.Add("W_WORK_DAY", null);
            //dicEmpPayRollDetail.Add("W_SAL_ADV", 0.00);
            //FIN BOOK
            dicEmpPayRollDetail.Add("W_MARKUP_L", 0.00);
            dicEmpPayRollDetail.Add("W_INST_L", 0.00);
            dicEmpPayRollDetail.Add("W_MARKUP_F", 0.00);
            dicEmpPayRollDetail.Add("W_INST_F", 0.00);
            dicEmpPayRollDetail.Add("W_EXCISE", 0.00);
            dicEmpPayRollDetail.Add("W_FINANCE", null);
            dicEmpPayRollDetail.Add("W_MONTH_DED", 0.00);
            dicEmpPayRollDetail.Add("W_EXTRA", null);
            dicEmpPayRollDetail.Add("W_ITAX", 0.00);

            dicEmpPayRollDetail.Add("WPER_DAY", null);

            // Z_TAX
            dicEmpPayRollDetail.Add("W_PF_EXEMPT", slTB_PF_Exempt);
            dicEmpPayRollDetail.Add("W_VP", slTB_Transport_Vp);
            dicEmpPayRollDetail.Add("W_SYS", Sys_Date);
            dicEmpPayRollDetail.Add("GLOBAL_W_10_AMT", GLOBAL_W_10_AMT);
            dicEmpPayRollDetail.Add("GLOBAL_W_INC_AMT", GLOBAL_W_INC_AMT);
            dicEmpPayRollDetail.Add("SUBS_LOAN_TAX_AMT", 0.00);
            dicEmpPayRollDetail.Add("Z_TAXABLE_AMOUNT", 0.00);


        }
        private void GetAllEmployees()
        {
            //fetch all employees with condition described in cursor emp_no(oracle form,Valid_Pno). 
            CmnDataManager objCmnDataManager = new CmnDataManager();

            Result rslt = objCmnDataManager.GetData("CHRIS_SP_GET_PERSONNEL_INFO", "");

            if (rslt.isSuccessful)
            {
                if (rslt.dstResult != null && rslt.dstResult.Tables[0].Rows.Count > 0)
                {                    
                    dtPersonnels = rslt.dstResult.Tables[0];
                }
            }
        }
        private void InitiateProcess(DateTime? Sys_Date, string TB_No)
        {
            try
            {
                
                    //proCon = connbean.getDatabaseConnection();
                    //processTrans = proCon.BeginTransaction();
                    int personnel_Cursor = 0;
                    DateTime dt = Convert.ToDateTime(Sys_Date);
                    DateTime dtWW_DATE = DateTime.Now;
                    int W_SW_PAY = 1;
                    dicEmpPayRollDetail["W_DATE"] = dt;

                    if (dtPersonnels != null)
                    {

                        for (int rowPersonnelNum = 0; rowPersonnelNum < dtPersonnels.Rows.Count; rowPersonnelNum++)
                        {
                           
                            string msg = string.Empty;
                            try
                            {
                                TB_No = dtPersonnels.Rows[rowPersonnelNum]["PR_P_NO"].ToString();
                                
                                FillPayRollDataWithPersonnelBasicInfo(rowPersonnelNum);//(rowPersonnelNum);
                                dicEmpPayRollDetail["W_DATE"] = dt;
                                dicEmpPayRollDetail["WW_DATE"] = dtWW_DATE;
                                dicEmpPayRollDetail["W_SW_PAY"] = W_SW_PAY;
                                #region promotion_proc
                            //********************calling procedure to check promotion****************************
                                TB_No = "";
                                paramList.Clear();
                                paramList.Add("PR_P_NO",dicEmpPayRollDetail["PR_P_NO"]);
                                paramList.Add("W_PR_DATE", dicEmpPayRollDetail["W_PR_DATE"]);
                                paramList.Add("W_DESIG", dicEmpPayRollDetail["W_DESIG"]);
                                paramList.Add("W_LEVEL", dicEmpPayRollDetail["W_LEVEL"]);
                                paramList.Add("W_PROMOTED", dicEmpPayRollDetail["W_PROMOTED"]);
                                flowRslt = cmnDM.Execute("CHRIS_SP_PROMOTION_PROC", "", paramList, ref processTrans, 60);
                                //SetStatusBar("Processing in Promotion (promotion_proc)");
                                if (flowRslt.isSuccessful)
                                {
                                    if (flowRslt.dstResult != null && flowRslt.dstResult.Tables.Count > 0 && flowRslt.dstResult.Tables[0].Rows.Count > 0)
                                    {
                                        FillListWithOutputValues();
                                    }
                                }
                                #endregion
                            }
                            catch (Exception exp)
                            {                               
                                MessageBox = "InitiateProcess--CHRIS_SP_PROMOTION_PROC" + exp;
                            }
                            finally
                            {
                              Logging("", "PROMOTION_PROC PROCESS Run SUCCESSFULLY FOR PR_P_NO: " + dicEmpPayRollDetail["PR_P_NO"]);
                               // MessageBox = "PROMOTION_PROC PROCESS Run SUCCESSFULLY FOR PR_P_NO: " + dicEmpPayRollDetail["PR_P_NO"];
                            }
                            #region Govt_allowance

                            if (flowRslt.isSuccessful)
                            {
                                //********************calling procedure for govt. allowance amount ------**********
                                try
                                {

                                    TB_No = "";
                                    paramList.Clear();
                                    paramList.Add("PR_P_NO",dicEmpPayRollDetail["PR_P_NO"]);
                                    paramList.Add("W_DATE", dicEmpPayRollDetail["W_DATE"]);
                                    paramList.Add("W_BRANCH", dicEmpPayRollDetail["W_BRANCH"]);
                                    paramList.Add("W_DESIG", dicEmpPayRollDetail["W_DESIG"]);
                                    paramList.Add("W_LEVEL", dicEmpPayRollDetail["W_LEVEL"]);
                                    paramList.Add("W_CATE", dicEmpPayRollDetail["W_CATE"]);
                                    paramList.Add("W_AN_PACK", dicEmpPayRollDetail["W_AN_PACK"]);
                                    paramList.Add("W_ASR", dicEmpPayRollDetail["W_ASR"]);
                                    paramList.Add("W_HRENT", dicEmpPayRollDetail["W_HRENT"]);
                                    paramList.Add("W_CONV", dicEmpPayRollDetail["W_CONV"]);
                                    paramList.Add("W_GRAT", dicEmpPayRollDetail["W_GRAT"]);
                                    paramList.Add("W_GOVT_ALL", dicEmpPayRollDetail["W_GOVT_ALL"]);
                                    flowRslt = cmnDM.Execute("CHRIS_SP_GOVT_ALL_AND_ALLOWANCES", "", paramList, ref processTrans, 60);
                                   // SetStatusBar("Processing in Govt. Allowance (govt_all)");
                                   
                                    if (flowRslt.isSuccessful)
                                    {
                                        if (flowRslt.dstResult != null && flowRslt.dstResult.Tables[0].Rows.Count > 0)
                                        {
                                            FillListWithOutputValues();
                                        }

                                    }
                                }
                                catch (Exception exp)
                                {
                                    MessageBox = "InitiateProcess--CHRIS_SP_GOVT_ALL_AND_ALLOWANCES"+ exp;
                                    msg = MessageBox.ToString();
                                }
                                finally
                                {
                                Logging("", "GOVT_ALLOWANCE PROCESS Run SUCCESSFULLY FOR PR_P_NO: " + dicEmpPayRollDetail["PR_P_NO"]);
                               // MessageBox = "GOVT_ALLOWANCE PROCESS Run SUCCESSFULLY FOR PR_P_NO: " + dicEmpPayRollDetail["PR_P_NO"];
                                }
                            }
                            #endregion

                            #region Provident Fund
                            if (flowRslt.isSuccessful)
                            {
                                try
                                {
                                //--************* Provident Fund Calculation **************************--
                                    TB_No = "5000412";
                                    paramList.Clear();
                                    paramList.Add("W_CONF_FLAG", dicEmpPayRollDetail["W_CONF_FLAG"]);
                                    paramList.Add("W_DATE", dicEmpPayRollDetail["W_DATE"]);
                                    paramList.Add("W_CONFIRM_ON", dicEmpPayRollDetail["W_CONFIRM_ON"]);
                                    paramList.Add("W_AN_PACK", dicEmpPayRollDetail["W_AN_PACK"]);
                                    paramList.Add("W_CATE", dicEmpPayRollDetail["W_CATE"]);
                                    paramList.Add("W_GOVT_ALL", dicEmpPayRollDetail["W_GOVT_ALL"]);
                                    paramList.Add("W_PFUND", dicEmpPayRollDetail["W_PFUND"]);

                                    flowRslt = cmnDM.Execute("CHRIS_SP_PROVIDENT_FUND", "", paramList, ref processTrans, 60);
                                    //SetStatusBar("Calculating P.F. (Valid_pno)");
                                    if (flowRslt.isSuccessful)
                                    {
                                        if (flowRslt.dstResult != null && flowRslt.dstResult.Tables[0].Rows.Count > 0)
                                        {
                                            FillListWithOutputValues();
                                        }

                                    }
                                }
                                catch (Exception exp)
                                {
                                    MessageBox = "InitiateProcess--CHRIS_SP_PROVIDENT_FUND"+ exp;
                                    msg = MessageBox.ToString();
                                }
                                finally
                                {
                                Logging("", "PROVIDENT FUND PROCESS Run SUCCESSFULLY FOR PR_P_NO: " + dicEmpPayRollDetail["PR_P_NO"]);
                                //MessageBox = "PROVIDENT FUND PROCESS Run SUCCESSFULLY FOR PR_P_NO: " + dicEmpPayRollDetail["PR_P_NO"];
                                }
                            }
                            #endregion

                            #region Proc Shift
                            if (flowRslt.isSuccessful)
                            {
                                //--************* shift PROC FOR ALLOWANCE TABLE UPDATION**************************--

                                try
                                {
                                    TB_No = "5000416";
                                    paramList.Clear();
                                    paramList.Add("PR_P_NO",dicEmpPayRollDetail["PR_P_NO"]);
                                    paramList.Add("W_DATE", dicEmpPayRollDetail["W_DATE"]);

                                    flowRslt = cmnDM.Execute("CHRIS_SP_PROC_SHIFT", "", paramList, ref processTrans, 60);
                                    //SetStatusBar("Processing in Allowance (proc_shift)");
                                    
                                    if (flowRslt.isSuccessful)
                                    {

                                    }
                                }
                                catch (Exception exp)
                                {
                                    MessageBox = "InitiateProcess--CHRIS_SP_PROC_SHIFT" + exp;
                                    msg = MessageBox.ToString();
                                }
                                finally
                                {
                                Logging("", "PROC SHIFT PROCESS Run SUCCESSFULLY FOR PR_P_NO: " + dicEmpPayRollDetail["PR_P_NO"]);
                               // MessageBox = "PROC SHIFT PROCESS Run SUCCESSFULLY FOR PR_P_NO: " + dicEmpPayRollDetail["PR_P_NO"];
                                }
                            }


                            #endregion

                            #region Overtime & inc_promo_check
                            if (flowRslt.isSuccessful)
                            {
                                //--************* -- check to calculate overtime for ctt to officers --  *******************--

                                DateTime pr_date, temp_w_date;
                                if (dicEmpPayRollDetail["W_PR_DATE"] != null && dicEmpPayRollDetail["W_PR_DATE"].ToString() != string.Empty)
                                {
                                    pr_date = Convert.ToDateTime(dicEmpPayRollDetail["W_PR_DATE"]);
                                    temp_w_date = Convert.ToDateTime(dicEmpPayRollDetail["W_DATE"]);
                                    if (pr_date.Month == temp_w_date.Month && pr_date.Year == temp_w_date.Year)
                                    {
                                        //--  flag up because inc_promo_check calls forcasted procedure to --********
                                        //TB_No = "";
                                        try
                                        {
                                            dicEmpPayRollDetail["GLOBAL_FLAG"] = "T";
                                            paramList.Clear();
                                            paramList.Add("PR_P_NO",dicEmpPayRollDetail["PR_P_NO"]);
                                            paramList.Add("W_DATE", dicEmpPayRollDetail["W_DATE"]);
                                            paramList.Add("GLOBAL_FLAG", dicEmpPayRollDetail["GLOBAL_FLAG"]);
                                            paramList.Add("W_ATT_FLAG", dicEmpPayRollDetail["W_ATT_FLAG"]);
                                            paramList.Add("W_BRANCH", dicEmpPayRollDetail["W_BRANCH"]);
                                            paramList.Add("W_ASR", dicEmpPayRollDetail["W_ASR"]);
                                            paramList.Add("W_GOVT_ALL", dicEmpPayRollDetail["W_GOVT_ALL"]);
                                            paramList.Add("W_PR_DATE", dicEmpPayRollDetail["W_PR_DATE"]);
                                            paramList.Add("W_PROMOTED", dicEmpPayRollDetail["W_PROMOTED"]);
                                            paramList.Add("W_OT_ASR", dicEmpPayRollDetail["W_OT_ASR"]);
                                            paramList.Add("W_DEPT_FT", dicEmpPayRollDetail["W_DEPT_FT"]);
                                            flowRslt = cmnDM.Execute("CHRIS_SP_INC_PROMO_CHECK", "", paramList, ref processTrans, 60);
                                           // SetStatusBar("Calculate OT For Ctt to Officers (inc_promo_check)");
                                            
                                            if (rslt.isSuccessful)
                                            {
                                                if (flowRslt.dstResult != null && flowRslt.dstResult.Tables[0].Rows.Count > 0)
                                                {
                                                    FillListWithOutputValues();
                                                }
                                            }
                                        }
                                        catch (Exception exp)
                                        {
                                            MessageBox = "InitiateProcess--CHRIS_SP_INC_PROMO_CHECK" + exp;
                                            msg = MessageBox.ToString();
                                        }
                                        finally
                                        {
                                        Logging("", "INC_PROMO_CHECK PROCESS Run SUCCESSFULLY FOR PR_P_NO: " + dicEmpPayRollDetail["PR_P_NO"]);
                                       // MessageBox = "INC_PROMO_CHECK PROCESS Run SUCCESSFULLY FOR PR_P_NO: " + dicEmpPayRollDetail["PR_P_NO"];
                                        }

                                    }
                                    else
                                    {
                                        //****************-- calculate ot for regular --*******************
                                        //TB_No = "";
                                        try
                                        {
                                            paramList.Clear();
                                            paramList.Add("PR_P_NO",dicEmpPayRollDetail["PR_P_NO"]);
                                            paramList.Add("W_DATE", dicEmpPayRollDetail["W_DATE"]);
                                            paramList.Add("W_ASR", dicEmpPayRollDetail["W_ASR"]);
                                            paramList.Add("W_GOVT_ALL", dicEmpPayRollDetail["W_GOVT_ALL"]);
                                            paramList.Add("W_DEPT_FT", dicEmpPayRollDetail["W_DEPT_FT"]);
                                            flowRslt = cmnDM.Execute("CHRIS_SP_OVER_TIME", "", paramList, ref processTrans, 60);
                                           // SetStatusBar("Processing in Overtime (over_time)");
                                            
                                            if (flowRslt.isSuccessful)
                                            {
                                                if (flowRslt.dstResult != null && flowRslt.dstResult.Tables[0].Rows.Count > 0)
                                                {
                                                    FillListWithOutputValues();
                                                }
                                            }
                                        }
                                        catch (Exception exp)
                                        {
                                            MessageBox = "InitiateProcess--CHRIS_SP_OVER_TIME"+ exp;
                                            msg = MessageBox.ToString();
                                        }
                                        finally
                                        {
                                        Logging("", "OVER_TIME PROCESS Run SUCCESSFULLY FOR PR_P_NO: " + dicEmpPayRollDetail["PR_P_NO"]);
                                       // MessageBox = "OVER_TIME PROCESS Run SUCCESSFULLY FOR PR_P_NO: " + dicEmpPayRollDetail["PR_P_NO"];
                                        }
                                    }
                                }
                                else
                                {
                                    //****************-- calculate ot for regular --*******************
                                    //TB_No = "";
                                    try
                                    {
                                        paramList.Clear();
                                        paramList.Add("PR_P_NO",dicEmpPayRollDetail["PR_P_NO"]);
                                        paramList.Add("W_DATE", dicEmpPayRollDetail["W_DATE"]);
                                        paramList.Add("W_ASR", dicEmpPayRollDetail["W_ASR"]);
                                        paramList.Add("W_GOVT_ALL", dicEmpPayRollDetail["W_GOVT_ALL"]);
                                        paramList.Add("W_DEPT_FT", dicEmpPayRollDetail["W_DEPT_FT"]);
                                        flowRslt = cmnDM.Execute("CHRIS_SP_OVER_TIME", "", paramList, ref processTrans, 60);
                                        //SetStatusBar("Processing in Overtime (over_time)");
                                       
                                        if (flowRslt.isSuccessful)
                                        {
                                            if (flowRslt.dstResult != null && flowRslt.dstResult.Tables[0].Rows.Count > 0)
                                            {
                                                FillListWithOutputValues();
                                            }
                                        }
                                    }
                                    catch (Exception exp)
                                    {
                                    MessageBox = "InitiateProcess--CHRIS_SP_OVER_TIME" + exp;
                                        msg = MessageBox.ToString();
                                    }
                                    finally
                                    {
                                    Logging("", "OVER_TIME ESLE CASE PROCESS Run SUCCESSFULLY FOR PR_P_NO: " + dicEmpPayRollDetail["PR_P_NO"]);
                                   // MessageBox = "OVER_TIME ESLE CASE PROCESS Run SUCCESSFULLY FOR PR_P_NO: " + dicEmpPayRollDetail["PR_P_NO"];
                                    }
                                }

                            }
                            #endregion

                            #region Proc_allow

                            if (flowRslt.isSuccessful)
                            {
                                if (dicEmpPayRollDetail["W_PR_MONTH"] != null && dicEmpPayRollDetail["W_PR_MONTH"].ToString() != string.Empty)
                                {
                                    int temp_month = Convert.ToInt32(dicEmpPayRollDetail["W_PR_MONTH"]);
                                    if (temp_month == 12 && dt.Month == 1) //i.e. W_date.monthName =="JAN"
                                    {
                                        try
                                        {
                                            TB_No = "50002";
                                            //SetStatusBar("Processing in Yearly Award (proc_allow)");
                                            paramList.Clear();
                                            paramList.Add("PR_P_NO",dicEmpPayRollDetail["PR_P_NO"]);
                                            paramList.Add("W_DATE", dicEmpPayRollDetail["W_DATE"]);
                                            paramList.Add("W_BRANCH", dicEmpPayRollDetail["W_BRANCH"]);
                                            paramList.Add("W_DESIG", dicEmpPayRollDetail["W_DESIG"]);
                                            paramList.Add("W_CATE", dicEmpPayRollDetail["W_CATE"]);
                                            
                                            flowRslt = cmnDM.Execute("CHRIS_SP_PROC_ALLOW", "", paramList, ref processTrans, 60);

                                        }
                                        catch (Exception exp)
                                        {
                                        MessageBox = "InitiateProcess--CHRIS_SP_PROC_ALLOW" + exp;
                                            msg = MessageBox.ToString();
                                        }
                                        finally
                                        {
                                        Logging("", "PROC_ALLOW PROCESS Run SUCCESSFULLY FOR PR_P_NO: " + dicEmpPayRollDetail["PR_P_NO"]);
                                       // MessageBox = "PROC_ALLOW PROCESS Run SUCCESSFULLY FOR PR_P_NO: " + dicEmpPayRollDetail["PR_P_NO"];
                                        }
                                    }
                                }
                            }

                            #endregion

                            #region att_award
                            if (flowRslt.isSuccessful)
                            {
                                if (dicEmpPayRollDetail["W_CATE"] != null && dicEmpPayRollDetail["W_CATE"].ToString() != string.Empty)
                                {
                                    if (dicEmpPayRollDetail["W_CATE"].ToString().Equals("C"))
                                    {
                                        try
                                        {
                                        TB_No  = "50007";
                                            //SetStatusBar("Processing in Att. Award (att_award)");
                                            paramList.Clear();
                                            paramList.Add("PR_P_NO",dicEmpPayRollDetail["PR_P_NO"]);
                                            paramList.Add("W_DATE", dicEmpPayRollDetail["W_DATE"]);
                                            paramList.Add("W_ATT_FLAG", dicEmpPayRollDetail["W_ATT_FLAG"]);
                                            paramList.Add("W_BRANCH", dicEmpPayRollDetail["W_BRANCH"]);
                                            paramList.Add("W_JOINING_DATE", dicEmpPayRollDetail["W_JOINING_DATE"]);
                                            flowRslt = cmnDM.Execute("CHRIS_SP_ATT_AWARD", "", paramList, ref processTrans, 60);
                                            
                                            if (flowRslt.isSuccessful)
                                            {
                                                if (flowRslt.dstResult != null && flowRslt.dstResult.Tables[0].Rows.Count > 0)
                                                {
                                                    FillListWithOutputValues();
                                                }
                                            }
                                        }
                                        catch (Exception exp)
                                        {
                                        MessageBox = "InitiateProcess--CHRIS_SP_ATT_AWARD"+exp;
                                            msg = MessageBox.ToString();
                                        }
                                        finally
                                        {
                                        Logging("", "ATT_AWARD PROCESS Run SUCCESSFULLY FOR PR_P_NO: " + dicEmpPayRollDetail["PR_P_NO"]);
                                      //  MessageBox = "ATT_AWARD PROCESS Run SUCCESSFULLY FOR PR_P_NO: " + dicEmpPayRollDetail["PR_P_NO"];
                                        }

                                    }
                                }
                            }
                            #endregion

                            #region proc_1

                            if (flowRslt.isSuccessful)
                            {
                                //Processing for Last Payroll Date (proc_1)
                                try
                                {
                                TB_No = "500016";
                                    //SetStatusBar("Processing for Last Payroll Date (proc_1)");
                                    paramList.Clear();
                                    paramList.Add("PR_P_NO",dicEmpPayRollDetail["PR_P_NO"]);
                                    paramList.Add("W_DATE", dicEmpPayRollDetail["W_DATE"]);
                                    paramList.Add("W_SW_PAY", dicEmpPayRollDetail["W_SW_PAY"]);
                                    paramList.Add("W_DAYS", dicEmpPayRollDetail["W_DAYS"]);
                                    paramList.Add("W_DIVISOR", dicEmpPayRollDetail["W_DIVISOR"]);
                                    paramList.Add("W_FLAG", dicEmpPayRollDetail["W_FLAG"]);
                                    paramList.Add("WW_DATE", dicEmpPayRollDetail["WW_DATE"]);
                                    paramList.Add("WW_BASIC", dicEmpPayRollDetail["WW_BASIC"]);
                                    paramList.Add("WW_CONV", dicEmpPayRollDetail["WW_CONV"]);
                                    paramList.Add("WW_HOUSE", dicEmpPayRollDetail["WW_HOUSE"]);
                                   
                                    flowRslt = cmnDM.Execute("CHRIS_SP_PROC_1", "", paramList, ref processTrans, 60);
                                    if (flowRslt.isSuccessful)
                                    {
                                        if (flowRslt.dstResult != null && flowRslt.dstResult.Tables[0].Rows.Count > 0)
                                        {
                                            FillListWithOutputValues();
                                        }
                                    }
                                }
                                catch (Exception exp)
                                {
                                MessageBox = "InitiateProcess--CHRIS_SP_PROC_1" +exp;
                                    msg = MessageBox.ToString();
                                }
                                finally
                                {
                                Logging("", "PROC_1 PROCESS Run SUCCESSFULLY FOR PR_P_NO: " + dicEmpPayRollDetail["PR_P_NO"]);
                                //MessageBox = "PROC_1 PROCESS Run SUCCESSFULLY FOR PR_P_NO: " + dicEmpPayRollDetail["PR_P_NO"];
                                }

                            }
                            #endregion

                            #region Proc_6
                            if (flowRslt.isSuccessful)
                            {
                                try
                                {
                                //-- TO CALCULATE BACK DATED SALARY AREARS --
                                TB_No = "500018";
                                   // SetStatusBar("Processing in Salary Arears (proc_6)");
                                    paramList.Clear();
                                    paramList.Add("PR_P_NO",dicEmpPayRollDetail["PR_P_NO"]);
                                    paramList.Add("W_DATE", dicEmpPayRollDetail["W_DATE"]);
                                    paramList.Add("W_CATE", dicEmpPayRollDetail["W_CATE"]);
                                    paramList.Add("W_PA_DATE", dicEmpPayRollDetail["W_PA_DATE"]);
                                    paramList.Add("W_INC_MONTHS", dicEmpPayRollDetail["W_INC_MONTHS"]);
                                    paramList.Add("W_FLAG", dicEmpPayRollDetail["W_FLAG"]);
                                    paramList.Add("W_BASIC_AREAR", dicEmpPayRollDetail["W_BASIC_AREAR"]);
                                    paramList.Add("W_HOUSE_AREAR", dicEmpPayRollDetail["W_HOUSE_AREAR"]);
                                    paramList.Add("W_CONV_AREAR", dicEmpPayRollDetail["W_CONV_AREAR"]);
                                    paramList.Add("W_PF_AREAR", dicEmpPayRollDetail["W_PF_AREAR"]);
                                    paramList.Add("GLOBAL_W_FOUND", dicEmpPayRollDetail["GLOBAL_W_FOUND"]);
                                   
                                    flowRslt = cmnDM.Execute("CHRIS_SP_PROC_6", "", paramList, ref processTrans, 60);
                                    if (flowRslt.isSuccessful)
                                    {
                                        if (flowRslt.dstResult != null && flowRslt.dstResult.Tables.Count > 0 && flowRslt.dstResult.Tables[0].Rows.Count > 0)
                                        {
                                            FillListWithOutputValues();
                                        }
                                    }
                                }
                                catch (Exception exp)
                                {
                                MessageBox = "InitiateProcess--CHRIS_SP_PROC_6" + exp;
                                    msg = MessageBox.ToString();
                                }
                                finally
                                {
                                Logging("", "PROC_6 PROCESS Run SUCCESSFULLY FOR PR_P_NO: " + dicEmpPayRollDetail["PR_P_NO"]);
                               // MessageBox = "PROC_6 PROCESS Run SUCCESSFULLY FOR PR_P_NO: " + dicEmpPayRollDetail["PR_P_NO"];
                                }

                            }
                            #endregion

                            #region Pro-Rate Income Or Income-Tax

                            if (flowRslt.isSuccessful)
                            {
                                //--*********** TO PICK ALLOWANCES IN CASE OF LAST JOIN AND JOIN B/W THE MONTH*********************--

                                //if (dicEmpPayRollDetail["W_FLAG"] != null && dicEmpPayRollDetail["W_FLAG"].ToString() != string.Empty)
                                //{
                                #region Pro-rate Income
                                if (dicEmpPayRollDetail["W_FLAG"].ToString() == "L" || dicEmpPayRollDetail["W_FLAG"].ToString() == "J")
                                {
                                    try
                                    {
                                    //--  ****Processing in Allowance for L,J (pro_rate_income)****
                                    TB_No = "500021";
                                        //SetStatusBar("Processing in Allowance for L,J (pro_rate_income)");
                                        paramList.Clear();
                                        paramList.Add("PR_P_NO",dicEmpPayRollDetail["PR_P_NO"]);
                                        paramList.Add("W_DATE", dicEmpPayRollDetail["W_DATE"]);
                                        paramList.Add("W_BRANCH", dicEmpPayRollDetail["W_BRANCH"]);
                                        paramList.Add("W_DESIG", dicEmpPayRollDetail["W_DESIG"]);
                                        paramList.Add("W_CATE", dicEmpPayRollDetail["W_CATE"]);
                                        paramList.Add("W_LEVEL", dicEmpPayRollDetail["W_LEVEL"]);
                                        paramList.Add("W_JOINING_DATE", dicEmpPayRollDetail["W_JOINING_DATE"]);
                                        paramList.Add("WW_DATE", dicEmpPayRollDetail["WW_DATE"]);
                                        paramList.Add("W_FLAG", dicEmpPayRollDetail["W_FLAG"]);
                                        paramList.Add("W_DIVISOR", dicEmpPayRollDetail["W_DIVISOR"]);
                                        paramList.Add("W_DAYS", dicEmpPayRollDetail["W_DAYS"]);
                                       
                                        flowRslt = cmnDM.Execute("CHRIS_SP_PRO_RATE_INCOME", "", paramList, ref processTrans, 60);

                                        //if (rslt.isSuccessful)
                                        //{if (flowRslt.dstResult != null && flowRslt.dstResult.Tables[0].Rows.Count > 0)
                                        //    {FillListWithOutputValues();}}
                                    }
                                    catch (Exception exp)
                                    {
                                    MessageBox = "InitiateProcess --CHRIS_SP_PRO_RATE_INCOME" + exp;
                                        msg = MessageBox.ToString();
                                    }
                                    finally
                                    {
                                    Logging("", "PRO-RATE INCOME PROCESS Run SUCCESSFULLY FOR PR_P_NO: " + dicEmpPayRollDetail["PR_P_NO"]);
                                   // MessageBox = "PRO-RATE INCOME PROCESS Run SUCCESSFULLY FOR PR_P_NO: " + dicEmpPayRollDetail["PR_P_NO"];
                                    }
                                }
                                #endregion

                                #region Income-Tax
                                else
                                {
                                    try
                                    {
                                    //**************Processing in Allowance (income_tax)*********************
                                    //SetStatusBar("Processing in Allowance (income_tax)");
                                    TB_No = "500023";
                                        paramList.Clear();
                                        paramList.Add("PR_P_NO",dicEmpPayRollDetail["PR_P_NO"]);
                                        paramList.Add("W_DATE", dicEmpPayRollDetail["W_DATE"]);
                                        paramList.Add("W_BRANCH", dicEmpPayRollDetail["W_BRANCH"]);
                                        paramList.Add("W_DESIG", dicEmpPayRollDetail["W_DESIG"]);
                                        paramList.Add("W_CATE", dicEmpPayRollDetail["W_CATE"]);
                                        paramList.Add("W_LEVEL", dicEmpPayRollDetail["W_LEVEL"]);
                                        
                                        flowRslt = cmnDM.Execute("CHRIS_SP_INCOME_TAX", "", paramList, ref processTrans, 60);
                                    }
                                    catch (Exception exp)
                                    {
                                        MessageBox = "InitiateProcess--CHRIS_SP_INCOME_TAX" + exp;
                                        msg = exp.ToString();
                                    }
                                    finally
                                    {
                                    Logging("", "Income-Tax PROCESS Run SUCCESSFULLY FOR PR_P_NO: " + dicEmpPayRollDetail["PR_P_NO"]);
                                    //MessageBox = "Income-Tax PROCESS Run SUCCESSFULLY FOR PR_P_NO: " +dicEmpPayRollDetail["PR_P_NO"];
                                    }
                                }
                                #endregion
                                //}

                            }

                        #endregion

                        //#region Salary_advance

                        //if (flowRslt.isSuccessful)
                        //{
                        //    try
                        //    {
                        //        //-- SALARY ADVANCE IF GIVEN BEFORE PAYROLL--
                        //        TB_No = "5000261";
                        //        //SetStatusBar("Processing in Salary Advance(salary_advance)");
                        //        paramList.Clear();
                        //        paramList.Add("PR_P_NO",dicEmpPayRollDetail["PR_P_NO"]);
                        //        paramList.Add("W_DATE", dicEmpPayRollDetail["W_DATE"]);
                        //        paramList.Add("W_SAL_ADV", dicEmpPayRollDetail["W_SAL_ADV"]);

                        //        flowRslt = cmnDM.Execute("CHRIS_SP_SALARY_ADVANCE", "", paramList, ref processTrans, 60);
                        //        if (flowRslt.isSuccessful)
                        //        {
                        //            if (flowRslt.dstResult != null && flowRslt.dstResult.Tables[0].Rows.Count > 0)
                        //            {
                        //                FillListWithOutputValues();
                        //            }
                        //        }
                        //    }
                        //    catch (Exception exp)
                        //    {
                        //        MessageBox = "InitiateProcess--CHRIS_SP_SALARY_ADVANCE" + exp;
                        //        msg = exp.ToString();
                        //    }
                        //    finally
                        //    {
                       //        Logging("", "SALARY_ADVANCE PROCESS Run SUCCESSFULLY FOR PR_P_NO: " + dicEmpPayRollDetail["PR_P_NO"]);
                        //        MessageBox = "SALARY_ADVANCE PROCESS Run SUCCESSFULLY FOR PR_P_NO: " +dicEmpPayRollDetail["PR_P_NO"];
                        //    }



                        //}
                        //#endregion

                        #region Up Deduction

                        if (flowRslt.isSuccessful)
                            {
                                try
                                {
                                    //-- TO PICK DEDUCTION--
                                    TB_No = "5000271";
                                   // SetStatusBar("Processing in Deduction (up_deduction)");
                                    paramList.Clear();
                                    paramList.Add("PR_P_NO",dicEmpPayRollDetail["PR_P_NO"]);
                                    paramList.Add("W_DATE", dicEmpPayRollDetail["W_DATE"]);
                                    paramList.Add("W_BRANCH", dicEmpPayRollDetail["W_BRANCH"]);
                                    paramList.Add("W_DESIG", dicEmpPayRollDetail["W_DESIG"]);
                                    paramList.Add("W_CATE", dicEmpPayRollDetail["W_CATE"]);
                                    paramList.Add("W_LEVEL", dicEmpPayRollDetail["W_LEVEL"]);
                                    paramList.Add("W_CONF_FLAG", dicEmpPayRollDetail["W_CONF_FLAG"]);
                                    
                                    flowRslt = cmnDM.Execute("CHRIS_SP_UP_DEDUCTION", "", paramList, ref processTrans, 60);
                                    if (flowRslt.isSuccessful)
                                    {
                                        //if (flowRslt.dstResult != null && flowRslt.dstResult.Tables[0].Rows.Count > 0)
                                        //{
                                        //    FillListWithOutputValues();
                                        //}
                                    }
                                }
                                catch (Exception exp)
                                {
                                    MessageBox = "InitiateProcess--CHRIS_SP_UP_DEDUCTION" + exp;
                                    msg = exp.ToString();
                                }
                                finally
                                {
                                Logging("", "UP DEDUCTION PROCESS Run SUCCESSFULLY FOR PR_P_NO: " + dicEmpPayRollDetail["PR_P_NO"]);
                                //MessageBox = "UP DEDUCTION PROCESS Run SUCCESSFULLY FOR PR_P_NO: " +dicEmpPayRollDetail["PR_P_NO"];
                                }
                            }

                            #endregion

                            #region Fin-Book

                            if (flowRslt.isSuccessful)
                            {
                                try
                                {
                                    //-- TO CALCULATE MONTH INSTALLMENT AND CALL FIN_CALC
                                    TB_No = "500029";
                                   // SetStatusBar("Processing in Finance (fin_book)");
                                    paramList.Clear();
                                    paramList.Add("PR_P_NO",dicEmpPayRollDetail["PR_P_NO"]);
                                    paramList.Add("W_DATE", dicEmpPayRollDetail["W_DATE"]);
                                    paramList.Add("W_BRANCH", dicEmpPayRollDetail["W_BRANCH"]);
                                    paramList.Add("W_MARKUP_L", dicEmpPayRollDetail["W_MARKUP_L"]);
                                    paramList.Add("W_INST_L", dicEmpPayRollDetail["W_INST_L"]);
                                    paramList.Add("W_MARKUP_F", dicEmpPayRollDetail["W_MARKUP_F"]);
                                    paramList.Add("W_INST_F", dicEmpPayRollDetail["W_INST_F"]);
                                    paramList.Add("W_EXCISE", dicEmpPayRollDetail["W_EXCISE"]);
                                    paramList.Add("W_FINANCE", dicEmpPayRollDetail["W_FINANCE"]);
                                    paramList.Add("W_MONTH_DED", dicEmpPayRollDetail["W_MONTH_DED"]);
                                    paramList.Add("W_EXTRA", dicEmpPayRollDetail["W_EXTRA"]);
                                    
                                    flowRslt = cmnDM.Execute("CHRIS_SP_FIN_BOOK", "", paramList, ref processTrans, 60);
                                    if (flowRslt.isSuccessful)
                                    {
                                        if (flowRslt.dstResult != null && flowRslt.dstResult.Tables[0].Rows.Count > 0)
                                        {
                                            FillListWithOutputValues();
                                        }
                                    }

                                }
                                catch (Exception exp)
                                {
                                    MessageBox = "InitiateProcess--CHRIS_SP_FIN_BOOK" + exp;
                                    msg = exp.ToString();
                                }
                                finally
                                {
                                Logging("", "FIN-BOOK PROCESS Run SUCCESSFULLY FOR PR_P_NO: " + dicEmpPayRollDetail["PR_P_NO"]);
                                //MessageBox = "FIN-BOOK PROCESS Run SUCCESSFULLY FOR PR_P_NO: " +dicEmpPayRollDetail["PR_P_NO"];
                                }
                            }
                            #endregion

                            #region Valid PNo. Calculations

                            if (flowRslt.isSuccessful)
                            {
                                try
                                {
                                    //-- TO CALCULATE BACK DATED SALARY AREARS --
                                    TB_No = "500030";
                                    //SetStatusBar("Processing in P.F. Arears (Valid_pno)");
                                    paramList.Clear();
                                    paramList.Add("PR_P_NO",dicEmpPayRollDetail["PR_P_NO"]);
                                    paramList.Add("W_DATE", dicEmpPayRollDetail["W_DATE"]);
                                    paramList.Add("W_CATE", dicEmpPayRollDetail["W_CATE"]);
                                    paramList.Add("W_CONF_FLAG", dicEmpPayRollDetail["W_CONF_FLAG"]);
                                    paramList.Add("W_CONFIRM_ON", dicEmpPayRollDetail["W_CONFIRM_ON"]);
                                    paramList.Add("W_EXCISE", dicEmpPayRollDetail["W_EXCISE"]);
                                    paramList.Add("W_FLAG", dicEmpPayRollDetail["W_FLAG"]);
                                    paramList.Add("W_ASR", dicEmpPayRollDetail["W_ASR"]);
                                    paramList.Add("W_HRENT", dicEmpPayRollDetail["W_HRENT"]);
                                    paramList.Add("W_CONV", dicEmpPayRollDetail["W_CONV"]);
                                    paramList.Add("W_GOVT_ALL", dicEmpPayRollDetail["W_GOVT_ALL"]);
                                    paramList.Add("W_PFUND", dicEmpPayRollDetail["W_PFUND"]);
                                    paramList.Add("WW_DATE", dicEmpPayRollDetail["WW_DATE"]);
                                    paramList.Add("WW_BASIC", dicEmpPayRollDetail["WW_BASIC"]);
                                    paramList.Add("WW_CONV", dicEmpPayRollDetail["WW_CONV"]);
                                    paramList.Add("WW_HOUSE", dicEmpPayRollDetail["WW_HOUSE"]);
                                    paramList.Add("W_BASIC_AREAR", dicEmpPayRollDetail["W_BASIC_AREAR"]);
                                    paramList.Add("W_HOUSE_AREAR", dicEmpPayRollDetail["W_HOUSE_AREAR"]);
                                    paramList.Add("W_CONV_AREAR", dicEmpPayRollDetail["W_CONV_AREAR"]);
                                    paramList.Add("W_PF_AREAR", dicEmpPayRollDetail["W_PF_AREAR"]);
                                    paramList.Add("GLOBAL_W_FOUND", dicEmpPayRollDetail["GLOBAL_W_FOUND"]);
                                    paramList.Add("MDAYS", dicEmpPayRollDetail["MDAYS"]);
                                    paramList.Add("W_DAYS", dicEmpPayRollDetail["W_DAYS"]);
                                    paramList.Add("WPER_DAY", dicEmpPayRollDetail["WPER_DAY"]);
                                    paramList.Add("W_AN_PACK", dicEmpPayRollDetail["W_AN_PACK"]);
                                    paramList.Add("W_WORK_DAY", dicEmpPayRollDetail["W_WORK_DAY"]);
                                    // 8 DEC,2011 | COM-530 | W_PAY_FLAG missing in parameter list and that causes PF Calculation issues.
                                    paramList.Add("W_PAY_FLAG", dicEmpPayRollDetail["W_PAY_FLAG"]);
                                    
                                    flowRslt = cmnDM.Execute("CHRIS_SP_PAYROLL_CALCULATIONS", "", paramList, ref processTrans, 0);
                                    if (flowRslt.isSuccessful)
                                    {
                                        if (flowRslt.dstResult != null && flowRslt.dstResult.Tables[0].Rows.Count > 0)
                                        {
                                            FillListWithOutputValues();
                                        }
                                    }
                                }
                                catch (Exception exp)
                                {
                                    MessageBox = "InitiateProcess--CHRIS_SP_PAYROLL_CALCULATIONS" + exp;
                                    msg = exp.ToString();
                                }
                                finally
                                {
                                Logging("", "Valid PNo. Calculations PROCESS Run SUCCESSFULLY FOR PR_P_NO: " + dicEmpPayRollDetail["PR_P_NO"]);
                                //MessageBox = "Valid PNo. Calculations PROCESS Run SUCCESSFULLY FOR PR_P_NO: " +dicEmpPayRollDetail["PR_P_NO"];
                                }

                            }


                            #endregion

                            #region Z_TAX
                            if (flowRslt.isSuccessful)
                            {
                                //--*********** inserting record in payroll table ******** --
                                try
                                {
                                    TB_No = "500034";
                                   // SetStatusBar("Processing Processing in Tax (z_tax)");
                                    paramList.Clear();

                                    paramList.Add("PR_P_NO",dicEmpPayRollDetail["PR_P_NO"]);
                                    paramList.Add("W_DATE", dicEmpPayRollDetail["W_DATE"]);
                                    paramList.Add("W_JOINING_DATE", dicEmpPayRollDetail["W_JOINING_DATE"]);
                                    paramList.Add("W_AN_PACK", dicEmpPayRollDetail["W_AN_PACK"]);
                                    paramList.Add("W_BRANCH", dicEmpPayRollDetail["W_BRANCH"]);
                                    paramList.Add("W_DESIG", dicEmpPayRollDetail["W_DESIG"]);
                                    paramList.Add("W_LEVEL", dicEmpPayRollDetail["W_LEVEL"]);
                                    paramList.Add("W_CATE", dicEmpPayRollDetail["W_CATE"]);
                                    paramList.Add("W_ZAKAT", dicEmpPayRollDetail["W_ZAKAT"]);
                                    paramList.Add("W_PRV_INC", dicEmpPayRollDetail["W_PRV_INC"]);
                                    paramList.Add("W_TAX_USED", dicEmpPayRollDetail["W_TAX_USED"]);
                                    paramList.Add("W_REFUND", dicEmpPayRollDetail["W_REFUND"]);
                                    paramList.Add("W_PRV_TAX_PD", dicEmpPayRollDetail["W_PRV_TAX_PD"]);
                                    paramList.Add("W_ITAX", dicEmpPayRollDetail["W_ITAX"]);
                                    paramList.Add("W_PF_EXEMPT", dicEmpPayRollDetail["W_PF_EXEMPT"]);
                                    paramList.Add("W_SYS", dicEmpPayRollDetail["W_SYS"]);
                                    paramList.Add("GLOBAL_W_10_AMT", dicEmpPayRollDetail["GLOBAL_W_10_AMT"]);
                                    paramList.Add("GLOBAL_W_INC_AMT", dicEmpPayRollDetail["GLOBAL_W_INC_AMT"]);
                                    paramList.Add("W_VP", dicEmpPayRollDetail["W_VP"]);
                                    paramList.Add("SCREEN_TYPE", "PG".ToUpper());
                                    
                                    flowRslt = cmnDM.Execute("CHRIS_SP_PAYROLL_GENERATION_Z_TAX", "", paramList, ref processTrans, 60);
                                    if (flowRslt.isSuccessful)
                                    {
                                        if (flowRslt.dstResult != null && flowRslt.dstResult.Tables[0].Rows.Count > 0)
                                        {
                                            FillListWithOutputValues();
                                        }

                                    }
                                    slTB_TotalTaxableAmount = dicEmpPayRollDetail["W_ITAX"].ToString();
                                    slTB_LoanTaxAmount      = dicEmpPayRollDetail["SUBS_LOAN_TAX_AMT"].ToString();
                                }
                                catch (Exception exp)
                                {
                                    MessageBox = "InitiateProcess--CHRIS_SP_PAYROLL_GENERATION_Z_TAX" + exp;
                                    msg = exp.ToString();
                                }
                                finally
                                {
                                Logging("", "Z_TAX PROCESS Run SUCCESSFULLY FOR PR_P_NO: " + dicEmpPayRollDetail["PR_P_NO"]);
                               // MessageBox = "Z_TAX PROCESS Run SUCCESSFULLY FOR PR_P_NO: " +dicEmpPayRollDetail["PR_P_NO"];
                                }

                            }
                            #endregion

                            #region Payroll_insertions

                            if (flowRslt.isSuccessful)
                            {
                                //--*********** inserting record in payroll table ******** --
                                TB_No = "500033";
                               // SetStatusBar("Processing in P.F. Arears (Valid_pno)");

                                Double W_ASR;
                                Double W_HRENT;
                                Double W_CONV;
                                Double W_PFUND;
                                Double W_GRAT;
                                Double W_ITAX;
                                Double W_SAL_ADV;
                                Double W_MARKUP_F;
                                Double W_INST_F;
                                Double W_MARKUP_L;
                                Double W_INST_L;
                                Double W_EXCISE;
                                Double W_ZAKAT;

                                if (Double.TryParse(dicEmpPayRollDetail["W_ASR"].ToString(), out W_ASR))
                                    dicEmpPayRollDetail["W_ASR"] = Math.Round(W_ASR, 2);

                                if (Double.TryParse(dicEmpPayRollDetail["W_HRENT"].ToString(), out W_HRENT))
                                    dicEmpPayRollDetail["W_HRENT"] = Math.Round(W_HRENT, 2);

                                if (Double.TryParse(dicEmpPayRollDetail["W_CONV"].ToString(), out W_CONV))
                                    dicEmpPayRollDetail["W_CONV"] = Math.Round(W_CONV, 2);

                                if (Double.TryParse(dicEmpPayRollDetail["W_PFUND"].ToString(), out W_PFUND))
                                    dicEmpPayRollDetail["W_PFUND"] = Math.Round(W_PFUND, 2);

                                if (Double.TryParse(dicEmpPayRollDetail["W_GRAT"].ToString(), out W_GRAT))
                                    dicEmpPayRollDetail["W_GRAT"] = Math.Round(W_GRAT, 2);

                                if (Double.TryParse(dicEmpPayRollDetail["W_ITAX"].ToString(), out W_ITAX))
                                    dicEmpPayRollDetail["W_ITAX"] = Math.Round(W_ITAX, 0);

                                if (Double.TryParse(dicEmpPayRollDetail["W_SAL_ADV"].ToString(), out W_SAL_ADV))
                                    dicEmpPayRollDetail["W_SAL_ADV"] = Math.Round(W_SAL_ADV, 2);

                                if (Double.TryParse(dicEmpPayRollDetail["W_MARKUP_F"].ToString(), out W_MARKUP_F))
                                    dicEmpPayRollDetail["W_MARKUP_F"] = Math.Round(W_MARKUP_F, 2);

                                if (Double.TryParse(dicEmpPayRollDetail["W_INST_F"].ToString(), out W_INST_F))
                                    dicEmpPayRollDetail["W_INST_F"] = Math.Round(W_INST_F, 2);

                                if (Double.TryParse(dicEmpPayRollDetail["W_MARKUP_L"].ToString(), out W_MARKUP_L))
                                    dicEmpPayRollDetail["W_MARKUP_L"] = Math.Round(W_MARKUP_L, 2);

                                if (Double.TryParse(dicEmpPayRollDetail["W_INST_L"].ToString(), out W_INST_L))
                                    dicEmpPayRollDetail["W_INST_L"] = Math.Round(W_INST_L, 0);

                                if (Double.TryParse(dicEmpPayRollDetail["W_EXCISE"].ToString(), out W_EXCISE))
                                    dicEmpPayRollDetail["W_EXCISE"] = Math.Round(W_EXCISE, 2);

                                if (Double.TryParse(dicEmpPayRollDetail["W_ZAKAT"].ToString(), out W_ZAKAT))
                                    dicEmpPayRollDetail["W_ZAKAT"] = Math.Round(W_ZAKAT, 2);

                                try
                                {
                                    paramList.Clear();
                                    paramList.Add("PR_P_NO",dicEmpPayRollDetail["PR_P_NO"]);
                                    paramList.Add("W_DATE", dicEmpPayRollDetail["W_DATE"]);
                                    paramList.Add("W_WORK_DAY", dicEmpPayRollDetail["W_WORK_DAY"]);
                                    paramList.Add("W_ASR", dicEmpPayRollDetail["W_ASR"]);
                                    paramList.Add("W_HRENT", dicEmpPayRollDetail["W_HRENT"]);
                                    paramList.Add("W_CONV", dicEmpPayRollDetail["W_CONV"]);
                                    paramList.Add("W_PFUND", dicEmpPayRollDetail["W_PFUND"]);
                                    paramList.Add("W_GRAT", dicEmpPayRollDetail["W_GRAT"]);
                                    paramList.Add("W_ITAX", dicEmpPayRollDetail["W_ITAX"]);
                                    paramList.Add("W_SAL_ADV", dicEmpPayRollDetail["W_SAL_ADV"]);
                                    paramList.Add("W_MARKUP_F", dicEmpPayRollDetail["W_MARKUP_F"]);
                                    paramList.Add("W_INST_F", dicEmpPayRollDetail["W_INST_F"]);
                                    paramList.Add("W_MARKUP_L", dicEmpPayRollDetail["W_MARKUP_L"]);
                                    paramList.Add("W_INST_L", dicEmpPayRollDetail["W_INST_L"]);
                                    paramList.Add("W_EXCISE", dicEmpPayRollDetail["W_EXCISE"]);
                                    paramList.Add("W_ZAKAT", dicEmpPayRollDetail["W_ZAKAT"]);
                                    paramList.Add("W_FLAG", dicEmpPayRollDetail["W_FLAG"]);
                                    paramList.Add("W_BRANCH", dicEmpPayRollDetail["W_BRANCH"]);
                                    paramList.Add("W_CATE", dicEmpPayRollDetail["W_CATE"]);
                                   
                                    flowRslt = cmnDM.Execute("CHRIS_SP_PAYROLL_INSERTIONS", "", paramList, ref processTrans, 60);
                                    if (flowRslt.isSuccessful)
                                    {
                                        //YOOOOOOOOOOOOOO
                                    }
                                }
                                catch (Exception exp)
                                {
                                    MessageBox = "InitiateProcess--CHRIS_SP_PAYROLL_INSERTIONS" + exp;
                                    msg = exp.ToString();
                                }
                                finally
                                {
                                Logging("", "PAYROLL_INSERTIONS PROCESS Run SUCCESSFULLY FOR PR_P_NO: " + dicEmpPayRollDetail["PR_P_NO"]);
                                //MessageBox = "PAYROLL_INSERTIONS PROCESS Run SUCCESSFULLY FOR PR_P_NO: " +dicEmpPayRollDetail["PR_P_NO"];
                                }

                            }
                            #endregion

                            #region If flowRslt is False
                            else if (!flowRslt.isSuccessful)
                            {
                                
                                if (processTrans != null && processTrans.Connection != null)
                                    processTrans.Rollback();
                                //if (conn.State != ConnectionState.Closed)
                                //    conn.Close();
                               // base.LogError(this.Name, "InitiateProcess()", flowRslt.exp);
                                MessageBox = "Error during Payroll Generation";
                                return;// break;
                            }
                            #endregion


                            DateTime.TryParse(dicEmpPayRollDetail["WW_DATE"].ToString(), out dtWW_DATE);
                            Int32.TryParse(dicEmpPayRollDetail["W_SW_PAY"].ToString(), out W_SW_PAY);


                    }
                    if (processState != FormMode.Failure)
                        processState = FormMode.Successfull;
                }
                else
                    processState = FormMode.Failure;

                if (processState == FormMode.Failure)
                {
                    MessageBox="Payroll cannot be generated.Check Log for Error Details";
                   

                }
                else if (processState == FormMode.Successfull)
                {
                    MessageBox = "Process Completed";
                }
            }

            
            catch (Exception dbEcxep)
            {
                processState = FormMode.Failure;
                if (processTrans != null && processTrans.Connection != null)
                    processTrans.Rollback();
                
                MessageBox= "InitiateProcess()" +dbEcxep;
            }
            finally
            {
                slTB_Reply_For_Generation = String.Empty;
            }

        }
        private void FillPayRollDataWithPersonnelBasicInfo(int pRow)
        {
            try
            {

                for (int pCol = 0; pCol < dtPersonnels.Columns.Count; pCol++)
                {
                    if (dicEmpPayRollDetail.ContainsKey(dtPersonnels.Columns[pCol].ColumnName))
                    {
                        if (dtPersonnels.Columns[pCol].ColumnName.ToUpper() != "W_DATE")
                            dicEmpPayRollDetail[dtPersonnels.Columns[pCol].ColumnName] = dtPersonnels.Rows[pRow][pCol];
                    }
                    //if (dtEmpPayRollDetail.Columns.Contains(dtPersonnels.Columns[pCol].ColumnName))
                    //    {dtEmpPayRollDetail.Rows[pRow][dtPersonnels.Columns[pCol].ColumnName] = dtPersonnels.Rows[pRow][pCol];}
                }
            }
            catch (Exception e)
            {
                MessageBox = "FillPayRollDataWithPersonnelBasicInfo(int personnelNumberRow)"+e;
                //error in copying data.
            }
        }
        private void FillListWithOutputValues()
        {
            try
            {
                for (int tableIndex = 0; tableIndex < flowRslt.dstResult.Tables.Count; tableIndex++)
                {
                    for (int outVarCount = 0; outVarCount < flowRslt.dstResult.Tables[tableIndex].Columns.Count; outVarCount++)
                    {
                        if (dicEmpPayRollDetail.ContainsKey(flowRslt.dstResult.Tables[tableIndex].Columns[outVarCount].ColumnName))
                        {
                            dicEmpPayRollDetail[flowRslt.dstResult.Tables[tableIndex].Columns[outVarCount].ColumnName] = flowRslt.dstResult.Tables[tableIndex].Rows[0][outVarCount];
                        }
                    }
                }
            }
            catch (Exception fillValue)
            {                
                MessageBox = "FillListWithOutputValues"+ fillValue;
            }
        }

        #endregion

        #region PayrollHistoryUpdationProcess

        public IActionResult PayrollHistoryUpdationProcess()
        {
            return View();
        }
        public ActionResult btn_PayrollHistoryUpdationProcess()
        {
            Dictionary<string, object> d = new Dictionary<string, object>();

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_PAYROLLHISTORYUPDATION_MANAGER", "P_LOV", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        public ActionResult PR_P_NO_Validation(Decimal? PR_P_NO,DateTime? ProcessingDate)
        {
            Dictionary<String, Object> paramValues = new Dictionary<String, Object>();
            Result rsltCode;
            CmnDataManager cmnDM = new CmnDataManager();
            string MessageBox = "";
           
            paramValues.Clear();
            paramValues.Add("PR_P_NO", PR_P_NO);
            paramValues.Add("W_DATE", ProcessingDate);

            // IS FAST CHECK
            rsltCode = cmnDM.GetData("CHRIS_SP_PAYROLLHISTORYUPDATION_MANAGER", "IS_FAST_NO", paramValues);
            if (rsltCode.isSuccessful)
            {
                if (rsltCode.dstResult != null && rsltCode.dstResult.Tables.Count > 0)
                {
                    if (rsltCode.dstResult.Tables[0].Rows.Count.Equals(0))
                    {
                        MessageBox = "Enter P.No. For IS Or FAST Only";
                        
                    }
                }
            }

            // FN MONTH CHECK
            rsltCode = cmnDM.GetData("CHRIS_SP_PAYROLLHISTORYUPDATION_MANAGER", "FIN_CHECK", paramValues);
            if (rsltCode.isSuccessful)
            {
                if (rsltCode.dstResult != null && rsltCode.dstResult.Tables.Count > 0)
                {
                    if (!rsltCode.dstResult.Tables[0].Rows.Count.Equals(0))
                    {
                        MessageBox = "Finance Installment Has Already Deducted For The Given Month";                        
                    }
                }
            }

            if (MessageBox=="")
            {
                return Json(null);
            }
            return Json(MessageBox);
        }

        public ActionResult Submit_PayrollHistoryUpdationProcess(string PR_P_NO, DateTime? ProcessingDate, string target)
        {
            string lblProcessing = "";
            if (target.Length > 0)
            {
                if (target == "YES" )
                {
                    if (target == "YES")
                    {
                       // int pr_p_no = 0;
                        Dictionary<String, Object> paramValues = new Dictionary<String, Object>();
                        Result rsltCode;
                        CmnDataManager cmnDM = new CmnDataManager();
                        target = String.Empty;
                        try
                        {
                            //connbean = SQLManager.GetConnectionBean();
                            //if (connbean != null)
                            //{
                            //    proCon = connbean.getDatabaseConnection(0);
                            //    processTrans = proCon.BeginTransaction();

                            //    lblProcessing.Visible = true;
                            //    Application.DoEvents();
                            //    Int32.TryParse(txtPersNo.Text, out pr_p_no);

                            paramValues.Clear();
                            paramValues.Add("PR_P_NO", PR_P_NO);
                            paramValues.Add("W_DATE", ProcessingDate);

                            rslt = cmnDM.Execute("CHRIS_SP_PAYROLLHISTORYUPDATION_MANAGER", "VALID_PNO", paramValues, ref processTrans, 60000);

                            if (!rslt.isSuccessful)
                            {
                                if (processTrans != null && processTrans.Connection != null)
                                    processTrans.Rollback();
                                Logging("", "PayrollHistoryUpdationProcess" + rslt.exp);
                                lblProcessing = "Process Failed!";

                            }
                            else
                            {
                                lblProcessing = "Process Completed.";

                            }
                            return Json(lblProcessing);
                            // }
                        }
                        catch (Exception dbEcxep)
                        {
                            if (processTrans != null && processTrans.Connection != null)
                                processTrans.Rollback();
                            Logging("", "PayrollHistoryUpdationProcess" + dbEcxep);
                        }
                        finally
                        {
                            target = String.Empty;
                        }
                    }
                    else
                        return Json(null);
                }
                else if (target == "NO")
                {
                    return Json(null);
                }
            }
            return Json(null);
        }
        #endregion
        public IActionResult POSTDUMPProcess()
        {
            return View();
        }
        public IActionResult PREDUMPProcess()
        {
            return View();
        }
    }
}

