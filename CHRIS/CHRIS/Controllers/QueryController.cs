﻿using CHRIS.Common;
using CHRIS.Data;
using CHRIS.Models;
using CHRIS.Services;
using DocumentFormat.OpenXml.InkML;
using DocumentFormat.OpenXml.Office2010.Excel;
using DocumentFormat.OpenXml.Spreadsheet;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Build.Framework;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion.Internal;
using Newtonsoft.Json;
using System.Data;
using System.Reflection.Metadata.Ecma335;

namespace CHRIS.Controllers
{
    public class QueryController : Controller
    {
        #region ALLOWANCE DEDUCTION QUERY
        public IActionResult AllowanceDeductionPayrollQuery()
        {
            return View();
        }
        [HttpPost]
        public JsonResult GetADPRNOList()
        {
            Dictionary<string, object> d = new Dictionary<string, object>();
            //d.Add("W_BRANCH", Branch);
            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_ALLOWANCEDEDUCTIONPAYROLL_MANAGER", "PP_LOV", d);
            if (rsltCode.isSuccessful)
            {
                if (rsltCode.dstResult.Tables.Count > 0)
                {
                    if (rsltCode.dstResult.Tables[0].Rows.Count > 0)
                    {
                        DataTable firstTable = rsltCode.dstResult.Tables[0];

                        var CatResul = JsonConvert.SerializeObject(firstTable);
                        return Json(CatResul);
                    }
                    else
                        return Json(null);
                }
                else
                    return Json(null);

            }
            else
                return Json(null);
        }
        [HttpPost]
        public JsonResult GetADDetail(string PRNO)
        {
            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("PR_P_NO", PRNO);
            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_PAYROLL_MANAGER", "List", d);
            if (rsltCode.isSuccessful)
            {
                if (rsltCode.dstResult.Tables.Count > 0)
                {
                    if (rsltCode.dstResult.Tables[0].Rows.Count > 0)
                    {
                        DataTable firstTable = rsltCode.dstResult.Tables[0];

                        var CatResul = JsonConvert.SerializeObject(firstTable);
                        return Json(CatResul);
                    }
                    else
                        return Json(null);
                }
                else
                    return Json(null);

            }
            else
                return Json(null);
        }
        [HttpPost]
        public JsonResult GetAllowanceDeductionPayroll(string PRNO)
        {
            string name = "";

            string message = "";
            string allowancelist = "";
            string deductionlist = "";

            DataTable dtPrsnl = new DataTable();
            Dictionary<String, Object> objValues = new Dictionary<String, Object>();
            Result rsltPrsnl, rsltPAllow, rsltPDeduct;
            CmnDataManager cmnDM = new CmnDataManager();
            if (PRNO != null && PRNO != "")
            {
                objValues.Clear();
                objValues.Add("PR_P_NO", PRNO);

                rsltPrsnl = cmnDM.GetData("CHRIS_SP_ALLOWANCEDEDUCTIONPAYROLL_MANAGER", "PP_VERIFY", objValues);

                if (rsltPrsnl.isSuccessful)
                {
                    if (rsltPrsnl.dstResult.Tables[0].Rows.Count == 0)
                    {
                        message = "Personnel # Does Not Exist.......";
                    }
                    else
                    {
                        rsltPrsnl = cmnDM.GetData("CHRIS_SP_ALLOWANCEDEDUCTIONPAYROLL_MANAGER", "PP_VERIFY", objValues);
                        dtPrsnl = rsltPrsnl.dstResult.Tables[0];
                        if (dtPrsnl.Rows.Count > 0)
                        {
                            //Name = Convert.ToString(dtPrsnl.Rows[0]["PR_FIRST_NAME"]) + "  " + Convert.ToString(dtPrsnl.Rows[0]["PR_LAST_NAME"]);
                            name = Convert.ToString(dtPrsnl.Rows[0]["PR_NAME"]);

                        }

                        rsltPAllow = cmnDM.GetData("CHRIS_SP_PAYROLL_ALLOW_MANAGER", "List", objValues);
                        if (rsltPAllow.isSuccessful)
                        {
                            if (rsltPAllow.dstResult.Tables[0].Rows.Count > 0)
                            {
                                DataTable firstTable = rsltPAllow.dstResult.Tables[0];
                                var CatResul = JsonConvert.SerializeObject(firstTable);
                                allowancelist = CatResul;
                            }

                        }

                        rsltPDeduct = cmnDM.GetData("CHRIS_SP_PAYROLL_DEDUC_MANAGER", "List", objValues);

                        if (rsltPDeduct.isSuccessful)
                        {
                            if (rsltPDeduct.dstResult.Tables[0].Rows.Count > 0)
                            {
                                DataTable SecondTable = rsltPDeduct.dstResult.Tables[0];
                                var CatResul = JsonConvert.SerializeObject(SecondTable);
                                deductionlist = CatResul;
                            }
                        }
                    }
                    var FinalResult = new
                    {
                        message = message,
                        name = name,
                        allowancelist = allowancelist,
                        deductionlist = deductionlist
                    };
                    return Json(FinalResult);
                }
            }
            return Json("");
        }
        #endregion

        #region BONUS QUERY
        public IActionResult BonusQuery()
        {
            return View();
        }
        [HttpPost]
        public JsonResult BonusQueryPRNO_Entry()
        {
            Dictionary<string, object> d = new Dictionary<string, object>();
            //d.Add("W_BRANCH", Branch);
            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_BONUSQUERY_MANAGER", "PP_LOV", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];

                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);

            }
            else
                return Json(null);
        }
        [HttpPost]
        public JsonResult GetBonusAnnualPakage(int? PRNO)
        {
            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("PR_P_NO", PRNO);
            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_SALARYHISTORYQUERY_INC_PROMOTION_GETALL", "", d);
            if (rsltCode.isSuccessful)
            {
                if (rsltCode.dstResult.Tables.Count > 0)
                {
                    if (rsltCode.dstResult.Tables[0].Rows.Count > 0)
                    {
                        DataTable firstTable = rsltCode.dstResult.Tables[0];
                        var test = firstTable.AsEnumerable().Last();
                        var CatResul = JsonConvert.SerializeObject(firstTable);
                        return Json(CatResul);
                    }
                }

            }
            return Json(null);
        }

        [HttpPost]
        public JsonResult GetBonusQueryList(int? PRNO)
        {
            CHRIS_Context ic = new CHRIS_Context();
            var context = new CHRIS_ContextProcedures(ic);

            OutputParameter<int?> outParam = new OutputParameter<int?>();

            var res = context.CHRIS_SP_BONUS_TAB_GETALLAsync(PRNO, outParam);
            res.Wait();
            return Json(res.Result);
        }
        [HttpPost]
        public JsonResult GetQueryBonusDetail(int? PersnolNo)
        {
            if (PersnolNo != null && PersnolNo != 0)
            {
                Dictionary<string, object> colsNVals = new Dictionary<string, object>();
                CmnDataManager cmnDM = new CmnDataManager();
                colsNVals.Clear();
                colsNVals.Add("PR_P_NO", PersnolNo);

                Result rsltCode = cmnDM.GetData("CHRIS_SP_BONUSQUERY_MANAGER", "PP_VERIFY", colsNVals);
                if (rsltCode.isSuccessful)
                {
                    if (rsltCode.dstResult.Tables[0].Rows.Count == 0)
                    {
                        var CoverLetter = new
                        {
                            FirstName = "",
                            LasteName = "",
                            Level = "",
                            Desig = "",
                            Branch = "",
                            AnnualPkg = "",
                        };
                        return Json(CoverLetter);
                    }
                }

                colsNVals.Clear();
                colsNVals.Add("PR_P_NO", PersnolNo);
                //dgvBonusDetail.Refresh(); 

                DataTable dtFPH = new DataTable();

                rsltCode = cmnDM.GetData("CHRIS_SP_BONUSQUERY_MANAGER", "PSN_INC_DSG", colsNVals);
                if (rsltCode.isSuccessful)
                {
                    if (rsltCode.dstResult.Tables.Count > 0)
                    {
                        dtFPH = rsltCode.dstResult.Tables[0];

                    }
                    if (dtFPH.Rows.Count > 0)
                    {
                        var CoverLetter = new
                        {
                            FirstName = "",
                            LasteName = "",
                            Level = dtFPH.Rows[0]["LEVEL"].ToString().ToUpper(),
                            Desig = dtFPH.Rows[0]["DGG"].ToString().ToUpper(),
                            Branch = dtFPH.Rows[0]["BRANCH"].ToString().ToUpper(),
                            AnnualPkg = "",
                        };
                        return Json(CoverLetter);
                    }
                    else
                    {
                        var CoverLetter = new
                        {
                            FirstName = "",
                            LasteName = "",
                            Level = "",
                            Desig = "",
                            Branch = "",
                            AnnualPkg = "",
                        };
                        return Json(CoverLetter);
                    }
                }
            }
            return Json("");
        }
        #endregion

        #region DEPARTMENT WISE QUERY
        public IActionResult DeptWiseQuery()
        {
            return View();
        }
        [HttpPost]
        public JsonResult GetDeptWiseDepartmentList(string Branch, string Segment)
        {
            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("W_BRANCH", Branch);
            d.Add("D_SEG", Segment);
            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_QUERY_dept_cont_MANAGER", "D_NO_LOV", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];

                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);

            }
            return Json(null);
        }
        [HttpPost]
        public JsonResult GetDeptWiseMainList(string Branch, string Segment, string DNo)
        {
            string message = "";
            Dictionary<string, object> param = new Dictionary<string, object>();
            CmnDataManager cmnDm = new CmnDataManager();
            param.Clear();
            param.Add("W_BRANCH", Branch);
            param.Add("D_SEG", Segment);
            param.Add("D_NO", DNo);

            Result rslt = cmnDm.GetData("CHRIS_SP_QUERY_dept_cont_MANAGER", "DETAIL_VERIFY", param);
            if (rslt.isSuccessful)
            {
                if (rslt.dstResult.Tables[0].Rows.Count == 0)
                {
                    message = "No Record Found In This Branch/Segment/Department..";
                    var ResultData = new
                    {
                        message = message,
                        list = ""
                    };
                    return Json(ResultData);
                }
                else
                {
                    DataTable firstTable = rslt.dstResult.Tables[0];

                    var CatResul = JsonConvert.SerializeObject(firstTable);
                    var ResultData = new
                    {
                        message = "",
                        list = CatResul
                    };
                    return Json(ResultData);
                }
            }
            return Json(null);
        }
        #endregion
        public IActionResult EmployeeCurrentStatusQuery()
        {
            return View();
        }
        [HttpPost]
        public JsonResult GetEmployeeCurrentDetail(int? PERSNOL_NO)
        {
            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("PR_P_NO", PERSNOL_NO);

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_EMPLOYEECURRENTSTATUS_MANAGER", "PRSNL_LIST", d);
            if (rsltCode.isSuccessful)
            {
                if (rsltCode.dstResult.Tables.Count > 0)
                {
                    if (rsltCode.dstResult.Tables[0].Rows.Count > 0)
                    {
                        Result rsltCode1 = objCmnDataManager.GetData("CHRIS_SP_EMPLOYEECURRENTSTATUS_MANAGER", "MISC_FILL", d);
                        if (rsltCode1.isSuccessful)
                        {
                            if (rsltCode1.dstResult.Tables.Count > 0)
                            {
                                if (rsltCode1.dstResult.Tables[0].Rows.Count > 0)
                                {
                                    Result rsltCode2 = objCmnDataManager.GetData("CHRIS_SP_EMPLOYEECURRENTSTATUS_MANAGER", "PRSNAL", d);
                                    if (rsltCode2.isSuccessful)
                                    {
                                        if (rsltCode2.dstResult.Tables.Count > 0)
                                        {
                                            if (rsltCode2.dstResult.Tables[0].Rows.Count > 0)
                                            {
                                                var coverletters = new
                                                {
                                                    FirstName = rsltCode.dstResult.Tables[0].Rows[0]["PR_FIRST_NAME"].ToString(),
                                                    LastName = rsltCode.dstResult.Tables[0].Rows[0]["PR_LAST_NAME"].ToString(),
                                                    PR_FUNC_TITTLE1 = rsltCode.dstResult.Tables[0].Rows[0]["PR_FUNC_TITTLE1"].ToString(),
                                                    PR_FUNC_TITTLE2 = rsltCode.dstResult.Tables[0].Rows[0]["PR_FUNC_TITTLE2"].ToString(),
                                                    PR_JOINING_DATE = rsltCode.dstResult.Tables[0].Rows[0]["PR_JOINING_DATE"].ToString(),
                                                    PR_CONFIRM = rsltCode.dstResult.Tables[0].Rows[0]["PR_CONFIRM"].ToString(),
                                                    PR_CONFIRM_ON = rsltCode.dstResult.Tables[0].Rows[0]["PR_CONFIRM_ON"].ToString(),
                                                    PR_LAST_INCREMENT = rsltCode.dstResult.Tables[0].Rows[0]["PR_LAST_INCREMENT"].ToString(),
                                                    PR_NEXT_INCREMENT = rsltCode.dstResult.Tables[0].Rows[0]["PR_NEXT_INCREMENT"].ToString(),
                                                    PR_CATEGORY = rsltCode.dstResult.Tables[0].Rows[0]["PR_CATEGORY"].ToString(),
                                                    PR_NEW_ANNUAL_PACK = rsltCode.dstResult.Tables[0].Rows[0]["PR_NEW_ANNUAL_PACK"].ToString(),
                                                    PR_ACCOUNT_NO = rsltCode.dstResult.Tables[0].Rows[0]["PR_ACCOUNT_NO"].ToString(),
                                                    PR_NATIONAL_TAX = rsltCode.dstResult.Tables[0].Rows[0]["PR_NATIONAL_TAX"].ToString(),
                                                    LOCATION = rsltCode.dstResult.Tables[0].Rows[0]["LOCATION"].ToString(),
                                                    DGG = rsltCode1.dstResult.Tables[0].Rows[0]["DGG"].ToString(),
                                                    LEVEL = rsltCode1.dstResult.Tables[0].Rows[0]["LEVEL"].ToString(),
                                                    BRNCHNAME = rsltCode1.dstResult.Tables[0].Rows[0]["BRNCHNAME"].ToString(),
                                                    DGREE = rsltCode1.dstResult.Tables[0].Rows[0]["DGREE"].ToString(),
                                                    GEIDNO = rsltCode1.dstResult.Tables[0].Rows[0]["GEIDNO"].ToString(),
                                                    PR_ADD1 = rsltCode2.dstResult.Tables[0].Rows[0]["PR_ADD1"].ToString(),
                                                    PR_ADD2 = rsltCode2.dstResult.Tables[0].Rows[0]["PR_ADD2"].ToString(),
                                                    PR_PHONE1 = rsltCode2.dstResult.Tables[0].Rows[0]["PR_PHONE1"].ToString(),
                                                    PR_PHONE2 = rsltCode2.dstResult.Tables[0].Rows[0]["PR_PHONE2"].ToString(),
                                                    PR_ID_CARD_NO = rsltCode2.dstResult.Tables[0].Rows[0]["PR_ID_CARD_NO"].ToString(),
                                                    PR_SEX = rsltCode2.dstResult.Tables[0].Rows[0]["PR_SEX"].ToString(),
                                                    PR_D_BIRTH = rsltCode2.dstResult.Tables[0].Rows[0]["PR_D_BIRTH"].ToString(),
                                                    PR_MARITAL = rsltCode2.dstResult.Tables[0].Rows[0]["PR_MARITAL"].ToString(),
                                                    PR_MARRIAGE = rsltCode2.dstResult.Tables[0].Rows[0]["PR_MARRIAGE"].ToString(),
                                                    PR_BIRTH_SP = rsltCode2.dstResult.Tables[0].Rows[0]["PR_BIRTH_SP"].ToString(),
                                                    PR_NO_OF_CHILD = rsltCode2.dstResult.Tables[0].Rows[0]["PR_NO_OF_CHILD"].ToString(),
                                                    PR_SPOUSE = rsltCode2.dstResult.Tables[0].Rows[0]["PR_SPOUSE"].ToString()
                                                };
                                                return Json(coverletters);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return Json("");
        }

        [HttpPost]
        public JsonResult GetEmployeeChildDetail(int? PERSNOL_NO)
        {
            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("PR_P_NO", PERSNOL_NO);
            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_EMPLOYEECURRENTSTATUS_MANAGER", "CHILD", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];

                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);

            }
            return Json(null);
        }

        #region RANK HISTORY QUERY
        public IActionResult RankHistoryQuery()
        {
            return View();
        }
        [HttpPost]
        public JsonResult GetRankHistoryQueryPersonnelNoList()
        {
            Dictionary<string, object> d = new Dictionary<string, object>();
            //d.Add("W_BRANCH", Branch);
            //d.Add("D_SEG", Segment);
            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_QUERY_RANK_PERSONNEL_PERSONNEL_MANAGER", "P_NO_LOV", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];

                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);

            }
            else
                return Json(null);
        }
        [HttpPost]
        public JsonResult GetRankHistoryQueryList(int? PRNO)
        {
            CHRIS_Context ic = new CHRIS_Context();
            var context = new CHRIS_ContextProcedures(ic);

            OutputParameter<int?> outParam = new OutputParameter<int?>();

            var res = context.CHRIS_SP_QUERY_RANK_RANK_GETALLAsync(PRNO, outParam);
            res.Wait();
            return Json(res.Result);
        }
        #endregion
        public IActionResult SalaryHistoryQuery()
        {
            return View();
        }

        [HttpPost]
        public JsonResult PRNO_Entry()
        {
            Dictionary<string, object> d = new Dictionary<string, object>();
            //d.Add("W_BRANCH", Branch);
            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_SALARYHISTORYQUERY_PERSONNEL_MANAGER", "LovPersonnel", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];

                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);

            }
            else
                return Json(null);
        }
        [HttpPost]
        public JsonResult EmployeeHistoryPRNO_Entry()
        {
            Dictionary<string, object> d = new Dictionary<string, object>();
            //d.Add("W_BRANCH", Branch);
            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_EMPLOYEECURRENTSTATUS_MANAGER", "PP_LOV", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];

                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);

            }
            else
                return Json(null);
        }
        #region TAX INFORMATION QUERY
        public IActionResult TaxInformationQuery()
        {
            return View();
        }
        [HttpPost]
        public JsonResult FillDropDownOfTaxInfoPersnolNo()
        {
            Dictionary<string, object> d = new Dictionary<string, object>();
            //d.Add("W_BRANCH", Branch);
            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_TAXINFORMATION_MANAGER", "PP_LOV", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];

                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);

            }
            else
                return Json(null);
        }
        [HttpPost]
        public JsonResult CheckValidPayRoll(int? PERSNOL_NO, string Payroll_Date)
        {
            string NextYear = "";
            string CurrentYear = "";
            DataTable dtTaxQuery = new DataTable();
            if (PERSNOL_NO != null && PERSNOL_NO != 0 && Payroll_Date != null)
            {
                if (BaseFunction.IsValidExpression(Payroll_Date, "\\d{2}/\\d{2}/\\d{4}") || BaseFunction.IsValidExpression(Payroll_Date, "\\d{8}"))
                {
                    DateTime payrollDate;
                    if (BaseFunction.IsValidExpression(Payroll_Date, "\\d{8}"))
                    {
                        payrollDate = new DateTime(Convert.ToInt16(Payroll_Date.Substring(4, 4)), Convert.ToInt16(Payroll_Date.Substring(2, 2)), Convert.ToInt16(Payroll_Date.Substring(0, 2)));
                        DateTime.TryParse(payrollDate.ToString("dd/MM/yyyy"), out payrollDate);
                    }
                    else
                    {
                        DateTime.TryParse(Payroll_Date, out payrollDate);
                    }

                    if (payrollDate.Equals(DateTime.MinValue))
                    {
                        return Json("Enter Last Payroll Date (DD/MM/YYYY)");
                    }
                    else
                    {
                        Payroll_Date = payrollDate.ToString("dd/MM/yyyy");
                    }

                }
                else
                {
                    return Json("Enter Last Payroll Date (DD/MM/YYYY)");
                }
                //string currYear = Convert.ToString(DateTime.Now.Year);
                //string lastYear = Convert.ToString(DateTime.Now.AddYears(-1).Year);

                int currYear = Convert.ToDateTime(Payroll_Date).Year;
                int payrollMonth = Convert.ToDateTime(Payroll_Date).Month;
                if (payrollMonth < 7)
                    currYear = currYear - 1;

                int nextYear = currYear + 1;

                NextYear = nextYear.ToString();
                CurrentYear = currYear.ToString();

                Dictionary<string, object> d = new Dictionary<string, object>();
                d.Add("PR_P_NO", PERSNOL_NO);
                CmnDataManager objCmnDataManager = new CmnDataManager();
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_TAXINFORMATION_MANAGER", "PYRL_MAX", d);

                if (rsltCode.dstResult.Tables[0].Rows[0].ItemArray[0].ToString() == "")
                {
                    return Json("No Payroll is Generated");
                }
                else
                {
                    dtTaxQuery = rsltCode.dstResult.Tables[0];
                    if (Convert.ToDateTime(Payroll_Date) > Convert.ToDateTime(dtTaxQuery.Rows[0]["W_SYS"]))
                    {
                        return Json("Last Payroll Was Generated On " + Convert.ToDateTime(rsltCode.dstResult.Tables[0].Rows[0]["W_SYS"]).Date.ToShortDateString());
                    }

                }

                d.Clear();
                d.Add("PAYROLL_DATE", Payroll_Date);
                rsltCode = objCmnDataManager.GetData("CHRIS_SP_TAXINFORMATION_MANAGER", "PYRL_VALID", d);
                if (rsltCode.isSuccessful)
                {
                    if (rsltCode.dstResult.Tables[0].Rows[0].ItemArray[0].ToString() == "0")
                    {
                        rsltCode = objCmnDataManager.GetData("CHRIS_SP_TAXINFORMATION_MANAGER", "PYRL_VOID", d);
                        if (rsltCode.dstResult.Tables[0].Rows[0].ItemArray[0].ToString() != "")
                        {
                            return Json("Payroll Date Of The Given Month Is :  " + Convert.ToDateTime(rsltCode.dstResult.Tables[0].Rows[0]["Max_Payroll"]).Date.ToShortDateString());
                        }
                    }
                }
            }
            else
            {
                CurrentYear = "";
                NextYear = "";
            }
            return Json(null);
        }
        [HttpPost]
        public JsonResult GetTaxInfoFormData(string PNO, string FROM, string TO, string PAY_ROLL_DATE)
        {
            Dictionary<string, object> colsNVals = new Dictionary<string, object>();
            Result rslt, rsltDtl;
            CmnDataManager cmnDM = new CmnDataManager();
            DataTable dtDTaxPaid = new DataTable();
            DataTable dtTaxQuery = new DataTable();
            #region Variable
            string List1 = "";
            string List2 = "";
            string txtDesig = "";
            string txtLevel = "";
            string txtCategory = "";
            string txtIncrDate = "";
            string txtPromoDate = "";
            string txtJoinDate = "";
            string txtAnnPkge = "";
            string txtAnnBasic = "";
            string txtRVPTrans = "";
            string txtTaxAllow = "";
            string txtTotal = "";
            string txtSalArear = "";
            string txtFunctional = "";
            string txtNatTaxNum = "";
            string txtAccNum = "";
            string txtEducation = "";
            string txtOTCost = "";
            string txtAvgOT = "";
            string txtOTArear = "";
            string txtForcastOT = "";
            string txtTOTCost = "";
            string txtAvMnt = "";
            string txtForcastAll = "";
            string txtTaxInc = "";
            string txtAnnTax = "";
            string txtTaxRfnd = "";
            string txtTotalIncome = "";
            string txtTaxPaidUpdate = "";
            string txtCurrMntTax = "";
            string txtMSG = "";
            string txtTEST1 = "";
            string txtGEID = "";
            string txtForcastMnt = "";
            #endregion

            colsNVals.Clear();
            colsNVals.Add("PR_P_NO", PNO);
            colsNVals.Add("W_TAX_FROM", FROM);
            colsNVals.Add("W_TAX_TO", TO);

            rslt = cmnDM.GetData("CHRIS_SP_TAXINFORMATION_MANAGER", "PRLDTAX_FILL", colsNVals);
            if (rslt.isSuccessful)
            {
                if (rslt.dstResult.Tables.Count > 0)
                {
                    dtDTaxPaid = rslt.dstResult.Tables[0];
                    //this.dgvDT.DataSource = dtDTaxPaid;
                    List1 = JsonConvert.SerializeObject(dtDTaxPaid);
                }
                else
                    List1 = "";
            }

            colsNVals.Clear();
            colsNVals.Add("PR_P_NO", PNO);
            colsNVals.Add("W_TAX_FROM", FROM);
            colsNVals.Add("W_TAX_TO", TO);
            colsNVals.Add("PAYROLL_DATE", PAY_ROLL_DATE);

            rslt = cmnDM.GetData("CHRIS_SP_TAXINFORMATION_MANAGER", "ASR_FILL", colsNVals);
            if (rslt.isSuccessful)
            {
                if (rslt.dstResult.Tables.Count > 0)
                {
                    //this.dgvTDASR.DataSource = rslt.dstResult.Tables[0];
                    DataTable firstTable = rslt.dstResult.Tables[0];
                    List2 = JsonConvert.SerializeObject(firstTable);
                }
                else
                    List2 = "";
            }

            colsNVals.Clear();
            colsNVals.Add("PR_P_NO", PNO);
            colsNVals.Add("W_SYS", PAY_ROLL_DATE);
            colsNVals.Add("W_TAX_FROM", FROM); //year//


            //CALLING THE MAIN SP - VALID_PNO//
            rsltDtl = cmnDM.GetData("CHRIS_SP_TAXINFORMATION_VALID_PNO", "", colsNVals);
            if (rsltDtl.isSuccessful)
            {
                if (rsltDtl.dstResult.Tables.Count > 0)
                {
                    dtTaxQuery = rsltDtl.dstResult.Tables[0];

                    txtTaxAllow = "0";
                    txtSalArear = "0";
                    txtEducation = "0";
                    txtForcastAll = "0";
                    txtTOTCost = "0";
                    txtAccNum = "0";
                    //txtNatTaxNum = "0";


                    if (dtTaxQuery.Rows.Count > 0)
                    {
                        txtSalArear = Convert.ToString(dtTaxQuery.Rows[0]["w_sal_arear"]);
                        txtEducation = Convert.ToString(dtTaxQuery.Rows[0]["GLOBAL_W_INC_AMT"]);
                        txtDesig = Convert.ToString(dtTaxQuery.Rows[0]["W_DESIG"]);
                        txtLevel = Convert.ToString(dtTaxQuery.Rows[0]["W_LEVEL"]);
                        txtCategory = Convert.ToString(dtTaxQuery.Rows[0]["W_CATE"]);
                        txtIncrDate = (dtTaxQuery.Rows[0]["W_INC_EFF"] == DBNull.Value) ? "" : Convert.ToDateTime(dtTaxQuery.Rows[0]["W_INC_EFF"]).ToString("dd/MM/yyyy");
                        txtPromoDate = (dtTaxQuery.Rows[0]["W_PR_DATE"] == DBNull.Value) ? "" : Convert.ToDateTime(dtTaxQuery.Rows[0]["W_PR_DATE"]).ToString("dd/MM/yyyy");
                        txtJoinDate = (dtTaxQuery.Rows[0]["W_JOIN_DATE"] == DBNull.Value) ? "" : Convert.ToDateTime(dtTaxQuery.Rows[0]["W_JOIN_DATE"]).ToString("dd/MM/yyyy");

                        //txtAnnPkge = Convert.ToString(dtTaxQuery.Rows[0]["W_AN_PACK"]);
                        txtAnnPkge = (dtTaxQuery.Rows[0]["W_AN_PACK"] == DBNull.Value) ? "" : System.Math.Abs(Convert.ToInt32(dtTaxQuery.Rows[0]["W_AN_PACK"])).ToString();

                        //txtAnnBasic = Convert.ToString(dtTaxQuery.Rows[0]["W_ANNUAL_INCOME"]);
                        //txtAnnBasic = (dtTaxQuery.Rows[0]["W_ANNUAL_INCOME"] == DBNull.Value) ? "" : System.Math.Abs(Convert.ToInt32(dtTaxQuery.Rows[0]["W_ANNUAL_INCOME"])).ToString();

                        txtAnnBasic = (dtTaxQuery.Rows[0]["blk_w_annual_income"] == DBNull.Value) ? "" : System.Math.Abs(Convert.ToInt32(dtTaxQuery.Rows[0]["blk_w_annual_income"])).ToString();


                        //txtRVPTrans = dtTaxQuery.Rows[0]["W_VP"]==null? "0" : Convert.ToString(dtTaxQuery.Rows[0]["W_VP"]);
                        txtRVPTrans = "0";

                        //txtGEID = Convert.ToString(dtTaxQuery.Rows[0]["W_10_BONUS"]);
                        txtGEID = (dtTaxQuery.Rows[0]["W_10_BONUS"] == DBNull.Value) ? "" : System.Math.Abs(Convert.ToInt32(dtTaxQuery.Rows[0]["W_10_BONUS"])).ToString();
                        if (Convert.ToString(dtTaxQuery.Rows[0]["W_DESIG"]).ToUpper() == "VP" || Convert.ToString(dtTaxQuery.Rows[0]["W_DESIG"]).ToUpper() == "RVP")
                        {
                            txtGEID = Convert.ToString(dtTaxQuery.Rows[0]["w_gha_forcast"]);
                        }

                        txtTotal = Convert.ToString(dtTaxQuery.Rows[0]["W_TOTAL"]);

                        txtFunctional = (dtTaxQuery.Rows[0]["W_TAX_PACK"] == DBNull.Value) ? "" : System.Math.Abs(Convert.ToInt32(dtTaxQuery.Rows[0]["W_TAX_PACK"])).ToString();
                        //txtFunctional = Convert.ToString(dtTaxQuery.Rows[0]["W_TAX_PACK"]);

                        txtAccNum = Convert.ToString(dtTaxQuery.Rows[0]["SUBS_LOAN_TAXAMT"]);

                        txtOTCost = Convert.ToString(dtTaxQuery.Rows[0]["W_ACTUAL_OT"]);
                        txtTaxAllow = Convert.ToString(dtTaxQuery.Rows[0]["W_OTHER_ALL"]);

                        txtAvgOT = Convert.ToString(dtTaxQuery.Rows[0]["W_AVG"]);


                        txtOTArear = Convert.ToString(dtTaxQuery.Rows[0]["W_AREARS"]);

                        txtForcastMnt = Convert.ToString(dtTaxQuery.Rows[0]["W_FORCAST_MONTHS"]);
                        txtForcastOT = Convert.ToString(dtTaxQuery.Rows[0]["W_FORCAST_OT"]);

                        txtAvMnt = Convert.ToString(dtTaxQuery.Rows[0]["W_OT_MONTHS"]);

                        txtNatTaxNum = Convert.ToString(dtTaxQuery.Rows[0]["W_GOVT_DISP"]);

                        txtTaxInc = Convert.ToString(dtTaxQuery.Rows[0]["W_TAXABLE_INC"]);
                        txtAnnTax = Convert.ToString(dtTaxQuery.Rows[0]["W_A_TAX"]);

                        txtTaxRfnd = Convert.ToString(dtTaxQuery.Rows[0]["W_REFUND"]);

                        //txtTotalIncome = Convert.ToString(dtTaxQuery.Rows[0]["W_INCOME"]);
                        txtTotalIncome = (dtTaxQuery.Rows[0]["W_INCOME"] == DBNull.Value) ? "" : System.Math.Abs(Convert.ToInt32(dtTaxQuery.Rows[0]["W_INCOME"])).ToString();

                        txtTaxPaidUpdate = Convert.ToString(dtTaxQuery.Rows[0]["W_TAX_PAID"]);
                        txtCurrMntTax = Convert.ToString(dtTaxQuery.Rows[0]["W_ITAX"]);
                        txtMSG = Convert.ToString(dtTaxQuery.Rows[0]["MSG"]);
                        txtTEST1 = Convert.ToString(dtTaxQuery.Rows[0]["TEST1"]);
                        txtForcastAll = Convert.ToString(dtTaxQuery.Rows[0]["w_forcast_all"]);

                        var ResultData = new
                        {
                            List1 = List1,
                            List2 = List2,
                            txtDesig = txtDesig,
                            txtLevel = txtLevel,
                            txtCategory = txtCategory,
                            txtIncrDate = txtIncrDate,
                            txtPromoDate = txtPromoDate,
                            txtJoinDate = txtJoinDate,
                            txtAnnPkge = txtAnnPkge,
                            txtAnnBasic = txtAnnBasic,
                            txtRVPTrans = txtRVPTrans,
                            txtTaxAllow = txtTaxAllow,
                            txtTotal = txtTotal,
                            txtSalArear = txtSalArear,
                            txtFunctional = txtFunctional,
                            txtNatTaxNum = txtNatTaxNum,
                            txtAccNum = txtAccNum,
                            txtEducation = txtEducation,
                            txtOTCost = txtOTCost,
                            txtAvgOT = txtAvgOT,
                            txtOTArear = txtOTArear,
                            txtForcastOT = txtForcastOT,
                            txtTOTCost = txtTOTCost,
                            txtAvMnt = txtAvMnt,
                            txtForcastAll = txtForcastAll,
                            txtTaxInc = txtTaxInc,
                            txtAnnTax = txtAnnTax,
                            txtTaxRfnd = txtTaxRfnd,
                            txtTotalIncome = txtTotalIncome,
                            txtTaxPaidUpdate = txtTaxPaidUpdate,
                            txtCurrMntTax = txtCurrMntTax,
                            txtMSG = txtMSG,
                            txtTEST1 = txtTEST1,
                            txtGEID = txtGEID,
                            txtForcastMnt = txtForcastMnt
                        };
                        return Json(ResultData);
                    }

                }
                else
                {
                    txtDesig = "";
                    txtLevel = "";
                    txtCategory = "";
                    txtIncrDate = "";
                    txtPromoDate = "";
                    txtJoinDate = "";
                    txtAnnPkge = "";
                    txtAnnBasic = "";
                    txtRVPTrans = "";
                    txtTaxAllow = "";
                    txtTotal = "";
                    txtSalArear = "";
                    txtFunctional = "";
                    txtNatTaxNum = "";
                    txtAccNum = "";
                    txtEducation = "";
                    txtOTCost = "";
                    txtAvgOT = "";
                    txtOTArear = "";
                    txtForcastOT = "";
                    txtTOTCost = "";
                    txtAvMnt = "";
                    txtForcastAll = "";
                    txtTaxInc = "";
                    txtAnnTax = "";
                    txtTaxRfnd = "";
                    txtTotalIncome = "";
                    txtTaxPaidUpdate = "";
                    txtCurrMntTax = "";
                    txtMSG = "";
                    txtTEST1 = "";
                    var ResultData = new
                    {
                        List1 = List1,
                        List2 = List2,
                        txtDesig = txtDesig,
                        txtLevel = txtLevel,
                        txtCategory = txtCategory,
                        txtIncrDate = txtIncrDate,
                        txtPromoDate = txtPromoDate,
                        txtJoinDate = txtJoinDate,
                        txtAnnPkge = txtAnnPkge,
                        txtAnnBasic = txtAnnBasic,
                        txtRVPTrans = txtRVPTrans,
                        txtTaxAllow = txtTaxAllow,
                        txtTotal = txtTotal,
                        txtSalArear = txtSalArear,
                        txtFunctional = txtFunctional,
                        txtNatTaxNum = txtNatTaxNum,
                        txtAccNum = txtAccNum,
                        txtEducation = txtEducation,
                        txtOTCost = txtOTCost,
                        txtAvgOT = txtAvgOT,
                        txtOTArear = txtOTArear,
                        txtForcastOT = txtForcastOT,
                        txtTOTCost = txtTOTCost,
                        txtAvMnt = txtAvMnt,
                        txtForcastAll = txtForcastAll,
                        txtTaxInc = txtTaxInc,
                        txtAnnTax = txtAnnTax,
                        txtTaxRfnd = txtTaxRfnd,
                        txtTotalIncome = txtTotalIncome,
                        txtTaxPaidUpdate = txtTaxPaidUpdate,
                        txtCurrMntTax = txtCurrMntTax,
                        txtMSG = txtMSG,
                        txtTEST1 = txtTEST1,
                        txtGEID = txtGEID,
                        txtForcastMnt = txtForcastMnt
                    };
                    return Json(ResultData);
                }
            }
            return Json(null);
        }
        
        #endregion
        [HttpPost]
        public JsonResult GetSingleRecordAgainstPRNO(int? PRNO)
        {
            CHRIS_Context ic = new CHRIS_Context();
            var context = new CHRIS_ContextProcedures(ic);

            OutputParameter<int?> outParam = new OutputParameter<int?>();

            var res = context.CHRIS_SP_SALARYHISTORYQUERY_PERSONNEL_GETAsync(PRNO, outParam);
            res.Wait();
            return Json(res.Result);
        }

        [HttpPost]
        public JsonResult GetListRecordAgainstPRNO(int? PRNO)
        {
            CHRIS_Context ic = new CHRIS_Context();
            var context = new CHRIS_ContextProcedures(ic);

            OutputParameter<int?> outParam = new OutputParameter<int?>();

            var res = context.CHRIS_SP_SALARYHISTORYQUERY_INC_PROMOTION_GETALLAsync(PRNO, outParam);
            res.Wait();
            return Json(res.Result);
        }

        public IActionResult TaxInformationAllowance()
        {
            return View();
        }
        public IActionResult TaxInformationIncome()
        {
            return View();
        }
    }
}
