﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CHRIS.Controllers
{
    public class AuthenticationController : Controller
    {
        // GET: Authentication
        public ActionResult Index()
        {
            return Redirect("Login");//View();
        }

        public ActionResult Logout()
        {
            //if (Session["USER_ID"] != null)
            if (HttpContext.Session.GetString("USER_ID") != null)
            {
                HttpContext.Session.Clear();
            }
            HttpContext.SignOutAsync();
            //FormsAuthentication.SignOut();
            //Session.Clear();
            HttpContext.Session.Clear();
            //Response.Cache.SetCacheability(HttpCacheability.NoCache);
            HttpContext.Response.Headers["Cache-Control"] = "no-cache";
            //Response.ExpiresAbsolute = DateTime.UtcNow.AddDays(-1d);
            //Response.Expires = -1500;
            HttpContext.Response.Headers["Expires"] = DateTime.UtcNow.AddDays(-1d).ToString("R");
            //Response.CacheControl = "no-Cache";
            //Response.Cache.SetCacheability(HttpCacheability.NoCache);
            //Response.Cache.SetExpires(DateTime.UtcNow.AddHours(-1));
            //Response.Cache.SetNoStore();
            Response.Headers["Cache-Control"] = "no-store";

            //Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));
            Response.Cookies.Append("ASP.NET_SessionId", "");
            return RedirectToAction("Index", "Login");
        }
    }
}