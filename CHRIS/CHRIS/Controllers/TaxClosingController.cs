﻿using CHRIS.Common;
using CHRIS.Services;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Data;
using System.Data.SqlClient;

namespace CHRIS.Controllers
{
    public class TaxClosingController : Controller
    {
        public IActionResult _139TaxStatement()
        {
            return View();
        }

        #region  AnnualTaxProcessing
       
        public IActionResult AnnualTaxProcessing()
        {
            return View();
        }

        #region Declarations

        DateTime W_DATE = new DateTime();
        DateTime W_SYS = new DateTime();
        int W_TAX_FROM = 0;
        int W_TAX_TO = 0;

        string txtDate = "";
        string txtTaxTo = "";
        string txtTaxFrom = "";

        string sltxtPR_P_NO="";
        string sltxtPR_NAME="";
        string MessageBox = "";

        /*------For custom call in database------- */
        CmnDataManager objCmnDataManager;

        /*------To store input parameters of StoredProcedure------*/
        Dictionary<string, object> dicInputParameters = new Dictionary<string, object>();

        /*------To store data on form level-------------*/
        Dictionary<string, object> dicEmployeeDetail = new Dictionary<string, object>();

        /*------To contain personnel records,fetched via cursor in oracle-------------*/
        DataTable dtEmployeeDetail = null;

        #endregion

        #region Methods


        private void InitializeEmployeeDetailDictionary()
        {

            dicEmployeeDetail.Clear();

            dicEmployeeDetail.Add("W_PR_DATE", null);
            dicEmployeeDetail.Add("W_PFUND", 0);
            dicEmployeeDetail.Add("W_ITAX", 0);
            dicEmployeeDetail.Add("W_P_BASIC", 0);
            dicEmployeeDetail.Add("W_FORCAST", 0);
            dicEmployeeDetail.Add("W_DEPT_FT", null);
            dicEmployeeDetail.Add("W_ANS", null);
            dicEmployeeDetail.Add("W_GOVT_ALL", 0);
            dicEmployeeDetail.Add("W_INC_BONUS", 0);
            dicEmployeeDetail.Add("W_TAX_NO", null);
            dicEmployeeDetail.Add("W_ADD2", null);
            dicEmployeeDetail.Add("W_LEVEL", null);
            dicEmployeeDetail.Add("W_CATE", null);
            dicEmployeeDetail.Add("W_CONV_AMT", 0);
            dicEmployeeDetail.Add("W_TAX_PAID", 0);
            dicEmployeeDetail.Add("W_OTHER_ALL", 0);
            dicEmployeeDetail.Add("W_TAXABLE_INC", 0);
            dicEmployeeDetail.Add("W_REBATE", 0);
            dicEmployeeDetail.Add("W_TAX_FRM_AMT", 0);
            dicEmployeeDetail.Add("W_FIX_TAX", 0);
            dicEmployeeDetail.Add("W_REMAIN_AMT", 0);
            dicEmployeeDetail.Add("W_TAX_REMAIN", 0);
            dicEmployeeDetail.Add("W_SUR", 0);
            dicEmployeeDetail.Add("W_FLAG", 0);
            dicEmployeeDetail.Add("W_OT_ASR", 0);
            dicEmployeeDetail.Add("W_PROMOTED", null);
            dicEmployeeDetail.Add("W_SAL_AREAR", 0);
            dicEmployeeDetail.Add("W_BRANCH", null);
            dicEmployeeDetail.Add("W_ANNUAL_TAX", 0);
            dicEmployeeDetail.Add("W_BASIC_AREAR", 0);
            dicEmployeeDetail.Add("W_10_BONUS", 0);
            dicEmployeeDetail.Add("W_ANNUAL_INCOME", 0);
            dicEmployeeDetail.Add("W_ADD1", null);
            dicEmployeeDetail.Add("W_DESIG", null);
            dicEmployeeDetail.Add("W_HOUSE_RENT", 0);
            dicEmployeeDetail.Add("W_ZAKAT", 0);
            dicEmployeeDetail.Add("W_REFUND_AMT", 0);
            dicEmployeeDetail.Add("W_REFUND_FOR", null);
            dicEmployeeDetail.Add("W_GHA_FORCAST", 0);
            dicEmployeeDetail.Add("W_VP", 0);
            dicEmployeeDetail.Add("W_SEX", null);
            dicEmployeeDetail.Add("W_GHA", 0);


            //Not included in "INITIAL" program Unit
            dicEmployeeDetail.Add("PR_P_NO", 0);
            dicEmployeeDetail.Add("PR_NAME", null);
            dicEmployeeDetail.Add("W_AN_PACK", 0);
            dicEmployeeDetail.Add("W_PRV_INC", 0);
            dicEmployeeDetail.Add("W_PRV_TAX_PD", 0);
            dicEmployeeDetail.Add("W_TAX_USED", 0);
            dicEmployeeDetail.Add("W_JOIN_DATE", null);
            dicEmployeeDetail.Add("W_TRANS_DATE", null);
            dicEmployeeDetail.Add("W_TERMIN_DATE", null);
            dicEmployeeDetail.Add("W_TRANSFER", null);
            dicEmployeeDetail.Add("W_TAX_FROM_DATE", null);
            dicEmployeeDetail.Add("W_TAX_TO_DATE", null);
            dicEmployeeDetail.Add("W_SYS", null);
            dicEmployeeDetail.Add("W_DATE", null);
            dicEmployeeDetail.Add("W_INC_EFF", null);
            dicEmployeeDetail.Add("W_TAX_FROM", null);
            dicEmployeeDetail.Add("W_TAX_TO", null);

            dicEmployeeDetail.Add("W_ACTUAL_OT", 0);          //NUMBER
            dicEmployeeDetail.Add("W_AVG", 0);                //NUMBER
            dicEmployeeDetail.Add("W_AREARS", 0);             //NUMBER
            dicEmployeeDetail.Add("W_OT_MONTHS", null);       //CHARACTER
            dicEmployeeDetail.Add("W_FORCAST_MONTHS", null);  //CHARACTER
            dicEmployeeDetail.Add("W_FORCAST_OT", 0);         //NUMBER
            dicEmployeeDetail.Add("GLOBAL_W_10_BONUS", 0);
            dicEmployeeDetail.Add("W_CONV_ADDED", 0);
            dicEmployeeDetail.Add("W_SUBS_LOAN_TAX_AMT", 0);

            dicEmployeeDetail["W_SYS"] = W_SYS;
            dicEmployeeDetail["W_DATE"] = W_DATE;
            dicEmployeeDetail["W_TAX_TO"] = W_TAX_TO;
            dicEmployeeDetail["W_TAX_FROM"] = W_TAX_FROM;


            dicEmployeeDetail.Add("W_forcast_all", 0);
        }

        /// <summary>
        /// Fetch all employees with condition described in cursor in oracle form
        /// </summary>
        private void GetAllEmployees()
        {
            try
            {
                dicInputParameters.Clear();
                dicInputParameters.Add("pW_TAX_FROM", dicEmployeeDetail["W_TAX_FROM"]);
                dicInputParameters.Add("pW_TAX_TO", dicEmployeeDetail["W_TAX_TO"]);

                objCmnDataManager = new CmnDataManager();
                Result rsltAllEmployees = objCmnDataManager.GetData("CHRIS_SP_TAXYEARENDCLOSING_VALID_PNO", "VALID_PNO", dicInputParameters);

                if (rsltAllEmployees.isSuccessful)
                {
                    if (rsltAllEmployees.dstResult != null)
                    {
                        if (rsltAllEmployees.dstResult.Tables.Count > 0)
                        {
                            dtEmployeeDetail = rsltAllEmployees.dstResult.Tables[0];
                            if (rsltAllEmployees.dstResult.Tables[0].Rows.Count.Equals(0))
                            {
                                MessageBox = "TAX PROCESSING ALREADY DONE FOR " + dicEmployeeDetail["W_TAX_FROM"].ToString() + "-" + dicEmployeeDetail["W_TAX_TO"].ToString() + ".";
                               
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        /// <summary>
        /// Fill EmployeeDetailDictionary with EmployeeDetailDataTable
        /// </summary>
        /// 

        private void FillEmployeeDetailDictionary(int pRow)
        {
            try
            {

                #region Fill EmployeeDetailDictionary with EmployeeDetailDataTable

                dicEmployeeDetail["PR_P_NO"] = dtEmployeeDetail.Rows[pRow]["PR_P_NO"];
                dicEmployeeDetail["PR_NAME"] = dtEmployeeDetail.Rows[pRow]["PR_NAME"];
                dicEmployeeDetail["W_TAX_NO"] = dtEmployeeDetail.Rows[pRow]["pr_national_tax"];
                dicEmployeeDetail["W_ADD1"] = dtEmployeeDetail.Rows[pRow]["pr_add1"];
                dicEmployeeDetail["W_ADD2"] = dtEmployeeDetail.Rows[pRow]["pr_add2"];
                dicEmployeeDetail["W_CATE"] = dtEmployeeDetail.Rows[pRow]["pr_category"];
                dicEmployeeDetail["W_DESIG"] = dtEmployeeDetail.Rows[pRow]["pr_desig"];
                dicEmployeeDetail["W_LEVEL"] = dtEmployeeDetail.Rows[pRow]["pr_level"];
                dicEmployeeDetail["W_AN_PACK"] = dtEmployeeDetail.Rows[pRow]["pr_new_annual_pack"];
                dicEmployeeDetail["W_ZAKAT"] = dtEmployeeDetail.Rows[pRow]["pr_zakat_amt"];
                dicEmployeeDetail["W_PRV_INC"] = dtEmployeeDetail.Rows[pRow]["pr_tax_inc"];
                dicEmployeeDetail["W_PRV_TAX_PD"] = dtEmployeeDetail.Rows[pRow]["pr_tax_paid"];
                dicEmployeeDetail["W_TAX_USED"] = dtEmployeeDetail.Rows[pRow]["pr_tax_used"];
                dicEmployeeDetail["W_JOIN_DATE"] = dtEmployeeDetail.Rows[pRow]["pr_joining_date"];
                dicEmployeeDetail["W_BRANCH"] = dtEmployeeDetail.Rows[pRow]["pr_new_branch"];
                dicEmployeeDetail["W_PR_DATE"] = dtEmployeeDetail.Rows[pRow]["pr_promotion_date"];
                dicEmployeeDetail["W_PROMOTED"] = null;
                dicEmployeeDetail["W_TRANS_DATE"] = dtEmployeeDetail.Rows[pRow]["pr_transfer_date"];
                dicEmployeeDetail["W_TERMIN_DATE"] = dtEmployeeDetail.Rows[pRow]["pr_termin_date"];
                dicEmployeeDetail["W_TRANSFER"] = dtEmployeeDetail.Rows[pRow]["pr_transfer"];
                dicEmployeeDetail["W_SEX"] = dtEmployeeDetail.Rows[pRow]["pr_SEX"];
                dicEmployeeDetail["W_REFUND_AMT"] = dtEmployeeDetail.Rows[pRow]["pr_refund_amt"];
                dicEmployeeDetail["W_REFUND_FOR"] = dtEmployeeDetail.Rows[pRow]["pr_refund_for"];
                dicEmployeeDetail["W_GHA"] = 0;


                sltxtPR_P_NO = dtEmployeeDetail.Rows[pRow]["PR_P_NO"].ToString();
                sltxtPR_NAME = dtEmployeeDetail.Rows[pRow]["PR_NAME"].ToString();

              

                #endregion

                #region From date Calculation
                if ((dicEmployeeDetail["W_JOIN_DATE"] != System.DBNull.Value) && (!String.IsNullOrEmpty(dicEmployeeDetail["W_JOIN_DATE"].ToString()))) //1989-03-04 00:00:00.000
                {

                    DateTime dtJoiningDate = DateTime.Parse(dicEmployeeDetail["W_JOIN_DATE"].ToString());
                    DateTime dtTaxFromDate = DateTime.Parse(dicEmployeeDetail["W_TAX_FROM"].ToString() + "-06-30 00:00:00.000");    //30th june              

                    if (dtJoiningDate > dtTaxFromDate)
                    {

                        dicEmployeeDetail["W_TAX_FROM_DATE"] = dtJoiningDate.ToString("dd-MMM-yyyy");
                        dicEmployeeDetail["W_FLAG"] = 1;
                    }
                    else
                    {
                        dtTaxFromDate = DateTime.Parse(dicEmployeeDetail["W_TAX_FROM"].ToString() + "-07-01 00:00:00.000");        //1st july
                        dicEmployeeDetail["W_TAX_FROM_DATE"] = dtTaxFromDate.ToString("dd-MMM-yyyy");
                        dicEmployeeDetail["W_FLAG"] = 1;
                    }

                }
                #endregion

                #region To date Calculation

                if ((dicEmployeeDetail["W_TERMIN_DATE"] != System.DBNull.Value) && (!String.IsNullOrEmpty(dicEmployeeDetail["W_TERMIN_DATE"].ToString())))
                {
                    DateTime dtTerminationDate = DateTime.Parse(dicEmployeeDetail["W_TERMIN_DATE"].ToString());
                    dicEmployeeDetail["W_TAX_TO_DATE"] = dtTerminationDate.ToString("dd-MMM-yyyy");
                    dicEmployeeDetail["W_FLAG"] = 2;
                }
                else if ((dicEmployeeDetail["W_TRANSFER"] != System.DBNull.Value) && (!String.IsNullOrEmpty(dicEmployeeDetail["W_TRANSFER"].ToString())))
                {
                    if (Convert.ToInt32(dicEmployeeDetail["W_TRANSFER"]) > 5)
                    {
                        if ((dicEmployeeDetail["W_TRANS_DATE"] != System.DBNull.Value) && (!String.IsNullOrEmpty(dicEmployeeDetail["W_TRANS_DATE"].ToString())))
                        {
                            DateTime dtTransferDate = DateTime.Parse(dicEmployeeDetail["W_TRANS_DATE"].ToString());
                            dicEmployeeDetail["W_TAX_TO_DATE"] = dtTransferDate.ToString("dd-MMM-yyyy");
                        }
                        dicEmployeeDetail["W_FLAG"] = 2;
                    }
                    else
                    {
                        DateTime dtTaxToDate = DateTime.Parse(dicEmployeeDetail["W_TAX_TO"].ToString() + "-06-30 00:00:00.000");
                        dicEmployeeDetail["W_TAX_TO_DATE"] = dtTaxToDate.ToString("dd-MMM-yyyy");
                    }
                }
                else
                {
                    DateTime dtTaxToDate = DateTime.Parse(dicEmployeeDetail["W_TAX_TO"].ToString() + "-06-30 00:00:00.000");
                    dicEmployeeDetail["W_TAX_TO_DATE"] = dtTaxToDate.ToString("dd-MMM-yyyy");
                }

                #endregion

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Calculate Promotion
        /// </summary>
        private void GetPromotionProc()
        {
            try
            {
                dicInputParameters.Clear();
                dicInputParameters.Add("pPR_P_NO", dicEmployeeDetail["PR_P_NO"]);
                dicInputParameters.Add("pW_SYS", dicEmployeeDetail["W_SYS"]);
                dicInputParameters.Add("pW_PR_DATE", dicEmployeeDetail["W_PR_DATE"]);

                objCmnDataManager = new CmnDataManager();
                Result rsltPromotionProc = objCmnDataManager.GetData("CHRIS_SP_TAXYEARENDCLOSING_PROMOTION_PROC", "PROMOTION_PROC", dicInputParameters);

                if (rsltPromotionProc.isSuccessful)
                {
                    if (rsltPromotionProc.dstResult != null)
                    {
                        if (rsltPromotionProc.dstResult.Tables.Count > 0)
                        {
                            if (rsltPromotionProc.dstResult.Tables[0].Rows.Count > 0)
                            {

                                dicEmployeeDetail["W_DESIG"] = rsltPromotionProc.dstResult.Tables[0].Rows[0]["W_DESIG"];
                                dicEmployeeDetail["W_LEVEL"] = rsltPromotionProc.dstResult.Tables[0].Rows[0]["W_LEVEL"];
                                dicEmployeeDetail["W_PROMOTED"] = rsltPromotionProc.dstResult.Tables[0].Rows[0]["W_PROMOTED"];



                                if (rsltPromotionProc.dstResult.Tables[0].Columns.Count > 3)
                                {
                                    dicEmployeeDetail["W_AN_PACK"] = rsltPromotionProc.dstResult.Tables[0].Rows[0]["W_AN_PACK"];
                                    dicEmployeeDetail["W_CATE"] = rsltPromotionProc.dstResult.Tables[0].Rows[0]["W_CATE"];

                                    if (dicEmployeeDetail["W_CATE"].ToString() == "C")
                                    {
                                        GetGovernmentAllowance();
                                    }
                                }
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Calculate Government allowance
        /// </summary>
        private void GetGovernmentAllowance()
        {
            try
            {
                dicInputParameters.Clear();
                dicInputParameters.Add("pW_DESIG", dicEmployeeDetail["W_DESIG"]);
                dicInputParameters.Add("pW_LEVEL", dicEmployeeDetail["W_LEVEL"]);
                dicInputParameters.Add("pW_CATE", dicEmployeeDetail["W_CATE"]);
                dicInputParameters.Add("pW_BRANCH", dicEmployeeDetail["W_BRANCH"]);
                dicInputParameters.Add("pW_DATE", dicEmployeeDetail["W_DATE"]);

                objCmnDataManager = new CmnDataManager();
                Result rsltGovernmentAllowance = objCmnDataManager.GetData("CHRIS_SP_TAXYEARENDCLOSING_GOVT_ALL", "GOVT_ALL", dicInputParameters);

                if (rsltGovernmentAllowance.isSuccessful)
                {
                    if (rsltGovernmentAllowance.dstResult != null)
                    {
                        if (rsltGovernmentAllowance.dstResult.Tables.Count > 0)
                        {
                            if (rsltGovernmentAllowance.dstResult.Tables[0].Rows.Count > 0)
                            {
                                dicEmployeeDetail["W_GOVT_ALL"] = rsltGovernmentAllowance.dstResult.Tables[0].Rows[0]["W_GOVT_ALL"];
                            }
                        }

                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Calculate last increase
        /// </summary>
        private void GetLastIncrease()
        {
            try
            {
                dicInputParameters.Clear();
                dicInputParameters.Add("pPR_P_NO", dicEmployeeDetail["PR_P_NO"]);
                dicInputParameters.Add("pW_SYS", dicEmployeeDetail["W_SYS"]);

                objCmnDataManager = new CmnDataManager();
                Result rslt = objCmnDataManager.GetData("CHRIS_SP_TAXYEARENDCLOSING_LAST_INCREASE", "LAST_INCREASE", dicInputParameters);

                if (rslt.isSuccessful)
                {
                    if (rslt.dstResult != null)
                    {
                        if (rslt.dstResult.Tables.Count > 0)
                        {
                            if (rslt.dstResult.Tables[0].Rows.Count > 0)
                            {
                                dicEmployeeDetail["W_INC_EFF"] = rslt.dstResult.Tables[0].Rows[0]["W_INC_EFF"];
                            }
                        }

                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Calculate Z Tax
        /// </summary>
        private void GetZ_Tax()
        {
            try
            {
                dicInputParameters.Clear();

                dicInputParameters.Add("pPR_P_NO", dicEmployeeDetail["PR_P_NO"]);
                dicInputParameters.Add("pW_SYS", dicEmployeeDetail["W_SYS"]);
                dicInputParameters.Add("pW_DATE", dicEmployeeDetail["W_DATE"]);
                dicInputParameters.Add("pW_PR_DATE", dicEmployeeDetail["W_PR_DATE"]);
                dicInputParameters.Add("pW_BRANCH", dicEmployeeDetail["W_BRANCH"]);
                dicInputParameters.Add("pW_DESIG", dicEmployeeDetail["W_DESIG"]);
                dicInputParameters.Add("pW_LEVEL", dicEmployeeDetail["W_LEVEL"]);
                dicInputParameters.Add("pW_CATE", dicEmployeeDetail["W_CATE"]);
                dicInputParameters.Add("pW_AN_PACK", dicEmployeeDetail["W_AN_PACK"]);
                dicInputParameters.Add("pGLOBAL_W_10_BONUS", dicEmployeeDetail["GLOBAL_W_10_BONUS"]);
                dicInputParameters.Add("pW_ZAKAT", dicEmployeeDetail["W_ZAKAT"]);
                dicInputParameters.Add("pW_PRV_INC", dicEmployeeDetail["W_PRV_INC"]);
                dicInputParameters.Add("pW_TAX_USED", dicEmployeeDetail["W_TAX_USED"]);
                dicInputParameters.Add("pW_JOIN_DATE", dicEmployeeDetail["W_JOIN_DATE"]);
                dicInputParameters.Add("pW_TAX_TO_DATE", dicEmployeeDetail["W_TAX_TO_DATE"]);
                dicInputParameters.Add("pW_SEX", dicEmployeeDetail["W_SEX"]);
                dicInputParameters.Add("pW_REFUND_AMT", dicEmployeeDetail["W_REFUND_AMT"]);
                dicInputParameters.Add("W_forcast_all", dicEmployeeDetail["W_forcast_all"]);

                //if (dicEmployeeDetail["PR_P_NO"].ToString() == "2085" || dicEmployeeDetail["PR_P_NO"].ToString() == "2069" || dicEmployeeDetail["PR_P_NO"].ToString() == "2047")
                //{
                //    MessageBox.Show("pr_p_no=" + dicEmployeeDetail["PR_P_NO"] + ", w_forcast_all=" + dicEmployeeDetail["W_forcast_all"]);
                //}


                objCmnDataManager = new CmnDataManager();
                Result rslt = objCmnDataManager.Execute("CHRIS_SP_TAXYEARENDCLOSING_Z_TAX", " ", dicInputParameters, 0);

                if (rslt.isSuccessful)
                {
                    if (rslt.dstResult != null)
                    {
                        if (rslt.dstResult.Tables.Count > 0)
                        {
                            if (rslt.dstResult.Tables[0].Rows.Count > 0)
                            {

                                dicEmployeeDetail["W_FIX_TAX"] = rslt.dstResult.Tables[0].Rows[0]["W_FIX_TAX"];
                                dicEmployeeDetail["W_TAX_REMAIN"] = rslt.dstResult.Tables[0].Rows[0]["W_TAX_REMAIN"];
                                dicEmployeeDetail["W_TAXABLE_INC"] = rslt.dstResult.Tables[0].Rows[0]["W_TAXABLE_INC"];
                                dicEmployeeDetail["W_INC_BONUS"] = rslt.dstResult.Tables[0].Rows[0]["W_INC_BONUS"];
                                dicEmployeeDetail["W_ANNUAL_INCOME"] = rslt.dstResult.Tables[0].Rows[0]["W_ANNUAL_INCOME"];
                                dicEmployeeDetail["W_HOUSE_RENT"] = rslt.dstResult.Tables[0].Rows[0]["W_HOUSE_RENT"];
                                dicEmployeeDetail["W_CONV_ADDED"] = rslt.dstResult.Tables[0].Rows[0]["W_CONV_ADDED"];
                                dicEmployeeDetail["W_TAX_PAID"] = rslt.dstResult.Tables[0].Rows[0]["W_TAX_PAID"];
                                dicEmployeeDetail["W_OTHER_ALL"] = rslt.dstResult.Tables[0].Rows[0]["W_OTHER_ALL"];
                                dicEmployeeDetail["W_PFUND"] = rslt.dstResult.Tables[0].Rows[0]["W_PFUND"];
                                dicEmployeeDetail["W_TAX_FRM_AMT"] = rslt.dstResult.Tables[0].Rows[0]["W_TAX_FRM_AMT"];
                                dicEmployeeDetail["W_REMAIN_AMT"] = rslt.dstResult.Tables[0].Rows[0]["W_REMAIN_AMT"];
                                dicEmployeeDetail["W_SUR"] = rslt.dstResult.Tables[0].Rows[0]["W_SUR"];
                                dicEmployeeDetail["W_SUBS_LOAN_TAX_AMT"] = rslt.dstResult.Tables[0].Rows[0]["SUBS_LOAN_TAX_AMT"];

                                dicEmployeeDetail["W_forcast_all"] = rslt.dstResult.Tables[0].Rows[0]["W_forcast_all"];
                                dicEmployeeDetail["W_GHA"] = rslt.dstResult.Tables[0].Rows[0]["w_gha"];
                            }
                        }

                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Calculate Rebate
        /// </summary>
        private void Rebate()
        {
            try
            {
                dicInputParameters.Clear();

                dicInputParameters.Add("pw_fix_tax", dicEmployeeDetail["W_FIX_TAX"]);
                dicInputParameters.Add("pw_tax_remain", dicEmployeeDetail["W_TAX_REMAIN"]);
                dicInputParameters.Add("pw_taxable_inc", dicEmployeeDetail["W_TAXABLE_INC"]);

                objCmnDataManager = new CmnDataManager();
                Result rslt = objCmnDataManager.GetData("CHRIS_SP_TAXYEARENDCLOSING_REBATE", "Rebate", dicInputParameters);

                if (rslt.isSuccessful)
                {
                    if (rslt.dstResult != null)
                    {
                        if (rslt.dstResult.Tables.Count > 0)
                        {
                            if (rslt.dstResult.Tables[0].Rows.Count > 0)
                            {
                                dicEmployeeDetail["W_REBATE"] = rslt.dstResult.Tables[0].Rows[0]["w_REBATE"];
                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Insert into Annual Tax
        /// </summary>
        private void AddProc()
        {
            try
            {
                dicInputParameters.Clear();

                dicInputParameters.Add("pPR_P_NO", dicEmployeeDetail["PR_P_NO"]);
                dicInputParameters.Add("pPR_NAME", dicEmployeeDetail["PR_NAME"]);
                dicInputParameters.Add("pW_TAX_NO", dicEmployeeDetail["W_TAX_NO"]);
                dicInputParameters.Add("pW_ADD1", dicEmployeeDetail["W_ADD1"]);
                dicInputParameters.Add("pW_ADD2", dicEmployeeDetail["W_ADD2"]);
                dicInputParameters.Add("pW_DESIG", dicEmployeeDetail["W_DESIG"]);
                dicInputParameters.Add("pW_LEVEL", dicEmployeeDetail["W_LEVEL"]);
                dicInputParameters.Add("pW_BRANCH", dicEmployeeDetail["W_BRANCH"]);
                dicInputParameters.Add("pW_CATE", dicEmployeeDetail["W_CATE"]);
                dicInputParameters.Add("pW_TAX_FROM", dicEmployeeDetail["W_TAX_FROM"]);
                dicInputParameters.Add("pW_TAX_TO", dicEmployeeDetail["W_TAX_TO"]);
                dicInputParameters.Add("pW_TAX_FROM_DATE", dicEmployeeDetail["W_TAX_FROM_DATE"]);
                dicInputParameters.Add("pW_TAX_TO_DATE", dicEmployeeDetail["W_TAX_TO_DATE"]);
                dicInputParameters.Add("pW_10_BONUS", dicEmployeeDetail["W_10_BONUS"]);
                dicInputParameters.Add("pW_INC_BONUS", dicEmployeeDetail["W_INC_BONUS"]);
                dicInputParameters.Add("pW_ANNUAL_INCOME", dicEmployeeDetail["W_ANNUAL_INCOME"]);
                dicInputParameters.Add("pW_HOUSE_RENT", dicEmployeeDetail["W_HOUSE_RENT"]);
                dicInputParameters.Add("pW_CONV_ADDED", dicEmployeeDetail["W_CONV_ADDED"]);
                dicInputParameters.Add("pW_ZAKAT", dicEmployeeDetail["W_ZAKAT"]);
                dicInputParameters.Add("pW_FORCAST", dicEmployeeDetail["W_FORCAST"]);
                dicInputParameters.Add("pW_TAX_PAID", dicEmployeeDetail["W_TAX_PAID"]);
                dicInputParameters.Add("pW_OTHER_ALL", dicEmployeeDetail["W_OTHER_ALL"]);
                dicInputParameters.Add("pW_GHA", dicEmployeeDetail["W_GHA"]);
                dicInputParameters.Add("pW_TAXABLE_INC", dicEmployeeDetail["W_TAXABLE_INC"]);
                dicInputParameters.Add("pW_PFUND", dicEmployeeDetail["W_PFUND"]);
                dicInputParameters.Add("pW_REBATE", dicEmployeeDetail["W_REBATE"]);
                dicInputParameters.Add("pW_TAX_FRM_AMT", dicEmployeeDetail["W_TAX_FRM_AMT"]);
                dicInputParameters.Add("pW_FIX_TAX", dicEmployeeDetail["W_FIX_TAX"]);
                dicInputParameters.Add("pW_REMAIN_AMT", dicEmployeeDetail["W_REMAIN_AMT"]);
                dicInputParameters.Add("pW_TAX_REMAIN", dicEmployeeDetail["W_TAX_REMAIN"]);
                dicInputParameters.Add("pW_SUR", dicEmployeeDetail["W_SUR"]);
                dicInputParameters.Add("pW_REFUND_AMT", dicEmployeeDetail["W_REFUND_AMT"]);
                dicInputParameters.Add("pW_REFUND_FOR", dicEmployeeDetail["W_REFUND_FOR"]);
                dicInputParameters.Add("pW_FLAG", dicEmployeeDetail["W_FLAG"]);
                dicInputParameters.Add("pSUBS_LOAN_TAX_AMT", dicEmployeeDetail["W_SUBS_LOAN_TAX_AMT"]);

                objCmnDataManager = new CmnDataManager();
                Result rsltPromotionProc = objCmnDataManager.GetData("CHRIS_SP_TAXYEARENDCLOSING_ADD_PROC", "", dicInputParameters);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Start year End closing process
        /// </summary>
        public void StartTaxProcessing()
        {
            DateTime wIncEff;
            DateTime wPrDate;
            DateTime wSys;

            try
            {

                #region INITIAL

                InitializeEmployeeDetailDictionary();
                #endregion             

                #region VALID_PNO
                GetAllEmployees();
                #endregion

                //If employee exist
                if (dtEmployeeDetail != null)
                {
                    for (int EmployeeNo = 0; EmployeeNo < dtEmployeeDetail.Rows.Count; EmployeeNo++)
                    {
                        //dtEmployeeDetail.Rows.Count
                        //int EmployeeNo = 0; // for testing purrpose
                        FillEmployeeDetailDictionary(EmployeeNo);

                        #region PROMOTION_PROC
                        GetPromotionProc();
                        #endregion

                        #region LAST_INCREASE
                        GetLastIncrease();
                        #endregion

                        #region Z_TAX
                        GetZ_Tax();
                        #endregion

                        #region VALID_PNO Part II

                        DateTime.TryParse(dicEmployeeDetail["W_PR_DATE"].ToString(), out wPrDate);
                        DateTime.TryParse(dicEmployeeDetail["W_SYS"].ToString(), out wSys);
                        DateTime.TryParse(dicEmployeeDetail["W_INC_EFF"].ToString(), out wIncEff);

                        if (wPrDate > wSys)
                            dicEmployeeDetail["W_PR_DATE"] = null;

                        if (wIncEff > wSys)
                            dicEmployeeDetail["W_INC_EFF"] = null;

                        #endregion

                        #region REBATE
                        Rebate();
                        #endregion

                        #region ADD_PROC
                        AddProc();
                        #endregion

                        #region VALID_PNO Part III
                        try
                        {
                            dicInputParameters.Clear();
                            dicInputParameters.Add("pW_TAX_FROM", dicEmployeeDetail["W_TAX_FROM"]);
                            dicInputParameters.Add("pW_TAX_TO", dicEmployeeDetail["W_TAX_TO"]);

                            objCmnDataManager = new CmnDataManager();
                            //Result rsltAllEmployees = objCmnDataManager.GetData("CHRIS_SP_TAXYEARENDCLOSING_VALID_PNO", "UPDATE_ANNUAL_TAX", dicInputParameters);
                            objCmnDataManager.GetData("CHRIS_SP_TAXYEARENDCLOSING_VALID_PNO", "UPDATE_ANNUAL_TAX", dicInputParameters);

                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }

                        #endregion
                    }

                    MessageBox = "PROCESSING COMPLETED";


                }
            }
            catch (Exception ex)
            {
                MessageBox = ex.Message;
            }
        }

        #endregion      
        public ActionResult RunAnnualTaxProcessing(string Target)
        {
           

            if ((Target != "YES") && (Target != "NO"))
            {
                MessageBox = "PLEASE ENTER [YES] OR [NO] ........";
                return Json(MessageBox);
            }
            else if (Target == "NO")
            {
                return Json(null);
            }
            else if (Target == "YES")
            {               
                
                try
                {
                    objCmnDataManager = new CmnDataManager();
                    Result rsltCodeProcessingDates = objCmnDataManager.GetData("CHRIS_SP_TAXYEARENDCLOSING_MANAGER", "YEARENDCLOSING_YES");

                    if (rsltCodeProcessingDates.isSuccessful)
                    {
                        if (rsltCodeProcessingDates.dstResult.Tables.Count > 0)
                        {
                            if (rsltCodeProcessingDates.dstResult.Tables[0].Rows.Count > 0)
                            {
                                if ((!String.IsNullOrEmpty(Convert.ToString(rsltCodeProcessingDates.dstResult.Tables[0].Rows[0]["W_SYS"]))) && (rsltCodeProcessingDates.dstResult.Tables[0].Rows[0]["W_SYS"] != System.DBNull.Value))
                                {
                                    DateTime dtSysDateTime = DateTime.Parse(rsltCodeProcessingDates.dstResult.Tables[0].Rows[0]["W_SYS"].ToString());
                                    W_SYS = dtSysDateTime;
                                    W_DATE = dtSysDateTime;
                                    txtDate = dtSysDateTime.ToString("dd/MM/yyyy");
                                }
                                if ((!String.IsNullOrEmpty(Convert.ToString(rsltCodeProcessingDates.dstResult.Tables[0].Rows[0]["W_TAX_TO"]))) && (rsltCodeProcessingDates.dstResult.Tables[0].Rows[0]["W_TAX_TO"] != System.DBNull.Value))
                                {
                                    W_TAX_TO = Convert.ToInt32(rsltCodeProcessingDates.dstResult.Tables[0].Rows[0]["W_TAX_TO"]);
                                    txtTaxTo = rsltCodeProcessingDates.dstResult.Tables[0].Rows[0]["W_TAX_TO"].ToString();
                                }
                                if ((!String.IsNullOrEmpty(Convert.ToString(rsltCodeProcessingDates.dstResult.Tables[0].Rows[0]["W_TAX_FROM"]))) && (rsltCodeProcessingDates.dstResult.Tables[0].Rows[0]["W_TAX_FROM"] != System.DBNull.Value))
                                {
                                    W_TAX_FROM = Convert.ToInt32(rsltCodeProcessingDates.dstResult.Tables[0].Rows[0]["W_TAX_FROM"]);
                                    txtTaxFrom = rsltCodeProcessingDates.dstResult.Tables[0].Rows[0]["W_TAX_FROM"].ToString();
                                }
                                StartTaxProcessing();
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox = ex.Message;
                    return Json(MessageBox);
                }

                var res = new
                {
                        W_DATE = W_DATE,
                        W_SYS = W_SYS,
                        W_TAX_FROM = W_TAX_FROM,
                        W_TAX_TO = W_TAX_TO,
                        txtDate = txtDate,
                        txtTaxTo = txtTaxTo,
                        txtTaxFrom = txtTaxFrom,
                        sltxtPR_P_NO = sltxtPR_P_NO,
                        sltxtPR_NAME = sltxtPR_NAME,
                        MessageBox = MessageBox,
                };
                return Json(res);

            }
            return Json(null);
        }

        #endregion

        public IActionResult AnnualTaxProcessing_ProcessingTaxFor()
        {
            return View();
        }
        public IActionResult DeductionLetterReport()
        {
            return View();
        }
        public IActionResult IncomeTaxAssessment()
        {
            return View();
        }
        public IActionResult IncomeTaxAssessmentStatus()
        {
            return View();
        }
        public IActionResult NTNCardsStatus()
        {
            return View();
        }
        public IActionResult NTNCertification()
        {
            return View();
        }
        public IActionResult ProvidentFundDeductionLetter()
        {
            return View();
        }


        #region ProvidentFundEntry

        public IActionResult ProvidentFundEntry()
        {
            return View();
        }

        public ActionResult btn_AN_P_NO_PFE()
        {
            Dictionary<string, object> d = new Dictionary<string, object>();

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_BALANCEUPDATION_ANNUAL_TAX_MANAGER", "AnnualTaxLOV", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        public ActionResult Submit_PFE(Decimal? AN_P_NO, string AN_P_NAME, string AN_TAX_NUM, string AN_ADD1, string AN_ADD2, string AN_DESIG, string AN_BRANCH, string AN_CATEGORY, string AN_LEVEL, Decimal? AN_ASSES_FROM, Decimal? AN_ASSES_TO, DateTime? AN_TAX_FROM_DATE, DateTime? AN_TAX_TO_DATE, Decimal? AN_10C_BONUS, Decimal? AN_INC_BONUS, Decimal? AN_BASIC, Decimal? AN_HOUSE_RENT, Decimal? AN_CONV, Decimal? AN_CONV_ADDED, Decimal? AN_ZAKAT, Decimal? AN_OVT, Decimal? AN_TAX_ASR, Decimal? AN_TAX_PAID, Decimal? AN_TAXABLE_INC, Decimal? AN_PFUND, Decimal? AN_REBATE, Decimal? AN_TAX_FROM_AMT, Decimal? AN_FIX_TAX, Decimal? AN_REM_AMT, Decimal? AN_TAX_REM, Decimal? AN_SUR, Decimal? AN_PF_BAL, string AN_FLAG, Decimal? AN_TAXABLE_ALL, Decimal? AN_LFA, Decimal? AN_REFUND_AMT, string AN_REFUND_FOR, Decimal? AN_GHA_ALL, Decimal? AN_EDUCATION, Decimal? AN_SER_SAL, Decimal? AN_L_H_WATER, Decimal? AN_RNT_FREE_UNFURNISHED, Decimal? AN_RNT_FREE_FURNISHED, Decimal? PRIVATE, Decimal? GRATUITY, Decimal? UNFURNISHED_SQ, Decimal? FURNISHED_SQ, Decimal? AN_SEVARANCE_AMT, Decimal? AN_SUBS_LOAN_TAX, int? ID)
        {
            try
            {
                Dictionary<string, object> d = new Dictionary<string, object>();

                d.Add("AN_P_NO",AN_P_NO);
                d.Add("AN_P_NAME",AN_P_NAME);
                d.Add("AN_TAX_NUM",AN_TAX_NUM);
                d.Add("AN_ADD1",AN_ADD1);
                d.Add("AN_ADD2",AN_ADD2);
                d.Add("AN_DESIG",AN_DESIG);
                d.Add("AN_BRANCH",AN_BRANCH);
                d.Add("AN_CATEGORY",AN_CATEGORY);
                d.Add("AN_LEVEL",AN_LEVEL);
                d.Add("AN_ASSES_FROM",AN_ASSES_FROM);
                d.Add("AN_ASSES_TO",AN_ASSES_TO);
                d.Add("AN_TAX_FROM_DATE",AN_TAX_FROM_DATE);
                d.Add("AN_TAX_TO_DATE",AN_TAX_TO_DATE);
                d.Add("AN_10C_BONUS",AN_10C_BONUS);
                d.Add("AN_INC_BONUS",AN_INC_BONUS);
                d.Add("AN_BASIC",AN_BASIC);
                d.Add("AN_HOUSE_RENT",AN_HOUSE_RENT);
                d.Add("AN_CONV",AN_CONV);
                d.Add("AN_CONV_ADDED",AN_CONV_ADDED);
                d.Add("AN_ZAKAT",AN_ZAKAT);
                d.Add("AN_OVT",AN_OVT);
                d.Add("AN_TAX_ASR",AN_TAX_ASR);
                d.Add("AN_TAX_PAID",AN_TAX_PAID);
                d.Add("AN_TAXABLE_INC",AN_TAXABLE_INC);
                d.Add("AN_PFUND",AN_PFUND);
                d.Add("AN_REBATE",AN_REBATE);
                d.Add("AN_TAX_FROM_AMT",AN_TAX_FROM_AMT);
                d.Add("AN_FIX_TAX",AN_FIX_TAX);
                d.Add("AN_REM_AMT",AN_REM_AMT);
                d.Add("AN_TAX_REM",AN_TAX_REM);
                d.Add("AN_SUR",AN_SUR);
                d.Add("AN_PF_BAL",AN_PF_BAL);
                d.Add("AN_FLAG",AN_FLAG);
                d.Add("AN_TAXABLE_ALL",AN_TAXABLE_ALL);
                d.Add("AN_LFA",AN_LFA);
                d.Add("AN_REFUND_AMT",AN_REFUND_AMT);
                d.Add("AN_REFUND_FOR",AN_REFUND_FOR);
                d.Add("AN_GHA_ALL",AN_GHA_ALL);
                d.Add("AN_EDUCATION",AN_EDUCATION);
                d.Add("AN_SER_SAL",AN_SER_SAL);
                d.Add("AN_L_H_WATER",AN_L_H_WATER);
                d.Add("AN_RNT_FREE_UNFURNISHED",AN_RNT_FREE_UNFURNISHED);
                d.Add("AN_RNT_FREE_FURNISHED",AN_RNT_FREE_FURNISHED);
                d.Add("PRIVATE",PRIVATE);
                d.Add("GRATUITY",GRATUITY);
                d.Add("UNFURNISHED_SQ",UNFURNISHED_SQ);
                d.Add("FURNISHED_SQ",FURNISHED_SQ);
                d.Add("AN_SEVARANCE_AMT",AN_SEVARANCE_AMT);
                d.Add("AN_SUBS_LOAN_TAX", AN_SUBS_LOAN_TAX);
                d.Add("ID", ID);

                CmnDataManager objCmnDataManager = new CmnDataManager();

                if (ID != null)
                {
                    Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_BALANCEUPDATION_ANNUAL_TAX_MANAGER", "Update", d);
                    if (rsltCode.isSuccessful)
                    {
                        var res = Json(rsltCode);
                        return Json("Your request is modify successfully !");
                    }
                }
                else
                {
                    Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_BALANCEUPDATION_ANNUAL_TAX_MANAGER", "Save", d);
                    if (rsltCode.isSuccessful)
                    {
                        var res = Json(rsltCode);
                        return Json("Your request is successfully completed !");
                    }
                }
                return Json(null);
            }
            catch (Exception ex)
            {
                return Json("Something went wrong");
            }
            return Json(null);
        }


        #endregion

        public IActionResult SalaryCertificate()
        {
            return View();
        }
        public IActionResult TaxAssessmentStatus()
        {
            return View();
        }

        #region TxUptEntry

        public IActionResult TxUptEntry()
        {
            return View();
        }

        public ActionResult btn_F_PNO_TAX_U_E()
            {
            Dictionary<string, object> d = new Dictionary<string, object>();

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_TAX_UPDATION_ENTRY_ANNUAL_TAX_MANAGER", "MASTER_F_PNO_LOV0", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        public ActionResult F_PNO_TAX_U_E_Validation(string ID)
        {
            if (ID != "")
            {
                return Json(null);
            }
            else
            {                
                {
                    MessageBox = "Personnel No. Not Found In Tax Work File ........!";
                    return Json(MessageBox);
                }
            }
            return Json(null);
        }

        public ActionResult Submit_TxUptEntry(Decimal? F_PNO, string AN_P_NAME, string AN_TAX_NUM, string AN_ADD1, string AN_ADD2, string AN_DESIG, string AN_BRANCH, string AN_CATEGORY, string AN_LEVEL, Decimal? AN_ASSES_FROM, Decimal? AN_ASSES_TO, DateTime? AN_TAX_FROM_DATE, DateTime? AN_TAX_TO_DATE, Decimal? AN_10C_BONUS, Decimal? AN_INC_BONUS, Decimal? AN_BASIC, Decimal? AN_HOUSE_RENT, Decimal? AN_CONV, Decimal? AN_CONV_ADDED, Decimal? AN_ZAKAT, Decimal? AN_OVT, Decimal? AN_TAX_ASR, Decimal? AN_TAX_PAID, Decimal? AN_TAXABLE_INC, Decimal? AN_PFUND, Decimal? AN_REBATE, Decimal? AN_TAX_FROM_AMT, Decimal? AN_FIX_TAX, Decimal? AN_REM_AMT, Decimal? AN_TAX_REM, Decimal? AN_SUR, Decimal? AN_PF_BAL, string AN_FLAG, Decimal? AN_TAXABLE_ALL, Decimal? AN_LFA, Decimal? AN_REFUND_AMT, string AN_REFUND_FOR, Decimal? AN_GHA_ALL, Decimal? AN_EDUCATION, Decimal? AN_SER_SAL, Decimal? AN_L_H_WATER, Decimal? AN_RNT_FREE_UNFURNISHED, Decimal? AN_RNT_FREE_FURNISHED, Decimal? PRIVATE, Decimal? GRATUITY, Decimal? UNFURNISHED_SQ, Decimal? FURNISHED_SQ, Decimal? AN_SEVARANCE_AMT, Decimal? AN_SUBS_LOAN_TAX, int? ID)
        {
            try
            {
                Dictionary<string, object> d = new Dictionary<string, object>();

                d.Add("F_PNO", F_PNO );  
	   			d.Add("AN_P_NAME", AN_P_NAME);
	   			d.Add("AN_TAX_NUM", AN_TAX_NUM);
	   			d.Add("AN_ADD1", AN_ADD1);
	   			d.Add("AN_ADD2", AN_ADD2);
	   			d.Add("AN_DESIG", AN_DESIG);
	   			d.Add("AN_BRANCH", AN_BRANCH);
	   			d.Add("AN_CATEGORY", AN_CATEGORY);
	   			d.Add("AN_LEVEL", AN_LEVEL);
	   			d.Add("AN_ASSES_FROM", AN_ASSES_FROM);
	   			d.Add("AN_ASSES_TO", AN_ASSES_TO);
	   			d.Add("AN_TAX_FROM_DATE", AN_TAX_FROM_DATE);
	   			d.Add("AN_TAX_TO_DATE", AN_TAX_TO_DATE);
	   			d.Add("AN_10C_BONUS", AN_10C_BONUS);
	   			d.Add("AN_INC_BONUS", AN_INC_BONUS);
	   			d.Add("AN_BASIC", AN_BASIC);
	   			d.Add("AN_HOUSE_RENT", AN_HOUSE_RENT);
	   			d.Add("AN_CONV", AN_CONV);
	   			d.Add("AN_CONV_ADDED", AN_CONV_ADDED);
	   			d.Add("AN_ZAKAT", AN_ZAKAT);
	   			d.Add("AN_OVT", AN_OVT);
	   			d.Add("AN_TAX_ASR", AN_TAX_ASR);
	   			d.Add("AN_TAX_PAID", AN_TAX_PAID);
	   			d.Add("AN_TAXABLE_INC", AN_TAXABLE_INC);
	   			d.Add("AN_PFUND", AN_PFUND);
	   			d.Add("AN_REBATE", AN_REBATE);
	   			d.Add("AN_TAX_FROM_AMT", AN_TAX_FROM_AMT);
	   			d.Add("AN_FIX_TAX", AN_FIX_TAX);
	   			d.Add("AN_REM_AMT", AN_REM_AMT);
	   			d.Add("AN_TAX_REM", AN_TAX_REM);
	   			d.Add("AN_SUR", AN_SUR);
	   			d.Add("AN_PF_BAL", AN_PF_BAL);
	   			d.Add("AN_FLAG", AN_FLAG);
	   			d.Add("AN_TAXABLE_ALL", AN_TAXABLE_ALL);
	   			d.Add("AN_LFA", AN_LFA);
	   			d.Add("AN_REFUND_AMT", AN_REFUND_AMT);
	   			d.Add("AN_REFUND_FOR", AN_REFUND_FOR);
	   			d.Add("AN_GHA_ALL", AN_GHA_ALL);
	   			d.Add("AN_EDUCATION", AN_EDUCATION);
	   			d.Add("AN_SER_SAL", AN_SER_SAL);
	   			d.Add("AN_L_H_WATER", AN_L_H_WATER);
	   			d.Add("AN_RNT_FREE_UNFURNISHED", AN_RNT_FREE_UNFURNISHED);
	   			d.Add("AN_RNT_FREE_FURNISHED", AN_RNT_FREE_FURNISHED);
	   			d.Add("PRIVATE", PRIVATE);
	   			d.Add("GRATUITY", GRATUITY);
	   			d.Add("UNFURNISHED_SQ", UNFURNISHED_SQ);
	   			d.Add("FURNISHED_SQ", FURNISHED_SQ);
	   			d.Add("AN_SEVARANCE_AMT", AN_SEVARANCE_AMT);
                d.Add("AN_SUBS_LOAN_TAX", AN_SUBS_LOAN_TAX);
                d.Add("ID", ID);

                CmnDataManager objCmnDataManager = new CmnDataManager();

                if (ID != null)
                {
                    Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_TAX_UPDATION_ENTRY_ANNUAL_TAX_MANAGER", "Update", d);
                    if (rsltCode.isSuccessful)
                    {
                        var res = Json(rsltCode);
                        return Json("Your request is modify successfully !");
                    }
                }
                else
                {
                    Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_TAX_UPDATION_ENTRY_ANNUAL_TAX_MANAGER", "Save", d);
                    if (rsltCode.isSuccessful)
                    {
                        var res = Json(rsltCode);
                        return Json("Your request is successfully completed !");
                    }
                }
                return Json(null);
            }
            catch (Exception ex)
            {
                return Json("Something went wrong");
            }
            return Json(null);
        }


        #endregion

        public IActionResult WealthTaxAssessment()
        {
            return View();
        }
    }
}
