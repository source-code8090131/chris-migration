﻿using Microsoft.AspNetCore.Mvc;
using NuGet.Common;
using System.Net.Http.Headers;
using System.Text.Json;
using System.Text;
using System.Diagnostics;
using Newtonsoft.Json.Linq;
using CHRIS.ReportModels;
using Azure;

namespace CHRIS.Controllers
{
    public class FinanceReportsController : Controller
    {
        #region FINANCE FOR A PERSON
        public IActionResult FinancePerson()
        {
            string message = "";
            try
            {

            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        [HttpPost]
        public IActionResult FinancePerson(string FP_PNO)
        {
            string message = "";
            try
            {
                if (FP_PNO != null && FP_PNO != "")
                {
                    HttpResponseMessage file = new HttpResponseMessage();
                    FNREP08Model entity = new FNREP08Model();
                    entity.P_NO = FP_PNO;

                    var DomainName = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetSection("CHRIS_REPORTING")["API_URL"];
                    string BASE_URL = DomainName + "api/FinanceReporting/FinancePerson/";
                    using (HttpClient clienFNREP08 = new HttpClient())
                    {
                        using (HttpRequestMessage req310 = new HttpRequestMessage(HttpMethod.Post, BASE_URL))
                        {
                            req310.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                            string dataAsJson = System.Text.Json.JsonSerializer.Serialize(entity,
                            new JsonSerializerOptions() { WriteIndented = false });
                            message += dataAsJson + Environment.NewLine;
                            var content = new StringContent(dataAsJson, Encoding.UTF8, "application/json");
                            req310.Content = content;
                            req310.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                            HttpResponseMessage response = clienFNREP08.SendAsync(req310).Result;
                            int StatusCode = (int)response.StatusCode;
                            string StatusDesc = response.StatusCode.ToString();
                            if (StatusCode == 200)
                            {
                                file = response;
                                return File(file.Content.ReadAsByteArrayAsync().Result, "application/octet-stream", "FNREP08_" + DateTime.Now.ToString("dd_MM_yyyy") + ".pdf");
                            }
                            else
                            {
                                message = "Code : " + StatusCode + " | Description : " + StatusDesc;
                                return View();
                            }
                            //return responsse;
                        }
                    }
                }
                else
                    message = "Enter Personnel Number.";
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        #endregion

        #region FINANCE LIQUIDATION
        public IActionResult FinanceLiquidation()
        {
            string message = "";
            try
            {

            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        [HttpPost]
        public IActionResult FinanceLiquidation(string FL_BRANCH, string FL_SEGMENT, DateTime? FL_FROM_DATE, DateTime? FL_TO_DATE, string FL_FLAG )
        {
            string message = "";
            try
            {
                if (FL_BRANCH != null && FL_SEGMENT != null && FL_FROM_DATE != null && FL_TO_DATE != null && FL_FLAG != null)
                {
                    FNREP09Model entity = new FNREP09Model();
                    entity.Branch = FL_BRANCH;
                    entity.Segment = FL_SEGMENT;
                    entity.FromDate = FL_FROM_DATE;
                    entity.ToDate = FL_TO_DATE;
                    entity.Flag = FL_FLAG;
                    HttpResponseMessage file = new HttpResponseMessage();

                    var DomainName = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetSection("CHRIS_REPORTING")["API_URL"];
                    string BASE_URL = DomainName + "api/FinanceReporting/FinanceLiquidation/";
                    using (HttpClient clienFNREP08 = new HttpClient())
                    {
                        using (HttpRequestMessage req310 = new HttpRequestMessage(HttpMethod.Post, BASE_URL))
                        {
                            req310.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                            string dataAsJson = System.Text.Json.JsonSerializer.Serialize(entity,
                            new JsonSerializerOptions() { WriteIndented = false });
                            message += dataAsJson + Environment.NewLine;
                            var content = new StringContent(dataAsJson, Encoding.UTF8, "application/json");
                            req310.Content = content;
                            req310.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                            HttpResponseMessage response = clienFNREP08.SendAsync(req310).Result;
                            int StatusCode = (int)response.StatusCode;
                            string StatusDesc = response.StatusCode.ToString();
                            if (StatusCode == 200)
                            {
                                file = response;
                                return File(file.Content.ReadAsByteArrayAsync().Result, "application/octet-stream", "FNREP09_" + DateTime.Now.ToString("dd_MM_yyyy") + ".pdf");
                            }
                            else
                            {
                                message = "Code : " + StatusCode + " | Description : " + StatusDesc;
                                return View();
                            }
                            //return responsse;
                        }
                    }
                }
                else
                {
                    if (FL_BRANCH == null)
                        message = "Enter branch code";
                    else if (FL_SEGMENT == null)
                        message = "Enter Segment";
                    else if (FL_SEGMENT == null)
                        message = "Enter branch code";
                    else if (FL_FROM_DATE == null)
                        message = "Select From Date";
                    else if (FL_TO_DATE == null)
                        message = "Select To Date";
                    else if (FL_FLAG == null)
                        message = "Enter Flag";
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        #endregion

        #region FINANCE OUTSTANDING
        public IActionResult FinanceOutstanding()
        {
            string message = "";
            try
            {

            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        [HttpPost]
        public IActionResult FinanceOutstanding(string FO_BRANCH)
        {
            string message = "";
            try
            {
                if (FO_BRANCH != null)
                {
                    FNREP10Model entity = new FNREP10Model();
                    entity.Branch = FO_BRANCH;
                    HttpResponseMessage file = new HttpResponseMessage();

                    var DomainName = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetSection("CHRIS_REPORTING")["API_URL"];
                    string BASE_URL = DomainName + "api/FinanceReporting/FinanceOutstanding/";
                    using (HttpClient clienFNREP08 = new HttpClient())
                    {
                        using (HttpRequestMessage req310 = new HttpRequestMessage(HttpMethod.Post, BASE_URL))
                        {
                            req310.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                            string dataAsJson = System.Text.Json.JsonSerializer.Serialize(entity,
                            new JsonSerializerOptions() { WriteIndented = false });
                            message += dataAsJson + Environment.NewLine;
                            var content = new StringContent(dataAsJson, Encoding.UTF8, "application/json");
                            req310.Content = content;
                            req310.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                            HttpResponseMessage response = clienFNREP08.SendAsync(req310).Result;
                            int StatusCode = (int)response.StatusCode;
                            string StatusDesc = response.StatusCode.ToString();
                            if (StatusCode == 200)
                            {
                                file = response;
                                return File(file.Content.ReadAsByteArrayAsync().Result, "application/octet-stream", "FNREP10_" + DateTime.Now.ToString("dd_MM_yyyy") + ".pdf");
                            }
                            else
                            {
                                message = "Code : " + StatusCode + " | Description : " + StatusDesc;
                                return View();
                            }
                            //return responsse;
                        }
                    }
                }
                else
                    message = "Enter branch code";
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        #endregion

        #region FINANCE BALANCE
        public IActionResult FinanceBalance()
        {
            string message = "";
            try
            {

            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        [HttpPost]
        public IActionResult FinanceBalance(string FB_BRANCH, string FB_SEGMENT, DateTime? FB_DATE)
        {
            string message = "";
            try
            {
                if (FB_BRANCH != null && FB_SEGMENT != null && FB_DATE != null )
                {
                    FNREP01Model entity = new FNREP01Model();
                    entity.Branch = FB_BRANCH;
                    entity.Date = FB_DATE;
                    entity.Segment = FB_SEGMENT;
                    HttpResponseMessage file = new HttpResponseMessage();

                    var DomainName = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetSection("CHRIS_REPORTING")["API_URL"];
                    string BASE_URL = DomainName + "api/FinanceReporting/FinanceBalance/";
                    using (HttpClient clienFNREP08 = new HttpClient())
                    {
                        using (HttpRequestMessage req310 = new HttpRequestMessage(HttpMethod.Post, BASE_URL))
                        {
                            req310.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                            string dataAsJson = System.Text.Json.JsonSerializer.Serialize(entity,
                            new JsonSerializerOptions() { WriteIndented = false });
                            message += dataAsJson + Environment.NewLine;
                            var content = new StringContent(dataAsJson, Encoding.UTF8, "application/json");
                            req310.Content = content;
                            req310.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                            HttpResponseMessage response = clienFNREP08.SendAsync(req310).Result;
                            int StatusCode = (int)response.StatusCode;
                            string StatusDesc = response.StatusCode.ToString();
                            if (StatusCode == 200)
                            {
                                file = response;
                                return File(file.Content.ReadAsByteArrayAsync().Result, "application/octet-stream", "FNREP01_" + DateTime.Now.ToString("dd_MM_yyyy") + ".pdf");
                            }
                            else
                            {
                                message = "Code : " + StatusCode + " | Description : " + StatusDesc;
                                return View();
                            }
                            //return responsse;
                        }
                    }
                }
                else
                    message = "Enter branch code";
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        #endregion
        
        #region FINANCE BALANCE
        public IActionResult MonthlyFinance()
        {
            string message = "";
            try
            {

            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        [HttpPost]
        public IActionResult MonthlyFinance(string MF_BRANCH, string MF_SEGMENT)
        {
            string message = "";
            try
            {
                if (MF_BRANCH != null && MF_SEGMENT != null)
                {
                    FNREP02Model entity = new FNREP02Model();
                    entity.Branch = MF_BRANCH;
                    entity.Segment = MF_SEGMENT;
                    HttpResponseMessage file = new HttpResponseMessage();

                    var DomainName = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetSection("CHRIS_REPORTING")["API_URL"];
                    string BASE_URL = DomainName + "api/FinanceReporting/MonthlyFinance/";
                    using (HttpClient clienFNREP08 = new HttpClient())
                    {
                        using (HttpRequestMessage req310 = new HttpRequestMessage(HttpMethod.Post, BASE_URL))
                        {
                            req310.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                            string dataAsJson = System.Text.Json.JsonSerializer.Serialize(entity,
                            new JsonSerializerOptions() { WriteIndented = false });
                            message += dataAsJson + Environment.NewLine;
                            var content = new StringContent(dataAsJson, Encoding.UTF8, "application/json");
                            req310.Content = content;
                            req310.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                            HttpResponseMessage response = clienFNREP08.SendAsync(req310).Result;
                            int StatusCode = (int)response.StatusCode;
                            string StatusDesc = response.StatusCode.ToString();
                            if (StatusCode == 200)
                            {
                                file = response;
                                return File(file.Content.ReadAsByteArrayAsync().Result, "application/octet-stream", "FNREP02_" + DateTime.Now.ToString("dd_MM_yyyy") + ".pdf");
                            }
                            else
                            {
                                message = "Code : " + StatusCode + " | Description : " + StatusDesc;
                                return View();
                            }
                            //return responsse;
                        }
                    }
                }
                else
                    message = "Enter branch code";
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        #endregion
        
        #region FINANCE LEDGER
        public IActionResult FinanceLedger()
        {
            string message = "";
            try
            {

            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        [HttpPost]
        public IActionResult FinanceLedger(string FL_FINANCE_NO)
        {
            string message = "";
            try
            {
                if (FL_FINANCE_NO != null)
                {
                    FNREP03Model entity = new FNREP03Model();
                    entity.FiananceNo = FL_FINANCE_NO;
                    HttpResponseMessage file = new HttpResponseMessage();

                    var DomainName = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetSection("CHRIS_REPORTING")["API_URL"];
                    string BASE_URL = DomainName + "api/FinanceReporting/FinanceLedger/";
                    using (HttpClient clienFNREP08 = new HttpClient())
                    {
                        using (HttpRequestMessage req310 = new HttpRequestMessage(HttpMethod.Post, BASE_URL))
                        {
                            req310.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                            string dataAsJson = System.Text.Json.JsonSerializer.Serialize(entity,
                            new JsonSerializerOptions() { WriteIndented = false });
                            message += dataAsJson + Environment.NewLine;
                            var content = new StringContent(dataAsJson, Encoding.UTF8, "application/json");
                            req310.Content = content;
                            req310.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                            HttpResponseMessage response = clienFNREP08.SendAsync(req310).Result;
                            int StatusCode = (int)response.StatusCode;
                            string StatusDesc = response.StatusCode.ToString();
                            if (StatusCode == 200)
                            {
                                file = response;
                                return File(file.Content.ReadAsByteArrayAsync().Result, "application/octet-stream", "FNREP03_" + DateTime.Now.ToString("dd_MM_yyyy") + ".pdf");
                            }
                            else
                            {
                                message = "Code : " + StatusCode + " | Description : " + StatusDesc;
                                return View();
                            }
                            //return responsse;
                        }
                    }
                }
                else
                    message = "Enter Finance Number";
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        #endregion

        #region MCO REPORT
        public IActionResult MCOReport()
        {
            string message = "";
            try
            {

            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        [HttpPost]
        public IActionResult MCOReport(string MCO_BALANCE, string MCO_SEGMENT, string MCO_BRANCH)
        {
            string message = "";
            try
            {
                if (MCO_BRANCH != null && MCO_SEGMENT != null)
                {
                    MCOREP2Model entity = new MCOREP2Model();
                    if (MCO_BALANCE != null)
                        entity.Balance = Convert.ToDecimal(MCO_BALANCE);
                    else
                        entity.Balance = null;
                    entity.Branch = MCO_BRANCH;
                    entity.Segment = MCO_SEGMENT;
                    HttpResponseMessage file = new HttpResponseMessage();

                    var DomainName = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetSection("CHRIS_REPORTING")["API_URL"];
                    string BASE_URL = DomainName + "api/FinanceReporting/MCOReport/";
                    using (HttpClient clienFNREP08 = new HttpClient())
                    {
                        using (HttpRequestMessage req310 = new HttpRequestMessage(HttpMethod.Post, BASE_URL))
                        {
                            req310.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                            string dataAsJson = System.Text.Json.JsonSerializer.Serialize(entity,
                            new JsonSerializerOptions() { WriteIndented = false });
                            message += dataAsJson + Environment.NewLine;
                            var content = new StringContent(dataAsJson, Encoding.UTF8, "application/json");
                            req310.Content = content;
                            req310.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                            HttpResponseMessage response = clienFNREP08.SendAsync(req310).Result;
                            int StatusCode = (int)response.StatusCode;
                            string StatusDesc = response.StatusCode.ToString();
                            if (StatusCode == 200)
                            {
                                file = response;
                                return File(file.Content.ReadAsByteArrayAsync().Result, "application/octet-stream", "MCOREP2_" + DateTime.Now.ToString("dd_MM_yyyy") + ".pdf");
                            }
                            else
                            {
                                message = "Code : " + StatusCode + " | Description : " + StatusDesc;
                                return View();
                            }
                            //return responsse;
                        }
                    }
                }
                else
                    message = "Enter Finance Number";
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        #endregion
        public IActionResult FinanceMaturity()
        {
            return View();
        }
        public IActionResult CategoryWiseOutstandingBalanceLocal()
        {
            return View();
        }
        public IActionResult CategoryWiseOutstandingBalanceOut()
        {
            return View();
        }
        public IActionResult DesigWiseISOSOthers()
        {
            return View();
        }
        public IActionResult DesigWiseOS()
        {
            return View();
        }
        public IActionResult DisbursementOuts()
        {
            return View();
        }
        public IActionResult ExciseLevelPaid()
        {
            return View();
        }
        public IActionResult FinanceMaturity1()
        {
            return View();
        }
        public IActionResult FinanceOSForISAndOthers()
        {
            return View();
        }
        public IActionResult FireInsurance()
        {
            return View();
        }
        public IActionResult FNHistoryLedger()
        {
            return View();
        }
        public IActionResult FNOutstandingASR()
        {
            return View();
        }
        public IActionResult FNREP18A()
        {
            return View();
        }
        public IActionResult FNREP18B()
        {
            return View();
        }
        public IActionResult InsuranceRequired()
        {
            return View();
        }
        public IActionResult InsuranceRequiredReport()
        {
            return View();
        }
        public IActionResult Loan_Asim()
        {
            return View();
        }
        public IActionResult LoanDisbursement()
        {
            return View();
        }
        public IActionResult LoanStatusReport()
        {
            return View();
        }
        public IActionResult MarkupAmountCollected()
        {
            return View();
        }
        public IActionResult MCOProcess()
        {
            return View();
        }
        public IActionResult NewlyFinanceBooking()
        {
            return View();
        }
        public IActionResult SegmentWiseFinance()
        {
            return View();
        }
        public IActionResult SizeWiseBalance()
        {
            return View();
        }
        public IActionResult VehicleInsurancePolicy()
        {
            return View();
        }
        public IActionResult MCO_REP1()
        {
            return View();
        }
        public IActionResult MCO_REP2()
        {
            return View();
        }
       
    }
}
