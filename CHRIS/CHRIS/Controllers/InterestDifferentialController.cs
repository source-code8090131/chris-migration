﻿using CHRIS.Common;
using CHRIS.Models;
using CHRIS.Services;
using DocumentFormat.OpenXml.Spreadsheet;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Data;

namespace CHRIS.Controllers
{
    public class InterestDifferentialController : Controller
    {
       
        #region CountryCode

        public ActionResult CountryCode()
        {
            var user = HttpContext.Session.GetString("USER_ID");

            var CountryCode = CountryCodeService.getInstance().CountryCode();
            return View(CountryCode);
        }
        public JsonResult btn_Modal_PR_COUNTRY_CODE()
        {
            Dictionary<string, object> d = new Dictionary<string, object>();

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_PR_COUNTRY_MANAGER", "CountryCodeLov", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];


                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }      


        [HttpPost]
        public JsonResult UpsertCountryCode(string PR_COUNTRY_CODE, string PR_COUNTRY_DESC, string PR_COUNTRY_AC, string PR_CITIMAIL, string PR_MAKER_NAME, DateTime? PR_MAKER_DATE, string PR_MAKER_TIME, string PR_MAKER_LOC, string PR_MAKER_TERM, string PR_AUTH_NAME, DateTime? PR_AUTH_DATE, string PR_AUTH_TIME, string PR_AUTH_LOC, string PR_AUTH_TERM, string PR_AUTH_FLAG, string PR_STATUS, int? ID)
        {
            var user = HttpContext.Session.GetString("USER_ID");
            var datetime = DateTime.Parse(DateTime.Now.ToShortDateString());
            var TIME = DateTime.Now.TimeOfDay.Hours + "" + DateTime.Now.TimeOfDay.Minutes + "" + DateTime.Now.TimeOfDay.Seconds;

            try
            {
                Dictionary<string, object> d = new Dictionary<string, object>();

                     d.Add("PR_COUNTRY_CODE", PR_COUNTRY_CODE);
                     d.Add("PR_COUNTRY_DESC", PR_COUNTRY_DESC);
                     d.Add("PR_COUNTRY_AC", PR_COUNTRY_AC);
                     d.Add("PR_CITIMAIL", PR_CITIMAIL);
                     d.Add("PR_MAKER_NAME", user);
                     d.Add("PR_MAKER_DATE", datetime);
                     d.Add("PR_MAKER_TIME", TIME);
                     d.Add("PR_MAKER_LOC", PR_MAKER_LOC);
                     d.Add("PR_MAKER_TERM", PR_MAKER_TERM);
                     d.Add("PR_AUTH_NAME", PR_AUTH_NAME);
                     d.Add("PR_AUTH_DATE", PR_AUTH_DATE);
                     d.Add("PR_AUTH_TIME", PR_AUTH_TIME);
                     d.Add("PR_AUTH_LOC", PR_AUTH_LOC);
                     d.Add("PR_AUTH_TERM", PR_AUTH_TERM);
                     d.Add("PR_AUTH_FLAG", PR_AUTH_FLAG);
                     d.Add("PR_STATUS", PR_STATUS);
                     d.Add("ID", ID);

                CmnDataManager objCmnDataManager = new CmnDataManager();
                if (ID > 0)
                {

                    Result rsltCode = objCmnDataManager.GetData("CHRIS_PR_COUNTRY_MANAGER", "Update", d);
                    if (rsltCode.isSuccessful)
                    {

                        var res = Json(rsltCode);

                        return Json(res);
                    }
                }
                else
                {
                    Result rsltCode = objCmnDataManager.GetData("CHRIS_PR_COUNTRY_MANAGER", "Save", d);
                    if (rsltCode.isSuccessful)
                    {
                        var res = Json(rsltCode);

                        return Json(res);

                    }
                }

                return Json(null);
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public ViewResponseModel DeleteCountryCode(CountryCodeModel model)
        {
            return CountryCodeService.getInstance().DeleteCountryCode(model);
        }

        #endregion

        #region CountryCodeAuthorization

        public IActionResult CountryCodeAuthorization()
        {
            return View();
        }

        public ActionResult tbl_CountryCodeAuthorization()
        {
            //var user = HttpContext.Session.GetString("USER_ID");

            Dictionary<string, object> d = new Dictionary<string, object>();

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_PR_COUNTRY_AUTHORIZATION_MANAGER", "List", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];


                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }
       


        [HttpPost]
        public JsonResult UpsertCountryCodeAuthorization(string PR_COUNTRY_CODE, string PR_COUNTRY_DESC, string PR_COUNTRY_AC, string PR_CITIMAIL, string PR_MAKER_NAME, DateTime? PR_MAKER_DATE, string PR_MAKER_TIME, string PR_MAKER_LOC, string PR_MAKER_TERM, string PR_AUTH_NAME, DateTime? PR_AUTH_DATE, string PR_AUTH_TIME, string PR_AUTH_LOC, string PR_AUTH_TERM, string PR_AUTH_FLAG, string PR_STATUS, int? ID)
        {
            var user = HttpContext.Session.GetString("USER_ID");
            var datetime = DateTime.Parse(DateTime.Now.ToShortDateString());
            var TIME = DateTime.Now.TimeOfDay.Hours + "" + DateTime.Now.TimeOfDay.Minutes + "" + DateTime.Now.TimeOfDay.Seconds;
                        
            try
            {
                Dictionary<string, object> d = new Dictionary<string, object>();

                d.Add("PR_COUNTRY_CODE", PR_COUNTRY_CODE);
                d.Add("PR_COUNTRY_DESC", PR_COUNTRY_DESC);
                d.Add("PR_COUNTRY_AC", PR_COUNTRY_AC);
                d.Add("PR_CITIMAIL", PR_CITIMAIL);
                d.Add("PR_MAKER_NAME", PR_MAKER_NAME);
                d.Add("PR_MAKER_DATE", PR_MAKER_DATE);
                d.Add("PR_MAKER_TIME", PR_MAKER_TIME);
                d.Add("PR_MAKER_LOC", PR_MAKER_LOC);
                d.Add("PR_MAKER_TERM", PR_MAKER_TERM);
                if (PR_AUTH_FLAG == "A")
                {
                    PR_AUTH_NAME = user;
                    PR_AUTH_DATE = datetime;
                    PR_AUTH_TIME = TIME;
                }
                d.Add("PR_AUTH_NAME", PR_AUTH_NAME);
                d.Add("PR_AUTH_DATE", PR_AUTH_DATE);
                d.Add("PR_AUTH_TIME", PR_AUTH_TIME);
                d.Add("PR_AUTH_LOC", PR_AUTH_LOC);
                d.Add("PR_AUTH_TERM", PR_AUTH_TERM);
                d.Add("PR_AUTH_FLAG", PR_AUTH_FLAG);
                d.Add("PR_STATUS", PR_STATUS);
                d.Add("ID", ID);

                CmnDataManager objCmnDataManager = new CmnDataManager();
                if (user != PR_MAKER_NAME)
                {
                    if (ID > 0)
                    {
                        Result rsltCode = objCmnDataManager.GetData("CHRIS_PR_COUNTRY_AUTHORIZATION_MANAGER", "Update", d);
                        if (rsltCode.isSuccessful)
                        {
                            var res = Json(rsltCode);

                            return Json("Your request is successfully completed !");
                        }
                    }
                    else
                    {
                        Result rsltCode = objCmnDataManager.GetData("CHRIS_PR_COUNTRY_AUTHORIZATION_MANAGER", "Save", d);
                        if (rsltCode.isSuccessful)
                        {
                            var res = Json(rsltCode);

                            return Json("Your request is successfully completed !");
                        }
                    }
                }
                else
                {
                    return Json("Record Not Authorized ...Because Maker & Authorizer is Same...");
                }

                return Json(null);
            }
            catch (Exception ex)
            {
                return Json(null);
            }
        }

        public ViewResponseModel DeleteCountryCodeAuthorization(CountryCodeAuthorizationModel model)
        {
            return CountryCodeAuthorizationService.getInstance().DeleteCountryCodeAuthorization(model);
        }

        #endregion

        #region DollarRateEntry

        public IActionResult DollarRateAuthorization()
        {
            return View();
        }
        public ActionResult tbl_DollarRateAuthorization()
        {
            Dictionary<string, object> d = new Dictionary<string, object>();

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_FN_USRATE_MANAGER", "List", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];

                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);

        }

        [HttpPost]
        public JsonResult UpsertDollarRateAuthorization(DateTime? FN_DATE, DateTime? FN_TRN_DATE, decimal? FN_RATE, string FN_MAKER_NAME, DateTime? FN_MAKER_DATE, string FN_MAKER_TIME, string FN_MAKER_LOC, string FN_MAKER_TERM, string FN_AUTH_NAME, DateTime? FN_AUTH_DATE, string FN_AUTH_TIME, string FN_AUTH_LOC, string FN_AUTH_TERM, string FN_AUTH_FLAG, string FN_STATUS, int? ID)
        {
            var user = HttpContext.Session.GetString("USER_ID");
            var datetime = DateTime.Parse(DateTime.Now.ToShortDateString());
            var TIME = DateTime.Now.TimeOfDay.Hours + "" + DateTime.Now.TimeOfDay.Minutes + "" + DateTime.Now.TimeOfDay.Seconds;

            try
            {
                Dictionary<string, object> d = new Dictionary<string, object>();

                d.Add("FN_DATE", FN_DATE);
                d.Add("FN_TRN_DATE", FN_TRN_DATE);
                d.Add("FN_RATE", FN_RATE);
                d.Add("FN_MAKER_NAME", FN_MAKER_NAME);
                d.Add("FN_MAKER_DATE", FN_MAKER_DATE);
                d.Add("FN_MAKER_TIME", FN_MAKER_TIME);
                d.Add("FN_MAKER_LOC", FN_MAKER_LOC);
                d.Add("FN_MAKER_TERM", FN_MAKER_TERM);
                if (FN_AUTH_FLAG == "A")
                {
                    FN_AUTH_NAME = user;
                    FN_AUTH_DATE = datetime;
                    FN_AUTH_TIME = TIME;
                }
                d.Add("FN_AUTH_NAME", FN_AUTH_NAME);
                d.Add("FN_AUTH_DATE", FN_AUTH_DATE);
                d.Add("FN_AUTH_TIME", FN_AUTH_TIME);
                d.Add("FN_AUTH_LOC", FN_AUTH_LOC);
                d.Add("FN_AUTH_TERM", FN_AUTH_TERM);
                d.Add("FN_AUTH_FLAG", FN_AUTH_FLAG);
                d.Add("FN_STATUS", FN_STATUS);
                d.Add("ID", ID);

                CmnDataManager objCmnDataManager = new CmnDataManager();
                if (user != FN_MAKER_NAME)
                {
                    if (ID > 0)
                    {
                        Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_FN_USRATE_AUTHORIZATION_MANAGER", "Update", d);
                        if (rsltCode.isSuccessful)
                        {
                            var res = Json(rsltCode);
                            return Json("Your request is successfully completed !");
                        }
                    }
                    else
                    {
                        Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_FN_USRATE_AUTHORIZATION_MANAGER", "Save", d);
                        if (rsltCode.isSuccessful)
                        {
                            var res = Json(rsltCode);
                            return Json("Your request is successfully completed !");
                        }
                    }
                }
            
                else
                {
                    return Json("Record Not Authorized ...Because Maker & Authorizer is Same...");
                }
            
                return Json(null);
            }
            catch (Exception ex)
            {
                return Json("Something went wrong");
            }
        }

        public ViewResponseModel DeleteDollarRateAuthorization(DollarRateAuthorizationModel model)
        {
            return DollarRateAuthorizationService.getInstance().DeleteDollarRateAuthorization(model);
        }

        #endregion

        #region DollarRateEntry

        public IActionResult DollarRateEntry()
        {
            return View();
        }
        public ActionResult tbl_DollarRateEntry()
        {  
            Dictionary<string, object> d = new Dictionary<string, object>();

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_FN_USRATE_MANAGER", "List", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];

                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);

        }
       
        [HttpPost]
        public JsonResult UpsertDollarRateEntry(DateTime? FN_DATE, DateTime? FN_TRN_DATE, decimal? FN_RATE, string FN_MAKER_NAME, DateTime? FN_MAKER_DATE, string FN_MAKER_TIME, string FN_MAKER_LOC, string FN_MAKER_TERM, string FN_AUTH_NAME, DateTime? FN_AUTH_DATE, string FN_AUTH_TIME, string FN_AUTH_LOC, string FN_AUTH_TERM, string FN_AUTH_FLAG, string FN_STATUS, int? ID)
        {
            var user = HttpContext.Session.GetString("USER_ID");
            var datetime = DateTime.Parse(DateTime.Now.ToShortDateString());
            var TIME = DateTime.Now.TimeOfDay.Hours + "" + DateTime.Now.TimeOfDay.Minutes + "" + DateTime.Now.TimeOfDay.Seconds;

            try
            {
                Dictionary<string, object> d = new Dictionary<string, object>();

                d.Add("FN_DATE", FN_DATE);     
                d.Add("FN_TRN_DATE", FN_TRN_DATE);    
                d.Add("FN_RATE" , FN_RATE);
                d.Add("FN_MAKER_NAME" , user);  
                d.Add("FN_MAKER_DATE", datetime);    
                d.Add("FN_MAKER_TIME" , TIME);   
                d.Add("FN_MAKER_LOC" , FN_MAKER_LOC);   
                d.Add("FN_MAKER_TERM" , FN_MAKER_TERM);                
                d.Add("FN_AUTH_NAME" , FN_AUTH_NAME);  
                d.Add("FN_AUTH_DATE", FN_AUTH_DATE);    
                d.Add("FN_AUTH_TIME" , FN_AUTH_TIME);
                d.Add("FN_AUTH_LOC", FN_AUTH_LOC);
                d.Add("FN_AUTH_TERM" , FN_AUTH_TERM);
                d.Add("FN_AUTH_FLAG" , FN_AUTH_FLAG);
                d.Add("FN_STATUS", FN_STATUS);
                d.Add("ID", ID);

                CmnDataManager objCmnDataManager = new CmnDataManager();
                if (ID > 0)
                {
                    Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_FN_USRATE_MANAGER", "Update", d);
                    if (rsltCode.isSuccessful)
                    {
                        var res = Json(rsltCode);
                        return Json("Your request is successfully completed !");
                    }
                }
                else
                {
                    Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_FN_USRATE_MANAGER", "Save", d);
                    if (rsltCode.isSuccessful)
                    {
                        var res = Json(rsltCode);
                        return Json("Your request is successfully completed !");
                    }
                }
                return Json(null);
            }
            catch (Exception ex)
            {
                return Json("Something went wrong");
            }
        }

        public ViewResponseModel DeleteDollarRateEntry(DollarRateEntryModel model)
        {
            return DollarRateEntryService.getInstance().DeleteDollarRateEntry(model);
        }

        #endregion
        public IActionResult FNREP31()
        {
            return View();
        }
        public IActionResult FNREP32()
        {
            return View();
        }

        #region InterestDifferentailProcess

        public IActionResult InterestDifferentailProcess()
        {
            return View();
        }

        #region Data Members

        Result rsltCode;
        CmnDataManager cmnDM = new CmnDataManager();
        public int MARKUP_RATE_DAYS = -1;
        #endregion


        [HttpPost]
        public JsonResult StartProcess(DateTime? SLFromDate, DateTime? SLToDate, DateTime? SLFromDate1, DateTime? SLToDate1,decimal? txtPersonal, string txtPer, decimal? txtRate)
        {
            Dictionary<string, object> param = new Dictionary<string, object>();
            Dictionary<string, object> paramD = new Dictionary<string, object>();
            Dictionary<string, object> paramBook = new Dictionary<string, object>();
            Dictionary<string, object> param1 = new Dictionary<string, object>();
            param1.Add("W_PNumber", txtPersonal);

            DataTable dtPrNos = this.GetData("CHRIS_SP_fin_Differential_INPUTS_PROC", "", param1);

            paramD.Add("W_PNUMBER", txtPersonal);
            paramD.Add("FromDate2", (SLFromDate1 == null ? null : Convert.ToDateTime(SLFromDate1).ToShortDateString()));
            paramD.Add("ToDate2", (SLToDate1 == null ? null : Convert.ToDateTime(SLToDate1).ToShortDateString()));

            cmnDM.Execute("CHRIS_SP_FIN_DIFFERENTIAL_DELETE_LEDGER_PROC", "", paramD);


            if (dtPrNos != null)
            {
                if (dtPrNos.Rows.Count > 0)
                {
                    foreach (DataRow dtPr in dtPrNos.Rows)
                    {
                        txtPer = dtPr.ItemArray[0].ToString();
                        //Application.DoEvents();
                        param.Add("W_PNumber", dtPr.ItemArray[0].ToString());
                        param.Add("FromDate2", (SLFromDate1 == null ? null : Convert.ToDateTime(SLFromDate1).ToShortDateString()));
                        param.Add("ToDate2", (SLToDate1 == null ? null : Convert.ToDateTime(SLToDate1).ToShortDateString()));
                        param.Add("FromDate", (SLFromDate == null ? null : Convert.ToDateTime(SLFromDate).ToShortDateString()));
                        param.Add("ToDate", (SLToDate == null ? null : Convert.ToDateTime(SLToDate).ToShortDateString()));
                        param.Add("strRate", txtRate);

                        Result rsltCode = cmnDM.Execute("CHRIS_SP_fin_Differential_CALCULATION_PROC", "", param);
                        

                        param.Clear();
                    }
                    if (rsltCode.isSuccessful)
                    {
                        var res = Json(rsltCode);

                        return Json("PROCESS HAS BEEN COMPLETED");
                    }
                }
            }

            return Json("PROCESS HAS BEEN COMPLETED");

           
        }

        private DataTable GetData(string sp, string actionType, Dictionary<string, object> param)
        {
            DataTable dt = null;

            Result rsltCode1;
            CmnDataManager cmnDM1 = new CmnDataManager();
            rsltCode1 = cmnDM1.GetData(sp, actionType, param);

            if (rsltCode1.isSuccessful)
            {
                if (rsltCode1.dstResult.Tables.Count > 0 && rsltCode1.dstResult.Tables[0].Rows.Count > 0)
                {
                    dt = rsltCode1.dstResult.Tables[0];
                }
            }
            return dt;

        }

       

        #endregion

        #region LSOtherAuthorization
        public IActionResult LSOtherAuthorization()
        {
            return View();
        }
        public IActionResult tbl_LSOtherAuthorization()
        {
            Dictionary<string, object> d = new Dictionary<string, object>();

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_FN_INT_DIFF_MANAGER", "LOVPERSONNELAUTH", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];

                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        public IActionResult btn_Modal_PR_P_NO11()
        {
            Dictionary<string, object> d = new Dictionary<string, object>();

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_FN_INT_DIFF_AUTH_MANAGER", "LOVPersonnel", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];


                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

      
        [HttpPost]
        public JsonResult UpsertLSOtherAuthorization(decimal? PR_P_NO, string Name, string FN_COUNTRY_CODE, string FN_Country_Desc, string FN_SEGMENT, decimal? FN_RENTAL_GUARANT, string FN_MAKER_NAME, DateTime? FN_MAKER_DATE, string FN_MAKER_TIME, string FN_MAKER_LOC, string FN_MAKER_TERM, string FN_AUTH_NAME, DateTime? FN_AUTH_DATE, string FN_AUTH_TIME, string FN_AUTH_LOC, string FN_AUTH_TERM, string FN_AUTH_FLAG, string FN_STATUS, int? ID)
        {
            var user = HttpContext.Session.GetString("USER_ID");
            var datetime = DateTime.Parse(DateTime.Now.ToShortDateString());
            var TIME = DateTime.Now.TimeOfDay.Hours + "" + DateTime.Now.TimeOfDay.Minutes + "" + DateTime.Now.TimeOfDay.Seconds;

            try
            {
                Dictionary<string, object> d = new Dictionary<string, object>();

                d.Add("PR_P_NO", PR_P_NO);
                d.Add("Name", Name);
                d.Add("FN_COUNTRY_CODE", FN_COUNTRY_CODE);
                d.Add("FN_Country_Desc", FN_Country_Desc);
                d.Add("FN_SEGMENT", FN_SEGMENT);
                d.Add("FN_RENTAL_GUARANT ", FN_RENTAL_GUARANT);
                d.Add("FN_MAKER_NAME", FN_MAKER_NAME);
                d.Add("FN_MAKER_DATE", FN_MAKER_DATE);
                d.Add("FN_MAKER_TIME", FN_MAKER_TIME);
                d.Add("FN_MAKER_LOC", FN_MAKER_LOC);
                d.Add("FN_MAKER_TERM", FN_MAKER_TERM);

                if (FN_AUTH_FLAG == "A")
                {
                    FN_AUTH_NAME = user;
                    FN_AUTH_DATE = datetime;
                    FN_AUTH_TIME = TIME;
                }

                d.Add("FN_AUTH_NAME", FN_AUTH_NAME);
                d.Add("FN_AUTH_DATE", FN_AUTH_DATE);
                d.Add("FN_AUTH_TIME", FN_AUTH_TIME);
                d.Add("FN_AUTH_LOC", FN_AUTH_LOC);
                d.Add("FN_AUTH_TERM", FN_AUTH_TERM);
                d.Add("FN_AUTH_FLAG", FN_AUTH_FLAG);
                d.Add("FN_STATUS", FN_STATUS);
                d.Add("ID", ID);

                CmnDataManager objCmnDataManager = new CmnDataManager();
                if (FN_AUTH_FLAG == "A")
                {
                    if (user != FN_MAKER_NAME)
                    {
                        if (ID > 0)
                        {
                            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_FN_INT_DIFF_AUTH_MANAGER", "Update", d);
                            if (rsltCode.isSuccessful)
                            {
                                var res = Json(rsltCode);

                                return Json("Your request is successfully completed !");
                            }
                        }
                        else
                        {
                            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_FN_INT_DIFF_AUTH_MANAGER", "Save", d);
                            if (rsltCode.isSuccessful)
                            {
                                var res = Json(rsltCode);

                                return Json("Your request is successfully completed !");
                            }
                        }
                    }
                    else
                    {
                        return Json("User Not Authrized To Change This Record..");
                    }
                }
                else
                {
                    return Json("Record Not Authrized..");
                }
               

                return Json(null);
            }
            catch (Exception ex)
            {
                return Json(null);
            }
        }

        public ViewResponseModel DeleteLSOtherEntry1(LSOtherEntryModel model)
        {
            return LSOtherEntryService.getInstance().DeleteLSOtherEntry(model);
        }

        #endregion

        #region LSOtherEntry

        public IActionResult LSOtherEntry()
        {
            var LSOtherEntry = LSOtherEntryService.getInstance().LSOtherEntry();
            return View(LSOtherEntry);
        }

        public IActionResult btn_Modal_PR_P_NO1()
        {
            Dictionary<string, object> d = new Dictionary<string, object>();

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_FN_INT_DIFF_MANAGER", "LOVPersonnel", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];


                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        public IActionResult btn_Modal_FN_COUNTRY_CODE()
        {
            Dictionary<string, object> d = new Dictionary<string, object>();

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_FN_INT_DIFF_MANAGER", "LOVCountry", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];


                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        [HttpPost]
        public JsonResult UpsertLSOtherEntry(decimal? PR_P_NO, string Name, string FN_COUNTRY_CODE, string FN_Country_Desc, string FN_SEGMENT, decimal? FN_RENTAL_GUARANT, string FN_MAKER_NAME, DateTime? FN_MAKER_DATE, string FN_MAKER_TIME, string FN_MAKER_LOC, string FN_MAKER_TERM, string FN_AUTH_NAME, DateTime? FN_AUTH_DATE, string FN_AUTH_TIME, string FN_AUTH_LOC, string FN_AUTH_TERM, string FN_AUTH_FLAG, string FN_STATUS, int? ID)
        {
            var user = HttpContext.Session.GetString("USER_ID");
            var datetime = DateTime.Parse(DateTime.Now.ToShortDateString());
            var TIME = DateTime.Now.TimeOfDay.Hours + "" + DateTime.Now.TimeOfDay.Minutes + "" + DateTime.Now.TimeOfDay.Seconds;

            try
            {
                Dictionary<string, object> d = new Dictionary<string, object>();

                d.Add("PR_P_NO", PR_P_NO);       
		        d.Add("Name", Name);
	   	        d.Add("FN_COUNTRY_CODE", FN_COUNTRY_CODE);
		        d.Add("FN_Country_Desc", FN_Country_Desc);
	   	        d.Add("FN_SEGMENT", FN_SEGMENT);
	   	        d.Add("FN_RENTAL_GUARANT ", FN_RENTAL_GUARANT);
	   	        d.Add("FN_MAKER_NAME", user);
	   	        d.Add("FN_MAKER_DATE", datetime);
	   	        d.Add("FN_MAKER_TIME", TIME);
	   	        d.Add("FN_MAKER_LOC", FN_MAKER_LOC);
	   	        d.Add("FN_MAKER_TERM", FN_MAKER_TERM);
	   	        d.Add("FN_AUTH_NAME", FN_AUTH_NAME);
	   	        d.Add("FN_AUTH_DATE", FN_AUTH_DATE);
	   	        d.Add("FN_AUTH_TIME", FN_AUTH_TIME);
	   	        d.Add("FN_AUTH_LOC", FN_AUTH_LOC);
	   	        d.Add("FN_AUTH_TERM", FN_AUTH_TERM);
	   	        d.Add("FN_AUTH_FLAG", FN_AUTH_FLAG);
                d.Add("FN_STATUS", FN_STATUS);
                d.Add("ID", ID);

                CmnDataManager objCmnDataManager = new CmnDataManager();
                if (user != FN_AUTH_NAME)
                {
                    if (ID > 0)
                    {
                        Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_FN_INT_DIFF_MANAGER", "Update", d);
                        if (rsltCode.isSuccessful)
                        {
                            var res = Json(rsltCode);

                            return Json("Your request is successfully completed !");
                        }
                    }
                    else
                    {
                        Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_FN_INT_DIFF_MANAGER", "Save", d);
                        if (rsltCode.isSuccessful)
                        {
                            var res = Json(rsltCode);

                            return Json("Your request is successfully completed !");
                        }
                    }
                }
                else
                {
                    return Json("User Not Authrized To Change This Record..");
                }

                return Json(null);
            }
            catch (Exception ex)
            {
                return Json(null);
            }
        }

        public ViewResponseModel DeleteLSOtherEntry(LSOtherEntryModel model)
        {
            return LSOtherEntryService.getInstance().DeleteLSOtherEntry(model);
        }

        #endregion

    }
}
