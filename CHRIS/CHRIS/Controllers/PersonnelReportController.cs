﻿using CHRIS.ReportModels;
using Microsoft.AspNetCore.Mvc;
using System.Net.Http.Headers;
using System.Text.Json;
using System.Text;
using CHRIS.Common;
using CHRIS.Services;
using Newtonsoft.Json;
using System.Data;
using CHRIS.Data;

namespace CHRIS.Controllers
{
    public class PersonnelReportController : Controller
    {
        #region PERSONNEL REPORT FOR ALL
        public IActionResult PersonnelReportForAll()
        {
            string message = "";
            try
            {

            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        [HttpPost]
        public IActionResult PersonnelReportForAll(string PRFL_BRANCH, string PRFL_SEGMENT, string PRFL_DESIG, string PRFL_FROM_PNO, string PRFL_END_PNO, DateTime? PRFL_FROM_DATE, DateTime? PRFL_TO_DATE)
        {
            string message = "";
            try
            {
                HttpResponseMessage file = new HttpResponseMessage();
                PRREP46Model entity = new PRREP46Model();
                entity.Branch = PRFL_BRANCH;
                entity.Segment = PRFL_SEGMENT;
                entity.Desig = PRFL_DESIG;
                entity.FromPrNo = PRFL_FROM_PNO;
                entity.EndPrNo = PRFL_END_PNO;
                if (PRFL_FROM_DATE != null)
                    entity.From_Date = PRFL_FROM_DATE;
                else
                    entity.From_Date = null;
                if (PRFL_TO_DATE != null)
                    entity.To_Date = PRFL_TO_DATE;
                else
                    entity.To_Date = null;
                var DomainName = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetSection("CHRIS_REPORTING")["API_URL"];
                string BASE_URL = DomainName + "api/PersonnelReporting/PersonnelReportForAll/";
                using (HttpClient clienFNREP08 = new HttpClient())
                {
                    using (HttpRequestMessage req310 = new HttpRequestMessage(HttpMethod.Post, BASE_URL))
                    {
                        req310.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        string dataAsJson = System.Text.Json.JsonSerializer.Serialize(entity,
                        new JsonSerializerOptions() { WriteIndented = false });
                        message += dataAsJson + Environment.NewLine;
                        var content = new StringContent(dataAsJson, Encoding.UTF8, "application/json");
                        req310.Content = content;
                        req310.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                        HttpResponseMessage response = clienFNREP08.SendAsync(req310).Result;
                        int StatusCode = (int)response.StatusCode;
                        string StatusDesc = response.StatusCode.ToString();
                        if (StatusCode == 200)
                        {
                            file = response;
                            return File(file.Content.ReadAsByteArrayAsync().Result, "application/octet-stream", "PRREP46_" + DateTime.Now.ToString("dd_MM_yyyy") + ".pdf");
                        }
                        else
                        {
                            message = "Code : " + StatusCode + " | Description : " + StatusDesc;
                            return View();
                        }
                        //return responsse;
                    }
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        #endregion

        #region PERSONNEL ON PAGER REPORT
        public IActionResult PersonnelOnPagerReport()
        {
            string message = "";
            try
            {

            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        [HttpPost]
        public IActionResult PersonnelOnPagerReport(string POP_BRANCH, string POP_START_PNO, string POP_END_PNO, string POP_SEGMENT, string POP_DEPT, string POP_LEVEL, string POP_DESIG, string POP_GROUP)
        {
            string message = "";
            try
            {
                HttpResponseMessage file = new HttpResponseMessage();
                CITI_EMP2Model entity = new CITI_EMP2Model();
                entity.Branch = POP_BRANCH;
                entity.FromPNo = POP_START_PNO;
                entity.ToPNo = POP_END_PNO;
                entity.Segment = POP_SEGMENT;
                entity.Department = POP_DEPT;
                entity.Level = POP_LEVEL;
                entity.Desig = POP_DESIG;
                entity.Group = POP_GROUP;

                var DomainName = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetSection("CHRIS_REPORTING")["API_URL"];
                string BASE_URL = DomainName + "api/PersonnelReporting/PersonnelOnPagerReport/";
                using (HttpClient clienFNREP08 = new HttpClient())
                {
                    using (HttpRequestMessage req310 = new HttpRequestMessage(HttpMethod.Post, BASE_URL))
                    {
                        req310.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        string dataAsJson = System.Text.Json.JsonSerializer.Serialize(entity,
                        new JsonSerializerOptions() { WriteIndented = false });
                        message += dataAsJson + Environment.NewLine;
                        var content = new StringContent(dataAsJson, Encoding.UTF8, "application/json");
                        req310.Content = content;
                        req310.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                        HttpResponseMessage response = clienFNREP08.SendAsync(req310).Result;
                        int StatusCode = (int)response.StatusCode;
                        string StatusDesc = response.StatusCode.ToString();
                        if (StatusCode == 200)
                        {
                            file = response;
                            return File(file.Content.ReadAsByteArrayAsync().Result, "application/octet-stream", "CITI_EMP2_" + DateTime.Now.ToString("dd_MM_yyyy") + ".pdf");
                        }
                        else
                        {
                            message = "Code : " + StatusCode + " | Description : " + StatusDesc;
                            return View();
                        }
                        //return responsse;
                    }
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        [HttpPost]
        public JsonResult GetDataForOnePagerReport(string Action)
        {
            Dictionary<string, object> d = new Dictionary<string, object>();
            CmnDataManager objCmnDataManager = new CmnDataManager();
            DataTable firstTable = new DataTable();
            if (Action == "GetBranch")
            {
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_BASE_LOV_ACTION", "CITIEMPAD_BRANCH", d);
                if (rsltCode.isSuccessful)
                {
                    if (rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                    {
                        firstTable = rsltCode.dstResult.Tables[0];
                        var CatResul = JsonConvert.SerializeObject(firstTable);
                        return Json(CatResul);
                    }
                    else
                        return Json(null);
                }
                else
                    return Json(null);
            }
            else if (Action == "GetDepartment")
            {
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_BASE_LOV_ACTION", "CITIEMPAD_DEPT", d);
                if (rsltCode.isSuccessful)
                {
                    if (rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                    {
                        firstTable = rsltCode.dstResult.Tables[0];
                        var CatResul = JsonConvert.SerializeObject(firstTable);
                        return Json(CatResul);
                    }
                    else
                        return Json(null);
                }
                else
                    return Json(null);
            }
            else if (Action == "GetLevel")
            {
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_BASE_LOV_ACTION", "CITIEMPAD_LEVEL", d);
                if (rsltCode.isSuccessful)
                {
                    if (rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                    {
                        firstTable = rsltCode.dstResult.Tables[0];
                        var CatResul = JsonConvert.SerializeObject(firstTable);
                        return Json(CatResul);
                    }
                    else
                        return Json(null);
                }
                else
                    return Json(null);
            }
            else if (Action == "GetDesig")
            {
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_BASE_LOV_ACTION", "CITIEMPAD_DESIG", d);
                if (rsltCode.isSuccessful)
                {
                    if (rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                    {
                        firstTable = rsltCode.dstResult.Tables[0];
                        var CatResul = JsonConvert.SerializeObject(firstTable);
                        return Json(CatResul);
                    }
                    else
                        return Json(null);
                }
                else
                    return Json(null);
            }
            else if (Action == "GetGroup")
            {
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_BASE_LOV_ACTION", "CITIEMPAD_GROUP", d);
                if (rsltCode.isSuccessful)
                {
                    if (rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                    {
                        firstTable = rsltCode.dstResult.Tables[0];
                        var CatResul = JsonConvert.SerializeObject(firstTable);
                        return Json(CatResul);
                    }
                    else
                        return Json(null);
                }
                else
                    return Json(null);
            }
            else
                return Json(null);
        }
        #endregion

        #region PERSONNEL REPORT FOR ALL
        public IActionResult NewHiresHead_CountAnalysis()
        {
            string message = "";
            try
            {

            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        [HttpPost]
        public IActionResult NewHiresHead_CountAnalysis(string NHHA_BRANCH, string NHHA_SEGMENT, string NHHA_GROUP, DateTime? NHHA_FROM_DATE, DateTime? NHHA_END_DATE)
        {
            string message = "";
            try
            {
                HttpResponseMessage file = new HttpResponseMessage();
                PRREP52Model entity = new PRREP52Model();
                entity.Branch = NHHA_BRANCH;
                entity.Segment = NHHA_SEGMENT;
                entity.Group = NHHA_GROUP;
                if (NHHA_FROM_DATE != null)
                    entity.FromDate = NHHA_FROM_DATE;
                else
                    entity.FromDate = null;
                if (NHHA_END_DATE != null)
                    entity.EndDate = NHHA_END_DATE;
                else
                    entity.EndDate = null;
                var DomainName = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetSection("CHRIS_REPORTING")["API_URL"];
                string BASE_URL = DomainName + "api/PersonnelReporting/NewHiresHead_CountAnalysis/";
                using (HttpClient clienFNREP08 = new HttpClient())
                {
                    using (HttpRequestMessage req310 = new HttpRequestMessage(HttpMethod.Post, BASE_URL))
                    {
                        req310.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        string dataAsJson = System.Text.Json.JsonSerializer.Serialize(entity,
                        new JsonSerializerOptions() { WriteIndented = false });
                        message += dataAsJson + Environment.NewLine;
                        var content = new StringContent(dataAsJson, Encoding.UTF8, "application/json");
                        req310.Content = content;
                        req310.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                        HttpResponseMessage response = clienFNREP08.SendAsync(req310).Result;
                        int StatusCode = (int)response.StatusCode;
                        string StatusDesc = response.StatusCode.ToString();
                        if (StatusCode == 200)
                        {
                            file = response;
                            return File(file.Content.ReadAsByteArrayAsync().Result, "application/octet-stream", "PRREP52_" + DateTime.Now.ToString("dd_MM_yyyy") + ".pdf");
                        }
                        else
                        {
                            message = "Code : " + StatusCode + " | Description : " + StatusDesc;
                            return View();
                        }
                        //return responsse;
                    }
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        #endregion

        #region REPORT FOR NEW INDUCTION
        public IActionResult NewInduction()
        {
            string message = "";
            try
            {

            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        [HttpPost]
        public IActionResult NewInduction(DateTime? NI_FROM_DATE, DateTime? NI_TODATE, string NI_SEGMENT, string NI_DESIG, string NI_BRANCH, string NI_GROUP)
        {
            string message = "";
            try
            {
                HttpResponseMessage file = new HttpResponseMessage();
                PRREP06Model entity = new PRREP06Model();
                if (NI_FROM_DATE != null)
                    entity.FromDate = NI_FROM_DATE;
                else
                    entity.FromDate = null;
                if (NI_TODATE != null)
                    entity.EndDate = NI_TODATE;
                else
                    entity.EndDate = null;
                entity.Segment = NI_SEGMENT;
                entity.Branch = NI_BRANCH;
                entity.Group = NI_GROUP;
                entity.Desig = NI_DESIG;
                var DomainName = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetSection("CHRIS_REPORTING")["API_URL"];
                string BASE_URL = DomainName + "api/PersonnelReporting/NewInduction/";
                using (HttpClient clienFNREP08 = new HttpClient())
                {
                    using (HttpRequestMessage req310 = new HttpRequestMessage(HttpMethod.Post, BASE_URL))
                    {
                        req310.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        string dataAsJson = System.Text.Json.JsonSerializer.Serialize(entity,
                        new JsonSerializerOptions() { WriteIndented = false });
                        message += dataAsJson + Environment.NewLine;
                        var content = new StringContent(dataAsJson, Encoding.UTF8, "application/json");
                        req310.Content = content;
                        req310.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                        HttpResponseMessage response = clienFNREP08.SendAsync(req310).Result;
                        int StatusCode = (int)response.StatusCode;
                        string StatusDesc = response.StatusCode.ToString();
                        if (StatusCode == 200)
                        {
                            file = response;
                            return File(file.Content.ReadAsByteArrayAsync().Result, "application/octet-stream", "PRREP06_" + DateTime.Now.ToString("dd_MM_yyyy") + ".pdf");
                        }
                        else
                        {
                            message = "Code : " + StatusCode + " | Description : " + StatusDesc;
                            return View();
                        }
                        //return responsse;
                    }
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        #endregion

        #region REPORT FOR ALL EMPLOYEES CURRENT DEPT WISE
        public IActionResult ReportForAllEmployeesCurrentDeptWise()
        {
            string message = "";
            try
            {

            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        [HttpPost]
        public IActionResult ReportForAllEmployeesCurrentDeptWise(string RFA_BRANCH, string RFA_SEGMENT, string RFA_DEPARTMENT, string RFA_GROUP, string RFA_DESIG, string RFA_LEVEL)
        {
            string message = "";
            try
            {
                HttpResponseMessage file = new HttpResponseMessage();
                PREP0001Model entity = new PREP0001Model();
                entity.Branch = RFA_BRANCH;
                entity.Segment = RFA_SEGMENT;
                entity.Department = RFA_DEPARTMENT;
                entity.Group = RFA_GROUP;
                entity.Desig = RFA_DESIG;
                entity.Level = RFA_LEVEL;
                var DomainName = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetSection("CHRIS_REPORTING")["API_URL"];
                string BASE_URL = DomainName + "api/PersonnelReporting/ReportForAllEmployeesCurrentDeptWise/";
                using (HttpClient clienFNREP08 = new HttpClient())
                {
                    using (HttpRequestMessage req310 = new HttpRequestMessage(HttpMethod.Post, BASE_URL))
                    {
                        req310.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        string dataAsJson = System.Text.Json.JsonSerializer.Serialize(entity,
                        new JsonSerializerOptions() { WriteIndented = false });
                        message += dataAsJson + Environment.NewLine;
                        var content = new StringContent(dataAsJson, Encoding.UTF8, "application/json");
                        req310.Content = content;
                        req310.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                        HttpResponseMessage response = clienFNREP08.SendAsync(req310).Result;
                        int StatusCode = (int)response.StatusCode;
                        string StatusDesc = response.StatusCode.ToString();
                        if (StatusCode == 200)
                        {
                            file = response;
                            return File(file.Content.ReadAsByteArrayAsync().Result, "application/octet-stream", "PREP0001_" + DateTime.Now.ToString("dd_MM_yyyy") + ".pdf");
                        }
                        else
                        {
                            message = "Code : " + StatusCode + " | Description : " + StatusDesc;
                            return View();
                        }
                        //return responsse;
                    }
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        #endregion

        #region EMPLOYEES FOR PROBATION
        public IActionResult EmployeesForProbation()
        {
            string message = "";
            try
            {

            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        [HttpPost]
        public IActionResult EmployeesForProbation(string EFP_BRANCH, string EFP_SEGMENT, string EFP_DESIG, string EFP_LEVEL)
        {
            string message = "";
            try
            {
                HttpResponseMessage file = new HttpResponseMessage();
                PRREP03Model entity = new PRREP03Model();
                entity.Branch = EFP_BRANCH;
                entity.Segment = EFP_SEGMENT;
                entity.Desig = EFP_DESIG;
                entity.Level = EFP_LEVEL;
                var DomainName = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetSection("CHRIS_REPORTING")["API_URL"];
                string BASE_URL = DomainName + "api/PersonnelReporting/EmployeesForProbation/";
                using (HttpClient clienFNREP08 = new HttpClient())
                {
                    using (HttpRequestMessage req310 = new HttpRequestMessage(HttpMethod.Post, BASE_URL))
                    {
                        req310.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        string dataAsJson = System.Text.Json.JsonSerializer.Serialize(entity,
                        new JsonSerializerOptions() { WriteIndented = false });
                        message += dataAsJson + Environment.NewLine;
                        var content = new StringContent(dataAsJson, Encoding.UTF8, "application/json");
                        req310.Content = content;
                        req310.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                        HttpResponseMessage response = clienFNREP08.SendAsync(req310).Result;
                        int StatusCode = (int)response.StatusCode;
                        string StatusDesc = response.StatusCode.ToString();
                        if (StatusCode == 200)
                        {
                            file = response;
                            return File(file.Content.ReadAsByteArrayAsync().Result, "application/octet-stream", "PRREP03_" + DateTime.Now.ToString("dd_MM_yyyy") + ".pdf");
                        }
                        else
                        {
                            message = "Code : " + StatusCode + " | Description : " + StatusDesc;
                            return View();
                        }
                        //return responsse;
                    }
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        #endregion

        #region ANALYSIS REPORT
        public IActionResult AnalysisReport()
        {
            string message = "";
            try
            {

            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        [HttpPost]
        public IActionResult AnalysisReport(string AP_BRANCH, string AR_SEGMENT, string AR_DESIG, int? AR_YEAR)
        {
            string message = "";
            try
            {
                HttpResponseMessage file = new HttpResponseMessage();
                PRREP33Model entity = new PRREP33Model();
                entity.Branch = AP_BRANCH;
                entity.Segment = AR_SEGMENT;
                entity.Desig = AR_DESIG;
                if (AR_YEAR != null)
                    entity.Year = Convert.ToInt32(AR_YEAR);
                else
                    entity.Year = null;
                var DomainName = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetSection("CHRIS_REPORTING")["API_URL"];
                string BASE_URL = DomainName + "api/PersonnelReporting/AnalysisReport/";
                using (HttpClient clienFNREP08 = new HttpClient())
                {
                    using (HttpRequestMessage req310 = new HttpRequestMessage(HttpMethod.Post, BASE_URL))
                    {
                        req310.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        string dataAsJson = System.Text.Json.JsonSerializer.Serialize(entity,
                        new JsonSerializerOptions() { WriteIndented = false });
                        message += dataAsJson + Environment.NewLine;
                        var content = new StringContent(dataAsJson, Encoding.UTF8, "application/json");
                        req310.Content = content;
                        req310.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                        HttpResponseMessage response = clienFNREP08.SendAsync(req310).Result;
                        int StatusCode = (int)response.StatusCode;
                        string StatusDesc = response.StatusCode.ToString();
                        if (StatusCode == 200)
                        {
                            file = response;
                            return File(file.Content.ReadAsByteArrayAsync().Result, "application/octet-stream", "PRREP33_" + DateTime.Now.ToString("dd_MM_yyyy") + ".pdf");
                        }
                        else
                        {
                            message = "Code : " + StatusCode + " | Description : " + StatusDesc;
                            return View();
                        }
                        //return responsse;
                    }
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        #endregion

        #region PRIOR EXPERIENCE EDU
        public IActionResult PriorExperienceEDU()
        {
            string message = "";
            try
            {

            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        [HttpPost]
        public IActionResult PriorExperienceEDU(string PEE_BRANCH, string PEE_SEGMENT, string PEE_DESIG, int? PEE_YEAR)
        {
            string message = "";
            try
            {
                HttpResponseMessage file = new HttpResponseMessage();
                PRREP38Model entity = new PRREP38Model();
                entity.Branch = PEE_BRANCH;
                entity.Segment = PEE_SEGMENT;
                entity.Desig = PEE_DESIG;
                if (PEE_YEAR != null)
                    entity.Year = Convert.ToInt32(PEE_YEAR);
                else
                    entity.Year = null;
                var DomainName = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetSection("CHRIS_REPORTING")["API_URL"];
                string BASE_URL = DomainName + "api/PersonnelReporting/PriorExperienceEDU/";
                using (HttpClient clienFNREP08 = new HttpClient())
                {
                    using (HttpRequestMessage req310 = new HttpRequestMessage(HttpMethod.Post, BASE_URL))
                    {
                        req310.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        string dataAsJson = System.Text.Json.JsonSerializer.Serialize(entity,
                        new JsonSerializerOptions() { WriteIndented = false });
                        message += dataAsJson + Environment.NewLine;
                        var content = new StringContent(dataAsJson, Encoding.UTF8, "application/json");
                        req310.Content = content;
                        req310.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                        HttpResponseMessage response = clienFNREP08.SendAsync(req310).Result;
                        int StatusCode = (int)response.StatusCode;
                        string StatusDesc = response.StatusCode.ToString();
                        if (StatusCode == 200)
                        {
                            file = response;
                            return File(file.Content.ReadAsByteArrayAsync().Result, "application/octet-stream", "PRREP38_" + DateTime.Now.ToString("dd_MM_yyyy") + ".pdf");
                        }
                        else
                        {
                            message = "Code : " + StatusCode + " | Description : " + StatusDesc;
                            return View();
                        }
                        //return responsse;
                    }
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        #endregion

        #region PRIOR EXPERIENCE EMP 1
        public IActionResult PriorExp()
        {
            string message = "";
            try
            {

            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        [HttpPost]
        public IActionResult PriorExp(string PE_BRANCH, string PE_SEGMENT, string PE_DESIG, DateTime? PR_TODAY_DATE)
        {
            string message = "";
            try
            {
                HttpResponseMessage file = new HttpResponseMessage();
                PRREP39Model entity = new PRREP39Model();
                entity.Branch = PE_BRANCH;
                entity.Segment = PE_SEGMENT;
                entity.Desig = PE_DESIG;
                if (PR_TODAY_DATE != null)
                    entity.Today_Date = Convert.ToDateTime(PR_TODAY_DATE);
                else
                    entity.Today_Date = null;
                var DomainName = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetSection("CHRIS_REPORTING")["API_URL"];
                string BASE_URL = DomainName + "api/PersonnelReporting/PriorExp/";
                using (HttpClient clienFNREP08 = new HttpClient())
                {
                    using (HttpRequestMessage req310 = new HttpRequestMessage(HttpMethod.Post, BASE_URL))
                    {
                        req310.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        string dataAsJson = System.Text.Json.JsonSerializer.Serialize(entity,
                        new JsonSerializerOptions() { WriteIndented = false });
                        message += dataAsJson + Environment.NewLine;
                        var content = new StringContent(dataAsJson, Encoding.UTF8, "application/json");
                        req310.Content = content;
                        req310.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                        HttpResponseMessage response = clienFNREP08.SendAsync(req310).Result;
                        int StatusCode = (int)response.StatusCode;
                        string StatusDesc = response.StatusCode.ToString();
                        if (StatusCode == 200)
                        {
                            file = response;
                            return File(file.Content.ReadAsByteArrayAsync().Result, "application/octet-stream", "PRREP39_" + DateTime.Now.ToString("dd_MM_yyyy") + ".pdf");
                        }
                        else
                        {
                            message = "Code : " + StatusCode + " | Description : " + StatusDesc;
                            return View();
                        }
                        //return responsse;
                    }
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        #endregion

        #region HEAD COUNT CHART
        public IActionResult HeadCountChart()
        {
            string message = "";
            try
            {

            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        [HttpPost]
        public IActionResult HeadCountChart(string HCC_BRANCH, string HCC_SEGMENT, string HCC_GROUP)
        {
            string message = "";
            try
            {
                HttpResponseMessage file = new HttpResponseMessage();
                PRREP51Model entity = new PRREP51Model();
                entity.Branch = HCC_BRANCH;
                entity.Segment = HCC_SEGMENT;
                entity.Group = HCC_GROUP;
                var DomainName = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetSection("CHRIS_REPORTING")["API_URL"];
                string BASE_URL = DomainName + "api/PersonnelReporting/HeadCountChart/";
                using (HttpClient clienFNREP08 = new HttpClient())
                {
                    using (HttpRequestMessage req310 = new HttpRequestMessage(HttpMethod.Post, BASE_URL))
                    {
                        req310.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        string dataAsJson = System.Text.Json.JsonSerializer.Serialize(entity,
                        new JsonSerializerOptions() { WriteIndented = false });
                        message += dataAsJson + Environment.NewLine;
                        var content = new StringContent(dataAsJson, Encoding.UTF8, "application/json");
                        req310.Content = content;
                        req310.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                        HttpResponseMessage response = clienFNREP08.SendAsync(req310).Result;
                        int StatusCode = (int)response.StatusCode;
                        string StatusDesc = response.StatusCode.ToString();
                        if (StatusCode == 200)
                        {
                            file = response;
                            return File(file.Content.ReadAsByteArrayAsync().Result, "application/octet-stream", "PRREP51_" + DateTime.Now.ToString("dd_MM_yyyy") + ".pdf");
                        }
                        else
                        {
                            message = "Code : " + StatusCode + " | Description : " + StatusDesc;
                            return View();
                        }
                        //return responsse;
                    }
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        #endregion

        #region NEW HIRES HEAD COUNT DEPT WISE
        public IActionResult NewHiresHeadCountDeptWise()
        {
            string message = "";
            try
            {

            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        [HttpPost]
        public IActionResult NewHiresHeadCountDeptWise(string NHHC_BRANCH, string NHHC_SEGMENT, string NHHC_DEPARTMENT, string NHHC_GROUP, string NHHC_DESIG
            , string NHHC_LEVEL, DateTime? NHHC_FROM_DATE, DateTime? NHHC_END_DATE)
        {
            string message = "";
            try
            {
                HttpResponseMessage file = new HttpResponseMessage();
                PRREP50Model entity = new PRREP50Model();
                entity.Branch = NHHC_BRANCH;
                entity.Segment = NHHC_SEGMENT;
                entity.Department = NHHC_DEPARTMENT;
                entity.Group = NHHC_GROUP;
                entity.Desig = NHHC_DESIG;
                entity.Level = NHHC_LEVEL;
                if (NHHC_FROM_DATE != null)
                    entity.Start_Date = Convert.ToDateTime(NHHC_FROM_DATE);
                else
                    entity.Start_Date = null;
                if (NHHC_END_DATE != null)
                    entity.End_Date = Convert.ToDateTime(NHHC_END_DATE);
                else
                    entity.End_Date = null;
                var DomainName = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetSection("CHRIS_REPORTING")["API_URL"];
                string BASE_URL = DomainName + "api/PersonnelReporting/NewHiresHeadCountDeptWise/";
                using (HttpClient clienFNREP08 = new HttpClient())
                {
                    using (HttpRequestMessage req310 = new HttpRequestMessage(HttpMethod.Post, BASE_URL))
                    {
                        req310.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        string dataAsJson = System.Text.Json.JsonSerializer.Serialize(entity,
                        new JsonSerializerOptions() { WriteIndented = false });
                        message += dataAsJson + Environment.NewLine;
                        var content = new StringContent(dataAsJson, Encoding.UTF8, "application/json");
                        req310.Content = content;
                        req310.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                        HttpResponseMessage response = clienFNREP08.SendAsync(req310).Result;
                        int StatusCode = (int)response.StatusCode;
                        string StatusDesc = response.StatusCode.ToString();
                        if (StatusCode == 200)
                        {
                            file = response;
                            return File(file.Content.ReadAsByteArrayAsync().Result, "application/octet-stream", "PRREP51_" + DateTime.Now.ToString("dd_MM_yyyy") + ".pdf");
                        }
                        else
                        {
                            message = "Code : " + StatusCode + " | Description : " + StatusDesc;
                            return View();
                        }
                        //return responsse;
                    }
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        #endregion

        #region PF REPORT
        public IActionResult PFReport()
        {
            string message = "";
            try
            {

            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        [HttpPost]
        public IActionResult PFReport(string PF_YEAR, string PF_PNO, string PF_BRANCH, string PF_DEPARTMENT, DateTime? PF_TERMINATION_DATE, string PF_TRUSTEE1, string PF_TRUSTEE2, string PF_ZAKAT)
        {
            string message = "";
            try
            {
                HttpResponseMessage file = new HttpResponseMessage();
                PFR07NEWModel entity = new PFR07NEWModel();
                entity.Year = PF_YEAR != null ? Convert.ToInt32(PF_YEAR) : null;
                entity.PNO = PF_PNO != null ? Convert.ToInt32(PF_PNO) : null;
                entity.Branch = PF_BRANCH;
                entity.Department = PF_DEPARTMENT;
                entity.TerminationDate = PF_TERMINATION_DATE != null ? Convert.ToDateTime(PF_TERMINATION_DATE) : null;
                entity.Trustee1 = PF_TRUSTEE1;
                entity.Trustee2 = PF_TRUSTEE2;
                entity.Zakat = PF_ZAKAT;
                var DomainName = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetSection("CHRIS_REPORTING")["API_URL"];
                string BASE_URL = DomainName + "api/PersonnelReporting/PFReport/";
                using (HttpClient clienFNREP08 = new HttpClient())
                {
                    using (HttpRequestMessage req310 = new HttpRequestMessage(HttpMethod.Post, BASE_URL))
                    {
                        req310.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        string dataAsJson = System.Text.Json.JsonSerializer.Serialize(entity,
                        new JsonSerializerOptions() { WriteIndented = false });
                        message += dataAsJson + Environment.NewLine;
                        var content = new StringContent(dataAsJson, Encoding.UTF8, "application/json");
                        req310.Content = content;
                        req310.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                        HttpResponseMessage response = clienFNREP08.SendAsync(req310).Result;
                        int StatusCode = (int)response.StatusCode;
                        string StatusDesc = response.StatusCode.ToString();
                        if (StatusCode == 200)
                        {
                            file = response;
                            return File(file.Content.ReadAsByteArrayAsync().Result, "application/octet-stream", "PFR07NEW_" + DateTime.Now.ToString("dd_MM_yyyy") + ".pdf");
                        }
                        else
                        {
                            message = "Code : " + StatusCode + " | Description : " + StatusDesc;
                            return View();
                        }
                        //return responsse;
                    }
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        #endregion

        #region GRATUITY SETTLEMENT
        public IActionResult GratuitySettlement()
        {
            string message = "";
            try
            {

            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        [HttpPost]
        public IActionResult GratuitySettlement(string GS_YEAR, string GS_PNO, string GS_BRANCH, string GS_DEPARTMENT, DateTime? GS_TERMINATION_DATE,
            string GS_TRUSTEES1, string GS_TRUSTEES2)
        {
            string message = "";
            try
            {
                HttpResponseMessage file = new HttpResponseMessage();
                GRATUITYModel entity = new GRATUITYModel();
                entity.Year = GS_YEAR != null ? Convert.ToInt32(GS_YEAR) : null;
                entity.PNO = GS_PNO != null ? Convert.ToInt32(GS_PNO) : null;
                entity.Branch = GS_BRANCH;
                entity.Department = GS_DEPARTMENT;
                entity.TerminationDate = GS_TERMINATION_DATE != null ? Convert.ToDateTime(GS_TERMINATION_DATE) : null;
                entity.Trustee1 = GS_TRUSTEES1;
                entity.Trustee2 = GS_TRUSTEES2;
                var DomainName = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetSection("CHRIS_REPORTING")["API_URL"];
                string BASE_URL = DomainName + "api/PersonnelReporting/GratuitySettlement/";
                using (HttpClient clienFNREP08 = new HttpClient())
                {
                    using (HttpRequestMessage req310 = new HttpRequestMessage(HttpMethod.Post, BASE_URL))
                    {
                        req310.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        string dataAsJson = System.Text.Json.JsonSerializer.Serialize(entity,
                        new JsonSerializerOptions() { WriteIndented = false });
                        message += dataAsJson + Environment.NewLine;
                        var content = new StringContent(dataAsJson, Encoding.UTF8, "application/json");
                        req310.Content = content;
                        req310.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                        HttpResponseMessage response = clienFNREP08.SendAsync(req310).Result;
                        int StatusCode = (int)response.StatusCode;
                        string StatusDesc = response.StatusCode.ToString();
                        if (StatusCode == 200)
                        {
                            file = response;
                            return File(file.Content.ReadAsByteArrayAsync().Result, "application/octet-stream", "GRATUITY_" + DateTime.Now.ToString("dd_MM_yyyy") + ".pdf");
                        }
                        else
                        {
                            message = "Code : " + StatusCode + " | Description : " + StatusDesc;
                            return View();
                        }
                        //return responsse;
                    }
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        #endregion

        #region ATTRITION REPORT
        public IActionResult AttritionReport()
        {
            string message = "";
            try
            {

            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        [HttpPost]
        public IActionResult AttritionReport(DateTime? AR_START_DATE, DateTime? AR_END_DATE, string AR_CATEGORY, string AP_TYPE, string AR_BRANCH, string AR_SEGMENT,
            string AR_GROUP, string AR_DEPARTMENT, string AR_SORT_ORDER)
        {
            string message = "";
            try
            {
                HttpResponseMessage file = new HttpResponseMessage();
                ATTRITION_REP1Model entity = new ATTRITION_REP1Model();
                entity.StartDate = AR_START_DATE != null ? Convert.ToDateTime(AR_START_DATE) : null;
                entity.EndDate = AR_END_DATE != null ? Convert.ToDateTime(AR_END_DATE) : null;
                entity.Category = AR_CATEGORY;
                entity.Type = AP_TYPE;
                entity.Branch = AR_BRANCH;
                entity.Segment = AR_SEGMENT;
                entity.Group = AR_GROUP;
                entity.Department = AR_DEPARTMENT;
                entity.Order = AR_SORT_ORDER;
                var DomainName = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetSection("CHRIS_REPORTING")["API_URL"];
                string BASE_URL = DomainName + "api/PersonnelReporting/AttritionReport/";
                using (HttpClient clienFNREP08 = new HttpClient())
                {
                    using (HttpRequestMessage req310 = new HttpRequestMessage(HttpMethod.Post, BASE_URL))
                    {
                        req310.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        string dataAsJson = System.Text.Json.JsonSerializer.Serialize(entity,
                        new JsonSerializerOptions() { WriteIndented = false });
                        message += dataAsJson + Environment.NewLine;
                        var content = new StringContent(dataAsJson, Encoding.UTF8, "application/json");
                        req310.Content = content;
                        req310.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                        HttpResponseMessage response = clienFNREP08.SendAsync(req310).Result;
                        int StatusCode = (int)response.StatusCode;
                        string StatusDesc = response.StatusCode.ToString();
                        if (StatusCode == 200)
                        {
                            file = response;
                            return File(file.Content.ReadAsByteArrayAsync().Result, "application/octet-stream", "ATTRITION_REP1_" + DateTime.Now.ToString("dd_MM_yyyy") + ".pdf");
                        }
                        else
                        {
                            message = "Code : " + StatusCode + " | Description : " + StatusDesc;
                            return View();
                        }
                        //return responsse;
                    }
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        [HttpPost]
        public JsonResult AttritionData(string Action)
        {
            Dictionary<string, object> d = new Dictionary<string, object>();
            CmnDataManager objCmnDataManager = new CmnDataManager();
            DataTable firstTable = new DataTable();
            if (Action == "Branch")
            {
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_BRANCH_MANAGER", "BranchCountnull", d);
                if (rsltCode.isSuccessful)
                {
                    if (rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                    {
                        firstTable = rsltCode.dstResult.Tables[0];
                        var CatResul = JsonConvert.SerializeObject(firstTable);
                        return Json(CatResul);
                    }
                    else
                        return Json(null);
                }
                else
                    return Json(null);
            }
            else if (Action == "Segment")
            {
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_BASE_LOV_ACTION", "SEGNEWNULL", d);
                if (rsltCode.isSuccessful)
                {
                    if (rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                    {
                        firstTable = rsltCode.dstResult.Tables[0];
                        var CatResul = JsonConvert.SerializeObject(firstTable);
                        return Json(CatResul);
                    }
                    else
                        return Json(null);
                }
                else
                    return Json(null);
            }
            else if (Action == "Department")
            {
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_DEPT_MANAGER", "DEPT_CMB_null", d);
                if (rsltCode.isSuccessful)
                {
                    if (rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                    {
                        firstTable = rsltCode.dstResult.Tables[0];
                        var CatResul = JsonConvert.SerializeObject(firstTable);
                        return Json(CatResul);
                    }
                    else
                        return Json(null);
                }
                else
                    return Json(null);
            }
            else if (Action == "Group")
            {
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_DEPT_MANAGER", "GROUP_CMB_null", d);
                if (rsltCode.isSuccessful)
                {
                    if (rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                    {
                        firstTable = rsltCode.dstResult.Tables[0];
                        var CatResul = JsonConvert.SerializeObject(firstTable);
                        return Json(CatResul);
                    }
                    else
                        return Json(null);
                }
                else
                    return Json(null);
            }
            else if (Action == "Category")
            {
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_CATEGORY_MANAGER", "CAT_CMB_null", d);
                if (rsltCode.isSuccessful)
                {
                    if (rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                    {
                        firstTable = rsltCode.dstResult.Tables[0];
                        var CatResul = JsonConvert.SerializeObject(firstTable);
                        return Json(CatResul);
                    }
                    else
                        return Json(null);
                }
                else
                    return Json(null);
            }
            else if (Action == "Type")
            {
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_CATEGORY_MANAGER", "TYP_CMB_null", d);
                if (rsltCode.isSuccessful)
                {
                    if (rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                    {
                        firstTable = rsltCode.dstResult.Tables[0];
                        var CatResul = JsonConvert.SerializeObject(firstTable);
                        return Json(CatResul);
                    }
                    else
                        return Json(null);
                }
                else
                    return Json(null);
            }
            return Json(null);
        }
        #endregion

        #region ATTRITION SUMMARY REPORT
        public IActionResult AttritionSummaryReport()
        {
            string message = "";
            try
            {

            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        [HttpPost]
        public IActionResult AttritionSummaryReport(DateTime? ASR_START_DATE, DateTime? ASR_END_DATE, string ASR_BRANCH, string ASR_SEGMENT)
        {
            string message = "";
            try
            {
                HttpResponseMessage file = new HttpResponseMessage();
                ATTR_REP_SUMMModel entity = new ATTR_REP_SUMMModel();
                entity.StartDate = ASR_START_DATE != null ? Convert.ToDateTime(ASR_START_DATE) : null;
                entity.EndDate = ASR_END_DATE != null ? Convert.ToDateTime(ASR_END_DATE) : null;
                entity.Branch = ASR_BRANCH;
                entity.Segment = ASR_SEGMENT;
                var DomainName = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetSection("CHRIS_REPORTING")["API_URL"];
                string BASE_URL = DomainName + "api/PersonnelReporting/AttritionSummaryReport/";
                using (HttpClient clienFNREP08 = new HttpClient())
                {
                    using (HttpRequestMessage req310 = new HttpRequestMessage(HttpMethod.Post, BASE_URL))
                    {
                        req310.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        string dataAsJson = System.Text.Json.JsonSerializer.Serialize(entity,
                        new JsonSerializerOptions() { WriteIndented = false });
                        message += dataAsJson + Environment.NewLine;
                        var content = new StringContent(dataAsJson, Encoding.UTF8, "application/json");
                        req310.Content = content;
                        req310.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                        HttpResponseMessage response = clienFNREP08.SendAsync(req310).Result;
                        int StatusCode = (int)response.StatusCode;
                        string StatusDesc = response.StatusCode.ToString();
                        if (StatusCode == 200)
                        {
                            file = response;
                            return File(file.Content.ReadAsByteArrayAsync().Result, "application/octet-stream", "ATTR_REP_SUMM_" + DateTime.Now.ToString("dd_MM_yyyy") + ".pdf");
                        }
                        else
                        {
                            message = "Code : " + StatusCode + " | Description : " + StatusDesc;
                            return View();
                        }
                        //return responsse;
                    }
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        [HttpPost]
        public JsonResult AttritionSummaryData()
        {
            Dictionary<string, object> d = new Dictionary<string, object>();
            CmnDataManager objCmnDataManager = new CmnDataManager();
            DataTable firstTable = new DataTable();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_BASE_LOV_ACTION", "BRANCHCODENAMENULL", d);
            if (rsltCode.isSuccessful)
            {
                if (rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                {
                    firstTable = rsltCode.dstResult.Tables[0];
                    var CatResul = JsonConvert.SerializeObject(firstTable);
                    return Json(CatResul);
                }
                else
                    return Json(null);
            }
            else
                return Json(null);
        }
        #endregion

        #region OVER TIME ENTRY REPORT
        public IActionResult OverTimeEntryReport()
        {
            string message = "";
            try
            {

            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        [HttpPost]
        public IActionResult OverTimeEntryReport(string OTE_START_PNO, string OTE_END_PNO, string OTE_MONTH, string OTE_BRANCH)
        {
            string message = "";
            try
            {
                HttpResponseMessage file = new HttpResponseMessage();
                PY017Model entity = new PY017Model();
                entity.Start_PNo = OTE_START_PNO != null ? Convert.ToInt32(OTE_START_PNO) : null;
                entity.End_PNo = OTE_END_PNO != null ? Convert.ToInt32(OTE_END_PNO) : null;
                entity.Branch = OTE_BRANCH;
                entity.Month = OTE_MONTH;
                var DomainName = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetSection("CHRIS_REPORTING")["API_URL"];
                string BASE_URL = DomainName + "api/PersonnelReporting/OverTimeEntryReport/";
                using (HttpClient clienFNREP08 = new HttpClient())
                {
                    using (HttpRequestMessage req310 = new HttpRequestMessage(HttpMethod.Post, BASE_URL))
                    {
                        req310.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        string dataAsJson = System.Text.Json.JsonSerializer.Serialize(entity,
                        new JsonSerializerOptions() { WriteIndented = false });
                        message += dataAsJson + Environment.NewLine;
                        var content = new StringContent(dataAsJson, Encoding.UTF8, "application/json");
                        req310.Content = content;
                        req310.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                        HttpResponseMessage response = clienFNREP08.SendAsync(req310).Result;
                        int StatusCode = (int)response.StatusCode;
                        string StatusDesc = response.StatusCode.ToString();
                        if (StatusCode == 200)
                        {
                            file = response;
                            return File(file.Content.ReadAsByteArrayAsync().Result, "application/octet-stream", "PY017_" + DateTime.Now.ToString("dd_MM_yyyy") + ".pdf");
                        }
                        else
                        {
                            message = "Code : " + StatusCode + " | Description : " + StatusDesc;
                            return View();
                        }
                        //return responsse;
                    }
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        #endregion

        #region EMPLOYEES CURRENT
        public IActionResult EmployeesCurrent()
        {
            string message = "";
            try
            {

            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        [HttpPost]
        public IActionResult EmployeesCurrent(string EC_BRANCH, string EC_SEGMENT, string EC_DEPARTMENT, string EC_GORUP, string EC_DESIG, string EC_LEVEL)
        {
            string message = "";
            try
            {
                HttpResponseMessage file = new HttpResponseMessage();
                PRREP01Model entity = new PRREP01Model();
                entity.Branch = EC_BRANCH;
                entity.Segment = EC_SEGMENT;
                entity.Department = EC_DEPARTMENT;
                entity.Group = EC_GORUP;
                entity.Desig = EC_DESIG;
                entity.Level = EC_LEVEL;
                var DomainName = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetSection("CHRIS_REPORTING")["API_URL"];
                string BASE_URL = DomainName + "api/PersonnelReporting/EmployeesCurrent/";
                using (HttpClient clienFNREP08 = new HttpClient())
                {
                    using (HttpRequestMessage req310 = new HttpRequestMessage(HttpMethod.Post, BASE_URL))
                    {
                        req310.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        string dataAsJson = System.Text.Json.JsonSerializer.Serialize(entity,
                        new JsonSerializerOptions() { WriteIndented = false });
                        message += dataAsJson + Environment.NewLine;
                        var content = new StringContent(dataAsJson, Encoding.UTF8, "application/json");
                        req310.Content = content;
                        req310.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                        HttpResponseMessage response = clienFNREP08.SendAsync(req310).Result;
                        int StatusCode = (int)response.StatusCode;
                        string StatusDesc = response.StatusCode.ToString();
                        if (StatusCode == 200)
                        {
                            file = response;
                            return File(file.Content.ReadAsByteArrayAsync().Result, "application/octet-stream", "PRREP01_" + DateTime.Now.ToString("dd_MM_yyyy") + ".pdf");
                        }
                        else
                        {
                            message = "Code : " + StatusCode + " | Description : " + StatusDesc;
                            return View();
                        }
                        //return responsse;
                    }
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        #endregion
       
        #region REPORT FOR CONFIRMATION DUE
        public IActionResult ReportForConfirmationDue()
        {
            string message = "";
            try
            {

            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        [HttpPost]
        public IActionResult ReportForConfirmationDue(string RCD_BRANCH, string RCD_ISSUE_DATE, string RCD_EXPIRY_DATE, string RCD_SEGMENT, string RCD_DESIG)
        {
            string message = "";
            try
            {
                HttpResponseMessage file = new HttpResponseMessage();
                PRREP04Model entity = new PRREP04Model();
                entity.Branch = RCD_BRANCH;
                entity.IssueDate = RCD_ISSUE_DATE != null ? Convert.ToDateTime(RCD_ISSUE_DATE) : null;
                entity.ExpiryDate = RCD_EXPIRY_DATE != null ? Convert.ToDateTime(RCD_EXPIRY_DATE) : null;
                entity.NewBranch = RCD_BRANCH;
                entity.Segment = RCD_SEGMENT;
                entity.Desig = RCD_DESIG;
                var DomainName = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetSection("CHRIS_REPORTING")["API_URL"];
                string BASE_URL = DomainName + "api/PersonnelReporting/ReportForConfirmationDue/";
                using (HttpClient clienFNREP08 = new HttpClient())
                {
                    using (HttpRequestMessage req310 = new HttpRequestMessage(HttpMethod.Post, BASE_URL))
                    {
                        req310.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        string dataAsJson = System.Text.Json.JsonSerializer.Serialize(entity,
                        new JsonSerializerOptions() { WriteIndented = false });
                        message += dataAsJson + Environment.NewLine;
                        var content = new StringContent(dataAsJson, Encoding.UTF8, "application/json");
                        req310.Content = content;
                        req310.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                        HttpResponseMessage response = clienFNREP08.SendAsync(req310).Result;
                        int StatusCode = (int)response.StatusCode;
                        string StatusDesc = response.StatusCode.ToString();
                        if (StatusCode == 200)
                        {
                            file = response;
                            return File(file.Content.ReadAsByteArrayAsync().Result, "application/octet-stream", "PRREP04_" + DateTime.Now.ToString("dd_MM_yyyy") + ".pdf");
                        }
                        else
                        {
                            message = "Code : " + StatusCode + " | Description : " + StatusDesc;
                            return View();
                        }
                        //return responsse;
                    }
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        #endregion
       
        #region APPRAISAL DUE
        public IActionResult AppraisalDue()
        {
            string message = "";
            try
            {

            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        [HttpPost]
        public IActionResult AppraisalDue(string AD_START_DATE, string AD_END_DATE, string AD_BRANCH)
        {
            string message = "";
            try
            {
                HttpResponseMessage file = new HttpResponseMessage();
                PRREP08Model entity = new PRREP08Model();
                entity.StartDate = AD_START_DATE != null ? Convert.ToDateTime(AD_START_DATE) : null;
                entity.EndDate = AD_END_DATE != null ? Convert.ToDateTime(AD_END_DATE) : null;
                entity.Branch = AD_BRANCH;
                var DomainName = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetSection("CHRIS_REPORTING")["API_URL"];
                string BASE_URL = DomainName + "api/PersonnelReporting/AppraisalDue/";
                using (HttpClient clienFNREP08 = new HttpClient())
                {
                    using (HttpRequestMessage req310 = new HttpRequestMessage(HttpMethod.Post, BASE_URL))
                    {
                        req310.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        string dataAsJson = System.Text.Json.JsonSerializer.Serialize(entity,
                        new JsonSerializerOptions() { WriteIndented = false });
                        message += dataAsJson + Environment.NewLine;
                        var content = new StringContent(dataAsJson, Encoding.UTF8, "application/json");
                        req310.Content = content;
                        req310.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                        HttpResponseMessage response = clienFNREP08.SendAsync(req310).Result;
                        int StatusCode = (int)response.StatusCode;
                        string StatusDesc = response.StatusCode.ToString();
                        if (StatusCode == 200)
                        {
                            file = response;
                            return File(file.Content.ReadAsByteArrayAsync().Result, "application/octet-stream", "PRREP08_" + DateTime.Now.ToString("dd_MM_yyyy") + ".pdf");
                        }
                        else
                        {
                            message = "Code : " + StatusCode + " | Description : " + StatusDesc;
                            return View();
                        }
                        //return responsse;
                    }
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        #endregion

        #region GIVEN DESIGNATION
        public IActionResult GivenDesignation()
        {
            string message = "";
            try
            {

            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        [HttpPost]
        public IActionResult GivenDesignation(string GD_TODAY_DATE, string GD_BRANCH, string GD_SEGMENT, string GD_DESIG)
        {
            string message = "";
            try
            {
                HttpResponseMessage file = new HttpResponseMessage();
                PRREP09Model entity = new PRREP09Model();
                entity.Branch = GD_BRANCH;
                entity.Desig = GD_DESIG;
                entity.Segment = GD_SEGMENT;
                entity.TodayDate = GD_TODAY_DATE != null ? Convert.ToDateTime(GD_TODAY_DATE) : null;
                var DomainName = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetSection("CHRIS_REPORTING")["API_URL"];
                string BASE_URL = DomainName + "api/PersonnelReporting/GivenDesignation/";
                using (HttpClient clienFNREP08 = new HttpClient())
                {
                    using (HttpRequestMessage req310 = new HttpRequestMessage(HttpMethod.Post, BASE_URL))
                    {
                        req310.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        string dataAsJson = System.Text.Json.JsonSerializer.Serialize(entity,
                        new JsonSerializerOptions() { WriteIndented = false });
                        message += dataAsJson + Environment.NewLine;
                        var content = new StringContent(dataAsJson, Encoding.UTF8, "application/json");
                        req310.Content = content;
                        req310.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                        HttpResponseMessage response = clienFNREP08.SendAsync(req310).Result;
                        int StatusCode = (int)response.StatusCode;
                        string StatusDesc = response.StatusCode.ToString();
                        if (StatusCode == 200)
                        {
                            file = response;
                            return File(file.Content.ReadAsByteArrayAsync().Result, "application/octet-stream", "PRREP09_" + DateTime.Now.ToString("dd_MM_yyyy") + ".pdf");
                        }
                        else
                        {
                            message = "Code : " + StatusCode + " | Description : " + StatusDesc;
                            return View();
                        }
                        //return responsse;
                    }
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        #endregion
        
        #region EMPLOYEES CURRENT SALARY STATUS
        public IActionResult EmployeesCurrentSalaryStatus()
        {
            string message = "";
            try
            {

            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        [HttpPost]
        public IActionResult EmployeesCurrentSalaryStatus(string ECS_BRANCH)
        {
            string message = "";
            try
            {
                HttpResponseMessage file = new HttpResponseMessage();
                PY018Model entity = new PY018Model();
                entity.Branch = ECS_BRANCH;
                var DomainName = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetSection("CHRIS_REPORTING")["API_URL"];
                string BASE_URL = DomainName + "api/PersonnelReporting/EmployeesCurrentSalaryStatus/";
                using (HttpClient clienFNREP08 = new HttpClient())
                {
                    using (HttpRequestMessage req310 = new HttpRequestMessage(HttpMethod.Post, BASE_URL))
                    {
                        req310.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        string dataAsJson = System.Text.Json.JsonSerializer.Serialize(entity,
                        new JsonSerializerOptions() { WriteIndented = false });
                        message += dataAsJson + Environment.NewLine;
                        var content = new StringContent(dataAsJson, Encoding.UTF8, "application/json");
                        req310.Content = content;
                        req310.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                        HttpResponseMessage response = clienFNREP08.SendAsync(req310).Result;
                        int StatusCode = (int)response.StatusCode;
                        string StatusDesc = response.StatusCode.ToString();
                        if (StatusCode == 200)
                        {
                            file = response;
                            return File(file.Content.ReadAsByteArrayAsync().Result, "application/octet-stream", "PY018_" + DateTime.Now.ToString("dd_MM_yyyy") + ".pdf");
                        }
                        else
                        {
                            message = "Code : " + StatusCode + " | Description : " + StatusDesc;
                            return View();
                        }
                        //return responsse;
                    }
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        #endregion

        #region REPORT FOR DEPARTMENT
        public IActionResult ReportForDepartment()
        {
            string message = "";
            try
            {

            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        [HttpPost]
        public IActionResult ReportForDepartment(string RD_RUN_DATE, string RD_BRANCH, string RD_SEGMENT, string RD_GROUP, string RD_DEPARTMENT)
        {
            string message = "";
            try
            {
                HttpResponseMessage file = new HttpResponseMessage();
                PRREP02Model entity = new PRREP02Model();
                entity.Branch = RD_BRANCH;
                entity.Segment = RD_SEGMENT;
                entity.Department = RD_DEPARTMENT;
                entity.Group = RD_GROUP;
                entity.RunDate = RD_RUN_DATE != null ? Convert.ToDateTime(RD_RUN_DATE) : null;
                var DomainName = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetSection("CHRIS_REPORTING")["API_URL"];
                string BASE_URL = DomainName + "api/PersonnelReporting/ReportForDepartment/";
                using (HttpClient clienFNREP08 = new HttpClient())
                {
                    using (HttpRequestMessage req310 = new HttpRequestMessage(HttpMethod.Post, BASE_URL))
                    {
                        req310.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        string dataAsJson = System.Text.Json.JsonSerializer.Serialize(entity,
                        new JsonSerializerOptions() { WriteIndented = false });
                        message += dataAsJson + Environment.NewLine;
                        var content = new StringContent(dataAsJson, Encoding.UTF8, "application/json");
                        req310.Content = content;
                        req310.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                        HttpResponseMessage response = clienFNREP08.SendAsync(req310).Result;
                        int StatusCode = (int)response.StatusCode;
                        string StatusDesc = response.StatusCode.ToString();
                        if (StatusCode == 200)
                        {
                            file = response;
                            return File(file.Content.ReadAsByteArrayAsync().Result, "application/octet-stream", "PRREP02_" + DateTime.Now.ToString("dd_MM_yyyy") + ".pdf");
                        }
                        else
                        {
                            message = "Code : " + StatusCode + " | Description : " + StatusDesc;
                            return View();
                        }
                        //return responsse;
                    }
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        #endregion

        #region REPORT FOR INTERNATIONAL STAFF
        public IActionResult ReportForInternationalStaff()
        {
            string message = "";
            try
            {

            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        [HttpPost]
        public IActionResult ReportForInternationalStaff(string Nothing)
        {
            string message = "";
            try
            {
                HttpResponseMessage file = new HttpResponseMessage();
                PRREP05Model entity = new PRREP05Model();
                var DomainName = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetSection("CHRIS_REPORTING")["API_URL"];
                string BASE_URL = DomainName + "api/PersonnelReporting/ReportForInternationalStaff/";
                using (HttpClient clienFNREP08 = new HttpClient())
                {
                    using (HttpRequestMessage req310 = new HttpRequestMessage(HttpMethod.Post, BASE_URL))
                    {
                        req310.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        string dataAsJson = System.Text.Json.JsonSerializer.Serialize(entity,
                        new JsonSerializerOptions() { WriteIndented = false });
                        message += dataAsJson + Environment.NewLine;
                        var content = new StringContent(dataAsJson, Encoding.UTF8, "application/json");
                        req310.Content = content;
                        req310.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                        HttpResponseMessage response = clienFNREP08.SendAsync(req310).Result;
                        int StatusCode = (int)response.StatusCode;
                        string StatusDesc = response.StatusCode.ToString();
                        if (StatusCode == 200)
                        {
                            file = response;
                            return File(file.Content.ReadAsByteArrayAsync().Result, "application/octet-stream", "PRREP05_" + DateTime.Now.ToString("dd_MM_yyyy") + ".pdf");
                        }
                        else
                        {
                            message = "Code : " + StatusCode + " | Description : " + StatusDesc;
                            return View();
                        }
                        //return responsse;
                    }
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        #endregion
       
        #region SERVICE TERMINATION REPORT
        public IActionResult ServiceTerminationReport()
        {
            string message = "";
            try
            {

            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        [HttpPost]
        public IActionResult ServiceTerminationReport(string STR_BRANCH, string STR_SEGMENT, string STR_DESIG, string STR_START_DATE, string STR_END_DATE, string STR_HOLD)
        {
            string message = "";
            try
            {
                HttpResponseMessage file = new HttpResponseMessage();
                PRREP07Model entity = new PRREP07Model();
                entity.Hold = STR_HOLD;
                entity.StartDate = STR_START_DATE != null ? Convert.ToDateTime(STR_START_DATE) : null;
                entity.EndDate = STR_END_DATE != null ? Convert.ToDateTime(STR_END_DATE) : null;
                entity.Branch = STR_BRANCH;
                entity.Segment = STR_SEGMENT;
                entity.Desig = STR_DESIG;
                var DomainName = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetSection("CHRIS_REPORTING")["API_URL"];
                string BASE_URL = DomainName + "api/PersonnelReporting/ServiceTerminationReport/";
                using (HttpClient clienFNREP08 = new HttpClient())
                {
                    using (HttpRequestMessage req310 = new HttpRequestMessage(HttpMethod.Post, BASE_URL))
                    {
                        req310.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        string dataAsJson = System.Text.Json.JsonSerializer.Serialize(entity,
                        new JsonSerializerOptions() { WriteIndented = false });
                        message += dataAsJson + Environment.NewLine;
                        var content = new StringContent(dataAsJson, Encoding.UTF8, "application/json");
                        req310.Content = content;
                        req310.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                        HttpResponseMessage response = clienFNREP08.SendAsync(req310).Result;
                        int StatusCode = (int)response.StatusCode;
                        string StatusDesc = response.StatusCode.ToString();
                        if (StatusCode == 200)
                        {
                            file = response;
                            return File(file.Content.ReadAsByteArrayAsync().Result, "application/octet-stream", "PRREP07_" + DateTime.Now.ToString("dd_MM_yyyy") + ".pdf");
                        }
                        else
                        {
                            message = "Code : " + StatusCode + " | Description : " + StatusDesc;
                            return View();
                        }
                        //return responsse;
                    }
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        #endregion
      
        #region REPORT FOR INTERN
        public IActionResult ReportForIntern()
        {
            string message = "";
            try
            {

            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        [HttpPost]
        public IActionResult ReportForIntern(string RFI_BRANCH)
        {
            string message = "";
            try
            {
                HttpResponseMessage file = new HttpResponseMessage();
                PRREP10Model entity = new PRREP10Model();
                entity.Branch = RFI_BRANCH;
                var DomainName = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetSection("CHRIS_REPORTING")["API_URL"];
                string BASE_URL = DomainName + "api/PersonnelReporting/ReportForIntern/";
                using (HttpClient clienFNREP08 = new HttpClient())
                {
                    using (HttpRequestMessage req310 = new HttpRequestMessage(HttpMethod.Post, BASE_URL))
                    {
                        req310.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        string dataAsJson = System.Text.Json.JsonSerializer.Serialize(entity,
                        new JsonSerializerOptions() { WriteIndented = false });
                        message += dataAsJson + Environment.NewLine;
                        var content = new StringContent(dataAsJson, Encoding.UTF8, "application/json");
                        req310.Content = content;
                        req310.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                        HttpResponseMessage response = clienFNREP08.SendAsync(req310).Result;
                        int StatusCode = (int)response.StatusCode;
                        string StatusDesc = response.StatusCode.ToString();
                        if (StatusCode == 200)
                        {
                            file = response;
                            return File(file.Content.ReadAsByteArrayAsync().Result, "application/octet-stream", "PRREP10_" + DateTime.Now.ToString("dd_MM_yyyy") + ".pdf");
                        }
                        else
                        {
                            message = "Code : " + StatusCode + " | Description : " + StatusDesc;
                            return View();
                        }
                        //return responsse;
                    }
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        #endregion
       
        #region REPORT FOR CONTRACTUAL
        public IActionResult ReprtForContractuals()
        {
            string message = "";
            try
            {

            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        [HttpPost]
        public IActionResult ReprtForContractuals(string RFC_BRANCH)
        {
            string message = "";
            try
            {
                HttpResponseMessage file = new HttpResponseMessage();
                PRREP11Model entity = new PRREP11Model();
                entity.Branch = RFC_BRANCH;
                var DomainName = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetSection("CHRIS_REPORTING")["API_URL"];
                string BASE_URL = DomainName + "api/PersonnelReporting/ReprtForContractuals/";
                using (HttpClient clienFNREP08 = new HttpClient())
                {
                    using (HttpRequestMessage req310 = new HttpRequestMessage(HttpMethod.Post, BASE_URL))
                    {
                        req310.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        string dataAsJson = System.Text.Json.JsonSerializer.Serialize(entity,
                        new JsonSerializerOptions() { WriteIndented = false });
                        message += dataAsJson + Environment.NewLine;
                        var content = new StringContent(dataAsJson, Encoding.UTF8, "application/json");
                        req310.Content = content;
                        req310.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                        HttpResponseMessage response = clienFNREP08.SendAsync(req310).Result;
                        int StatusCode = (int)response.StatusCode;
                        string StatusDesc = response.StatusCode.ToString();
                        if (StatusCode == 200)
                        {
                            file = response;
                            return File(file.Content.ReadAsByteArrayAsync().Result, "application/octet-stream", "PRREP11_" + DateTime.Now.ToString("dd_MM_yyyy") + ".pdf");
                        }
                        else
                        {
                            message = "Code : " + StatusCode + " | Description : " + StatusDesc;
                            return View();
                        }
                        //return responsse;
                    }
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        #endregion

        #region REPORT FOR TRANSFER
        public IActionResult ReportForTransfer()
        {
            string message = "";
            try
            {

            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        [HttpPost]
        public IActionResult ReportForTransfer(string RFT_BRANCH, string RFT_START_DATE, string RFT_END_DATE)
        {
            string message = "";
            try
            {
                HttpResponseMessage file = new HttpResponseMessage();
                PRREP13Model entity = new PRREP13Model();
                entity.Branch = RFT_BRANCH;
                entity.StartDate = RFT_START_DATE != null ? Convert.ToDateTime(RFT_START_DATE) : null;
                entity.EndDate = RFT_END_DATE != null ? Convert.ToDateTime(RFT_END_DATE) : null;
                var DomainName = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetSection("CHRIS_REPORTING")["API_URL"];
                string BASE_URL = DomainName + "api/PersonnelReporting/ReportForTransfer/";
                using (HttpClient clienFNREP08 = new HttpClient())
                {
                    using (HttpRequestMessage req310 = new HttpRequestMessage(HttpMethod.Post, BASE_URL))
                    {
                        req310.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        string dataAsJson = System.Text.Json.JsonSerializer.Serialize(entity,
                        new JsonSerializerOptions() { WriteIndented = false });
                        message += dataAsJson + Environment.NewLine;
                        var content = new StringContent(dataAsJson, Encoding.UTF8, "application/json");
                        req310.Content = content;
                        req310.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                        HttpResponseMessage response = clienFNREP08.SendAsync(req310).Result;
                        int StatusCode = (int)response.StatusCode;
                        string StatusDesc = response.StatusCode.ToString();
                        if (StatusCode == 200)
                        {
                            file = response;
                            return File(file.Content.ReadAsByteArrayAsync().Result, "application/octet-stream", "PRREP13_" + DateTime.Now.ToString("dd_MM_yyyy") + ".pdf");
                        }
                        else
                        {
                            message = "Code : " + StatusCode + " | Description : " + StatusDesc;
                            return View();
                        }
                        //return responsse;
                    }
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        #endregion

        #region LEAVES BALANCE REPORT
        public IActionResult LeavesBalanceReport()
        {
            string message = "";
            try
            {

            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        [HttpPost]
        public IActionResult LeavesBalanceReport(string LBR_DATE, string LBR_YEAR, string LBR_GROUP, string LBR_BRANCH, string LBR_SEGMENT)
        {
            string message = "";
            try
            {
                HttpResponseMessage file = new HttpResponseMessage();
                PRREP16Model entity = new PRREP16Model();
                entity.Branch = LBR_BRANCH;
                entity.Group = LBR_GROUP;
                entity.Segment = LBR_SEGMENT;
                entity.Date = LBR_DATE != null ? Convert.ToDateTime(LBR_DATE) : null;
                entity.Year = LBR_YEAR != null ? Convert.ToDateTime(LBR_YEAR) : null;
                var DomainName = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetSection("CHRIS_REPORTING")["API_URL"];
                string BASE_URL = DomainName + "api/PersonnelReporting/LeavesBalanceReport/";
                using (HttpClient clienFNREP08 = new HttpClient())
                {
                    using (HttpRequestMessage req310 = new HttpRequestMessage(HttpMethod.Post, BASE_URL))
                    {
                        req310.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        string dataAsJson = System.Text.Json.JsonSerializer.Serialize(entity,
                        new JsonSerializerOptions() { WriteIndented = false });
                        message += dataAsJson + Environment.NewLine;
                        var content = new StringContent(dataAsJson, Encoding.UTF8, "application/json");
                        req310.Content = content;
                        req310.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                        HttpResponseMessage response = clienFNREP08.SendAsync(req310).Result;
                        int StatusCode = (int)response.StatusCode;
                        string StatusDesc = response.StatusCode.ToString();
                        if (StatusCode == 200)
                        {
                            file = response;
                            return File(file.Content.ReadAsByteArrayAsync().Result, "application/octet-stream", "PRREP16_" + DateTime.Now.ToString("dd_MM_yyyy") + ".pdf");
                        }
                        else
                        {
                            message = "Code : " + StatusCode + " | Description : " + StatusDesc;
                            return View();
                        }
                        //return responsse;
                    }
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        #endregion

        #region LEAVES SUMMARY REPORT
        public IActionResult LeavesSummaryReport()
        {
            string message = "";
            try
            {

            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        [HttpPost]
        public IActionResult LeavesSummaryReport(string LSR_DATE, string LSR_YEAR, string LSR_DEPARTMENT, string LSR_BRANCH, string LSR_SEGMENT)
        {
            string message = "";
            try
            {
                HttpResponseMessage file = new HttpResponseMessage();
                PRREP16Model entity = new PRREP16Model();
                entity.Branch = LSR_BRANCH;
                entity.Group = LSR_DEPARTMENT;
                entity.Segment = LSR_SEGMENT;
                entity.Date = LSR_DATE != null ? Convert.ToDateTime(LSR_DATE) : null;
                entity.Year = LSR_YEAR != null ? Convert.ToDateTime(LSR_YEAR) : null;
                var DomainName = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetSection("CHRIS_REPORTING")["API_URL"];
                string BASE_URL = DomainName + "api/PersonnelReporting/LeavesSummaryReport/";
                using (HttpClient clienFNREP08 = new HttpClient())
                {
                    using (HttpRequestMessage req310 = new HttpRequestMessage(HttpMethod.Post, BASE_URL))
                    {
                        req310.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        string dataAsJson = System.Text.Json.JsonSerializer.Serialize(entity,
                        new JsonSerializerOptions() { WriteIndented = false });
                        message += dataAsJson + Environment.NewLine;
                        var content = new StringContent(dataAsJson, Encoding.UTF8, "application/json");
                        req310.Content = content;
                        req310.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                        HttpResponseMessage response = clienFNREP08.SendAsync(req310).Result;
                        int StatusCode = (int)response.StatusCode;
                        string StatusDesc = response.StatusCode.ToString();
                        if (StatusCode == 200)
                        {
                            file = response;
                            return File(file.Content.ReadAsByteArrayAsync().Result, "application/octet-stream", "PRREP16_" + DateTime.Now.ToString("dd_MM_yyyy") + ".pdf");
                        }
                        else
                        {
                            message = "Code : " + StatusCode + " | Description : " + StatusDesc;
                            return View();
                        }
                        //return responsse;
                    }
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        #endregion

        #region REFERENCE LETTER
        public IActionResult ReferenceLetter()
        {
            string message = "";
            try
            {

            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        [HttpPost]
        public IActionResult ReferenceLetter(string RL_PNO, string RL_AUTH_NAME, string RL_AUTH_DESG, string RL_AUTH_PLACE)
        {
            string message = "";
            try
            {
                HttpResponseMessage file = new HttpResponseMessage();
                PRREP23Model entity = new PRREP23Model();
                entity.PNO = RL_PNO != null ? Convert.ToInt32(RL_PNO) : null;
                entity.AuthName = RL_AUTH_NAME;
                entity.AuthDescription = RL_AUTH_DESG;
                entity.AuthPlace = RL_AUTH_PLACE;
                var DomainName = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetSection("CHRIS_REPORTING")["API_URL"];
                string BASE_URL = DomainName + "api/PersonnelReporting/ReferenceLetter/";
                using (HttpClient clienFNREP08 = new HttpClient())
                {
                    using (HttpRequestMessage req310 = new HttpRequestMessage(HttpMethod.Post, BASE_URL))
                    {
                        req310.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        string dataAsJson = System.Text.Json.JsonSerializer.Serialize(entity,
                        new JsonSerializerOptions() { WriteIndented = false });
                        message += dataAsJson + Environment.NewLine;
                        var content = new StringContent(dataAsJson, Encoding.UTF8, "application/json");
                        req310.Content = content;
                        req310.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                        HttpResponseMessage response = clienFNREP08.SendAsync(req310).Result;
                        int StatusCode = (int)response.StatusCode;
                        string StatusDesc = response.StatusCode.ToString();
                        if (StatusCode == 200)
                        {
                            file = response;
                            return File(file.Content.ReadAsByteArrayAsync().Result, "application/octet-stream", "PRREP23_" + DateTime.Now.ToString("dd_MM_yyyy") + ".pdf");
                        }
                        else
                        {
                            message = "Code : " + StatusCode + " | Description : " + StatusDesc;
                            return View();
                        }
                        //return responsse;
                    }
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        #endregion

        #region PREVIOUS EMP LETTER
        public IActionResult PreviousEmpLetter()
        {
            string message = "";
            try
            {

            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        [HttpPost]
        public IActionResult PreviousEmpLetter(string PEL_PNO, string PEL_AUTH_NAME, string PEL_AUTH_DESIG, string PEL_AUTH_PLACE)
        {
            string message = "";
            try
            {
                HttpResponseMessage file = new HttpResponseMessage();
                PRREP22Model entity = new PRREP22Model();
                entity.PNO = PEL_PNO != null ? Convert.ToDecimal(PEL_PNO) : null;
                entity.AuthName = PEL_AUTH_NAME;
                entity.AuthDesig = PEL_AUTH_DESIG;
                entity.AuthPlace = PEL_AUTH_PLACE;
                var DomainName = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetSection("CHRIS_REPORTING")["API_URL"];
                string BASE_URL = DomainName + "api/PersonnelReporting/PreviousEmpLetter/";
                using (HttpClient clienFNREP08 = new HttpClient())
                {
                    using (HttpRequestMessage req310 = new HttpRequestMessage(HttpMethod.Post, BASE_URL))
                    {
                        req310.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        string dataAsJson = System.Text.Json.JsonSerializer.Serialize(entity,
                        new JsonSerializerOptions() { WriteIndented = false });
                        message += dataAsJson + Environment.NewLine;
                        var content = new StringContent(dataAsJson, Encoding.UTF8, "application/json");
                        req310.Content = content;
                        req310.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                        HttpResponseMessage response = clienFNREP08.SendAsync(req310).Result;
                        int StatusCode = (int)response.StatusCode;
                        string StatusDesc = response.StatusCode.ToString();
                        if (StatusCode == 200)
                        {
                            file = response;
                            return File(file.Content.ReadAsByteArrayAsync().Result, "application/octet-stream", "PRREP22_" + DateTime.Now.ToString("dd_MM_yyyy") + ".pdf");
                        }
                        else
                        {
                            message = "Code : " + StatusCode + " | Description : " + StatusDesc;
                            return View();
                        }
                        //return responsse;
                    }
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        #endregion

        #region PERSONNEL INFO REPORT
        public IActionResult PersonnelInfo()
        {
            string message = "";
            try
            {

            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        [HttpPost]
        public IActionResult PersonnelInfo(string PI_BRANCH, string PI_PERSONNEL_NO, string PI_END_PERSONNEL_NO, string PI_DEPARTMENT, string PI_SEGMENT, string PI_LEVEL, string PI_YEAR)
        {
            string message = "";
            try
            {
                HttpResponseMessage file = new HttpResponseMessage();
                PRREP35Model entity = new PRREP35Model();
                entity.Branch = PI_BRANCH;
                entity.SPNO = PI_PERSONNEL_NO != null ? Convert.ToInt32(PI_PERSONNEL_NO) : null;
                entity.TPNO = PI_END_PERSONNEL_NO != null ? Convert.ToInt32(PI_END_PERSONNEL_NO) : null;
                entity.Segment = PI_SEGMENT;
                entity.Department = PI_DEPARTMENT;
                entity.Level = PI_LEVEL;
                entity.Year = PI_YEAR != null ? Convert.ToInt32(PI_YEAR) : null;
                var DomainName = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetSection("CHRIS_REPORTING")["API_URL"];
                string BASE_URL = DomainName + "api/PersonnelReporting/PersonnelInfo/";
                using (HttpClient clienFNREP08 = new HttpClient())
                {
                    using (HttpRequestMessage req310 = new HttpRequestMessage(HttpMethod.Post, BASE_URL))
                    {
                        req310.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        string dataAsJson = System.Text.Json.JsonSerializer.Serialize(entity,
                        new JsonSerializerOptions() { WriteIndented = false });
                        message += dataAsJson + Environment.NewLine;
                        var content = new StringContent(dataAsJson, Encoding.UTF8, "application/json");
                        req310.Content = content;
                        req310.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                        HttpResponseMessage response = clienFNREP08.SendAsync(req310).Result;
                        int StatusCode = (int)response.StatusCode;
                        string StatusDesc = response.StatusCode.ToString();
                        if (StatusCode == 200)
                        {
                            file = response;
                            return File(file.Content.ReadAsByteArrayAsync().Result, "application/octet-stream", "PRREP35_" + DateTime.Now.ToString("dd_MM_yyyy") + ".pdf");
                        }
                        else
                        {
                            message = "Code : " + StatusCode + " | Description : " + StatusDesc;
                            return View();
                        }
                        //return responsse;
                    }
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        #endregion

        #region ACADEMIC QUALIFICATION REPORT
        public IActionResult AcademicQualificationReport()
        {
            string message = "";
            try
            {

            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        [HttpPost]
        public IActionResult AcademicQualificationReport(string AQR_TODAY_DATE, string AQR_BRANCH, string AQR_SEGMENT, string AQR_DESIG, string AQR_YEAR)
        {
            string message = "";
            try
            {
                HttpResponseMessage file = new HttpResponseMessage();
                PRREP32Model entity = new PRREP32Model();
                entity.Branch = AQR_BRANCH;
                entity.Segment = AQR_SEGMENT;
                entity.Desig = AQR_DESIG;
                entity.Year = AQR_YEAR != null ? Convert.ToInt32(AQR_YEAR) : null;
                entity.TodayDate = AQR_TODAY_DATE != null ? Convert.ToDateTime(AQR_TODAY_DATE) : null;
                var DomainName = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetSection("CHRIS_REPORTING")["API_URL"];
                string BASE_URL = DomainName + "api/PersonnelReporting/AcademicQualificationReport/";
                using (HttpClient clienFNREP08 = new HttpClient())
                {
                    using (HttpRequestMessage req310 = new HttpRequestMessage(HttpMethod.Post, BASE_URL))
                    {
                        req310.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        string dataAsJson = System.Text.Json.JsonSerializer.Serialize(entity,
                        new JsonSerializerOptions() { WriteIndented = false });
                        message += dataAsJson + Environment.NewLine;
                        var content = new StringContent(dataAsJson, Encoding.UTF8, "application/json");
                        req310.Content = content;
                        req310.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                        HttpResponseMessage response = clienFNREP08.SendAsync(req310).Result;
                        int StatusCode = (int)response.StatusCode;
                        string StatusDesc = response.StatusCode.ToString();
                        if (StatusCode == 200)
                        {
                            file = response;
                            return File(file.Content.ReadAsByteArrayAsync().Result, "application/octet-stream", "PRREP32_" + DateTime.Now.ToString("dd_MM_yyyy") + ".pdf");
                        }
                        else
                        {
                            message = "Code : " + StatusCode + " | Description : " + StatusDesc;
                            return View();
                        }
                        //return responsse;
                    }
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        #endregion
        public IActionResult CITI_EMPAD()
        {
            return View();
        }
        public IActionResult ConfirmationMemo()
        {
            return View();
        }
        public IActionResult DepartmentWisePayroll_Clerical()
        {
            return View();
        }
        public IActionResult PersonalOnePagerReport()
        {
            return View();
        }
        public IActionResult SegmentWiseReportold()
        {
            return View();
        }
    }
}
