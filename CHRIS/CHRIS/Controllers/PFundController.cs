﻿using CHRIS.Common;
using CHRIS.Data;
using CHRIS.Models;
using CHRIS.Services;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Data;

namespace CHRIS.Controllers
{
    public class PFundController : Controller
    {
        public IActionResult CitiWiseProvidentFundBalances()
        {
            return View();
        }
        public IActionResult EmployeesProvidentFundLetter()
        {
            return View();
        }
        public IActionResult IndividualProvidentFundBalances()
        {
            return View();
        }


        #region MonthlyPFUpdate

        public IActionResult MonthlyPFUpdate()
        {
            return View();
        }

       
        public ActionResult RunMonthlyPFUpdate(string PR_P_NO, DateTime? PAYROLL_DATE)
        {
            Dictionary<string, object> objVals = new Dictionary<string, object>();
            DataTable dtPF = new DataTable();
            //DataSet ds = new DataSet();
            Result rsltPFund;
            CmnDataManager cmnDM = new CmnDataManager();
            DataSet ds = new DataSet();

            string txtPersonnelNo2 = "";
            string txtPFAREARS = "";
            string txtPFund = "";

            objVals.Clear();
            if (PR_P_NO == "")
            {
                PR_P_NO = null;
            }

            objVals.Add("PR_P_NO", PR_P_NO);
            objVals.Add("PAYROLL_DATE", PAYROLL_DATE);


            ds.Tables.Add(SQLManager.GetSPParams("CHRIS_SP_PROVIDENTFUNDUPDATE_MANAGER"));
            cmnDM.PopulateDataTableFromObject(ds.Tables[0], "CALCULATION", objVals);
            rsltPFund = SQLManager.SelectDataSet(ds, 20000);


            if (rsltPFund.isSuccessful)
            {
                if (rsltPFund.dstResult != null && rsltPFund.dstResult.Tables.Count > 0 && rsltPFund.dstResult.Tables[0].Rows.Count > 0)
                {
                    dtPF = rsltPFund.dstResult.Tables[0];
                }
                if (dtPF.Rows.Count > 0)
                {

                    txtPersonnelNo2 = dtPF.Rows[0]["PNUM"] == string.Empty ? "0" : Convert.ToString(dtPF.Rows[0]["PNUM"]);
                    txtPFAREARS = dtPF.Rows[0]["PFAR"] == string.Empty ? "0" : Convert.ToString(dtPF.Rows[0]["PFAR"]);
                    txtPFund  = dtPF.Rows[0]["PFND"] == string.Empty ? "0" : Convert.ToString(dtPF.Rows[0]["PFND"]);
                    if (txtPersonnelNo2 != "0" || txtPersonnelNo2 != "")
                    {
                        MessageBox = "Process Has Been Completed...";

                        return Json(MessageBox);
                    }
                }
                else
                {
                    return Json(null);
                }
            }
            return Json(null);
        }

       
        #endregion
        public IActionResult PersonalProvidentFundBalance()
        {
            return View();
        }

        #region PFContributionUpdate

        public IActionResult PFContributionUpdate()
        {
            return View();
        }

        [HttpPost]
        public JsonResult PFContributionUpdateCheckName(int? PNO, string action)
        {
            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("PFD_PR_P_NO", PNO);
            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_PFUND_DETAIL_MANAGER", action, d);
            if (rsltCode.isSuccessful)
            {
                if (rsltCode.dstResult.Tables.Count > 0)
                {
                    if (rsltCode.dstResult.Tables[0].Rows.Count > 0)
                    {
                        DataTable firstTable = rsltCode.dstResult.Tables[0];
                        var test = firstTable.AsEnumerable().Last();
                        var CatResul = JsonConvert.SerializeObject(firstTable);
                        return Json(CatResul);
                    }
                }

            }
            return Json(null);
        }


        [HttpPost]
        public JsonResult PFContributionUpdateList(int? PNO, string year, string action)
        {
            Dictionary<string, object> d = new Dictionary<string, object>();
            d.Add("PFD_PR_P_NO", PNO);
            d.Add("dummy_year", year);
            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_PFUND_DETAIL_MANAGER", action, d);
            if (rsltCode.isSuccessful)
            {
                if (rsltCode.dstResult.Tables.Count > 0)
                {
                    if (rsltCode.dstResult.Tables[0].Rows.Count > 0)
                    {
                        DataTable firstTable = rsltCode.dstResult.Tables[0];
                        var test = firstTable.AsEnumerable().Last();
                        var CatResul = JsonConvert.SerializeObject(firstTable);
                        return Json(CatResul);
                    }
                }

            }
            return Json(null);
        }

        [HttpPost]
        public ViewResponseModel AddPFContributionUpdate(CHRIS_SP_PFUND_DETAIL_GETALLResult model)
        {
            ViewResponseModel response = new ViewResponseModel();
            try
            {
                CHRIS_Context ic = new CHRIS_Context();
                var context = new CHRIS_ContextProcedures(ic);
                OutputParameter<int?> id = new OutputParameter<int?>();
                OutputParameter<int?> returnVal = new OutputParameter<int?>();

                var result = context.CHRIS_SP_PFUND_DETAIL_ADDAsync(model.PFD_PR_P_NO, model.PFD_DATE, model.PFD_PFUND, model.PFD_PF_AREAR, model.PFD_FLAG, id, returnVal);

                result.Wait();

                if (returnVal != null && returnVal.Value > 0)
                {
                    response.responseCode = Constants.ResponseCode.success;
                    response.responseMessage = "Record has been inserted successfully";
                }
                else if (returnVal != null && returnVal.Value == -40)
                {
                    response.responseCode = Constants.ResponseCode.error;
                    response.responseMessage = "No record inserted";
                }
                else
                {
                    response.responseCode = Constants.ResponseCode.error;
                    response.responseMessage = "Error Inserting Record";
                }


                ic.Dispose();
                context = null;
            }
            catch (Exception ex)
            {
                response.responseCode = Constants.ResponseCode.error;
                response.responseMessage = ex.InnerException.Message;
            }
            return response;
        }

        [HttpPost]
        public ViewResponseModel DeletePFContributionUpdate(string id)
        {
            ViewResponseModel response = new ViewResponseModel();
            try
            {
                CHRIS_Context ic = new CHRIS_Context();
                var context = new CHRIS_ContextProcedures(ic);
                OutputParameter<int?> returnVal = new OutputParameter<int?>();


                int tmpID = 0;
                int? sID = int.TryParse(id, out tmpID) ?
                  tmpID : (int?)null;

                var result = context.CHRIS_SP_PFUND_DETAIL_DELETEAsync(sID, returnVal);

                result.Wait();

                if (returnVal != null && returnVal.Value == -10)
                {
                    response.responseCode = Constants.ResponseCode.success;
                    response.responseMessage = "Record Deleted Succesfully";
                }
                else if (returnVal != null && returnVal.Value == -40)
                {
                    response.responseCode = Constants.ResponseCode.error;
                    response.responseMessage = "Record not found";
                }
                else
                {
                    response.responseCode = Constants.ResponseCode.error;
                    response.responseMessage = "Error deleting Record";
                }


                ic.Dispose();
                context = null;
            }
            catch (Exception ex)
            {
                response.responseCode = Constants.ResponseCode.error;
                response.responseMessage = ex.InnerException.Message;
            }
            return response;
        }

        [HttpPost]
        public ViewResponseModel UpdatePFContributionUpdate(CHRIS_SP_PFUND_DETAIL_GETALLResult model)
        {
            ViewResponseModel response = new ViewResponseModel();
            try
            {
                CHRIS_Context ic = new CHRIS_Context();
                var context = new CHRIS_ContextProcedures(ic);
                OutputParameter<int?> returnVal = new OutputParameter<int?>();



                var result = context.CHRIS_SP_PFUND_DETAIL_UPDATEAsync(model.PFD_PR_P_NO, model.PFD_DATE, model.PFD_PFUND, model.PFD_PF_AREAR, model.PFD_FLAG, model.ID, returnVal);

                result.Wait();

                if (returnVal != null && returnVal.Value == -10)
                {
                    response.responseCode = Constants.ResponseCode.success;
                    response.responseMessage = "Record updated successfully";
                }
                else
                {
                    response.responseCode = Constants.ResponseCode.error;
                    response.responseMessage = "Error Updating Record";
                }


                ic.Dispose();
                context = null;
            }
            catch (Exception ex)
            {
                response.responseCode = Constants.ResponseCode.error;
                response.responseMessage = ex.InnerException.Message;
            }
            return response;

        }
        #endregion

        #region PFundEntry

        public IActionResult PFundEntry()
        {
            return View();
        }

        [HttpPost]
        public JsonResult PFEntryFetchLists(decimal? PF_PR_P_NO, string? PF_YEAR, string action)
        {
            Dictionary<string, object> d = new Dictionary<string, object>();
            if (action == "Year")
            {
                d.Add("searchfilter", PF_PR_P_NO);
            }
            else if (action == "Execute_Query")
            {
                d.Add("PF_PR_P_NO", PF_PR_P_NO);
                d.Add("PF_YEAR", PF_YEAR);
            }

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_PFUND_PFundEntry_MANAGER", action, d);
            if (rsltCode.isSuccessful)
            {
                if (rsltCode.dstResult.Tables.Count > 0)
                {
                    if (rsltCode.dstResult.Tables[0].Rows.Count > 0)
                    {
                        DataTable firstTable = rsltCode.dstResult.Tables[0];
                        var test = firstTable.AsEnumerable().Last();
                        var CatResul = JsonConvert.SerializeObject(firstTable);
                        return Json(CatResul);
                    }
                }

            }
            return Json(null);
        }

        public JsonResult PFEntryValueUpdate(decimal PF_PR_P_NO, string PF_YEAR, decimal PF_EMP_CONT, decimal PF_BNK_CONT, decimal PF_INTEREST, decimal PF_PREMIUM, int ID)
        {
            try
            {
                Dictionary<string, object> d = new Dictionary<string, object>();

                d.Add("PF_PR_P_NO", PF_PR_P_NO);
                d.Add("PF_YEAR", PF_YEAR);
                d.Add("PF_EMP_CONT", PF_EMP_CONT);
                d.Add("PF_BNK_CONT", PF_BNK_CONT);
                d.Add("PF_INTEREST", PF_INTEREST);
                d.Add("PF_PREMIUM", PF_PREMIUM);
                d.Add("ID", ID);

                CmnDataManager objCmnDataManager = new CmnDataManager();
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_PFUND_PFundEntry_MANAGER", "Update", d);
                if (rsltCode.isSuccessful)
                {
                    var res = Json(rsltCode);
                    res = Json("Data Updated Successfully");
                    return Json(res);
                }

                return Json(null);
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        #endregion

        #region PFundInterestEntry

        public IActionResult PFundInterestEntry()
        {

            return View();
        }

        public JsonResult PFInterestEntryList(string action)
        {
            Dictionary<string, object> d = new Dictionary<string, object>();

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_PFUND_INTEREST_MANAGER", action, d);
            if (rsltCode.isSuccessful)
            {
                if (rsltCode.dstResult.Tables.Count > 0)
                {
                    if (rsltCode.dstResult.Tables[0].Rows.Count > 0)
                    {
                        DataTable firstTable = rsltCode.dstResult.Tables[0];
                        var test = firstTable.AsEnumerable().Last();
                        var CatResul = JsonConvert.SerializeObject(firstTable);
                        return Json(CatResul);
                    }
                }

            }
            return Json(null);
        }


        public JsonResult PFInterestEntryUpdate(int PFI_YEAR, decimal PFI_RATE, decimal PFI_RES_RATE, int ID)
        {
            try
            {
                Dictionary<string, object> d = new Dictionary<string, object>();

                d.Add("PFI_YEAR", PFI_YEAR);
                d.Add("PFI_RATE", PFI_RATE);
                d.Add("PFI_RES_RATE", PFI_RES_RATE);
                d.Add("ID", ID);

                CmnDataManager objCmnDataManager = new CmnDataManager();
                if (ID > 0)
                {

                    Result rsltCode = objCmnDataManager.GetData("CHRIS_PFUND_INTEREST_MANAGER", "Update", d);
                    if (rsltCode.isSuccessful)
                    {

                        var res = Json(rsltCode);
                        res = Json("Data Updated Successfully");

                        return Json(res);

                    }
                }
                else
                {
                    Result rsltCode = objCmnDataManager.GetData("CHRIS_PFUND_INTEREST_MANAGER", "Save", d);
                    if (rsltCode.isSuccessful)
                    {

                        var res = Json(rsltCode);
                        res = Json("Data Inserted Successfully");
                        return Json(res);

                    }
                }

                return Json(null);
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public ViewResponseModel DeletePFInterestEntry(string id)
        {
            ViewResponseModel response = new ViewResponseModel();
            try
            {
                CHRIS_Context ic = new CHRIS_Context();
                var context = new CHRIS_ContextProcedures(ic);
                OutputParameter<int?> returnVal = new OutputParameter<int?>();


                int tmpID = 0;
                int? sID = int.TryParse(id, out tmpID) ?
                  tmpID : (int?)null;

                var result = context.CHRIS_PFUND_INTEREST_DELETEAsync(sID, returnVal);

                result.Wait();

                if (returnVal != null && returnVal.Value == -10)
                {
                    response.responseCode = Constants.ResponseCode.success;
                    response.responseMessage = "Record Deleted Succesfully";
                }
                else if (returnVal != null && returnVal.Value == -40)
                {
                    response.responseCode = Constants.ResponseCode.error;
                    response.responseMessage = "Record not found";
                }
                else
                {
                    response.responseCode = Constants.ResponseCode.error;
                    response.responseMessage = "Error deleting Record";
                }


                ic.Dispose();
                context = null;
            }
            catch (Exception ex)
            {
                response.responseCode = Constants.ResponseCode.error;
                response.responseMessage = ex.InnerException.Message;
            }
            return response;
        }

        #endregion

        #region PFundQuery

        public IActionResult PFundQuery()
        {
            return View();
        }

        CmnDataManager CnmDM = new CmnDataManager();
        Result rslt;
        Dictionary<string, object> param = new Dictionary<string, object>();

        #region variables
        bool ValidateProc1 = false;
        double pf_rate = 0;
        double PF_TOTAL = 0;
        double INS_YEAR_TOT = 0;
        double EMP_YEAR_TOT = 0;
        double BNK_YEAR_TOT = 0;
        double F_PREMIUM = 0;
        // EMP_TOT,BNK_TOT,INS_TOT,PF_INTEREST,PF_BNK_CONT,PF_EMP_CONT,PF_TOT,INS_TOT,BNK_TOT,EMP_TOT,PF_YEAR_TOT
        double EMP_TOT = 0;
        double INS_TOT = 0;
        double PF_INTEREST = 0;
        double PF_BNK_CONT = 0;
        double PF_EMP_CONT = 0;
        double PF_TOT = 0;
        double BNK_TOT = 0;
        double PF_YEAR_TOT = 0;
        string pfundQry = "0";
        int count = 0;


        string txtjan_bnk = "";
        string txtfeb_bnk = "";
        string txtMar_bnk = "";
        string txtapr_bnk = "";
        string txtMay_bnk = "";
        string txtjun_bnk = "";
        string txtjul_bnk = "";
        string txtaug_bnk = "";
        string txtsep_bnk = "";
        string txtoct_bnk = "";
        string txtNov_bnk = "";
        string txtdec_bnk = "";

        string txtJan_emp = "";
        string txtfeb_emp = "";
        string txtMar_emp = "";
        string txtApr_emp = "";
        string txtMay_emp = "";
        string txtJun_emp = "";
        string txtJul_emp = "";
        string txtaug_emp = "";
        string txtsep_emp = "";
        string txtoct_emp = "";
        string txtNov_emp = "";
        string txtDec_emp = "";


        string txtyearEmp = "";
        string txtyearBnk = "";
        string MessageBox = "";

        string txtPFbalnce = "";
        string txtPfBankcont = "";
        string txtAccIntrst = "";
        string txtToatal = "";
        string txtPremium = "";
        string txtyearPF = "";
        string txtyearIns = "";
        string txtTotalEmp = "";
        string txtTotBnk = "";
        string txtTotIns = "";
        string txtTotalPF = "";
        string txtBranch = "";
        string txtBalance = "";

        #endregion

        public ActionResult btn_PNo_PFQ()
        {
            Dictionary<string, object> d = new Dictionary<string, object>();

            CmnDataManager objCmnDataManager = new CmnDataManager();
            Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_PFUND_MANAGER", "prpNo", d);
            if (rsltCode.isSuccessful)
            {
                DataTable firstTable = rsltCode.dstResult.Tables[0];
                var CatResul = JsonConvert.SerializeObject(firstTable);
                return Json(CatResul);
            }
            return Json(null);
        }

        public ActionResult btn_Cal_PFQ(DateTime? fromDate, DateTime? ToDate, string PR_Emp_No)
        {
            string slTxtBxTotalAmount = "";
            string slTxtBxEmployerShare = "";
            string slTxtBxEmpShare = "";

            DateTime fromDate1 = Convert.ToDateTime(fromDate);
            DateTime ToDate1 = Convert.ToDateTime(ToDate);
            if (!string.IsNullOrEmpty(PR_Emp_No))
            {
                DataTable dt = new DataTable();
                dt = SQLManager.exPFund(fromDate1, ToDate1, PR_Emp_No).Tables[0];
                string EmpShare = string.Empty;
                string EmplyerShare = string.Empty;


                if (dt.Rows.Count < 1)
                {
                    MessageBox = "No Record Found";
                    return Json(MessageBox);
                }

                foreach (DataRow rowCust in dt.Rows)
                {
                    EmplyerShare = string.IsNullOrEmpty(rowCust["PF_BNK_CONT"].ToString().Trim()) ? "0" : rowCust["PF_BNK_CONT"].ToString().Trim();
                    EmpShare = string.IsNullOrEmpty(rowCust["PF_EMP_CONT"].ToString().Trim()) ? "0" : rowCust["PF_EMP_CONT"].ToString().Trim();

                    double emp = Convert.ToDouble(EmplyerShare);
                    double bank = Convert.ToDouble(EmpShare);
                    slTxtBxTotalAmount = Convert.ToString((emp + bank));
                    slTxtBxEmployerShare = EmplyerShare;
                    slTxtBxEmpShare = EmpShare;

                }
                var res = new
                {
                    slTxtBxTotalAmount = slTxtBxTotalAmount,
                    slTxtBxEmployerShare = slTxtBxEmployerShare,
                    slTxtBxEmpShare = slTxtBxEmpShare,
                    MessageBox = MessageBox,
                };

                return Json(res);
            }
            else
            {
                MessageBox = "No Record Found";
                return Json(MessageBox);
            }
            return Json(null);
        }
        private void proc1(Decimal? PR_P_NO, DateTime? dtww_date)
        {
            BNK_YEAR_TOT = 0;
            EMP_YEAR_TOT = 0;

            int month;
            Result rslt;
            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Add("pf_pr_p_no", PR_P_NO);
            param.Add("ww_date", Convert.ToDateTime(dtww_date).ToShortDateString());
            rslt = CnmDM.GetData("CHRIS_SP_PFUND_MANAGER", "Proc_1", param);
            int index = 0;
            if (rslt.isSuccessful)
            {
                if (rslt.dstResult.Tables.Count > 0 && rslt.dstResult.Tables[0].Rows.Count > 0)
                {
                    int counter = rslt.dstResult.Tables[0].Rows.Count;
                    // foreach(counter cs in rslt.dstResult.Tables[0])
                    while (counter != 0)
                    {
                        month = Int32.Parse(rslt.dstResult.Tables[0].Rows[index].ItemArray[0].ToString());
                        switch (month)
                        {
                            case 1:

                                txtjan_bnk = "";
                                txtJan_emp = "";


                                txtjan_bnk = rslt.dstResult.Tables[0].Rows[0].ItemArray[1].ToString();
                                txtJan_emp = rslt.dstResult.Tables[0].Rows[0].ItemArray[1].ToString();
                                index = index + 1;
                                counter = counter - 1;
                                //EMP_YEAR_TOT = 0;
                                //BNK_YEAR_TOT = 0;
                                if (txtjan_bnk != "" && txtJan_emp != "")
                                {
                                    EMP_YEAR_TOT = EMP_YEAR_TOT + double.Parse(txtjan_bnk);
                                    BNK_YEAR_TOT = BNK_YEAR_TOT + double.Parse(txtJan_emp);
                                }
                                else if (txtjan_bnk == "" && txtJan_emp == "")
                                {
                                    EMP_YEAR_TOT = EMP_YEAR_TOT + double.Parse("0.00");
                                    BNK_YEAR_TOT = BNK_YEAR_TOT + double.Parse("0.00");
                                }
                                break;

                            case 2:

                                txtfeb_bnk = "";
                                txtfeb_emp = "";



                                txtfeb_bnk = rslt.dstResult.Tables[0].Rows[index].ItemArray[1].ToString();
                                txtfeb_emp = rslt.dstResult.Tables[0].Rows[index].ItemArray[1].ToString();
                                index = index + 1;
                                counter = counter - 1;
                                //EMP_YEAR_TOT = 0;
                                //BNK_YEAR_TOT = 0;
                                if (txtfeb_bnk != "" && txtfeb_emp != "")
                                {
                                    EMP_YEAR_TOT = EMP_YEAR_TOT + double.Parse(txtfeb_bnk);
                                    BNK_YEAR_TOT = BNK_YEAR_TOT + double.Parse(txtfeb_emp);
                                }
                                else if (txtfeb_bnk == "" && txtfeb_emp == "")
                                {
                                    EMP_YEAR_TOT = EMP_YEAR_TOT + double.Parse("0.00");
                                    BNK_YEAR_TOT = BNK_YEAR_TOT + double.Parse("0.00");
                                }
                                break;

                            case 3:

                                txtMar_bnk = "";
                                txtMar_emp = "";


                                txtMar_bnk = rslt.dstResult.Tables[0].Rows[index].ItemArray[1].ToString();
                                txtMar_emp = rslt.dstResult.Tables[0].Rows[index].ItemArray[1].ToString();
                                index = index + 1;
                                counter = counter - 1;
                                //EMP_YEAR_TOT = 0;
                                //BNK_YEAR_TOT = 0;
                                if (txtMar_bnk != "" && txtMar_emp != "")
                                {
                                    EMP_YEAR_TOT = EMP_YEAR_TOT + double.Parse(txtMar_bnk);
                                    BNK_YEAR_TOT = BNK_YEAR_TOT + double.Parse(txtMar_emp);
                                }
                                else if (txtMar_bnk == "" && txtMar_emp == "")
                                {
                                    EMP_YEAR_TOT = EMP_YEAR_TOT + double.Parse("0.00");
                                    BNK_YEAR_TOT = BNK_YEAR_TOT + double.Parse("0.00");
                                }
                                break;

                            case 4:

                                txtapr_bnk = "";
                                txtApr_emp = "";


                                txtapr_bnk = rslt.dstResult.Tables[0].Rows[index].ItemArray[1].ToString();
                                txtApr_emp = rslt.dstResult.Tables[0].Rows[index].ItemArray[1].ToString();
                                index = index + 1;
                                counter = counter - 1;
                                //EMP_YEAR_TOT = 0;
                                //BNK_YEAR_TOT = 0;

                                if (txtapr_bnk != "" && txtApr_emp != "")
                                {
                                    EMP_YEAR_TOT = EMP_YEAR_TOT + double.Parse(txtapr_bnk);
                                    BNK_YEAR_TOT = BNK_YEAR_TOT + double.Parse(txtApr_emp);
                                }
                                else if (txtapr_bnk == "" && txtApr_emp == "")
                                {
                                    EMP_YEAR_TOT = EMP_YEAR_TOT + double.Parse("0.00");
                                    BNK_YEAR_TOT = BNK_YEAR_TOT + double.Parse("0.00");
                                }

                                break;

                            case 5:

                                txtMay_bnk = "";
                                txtMay_emp = "";


                                txtMay_bnk = rslt.dstResult.Tables[0].Rows[index].ItemArray[1].ToString();
                                txtMay_emp = rslt.dstResult.Tables[0].Rows[index].ItemArray[1].ToString();
                                index = index + 1;
                                counter = counter - 1;
                                //EMP_YEAR_TOT = 0;
                                //BNK_YEAR_TOT = 0;
                                if (txtMay_bnk != "" && txtMay_emp != "")
                                {
                                    EMP_YEAR_TOT = EMP_YEAR_TOT + double.Parse(txtMay_bnk);
                                    BNK_YEAR_TOT = BNK_YEAR_TOT + double.Parse(txtMay_emp);
                                }
                                else if (txtMay_bnk == "" && txtMay_emp == "")
                                {
                                    EMP_YEAR_TOT = EMP_YEAR_TOT + double.Parse("0.00");
                                    BNK_YEAR_TOT = BNK_YEAR_TOT + double.Parse("0.00");
                                }
                                break;

                            case 6:

                                txtjun_bnk = "";
                                txtJun_emp = "";


                                txtjun_bnk = rslt.dstResult.Tables[0].Rows[index].ItemArray[1].ToString();
                                txtJun_emp = rslt.dstResult.Tables[0].Rows[index].ItemArray[1].ToString();
                                index = index + 1;
                                counter = counter - 1;
                                //EMP_YEAR_TOT = 0;
                                //BNK_YEAR_TOT = 0;
                                if (txtjun_bnk != "" && txtJun_emp != "")
                                {
                                    EMP_YEAR_TOT = EMP_YEAR_TOT + double.Parse(txtjun_bnk);
                                    BNK_YEAR_TOT = BNK_YEAR_TOT + double.Parse(txtJun_emp);
                                }
                                else if (txtjun_bnk == "" && txtJun_emp == "")
                                {
                                    EMP_YEAR_TOT = EMP_YEAR_TOT + double.Parse("0.00");
                                    BNK_YEAR_TOT = BNK_YEAR_TOT + double.Parse("0.00");
                                }
                                break;

                            case 7:

                                txtjul_bnk = "";
                                txtJul_emp = "";

                                txtjul_bnk = rslt.dstResult.Tables[0].Rows[index].ItemArray[1].ToString();
                                txtJul_emp = rslt.dstResult.Tables[0].Rows[index].ItemArray[1].ToString();
                                index = index + 1;
                                counter = counter - 1;
                                //EMP_YEAR_TOT = 0;
                                //BNK_YEAR_TOT = 0;

                                if (txtjul_bnk != "" && txtJul_emp != "")
                                {
                                    EMP_YEAR_TOT = EMP_YEAR_TOT + double.Parse(txtjul_bnk);
                                    BNK_YEAR_TOT = BNK_YEAR_TOT + double.Parse(txtJul_emp);
                                }
                                else if (txtjul_bnk == "" && txtJul_emp == "")
                                {
                                    EMP_YEAR_TOT = EMP_YEAR_TOT + double.Parse("0.00");
                                    BNK_YEAR_TOT = BNK_YEAR_TOT + double.Parse("0.00");
                                }
                                break;

                            case 8:

                                txtaug_bnk = "";
                                txtaug_emp = "";

                                txtaug_bnk = rslt.dstResult.Tables[0].Rows[index].ItemArray[1].ToString();
                                txtaug_emp = rslt.dstResult.Tables[0].Rows[index].ItemArray[1].ToString();
                                index = index + 1;
                                counter = counter - 1;
                                //EMP_YEAR_TOT = 0;
                                //BNK_YEAR_TOT = 0;

                                if (txtaug_bnk != "" && txtaug_emp != "")
                                {
                                    EMP_YEAR_TOT = EMP_YEAR_TOT + double.Parse(txtaug_bnk);
                                    BNK_YEAR_TOT = BNK_YEAR_TOT + double.Parse(txtaug_emp);
                                }
                                else if (txtaug_bnk == "" && txtaug_emp == "")
                                {
                                    EMP_YEAR_TOT = EMP_YEAR_TOT + double.Parse("0.00");
                                    BNK_YEAR_TOT = BNK_YEAR_TOT + double.Parse("0.00");
                                }
                                break;

                            case 9:

                                txtsep_bnk = "";
                                txtsep_emp = "";



                                txtsep_bnk = rslt.dstResult.Tables[0].Rows[index].ItemArray[1].ToString();
                                txtsep_emp = rslt.dstResult.Tables[0].Rows[index].ItemArray[1].ToString();
                                index = index + 1;
                                counter = counter - 1;
                                //EMP_YEAR_TOT = 0;
                                //BNK_YEAR_TOT = 0;
                                if (txtsep_bnk != "" && txtsep_emp != "")
                                {

                                    EMP_YEAR_TOT = EMP_YEAR_TOT + double.Parse(txtsep_bnk);
                                    BNK_YEAR_TOT = BNK_YEAR_TOT + double.Parse(txtsep_emp);
                                }
                                else if (txtsep_bnk == "" && txtsep_emp == "")
                                {
                                    EMP_YEAR_TOT = EMP_YEAR_TOT + double.Parse("0.00");
                                    BNK_YEAR_TOT = BNK_YEAR_TOT + double.Parse("0.00");
                                }
                                break;

                            case 10:

                                txtoct_bnk = "";
                                txtoct_emp = "";



                                txtoct_bnk = rslt.dstResult.Tables[0].Rows[index].ItemArray[1].ToString();
                                txtoct_emp = rslt.dstResult.Tables[0].Rows[index].ItemArray[1].ToString();
                                index = index + 1;
                                counter = counter - 1;
                                //EMP_YEAR_TOT = 0;
                                //BNK_YEAR_TOT = 0;

                                if (txtoct_bnk != "" && txtoct_bnk != "")
                                {
                                    EMP_YEAR_TOT = EMP_YEAR_TOT + double.Parse(txtoct_bnk);
                                    BNK_YEAR_TOT = BNK_YEAR_TOT + double.Parse(txtoct_emp);

                                }
                                else if (txtoct_bnk == "" && txtoct_bnk == "")
                                {
                                    EMP_YEAR_TOT = EMP_YEAR_TOT + double.Parse("0.00");
                                    BNK_YEAR_TOT = BNK_YEAR_TOT + double.Parse("0.00");
                                }
                                break;

                            case 11:

                                txtNov_bnk = "";
                                txtNov_emp = "";

                                txtNov_bnk = rslt.dstResult.Tables[0].Rows[index].ItemArray[1].ToString();
                                txtNov_emp = rslt.dstResult.Tables[0].Rows[index].ItemArray[1].ToString();
                                index = index + 1;
                                counter = counter - 1;
                                //EMP_YEAR_TOT = 0;
                                //BNK_YEAR_TOT = 0;

                                if (txtNov_bnk != "" && txtNov_emp != "")
                                {
                                    EMP_YEAR_TOT = EMP_YEAR_TOT + double.Parse(txtNov_bnk);
                                    BNK_YEAR_TOT = BNK_YEAR_TOT + double.Parse(txtNov_emp);

                                }
                                else if (txtNov_bnk == "" && txtNov_emp == "")
                                {
                                    EMP_YEAR_TOT = EMP_YEAR_TOT + double.Parse("0.00");
                                    BNK_YEAR_TOT = BNK_YEAR_TOT + double.Parse("0.00");
                                }
                                break;

                            case 12:

                                txtdec_bnk = "";
                                txtDec_emp = "";


                                txtdec_bnk = rslt.dstResult.Tables[0].Rows[index].ItemArray[1].ToString();
                                txtDec_emp = rslt.dstResult.Tables[0].Rows[index].ItemArray[1].ToString();
                                index = index + 1;
                                counter = counter - 1;
                                //EMP_YEAR_TOT = 0;
                                //BNK_YEAR_TOT = 0;

                                if (txtdec_bnk != "" && txtDec_emp != "")
                                {
                                    EMP_YEAR_TOT = EMP_YEAR_TOT + double.Parse(txtdec_bnk);
                                    BNK_YEAR_TOT = BNK_YEAR_TOT + double.Parse(txtDec_emp);

                                }
                                else if (txtdec_bnk == "" && txtDec_emp == "")
                                {
                                    EMP_YEAR_TOT = EMP_YEAR_TOT + double.Parse("0.00");
                                    BNK_YEAR_TOT = BNK_YEAR_TOT + double.Parse("0.00");
                                }
                                break;
                        }
                        txtyearEmp = EMP_YEAR_TOT.ToString();
                        txtyearBnk = BNK_YEAR_TOT.ToString();
                    }
                }
            }
        }

        public ActionResult PR_P_NO_Validation_PFQ(Decimal? PR_P_NO,DateTime? dtww_date)
        {
            bool chkprpno = true;//chkPersonnelNo();
            ValidateProc1 = true;
            if (ValidateProc1 && chkprpno)
            {
                PF_YEAR_TOT = 0;
                EMP_TOT = 0;
                BNK_TOT = 0;
                INS_TOT = 0;
                PF_TOT = 0;
                pf_rate = 0;
                PF_TOTAL = 0;
                INS_YEAR_TOT = 0;
                EMP_YEAR_TOT = 0;
                BNK_YEAR_TOT = 0;
                F_PREMIUM = 0;
                EMP_TOT = 0;
                INS_TOT = 0;
                PF_INTEREST = 0;
                PF_BNK_CONT = 0;
                PF_EMP_CONT = 0;
                PF_TOT = 0;
                BNK_TOT = 0;
                PF_YEAR_TOT = 0;

                if (PR_P_NO != null)
                {
                    param.Clear();
                    param.Add("PF_PR_P_NO", PR_P_NO);
                    rslt = CnmDM.GetData("CHRIS_SP_PFUND_MANAGER", "PP_VERIFY", param);
                    if (rslt.isSuccessful)
                    {
                        if (rslt.dstResult.Tables[0].Rows.Count == 0)
                        {
                            MessageBox = "Invalid Personnel No. Entered..Or Not Confirmed...";
                            
                        }
                    }
                }

                //this.txtPersonnel.KeyPress += new System.Windows.Forms.KeyPressEventHandler(CheckKeys);
                //-----------------------------------------------------//

                #region Key Next          

                if (PR_P_NO == null)
                {
                    return Json(null);
                }
               
                if (PF_TOTAL > 0)
                {
                    PF_EMP_CONT = 0;
                    PF_BNK_CONT = 0;
                    PF_INTEREST = 0;
                    PF_TOTAL = 0;
                    PF_EMP_CONT = 0;
                    PF_BNK_CONT = 0;
                    PF_INTEREST = 0;
                    F_PREMIUM = 0;
                }

                param.Clear();
                param.Add("PF_PR_P_NO", PR_P_NO);
                rslt = CnmDM.GetData("CHRIS_SP_PFUND_MANAGER", "Keynxt_PfYear", param);
                if (rslt.isSuccessful)
                {
                    if (rslt.dstResult.Tables.Count > 0 && rslt.dstResult.Tables[0].Rows.Count > 0)
                    {
                        string chknull = rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString();

                        if (chknull != string.Empty && chknull != "")
                        { dtww_date = Convert.ToDateTime(rslt.dstResult.Tables[0].Rows[0].ItemArray[0].ToString()); }
                        else
                        {
                            Result rslt4;
                            Dictionary<string, object> param4 = new Dictionary<string, object>();
                            param4.Add("PF_PR_P_NO", PR_P_NO);
                            rslt4 = CnmDM.GetData("CHRIS_SP_PFUND_MANAGER", "Keynxt_IfNull", param4);
                            if (rslt4.isSuccessful)
                            {
                                if (rslt4.dstResult.Tables.Count > 0 && rslt4.dstResult.Tables[0].Rows.Count > 0)
                                {

                                    string chk = rslt4.dstResult.Tables[0].Rows[0].ItemArray[0].ToString();

                                    if (chk != string.Empty && chk != "")
                                        dtww_date = Convert.ToDateTime(rslt4.dstResult.Tables[0].Rows[0].ItemArray[0].ToString());
                                }
                            }
                        }
                    }
                }

                Result rslt5;
                Dictionary<string, object> param5 = new Dictionary<string, object>();
                param5.Clear();
                param5.Add("PF_PR_P_NO", PR_P_NO);
                rslt5 = CnmDM.GetData("CHRIS_SP_PFUND_MANAGER", "GetYear", param5);
                if (rslt5.isSuccessful)
                {
                    if (rslt5.dstResult.Tables.Count > 0 && rslt5.dstResult.Tables[0].Rows.Count > 0)
                    {
                        string chknull = rslt5.dstResult.Tables[0].Rows[0].ItemArray[0].ToString();

                        if (chknull != "" && chknull != string.Empty)
                        { txtBalance = rslt5.dstResult.Tables[0].Rows[0].ItemArray[0].ToString(); }
                    }
                }

                #endregion

                Result rslt3;
                Dictionary<string, object> param3 = new Dictionary<string, object>();
                param3.Clear();
                param3.Add("pf_pr_p_no", PR_P_NO);
                rslt3 = CnmDM.GetData("CHRIS_SP_PFUND_MANAGER", "Execute_query", param3);

                if (rslt3.isSuccessful)
                {
                    if (rslt3.dstResult.Tables.Count > 0 && rslt3.dstResult.Tables[0].Rows.Count > 0)
                    {
                        txtPFbalnce = rslt3.dstResult.Tables[0].Rows[0].ItemArray[0].ToString();
                        txtPfBankcont = rslt3.dstResult.Tables[0].Rows[0].ItemArray[1].ToString();
                        txtAccIntrst = rslt3.dstResult.Tables[0].Rows[0].ItemArray[2].ToString();
                        PF_EMP_CONT = double.Parse(txtPFbalnce);
                        PF_BNK_CONT = double.Parse(txtPfBankcont);
                        PF_INTEREST = double.Parse(txtAccIntrst);
                        PF_TOTAL = PF_EMP_CONT + PF_BNK_CONT + PF_INTEREST;
                        txtToatal = PF_TOTAL.ToString();
                    }
                }

                proc1(PR_P_NO, dtww_date);

                Result rslt2;
                Dictionary<string, object> param2 = new Dictionary<string, object>();
                param2.Add("pf_pr_p_no", PR_P_NO);
                param2.Add("ww_date", Convert.ToDateTime(dtww_date).ToShortDateString());
                rslt2 = CnmDM.GetData("CHRIS_SP_PFUND_MANAGER", "KeyNxtf_Premium", param2);

                if (rslt2.isSuccessful)
                {
                    if (rslt2.dstResult.Tables.Count > 0 && rslt2.dstResult.Tables[0].Rows.Count > 0)
                    {
                        txtPremium = "";
                        txtPremium = rslt2.dstResult.Tables[0].Rows[0].ItemArray[0].ToString();
                        F_PREMIUM = double.Parse(txtPremium);
                    }
                }

                rslt2 = CnmDM.GetData("CHRIS_SP_PFUND_MANAGER", "KeyNxtf_Rate", param2);
                if (rslt2.isSuccessful)
                {
                    if (rslt2.dstResult.Tables.Count > 0 && rslt2.dstResult.Tables[0].Rows.Count > 0)
                    {
                        pf_rate = double.Parse(rslt2.dstResult.Tables[0].Rows[0].ItemArray[0].ToString());
                    }
                }

                if (pf_rate > 0)
                {
                    INS_YEAR_TOT = (((EMP_YEAR_TOT) + (BNK_YEAR_TOT) + (PF_TOTAL)) - (F_PREMIUM)) * (pf_rate / 100);
                }

                PF_YEAR_TOT = (EMP_YEAR_TOT) + (BNK_YEAR_TOT) + (INS_YEAR_TOT);
                EMP_TOT = (PF_EMP_CONT) + (EMP_YEAR_TOT) - (F_PREMIUM);
                BNK_TOT = (PF_BNK_CONT) + (BNK_YEAR_TOT);
                INS_TOT = (PF_INTEREST) + (INS_YEAR_TOT);
                PF_TOT = (EMP_TOT) + (BNK_TOT) + (INS_TOT);

                txtyearPF = PF_YEAR_TOT.ToString();
                txtyearIns = INS_YEAR_TOT.ToString();
                if (txtyearIns == "0" || txtyearIns == null)
                {
                    txtyearIns = "";
                }

                txtTotalEmp = EMP_TOT.ToString();
                txtTotBnk = BNK_TOT.ToString();
                txtTotIns = INS_TOT.ToString();
                txtTotalPF = PF_TOT.ToString();

                #region PostChng

                Result rslt1;
                Dictionary<string, object> param1 = new Dictionary<string, object>();
                param1.Add("PF_PR_P_NO", PR_P_NO);
                rslt1 = CnmDM.GetData("CHRIS_SP_PFUND_MANAGER", "PostChng", param1);
                if (rslt.isSuccessful)
                {
                    if (rslt1.dstResult.Tables.Count > 0 && rslt1.dstResult.Tables[0].Rows.Count > 0)
                    {
                        txtBranch = rslt1.dstResult.Tables[0].Rows[0].ItemArray.ToString() == "" ? "" : rslt1.dstResult.Tables[0].Rows[0].ItemArray[0].ToString();
                    }
                }
                #endregion
                ValidateProc1 = false;
            }

            if (MessageBox == "")
            {
                var res = new
                {
                    txtjan_bnk = txtjan_bnk,
                    txtfeb_bnk = txtfeb_bnk,
                    txtMar_bnk = txtMar_bnk,
                    txtapr_bnk = txtapr_bnk,
                    txtMay_bnk = txtMay_bnk,
                    txtjun_bnk = txtjun_bnk,
                    txtjul_bnk = txtjul_bnk,
                    txtaug_bnk = txtaug_bnk,
                    txtsep_bnk = txtsep_bnk,
                    txtoct_bnk = txtoct_bnk,
                    txtNov_bnk = txtNov_bnk,
                    txtdec_bnk = txtdec_bnk,
                                           
                    txtJan_emp = txtJan_emp,
                    txtfeb_emp = txtfeb_emp,
                    txtMar_emp = txtMar_emp,
                    txtApr_emp = txtApr_emp,
                    txtMay_emp = txtMay_emp,
                    txtJun_emp = txtJun_emp,
                    txtJul_emp = txtJul_emp,
                    txtaug_emp = txtaug_emp,
                    txtsep_emp = txtsep_emp,
                    txtoct_emp = txtoct_emp,
                    txtNov_emp = txtNov_emp,
                    txtDec_emp = txtDec_emp,
                                 
                    txtyearEmp = txtyearEmp,
                    txtyearBnk = txtyearBnk,
                    MessageBox = MessageBox,

                    txtPFbalnce = txtPFbalnce,
                    txtPfBankcont = txtPfBankcont,
                    txtAccIntrst = txtAccIntrst,
                    txtToatal = txtToatal,
                    txtPremium = txtPremium,
                    txtyearPF = txtyearPF,
                    txtyearIns = txtyearIns,
                    txtTotalEmp = txtTotalEmp,
                    txtTotBnk = txtTotBnk,
                    txtTotIns = txtTotIns,
                    txtTotalPF = txtTotalPF,
                    txtBranch = txtBranch,
                    txtBalance = txtBalance,
                    dtww_date = dtww_date,
                };

                return Json(res);
            }
            else
            {
                return Json(MessageBox);
            }            

            return Json(null);
        }

       
        #endregion
        public IActionResult PFundSummary()
        {
            return View();
        }
        public IActionResult ProvidentFundContribution()
        {
            return View();
        }
        public IActionResult ResignISandOtherIPFB()
        {
            return View();
        }
        public IActionResult ResignISandOtherSettlement()
        {
            return View();
        }
        public IActionResult YearEndClosingDialog()
        {
            return View();
        }


        #region YearEndClosingProcess

        public IActionResult YearEndClosingProcess()
        {
            return View();
        }
        public ActionResult YearValidation(string Year)
        {
            Dictionary<string, object> objVals = new Dictionary<string, object>();
            DataTable dtYear = new DataTable();
            Result rsltYear;
            CmnDataManager cmnDM = new CmnDataManager();

            if (Year == "")
            {
                MessageBox = "Processing Year Must Be Entered....";
                return Json(MessageBox);
            }
            else if (Year != "")
            {
                objVals.Clear();
                objVals.Add("PR_YEAR", Year);

                //VALIDATING PROPER YEAR//
                rsltYear = cmnDM.GetData("CHRIS_SP_YEARENDCLOSINGPROCESS_MANAGER", "VAL_YEAR", objVals);
                if (rsltYear.isSuccessful)
                {

                    //if (rsltYear.dstResult.Tables[0].Rows[0].ItemArray[0].ToString() != "0")
                    if (rsltYear.dstResult.Tables.Count > 0)
                    {
                        dtYear = rsltYear.dstResult.Tables[0];
                        if (dtYear.Rows.Count == 0 && Convert.ToString(dtYear.Rows[0]["W_YEAR"]) == "" || Convert.ToString(dtYear.Rows[0]["WW_DATE"]) == "")
                        {
                            MessageBox = "Invalid Year Entered.....Try Again";
                            return Json(MessageBox);
                        }
                    }
                }
                else
                {
                    MessageBox = "Invalid Year Entered.....Try Again";
                    return Json(MessageBox);
                }
            }
            return Json("OK");
        }

        public ActionResult YearEndClosingProcess_Start(string Year)
        {
            Dictionary<string, object> objVals = new Dictionary<string, object>();
            DataTable dtPFund = new DataTable();
            Result rsltPFund, rsltMain;
            CmnDataManager cmnDM = new CmnDataManager();

            objVals.Clear();
            objVals.Add("PR_YEAR", Year);

            //DELETING FROM PFUND//
            rsltPFund = cmnDM.GetData("CHRIS_SP_YEARENDCLOSINGPROCESS_MANAGER", "DEL_PFUND", objVals);
            if (rsltPFund.isSuccessful)
            {
               
                rsltMain = cmnDM.GetData("CHRIS_SP_YEARENDCLOSINGPROCESS_MANAGER", "VALID_PNO", objVals);

                if (rsltMain.isSuccessful)
                {
                    if (rsltMain.dstResult.Tables[0].Rows.Count > 0)
                    {
                        dtPFund = rsltMain.dstResult.Tables[0];
                        if (dtPFund.Rows.Count > 0)
                        {
                            for (int j = 0; j < dtPFund.Rows.Count; j++)
                            {
                                string PR_P_NO = Convert.ToString(dtPFund.Rows[j]["PR_P_NO"]);                                
                                objVals.Clear();
                                objVals.Add("PR_P_NUM", PR_P_NO);
                                objVals.Add("PR_YEAR", Year);
                                cmnDM.GetData("CHRIS_SP_YEARENDCLOSINGPROCESS_MANAGER", "PROCESS_PFUND", objVals);                              
                            }
                            return Json("PROCESS COMPLETE");
                        }
                    }           
                }          
            }
            return Json(null);
        }


        #endregion

    }
}
