﻿using Microsoft.AspNetCore.Mvc;

namespace CHRIS.Controllers
{
    public class SetupReportController : Controller
    {
        public IActionResult AccountFileList()
        {
            return View();
        }
        public IActionResult AllowanceFileList()
        {
            return View();
        }
        public IActionResult CategoryFileList()
        {
            return View();
        }
        public IActionResult ContractorCodeFileList()
        {
            return View();
        }
        public IActionResult DeductionFileList()
        {
            return View();
        }
        public IActionResult DeptFileList()
        {
            return View();
        }
        public IActionResult DesignationFileList()
        {
            return View();
        }
        public IActionResult GroupFileList()
        {
            return View();
        }
        public IActionResult HolidayFileList()
        {
            return View();
        }
        public IActionResult IncomeTaxFileList()
        {
            return View();
        }
        public IActionResult InsuranceFileList()
        {
            return View();
        }
        public IActionResult TIRFileList()
        {
            return View();
        }
    }
}
