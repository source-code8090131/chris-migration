﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using DocumentFormat.OpenXml.InkML;
using CHRIS.Controllers;
using CHRIS.Data;
using CHRIS.Data;
using CHRIS.Filters;
using CHRIS.Models;
using Microsoft.AspNetCore.Mvc;

namespace CHRIS.Controllers
{
    [LoginAuthentication]
    public class HomeController : Controller
    {
        CHRIS_Context context = new CHRIS_Context();

        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }
        //public ActionResult Index()
        //{
        //    return View();
        //}
        public ActionResult Index()
        {
            if (HttpContext.Session.GetString("USER_ID") != null)
            {
                if (DAL.CheckFunctionValidity("Home", "Index", HttpContext.Session.GetString("USER_ID").ToString()))
                {
                    string UserID = HttpContext.Session.GetString("USER_ID").ToString();
                    int?[] RoleIds = context.CHRIS_ASSIGN_LOGIN_RIGHTS.Where(m => m.PALR_USER_ID == UserID && m.PALR_STATUS == true).Select(m => m.PALR_ROLE_ID).ToArray();
                    RoleIds = context.CHRIS_ROLES.Where(m => RoleIds.Contains(m.PR_ID) && m.PR_STATUS == "true" && m.PR_ISAUTH == true).Select(m => (int?)m.PR_ID).ToArray();
                    int?[] FunctionIds = context.CHRIS_ASSIGN_FUNCTIONS.Where(m => RoleIds.Contains(m.PAF_ROLE_ID) && m.PAF_STATUS == true).Select(m => m.PAF_FUNCTION_ID).Distinct().ToArray();
                    List<CHRIS_FUNCTIONS> menu = context.CHRIS_FUNCTIONS.Where(m => FunctionIds.Contains(m.PF_ID) && m.PF_ACTION != "#" && m.PF_CONTROLLER != "#").Distinct().OrderBy(m => m.PF_NAME).ToList();
                    return PartialView(menu);
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
               // return PartialView();
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
        }

        //public ActionResult ImportIndex()
        //{
        //    return View();
        //}
        //public ActionResult Index()
        //{
        //    return View();
        //}


        //[ChildActionOnly]
        //public PartialViewResult Menus()



        public ActionResult Menu()
        {
            if (HttpContext.Session.GetString("USER_ID") != null)
            {
                string UserID = HttpContext.Session.GetString("USER_ID");
                int?[] RoleIds = context.CHRIS_ASSIGN_LOGIN_RIGHTS.Where(m => m.PALR_USER_ID == UserID && m.PALR_STATUS == true).Select(m => m.PALR_ROLE_ID).ToArray();
                RoleIds = context.CHRIS_ROLES.Where(m => RoleIds.Contains(m.PR_ID) && m.PR_STATUS == "true" && m.PR_ISAUTH == true).Select(m => (int?)m.PR_ID).ToArray();
                int?[] FunctionIds = context.CHRIS_ASSIGN_FUNCTIONS.Where(m => RoleIds.Contains(m.PAF_ROLE_ID) && m.PAF_STATUS == true).Select(m => m.PAF_FUNCTION_ID).Distinct().ToArray();
                List<CHRIS_FUNCTIONS> menu = context.CHRIS_FUNCTIONS.Where(m => FunctionIds.Contains(m.PF_ID)).Distinct().OrderBy(m => m.PF_NAME).ToList();
                return PartialView("Menu", menu);
               // return PartialView();
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
            //return PartialView();
        }


        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

    }
}