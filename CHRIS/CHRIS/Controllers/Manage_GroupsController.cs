﻿using CHRIS.Data;
using CHRIS.Models;
using CHRIS.Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;

namespace CHRIS.Controllers
{
    public class Manage_GroupsController : Controller
    {
        CHRIS_Context context = new CHRIS_Context();
        public ActionResult Index(int R = 0)
        {
            if (HttpContext.Session.GetString("USER_ID") != null)
            {
                if (DAL.CheckFunctionValidity("Manage_Groups", "Index", HttpContext.Session.GetString("USER_ID").ToString()))
                {
                    string message = "";
                    try
                    {
                        if (R != 0)
                        {
                            AssignFunctions_Custom entity = new AssignFunctions_Custom();
                            entity.Entity = context.CHRIS_ROLES.Where(m => m.PR_ID == R).FirstOrDefault();
                            if (entity.Entity != null)
                            {
                                List<int?> PreviousFunctionIds = context.CHRIS_ASSIGN_FUNCTIONS.Where(m => m.PAF_ROLE_ID == R && m.PAF_STATUS == true).Select(m => m.PAF_FUNCTION_ID).ToList();
                                List<SelectListItem> items = new SelectList(context.CHRIS_FUNCTIONS.OrderBy(m => m.PF_NAME).ToList(), "PF_ID", "PF_NAME", 0).ToList();
                                foreach (SelectListItem item in items)
                                {
                                    int? CurrentFId = Convert.ToInt32(item.Value);
                                    if (PreviousFunctionIds.Contains(CurrentFId))
                                        item.Selected = true;
                                }
                                entity.FunctionsList = items;
                                return View(entity);
                            }
                            else
                            {
                                message = "Exception Occur while fetching your record on User Id = " + R;
                                return View();
                            }
                        }
                        else
                        {
                            AssignFunctions_Custom entity = new AssignFunctions_Custom();
                            entity.Entity = new CHRIS_ROLES();
                            entity.Entity.PR_ID = 0;
                            List<SelectListItem> items = new SelectList(context.CHRIS_FUNCTIONS.OrderBy(m => m.PF_NAME).ToList(), "PF_ID", "PF_NAME", 0).ToList();
                            entity.FunctionsList = items;
                            return View(entity);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
            return View();
        }
        [HttpPost]
        public ActionResult Index(AssignFunctions_Custom db_table)
        {
            if (HttpContext.Session.GetString("USER_ID") != null)
            {
                if (DAL.CheckFunctionValidity("Manage_Groups", "Index", HttpContext.Session.GetString("USER_ID").ToString()))
                {
                    string message = "";
                    try
                    {
                        if (db_table.Entity.PR_NAME == "" || db_table.Entity.PR_NAME == null)
                        {
                            message = "Role Name is required.";
                        }
                        else
                        {
                            if (db_table.SelectedFunctions != null && db_table.SelectedFunctions.Count() > 0)
                            {

                                CHRIS_ROLES MasterEntityToUpdate = new CHRIS_ROLES();
                                MasterEntityToUpdate = context.CHRIS_ROLES.Where(m => m.PR_NAME.ToLower() == db_table.Entity.PR_NAME.ToLower() && m.PR_ID != db_table.Entity.PR_ID).FirstOrDefault();
                                if (MasterEntityToUpdate == null)
                                {
                                    MasterEntityToUpdate = new CHRIS_ROLES();
                                    if (db_table.Entity.PR_ID != 0)
                                    {
                                        MasterEntityToUpdate = context.CHRIS_ROLES.Where(m => m.PR_ID == db_table.Entity.PR_ID).FirstOrDefault();
                                        MasterEntityToUpdate.PR_DATA_EDIT_DATETIME = DateTime.Now;
                                        context.Entry(MasterEntityToUpdate).State = EntityState.Modified;
                                    }
                                    else
                                    {
                                        MasterEntityToUpdate.PR_ID = Convert.ToInt32(context.CHRIS_ROLES.Max(m => (decimal?)m.PR_ID)) + 1;
                                        MasterEntityToUpdate.PR_DATA_ENTRY_DATETIME = DateTime.Now;
                                        context.CHRIS_ROLES.Add(MasterEntityToUpdate);
                                    }
                                    MasterEntityToUpdate.PR_NAME = db_table.Entity.PR_NAME;
                                    MasterEntityToUpdate.PR_DESCRIPTION = db_table.Entity.PR_DESCRIPTION;
                                    MasterEntityToUpdate.PR_MAKER_ID = HttpContext.Session.GetString("USER_ID").ToString();
                                    if (MasterEntityToUpdate.PR_MAKER_ID == "Admin123")
                                    {
                                        MasterEntityToUpdate.PR_CHECKER_ID = MasterEntityToUpdate.PR_MAKER_ID;
                                        MasterEntityToUpdate.PR_ISAUTH = true;
                                    }
                                    else
                                    {
                                        MasterEntityToUpdate.PR_CHECKER_ID = null;
                                        MasterEntityToUpdate.PR_ISAUTH = false;
                                    }
                                    MasterEntityToUpdate.PR_STATUS = db_table.Entity.PR_STATUS;
                                    int RowCount = context.SaveChanges();
                                    if (RowCount > 0)
                                    {
                                        #region Get Old Function
                                        List<CHRIS_ASSIGN_FUNCTIONS> OldFunctions = context.CHRIS_ASSIGN_FUNCTIONS.Where(m => m.PAF_ROLE_ID == MasterEntityToUpdate.PR_ID && m.PAF_STATUS == true).ToList();

                                        string TempNewFunction = "";
                                        string TempRemoveFunction = "";
                                        foreach (int functionid in db_table.SelectedFunctions)
                                        {
                                            CHRIS_ASSIGN_FUNCTIONS Function = context.CHRIS_ASSIGN_FUNCTIONS.Where(m => m.PAF_FUNCTION_ID == functionid && m.PAF_ROLE_ID == MasterEntityToUpdate.PR_ID && m.PAF_STATUS == true).FirstOrDefault();
                                            if (Function == null)
                                            {
                                                CHRIS_FUNCTIONS FunctionTbl = context.CHRIS_FUNCTIONS.Where(m => m.PF_ID == functionid).FirstOrDefault();
                                                TempNewFunction += FunctionTbl.PF_NAME + ",";
                                            }
                                        }

                                        #endregion

                                        List<CHRIS_ASSIGN_FUNCTIONS> DeActivatedFunctions = context.CHRIS_ASSIGN_FUNCTIONS.Where(m => m.PAF_ROLE_ID == db_table.Entity.PR_ID && !db_table.SelectedFunctions.Contains(m.PAF_FUNCTION_ID)).ToList();
                                        if (DeActivatedFunctions.Count > 0)
                                        {
                                            foreach (CHRIS_ASSIGN_FUNCTIONS EachFunctionForUpdate in DeActivatedFunctions)
                                            {
                                                CHRIS_FUNCTIONS FunctionTbl = context.CHRIS_FUNCTIONS.Where(m => m.PF_ID == EachFunctionForUpdate.PAF_FUNCTION_ID).FirstOrDefault();
                                                TempRemoveFunction += FunctionTbl.PF_NAME + ",";
                                                EachFunctionForUpdate.PAF_STATUS = false;
                                                context.Entry(EachFunctionForUpdate).State = EntityState.Modified;
                                            }
                                        }
                                        int AssignFunId = 0;
                                        foreach (int? SelectedFunctionId in db_table.SelectedFunctions)
                                        {
                                            if (SelectedFunctionId != null)
                                            {
                                                CHRIS_ASSIGN_FUNCTIONS EachFunctionToUpdate = context.CHRIS_ASSIGN_FUNCTIONS.Where(m => m.PAF_ROLE_ID == db_table.Entity.PR_ID && m.PAF_FUNCTION_ID == SelectedFunctionId).FirstOrDefault();
                                                if (EachFunctionToUpdate == null)
                                                {
                                                    EachFunctionToUpdate = new CHRIS_ASSIGN_FUNCTIONS();
                                                    if (AssignFunId == 0)
                                                    {
                                                        EachFunctionToUpdate.PAF_ID = Convert.ToInt32(context.CHRIS_ASSIGN_FUNCTIONS.Max(m => (decimal?)m.PAF_ID)) + 1;
                                                        AssignFunId = EachFunctionToUpdate.PAF_ID;
                                                    }
                                                    else
                                                    {
                                                        AssignFunId = (AssignFunId + 1);
                                                        EachFunctionToUpdate.PAF_ID = AssignFunId;
                                                    }
                                                    EachFunctionToUpdate.PAF_FUNCTION_ID = SelectedFunctionId;
                                                    EachFunctionToUpdate.PAF_ROLE_ID = MasterEntityToUpdate.PR_ID;
                                                    EachFunctionToUpdate.PAF_MAKER_ID = HttpContext.Session.GetString("USER_ID").ToString();
                                                    EachFunctionToUpdate.PAF_STATUS = true;
                                                    EachFunctionToUpdate.PAF_DATA_ENTRY_DATETIME = DateTime.Now;
                                                    context.CHRIS_ASSIGN_FUNCTIONS.Add(EachFunctionToUpdate);
                                                }
                                                else
                                                {
                                                    EachFunctionToUpdate.PAF_MAKER_ID = HttpContext.Session.GetString("USER_ID").ToString();
                                                    EachFunctionToUpdate.PAF_STATUS = true;
                                                    EachFunctionToUpdate.PAF_DATA_ENTRY_DATETIME = DateTime.Now;
                                                    context.Entry(EachFunctionToUpdate).State = EntityState.Modified;
                                                }
                                            }
                                        }
                                        RowCount = context.SaveChanges();
                                        if (RowCount > 0)
                                        {
                                            message = "Data Inserted Successfully " + RowCount + " Affected";
                                        }
                                    }
                                    else
                                        message = "Problem While Inserting data " + Environment.NewLine + "Please Contact to Administrator";
                                }
                                else
                                {
                                    message = "A Role with the given name " + db_table.Entity.PR_NAME + " Already Exist";
                                }
                            }
                            else
                            {
                                message = "Please Select Functions.";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                    AssignFunctions_Custom entity = new AssignFunctions_Custom();
                    entity.Entity = new CHRIS_ROLES();
                    entity.Entity.PR_ID = db_table.Entity.PR_ID;
                    List<int?> PreviousFunctionIds = context.CHRIS_ASSIGN_FUNCTIONS.Where(m => m.PAF_ROLE_ID == db_table.Entity.PR_ID).Select(m => m.PAF_FUNCTION_ID).ToList();
                    List<SelectListItem> items = new SelectList(context.CHRIS_FUNCTIONS.OrderBy(m => m.PF_NAME).ToList(), "PF_ID", "PF_NAME", 0).OrderBy(m => m.Text).ToList();
                    foreach (SelectListItem item in items)
                    {
                        int? CurrentFId = Convert.ToInt32(item.Value);
                        if (PreviousFunctionIds.Contains(CurrentFId))
                            item.Selected = true;
                    }
                    entity.FunctionsList = items;
                    return View(entity);
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
        }
        //[ChildActionOnly]
        public PartialViewResult Grid_View()
        {
            if (HttpContext.Session.GetString("USER_ID") != null)
            {
                string SessionUser = HttpContext.Session.GetString("USER_ID").ToString();
                List<CHRIS_ROLE_WITH_FUNCTIONS_LIST_VIEW> ViewData = new List<CHRIS_ROLE_WITH_FUNCTIONS_LIST_VIEW>();
                if (SessionUser == "Admin123")
                    ViewData = context.CHRIS_ROLE_WITH_FUNCTIONS_LIST_VIEW.Where(m => m.PR_MAKER_ID != SessionUser || m.PR_ISAUTH == true).OrderByDescending(m => m.PR_ID).ToList();
                else
                    ViewData = context.CHRIS_ROLE_WITH_FUNCTIONS_LIST_VIEW.Where(m => m.PR_NAME != "ADMIN" && (m.PR_MAKER_ID != SessionUser || m.PR_ISAUTH == true)).OrderByDescending(m => m.PR_ID).ToList();
                return PartialView(ViewData);
            }
            else
            {
                return PartialView(null);
            }
        }
        public JsonResult AuthorizeGroupRole(int R_ID)
        {
            if (HttpContext.Session.GetString("USER_ID") != null && Request.Headers["X-Requested-With"] == "XMLHttpRequest")
            {
                if (DAL.CheckFunctionValidity("Manage_Groups", "Index", HttpContext.Session.GetString("USER_ID").ToString()))
                {
                    string message = "";
                    try
                    {
                        CHRIS_ROLES UpdateEntity = context.CHRIS_ROLES.Where(m => m.PR_ID == R_ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            if (UpdateEntity.PR_MAKER_ID != HttpContext.Session.GetString("USER_ID").ToString())
                            {
                                UpdateEntity.PR_ISAUTH = true;
                                UpdateEntity.PR_CHECKER_ID = HttpContext.Session.GetString("USER_ID").ToString();
                                UpdateEntity.PR_DATA_EDIT_DATETIME = DateTime.Now;
                                context.Entry(UpdateEntity).State = EntityState.Modified;
                                int RowCount = context.SaveChanges();
                                if (RowCount > 0)
                                {
                                    message = "Group Role : " + UpdateEntity.PR_NAME + " is successfully Authorized";
                                }
                            }
                            else
                            {
                                message = "Sorry, Maker cannot authorize the same record.";
                            }
                        }
                        else
                        {
                            message = "Probleum while fetching your record on ID# " + R_ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        public JsonResult RejectAuthorizeGroupRole(int R_ID)
        {
            if (HttpContext.Session.GetString("USER_ID") != null && Request.Headers["X-Requested-With"] == "XMLHttpRequest")
            {
                if (DAL.CheckFunctionValidity("Manage_Groups", "Index", HttpContext.Session.GetString("USER_ID").ToString()))
                {
                    string message = "";
                    try
                    {
                        CHRIS_ROLES UpdateEntity = context.CHRIS_ROLES.Where(m => m.PR_ID == R_ID).FirstOrDefault();
                        #region Save Temp Function Name Before Deleting
                        string GetAllFunction = "";
                        CHRIS_ROLE_WITH_FUNCTIONS_LIST_VIEW AllFunction = context.CHRIS_ROLE_WITH_FUNCTIONS_LIST_VIEW.Where(m => m.PR_ID == R_ID).FirstOrDefault();
                        if (AllFunction != null)
                        {
                            GetAllFunction += AllFunction.ASSIGNED_FUNCTION;
                        }
                        #endregion
                        if (UpdateEntity != null)
                        {
                            if (UpdateEntity.PR_MAKER_ID != HttpContext.Session.GetString("USER_ID").ToString())
                            {
                                context.CHRIS_ROLES.Remove(UpdateEntity);
                                int RowCount = context.SaveChanges();
                                if (RowCount > 0)
                                {
                                    List<CHRIS_ASSIGN_FUNCTIONS> GetAllAssignFun = context.CHRIS_ASSIGN_FUNCTIONS.Where(m => m.PAF_ROLE_ID == UpdateEntity.PR_ID).ToList();
                                    if (GetAllAssignFun.Count > 0)
                                    {
                                        RowCount = 0;
                                        foreach (CHRIS_ASSIGN_FUNCTIONS FuntionToRemove in GetAllAssignFun)
                                        {
                                            context.CHRIS_ASSIGN_FUNCTIONS.Remove(FuntionToRemove);
                                            RowCount += context.SaveChanges();
                                        }
                                    }
                                    if (RowCount > 0)
                                    {
                                        message = "Group Role : " + UpdateEntity.PR_NAME + " is Rejected";
                                    }
                                }
                            }
                            else
                                message = "Sorry, Maker cannot reject the same record.";
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + R_ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
    }
}
