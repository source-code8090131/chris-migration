﻿using CHRIS.ReportModels;
using Microsoft.AspNetCore.Mvc;
using System.Net.Http.Headers;
using System.Text.Json;
using System.Text;
using CHRIS.Common;
using CHRIS.Services;
using Newtonsoft.Json;
using System.Data;
using System.Security.Cryptography.Xml;

namespace CHRIS.Controllers
{
    public class PayrollReportsController : Controller
    {
        #region PERIODIC REPORT FOR INC PROMOTION
        public IActionResult PeriodicReportForIncPromotion()
        {
            string message = "";
            try
            {

            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        [HttpPost]
        public IActionResult PeriodicReportForIncPromotion(string PRFI_BRANCH, DateTime? PRFI_FROM_DATE, DateTime? PRFI_TO_DATE)
        {
            string message = "";
            try
            {
                HttpResponseMessage file = new HttpResponseMessage();
                PY001Model entity = new PY001Model();
                entity.Branch = PRFI_BRANCH;
                if (PRFI_FROM_DATE != null)
                    entity.From_Date = Convert.ToDateTime(PRFI_FROM_DATE);
                else
                    entity.From_Date = null;
                if (PRFI_TO_DATE != null)
                    entity.To_Date = Convert.ToDateTime(PRFI_TO_DATE);
                else
                    entity.To_Date = null;
                var DomainName = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetSection("CHRIS_REPORTING")["API_URL"];
                string BASE_URL = DomainName + "api/PayrollReporting/PeriodicReportForIncPromotion/";
                using (HttpClient clienFNREP08 = new HttpClient())
                {
                    using (HttpRequestMessage req310 = new HttpRequestMessage(HttpMethod.Post, BASE_URL))
                    {
                        req310.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        string dataAsJson = System.Text.Json.JsonSerializer.Serialize(entity,
                        new JsonSerializerOptions() { WriteIndented = false });
                        message += dataAsJson + Environment.NewLine;
                        var content = new StringContent(dataAsJson, Encoding.UTF8, "application/json");
                        req310.Content = content;
                        req310.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                        HttpResponseMessage response = clienFNREP08.SendAsync(req310).Result;
                        int StatusCode = (int)response.StatusCode;
                        string StatusDesc = response.StatusCode.ToString();
                        if (StatusCode == 200)
                        {
                            file = response;
                            return File(file.Content.ReadAsByteArrayAsync().Result, "application/octet-stream", "PY001_" + DateTime.Now.ToString("dd_MM_yyyy") + ".pdf");
                        }
                        else
                        {
                            message = "Code : " + StatusCode + " | Description : " + StatusDesc;
                            return View();
                        }
                        //return responsse;
                    }
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        #endregion

        #region MONTHLY STAFF PAYMENT LISTINGS
        public IActionResult MonthlyStaffPaymentListings()
        {
            string message = "";
            try
            {

            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        [HttpPost]
        public IActionResult MonthlyStaffPaymentListings(string MSP_BRANCH, string MSP_SEGMENT, string MSP_CATEGORY, int? MSP_MONTH, int? MSP_YEAR, string MSP_FLAG)
        {
            string message = "";
            try
            {
                HttpResponseMessage file = new HttpResponseMessage();
                PY002Model entity = new PY002Model();
                entity.Branch = MSP_BRANCH;
                entity.Segment = MSP_SEGMENT;
                entity.Category = MSP_CATEGORY;
                if (MSP_MONTH != null)
                    entity.Month = Convert.ToInt32(MSP_MONTH);
                else
                    entity.Month = null;
                if (MSP_YEAR != null)
                    entity.Year = Convert.ToInt32(MSP_YEAR);
                else
                    entity.Year = null;
                entity.Flag = MSP_FLAG;
                var DomainName = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetSection("CHRIS_REPORTING")["API_URL"];
                string BASE_URL = DomainName + "api/PayrollReporting/MonthlyStaffPaymentListings/";
                using (HttpClient clienFNREP08 = new HttpClient())
                {
                    using (HttpRequestMessage req310 = new HttpRequestMessage(HttpMethod.Post, BASE_URL))
                    {
                        req310.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        string dataAsJson = System.Text.Json.JsonSerializer.Serialize(entity,
                        new JsonSerializerOptions() { WriteIndented = false });
                        message += dataAsJson + Environment.NewLine;
                        var content = new StringContent(dataAsJson, Encoding.UTF8, "application/json");
                        req310.Content = content;
                        req310.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                        HttpResponseMessage response = clienFNREP08.SendAsync(req310).Result;
                        int StatusCode = (int)response.StatusCode;
                        string StatusDesc = response.StatusCode.ToString();
                        if (StatusCode == 200)
                        {
                            file = response;
                            return File(file.Content.ReadAsByteArrayAsync().Result, "application/octet-stream", "PY002_" + DateTime.Now.ToString("dd_MM_yyyy") + ".pdf");
                        }
                        else
                        {
                            message = "Code : " + StatusCode + " | Description : " + StatusDesc;
                            return View();
                        }
                        //return responsse;
                    }
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        #endregion

        #region PROVIDENT FUND REPORT
        public IActionResult ProvidentFundReport()
        {
            string message = "";
            try
            {

            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        [HttpPost]
        public IActionResult ProvidentFundReport(string PFR_BRANCH, string PFR_SEGMENT, string PFR_CATEGORY, int? PFR_MONTH, int? PFR_YEAR)
        {
            string message = "";
            try
            {
                HttpResponseMessage file = new HttpResponseMessage();
                PY003Model entity = new PY003Model();
                entity.Branch = PFR_BRANCH;
                entity.Segment = PFR_SEGMENT;
                entity.Category = PFR_CATEGORY;
                if (PFR_MONTH != null)
                    entity.Month = Convert.ToInt32(PFR_MONTH);
                else
                    entity.Month = null;
                if (PFR_YEAR != null)
                    entity.Year = Convert.ToInt32(PFR_YEAR);
                else
                    entity.Year = null;
                var DomainName = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetSection("CHRIS_REPORTING")["API_URL"];
                string BASE_URL = DomainName + "api/PayrollReporting/ProvidentFundReport/";
                using (HttpClient clienFNREP08 = new HttpClient())
                {
                    using (HttpRequestMessage req310 = new HttpRequestMessage(HttpMethod.Post, BASE_URL))
                    {
                        req310.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        string dataAsJson = System.Text.Json.JsonSerializer.Serialize(entity,
                        new JsonSerializerOptions() { WriteIndented = false });
                        message += dataAsJson + Environment.NewLine;
                        var content = new StringContent(dataAsJson, Encoding.UTF8, "application/json");
                        req310.Content = content;
                        req310.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                        HttpResponseMessage response = clienFNREP08.SendAsync(req310).Result;
                        int StatusCode = (int)response.StatusCode;
                        string StatusDesc = response.StatusCode.ToString();
                        if (StatusCode == 200)
                        {
                            file = response;
                            return File(file.Content.ReadAsByteArrayAsync().Result, "application/octet-stream", "PY003_" + DateTime.Now.ToString("dd_MM_yyyy") + ".pdf");
                        }
                        else
                        {
                            message = "Code : " + StatusCode + " | Description : " + StatusDesc;
                            return View();
                        }
                        //return responsse;
                    }
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        #endregion

        #region SALARY SETTLEMENT OFFICER
        public IActionResult SalarySettlementOfficer()
        {
            string message = "";
            try
            {

            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        [HttpPost]
        public IActionResult SalarySettlementOfficer(string SSO_BRANCH, string SSO_SEGMENT, string SSO_DEPARTMENT, string SSO_CATEGORY, int? SSO_MONTH, int? SSO_YEAR, string SSO_FPNO, string SSO_TPNO)
        {
            string message = "";
            try
            {
                HttpResponseMessage file = new HttpResponseMessage();
                PY011_BModel entity = new PY011_BModel();
                entity.Branch = SSO_BRANCH;
                entity.Segment = SSO_SEGMENT;
                entity.Department = SSO_DEPARTMENT;
                entity.Category = SSO_CATEGORY;
                if (SSO_MONTH != null)
                    entity.Month = Convert.ToInt32(SSO_MONTH);
                else
                    entity.Month = null;
                if (SSO_YEAR != null)
                    entity.Year = Convert.ToInt32(SSO_YEAR);
                else
                    entity.Year = null;
                entity.FPNO = SSO_FPNO;
                entity.TPNO = SSO_TPNO;
                entity.Mode = null;
                var DomainName = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetSection("CHRIS_REPORTING")["API_URL"];
                string BASE_URL = DomainName + "api/PayrollReporting/SalarySettlementOfficer/";
                using (HttpClient clienFNREP08 = new HttpClient())
                {
                    using (HttpRequestMessage req310 = new HttpRequestMessage(HttpMethod.Post, BASE_URL))
                    {
                        req310.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        string dataAsJson = System.Text.Json.JsonSerializer.Serialize(entity,
                        new JsonSerializerOptions() { WriteIndented = false });
                        message += dataAsJson + Environment.NewLine;
                        var content = new StringContent(dataAsJson, Encoding.UTF8, "application/json");
                        req310.Content = content;
                        req310.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                        HttpResponseMessage response = clienFNREP08.SendAsync(req310).Result;
                        int StatusCode = (int)response.StatusCode;
                        string StatusDesc = response.StatusCode.ToString();
                        if (StatusCode == 200)
                        {
                            file = response;
                            return File(file.Content.ReadAsByteArrayAsync().Result, "application/octet-stream", "PRREP46_" + DateTime.Now.ToString("dd_MM_yyyy") + ".pdf");
                        }
                        else
                        {
                            message = "Code : " + StatusCode + " | Description : " + StatusDesc;
                            return View();
                        }
                        //return responsse;
                    }
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        #endregion

        #region MONTHLY TAX REPORT
        public IActionResult MonthlyTaxReport()
        {
            string message = "";
            try
            {

            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        [HttpPost]
        public IActionResult MonthlyTaxReport(DateTime? MTP_MONTH, string MTP_CITY_CODE)
        {
            string message = "";
            try
            {
                HttpResponseMessage file = new HttpResponseMessage();
                MONTHLY_TAXModel entity = new MONTHLY_TAXModel();
                entity.Branch = MTP_CITY_CODE;
                if (MTP_MONTH != null)
                    entity.Month = Convert.ToDateTime(MTP_MONTH);
                else
                    entity.Month = null;
                var DomainName = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetSection("CHRIS_REPORTING")["API_URL"];
                string BASE_URL = DomainName + "api/PayrollReporting/MonthlyTaxReport/";
                using (HttpClient clienFNREP08 = new HttpClient())
                {
                    using (HttpRequestMessage req310 = new HttpRequestMessage(HttpMethod.Post, BASE_URL))
                    {
                        req310.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        string dataAsJson = System.Text.Json.JsonSerializer.Serialize(entity,
                        new JsonSerializerOptions() { WriteIndented = false });
                        message += dataAsJson + Environment.NewLine;
                        var content = new StringContent(dataAsJson, Encoding.UTF8, "application/json");
                        req310.Content = content;
                        req310.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                        HttpResponseMessage response = clienFNREP08.SendAsync(req310).Result;
                        int StatusCode = (int)response.StatusCode;
                        string StatusDesc = response.StatusCode.ToString();
                        if (StatusCode == 200)
                        {
                            file = response;
                            return File(file.Content.ReadAsByteArrayAsync().Result, "application/octet-stream", "MONTHLY_TAX_" + DateTime.Now.ToString("dd_MM_yyyy") + ".pdf");
                        }
                        else
                        {
                            message = "Code : " + StatusCode + " | Description : " + StatusDesc;
                            return View();
                        }
                        //return responsse;
                    }
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        #endregion

        #region EMPLOYEE LIST FOR SALARY
        public IActionResult EmployeeListForSalary()
        {
            string message = "";
            try
            {

            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        [HttpPost]
        public IActionResult EmployeeListForSalary(string Nothing)
        {
            string message = "";
            try
            {
                HttpResponseMessage file = new HttpResponseMessage();
                PRREP53Model entity = new PRREP53Model();
                var DomainName = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetSection("CHRIS_REPORTING")["API_URL"];
                string BASE_URL = DomainName + "api/PayrollReporting/EmployeeListForSalary/";
                using (HttpClient clienFNREP08 = new HttpClient())
                {
                    using (HttpRequestMessage req310 = new HttpRequestMessage(HttpMethod.Post, BASE_URL))
                    {
                        req310.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        string dataAsJson = System.Text.Json.JsonSerializer.Serialize(entity,
                        new JsonSerializerOptions() { WriteIndented = false });
                        message += dataAsJson + Environment.NewLine;
                        var content = new StringContent(dataAsJson, Encoding.UTF8, "application/json");
                        req310.Content = content;
                        req310.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                        HttpResponseMessage response = clienFNREP08.SendAsync(req310).Result;
                        int StatusCode = (int)response.StatusCode;
                        string StatusDesc = response.StatusCode.ToString();
                        if (StatusCode == 200)
                        {
                            file = response;
                            return File(file.Content.ReadAsByteArrayAsync().Result, "application/octet-stream", "PRREP53_" + DateTime.Now.ToString("dd_MM_yyyy") + ".pdf");
                        }
                        else
                        {
                            message = "Code : " + StatusCode + " | Description : " + StatusDesc;
                            return View();
                        }
                        //return responsse;
                    }
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        #endregion

        #region ATR DETAIL
        public IActionResult ATR_DETAIL()
        {
            string message = "";
            try
            {

            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        [HttpPost]
        public IActionResult ATR_DETAIL(string Nothing)
        {
            string message = "";
            try
            {
                HttpResponseMessage file = new HttpResponseMessage();
                ATR_DETAILModel entity = new ATR_DETAILModel();
                var DomainName = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetSection("CHRIS_REPORTING")["API_URL"];
                string BASE_URL = DomainName + "api/PayrollReporting/ATR_DETAIL/";
                using (HttpClient clienFNREP08 = new HttpClient())
                {
                    using (HttpRequestMessage req310 = new HttpRequestMessage(HttpMethod.Post, BASE_URL))
                    {
                        req310.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        string dataAsJson = System.Text.Json.JsonSerializer.Serialize(entity,
                        new JsonSerializerOptions() { WriteIndented = false });
                        message += dataAsJson + Environment.NewLine;
                        var content = new StringContent(dataAsJson, Encoding.UTF8, "application/json");
                        req310.Content = content;
                        req310.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                        HttpResponseMessage response = clienFNREP08.SendAsync(req310).Result;
                        int StatusCode = (int)response.StatusCode;
                        string StatusDesc = response.StatusCode.ToString();
                        if (StatusCode == 200)
                        {
                            file = response;
                            return File(file.Content.ReadAsByteArrayAsync().Result, "application/octet-stream", "ATR_DETAIL_" + DateTime.Now.ToString("dd_MM_yyyy") + ".pdf");
                        }
                        else
                        {
                            message = "Code : " + StatusCode + " | Description : " + StatusDesc;
                            return View();
                        }
                        //return responsse;
                    }
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        #endregion

        #region ATR INCENTIVE ALLOW
        public IActionResult ATRIncentiveAllow()
        {
            string message = "";
            try
            {

            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        [HttpPost]
        public IActionResult ATRIncentiveAllow(string ATRI_BRANCH, string ATRI_CATEGORY, string ATRI_ALLOW, int? ATRI_EMPLOYEE_NO)
        {
            string message = "";
            try
            {
                HttpResponseMessage file = new HttpResponseMessage();
                ATR_INCENTIVE_ALLOWModel entity = new ATR_INCENTIVE_ALLOWModel();
                entity.Branch = ATRI_BRANCH;
                entity.Category = ATRI_CATEGORY;
                entity.Allow = ATRI_ALLOW;
                if (ATRI_EMPLOYEE_NO != null)
                    entity.EmployeeNo = Convert.ToInt32(ATRI_EMPLOYEE_NO);
                else
                    entity.EmployeeNo = null;
                var DomainName = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetSection("CHRIS_REPORTING")["API_URL"];
                string BASE_URL = DomainName + "api/PayrollReporting/ATRIncentiveAllow/";
                using (HttpClient clienFNREP08 = new HttpClient())
                {
                    using (HttpRequestMessage req310 = new HttpRequestMessage(HttpMethod.Post, BASE_URL))
                    {
                        req310.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        string dataAsJson = System.Text.Json.JsonSerializer.Serialize(entity,
                        new JsonSerializerOptions() { WriteIndented = false });
                        message += dataAsJson + Environment.NewLine;
                        var content = new StringContent(dataAsJson, Encoding.UTF8, "application/json");
                        req310.Content = content;
                        req310.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                        HttpResponseMessage response = clienFNREP08.SendAsync(req310).Result;
                        int StatusCode = (int)response.StatusCode;
                        string StatusDesc = response.StatusCode.ToString();
                        if (StatusCode == 200)
                        {
                            file = response;
                            return File(file.Content.ReadAsByteArrayAsync().Result, "application/octet-stream", "ATR_INCENTIVE_ALLOW_" + DateTime.Now.ToString("dd_MM_yyyy") + ".pdf");
                        }
                        else
                        {
                            message = "Code : " + StatusCode + " | Description : " + StatusDesc;
                            return View();
                        }
                        //return responsse;
                    }
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        #endregion

        #region SEGMENT WISE REPORT
        public IActionResult SegmentWiseReport()
        {
            string message = "";
            try
            {

            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        [HttpPost]
        public IActionResult SegmentWiseReport(string SWR_BRANCH, string SWR_SEGMENT, string SWR_MONTH, string SWR_YEAR)
        {
            string message = "";
            try
            {
                HttpResponseMessage file = new HttpResponseMessage();
                PY009AModel entity = new PY009AModel();
                entity.Month = SWR_MONTH != null ? Convert.ToInt32(SWR_MONTH) : null;
                entity.Year = SWR_YEAR != null ? Convert.ToInt32(SWR_YEAR) : null;
                entity.Branch = SWR_BRANCH;
                entity.Segment = SWR_SEGMENT;
                var DomainName = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetSection("CHRIS_REPORTING")["API_URL"];
                string BASE_URL = DomainName + "api/PayrollReporting/SegmentWiseReport/";
                using (HttpClient clienFNREP08 = new HttpClient())
                {
                    using (HttpRequestMessage req310 = new HttpRequestMessage(HttpMethod.Post, BASE_URL))
                    {
                        req310.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        string dataAsJson = System.Text.Json.JsonSerializer.Serialize(entity,
                        new JsonSerializerOptions() { WriteIndented = false });
                        message += dataAsJson + Environment.NewLine;
                        var content = new StringContent(dataAsJson, Encoding.UTF8, "application/json");
                        req310.Content = content;
                        req310.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                        HttpResponseMessage response = clienFNREP08.SendAsync(req310).Result;
                        int StatusCode = (int)response.StatusCode;
                        string StatusDesc = response.StatusCode.ToString();
                        if (StatusCode == 200)
                        {
                            file = response;
                            return File(file.Content.ReadAsByteArrayAsync().Result, "application/octet-stream", "PY009A_" + DateTime.Now.ToString("dd_MM_yyyy") + ".pdf");
                        }
                        else
                        {
                            message = "Code : " + StatusCode + " | Description : " + StatusDesc;
                            return View();
                        }
                        //return responsse;
                    }
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        [HttpPost]
        public JsonResult GetDataForSegmentWiseReport(string Action)
        {
            Dictionary<string, object> d = new Dictionary<string, object>();
            CmnDataManager objCmnDataManager = new CmnDataManager();
            DataTable firstTable = new DataTable();
            if (Action == "GetBranch")
            {
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_BRANCH_MANAGER", "BranchCount", d);
                if (rsltCode.isSuccessful)
                {
                    if (rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                    {
                        firstTable = rsltCode.dstResult.Tables[0];
                        var CatResul = JsonConvert.SerializeObject(firstTable);
                        return Json(CatResul);
                    }
                    else
                        return Json(null);
                }
                else
                    return Json(null);
            }
            return Json(null);
        }
        #endregion

        #region DEPARTMENT WISE PAYROLL CLERICAL
        public IActionResult DepartmentWisePayrollClerical()
        {
            string message = "";
            try
            {

            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        [HttpPost]
        public IActionResult DepartmentWisePayrollClerical(string DWP_BRANCH, string DWP_SEGMENT, string DWP_MONTH, string DWP_YEAR)
        {
            string message = "";
            try
            {
                HttpResponseMessage file = new HttpResponseMessage();
                PY010Model entity = new PY010Model();
                entity.Month = DWP_MONTH != null ? Convert.ToInt32(DWP_MONTH) : null;
                entity.Year = DWP_YEAR != null ? Convert.ToInt32(DWP_YEAR) : null;
                entity.Branch = DWP_BRANCH;
                entity.Segment = DWP_SEGMENT;
                var DomainName = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetSection("CHRIS_REPORTING")["API_URL"];
                string BASE_URL = DomainName + "api/PayrollReporting/DepartmentWisePayrollClerical/";
                using (HttpClient clienFNREP08 = new HttpClient())
                {
                    using (HttpRequestMessage req310 = new HttpRequestMessage(HttpMethod.Post, BASE_URL))
                    {
                        req310.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        string dataAsJson = System.Text.Json.JsonSerializer.Serialize(entity,
                        new JsonSerializerOptions() { WriteIndented = false });
                        message += dataAsJson + Environment.NewLine;
                        var content = new StringContent(dataAsJson, Encoding.UTF8, "application/json");
                        req310.Content = content;
                        req310.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                        HttpResponseMessage response = clienFNREP08.SendAsync(req310).Result;
                        int StatusCode = (int)response.StatusCode;
                        string StatusDesc = response.StatusCode.ToString();
                        if (StatusCode == 200)
                        {
                            file = response;
                            return File(file.Content.ReadAsByteArrayAsync().Result, "application/octet-stream", "PY010_" + DateTime.Now.ToString("dd_MM_yyyy") + ".pdf");
                        }
                        else
                        {
                            message = "Code : " + StatusCode + " | Description : " + StatusDesc;
                            return View();
                        }
                        //return responsse;
                    }
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        #endregion

        #region OVER TIME REPORT
        public IActionResult OverTimeReport()
        {
            string message = "";
            try
            {

            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        [HttpPost]
        public IActionResult OverTimeReport(string OTR_BRANCH, string OTR_SEGMENT, string OTR_MONTH, string OTR_YEAR, string OTR_CATEGORY)
        {
            string message = "";
            try
            {
                HttpResponseMessage file = new HttpResponseMessage();
                PY004Model entity = new PY004Model();
                entity.Month = OTR_MONTH != null ? Convert.ToInt32(OTR_MONTH) : null;
                entity.City = OTR_BRANCH;
                entity.Segment = OTR_SEGMENT;
                entity.Category = OTR_CATEGORY;
                entity.Year = OTR_YEAR != null ? Convert.ToInt32(OTR_YEAR) : null;
                var DomainName = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetSection("CHRIS_REPORTING")["API_URL"];
                string BASE_URL = DomainName + "api/PayrollReporting/OverTimeReport/";
                using (HttpClient clienFNREP08 = new HttpClient())
                {
                    using (HttpRequestMessage req310 = new HttpRequestMessage(HttpMethod.Post, BASE_URL))
                    {
                        req310.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        string dataAsJson = System.Text.Json.JsonSerializer.Serialize(entity,
                        new JsonSerializerOptions() { WriteIndented = false });
                        message += dataAsJson + Environment.NewLine;
                        var content = new StringContent(dataAsJson, Encoding.UTF8, "application/json");
                        req310.Content = content;
                        req310.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                        HttpResponseMessage response = clienFNREP08.SendAsync(req310).Result;
                        int StatusCode = (int)response.StatusCode;
                        string StatusDesc = response.StatusCode.ToString();
                        if (StatusCode == 200)
                        {
                            file = response;
                            return File(file.Content.ReadAsByteArrayAsync().Result, "application/octet-stream", "PY004_" + DateTime.Now.ToString("dd_MM_yyyy") + ".pdf");
                        }
                        else
                        {
                            message = "Code : " + StatusCode + " | Description : " + StatusDesc;
                            return View();
                        }
                        //return responsse;
                    }
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        #endregion

        #region INCOMETAX REPORT
        public IActionResult IncomeTaxReport()
        {
            string message = "";
            try
            {

            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        [HttpPost]
        public IActionResult IncomeTaxReport(string ITR_CITY, string ITR_SEGMENT, string ITR_MONTH, string ITR_YEAR, string ITR_CATEGORY)
        {
            string message = "";
            try
            {
                HttpResponseMessage file = new HttpResponseMessage();
                PY005Model entity = new PY005Model();
                entity.City = ITR_CITY;
                entity.Month = ITR_MONTH != null ? Convert.ToInt32(ITR_MONTH) : null;
                entity.Segment = ITR_SEGMENT;
                entity.Category = ITR_CATEGORY;
                entity.Year = ITR_YEAR != null ? Convert.ToInt32(ITR_YEAR) : null;
                var DomainName = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetSection("CHRIS_REPORTING")["API_URL"];
                string BASE_URL = DomainName + "api/PayrollReporting/IncomeTaxReport/";
                using (HttpClient clienFNREP08 = new HttpClient())
                {
                    using (HttpRequestMessage req310 = new HttpRequestMessage(HttpMethod.Post, BASE_URL))
                    {
                        req310.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        string dataAsJson = System.Text.Json.JsonSerializer.Serialize(entity,
                        new JsonSerializerOptions() { WriteIndented = false });
                        message += dataAsJson + Environment.NewLine;
                        var content = new StringContent(dataAsJson, Encoding.UTF8, "application/json");
                        req310.Content = content;
                        req310.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                        HttpResponseMessage response = clienFNREP08.SendAsync(req310).Result;
                        int StatusCode = (int)response.StatusCode;
                        string StatusDesc = response.StatusCode.ToString();
                        if (StatusCode == 200)
                        {
                            file = response;
                            return File(file.Content.ReadAsByteArrayAsync().Result, "application/octet-stream", "PY005_" + DateTime.Now.ToString("dd_MM_yyyy") + ".pdf");
                        }
                        else
                        {
                            message = "Code : " + StatusCode + " | Description : " + StatusDesc;
                            return View();
                        }
                        //return responsse;
                    }
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        #endregion

        #region RECONCILIMENT REPORT
        public IActionResult ReconcilimentReport()
        {
            string message = "";
            try
            {

            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        [HttpPost]
        public IActionResult ReconcilimentReport(string RR_BRANCH, string RR_SEGMENT, string RR_MONTH, string RR_YEAR, string RR_CATEGORY)
        {
            string message = "";
            try
            {
                HttpResponseMessage file = new HttpResponseMessage();
                PY006BModel entity = new PY006BModel();
                entity.Segment = RR_SEGMENT;
                entity.Branch = RR_BRANCH;
                entity.Month = RR_MONTH != null ? Convert.ToInt32(RR_MONTH) : null;
                entity.Year = RR_YEAR != null ? Convert.ToInt32(RR_YEAR) : null;
                entity.Category = RR_CATEGORY;
                var DomainName = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetSection("CHRIS_REPORTING")["API_URL"];
                string BASE_URL = DomainName + "api/PayrollReporting/ReconcilimentReport/";
                using (HttpClient clienFNREP08 = new HttpClient())
                {
                    using (HttpRequestMessage req310 = new HttpRequestMessage(HttpMethod.Post, BASE_URL))
                    {
                        req310.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        string dataAsJson = System.Text.Json.JsonSerializer.Serialize(entity,
                        new JsonSerializerOptions() { WriteIndented = false });
                        message += dataAsJson + Environment.NewLine;
                        var content = new StringContent(dataAsJson, Encoding.UTF8, "application/json");
                        req310.Content = content;
                        req310.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                        HttpResponseMessage response = clienFNREP08.SendAsync(req310).Result;
                        int StatusCode = (int)response.StatusCode;
                        string StatusDesc = response.StatusCode.ToString();
                        if (StatusCode == 200)
                        {
                            file = response;
                            return File(file.Content.ReadAsByteArrayAsync().Result, "application/octet-stream", "PY006B_" + DateTime.Now.ToString("dd_MM_yyyy") + ".pdf");
                        }
                        else
                        {
                            message = "Code : " + StatusCode + " | Description : " + StatusDesc;
                            return View();
                        }
                        //return responsse;
                    }
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        [HttpPost]
        public JsonResult GetDataForReconcilimentReport(string Action)
        {
            Dictionary<string, object> d = new Dictionary<string, object>();
            CmnDataManager objCmnDataManager = new CmnDataManager();
            DataTable firstTable = new DataTable();
            if (Action == "GetBranch")
            {
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_BRANCH_MANAGER", "BranchCount", d);
                if (rsltCode.isSuccessful)
                {
                    if (rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                    {
                        firstTable = rsltCode.dstResult.Tables[0];
                        var CatResul = JsonConvert.SerializeObject(firstTable);
                        return Json(CatResul);
                    }
                    else
                        return Json(null);
                }
                else
                    return Json(null);
            }
            return Json(null);
        }
        #endregion

        #region PAYROLL REGISTER
        public IActionResult PayrollRegister()
        {
            string message = "";
            try
            {

            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        [HttpPost]
        public IActionResult PayrollRegister(string PR_BRANCH, string PR_SEGMENT, string PR_MONTH, string PR_YEAR, string PR_CATEGORY)
        {
            string message = "";
            try
            {
                HttpResponseMessage file = new HttpResponseMessage();
                PY007Model entity = new PY007Model();
                entity.Branch = PR_BRANCH;
                entity.Segment = PR_SEGMENT;
                entity.Month = PR_MONTH != null ? Convert.ToInt32(PR_MONTH) : null;
                entity.Year = PR_YEAR != null ? Convert.ToInt32(PR_YEAR) : null;
                entity.Category = PR_CATEGORY;
                var DomainName = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetSection("CHRIS_REPORTING")["API_URL"];
                string BASE_URL = DomainName + "api/PayrollReporting/PayrollRegister/";
                using (HttpClient clienFNREP08 = new HttpClient())
                {
                    using (HttpRequestMessage req310 = new HttpRequestMessage(HttpMethod.Post, BASE_URL))
                    {
                        req310.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        string dataAsJson = System.Text.Json.JsonSerializer.Serialize(entity,
                        new JsonSerializerOptions() { WriteIndented = false });
                        message += dataAsJson + Environment.NewLine;
                        var content = new StringContent(dataAsJson, Encoding.UTF8, "application/json");
                        req310.Content = content;
                        req310.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                        HttpResponseMessage response = clienFNREP08.SendAsync(req310).Result;
                        int StatusCode = (int)response.StatusCode;
                        string StatusDesc = response.StatusCode.ToString();
                        if (StatusCode == 200)
                        {
                            file = response;
                            return File(file.Content.ReadAsByteArrayAsync().Result, "application/octet-stream", "PY007_" + DateTime.Now.ToString("dd_MM_yyyy") + ".pdf");
                        }
                        else
                        {
                            message = "Code : " + StatusCode + " | Description : " + StatusDesc;
                            return View();
                        }
                        //return responsse;
                    }
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        [HttpPost]
        public JsonResult GetDataForPayrollRegister(string Action)
        {
            Dictionary<string, object> d = new Dictionary<string, object>();
            CmnDataManager objCmnDataManager = new CmnDataManager();
            DataTable firstTable = new DataTable();
            if (Action == "GetBranch")
            {
                Result rsltCode = objCmnDataManager.GetData("CHRIS_SP_BRANCH_MANAGER", "BranchCount", d);
                if (rsltCode.isSuccessful)
                {
                    if (rsltCode.dstResult.Tables.Count > 0 && rsltCode.dstResult.Tables[0].Rows.Count > 0)
                    {
                        firstTable = rsltCode.dstResult.Tables[0];
                        var CatResul = JsonConvert.SerializeObject(firstTable);
                        return Json(CatResul);
                    }
                    else
                        return Json(null);
                }
                else
                    return Json(null);
            }
            return Json(null);
        }
        #endregion

        #region E FORM
        public IActionResult EForm()
        {
            string message = "";
            try
            {

            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        [HttpPost]
        public IActionResult EForm(string EF_FIRST_NAME, string EF_LAST_NAME, string EF_RATE, string EF_INITIATING_OFFICER, string EF_SENIOR_OFFICER)
        {
            string message = "";
            try
            {
                HttpResponseMessage file = new HttpResponseMessage();
                PY013Model entity = new PY013Model();
                entity.Name = EF_FIRST_NAME;
                entity.LastName = EF_LAST_NAME;
                entity.Rate = EF_RATE != null ? Convert.ToDecimal(EF_RATE) : null;
                entity.InitOfficer = EF_INITIATING_OFFICER;
                entity.SeniorOfficer = EF_SENIOR_OFFICER;
                var DomainName = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetSection("CHRIS_REPORTING")["API_URL"];
                string BASE_URL = DomainName + "api/PayrollReporting/EForm/";
                using (HttpClient clienFNREP08 = new HttpClient())
                {
                    using (HttpRequestMessage req310 = new HttpRequestMessage(HttpMethod.Post, BASE_URL))
                    {
                        req310.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        string dataAsJson = System.Text.Json.JsonSerializer.Serialize(entity,
                        new JsonSerializerOptions() { WriteIndented = false });
                        message += dataAsJson + Environment.NewLine;
                        var content = new StringContent(dataAsJson, Encoding.UTF8, "application/json");
                        req310.Content = content;
                        req310.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                        HttpResponseMessage response = clienFNREP08.SendAsync(req310).Result;
                        int StatusCode = (int)response.StatusCode;
                        string StatusDesc = response.StatusCode.ToString();
                        if (StatusCode == 200)
                        {
                            file = response;
                            return File(file.Content.ReadAsByteArrayAsync().Result, "application/octet-stream", "PY013_" + DateTime.Now.ToString("dd_MM_yyyy") + ".pdf");
                        }
                        else
                        {
                            message = "Code : " + StatusCode + " | Description : " + StatusDesc;
                            return View();
                        }
                        //return responsse;
                    }
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        #endregion

        #region FTE ANALYSIS
        public IActionResult FTEAnalysis()
        {
            string message = "";
            try
            {

            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        [HttpPost]
        public IActionResult FTEAnalysis(string FA_MONTH_YEAR, string FA_SEGMENT, string FA_BRANCH, string FA_PNO)
        {
            string message = "";
            try
            {
                HttpResponseMessage file = new HttpResponseMessage();
                PY014Model entity = new PY014Model();
                entity.Branch = FA_BRANCH;
                entity.MonthYear = FA_MONTH_YEAR != null ? Convert.ToInt32(FA_MONTH_YEAR) : null;
                entity.PNO = FA_PNO != null ? Convert.ToInt32(FA_PNO) : null;
                entity.Segment = FA_SEGMENT;
                var DomainName = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetSection("CHRIS_REPORTING")["API_URL"];
                string BASE_URL = DomainName + "api/PayrollReporting/FTEAnalysis/";
                using (HttpClient clienFNREP08 = new HttpClient())
                {
                    using (HttpRequestMessage req310 = new HttpRequestMessage(HttpMethod.Post, BASE_URL))
                    {
                        req310.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        string dataAsJson = System.Text.Json.JsonSerializer.Serialize(entity,
                        new JsonSerializerOptions() { WriteIndented = false });
                        message += dataAsJson + Environment.NewLine;
                        var content = new StringContent(dataAsJson, Encoding.UTF8, "application/json");
                        req310.Content = content;
                        req310.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                        HttpResponseMessage response = clienFNREP08.SendAsync(req310).Result;
                        int StatusCode = (int)response.StatusCode;
                        string StatusDesc = response.StatusCode.ToString();
                        if (StatusCode == 200)
                        {
                            file = response;
                            return File(file.Content.ReadAsByteArrayAsync().Result, "application/octet-stream", "PY014_" + DateTime.Now.ToString("dd_MM_yyyy") + ".pdf");
                        }
                        else
                        {
                            message = "Code : " + StatusCode + " | Description : " + StatusDesc;
                            return View();
                        }
                        //return responsse;
                    }
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        #endregion

        #region MONTHLY SALARY INTER
        public IActionResult MonthlySalaryInter()
        {
            string message = "";
            try
            {

            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        [HttpPost]
        public IActionResult MonthlySalaryInter(string MSI_MONTH, string MSI_BRANCH, string MSI_YEAR)
        {
            string message = "";
            try
            {
                HttpResponseMessage file = new HttpResponseMessage();
                PY015Model entity = new PY015Model();
                entity.Branch = MSI_BRANCH;
                entity.Month = MSI_MONTH != null ? Convert.ToInt32(MSI_MONTH) : null;
                entity.Year = MSI_YEAR != null ? Convert.ToInt32(MSI_YEAR) : null;
                var DomainName = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetSection("CHRIS_REPORTING")["API_URL"];
                string BASE_URL = DomainName + "api/PayrollReporting/MonthlySalaryInter/";
                using (HttpClient clienFNREP08 = new HttpClient())
                {
                    using (HttpRequestMessage req310 = new HttpRequestMessage(HttpMethod.Post, BASE_URL))
                    {
                        req310.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        string dataAsJson = System.Text.Json.JsonSerializer.Serialize(entity,
                        new JsonSerializerOptions() { WriteIndented = false });
                        message += dataAsJson + Environment.NewLine;
                        var content = new StringContent(dataAsJson, Encoding.UTF8, "application/json");
                        req310.Content = content;
                        req310.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                        HttpResponseMessage response = clienFNREP08.SendAsync(req310).Result;
                        int StatusCode = (int)response.StatusCode;
                        string StatusDesc = response.StatusCode.ToString();
                        if (StatusCode == 200)
                        {
                            file = response;
                            return File(file.Content.ReadAsByteArrayAsync().Result, "application/octet-stream", "PY015_" + DateTime.Now.ToString("dd_MM_yyyy") + ".pdf");
                        }
                        else
                        {
                            message = "Code : " + StatusCode + " | Description : " + StatusDesc;
                            return View();
                        }
                        //return responsse;
                    }
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        #endregion

        #region SHIFT ENTRY REPORT
        public IActionResult ShiftEntryReport()
        {
            string message = "";
            try
            {

            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        [HttpPost]
        public IActionResult ShiftEntryReport(string SER_MONTH, string SER_FPNO, string SER_EPNO)
        {
            string message = "";
            try
            {
                HttpResponseMessage file = new HttpResponseMessage();
                PY016Model entity = new PY016Model();
                entity.Month = SER_MONTH != null ? Convert.ToInt32(SER_MONTH) : null;
                entity.FPNO = SER_FPNO != null ? Convert.ToDecimal(SER_FPNO) : null;
                entity.EPNO = SER_EPNO != null ? Convert.ToDecimal(SER_EPNO) : null;
                var DomainName = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetSection("CHRIS_REPORTING")["API_URL"];
                string BASE_URL = DomainName + "api/PayrollReporting/ShiftEntryReport/";
                using (HttpClient clienFNREP08 = new HttpClient())
                {
                    using (HttpRequestMessage req310 = new HttpRequestMessage(HttpMethod.Post, BASE_URL))
                    {
                        req310.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        string dataAsJson = System.Text.Json.JsonSerializer.Serialize(entity,
                        new JsonSerializerOptions() { WriteIndented = false });
                        message += dataAsJson + Environment.NewLine;
                        var content = new StringContent(dataAsJson, Encoding.UTF8, "application/json");
                        req310.Content = content;
                        req310.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                        HttpResponseMessage response = clienFNREP08.SendAsync(req310).Result;
                        int StatusCode = (int)response.StatusCode;
                        string StatusDesc = response.StatusCode.ToString();
                        if (StatusCode == 200)
                        {
                            file = response;
                            return File(file.Content.ReadAsByteArrayAsync().Result, "application/octet-stream", "PY016_" + DateTime.Now.ToString("dd_MM_yyyy") + ".pdf");
                        }
                        else
                        {
                            message = "Code : " + StatusCode + " | Description : " + StatusDesc;
                            return View();
                        }
                        //return responsse;
                    }
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        #endregion

        #region EXCISE DEDUCTION REPORT
        public IActionResult ExciseDeductionReport()
        {
            string message = "";
            try
            {

            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        [HttpPost]
        public IActionResult ExciseDeductionReport(string ED_CITY, string ED_SEGMENT, string ED_MONTH, string ED_YEAR, string ED_CATEGORY)
        {
            string message = "";
            try
            {
                HttpResponseMessage file = new HttpResponseMessage();
                PY023Model entity = new PY023Model();
                entity.City = ED_CITY;
                entity.Segment = ED_SEGMENT;
                entity.Category = ED_CATEGORY;
                entity.Month = ED_MONTH;
                entity.Year = ED_YEAR;
                var DomainName = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetSection("CHRIS_REPORTING")["API_URL"];
                string BASE_URL = DomainName + "api/PayrollReporting/ExciseDeductionReport/";
                using (HttpClient clienFNREP08 = new HttpClient())
                {
                    using (HttpRequestMessage req310 = new HttpRequestMessage(HttpMethod.Post, BASE_URL))
                    {
                        req310.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        string dataAsJson = System.Text.Json.JsonSerializer.Serialize(entity,
                        new JsonSerializerOptions() { WriteIndented = false });
                        message += dataAsJson + Environment.NewLine;
                        var content = new StringContent(dataAsJson, Encoding.UTF8, "application/json");
                        req310.Content = content;
                        req310.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                        HttpResponseMessage response = clienFNREP08.SendAsync(req310).Result;
                        int StatusCode = (int)response.StatusCode;
                        string StatusDesc = response.StatusCode.ToString();
                        if (StatusCode == 200)
                        {
                            file = response;
                            return File(file.Content.ReadAsByteArrayAsync().Result, "application/octet-stream", "PY023_" + DateTime.Now.ToString("dd_MM_yyyy") + ".pdf");
                        }
                        else
                        {
                            message = "Code : " + StatusCode + " | Description : " + StatusDesc;
                            return View();
                        }
                        //return responsse;
                    }
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        #endregion

        #region ANNUAL 10 C BONUS
        public IActionResult Annual10CBonus()
        {
            string message = "";
            try
            {

            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        [HttpPost]
        public IActionResult Annual10CBonus(string AB_YEAR, string AB_BRANCH, string AB_SEGMENT)
        {
            string message = "";
            try
            {
                HttpResponseMessage file = new HttpResponseMessage();
                PY032Model entity = new PY032Model();
                entity.Year = AB_YEAR != null ? Convert.ToInt32(AB_YEAR) : null;
                entity.Branch = AB_BRANCH;
                entity.Segment = AB_SEGMENT;
                var DomainName = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetSection("CHRIS_REPORTING")["API_URL"];
                string BASE_URL = DomainName + "api/PayrollReporting/Annual10CBonus/";
                using (HttpClient clienFNREP08 = new HttpClient())
                {
                    using (HttpRequestMessage req310 = new HttpRequestMessage(HttpMethod.Post, BASE_URL))
                    {
                        req310.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        string dataAsJson = System.Text.Json.JsonSerializer.Serialize(entity,
                        new JsonSerializerOptions() { WriteIndented = false });
                        message += dataAsJson + Environment.NewLine;
                        var content = new StringContent(dataAsJson, Encoding.UTF8, "application/json");
                        req310.Content = content;
                        req310.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                        HttpResponseMessage response = clienFNREP08.SendAsync(req310).Result;
                        int StatusCode = (int)response.StatusCode;
                        string StatusDesc = response.StatusCode.ToString();
                        if (StatusCode == 200)
                        {
                            file = response;
                            return File(file.Content.ReadAsByteArrayAsync().Result, "application/octet-stream", "PY032_" + DateTime.Now.ToString("dd_MM_yyyy") + ".pdf");
                        }
                        else
                        {
                            message = "Code : " + StatusCode + " | Description : " + StatusDesc;
                            return View();
                        }
                        //return responsse;
                    }
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        #endregion

        #region ANNUAL 10C PAYMENT REPORT
        public IActionResult Annual10CPaymentReport()
        {
            string message = "";
            try
            {

            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        [HttpPost]
        public IActionResult Annual10CPaymentReport(string ABR_YEAR, string ABR_BRANCH, string ABR_SEGMENT)
        {
            string message = "";
            try
            {
                HttpResponseMessage file = new HttpResponseMessage();
                PY033Model entity = new PY033Model();
                entity.Year = ABR_YEAR;
                entity.Branch = ABR_BRANCH;
                entity.Segment = ABR_SEGMENT;
                var DomainName = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetSection("CHRIS_REPORTING")["API_URL"];
                string BASE_URL = DomainName + "api/PayrollReporting/Annual10CPaymentReport/";
                using (HttpClient clienFNREP08 = new HttpClient())
                {
                    using (HttpRequestMessage req310 = new HttpRequestMessage(HttpMethod.Post, BASE_URL))
                    {
                        req310.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        string dataAsJson = System.Text.Json.JsonSerializer.Serialize(entity,
                        new JsonSerializerOptions() { WriteIndented = false });
                        message += dataAsJson + Environment.NewLine;
                        var content = new StringContent(dataAsJson, Encoding.UTF8, "application/json");
                        req310.Content = content;
                        req310.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                        HttpResponseMessage response = clienFNREP08.SendAsync(req310).Result;
                        int StatusCode = (int)response.StatusCode;
                        string StatusDesc = response.StatusCode.ToString();
                        if (StatusCode == 200)
                        {
                            file = response;
                            return File(file.Content.ReadAsByteArrayAsync().Result, "application/octet-stream", "PY033_" + DateTime.Now.ToString("dd_MM_yyyy") + ".pdf");
                        }
                        else
                        {
                            message = "Code : " + StatusCode + " | Description : " + StatusDesc;
                            return View();
                        }
                        //return responsse;
                    }
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        #endregion

        #region TEN C BONUS RECIEPT
        public IActionResult TenCBonusReciept()
        {
            string message = "";
            try
            {

            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        [HttpPost]
        public IActionResult TenCBonusReciept(string BRR_DATE, string BRR_DECIMAL)
        {
            string message = "";
            try
            {
                HttpResponseMessage file = new HttpResponseMessage();
                PY034Model entity = new PY034Model();
                entity.DateTime = BRR_DATE != null ? Convert.ToDateTime(BRR_DATE) : null;
                entity.Bonus = BRR_DECIMAL != null ? Convert.ToDecimal(BRR_DECIMAL) : null;
                var DomainName = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetSection("CHRIS_REPORTING")["API_URL"];
                string BASE_URL = DomainName + "api/PayrollReporting/TenCBonusReciept/";
                using (HttpClient clienFNREP08 = new HttpClient())
                {
                    using (HttpRequestMessage req310 = new HttpRequestMessage(HttpMethod.Post, BASE_URL))
                    {
                        req310.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        string dataAsJson = System.Text.Json.JsonSerializer.Serialize(entity,
                        new JsonSerializerOptions() { WriteIndented = false });
                        message += dataAsJson + Environment.NewLine;
                        var content = new StringContent(dataAsJson, Encoding.UTF8, "application/json");
                        req310.Content = content;
                        req310.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                        HttpResponseMessage response = clienFNREP08.SendAsync(req310).Result;
                        int StatusCode = (int)response.StatusCode;
                        string StatusDesc = response.StatusCode.ToString();
                        if (StatusCode == 200)
                        {
                            file = response;
                            return File(file.Content.ReadAsByteArrayAsync().Result, "application/octet-stream", "PY034_" + DateTime.Now.ToString("dd_MM_yyyy") + ".pdf");
                        }
                        else
                        {
                            message = "Code : " + StatusCode + " | Description : " + StatusDesc;
                            return View();
                        }
                        //return responsse;
                    }
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        #endregion

        #region MONTHLY TAX PAYMENT REPORT
        public IActionResult MonthlyTaxPaymentReport()
        {
            string message = "";
            try
            {

            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        [HttpPost]
        public IActionResult MonthlyTaxPaymentReport(string MTP_BRANCH, string MTP_RUNDATE, string MTP_MONTH, string MTP_YEAR)
        {
            string message = "";
            try
            {
                HttpResponseMessage file = new HttpResponseMessage();
                PY024Model entity = new PY024Model();
                entity.RunDate = MTP_RUNDATE != null ? Convert.ToDateTime(MTP_RUNDATE) : null;
                entity.Month = MTP_MONTH != null ? Convert.ToInt32(MTP_MONTH) : null;
                entity.Year = MTP_YEAR != null ? Convert.ToInt32(MTP_YEAR) : null;
                entity.Branch = MTP_BRANCH;
                var DomainName = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetSection("CHRIS_REPORTING")["API_URL"];
                string BASE_URL = DomainName + "api/PayrollReporting/MonthlyTaxPaymentReport/";
                using (HttpClient clienFNREP08 = new HttpClient())
                {
                    using (HttpRequestMessage req310 = new HttpRequestMessage(HttpMethod.Post, BASE_URL))
                    {
                        req310.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        string dataAsJson = System.Text.Json.JsonSerializer.Serialize(entity,
                        new JsonSerializerOptions() { WriteIndented = false });
                        message += dataAsJson + Environment.NewLine;
                        var content = new StringContent(dataAsJson, Encoding.UTF8, "application/json");
                        req310.Content = content;
                        req310.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                        HttpResponseMessage response = clienFNREP08.SendAsync(req310).Result;
                        int StatusCode = (int)response.StatusCode;
                        string StatusDesc = response.StatusCode.ToString();
                        if (StatusCode == 200)
                        {
                            file = response;
                            return File(file.Content.ReadAsByteArrayAsync().Result, "application/octet-stream", "PY024_" + DateTime.Now.ToString("dd_MM_yyyy") + ".pdf");
                        }
                        else
                        {
                            message = "Code : " + StatusCode + " | Description : " + StatusDesc;
                            return View();
                        }
                        //return responsse;
                    }
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        #endregion
        public IActionResult ActurialReport()
        {
            return View();
        }
        public IActionResult ActurialReportDownload()
        {
            return View();
        }
        public IActionResult AnnualIncentiveBonus()
        {
            return View();
        }
        public IActionResult AnnualIncentivePayment()
        {
            return View();
        }
        public IActionResult ArearsSummaryReport()
        {
            return View();
        }
        public IActionResult ArrearsIndividual()
        {
            return View();
        }
        public IActionResult ArrearsReciept()
        {
            return View();
        }
        public IActionResult CHRS()
        {
            return View();
        }
        public IActionResult DepartmentWiseOV()
        {
            return View();
        }
        public IActionResult DepartmentWisePayrollReportOfficers()
        {
            return View();
        }
        public IActionResult FTE010()
        {
            return View();
        }
        public IActionResult FTE010JAW()
        {
            return View();
        }
        public IActionResult FTE010SEG()
        {
            return View();
        }
        public IActionResult FTE011()
        {
            return View();
        }
        public IActionResult FTE011B()
        {
            return View();
        }
        public IActionResult FTEDataEntryList()
        {
            return View();
        }
        public IActionResult FTEDepartmentAndHeadsGroupWise()
        {
            return View();
        }
        public IActionResult FTEDepartmentHeadsGroupWise()
        {
            return View();
        }
        public IActionResult FTEDesignationGroup()
        {
            return View();
        }
        public IActionResult FTEDesignationSegment()
        {
            return View();
        }
        public IActionResult FteDesignationSegmentWise()
        {
            return View();
        }
        public IActionResult FTEGroupAndHeadsSegmentWise()
        {
            return View();
        }
        public IActionResult FTEGroupHeadsSegmentWise()
        {
            return View();
        }
        public IActionResult IcentiveBonusReciept()
        {
            return View();
        }
        public IActionResult IncomeTaxPrerequiste()
        {
            return View();
        }
        public IActionResult MonthlyTax()
        {
            return View();
        }
        public IActionResult OvertimeArrearsDetail()
        {
            return View();
        }
        public IActionResult PY009()
        {
            return View();
        }
        public IActionResult RankingReport()
        {
            return View();
        }
        public IActionResult SalaryArrearDetailReport()
        {
            return View();
        }
        public IActionResult SalaryWorksheet()
        {
            return View();
        }
    }
}
