﻿using DocumentFormat.OpenXml.InkML;
using CHRIS.Data;
using CHRIS.Models;
using CHRIS.Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;

namespace CHRIS.Controllers
{
    public class Manage_UsersController : Controller
    {
        CHRIS_Context context = new CHRIS_Context();
        public ActionResult AddUser(int L = 0)
        {
            if (HttpContext.Session.GetString("USER_ID") != null)
            {
                if (DAL.CheckFunctionValidity("Manage_Users", "AddUser", HttpContext.Session.GetString("USER_ID").ToString()))
                {
                    string message = "";
                    AssignRoles_Custom entity = new AssignRoles_Custom();
                    try
                    {
                        if (L != 0)
                        {
                            string SessionUser = HttpContext.Session.GetString("USER_ID").ToString();
                            entity.Entity = context.CHRIS_LOGIN.Where(m => m.PLOG_ID == L && m.PLOG_USER_ID != SessionUser).FirstOrDefault();
                            if (entity.Entity != null)
                            {
                                List<int?> PreviousRoles = context.CHRIS_ASSIGN_LOGIN_RIGHTS.Where(m => m.PALR_LOG_ID == L && m.PALR_STATUS == true).Select(m => m.PALR_ROLE_ID).ToList();
                                List<SelectListItem> items = new SelectList(context.CHRIS_ROLES.Where(m => m.PR_NAME != "ADMIN" && m.PR_STATUS == "true" && m.PR_ISAUTH == true).OrderBy(m => m.PR_ID).ToList(), "PR_ID", "PR_NAME", 0).ToList();
                                foreach (SelectListItem item in items)
                                {
                                    int? CurrentRId = Convert.ToInt32(item.Value);
                                    if (PreviousRoles.Contains(CurrentRId))
                                        item.Selected = true;
                                }
                                entity.RolesList = items;
                                ViewBag.FormType = "Edit";
                                return View(entity);
                            }
                            else
                            {
                                entity.Entity = new CHRIS_LOGIN();
                                entity.Entity.PLOG_ID = 0;
                                List<SelectListItem> items = new SelectList(context.CHRIS_ROLES.Where(m => m.PR_NAME != "ADMIN" && m.PR_STATUS == "true" && m.PR_ISAUTH == true).OrderBy(m => m.PR_ID).ToList(), "PR_ID", "PR_NAME", 0).ToList();
                                entity.RolesList = items;
                                message = "Exception Occur while fetching your record on User Id = " + L;
                                return View(entity);
                            }
                        }
                        else
                        {
                            entity.Entity = new CHRIS_LOGIN();
                            entity.Entity.PLOG_ID = 0;
                            List<SelectListItem> items = new SelectList(context.CHRIS_ROLES.Where(m => m.PR_NAME != "ADMIN" && m.PR_STATUS == "true" && m.PR_ISAUTH == true).OrderBy(m => m.PR_ID).ToList(), "PR_ID", "PR_NAME", 0).ToList();
                            entity.RolesList = items;
                            return View(entity);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                        entity.Entity = new CHRIS_LOGIN();
                        entity.Entity.PLOG_ID = 0;
                        List<SelectListItem> items = new SelectList(context.CHRIS_ROLES.Where(m => m.PR_NAME != "ADMIN" && m.PR_STATUS == "true" && m.PR_ISAUTH == true).OrderBy(m => m.PR_ID).ToList(), "PR_ID", "PR_NAME", 0).ToList();
                        entity.RolesList = items;
                        return View(entity);
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
        }
        [HttpPost]
        public ActionResult AddUser(AssignRoles_Custom db_table)
        {
            if (HttpContext.Session.GetString("USER_ID") != null)
            {
                if (DAL.CheckFunctionValidity("Manage_Users", "AddUser", HttpContext.Session.GetString("USER_ID").ToString()))
                {
                    string message = "";
                    try
                    {
                        CHRIS_LOGIN UpdateEntity = new CHRIS_LOGIN();
                        CHRIS_LOGIN CheckIfExist = new CHRIS_LOGIN();
                        if (db_table.Entity.PLOG_ID == 0)
                        {
                            CheckIfExist = context.CHRIS_LOGIN.Where(m => m.PLOG_USER_ID.ToLower() == db_table.Entity.PLOG_USER_ID.ToLower()).FirstOrDefault();
                            if (CheckIfExist == null)
                            {
                                UpdateEntity.PLOG_ID = Convert.ToInt32(context.CHRIS_LOGIN.Max(m => (decimal?)m.PLOG_ID)) + 1;
                                UpdateEntity.PLOG_DATA_ENTRY_DATETIME = DateTime.Now;
                                UpdateEntity.PLOG_USER_ID = db_table.Entity.PLOG_USER_ID;
                                UpdateEntity.PLOG_USER_NAME = db_table.Entity.PLOG_USER_NAME;
                                UpdateEntity.PLOG_USER_LAST_NAME = db_table.Entity.PLOG_USER_LAST_NAME;
                                UpdateEntity.PLOG_USER_GE_ID = db_table.Entity.PLOG_USER_GE_ID;
                                UpdateEntity.PLOG_EMAIL = db_table.Entity.PLOG_EMAIL;
                                UpdateEntity.PLOG_RIST_ID = db_table.Entity.PLOG_RIST_ID;
                                UpdateEntity.PLOG_USER_STATUS = db_table.Entity.PLOG_USER_STATUS;
                                UpdateEntity.PLOG_MAKER_ID = HttpContext.Session.GetString("USER_ID").ToString();
                                if (UpdateEntity.PLOG_MAKER_ID == "Admin123")
                                {
                                    UpdateEntity.PLOG_ISAUTH = true;
                                    UpdateEntity.PLOG_CHECKER_ID = UpdateEntity.PLOG_MAKER_ID;
                                }
                                else
                                {
                                    UpdateEntity.PLOG_ISAUTH = false;
                                    UpdateEntity.PLOG_CHECKER_ID = null;
                                }
                                context.CHRIS_LOGIN.Add(UpdateEntity);
                            }
                            else
                            {
                                message = "A User with the given " + db_table.Entity.PLOG_USER_ID + " Already Exist";
                            }
                        }
                        else
                        {
                            UpdateEntity = context.CHRIS_LOGIN.Where(m => m.PLOG_ID == db_table.Entity.PLOG_ID).FirstOrDefault();
                            UpdateEntity.PLOG_DATA_EDIT_DATETIME = DateTime.Now;
                            UpdateEntity = context.CHRIS_LOGIN.Where(m => m.PLOG_ID == db_table.Entity.PLOG_ID).FirstOrDefault();
                            UpdateEntity.PLOG_DATA_EDIT_DATETIME = DateTime.Now;
                            UpdateEntity.PLOG_USER_ID = db_table.Entity.PLOG_USER_ID;
                            UpdateEntity.PLOG_USER_NAME = db_table.Entity.PLOG_USER_NAME;
                            UpdateEntity.PLOG_USER_LAST_NAME = db_table.Entity.PLOG_USER_LAST_NAME;
                            UpdateEntity.PLOG_USER_GE_ID = db_table.Entity.PLOG_USER_GE_ID;
                            UpdateEntity.PLOG_EMAIL = db_table.Entity.PLOG_EMAIL;
                            UpdateEntity.PLOG_RIST_ID = db_table.Entity.PLOG_RIST_ID;
                            UpdateEntity.PLOG_USER_STATUS = db_table.Entity.PLOG_USER_STATUS;
                            UpdateEntity.PLOG_MAKER_ID = HttpContext.Session.GetString("USER_ID").ToString();
                            if (UpdateEntity.PLOG_MAKER_ID == "Admin123")
                            {
                                UpdateEntity.PLOG_ISAUTH = true;
                                UpdateEntity.PLOG_CHECKER_ID = UpdateEntity.PLOG_MAKER_ID;
                            }
                            else
                            {
                                UpdateEntity.PLOG_ISAUTH = false;
                                UpdateEntity.PLOG_CHECKER_ID = null;
                            }

                            context.Entry(UpdateEntity).State = EntityState.Modified;
                        }
                        int RowsAffected = context.SaveChanges();
                        if (RowsAffected > 0)
                        {
                            string TempNewUser = "";
                            string TempRemoveUser = "";
                            if (db_table.SelectedRoles != null)
                            {
                                #region Get Groups for Log Table
                                foreach (int RoleId in db_table.SelectedRoles)
                                {
                                    CHRIS_ASSIGN_LOGIN_RIGHTS RoleTbl = context.CHRIS_ASSIGN_LOGIN_RIGHTS.Where(m => m.PALR_ROLE_ID == RoleId && m.PALR_USER_ID == UpdateEntity.PLOG_USER_ID && m.PALR_STATUS == true).FirstOrDefault();
                                    if (RoleTbl == null)
                                    {
                                        CHRIS_ROLES GetRoleName = context.CHRIS_ROLES.Where(m => m.PR_ID == RoleId).FirstOrDefault();
                                        TempNewUser += GetRoleName.PR_NAME + ",";
                                    }
                                }
                                #endregion

                                List<CHRIS_ASSIGN_LOGIN_RIGHTS> DeActivatedRoles = context.CHRIS_ASSIGN_LOGIN_RIGHTS.Where(m => m.PALR_LOG_ID == db_table.Entity.PLOG_ID && !db_table.SelectedRoles.Contains(m.PALR_ROLE_ID)).ToList();
                                if (DeActivatedRoles.Count > 0)
                                {
                                    foreach (CHRIS_ASSIGN_LOGIN_RIGHTS EachRolesForUpdate in DeActivatedRoles)
                                    {
                                        CHRIS_ROLES RoleTbl = context.CHRIS_ROLES.Where(m => m.PR_ID == EachRolesForUpdate.PALR_ROLE_ID).FirstOrDefault();
                                        TempRemoveUser += RoleTbl.PR_NAME + ",";
                                        EachRolesForUpdate.PALR_STATUS = false;
                                        context.Entry(EachRolesForUpdate).State = EntityState.Modified;
                                    }
                                }
                                int AssignRoleId = 0;
                                foreach (int? SelectedRolesId in db_table.SelectedRoles)
                                {
                                    if (SelectedRolesId != null)
                                    {
                                        CHRIS_ASSIGN_LOGIN_RIGHTS EachRolesToUpdate = context.CHRIS_ASSIGN_LOGIN_RIGHTS.Where(m => m.PALR_LOG_ID == db_table.Entity.PLOG_ID && m.PALR_ROLE_ID == SelectedRolesId).FirstOrDefault();
                                        if (EachRolesToUpdate == null)
                                        {
                                            EachRolesToUpdate = new CHRIS_ASSIGN_LOGIN_RIGHTS();
                                            if (AssignRoleId == 0)
                                            {
                                                EachRolesToUpdate.PALR_ID = Convert.ToInt32(context.CHRIS_ASSIGN_LOGIN_RIGHTS.Max(m => (decimal?)m.PALR_ID)) + 1;
                                                AssignRoleId = EachRolesToUpdate.PALR_ID;
                                            }
                                            else
                                            {
                                                AssignRoleId = (AssignRoleId + 1);
                                                EachRolesToUpdate.PALR_ID = AssignRoleId;
                                            }
                                            EachRolesToUpdate.PALR_ROLE_ID = SelectedRolesId;
                                            EachRolesToUpdate.PALR_USER_ID = db_table.Entity.PLOG_USER_ID;
                                            EachRolesToUpdate.PALR_STATUS = true;
                                            EachRolesToUpdate.PALR_MAKER_ID = HttpContext.Session.GetString("USER_ID").ToString();
                                            EachRolesToUpdate.PALR_CHECKER_ID = null;
                                            EachRolesToUpdate.PALR_DATA_ENTRY_DATETIME = DateTime.Now;
                                            EachRolesToUpdate.PALR_LOG_ID = UpdateEntity.PLOG_ID;
                                            context.CHRIS_ASSIGN_LOGIN_RIGHTS.Add(EachRolesToUpdate);
                                        }
                                        else
                                        {
                                            EachRolesToUpdate.PALR_MAKER_ID = HttpContext.Session.GetString("USER_ID").ToString();
                                            EachRolesToUpdate.PALR_STATUS = true;
                                            EachRolesToUpdate.PALR_DATA_EDIT_DATETIME = DateTime.Now;
                                            context.Entry(EachRolesToUpdate).State = EntityState.Modified;
                                        }
                                    }
                                }
                                RowsAffected = context.SaveChanges();
                                if (RowsAffected > 0)
                                {
                                    RowsAffected = 1;
                                    message = "Data Inserted Successfully " + RowsAffected + " Affected";
                                }
                            }
                            else
                            {
                                List<CHRIS_ASSIGN_LOGIN_RIGHTS> DeActivatedRoles = context.CHRIS_ASSIGN_LOGIN_RIGHTS.Where(m => m.PALR_LOG_ID == db_table.Entity.PLOG_ID).ToList();
                                if (DeActivatedRoles.Count > 0)
                                {
                                    foreach (CHRIS_ASSIGN_LOGIN_RIGHTS EachRolesForUpdate in DeActivatedRoles)
                                    {
                                        CHRIS_ROLES RoleTbl = context.CHRIS_ROLES.Where(m => m.PR_ID == EachRolesForUpdate.PALR_ROLE_ID).FirstOrDefault();
                                        TempRemoveUser += RoleTbl.PR_NAME + ",";
                                        EachRolesForUpdate.PALR_STATUS = false;
                                        context.Entry(EachRolesForUpdate).State = EntityState.Modified;
                                    }
                                    RowsAffected = context.SaveChanges();
                                }
                                else
                                    RowsAffected = 1;
                                if (RowsAffected > 0)
                                {
                                    RowsAffected = 1;
                                    message = "Data Inserted Successfully " + RowsAffected + " Affected";
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                    AssignRoles_Custom entity = new AssignRoles_Custom();
                    if (entity.RolesList != null)
                    {
                        entity.Entity = new CHRIS_LOGIN();
                        entity.Entity.PLOG_ID = db_table.Entity.PLOG_ID;
                        List<int?> PreviousRoles = context.CHRIS_ASSIGN_LOGIN_RIGHTS.Where(m => m.PALR_LOG_ID == db_table.Entity.PLOG_ID && m.PALR_STATUS == true).Select(m => m.PALR_ROLE_ID).ToList();
                        List<SelectListItem> items = new SelectList(context.CHRIS_ROLES.Where(m => m.PR_ID != 1 && m.PR_STATUS == "true" && m.PR_ISAUTH == true).OrderBy(m => m.PR_ID).ToList(), "PR_ID", "PR_NAME", 0).ToList();
                        foreach (SelectListItem item in items)
                        {
                            int? CurrentRId = Convert.ToInt32(item.Value);
                            if (PreviousRoles.Contains(CurrentRId))
                                item.Selected = true;
                        }
                        entity.RolesList = items;
                        return View(entity);
                    }
                    else
                    {
                        entity.Entity = new CHRIS_LOGIN();
                        entity.Entity.PLOG_ID = 0;
                        List<SelectListItem> items = new SelectList(context.CHRIS_ROLES.Where(m => m.PR_NAME != "ADMIN" && m.PR_STATUS == "true" && m.PR_ISAUTH == true).OrderBy(m => m.PR_ID).ToList(), "PR_ID", "PR_NAME", 0).ToList();
                        entity.RolesList = items;
                        return View(entity);
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
        }
        //[ChildActionOnly]
        public ActionResult AddUserView()
        {
            if (HttpContext.Session.GetString("USER_ID") == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                if (DAL.CheckFunctionValidity("Manage_Users", "AddUser", HttpContext.Session.GetString("USER_ID").ToString()))
                {
                    string SessionUser = HttpContext.Session.GetString("USER_ID").ToString();
                    List<CHRIS_USER_WITH_ROLES_LIST_VIEW> ViewData = context.CHRIS_USER_WITH_ROLES_LIST_VIEW.Where(m => m.PLOG_USER_ID != "Admin123" && m.PLOG_USER_ID != SessionUser && (m.PLOG_MAKER_ID != SessionUser || m.PLOG_ISAUTH == true)).OrderByDescending(m => m.PLOG_ID).ToList();
                    return PartialView(ViewData);
                }
                else
                    return PartialView(null);
            }
        }
        [HttpPost]
        public JsonResult AuthroizeUser(int LOG_ID)
        {
            if (HttpContext.Session.GetString("USER_ID") != null && Request.Headers["X-Requested-With"] == "XMLHttpRequest")
            {
                if (DAL.CheckFunctionValidity("Manage_Users", "AddUser", HttpContext.Session.GetString("USER_ID").ToString()))
                {
                    string message = "";
                    try
                    {
                        CHRIS_LOGIN UpdateEntity = context.CHRIS_LOGIN.Where(m => m.PLOG_ID == LOG_ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            if (UpdateEntity.PLOG_MAKER_ID != HttpContext.Session.GetString("USER_ID").ToString())
                            {
                                UpdateEntity.PLOG_ISAUTH = true;
                                UpdateEntity.PLOG_CHECKER_ID = HttpContext.Session.GetString("USER_ID").ToString();
                                UpdateEntity.PLOG_DATA_EDIT_DATETIME = DateTime.Now;
                                context.Entry(UpdateEntity).State = EntityState.Modified;
                                int RowCount = context.SaveChanges();
                                if (RowCount > 0)
                                {
                                    message = "User : " + UpdateEntity.PLOG_USER_NAME + " is successfully Authorized";
                                }
                            }
                            else
                                message = "Sorry, Maker cannot authorize the same record.";
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + LOG_ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid request");
                }
            }
            else
                return Json("Invalid request");
        }
        [HttpPost]
        public JsonResult DeleteFromAuthroizeUser(int LOG_ID)
        {
            if (HttpContext.Session.GetString("USER_ID") != null && Request.Headers["X-Requested-With"] == "XMLHttpRequest")
            {
                if (DAL.CheckFunctionValidity("Manage_Users", "AddUser", HttpContext.Session.GetString("USER_ID").ToString()))
                {
                    string message = "";
                    try
                    {
                        CHRIS_LOGIN UpdateEntity = context.CHRIS_LOGIN.Where(m => m.PLOG_ID == LOG_ID).FirstOrDefault();

                        #region Save Group Name Before Deleting
                        string GetGroupName = "";
                        CHRIS_USER_WITH_ROLES_LIST_VIEW GroupName = context.CHRIS_USER_WITH_ROLES_LIST_VIEW.Where(m => m.PLOG_USER_ID == UpdateEntity.PLOG_USER_ID).FirstOrDefault();
                        if (GroupName != null)
                        {
                            GetGroupName += GroupName.ASSIGNED_ROLES;
                        }
                        #endregion

                        if (UpdateEntity != null)
                        {
                            if (UpdateEntity.PLOG_MAKER_ID != HttpContext.Session.GetString("USER_ID").ToString())
                            {
                                context.CHRIS_LOGIN.Remove(UpdateEntity);
                                int RowCount = context.SaveChanges();
                                if (RowCount > 0)
                                {
                                    List<CHRIS_ASSIGN_LOGIN_RIGHTS> AssignUser = context.CHRIS_ASSIGN_LOGIN_RIGHTS.Where(m => m.PALR_USER_ID == UpdateEntity.PLOG_USER_ID).ToList();
                                    if (AssignUser.Count > 0)
                                    {
                                        RowCount = 0;
                                        foreach (CHRIS_ASSIGN_LOGIN_RIGHTS item in AssignUser)
                                        {
                                            context.CHRIS_ASSIGN_LOGIN_RIGHTS.Remove(item);
                                            RowCount = +context.SaveChanges();
                                        }
                                    }
                                    if (RowCount > 0)
                                    {
                                        message = "User : " + UpdateEntity.PLOG_USER_NAME + " is Rejected";
                                    }
                                }
                            }
                            else
                                message = "Sorry, Maker cannot reject the same record.";
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + LOG_ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid request");
                }
            }
            else
                return Json("Invalid request");
        }
    }
}
