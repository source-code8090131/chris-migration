USE [CHRIS_UAT]
GO
/****** Object:  StoredProcedure [dbo].[CHRIS_SP_TERMINATION_MANAGER] ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[CHRIS_SP_TERMINATION_MANAGER]
/*<Param>*/
@PR_P_NO numeric(6,0), --<descr></descr>
@PR_BRANCH varchar(3), --<descr></descr>
@PR_FIRST_NAME varchar(20), --<descr></descr>
@PR_LAST_NAME varchar(20), --<descr></descr>
@PR_DESIG varchar(3), --<descr></descr>
@PR_LEVEL varchar(3), --<descr></descr>
@PR_CATEGORY varchar(1), --<descr></descr>
@PR_FUNC_TITTLE1 varchar(30), --<descr></descr>
@PR_FUNC_TITTLE2 varchar(30), --<descr></descr>
@PR_JOINING_DATE datetime, --<descr></descr>
@PR_CONFIRM datetime, --<descr></descr>
@PR_ANNUAL_PACK numeric(11,0), --<descr></descr>
@PR_NATIONAL_TAX varchar(18), --<descr></descr>
@PR_ACCOUNT_NO varchar(14), --<descr></descr>
@PR_BANK_ID varchar(1), --<descr></descr>
@PR_ID_ISSUE datetime, --<descr></descr>
@PR_EXPIRY datetime, --<descr></descr>
@PR_TAX_INC numeric(7,0), --<descr></descr>
@PR_TAX_PAID numeric(7,0), --<descr></descr>
@PR_TAX_USED varchar(1), --<descr></descr>
@PR_CLOSE_FLAG varchar(1), --<descr></descr>
@PR_CONFIRM_ON datetime, --<descr></descr>
@PR_EXPECTED datetime, --<descr></descr>
@PR_TERMIN_TYPE varchar(1), --<descr></descr>
@PR_TERMIN_DATE datetime, --<descr></descr>
@PR_REASONS varchar(30), --<descr></descr>
@PR_RESIG_RECV varchar(1), --<descr></descr>
@PR_APP_RESIG varchar(1), --<descr></descr>
@PR_EXIT_INTER varchar(1), --<descr></descr>
@PR_ID_RETURN varchar(1), --<descr></descr>
@PR_NOTICE varchar(1), --<descr></descr>
@PR_ARTONY varchar(1), --<descr></descr>
@PR_DELETION varchar(1), --<descr></descr>
@PR_SETTLE varchar(1), --<descr></descr>
@PR_CONF_FLAG varchar(1), --<descr></descr>
@PR_LAST_INCREMENT datetime, --<descr></descr>
@PR_NEXT_INCREMENT datetime, --<descr></descr>
@PR_TRANSFER_DATE datetime, --<descr></descr>
@PR_PROMOTION_DATE datetime, --<descr></descr>
@PR_NEW_BRANCH varchar(3), --<descr></descr>
@PR_NEW_ANNUAL_PACK numeric(11,0), --<descr></descr>
@PR_TRANSFER numeric(1,0), --<descr></descr>
@PR_MONTH_AWARD numeric(3,0), --<descr></descr>
@PR_RELIGION varchar(15)=null, --<descr></descr>
@PR_ZAKAT_AMT numeric(5,0), --<descr></descr>
@PR_MED_FLAG varchar(1), --<descr></descr>
@PR_ATT_FLAG varchar(1), --<descr></descr>
@PR_PAY_FLAG varchar(2), --<descr></descr>
@PR_REP_NO numeric(6,0), --<descr></descr>
@PR_REFUND_AMT numeric(12,2), --<descr></descr>
@PR_REFUND_FOR varchar(9), --<descr></descr>
@PR_NTN_CERT_REC varchar(3), --<descr></descr>
@PR_NTN_CARD_REC varchar(3), --<descr></descr>
@PR_EMP_TYPE varchar(2), --<descr></descr>
@LOCATION varchar(40), --<descr></descr>
@PR_UserID VARCHAR(35), --<DESCR>Add By Talat</DESCR>
@ID INT  OUTPUT,  --<descr></descr>
@ActionType varchar(50)
,@oRetVal int = Null Output
/*</Param>*/
As

Set NoCount On
/*<Comment></Comment>*/


	If @ActionType = 'Save'
	Begin
		Exec CHRIS_SP_TERMINATION_UPDATE
	   			@PR_P_NO,
				@PR_TERMIN_TYPE,
				@PR_TERMIN_DATE,
				@PR_REASONS,
				@PR_RESIG_RECV,
				@PR_APP_RESIG,
				@PR_EXIT_INTER, 
				@PR_ID_RETURN, 
				@PR_NOTICE,
				@PR_ARTONY, 
				@PR_DELETION, 
				@PR_SETTLE , 
				@ID,
				@PR_UserID
	End
	Else If @ActionType = 'Update'
	Begin
		Exec CHRIS_SP_TERMINATION_UPDATE
	   			@PR_P_NO,
				@PR_TERMIN_TYPE,
				@PR_TERMIN_DATE,
				@PR_REASONS,
				@PR_RESIG_RECV,
				@PR_APP_RESIG,
				@PR_EXIT_INTER, 
				@PR_ID_RETURN, 
				@PR_NOTICE,
				@PR_ARTONY, 
				@PR_DELETION, 
				@PR_SETTLE , 
				@ID 

	End
	Else If @ActionType = 'Delete'
	Begin
		Exec CHRIS_SP_TERMINATION_DELETE
				@ID,@oRetVal
	End
	Else If @ActionType = 'Get'
	Begin
		Exec CHRIS_SP_PERSONNEL_GET
				@ID,@oRetVal
	End
	Else If @ActionType = 'List'
	Begin
--		Exec CHRIS_SP_TERMINATION_GETALL @oRetVal
		select rtrim(pr_first_name)+' '+ rtrim(pr_last_name) As PR_FIRST_NAME
			, PR_P_NO
			, PR_TERMIN_TYPE
			, PR_TERMIN_DATE
			, PR_REASONS
			, PR_RESIG_RECV
			, PR_APP_RESIG
			, PR_EXIT_INTER
			, PR_ID_RETURN
			, PR_NOTICE
			, PR_ARTONY
			, PR_DELETION
			, PR_SETTLE
			, PR_CLOSE_FLAG
			, PR_CONFIRM
			, PR_CONFIRM_ON
			, PR_JOINING_DATE
			, ID
		from personnel 
		order by pr_p_no
	End
	
	Else If @ActionType = 'P_LOV'
	Begin
		select rtrim(pr_first_name)+' '+ rtrim(pr_last_name) As PR_FIRST_NAME
			, pr_p_no
			, PR_TERMIN_TYPE
			, PR_TERMIN_DATE
			, PR_REASONS
			, PR_RESIG_RECV
			, PR_APP_RESIG
			, PR_EXIT_INTER
			, PR_ID_RETURN
			, PR_NOTICE
			, PR_ARTONY
			, PR_DELETION
			, PR_SETTLE
			, PR_CLOSE_FLAG
			, PR_CONFIRM
			, PR_CONFIRM_ON
			, PR_JOINING_DATE
			, ID
		from personnel 
		order by pr_p_no
	End
	Else If @ActionType = 'P_NO_EXISTS'
	BEGIN
		if EXISTS(select ID from Personnel where Pr_p_no = @PR_P_NO)
		BEGIN
			select rtrim(pr_first_name)+' '+ rtrim(pr_last_name) As PR_FIRST_NAME,pr_p_no 
				, PR_TERMIN_TYPE
				, PR_TERMIN_DATE
				, PR_REASONS
				, PR_RESIG_RECV
				, PR_APP_RESIG
				, PR_EXIT_INTER
				, PR_ID_RETURN
				, PR_NOTICE
				, PR_ARTONY
				, PR_DELETION
				, PR_SETTLE
				, PR_CLOSE_FLAG
				, PR_CONFIRM
				, PR_CONFIRM_ON
				, PR_JOINING_DATE
				, ID
			from personnel 
			WHERE pr_p_no = @PR_P_NO   
			order by pr_p_no
		END
		
		IF EXISTS(Select top(1) ID FROM  PERSONNEL  WHERE CAST(PR_P_NO AS VARCHAR) like CAST(@PR_P_NO AS VARCHAR) + '%' OR  ISNULL(CAST(@PR_P_NO AS VARCHAR),'') = '')  
		BEGIN
			select rtrim(pr_first_name)+' '+ rtrim(pr_last_name) As PR_FIRST_NAME,pr_p_no 
				, PR_TERMIN_TYPE
				, PR_TERMIN_DATE
				, PR_REASONS
				, PR_RESIG_RECV
				, PR_APP_RESIG
				, PR_EXIT_INTER
				, PR_ID_RETURN
				, PR_NOTICE
				, PR_ARTONY
				, PR_DELETION
				, PR_SETTLE
				, PR_CLOSE_FLAG
				, PR_CONFIRM
				, PR_CONFIRM_ON
				, PR_JOINING_DATE
				, ID
			from dbo.PERSONNEL 
			WHERE CAST(PR_P_NO AS VARCHAR) like CAST(@PR_P_NO AS VARCHAR) + '%'   
			order by pr_p_no
		END
	End

--	Else If @ActionType = 'ADDTEST'
--	Begin
--	SELECT
--		 pr_p_no,
--		 CAST(RTRIM(pr_first_name) AS VARCHAR) + ' ' + CAST(RTRIM(pr_last_name) AS VARCHAR) As Name,
--	     pr_confirm_on,
--		 pr_joining_date
--	FROM  personnel 
--	WHERE	 pr_p_no  = @PR_P_NO
--	
--	End
	Return
GO
/****** Object:  StoredProcedure [dbo].[CHRIS_SP_TERMINATION_UPDATE] ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[CHRIS_SP_TERMINATION_UPDATE]
/*<Param>*/
@PR_P_NO numeric(6,0), --<descr></descr>
@PR_TERMIN_TYPE varchar(1), --<descr></descr>
@PR_TERMIN_DATE datetime, --<descr></descr>
@PR_REASONS varchar(30), --<descr></descr>
@PR_RESIG_RECV varchar(1), --<descr></descr>
@PR_APP_RESIG varchar(1), --<descr></descr>
@PR_EXIT_INTER varchar(1), --<descr></descr>
@PR_ID_RETURN varchar(1), --<descr></descr>
@PR_NOTICE varchar(1), --<descr></descr>
@PR_ARTONY varchar(1), --<descr></descr>
@PR_DELETION varchar(1), --<descr></descr>
@PR_SETTLE varchar(1), --<descr></descr>
@ID INT, --<descr></descr>
@PR_UserID VARCHAR(35) --<DESCR>Add By Talat</DESCR>

AS

	--Update	PERSONNEL
	--Set
	--   	 PR_TERMIN_TYPE = @PR_TERMIN_TYPE
	--   	,PR_TERMIN_DATE = @PR_TERMIN_DATE
	--   	,PR_REASONS = @PR_REASONS
	--   	,PR_RESIG_RECV = @PR_RESIG_RECV
	--   	,PR_APP_RESIG = @PR_APP_RESIG
	--   	,PR_EXIT_INTER = @PR_EXIT_INTER
	--   	,PR_ID_RETURN = @PR_ID_RETURN
	--   	,PR_NOTICE = @PR_NOTICE
	--   	,PR_ARTONY = @PR_ARTONY
	--   	,PR_DELETION = @PR_DELETION
	--   	,PR_SETTLE = @PR_SETTLE
	--	,PR_UserID= @PR_UserID
	--Where 	 ID = @ID

INSERT INTO [dbo].[Terminations_CHK] (PR_P_NO, PR_FIRST_NAME,PR_LAST_NAME, PR_TERMIN_TYPE,PR_TERMIN_DATE,PR_REASONS,PR_RESIG_RECV,PR_APP_RESIG,PR_EXIT_INTER,PR_ID_RETURN,PR_NOTICE,PR_ARTONY,PR_DELETION,PR_SETTLE)
SELECT PR_P_NO, PR_FIRST_NAME,PR_LAST_NAME,PR_TERMIN_TYPE,PR_TERMIN_DATE,PR_REASONS,PR_RESIG_RECV,PR_APP_RESIG,PR_EXIT_INTER,PR_ID_RETURN,PR_NOTICE,PR_ARTONY,PR_DELETION,PR_SETTLE
FROM [dbo].[PERSONNEL]
Where 	 ID = @ID



