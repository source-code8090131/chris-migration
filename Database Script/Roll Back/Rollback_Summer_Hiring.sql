USE [CHRIS_UAT]
GO
IF NOT EXISTS (
  SELECT * 
  FROM   sys.columns 
  WHERE  object_id = OBJECT_ID(N'[dbo].[SUMMER_INTERNS]') 
         AND name = 'PR_UserID'
)
BEGIN
    ALTER TABLE SUMMER_INTERNS 
    DROP COLUMN PR_UserID
END
GO
IF NOT EXISTS (
  SELECT * 
  FROM   sys.columns 
  WHERE  object_id = OBJECT_ID(N'[dbo].[SUMMER_INTERNS]') 
         AND name = 'PR_UploadDate'
)
BEGIN
    ALTER TABLE SUMMER_INTERNS 
    DROP COLUMN PR_UploadDate
END
GO
IF NOT EXISTS (
  SELECT * 
  FROM   sys.columns 
  WHERE  object_id = OBJECT_ID(N'[dbo].[SUMMER_INTERNS]') 
         AND name = 'PR_User_Approved'
)
BEGIN
    ALTER TABLE SUMMER_INTERNS 
    DROP COLUMN PR_User_Approved
END
GO
/****** Object:  StoredProcedure [dbo].[CHRIS_SP_SUMMER_INTERNS_MANAGER] ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[CHRIS_SP_SUMMER_INTERNS_MANAGER]
/*<PARAM>*/
@PR_P_NO NUMERIC(6,0), --<DESCR></DESCR>
@PR_BRANCH VARCHAR(3), --<DESCR></DESCR>
@PR_FIRST_NAME VARCHAR(20), --<DESCR></DESCR>
@PR_LAST_NAME VARCHAR(20), --<DESCR></DESCR>
@PR_UNIVERSITY VARCHAR(20), --<DESCR></DESCR>
@PR_CLASS VARCHAR(20), --<DESCR></DESCR>
@PR_CON_FROM DATETIME, --<DESCR></DESCR>
@PR_CON_TO DATETIME, --<DESCR></DESCR>
@PR_ADDRESS VARCHAR(50), --<DESCR></DESCR>
@PR_PHONE1 VARCHAR(9), --<DESCR></DESCR>
@PR_PHONE2 VARCHAR(9), --<DESCR></DESCR>
@PR_DATE_BIRTH DATETIME, --<DESCR></DESCR>
@PR_MAR_STATUS VARCHAR(1), --<DESCR></DESCR>
@PR_SEX VARCHAR(1), --<DESCR></DESCR>
@PR_NID_CARD VARCHAR(14), --<DESCR></DESCR>
@PR_STIPENED NUMERIC(5,0), --<DESCR></DESCR>
@PR_INTERNSHIP NUMERIC(2,0), --<DESCR></DESCR>
@PR_RECOM VARCHAR(15), --<DESCR></DESCR>
@PR_UserID VARCHAR(35), --<DESCR>Add By Talat</DESCR>
@PR_FLAG VARCHAR(1), --<DESCR></DESCR>
@ID INT  OUTPUT,  --<DESCR></DESCR>
@ACTIONTYPE VARCHAR(50)
,@ORETVAL INT = NULL OUTPUT
/*</PARAM>*/
AS

SET NOCOUNT ON
/*<COMMENT></COMMENT>*/

SET @PR_BRANCH			= NULLIF(@PR_BRANCH,'') 
SET @PR_FIRST_NAME		= NULLIF(@PR_FIRST_NAME,'') 
SET @PR_LAST_NAME		= NULLIF(@PR_LAST_NAME,'') 
SET @PR_UNIVERSITY		= NULLIF(@PR_UNIVERSITY,'') 
SET @PR_CLASS			= NULLIF(@PR_CLASS,'') 
SET @PR_ADDRESS			= NULLIF(@PR_ADDRESS,'') 
SET @PR_PHONE1			= NULLIF(@PR_PHONE1,'') 
SET @PR_PHONE2			= NULLIF(@PR_PHONE2,'') 
SET @PR_SEX				= NULLIF(@PR_SEX,'') 
SET @PR_NID_CARD		= NULLIF(@PR_NID_CARD,'') 
SET @PR_RECOM			= NULLIF(@PR_RECOM,'') 
SET @PR_RECOM			= NULLIF(@PR_RECOM,'') 


	IF @ACTIONTYPE = 'SAVE'
	BEGIN
		SET @PR_FLAG = (CASE WHEN @PR_FLAG = '' THEN NULL ELSE  @PR_FLAG END)
		EXEC CHRIS_SP_SUMMER_INTERNS_ADD
	   			@PR_P_NO
	   			,@PR_BRANCH
	   			,@PR_FIRST_NAME
	   			,@PR_LAST_NAME
	   			,@PR_UNIVERSITY
	   			,@PR_CLASS
	   			,@PR_CON_FROM
	   			,@PR_CON_TO
	   			,@PR_ADDRESS
	   			,@PR_PHONE1
	   			,@PR_PHONE2
	   			,@PR_DATE_BIRTH
	   			,@PR_MAR_STATUS
	   			,@PR_SEX
	   			,@PR_NID_CARD
	   			,@PR_STIPENED
	   			,@PR_INTERNSHIP
	   			,@PR_RECOM
	   			,@PR_FLAG
	   			,@ID
				,@PR_UserID
				,@ORETVAL = @ORETVAL OUTPUT
			SET @ID =  @ORETVAL
	END
	ELSE IF @ACTIONTYPE = 'UPDATE'
	BEGIN
		SET @PR_FLAG = (CASE WHEN @PR_FLAG = '' THEN NULL ELSE  @PR_FLAG END)
		EXEC CHRIS_SP_SUMMER_INTERNS_UPDATE
	   			@PR_P_NO
	   			,@PR_BRANCH
	   			,@PR_FIRST_NAME
	   			,@PR_LAST_NAME
	   			,@PR_UNIVERSITY
	   			,@PR_CLASS
	   			,@PR_CON_FROM
	   			,@PR_CON_TO
	   			,@PR_ADDRESS
	   			,@PR_PHONE1
	   			,@PR_PHONE2
	   			,@PR_DATE_BIRTH
	   			,@PR_MAR_STATUS
	   			,@PR_SEX
	   			,@PR_NID_CARD
	   			,@PR_STIPENED
	   			,@PR_INTERNSHIP
	   			,@PR_RECOM
	   			,@PR_FLAG
	   			,@ID
	END
	ELSE IF @ACTIONTYPE = 'DELETE'
	BEGIN
		
		EXEC CHRIS_SP_SUMMER_INTERNS_DELETE
				@PR_P_NO,@ORETVAL
		
			
	END
	ELSE IF @ACTIONTYPE = 'GET'
	BEGIN
		EXEC CHRIS_SP_SUMMER_INTERNS_GET
				@ID,@ORETVAL
	END
	ELSE IF @ACTIONTYPE = 'LIST'
	BEGIN
		--EXEC CHRIS_SP_SUMMER_INTERNS_GETALL @ORETVAL
		SELECT ID
	   	,PR_I_NO AS PR_P_NO
	   	,PR_BRANCH
	   	,PR_FIRST_NAME
	   	,PR_LAST_NAME
	   	,PR_UNIVERSITY
	   	,PR_CLASS
	   	,PR_CON_FROM
	   	,PR_CON_TO
	   	,PR_ADDRESS
	   	,PR_PHONE1
	   	,PR_PHONE2
	   	,PR_DATE_BIRTH
	   	,PR_MAR_STATUS
	   	,PR_SEX
	   	,PR_NID_CARD
	   	,PR_STIPENED
	   	,PR_INTERNSHIP
	   	,PR_RECOM
	   	,PR_FLAG
	FROM DBO.SUMMER_INTERNS WITH(NOLOCK)
	WHERE PR_FLAG IS NULL
	
	END
	ELSE IF @ACTIONTYPE = 'PR_P_NO_LOV'
	BEGIN
		SELECT DISTINCT
				 SI.PR_I_NO AS PR_P_NO
				,PR_FIRST_NAME
				,PR_LAST_NAME 
				,PR_BRANCH
				,PR_UNIVERSITY
	   			,PR_CLASS
	   			,PR_CON_FROM
	   			,PR_CON_TO
	   			,PR_INTERNSHIP
	   			,PR_RECOM
	   			,PR_STIPENED
	   			,PR_ADDRESS
	   			,PR_PHONE1
	   			,PR_PHONE2
	   			,PR_DATE_BIRTH
	   			,PR_MAR_STATUS
	   			,PR_SEX
	   			,PR_NID_CARD
	   			,PR_FLAG
	   			,ID
		FROM SUMMER_INTERNS SI
		WHERE PR_FLAG IS NULL
	END
	ELSE IF @ACTIONTYPE = 'PR_P_NO_LOV_EXISTS'
	BEGIN
		IF EXISTS (SELECT ID FROM SUMMER_INTERNS  WHERE PR_I_NO  = @PR_P_NO)
		BEGIN
			SELECT DISTINCT 
				  SI.PR_I_NO AS PR_P_NO
				,PR_FIRST_NAME
				,PR_LAST_NAME 
				,PR_BRANCH
				,PR_UNIVERSITY
	   			,PR_CLASS
	   			,PR_CON_FROM
	   			,PR_CON_TO
	   			,PR_INTERNSHIP
	   			,PR_RECOM
	   			,PR_STIPENED
	   			,PR_ADDRESS
	   			,PR_PHONE1
	   			,PR_PHONE2
	   			,PR_DATE_BIRTH
	   			,PR_MAR_STATUS
	   			,PR_SEX
	   			,PR_NID_CARD
	   			,PR_FLAG
	   			,ID
		FROM SUMMER_INTERNS SI
			WHERE	PR_FLAG IS NULL
				AND PR_I_NO  = @PR_P_NO
		END
	END
	
	ELSE IF @ACTIONTYPE = 'BRANCH_LOV'
	BEGIN
		SELECT BRN_CODE AS PR_BRANCH, BRN_NAME FROM DBO.BRANCH
	END
	ELSE IF @ACTIONTYPE = 'BRANCH_LOV_EXISTS'
	BEGIN
		IF EXISTS (SELECT ID FROM BRANCH  WHERE BRN_CODE  = @PR_BRANCH)
		BEGIN
			SELECT BRN_CODE AS PR_BRANCH, BRN_NAME 
			FROM DBO.BRANCH 
			WHERE BRN_CODE = @PR_BRANCH
		END

		IF EXISTS (SELECT ID FROM BRANCH  WHERE BRN_CODE LIKE @PR_BRANCH  + '%' OR ISNULL(@PR_BRANCH ,'') = '')
		BEGIN
			SELECT BRN_CODE AS PR_BRANCH, BRN_NAME 
			FROM DBO.BRANCH  
			WHERE (BRN_CODE)  LIKE @PR_BRANCH + '%'

		END
	END
	ELSE IF @ACTIONTYPE = 'SER_NO'
	BEGIN
		SELECT ISNULL(PR_SNO,0)+1  AS SER_NO  FROM SERIAL_NO
        WHERE PR_TYPE = 'I';
	END
	
--	ELSE IF @ACTIONTYPE = 'DEPT_DELETE'
--	BEGIN
--		IF EXISTS (SELECT ID FROM DEPT_CONT WHERE PR_D_NO = @PR_P_NO ) 
--			BEGIN
--				DELETE FROM DBO.DEPT_CONT
--				WHERE 	PR_D_NO = @PR_P_NO AND 
--						PR_TYPE = 'I'
--			END
--	END
	

	RETURN

GO
/****** Object:  StoredProcedure [dbo].[CHRIS_SP_SUMMER_INTERNS_ADD] ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 

ALTER PROCEDURE [dbo].[CHRIS_SP_SUMMER_INTERNS_ADD]
/*<Params>*/
@PR_I_NO numeric(6,0), --<descr></descr>
@PR_BRANCH varchar(3), --<descr></descr>
@PR_FIRST_NAME varchar(20), --<descr></descr>
@PR_LAST_NAME varchar(20), --<descr></descr>
@PR_UNIVERSITY varchar(20), --<descr></descr>
@PR_CLASS varchar(20), --<descr></descr>
@PR_CON_FROM datetime, --<descr></descr>
@PR_CON_TO datetime, --<descr></descr>
@PR_ADDRESS varchar(50), --<descr></descr>
@PR_PHONE1 varchar(9), --<descr></descr>
@PR_PHONE2 varchar(9), --<descr></descr>
@PR_DATE_BIRTH datetime, --<descr></descr>
@PR_MAR_STATUS varchar(1), --<descr></descr>
@PR_SEX varchar(1), --<descr></descr>
@PR_NID_CARD varchar(14), --<descr></descr>
@PR_STIPENED numeric(5,0), --<descr></descr>
@PR_INTERNSHIP numeric(2,0), --<descr></descr>
@PR_RECOM varchar(15), --<descr></descr>
@PR_FLAG varchar(1), --<descr></descr>
@ID INT  OUTPUT,  --<descr></descr>
@PR_UserID varchar(35), --<descr>Add By Talat</descr>
@oRetVal int = Null Output --<descr></descr>
/*</Params>*/
As

Set NoCount On
/*<Comments></Comments>*/
/*<LocalVariables>*/
/*</LocalVariables>*/
 
	Insert Into dbo.SUMMER_INTERNS_CHK
	(
		PR_I_NO
		,PR_BRANCH
		,PR_FIRST_NAME
		,PR_LAST_NAME
		,PR_UNIVERSITY
		,PR_CLASS
		,PR_CON_FROM
		,PR_CON_TO
		,PR_ADDRESS
		,PR_PHONE1
		,PR_PHONE2
		,PR_DATE_BIRTH
		,PR_MAR_STATUS
		,PR_SEX
		,PR_NID_CARD
		,PR_STIPENED
		,PR_INTERNSHIP
		,PR_RECOM
		,PR_FLAG
		,PR_UserID
	)
	Values
	(
		@PR_I_NO
		,@PR_BRANCH
		,@PR_FIRST_NAME
		,@PR_LAST_NAME
		,@PR_UNIVERSITY
		,@PR_CLASS
		,@PR_CON_FROM
		,@PR_CON_TO
		,@PR_ADDRESS
		,@PR_PHONE1
		,@PR_PHONE2
		,@PR_DATE_BIRTH
		,@PR_MAR_STATUS
		,@PR_SEX
		,@PR_NID_CARD
		,@PR_STIPENED
		,@PR_INTERNSHIP
		,@PR_RECOM
		,@PR_FLAG
		,@PR_UserID
	)
	
	If @@RowCount > 0
	BEGIN
		Set @oRetVal = SCOPE_IDENTITY() --Record has been inserted successfully, return generated ID
		Set @ID = @oRetVal
		
		DECLARE @SER_NO INT
		SELECT @SER_NO = ISNULL(pr_sno,0)+1 from serial_no
			WHERE pr_type = 'I';
		UPDATE serial_no
			SET 
				pr_sno = ISNULL(pr_sno,0)+1 
		WHERE pr_type = 'I';
		
	END
	Else
		Set @oRetVal = -40 --No record inserted
	
	Return
GO
/****** Object:  StoredProcedure [dbo].[CHRIS_SP_SUMMER_INTERNS_UPDATE] ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* -----------------------------------------------------------------------------------------------------------
 * SP Name		: CHRIS_SP_SUMMER_INTERNSUPDATE
 * Created By	: Faisal Iqbal
 * Created On	: 13/01/2011
 * Purpose		: Updates record(s) in the Table(SUMMER_INTERNS) according to the parameter(s).
 * Module		: CHRIS
 * Called By	: iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.SUMMER_INTERNS.InsertSUMMER_INTERNS
 * Comments		: None
 * -----------------------------------------------------------------------------------------------------------
///-------------------------------------Modification History--------------------------------------------------
	[Date]		[Author]			[Purpose]

///----------------------------------------------------------------------------------------------------------- */
 
ALTER PROCEDURE [dbo].[CHRIS_SP_SUMMER_INTERNS_UPDATE]
/*<Param>*/
@PR_I_NO numeric(6,0), --<descr></descr>
@PR_BRANCH varchar(3), --<descr></descr>
@PR_FIRST_NAME varchar(20), --<descr></descr>
@PR_LAST_NAME varchar(20), --<descr></descr>
@PR_UNIVERSITY varchar(20), --<descr></descr>
@PR_CLASS varchar(20), --<descr></descr>
@PR_CON_FROM datetime, --<descr></descr>
@PR_CON_TO datetime, --<descr></descr>
@PR_ADDRESS varchar(50), --<descr></descr>
@PR_PHONE1 varchar(9), --<descr></descr>
@PR_PHONE2 varchar(9), --<descr></descr>
@PR_DATE_BIRTH datetime, --<descr></descr>
@PR_MAR_STATUS varchar(1), --<descr></descr>
@PR_SEX varchar(1), --<descr></descr>
@PR_NID_CARD varchar(14), --<descr></descr>
@PR_STIPENED numeric(5,0), --<descr></descr>
@PR_INTERNSHIP numeric(2,0), --<descr></descr>
@PR_RECOM varchar(15), --<descr></descr>
@PR_FLAG varchar(1), --<descr></descr>
@ID INT, --<descr></descr>
@oRetVal int = Null Output
/*</Param>*/
As

Set NoCount On
/*<Comment></Comment>*/
	Update	dbo.SUMMER_INTERNS
	Set
	   	PR_I_NO = @PR_I_NO
	   	,PR_BRANCH = @PR_BRANCH
	   	,PR_FIRST_NAME = @PR_FIRST_NAME
	   	,PR_LAST_NAME = @PR_LAST_NAME
	   	,PR_UNIVERSITY = @PR_UNIVERSITY
	   	,PR_CLASS = @PR_CLASS
	   	,PR_CON_FROM = @PR_CON_FROM
	   	,PR_CON_TO = @PR_CON_TO
	   	,PR_ADDRESS = @PR_ADDRESS
	   	,PR_PHONE1 = @PR_PHONE1
	   	,PR_PHONE2 = @PR_PHONE2
	   	,PR_DATE_BIRTH = @PR_DATE_BIRTH
	   	,PR_MAR_STATUS = @PR_MAR_STATUS
	   	,PR_SEX = @PR_SEX
	   	,PR_NID_CARD = @PR_NID_CARD
	   	,PR_STIPENED = @PR_STIPENED
	   	,PR_INTERNSHIP = @PR_INTERNSHIP
	   	,PR_RECOM = @PR_RECOM
	   	,PR_FLAG = @PR_FLAG
	Where 	 ID = @ID	
	
	Set @oRetVal = -10 --Record updated successfully
	Return
