USE [CHRIS_UAT]
GO
IF NOT EXISTS (
  SELECT * 
  FROM   sys.columns 
  WHERE  object_id = OBJECT_ID(N'[dbo].[CONTRACT]') 
         AND name = 'PR_UserID'
)
BEGIN
    ALTER TABLE CONTRACT 
    DROP COLUMN PR_UserID
END
GO
IF NOT EXISTS (
  SELECT * 
  FROM   sys.columns 
  WHERE  object_id = OBJECT_ID(N'[dbo].[CONTRACT]') 
         AND name = 'PR_UploadDate'
)
BEGIN
    ALTER TABLE CONTRACT 
    DROP COLUMN PR_UploadDate
END
GO
IF NOT EXISTS (
  SELECT * 
  FROM   sys.columns 
  WHERE  object_id = OBJECT_ID(N'[dbo].[CONTRACT]') 
         AND name = 'PR_User_Approved'
)
BEGIN
    ALTER TABLE CONTRACT 
    DROP COLUMN PR_User_Approved
END
GO
/****** Object:  StoredProcedure [dbo].[CHRIS_SP_CONTRACT_MANAGER] ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[CHRIS_SP_CONTRACT_MANAGER]
/*<Param>*/
@PR_P_NO numeric(6,0)OUTPUT, --<descr></descr>
@PR_BRANCH varchar(3), --<descr></descr>
@PR_FIRST_NAME varchar(20), --<descr></descr>
@PR_LAST_NAME varchar(20), --<descr></descr>
@PR_DESIG varchar(3), --<descr></descr>
@PR_CONTRACTOR varchar(3), --<descr></descr>
@PR_FROM_DATE datetime, --<descr></descr>
@PR_TO_DATE datetime, --<descr></descr>
@PR_ADDRESS varchar(50), --<descr></descr>
@PR_PHONE_1 varchar(9), --<descr></descr>
@PR_PHONE_2 varchar(9), --<descr></descr>
@PR_DATE_BIRTH datetime, --<descr></descr>
@PR_MARITAL varchar(1), --<descr></descr>
@PR_SEX varchar(1), --<descr></descr>
@PR_NID varchar(14), --<descr></descr>
@PR_MONTHLY_SALARY numeric(6,0), --<descr></descr>
@PR_UserID VARCHAR(35), --<DESCR>Add By Talat</DESCR>
@PR_FLAG varchar(1), --<descr></descr>
@ID INT  OUTPUT,  --<descr></descr>
@ActionType varchar(50)
,@oRetVal int = Null Output
/*</Param>*/
As

Set NoCount On
/*<Comment></Comment>*/
IF (@PR_LAST_NAME = '')
    SET @PR_LAST_NAME = NULL

IF (@PR_DESIG = '')
    SET @PR_DESIG = NULL

IF (@PR_CONTRACTOR = '')
    SET @PR_CONTRACTOR = NULL

IF (@PR_ADDRESS = '')
    SET @PR_ADDRESS = NULL

IF (@PR_PHONE_1 = '')
    SET @PR_PHONE_1 = NULL

IF (@PR_PHONE_2 = '')
    SET @PR_PHONE_2 = NULL

IF (@PR_MARITAL = '')
    SET @PR_MARITAL = NULL

IF (@PR_SEX = '')
    SET @PR_SEX = NULL

IF (@PR_NID = '')
    SET @PR_NID = NULL

if(@PR_FLAG='')
SET @PR_FLAG=NULL

	If @ActionType = 'Save'
	BEGIN
		IF Year(@PR_TO_DATE) = 1900
		BEGIN
		SET @PR_TO_DATE=NULL
		END
		SET @PR_FLAG = (CASE WHEN @PR_FLAG = '' THEN NULL ELSE  @PR_FLAG END)
		Exec CHRIS_SP_CONTRACT_ADD
	   			
	   			@PR_BRANCH
	   			,@PR_FIRST_NAME
	   			,@PR_LAST_NAME
	   			,@PR_DESIG
	   			,@PR_CONTRACTOR
	   			,@PR_FROM_DATE
	   			,@PR_TO_DATE
	   			,@PR_ADDRESS
	   			,@PR_PHONE_1
	   			,@PR_PHONE_2
	   			,@PR_DATE_BIRTH
	   			,@PR_MARITAL
	   			,@PR_SEX
	   			,@PR_NID
	   			,@PR_MONTHLY_SALARY
	   			,@PR_FLAG
	   			,@ID
	   			,@PR_P_NO
				,@PR_UserID
				,@oRetVal = @oRetVal OUTPUT
			SET @ID =  @oRetVal
	End
	Else If @ActionType = 'Update'
	BEGIN
		IF Year(@PR_TO_DATE) = 1900
		BEGIN
		SET @PR_TO_DATE=NULL
		END
		SET @PR_FLAG = (CASE WHEN @PR_FLAG = '' THEN NULL ELSE  @PR_FLAG END)
		Exec CHRIS_SP_CONTRACT_UPDATE
	   			@PR_P_NO
	   			,@PR_BRANCH
	   			,@PR_FIRST_NAME
	   			,@PR_LAST_NAME
	   			,@PR_DESIG
	   			,@PR_CONTRACTOR
	   			,@PR_FROM_DATE
	   			,@PR_TO_DATE
	   			,@PR_ADDRESS
	   			,@PR_PHONE_1
	   			,@PR_PHONE_2
	   			,@PR_DATE_BIRTH
	   			,@PR_MARITAL
	   			,@PR_SEX
	   			,@PR_NID
	   			,@PR_MONTHLY_SALARY
	   			,@PR_FLAG
	   			,@ID
	End
	Else If @ActionType = 'Delete'
	Begin
		Exec CHRIS_SP_CONTRACT_DELETE
				@ID,@oRetVal
	End
	Else If @ActionType = 'Get'
	Begin
		Exec CHRIS_SP_CONTRACT_GET
				@ID,@oRetVal
	End
	ELSE If @ActionType = 'List'
	BEGIN
		Exec CHRIS_SP_CONTRACT_GETALL @oRetVal
	END
	ELSE If @ActionType = 'PR_P_NO_LOV'
	Begin
		select distinct
				 PR_FIRST_NAME
				,PR_LAST_NAME 
				,PR_P_NO_C AS PR_P_NO
				,PR_BRANCH
				,PR_DESIG
	   			,PR_CONTRACTOR
	   			,PR_FROM_DATE
	   			,PR_TO_DATE
	   			,PR_ADDRESS
	   			,PR_PHONE_1
	   			,PR_PHONE_2
	   			,PR_DATE_BIRTH
	   			,PR_MARITAL
	   			,PR_SEX
	   			,PR_NID
	   			,PR_MONTHLY_SALARY
	   			,PR_FLAG
	   			,ID
		from CONTRACT WITH (NOLOCK)
		where PR_FLAG is null
	END
	ELSE If @ActionType = 'PR_P_NO_LOV_EXISTS'
	Begin
		If Exists (select ID from CONTRACT  where PR_P_NO_C  = @PR_P_NO)
		Begin
			select distinct 
				 PR_FIRST_NAME
				,PR_LAST_NAME 
				,PR_P_NO_C AS PR_P_NO
				,PR_BRANCH
				,PR_DESIG
	   			,PR_CONTRACTOR
	   			,PR_FROM_DATE
	   			,PR_TO_DATE
	   			,PR_ADDRESS
	   			,PR_PHONE_1
	   			,PR_PHONE_2
	   			,PR_DATE_BIRTH
	   			,PR_MARITAL
	   			,PR_SEX
	   			,PR_NID
	   			,PR_MONTHLY_SALARY
	   			,PR_FLAG
	   			,ID 
			from CONTRACT WITH (NOLOCK)
			where pr_flag is null
				and PR_P_NO_C  = @PR_P_NO

		END

--		If Exists (select ID from CONTRACT  where CAST(PR_P_NO_C AS VARCHAR) Like CAST(@PR_P_NO AS VARCHAR) + '%' Or Isnull(CAST(@PR_P_NO AS VARCHAR),'') = '')
--		Begin
--			select distinct 
--				PR_FIRST_NAME
--				,PR_LAST_NAME 
--				,PR_P_NO_C AS PR_P_NO
--				,PR_BRANCH
--				,PR_DESIG
--	   			,PR_CONTRACTOR
--	   			,PR_FROM_DATE
--	   			,PR_TO_DATE
--	   			,PR_ADDRESS
--	   			,PR_PHONE_1
--	   			,PR_PHONE_2
--	   			,PR_DATE_BIRTH
--	   			,PR_MARITAL
--	   			,PR_SEX
--	   			,PR_NID
--	   			,PR_MONTHLY_SALARY
--	   			,PR_FLAG
--	   			,ID 
--			from CONTRACT 
--			where pr_flag is null
--			and CAST(PR_P_NO_C AS VARCHAR)  like CAST(@PR_P_NO AS VARCHAR) + '%'
--
--		END
	END
	
	ELSE If @ActionType = 'BRANCH_LOV'
	Begin
		select BRN_CODE AS PR_BRANCH, BRN_NAME from dbo.BRANCH WITH (NOLOCK)
	END
	ELSE If @ActionType = 'BRANCH_LOV_EXISTS'
	Begin
		If Exists (select ID from BRANCH  where BRN_CODE  = @PR_BRANCH)
		Begin
			select BRN_CODE AS PR_BRANCH, BRN_NAME 
			from dbo.BRANCH WITH (NOLOCK)
			where BRN_CODE = @PR_BRANCH
		End

		If Exists (SELECT ID from BRANCH  where BRN_CODE Like @PR_BRANCH  + '%' Or Isnull(@PR_BRANCH ,'') = '')
		Begin
			select BRN_CODE AS PR_BRANCH, BRN_NAME 
			from dbo.BRANCH  WITH (NOLOCK)
			where (BRN_CODE)  like @PR_BRANCH + '%'

		END
	END
	
	ELSE If @ActionType = 'DESG_LOV'
	Begin
		select distinct sp_desg AS PR_DESIG, SP_DESC_DG1 
		from dbo.DESIG 
		where sp_current = 'C'
	END
	ELSE If @ActionType = 'DESG_LOV_EXISTS'
	Begin
		If Exists (select ID from DESIG  where sp_desg  = @PR_DESIG)
		Begin
			select distinct sp_desg AS PR_DESIG, SP_DESC_DG1 
			from dbo.DESIG WITH (NOLOCK)
			where sp_current = 'C'
			and sp_desg  = @PR_DESIG
		End

		If Exists (select ID from DESIG  where sp_desg  Like @PR_DESIG + '%' Or Isnull(@PR_DESIG ,'') = '')
		Begin
			select distinct sp_desg AS PR_DESIG, SP_DESC_DG1 
			from dbo.DESIG WITH (NOLOCK)
			where sp_current = 'C'
			and sp_desg  like @PR_DESIG  + '%'
		END
	END
	ELSE If @ActionType = 'CONTRACTOR_LOV'
	Begin
		SELECT DISTINCT 
			  SP_CONTRA_CODE AS PR_CONTRACTOR
			, SUBSTRING(SP_CONTRA_NAME,1,20) AS W_CONTRA_NAME
		FROM dbo.CONTRACTER WITH (NOLOCK)
	END
	ELSE If @ActionType = 'CONTRACTOR_LOV_EXISTS'
	Begin
		If Exists (select ID from CONTRACTER  where SP_CONTRA_CODE  = @PR_CONTRACTOR)
		Begin
			SELECT DISTINCT 
				  SP_CONTRA_CODE AS PR_CONTRACTOR
				, SUBSTRING(SP_CONTRA_NAME,1,20) AS W_CONTRA_NAME
			FROM dbo.CONTRACTER WITH (NOLOCK)
			where SP_CONTRA_CODE  = @PR_CONTRACTOR
		End

		If Exists (select ID from CONTRACTER  where SP_CONTRA_CODE  Like @PR_CONTRACTOR + '%' Or Isnull(@PR_CONTRACTOR ,'') = '')
		Begin
			SELECT DISTINCT 
				  SP_CONTRA_CODE AS PR_CONTRACTOR
				, SUBSTRING(SP_CONTRA_NAME,1,20) AS W_CONTRA_NAME
			FROM dbo.CONTRACTER WITH (NOLOCK)
			where SP_CONTRA_CODE  LIKE @PR_CONTRACTOR  + '%'
		END
	END
	
	ELSE If @ActionType = 'SER_NO'
	Begin
		SELECT ISNULL(pr_sno,0)+1 AS SER_NO from serial_no WITH (NOLOCK)
		WHERE pr_type = 'C';
	END
	
	
	ELSE If @ActionType = 'MONTH_BETWEEN'
	Begin
		SELECT dbo.CHRIS_FN_MONTHS_BETWEEN(@PR_FROM_DATE, @PR_DATE_BIRTH)
	END


	
RETURN

GO
/****** Object:  StoredProcedure [dbo].[CHRIS_SP_CONTRACT_ADD] ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[CHRIS_SP_CONTRACT_ADD]
/*<Params>*/
 --<descr></descr>
@PR_BRANCH varchar(3), --<descr></descr>
@PR_FIRST_NAME varchar(20), --<descr></descr>
@PR_LAST_NAME varchar(20), --<descr></descr>
@PR_DESIG varchar(3), --<descr></descr>
@PR_CONTRACTOR varchar(3), --<descr></descr>
@PR_FROM_DATE datetime, --<descr></descr>
@PR_TO_DATE datetime, --<descr></descr>
@PR_ADDRESS varchar(50), --<descr></descr>
@PR_PHONE_1 varchar(9), --<descr></descr>
@PR_PHONE_2 varchar(9), --<descr></descr>
@PR_DATE_BIRTH datetime, --<descr></descr>
@PR_MARITAL varchar(1), --<descr></descr>
@PR_SEX varchar(1), --<descr></descr>
@PR_NID varchar(14), --<descr></descr>
@PR_MONTHLY_SALARY numeric(6,0), --<descr></descr>
@PR_FLAG varchar(1), --<descr></descr>
@ID INT  OUTPUT,
@PR_UserID varchar(35), --<descr>Add By Talat</descr>
@PR_P_NO_C numeric(6,0)OUTPUT,  --<descr></descr>
@oRetVal int = Null Output --<descr></descr>
/*</Params>*/
As

Set NoCount On
/*<Comments></Comments>*/
/*<LocalVariables>*/
/*</LocalVariables>*/
 
 
SELECT @PR_P_NO_C= ISNULL(pr_sno,0)+1 from serial_no WITH (NOLOCK)WHERE pr_type = 'C'
	Insert Into dbo.CONTRACT_CHK
	(
		PR_P_NO_C
		,PR_BRANCH
		,PR_FIRST_NAME
		,PR_LAST_NAME
		,PR_DESIG
		,PR_CONTRACTOR
		,PR_FROM_DATE
		,PR_TO_DATE
		,PR_ADDRESS
		,PR_PHONE_1
		,PR_PHONE_2
		,PR_DATE_BIRTH
		,PR_MARITAL
		,PR_SEX
		,PR_NID
		,PR_MONTHLY_SALARY
		,PR_FLAG
		,PR_UserID
	)
	Values
	(
		@PR_P_NO_C
		,@PR_BRANCH
		,@PR_FIRST_NAME
		,@PR_LAST_NAME
		,@PR_DESIG
		,@PR_CONTRACTOR
		,@PR_FROM_DATE
		,@PR_TO_DATE
		,@PR_ADDRESS
		,@PR_PHONE_1
		,@PR_PHONE_2
		,@PR_DATE_BIRTH
		,@PR_MARITAL
		,@PR_SEX
		,@PR_NID
		,@PR_MONTHLY_SALARY
		,@PR_FLAG
		,@PR_UserID
	)
	
	If @@RowCount > 0
	BEGIN
		Set @oRetVal = IDENT_CURRENT('CONTRACT')--SCOPE_IDENTITY() --Record has been inserted successfully, return generated ID
		Set @ID = @oRetVal

		DECLARE @SER_NO INT
		SELECT @SER_NO = ISNULL(pr_sno,0)+1 from serial_no
			WHERE pr_type = 'C';
		UPDATE serial_no
			SET 
				pr_sno = ISNULL(pr_sno,0)+1 
		WHERE pr_type = 'C';

	END
	Else
		Set @oRetVal = -40 --No record inserted
	
	Return
GO
/****** Object:  StoredProcedure [dbo].[CHRIS_SP_CONTRACT_UPDATE] ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[CHRIS_SP_CONTRACT_UPDATE]
/*<Param>*/
@PR_P_NO_C numeric(6,0), --<descr></descr>
@PR_BRANCH varchar(3), --<descr></descr>
@PR_FIRST_NAME varchar(20), --<descr></descr>
@PR_LAST_NAME varchar(20), --<descr></descr>
@PR_DESIG varchar(3), --<descr></descr>
@PR_CONTRACTOR varchar(3), --<descr></descr>
@PR_FROM_DATE datetime, --<descr></descr>
@PR_TO_DATE datetime, --<descr></descr>
@PR_ADDRESS varchar(50), --<descr></descr>
@PR_PHONE_1 varchar(9), --<descr></descr>
@PR_PHONE_2 varchar(9), --<descr></descr>
@PR_DATE_BIRTH datetime, --<descr></descr>
@PR_MARITAL varchar(1), --<descr></descr>
@PR_SEX varchar(1), --<descr></descr>
@PR_NID varchar(14), --<descr></descr>
@PR_MONTHLY_SALARY numeric(6,0), --<descr></descr>
@PR_FLAG varchar(1), --<descr></descr>
@ID INT, --<descr></descr>
@oRetVal int = Null Output
/*</Param>*/
As

Set NoCount On
/*<Comment></Comment>*/
	Update	dbo.CONTRACT
	Set
	   	PR_P_NO_C = @PR_P_NO_C
	   	,PR_BRANCH = @PR_BRANCH
	   	,PR_FIRST_NAME = @PR_FIRST_NAME
	   	,PR_LAST_NAME = @PR_LAST_NAME
	   	,PR_DESIG = @PR_DESIG
	   	,PR_CONTRACTOR = @PR_CONTRACTOR
	   	,PR_FROM_DATE = @PR_FROM_DATE
	   	,PR_TO_DATE = @PR_TO_DATE
	   	,PR_ADDRESS = @PR_ADDRESS
	   	,PR_PHONE_1 = @PR_PHONE_1
	   	,PR_PHONE_2 = @PR_PHONE_2
	   	,PR_DATE_BIRTH = @PR_DATE_BIRTH
	   	,PR_MARITAL = @PR_MARITAL
	   	,PR_SEX = @PR_SEX
	   	,PR_NID = @PR_NID
	   	,PR_MONTHLY_SALARY = @PR_MONTHLY_SALARY
	   	,PR_FLAG = @PR_FLAG
	Where 	 ID = @ID	
	
	Set @oRetVal = -10 --Record updated successfully
	Return
GO
/****** Object:  StoredProcedure [dbo].[CHRIS_SP_CONTRACT_GETALL]    Script Date: 11/22/2023 1:03:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* -----------------------------------------------------------------------------------------------------------
 * SP Name		: CHRIS_SP_CONTRACT_GETALL
 * Created By	: Faisal Iqbal
 * Created On	: 10/01/2011
 * Purpose		: Retrieve entity(CONTRACT) records as per filter provided.
 * Module		: CHRIS
 * Called By	: iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.CONTRACT.GetCONTRACTRecords
 * Comments		: None
///----------------------------------------------------------------------------------------------------------- */
 
ALTER PROCEDURE [dbo].[CHRIS_SP_CONTRACT_GETALL]
/*<Param>*/
@oRetVal int Output --<descr></descr>
/*</Param>*/
As

Set NoCount On
/*<Comment></Comment>*/
	Select ID
	   	,PR_P_NO_C AS PR_P_NO
	   	,PR_BRANCH
	   	,PR_FIRST_NAME
	   	,PR_LAST_NAME
	   	,PR_DESIG
	   	,PR_CONTRACTOR
	   	,PR_FROM_DATE
	   	,PR_TO_DATE
	   	,PR_ADDRESS
	   	,PR_PHONE_1
	   	,PR_PHONE_2
	   	,PR_DATE_BIRTH
	   	,PR_MARITAL
	   	,PR_SEX
	   	,PR_NID
	   	,PR_MONTHLY_SALARY
	   	,PR_FLAG
	From dbo.CONTRACT WITH(NOLOCK)
	WHERE PR_FLAG IS NULL



	If @@RowCount = 0 
		Set @oRetVal = -40	--Record not found
	Else
		Set @oRetVal = -10 	--Success
	
	Return
