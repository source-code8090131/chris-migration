USE [CHRIS_UAT]
GO
IF EXISTS (
  SELECT * 
  FROM   sys.columns 
  WHERE  object_id = OBJECT_ID(N'[dbo].[BENCHMARK]') 
         AND name = 'BEN_MAKER_ID'
)
BEGIN
    ALTER TABLE BENCHMARK 
    DROP COLUMN BEN_MAKER_ID
END
GO
IF EXISTS (
  SELECT * 
  FROM   sys.columns 
  WHERE  object_id = OBJECT_ID(N'[dbo].[BENCHMARK]') 
         AND name = 'BEN_CHECKER_ID'
)
BEGIN
    ALTER TABLE BENCHMARK 
    DROP COLUMN BEN_CHECKER_ID
END
GO
IF EXISTS (
  SELECT * 
  FROM   sys.columns 
  WHERE  object_id = OBJECT_ID(N'[dbo].[BENCHMARK]') 
         AND name = 'BEN_ADDED_DATETIME'
)
BEGIN
    ALTER TABLE BENCHMARK 
    DROP COLUMN BEN_ADDED_DATETIME
END
GO
IF EXISTS (
  SELECT * 
  FROM   sys.columns 
  WHERE  object_id = OBJECT_ID(N'[dbo].[BENCHMARK]') 
         AND name = 'BEN_UPDATED_DATETIME'
)
BEGIN
    ALTER TABLE BENCHMARK 
    DROP COLUMN BEN_UPDATED_DATETIME
END
GO
GO
IF EXISTS (
  SELECT * 
  FROM   sys.columns 
  WHERE  object_id = OBJECT_ID(N'[dbo].[BENCHMARK]') 
         AND name = 'BEN_IS_AUTH'
)
BEGIN
    ALTER TABLE BENCHMARK 
    DROP COLUMN BEN_IS_AUTH
END
GO
/****** Object:  StoredProcedure [dbo].[CHRIS_SP_BENCHMARK_MANAGER] ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* -----------------------------------------------------------------------------------------------------------
 * SP Name		: CHRIS_SP_BENCHMARKMANAGER
 * Created By	: Nida Nazir
 * Created On	: 08/12/2010
 * Purpose		: Manages (BENCHMARK) store procedure depending upon the action type.
 * Module		: CHRIS
 * Called By	: CORE.CHRIS.BUSINESSOBJECTS.ENTITIES.BENCHMARKDAO
 * Comments		: None
 * -----------------------------------------------------------------------------------------------------------
///-------------------------------------Modification History--------------------------------------------------
	[Date]		[Author]			[Purpose]

///----------------------------------------------------------------------------------------------------------- */
 
ALTER PROCEDURE [dbo].[CHRIS_SP_BENCHMARK_MANAGER]
/*<Param>*/
@BEN_FIN_TYPE varchar(6)=null, --<descr></descr>
@BEN_DATE datetime=null, --<descr></descr>
@BEN_RATE numeric(10,6)=null, --<descr></descr>
@ID INT =null OUTPUT,  --<descr></descr>
@ActionType varchar(50)
,@oRetVal int = Null Output
/*</Param>*/
As

Set NoCount On
/*<Comment></Comment>*/



   If @ActionType = 'PRC01'
    BEGIN

    	
	DECLARE @Cursor_BenchMark CURSOR

	-----Declare @BEN_FIN_TYPE varchar(6)
	Declare @w_description varchar(20)

	Declare @fn_type varchar(6)
	Declare @fn_desc varchar(20)


	SET @Cursor_BenchMark = CURSOR READ_ONLY  ---FAST_FORWARD 
	FOR 
		select distinct fn_type as BEN_FIN_TYPE,fn_desc as w_description ,NULL as BEN_RATE,@BEN_DATE as BEN_DATE,ID
		from  fin_type order by fn_type;

	OPEN @Cursor_BenchMark 
	FETCH NEXT FROM @Cursor_BenchMark into @fn_type,@fn_desc,@BEN_RATE,@BEN_DATE,@ID

	WHILE (@@FETCH_STATUS = 0)

	BEGIN  
	  
	  SET @BEN_FIN_TYPE = @fn_type
	  SET @w_description = @fn_desc
	 
	FETCH NEXT FROM @Cursor_BenchMark into @fn_type,@fn_desc,@BEN_RATE,@BEN_DATE,@ID

	END

	SELECT FN_TYPE AS BEN_FIN_TYPE,FN_DESC AS w_description  ,Convert(numeric(10,6),NULL) as BEN_RATE ,@BEN_DATE as BEN_DATE,ID FROM FIN_TYPE order by fn_type
	CLOSE @Cursor_BenchMark 
	DEALLOCATE @Cursor_BenchMark 



    END



	ELSE If @ActionType = 'Save'
	Begin
		Exec CHRIS_SP_BENCHMARK_ADD
	   			@BEN_FIN_TYPE
	   			,@BEN_DATE
	   			,@BEN_RATE
	   			,@ID
				,@oRetVal = @oRetVal OUTPUT
			SET @ID =  @oRetVal
	End
	Else If @ActionType = 'Update'
	Begin
		Exec CHRIS_SP_BENCHMARK_UPDATE
	   			@BEN_FIN_TYPE
	   			,@BEN_DATE
	   			,@BEN_RATE
	   			,@ID
	End
	Else If @ActionType = 'Delete'
	Begin
		Exec CHRIS_SP_BENCHMARK_DELETE
				@ID,@oRetVal
	End
	Else If @ActionType = 'Get'
	Begin
		Exec CHRIS_SP_BENCHMARK_GET
				@ID,@oRetVal
	End



	Else If @ActionType = 'List'
	Begin
		Exec CHRIS_SP_BENCHMARK_GETALL @BEN_DATE ,@oRetVal
	End

	Else If @ActionType = 'BenchDateLOV'
	Begin
	SELECT DISTINCT CAST(CONVERT(VARCHAR,ben_date,101)  AS DATETIME) BEN_DATE FROM  benchmark  	ORDER BY 1 
	End

	else if @ActionType='BenchDateLOVExists'
	Begin
	IF EXISTS(Select ID FROM  benchmark WHERE BEN_DATE=@BEN_DATE )  
    BEGIN  
   SELECT DISTINCT CAST(CONVERT(VARCHAR,ben_date,101)  AS DATETIME) BEN_DATE FROM  benchmark 
	--WHERE ben_date=@ben_date
	order by 1
    END 
	Else IF EXISTS (Select ID From benchmark  WHERE BEN_DATE  LIKE  @BEN_DATE + '%'  OR ISNULL(@BEN_DATE,'')='')   
		BEGIN
		SELECT DISTINCT CAST(CONVERT(VARCHAR,ben_date,101)  AS DATETIME) as BEN_DATE
		FROM benchmark 
	END  
	

  

 END 

   

Return
GO
/****** Object:  StoredProcedure [dbo].[CHRIS_SP_BENCHMARK_UPDATE] ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* -----------------------------------------------------------------------------------------------------------
 * SP Name		: CHRIS_SP_BENCHMARKUPDATE
 * Created By	: Nida Nazir
 * Created On	: 08/12/2010
 * Purpose		: Updates record(s) in the Table(BENCHMARK) according to the parameter(s).
 * Module		: CHRIS
 * Called By	: CORE.CHRIS.BUSINESSOBJECTS.ENTITIES.BENCHMARK.InsertBENCHMARK
 * Comments		: None
 * -----------------------------------------------------------------------------------------------------------
///-------------------------------------Modification History--------------------------------------------------
	[Date]		[Author]			[Purpose]

///----------------------------------------------------------------------------------------------------------- */
 
ALTER PROCEDURE [dbo].[CHRIS_SP_BENCHMARK_UPDATE]
/*<Param>*/
@BEN_FIN_TYPE varchar(6)=null, --<descr></descr>
@BEN_DATE datetime=null, --<descr></descr>
@BEN_RATE numeric(10,6)=null, --<descr></descr>
@ID INT=null, --<descr></descr>
@oRetVal int = Null Output
/*</Param>*/
As

Set NoCount On
/*<Comment></Comment>*/
if exists (select 1 from dbo.BENCHMARK with (nolock) where 
BEN_FIN_TYPE = @BEN_FIN_TYPE
AND CONVERT(VARCHAR(10), BEN_DATE, 101) = CONVERT(VARCHAR(10), @BEN_DATE, 101)
)--id = @ID)
begin
	Update	dbo.BENCHMARK
	Set
	   	--BEN_FIN_TYPE = @BEN_FIN_TYPE
	   	BEN_DATE = @BEN_DATE,
	   	BEN_RATE = @BEN_RATE
	Where 	 --ID = @ID	
		BEN_FIN_TYPE = @BEN_FIN_TYPE
		AND CONVERT(VARCHAR(10), BEN_DATE, 101) = CONVERT(VARCHAR(10), @BEN_DATE, 101)
	Set @oRetVal = -10 --Record updated successfully
end
else
begin
	Insert Into dbo.BENCHMARK
	(
		BEN_FIN_TYPE
		,BEN_DATE
		,BEN_RATE
	)
	Values
	(
		@BEN_FIN_TYPE
		,@BEN_DATE
		,@BEN_RATE
	)
	
	If @@RowCount > 0
	BEGIN
		Set @oRetVal = SCOPE_IDENTITY() --Record has been inserted successfully, return generated ID
		Set @ID = @oRetVal
	END
	Else
		Set @oRetVal = -40 --No record inserted

end

	Return
GO

/****** Object:  StoredProcedure [dbo].[CHRIS_SP_BENCHMARK_GETALL] ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* -----------------------------------------------------------------------------------------------------------
 * SP Name		: CHRIS_SP_BENCHMARK_GETALL
 * Created By	: Nida Nazir
 * Created On	: 08/12/2010
 * Purpose		: Retrieve entity(BENCHMARK) records as per filter provided.
 * Module		: CHRIS
 * Called By	: CORE.CHRIS.BUSINESSOBJECTS.ENTITIES.BENCHMARK.GetBENCHMARKRecords
 * Comments		: None
 * -----------------------------------------------------------------------------------------------------------
///-------------------------------------Modification History--------------------------------------------------
	[Date]		[Author]			[Purpose]

///----------------------------------------------------------------------------------------------------------- */
 
ALTER PROCEDURE [dbo].[CHRIS_SP_BENCHMARK_GETALL]
/*<Param>*/
@BEN_DATE datetime,
@oRetVal int Output --<descr></descr>
/*</Param>*/
As

Set NoCount On
/*<Comment></Comment>*/
	Select
		BENCHMARK.ID AS ID
	   	,BEN_FIN_TYPE
	   	,BEN_DATE
	   	,BEN_RATE
		,fin_type.FN_DESC w_description
	From dbo.BENCHMARK WITH(NOLOCK)
	LEFT JOIN FIN_TYPE
	ON fin_type.FN_TYPE = BENCHMARK.BEN_FIN_TYPE 
	WHERE (DATEDIFF(dd,BEN_DATE ,@BEN_DATE)=0)
    order by BEN_FIN_TYPE asc
	

	If @@RowCount = 0 
		Set @oRetVal = -40	--Record not found
	Else
		Set @oRetVal = -10 	--Success
	
	Return
