USE [CHRIS_UAT]
GO
IF NOT EXISTS (
  SELECT * 
  FROM   sys.columns 
  WHERE  object_id = OBJECT_ID(N'[dbo].[PERSONNEL]') 
         AND name = 'PR_UserID'
)
BEGIN
    ALTER TABLE PERSONNEL 
    ADD PR_UserID varchar(50)
END
GO
IF NOT EXISTS (
  SELECT * 
  FROM   sys.columns 
  WHERE  object_id = OBJECT_ID(N'[dbo].[PERSONNEL]') 
         AND name = 'PR_UploadDate'
)
BEGIN
    ALTER TABLE PERSONNEL 
    ADD PR_UploadDate datetime
END
GO
IF NOT EXISTS (
  SELECT * 
  FROM   sys.columns 
  WHERE  object_id = OBJECT_ID(N'[dbo].[PERSONNEL]') 
         AND name = 'PR_User_Approved'
)
BEGIN
    ALTER TABLE PERSONNEL 
    ADD PR_User_Approved bit
END
GO
UPDATE PERSONNEL SET PR_User_Approved = 1
GO
/****** Object:  StoredProcedure [dbo].[CHRIS_SP_RegStHiEnt_PERSONNEL_ADD]  ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER  PROCEDURE [dbo].[CHRIS_SP_RegStHiEnt_PERSONNEL_ADD]
/*<Params>*/
@PR_P_NO numeric(6,0) , --<descr></descr>
@PR_BRANCH varchar(3), --<descr></descr>
@PR_FIRST_NAME varchar(20), --<descr></descr>
@PR_LAST_NAME varchar(20), --<descr></descr>
@PR_DESIG varchar(3), --<descr></descr>
@PR_LEVEL varchar(3), --<descr></descr>
@PR_CATEGORY varchar(1), --<descr></descr>
@PR_FUNC_TITTLE1 varchar(30), --<descr></descr>
@PR_FUNC_TITTLE2 varchar(30), --<descr></descr>
@PR_JOINING_DATE datetime, --<descr></descr>
@PR_CONFIRM datetime, --<descr></descr>
@PR_ANNUAL_PACK numeric(11,0), --<descr></descr>
@PR_NATIONAL_TAX varchar(18), --<descr></descr>
@PR_ACCOUNT_NO varchar(24), --<descr></descr>
@PR_BANK_ID varchar(1), --<descr></descr>
@PR_ID_ISSUE datetime, --<descr></descr>
@PR_EXPIRY datetime, --<descr></descr>
@PR_TAX_INC numeric(7,0), --<descr></descr>
@PR_TAX_PAID numeric(7,0), --<descr></descr>
@PR_TAX_USED varchar(1), --<descr></descr>
@PR_CLOSE_FLAG varchar(1) = NULL, --<descr></descr>
@PR_CONFIRM_ON datetime, --<descr></descr>
@PR_EXPECTED datetime, --<descr></descr>
@PR_TERMIN_TYPE varchar(1), --<descr></descr>
@PR_TERMIN_DATE datetime, --<descr></descr>
@PR_REASONS varchar(30), --<descr></descr>
@PR_RESIG_RECV varchar(1), --<descr></descr>
@PR_APP_RESIG varchar(1), --<descr></descr>
@PR_EXIT_INTER varchar(1), --<descr></descr>
@PR_ID_RETURN varchar(1), --<descr></descr>
@PR_NOTICE varchar(1), --<descr></descr>
@PR_ARTONY varchar(1), --<descr></descr>
@PR_DELETION varchar(1), --<descr></descr>
@PR_SETTLE varchar(1), --<descr></descr>
@PR_CONF_FLAG varchar(1), --<descr></descr>
@PR_LAST_INCREMENT datetime, --<descr></descr>
@PR_NEXT_INCREMENT datetime, --<descr></descr>
@PR_TRANSFER_DATE datetime, --<descr></descr>
@PR_PROMOTION_DATE datetime, --<descr></descr>
@PR_NEW_BRANCH varchar(3), --<descr></descr>
@PR_NEW_ANNUAL_PACK numeric(11,0), --<descr></descr>
@PR_TRANSFER numeric(1,0), --<descr></descr>
@PR_MONTH_AWARD numeric(3,0), --<descr></descr>
@PR_RELIGION varchar(15)=null, --<descr></descr>
@PR_ZAKAT_AMT numeric(5,0), --<descr></descr>
@PR_MED_FLAG varchar(1), --<descr></descr>
@PR_ATT_FLAG varchar(1), --<descr></descr>
@PR_PAY_FLAG varchar(2), --<descr></descr>
@PR_REP_NO numeric(6,0), --<descr></descr>
@PR_REFUND_AMT numeric(12,2), --<descr></descr>
@PR_REFUND_FOR varchar(9), --<descr></descr>
@PR_NTN_CERT_REC varchar(3), --<descr></descr>
@PR_NTN_CARD_REC varchar(3), --<descr></descr>
@PR_EMP_TYPE varchar(2), --<descr></descr>
@LOCATION varchar(40), --<descr></descr>
@ID INT  OUTPUT,  --<descr></descr>
@PR_EMAIL  varchar(255),  --<descr></descr>
@PR_UserID VARCHAR(35), --<DESCR></DESCR>
@PR_UploadDate datetime,
@PR_User_Approved bit,
@oRetVal int = Null Output --<descr></descr>
/*</Params>*/
As

Set NoCount On

	Insert Into dbo.PERSONNEL
	(
		PR_P_NO
		,PR_BRANCH
		,PR_FIRST_NAME
		,PR_LAST_NAME
		,PR_DESIG
		,PR_LEVEL
		,PR_CATEGORY
		,PR_FUNC_TITTLE1
		,PR_FUNC_TITTLE2
		,PR_JOINING_DATE
		,PR_CONFIRM
		,PR_ANNUAL_PACK
		,PR_NATIONAL_TAX
		,PR_ACCOUNT_NO
		,PR_BANK_ID
		,PR_ID_ISSUE
		,PR_EXPIRY
		,PR_TAX_INC
		,PR_TAX_PAID
		,PR_TAX_USED
		,PR_CLOSE_FLAG
		,PR_CONFIRM_ON
		,PR_EXPECTED
		,PR_TERMIN_TYPE
		,PR_TERMIN_DATE
		,PR_REASONS
		,PR_RESIG_RECV
		,PR_APP_RESIG
		,PR_EXIT_INTER
		,PR_ID_RETURN
		,PR_NOTICE
		,PR_ARTONY
		,PR_DELETION
		,PR_SETTLE
		,PR_CONF_FLAG
		,PR_LAST_INCREMENT
		,PR_NEXT_INCREMENT
		,PR_TRANSFER_DATE
		,PR_PROMOTION_DATE
		,PR_NEW_BRANCH
		,PR_NEW_ANNUAL_PACK
		,PR_TRANSFER
		,PR_MONTH_AWARD
		--,PR_RELIGION
		,PR_ZAKAT_AMT
		,PR_MED_FLAG
		,PR_ATT_FLAG
		,PR_PAY_FLAG
		,PR_REP_NO
		,PR_REFUND_AMT
		,PR_REFUND_FOR
		,PR_NTN_CERT_REC
		,PR_NTN_CARD_REC
		,PR_EMP_TYPE
		,LOCATION
		,PR_EMAIL
		,PR_UserID
		,PR_UploadDate
		,PR_User_Approved
	)
	Values
	(
		@PR_P_NO
		,@PR_BRANCH
		,@PR_FIRST_NAME
		,@PR_LAST_NAME
		,@PR_DESIG
		,@PR_LEVEL
		,@PR_CATEGORY
		,@PR_FUNC_TITTLE1
		,@PR_FUNC_TITTLE2
		,@PR_JOINING_DATE
		,@PR_CONFIRM
		,@PR_ANNUAL_PACK
		,@PR_NATIONAL_TAX
		,REPLACE(@PR_ACCOUNT_NO,'-','')
		,@PR_BANK_ID
		,@PR_ID_ISSUE
		,@PR_EXPIRY
		,@PR_TAX_INC
		,@PR_TAX_PAID
		,@PR_TAX_USED
		,@PR_CLOSE_FLAG
		,@PR_CONFIRM_ON
		,@PR_EXPECTED
		,@PR_TERMIN_TYPE
		,@PR_TERMIN_DATE
		,@PR_REASONS
		,@PR_RESIG_RECV
		,@PR_APP_RESIG
		,@PR_EXIT_INTER
		,@PR_ID_RETURN
		,@PR_NOTICE
		,@PR_ARTONY
		,@PR_DELETION
		,@PR_SETTLE
		,@PR_CONF_FLAG
		,@PR_LAST_INCREMENT
		,@PR_NEXT_INCREMENT
		,@PR_TRANSFER_DATE
		,@PR_PROMOTION_DATE
		,@PR_NEW_BRANCH
		,@PR_NEW_ANNUAL_PACK
		,@PR_TRANSFER
		,@PR_MONTH_AWARD
		--,@PR_RELIGION
		,@PR_ZAKAT_AMT
		,@PR_MED_FLAG
		,@PR_ATT_FLAG
		,@PR_PAY_FLAG
		,@PR_REP_NO
		,@PR_REFUND_AMT
		,@PR_REFUND_FOR
		,@PR_NTN_CERT_REC
		,@PR_NTN_CARD_REC
		,@PR_EMP_TYPE
		,@LOCATION
		,@PR_EMAIL
		,@PR_UserID
		,@PR_UploadDate
		,@PR_User_Approved
	)
	
----------Update the Serial Number------------------
UPDATE SERIAL_NO 
--SET PR_SNO = ISNULL(@PR_P_NO,0)+1 
SET PR_SNO = ISNULL(@PR_P_NO,0)
WHERE PR_TYPE = 'R'


	If @@RowCount > 0
	BEGIN
		Set @oRetVal = SCOPE_IDENTITY() --Record has been inserted successfully, return generated ID
		Set @ID = @oRetVal
	END
	Else
		Set @oRetVal = -40 --No record inserted
	
	Return
GO
/****** Object:  StoredProcedure [dbo].[CHRIS_SP_RegStHiEnt_PERSONNEL_UPDATE]  ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[CHRIS_SP_RegStHiEnt_PERSONNEL_UPDATE]
/*<Param>*/    
@PR_P_NO numeric(6,0) OUTPUT, --<descr></descr>    
@PR_BRANCH varchar(3), --<descr></descr>    
@PR_FIRST_NAME varchar(20), --<descr></descr>    
@PR_LAST_NAME varchar(20), --<descr></descr>    
@PR_DESIG varchar(3), --<descr></descr>    
@PR_LEVEL varchar(3), --<descr></descr>    
@PR_CATEGORY varchar(1), --<descr></descr>    
@PR_FUNC_TITTLE1 varchar(30), --<descr></descr>    
@PR_FUNC_TITTLE2 varchar(30), --<descr></descr>    
@PR_JOINING_DATE datetime, --<descr></descr>    
@PR_CONFIRM datetime, --<descr></descr>    
@PR_ANNUAL_PACK numeric(11,0), --<descr></descr>    
@PR_NATIONAL_TAX varchar(18), --<descr></descr>    
@PR_ACCOUNT_NO varchar(24), --<descr></descr>    
@PR_BANK_ID varchar(1), --<descr></descr>    
@PR_ID_ISSUE datetime, --<descr></descr>    
@PR_EXPIRY datetime, --<descr></descr>    
@PR_TAX_INC numeric(7,0), --<descr></descr>    
@PR_TAX_PAID numeric(7,0), --<descr></descr>    
@PR_TAX_USED varchar(1), --<descr></descr>    
@PR_CLOSE_FLAG varchar(1), --<descr></descr>    
@PR_CONFIRM_ON datetime, --<descr></descr>    
@PR_EXPECTED datetime, --<descr></descr>    
@PR_TERMIN_TYPE varchar(1), --<descr></descr>    
@PR_TERMIN_DATE datetime, --<descr></descr>    
@PR_REASONS varchar(30), --<descr></descr>    
@PR_RESIG_RECV varchar(1), --<descr></descr>    
@PR_APP_RESIG varchar(1), --<descr></descr>    
@PR_EXIT_INTER varchar(1), --<descr></descr>    
@PR_ID_RETURN varchar(1), --<descr></descr>    
@PR_NOTICE varchar(1), --<descr></descr>    
@PR_ARTONY varchar(1), --<descr></descr>    
@PR_DELETION varchar(1), --<descr></descr>    
@PR_SETTLE varchar(1), --<descr></descr>    
@PR_CONF_FLAG varchar(1), --<descr></descr>    
@PR_LAST_INCREMENT datetime, --<descr></descr>    
@PR_NEXT_INCREMENT datetime, --<descr></descr>    
@PR_TRANSFER_DATE datetime, --<descr></descr>    
@PR_PROMOTION_DATE datetime, --<descr></descr>    
@PR_NEW_BRANCH varchar(3), --<descr></descr>    
@PR_NEW_ANNUAL_PACK numeric(11,0), --<descr></descr>    
@PR_TRANSFER numeric(1,0), --<descr></descr>    
@PR_MONTH_AWARD numeric(3,0), --<descr></descr>    
@PR_RELIGION varchar(15), --<descr></descr>    
@PR_ZAKAT_AMT numeric(5,0), --<descr></descr>    
@PR_MED_FLAG varchar(1), --<descr></descr>    
@PR_ATT_FLAG varchar(1), --<descr></descr>    
@PR_PAY_FLAG varchar(2), --<descr></descr>    
@PR_REP_NO numeric(6,0), --<descr></descr>    
@PR_REFUND_AMT numeric(12,2), --<descr></descr>    
@PR_REFUND_FOR varchar(9), --<descr></descr>    
@PR_NTN_CERT_REC varchar(3), --<descr></descr>    
@PR_NTN_CARD_REC varchar(3), --<descr></descr>    
@PR_EMP_TYPE varchar(2), --<descr></descr>    
@LOCATION varchar(40), --<descr></descr>    
@ID INT, --<descr></descr>    
@PR_EMAIL varchar(255),
@PR_UserID VARCHAR(35), --<descr></descr>
@PR_UploadDate datetime,
@PR_User_Approved bit,
@oRetVal int = Null Output    
/*</Param>*/    
As    
    
Set NoCount On    
/*<Comment></Comment>*/    
    
Update	dbo.PERSONNEL
	Set
	   	PR_P_NO = @PR_P_NO
	   	,PR_BRANCH = @PR_BRANCH
	   	,PR_FIRST_NAME = @PR_FIRST_NAME
	   	,PR_LAST_NAME = @PR_LAST_NAME
	   	,PR_DESIG = @PR_DESIG
	   	,PR_LEVEL = @PR_LEVEL
	   	,PR_CATEGORY = @PR_CATEGORY
	   	,PR_FUNC_TITTLE1 = @PR_FUNC_TITTLE1
	   	,PR_FUNC_TITTLE2 = @PR_FUNC_TITTLE2
	   	,PR_JOINING_DATE = @PR_JOINING_DATE
	   	,PR_CONFIRM = @PR_CONFIRM
	   	,PR_ANNUAL_PACK = @PR_ANNUAL_PACK
	   	,PR_NATIONAL_TAX = @PR_NATIONAL_TAX
	   	,PR_ACCOUNT_NO = @PR_ACCOUNT_NO
	   	,PR_BANK_ID = @PR_BANK_ID
	   	,PR_ID_ISSUE = @PR_ID_ISSUE
	   	,PR_EXPIRY = @PR_EXPIRY
	   	,PR_TAX_INC = @PR_TAX_INC
	   	,PR_TAX_PAID = @PR_TAX_PAID
	   	,PR_TAX_USED = @PR_TAX_USED
	   	,PR_CLOSE_FLAG = @PR_CLOSE_FLAG
	   	,PR_CONFIRM_ON = @PR_CONFIRM_ON
	   	,PR_EXPECTED = @PR_EXPECTED
	   	,PR_TERMIN_TYPE = @PR_TERMIN_TYPE
	   	,PR_TERMIN_DATE = @PR_TERMIN_DATE
	   	,PR_REASONS = @PR_REASONS
	   	,PR_RESIG_RECV = @PR_RESIG_RECV
	   	,PR_APP_RESIG = @PR_APP_RESIG
	   	,PR_EXIT_INTER = @PR_EXIT_INTER
	   	,PR_ID_RETURN = @PR_ID_RETURN
	   	,PR_NOTICE = @PR_NOTICE
	   	,PR_ARTONY = @PR_ARTONY
	   	,PR_DELETION = @PR_DELETION
	   	,PR_SETTLE = @PR_SETTLE
	   	,PR_CONF_FLAG = @PR_CONF_FLAG
	   	,PR_LAST_INCREMENT = @PR_LAST_INCREMENT
	   	,PR_NEXT_INCREMENT = @PR_NEXT_INCREMENT
	   	,PR_TRANSFER_DATE = @PR_TRANSFER_DATE
	   	,PR_PROMOTION_DATE = @PR_PROMOTION_DATE
	   	,PR_NEW_BRANCH = @PR_NEW_BRANCH
	   	,PR_NEW_ANNUAL_PACK = @PR_NEW_ANNUAL_PACK
	   	,PR_TRANSFER = @PR_TRANSFER
	   	,PR_MONTH_AWARD = @PR_MONTH_AWARD
	   --	,PR_RELIGION = @PR_RELIGION
	   	,PR_ZAKAT_AMT = @PR_ZAKAT_AMT
	   	,PR_MED_FLAG = @PR_MED_FLAG
	   	,PR_ATT_FLAG = @PR_ATT_FLAG
	   	,PR_PAY_FLAG = @PR_PAY_FLAG
	   	,PR_REP_NO = @PR_REP_NO
	   	,PR_REFUND_AMT = @PR_REFUND_AMT
	   	,PR_REFUND_FOR = @PR_REFUND_FOR
	   	,PR_NTN_CERT_REC = @PR_NTN_CERT_REC
	   	,PR_NTN_CARD_REC = @PR_NTN_CARD_REC
	   	,PR_EMP_TYPE = @PR_EMP_TYPE
	   	,LOCATION = @LOCATION,
		PR_EMAIL = @PR_EMAIL,
		PR_UserID = @PR_UserID, 
		PR_UploadDate = @PR_UploadDate,
		PR_User_Approved = @PR_User_Approved
	Where 	 ID = @ID	
	
	Set @oRetVal = -10 --Record updated successfully
	Return    

GO

/****** Object:  StoredProcedure [dbo].[CHRIS_SP_RegStHiEnt_PERSONNEL_MANAGER] ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
 
ALTER PROCEDURE [dbo].[CHRIS_SP_RegStHiEnt_PERSONNEL_MANAGER]
/*<PARAM>*/      
@PR_P_NO  NUMERIC(6,0) , --<DESCR></DESCR>      
@PR_BRANCH  VARCHAR(3),  --<DESCR></DESCR>       
@PR_FIRST_NAME VARCHAR(20), --<DESCR></DESCR>      
@PR_LAST_NAME VARCHAR(20), --<DESCR></DESCR>      
@PR_DESIG  VARCHAR(3),  --<DESCR></DESCR>      
@PR_LEVEL  VARCHAR(3),  --<DESCR></DESCR>      
@PR_CATEGORY VARCHAR(1),  --<DESCR></DESCR>      
@PR_FUNC_TITTLE1 VARCHAR(30), --<DESCR></DESCR>      
@PR_FUNC_TITTLE2 VARCHAR(30), --<DESCR></DESCR>      
@PR_JOINING_DATE DATETIME,  --<DESCR></DESCR>      
@PR_CONFIRM  DATETIME,  --<DESCR></DESCR>      
@PR_ANNUAL_PACK NUMERIC(11,0), --<DESCR></DESCR>      
@PR_NATIONAL_TAX VARCHAR(18), --<DESCR></DESCR>      
@PR_ACCOUNT_NO VARCHAR(24), --<DESCR></DESCR>      
@PR_BANK_ID  VARCHAR(1),  --<DESCR></DESCR>      
@PR_ID_ISSUE DATETIME,  --<DESCR></DESCR>      
@PR_EXPIRY  DATETIME,  --<DESCR></DESCR>      
@PR_TAX_INC  NUMERIC(7,0), --<DESCR></DESCR>      
@PR_TAX_PAID NUMERIC(7,0), --<DESCR></DESCR>      
@PR_TAX_USED VARCHAR(1),  --<DESCR></DESCR>      
@PR_CLOSE_FLAG VARCHAR(1),  --<DESCR></DESCR>      
@PR_CONFIRM_ON DATETIME,  --<DESCR></DESCR>      
@PR_EXPECTED DATETIME,  --<DESCR></DESCR>      
@PR_TERMIN_TYPE VARCHAR(1),  --<DESCR></DESCR>      
@PR_TERMIN_DATE DATETIME,  --<DESCR></DESCR>      
@PR_REASONS  VARCHAR(30), --<DESCR></DESCR>      
@PR_RESIG_RECV VARCHAR(1),  --<DESCR></DESCR>      
@PR_APP_RESIG VARCHAR(1),  --<DESCR></DESCR>      
@PR_EXIT_INTER VARCHAR(1),  --<DESCR></DESCR>      
@PR_ID_RETURN VARCHAR(1),  --<DESCR></DESCR>      
@PR_NOTICE  VARCHAR(1),  --<DESCR></DESCR>      
@PR_ARTONY  VARCHAR(1),  --<DESCR></DESCR>      
@PR_DELETION VARCHAR(1),  --<DESCR></DESCR>      
@PR_SETTLE  VARCHAR(1),  --<DESCR></DESCR>      
@PR_CONF_FLAG VARCHAR(1),  --<DESCR></DESCR>      
@PR_LAST_INCREMENT DATETIME, --<DESCR></DESCR>      
@PR_NEXT_INCREMENT DATETIME, --<DESCR></DESCR>      
@PR_TRANSFER_DATE DATETIME,  --<DESCR></DESCR>      
@PR_PROMOTION_DATE DATETIME, --<DESCR></DESCR>      
@PR_NEW_BRANCH VARCHAR(3),  --<DESCR></DESCR>      
@PR_NEW_ANNUAL_PACK NUMERIC(11,0), --<DESCR></DESCR>      
@PR_TRANSFER NUMERIC(1,0), --<DESCR></DESCR>      
@PR_MONTH_AWARD NUMERIC(3,0), --<DESCR></DESCR>      
@PR_RELIGION VARCHAR(15)=null, --<DESCR></DESCR>      
@PR_ZAKAT_AMT NUMERIC(5,0), --<DESCR></DESCR>      
@PR_MED_FLAG VARCHAR(1),  --<DESCR></DESCR>      
@PR_ATT_FLAG VARCHAR(1),  --<DESCR></DESCR>      
@PR_PAY_FLAG VARCHAR(2),  --<DESCR></DESCR>      
@PR_REP_NO  NUMERIC(6,0), --<DESCR></DESCR>      
@PR_REFUND_AMT NUMERIC(12,2), --<DESCR></DESCR>      
@PR_REFUND_FOR VARCHAR(9),  --<DESCR></DESCR>      
@PR_NTN_CERT_REC VARCHAR(3), --<DESCR></DESCR>      
@PR_NTN_CARD_REC VARCHAR(3), --<DESCR></DESCR>      
@PR_EMP_TYPE VARCHAR(2),  --<DESCR></DESCR>      
@LOCATION  VARCHAR(40), --<DESCR></DESCR>     
@PR_UserID VARCHAR(35), --<descr></descr>
@CL    VARCHAR(10),      
@ML    VARCHAR(10),      
@SL    VARCHAR(10),      
@PL    VARCHAR(10),      
@SLH   VARCHAR(10),      
@ID    INT  OUTPUT, --<DESCR></DESCR>      
@ACTIONTYPE  VARCHAR(50),      
@SEARCHFILTER VARCHAR(50),  --<DESCR></DESCR>      
@PR_EMAIL		VARCHAR(50),--<DESCR></DESCR>
@PR_UploadDate datetime,
@PR_User_Approved bit
,@ORETVAL  INT = NULL OUTPUT      
/*</PARAM>*/      
AS      
      
SET NOCOUNT ON      
/*<COMMENT></COMMENT>*/      
      
SET @PR_BRANCH   = NULLIF(@PR_BRANCH,'')       
SET @PR_FIRST_NAME  = NULLIF(@PR_FIRST_NAME,'')       
SET @PR_LAST_NAME  = NULLIF(@PR_LAST_NAME,'')       
SET @PR_DESIG   = NULLIF(@PR_DESIG,'')       
SET @PR_LEVEL   = NULLIF(@PR_LEVEL,'')       
SET @PR_CATEGORY  = NULLIF(@PR_CATEGORY,'')       
SET @PR_FIRST_NAME  = NULLIF(@PR_FIRST_NAME,'')      
SET @PR_LAST_NAME  = NULLIF(@PR_LAST_NAME,'')      
SET @PR_DESIG   = NULLIF(@PR_DESIG,'')      
SET @PR_LEVEL   = NULLIF(@PR_LEVEL,'')      
SET @PR_CATEGORY  = NULLIF(@PR_CATEGORY,'')       
SET @PR_FUNC_TITTLE1 = NULLIF(@PR_FUNC_TITTLE1,'')      
SET @PR_FUNC_TITTLE2 = NULLIF(@PR_FUNC_TITTLE2,'')      
SET @PR_NATIONAL_TAX = NULLIF(@PR_NATIONAL_TAX,'')      
SET @PR_ACCOUNT_NO  = NULLIF(@PR_ACCOUNT_NO,'')      
SET @PR_BANK_ID   = NULLIF(@PR_BANK_ID,'')      
      
 IF @PR_TAX_INC = NULL       
 BEGIN      
   IF @PR_TAX_PAID = NULL       
   begin       
    SET @PR_TAX_USED  = 'U'      
   END      
 END      
 ELSE      
 BEGIN      
  SET @PR_TAX_USED  = 'N'      
 END       
      
SET @PR_CLOSE_FLAG  = NULLIF(@PR_CLOSE_FLAG,'')      
SET @PR_TERMIN_TYPE  = NULLIF(@PR_TERMIN_TYPE,'')      
SET @PR_REASONS   = NULLIF(@PR_REASONS,'')      
SET @PR_RESIG_RECV  = NULLIF(@PR_RESIG_RECV ,'')      
SET @PR_APP_RESIG  = NULLIF(@PR_APP_RESIG ,'')      
SET @PR_EXIT_INTER  = NULLIF(@PR_EXIT_INTER ,'')      
SET @PR_ID_RETURN  = NULLIF(@PR_ID_RETURN ,'')      
SET @PR_NOTICE   = NULLIF(@PR_NOTICE  ,'')      
SET @PR_ARTONY   = NULLIF(@PR_ARTONY  ,'')      
SET @PR_DELETION  = NULLIF(@PR_DELETION ,'')      
SET @PR_SETTLE   = NULLIF(@PR_SETTLE  ,'')      
SET @PR_CONF_FLAG  = NULLIF(@PR_CONF_FLAG ,'')      
SET @PR_NEW_BRANCH  = NULLIF(@PR_NEW_BRANCH ,'')      
    
SET @PR_MED_FLAG  = NULLIF(@PR_MED_FLAG ,'')      
SET @PR_ATT_FLAG  = NULLIF(@PR_ATT_FLAG ,'')      
SET @PR_PAY_FLAG  = NULLIF(@PR_PAY_FLAG ,'')      
SET @PR_REFUND_FOR  = NULLIF(@PR_REFUND_FOR ,'')      
SET @PR_NTN_CERT_REC = NULLIF(@PR_NTN_CERT_REC ,'')      
SET @PR_NTN_CARD_REC = NULLIF(@PR_NTN_CARD_REC ,'')      
SET @PR_EMP_TYPE  = NULLIF(@PR_EMP_TYPE ,'')      
SET @LOCATION   = NULLIF(@LOCATION,'')      
SET @CL     = NULLIF(@CL ,'')      
SET @ML     = NULLIF(@ML ,'')      
SET @SL     = NULLIF(@SL ,'')      
SET @PL     = NULLIF(@PL,'')      
       
 IF @ACTIONTYPE = 'SAVE'      
 BEGIN      
  EXEC CHRIS_SP_REGSTHIENT_PERSONNEL_ADD      
       @PR_P_NO       
       ,@PR_BRANCH      
       ,@PR_FIRST_NAME      
       ,@PR_LAST_NAME      
       ,@PR_DESIG      
       ,@PR_LEVEL      
       ,@PR_CATEGORY      
       ,@PR_FUNC_TITTLE1      
       ,@PR_FUNC_TITTLE2      
       ,@PR_JOINING_DATE      
       ,@PR_CONFIRM      
       ,@PR_ANNUAL_PACK      
       ,@PR_NATIONAL_TAX      
       ,@PR_ACCOUNT_NO      
       ,@PR_BANK_ID      
       ,@PR_ID_ISSUE      
       ,@PR_EXPIRY      
       ,@PR_TAX_INC      
       ,@PR_TAX_PAID      
       ,@PR_TAX_USED      
       ,@PR_CLOSE_FLAG      
       ,@PR_CONFIRM_ON      
       ,@PR_EXPECTED      
       ,@PR_TERMIN_TYPE      
       ,@PR_TERMIN_DATE      
       ,@PR_REASONS      
       ,@PR_RESIG_RECV      
       ,@PR_APP_RESIG      
       ,@PR_EXIT_INTER      
       ,@PR_ID_RETURN      
       ,@PR_NOTICE      
       ,@PR_ARTONY      
       ,@PR_DELETION      
       ,@PR_SETTLE      
       ,@PR_CONF_FLAG      
       ,@PR_LAST_INCREMENT      
       ,@PR_NEXT_INCREMENT      
       ,@PR_TRANSFER_DATE      
       ,@PR_PROMOTION_DATE      
       ,@PR_NEW_BRANCH      
       ,@PR_NEW_ANNUAL_PACK      
       ,@PR_TRANSFER      
       ,@PR_MONTH_AWARD      
       ,@PR_RELIGION      
       ,@PR_ZAKAT_AMT      
       ,@PR_MED_FLAG      
       ,@PR_ATT_FLAG      
       ,@PR_PAY_FLAG      
       ,@PR_REP_NO      
       ,@PR_REFUND_AMT      
       ,@PR_REFUND_FOR      
       ,@PR_NTN_CERT_REC      
       ,@PR_NTN_CARD_REC      
       ,@PR_EMP_TYPE      
       ,@LOCATION      
       ,@ID
	   ,@PR_EMAIL
	   ,@PR_UserID
	   ,@PR_UploadDate
	   ,@PR_User_Approved
    ,@ORETVAL = @ORETVAL OUTPUT      
   --SET @ID =  @ORETVAL      
 END      
 ELSE IF @ACTIONTYPE = 'UPDATE'      
 BEGIN      
  EXEC CHRIS_SP_REGSTHIENT_PERSONNEL_UPDATE      
       @PR_P_NO      
       ,@PR_BRANCH      
       ,@PR_FIRST_NAME      
       ,@PR_LAST_NAME      
       ,@PR_DESIG      
       ,@PR_LEVEL      
       ,@PR_CATEGORY      
       ,@PR_FUNC_TITTLE1      
       ,@PR_FUNC_TITTLE2      
       ,@PR_JOINING_DATE      
       ,@PR_CONFIRM      
       ,@PR_ANNUAL_PACK      
       ,@PR_NATIONAL_TAX      
       ,@PR_ACCOUNT_NO      
       ,@PR_BANK_ID      
       ,@PR_ID_ISSUE      
       ,@PR_EXPIRY      
       ,@PR_TAX_INC      
       ,@PR_TAX_PAID      
       ,@PR_TAX_USED      
       ,@PR_CLOSE_FLAG      
       ,@PR_CONFIRM_ON      
       ,@PR_EXPECTED      
       ,@PR_TERMIN_TYPE      
       ,@PR_TERMIN_DATE      
       ,@PR_REASONS      
       ,@PR_RESIG_RECV      
       ,@PR_APP_RESIG      
       ,@PR_EXIT_INTER      
       ,@PR_ID_RETURN      
       ,@PR_NOTICE      
       ,@PR_ARTONY      
       ,@PR_DELETION      
       ,@PR_SETTLE      
       ,@PR_CONF_FLAG      
       ,@PR_LAST_INCREMENT      
       ,@PR_NEXT_INCREMENT      
       ,@PR_TRANSFER_DATE      
       ,@PR_PROMOTION_DATE      
       ,@PR_NEW_BRANCH      
       ,@PR_NEW_ANNUAL_PACK      
       ,@PR_TRANSFER      
       ,@PR_MONTH_AWARD      
       ,@PR_RELIGION      
       ,@PR_ZAKAT_AMT      
       ,@PR_MED_FLAG      
       ,@PR_ATT_FLAG      
       ,@PR_PAY_FLAG      
       ,@PR_REP_NO      
       ,@PR_REFUND_AMT      
       ,@PR_REFUND_FOR      
       ,@PR_NTN_CERT_REC      
       ,@PR_NTN_CARD_REC      
       ,@PR_EMP_TYPE      
       ,@LOCATION      
       ,@ID      
	   ,@PR_EMAIL
	   ,@PR_UserID
	   ,@PR_UploadDate
	   ,@PR_User_Approved
 END      
 ELSE IF @ACTIONTYPE = 'DELETE'      
 BEGIN      
  EXEC CHRIS_SP_REGSTHIENT_PERSONNEL_DELETE      
    @ID,@PR_CLOSE_FLAG, @ORETVAL      
 END      
 ELSE IF @ACTIONTYPE = 'GET'      
 BEGIN      
  EXEC CHRIS_SP_REGSTHIENT_PERSONNEL_GET      
    @ID,@ORETVAL      
 END      
 ELSE IF @ACTIONTYPE = 'LIST'      
 BEGIN      
  EXEC CHRIS_SP_REGSTHIENT_PERSONNEL_GETALL       
    @PR_P_NO,@oRetVal      
 END      
   
 ELSE IF @ACTIONTYPE = 'AUTHORIZE_REGULAR_STAFF'      
 BEGIN      
  UPDATE PERSONNEL SET PR_User_Approved = 1, PR_UserID = @PR_UserID WHERE ID = @ID     
 END 

ELSE IF @ACTIONTYPE = 'GET_UNAUTHORIZED_DATA'      
 BEGIN      
   Select ID  
     ,PR_P_NO  
     ,PR_BRANCH  
     ,PR_FIRST_NAME  
     ,PR_LAST_NAME  
     ,PR_DESIG  
     ,PR_LEVEL  
     ,PR_CATEGORY  
     ,PR_FUNC_TITTLE1  
     ,PR_FUNC_TITTLE2  
     ,PR_JOINING_DATE  
     ,PR_CONFIRM  
     ,PR_ANNUAL_PACK  
     ,PR_NATIONAL_TAX  
     ,PR_ACCOUNT_NO  
     ,PR_BANK_ID  
     ,PR_ID_ISSUE  
     ,PR_EXPIRY  
     ,PR_TAX_INC  
     ,PR_TAX_PAID  
     ,PR_TAX_USED  
     ,PR_CLOSE_FLAG  
     ,PR_CONFIRM_ON  
     ,PR_EXPECTED  
     ,PR_TERMIN_TYPE  
     ,PR_TERMIN_DATE  
     ,PR_REASONS  
     ,PR_RESIG_RECV  
     ,PR_APP_RESIG  
     ,PR_EXIT_INTER  
     ,PR_ID_RETURN  
     ,PR_NOTICE  
     ,PR_ARTONY  
     ,PR_DELETION  
     ,PR_SETTLE  
     ,PR_CONF_FLAG  
     ,PR_LAST_INCREMENT  
     ,PR_NEXT_INCREMENT  
     ,PR_TRANSFER_DATE  
     ,PR_PROMOTION_DATE  
     ,PR_NEW_BRANCH  
     ,PR_NEW_ANNUAL_PACK  
     ,PR_TRANSFER  
     ,PR_MONTH_AWARD  
    -- ,PR_RELIGION  
     ,PR_ZAKAT_AMT  
     ,PR_MED_FLAG  
     ,PR_ATT_FLAG  
     ,PR_PAY_FLAG  
     ,PR_REP_NO  
     ,PR_REFUND_AMT  
     ,PR_REFUND_FOR  
     ,PR_NTN_CERT_REC  
     ,PR_NTN_CARD_REC  
     ,PR_EMP_TYPE  
     ,LOCATION  
		,PR_EMAIL	
 From dbo.PERSONNEL WITH(NOLOCK)  
 WHERE PR_CLOSE_FLAG IS NULL AND PR_UserID != @PR_UserID AND PR_User_Approved = 0   
 END 
      
 ELSE IF  @ACTIONTYPE = 'SERIAL_NO'      
 BEGIN      
  IF EXISTS (SELECT * FROM SERIAL_NO WHERE PR_TYPE = 'R')      
   BEGIN      
    SELECT  (ISNULL(PR_SNO, 0) + 1) AS PR_P_NO       
    FROM SERIAL_NO      
    WHERE PR_TYPE = 'R'      
   END      
 END       
      
 ELSE IF  @ACTIONTYPE = 'GEID'      
  BEGIN      
   SELECT GEI_GEID_NO       
   FROM GEID       
   WHERE GEI_PR_P_NO = @PR_P_NO      
  END      
      
 ELSE IF @ACTIONTYPE = 'SP_CATEGORY'      
  BEGIN      
     SELECT DISTINCT SP_CATEGORY      
     FROM   DESIG      
     WHERE  SP_DESG  = @PR_DESIG      
     AND    SP_LEVEL  = @PR_LEVEL      
     AND    SP_CURRENT = 'C'      
  END      
      
 ELSE IF @ACTIONTYPE = 'DESG_VALIDATE'      
  BEGIN      
     SELECT DISTINCT SP_DESG,SP_LEVEL,SP_MIN      
   FROM DESIG      
   WHERE SP_DESG  = @PR_DESIG      
   AND   SP_LEVEL  = @PR_LEVEL      
   AND   SP_CURRENT = 'C'      
   AND  (SP_BRANCH  = @PR_NEW_BRANCH OR SP_BRANCH IS NULL);      
  END      
      
 ELSE IF @ACTIONTYPE = 'DESG_VALIDATE_WO_LEVEL'      
  BEGIN      
     SELECT DISTINCT SP_DESG,SP_LEVEL,SP_MIN,SP_CATEGORY      
   --INTO  :PR_DESIG,:PR_LEVEL,:PR_ANNUAL_PACK      
   FROM DESIG      
   WHERE SP_DESG  = @PR_DESIG      
   --AND   SP_LEVEL  = @PR_LEVEL      
   AND   SP_CURRENT = 'C'      
   AND  (SP_BRANCH  = @PR_NEW_BRANCH OR SP_BRANCH IS NULL);      
  END      
      
      
 ELSE IF @ACTIONTYPE = 'LEVEL_VALIDATE'      
  BEGIN      
     SELECT DISTINCT  SP_LEVEL ,SP_MIN      
   --INTO   :PR_LEVEL ,:PR_ANNUAL_PACK      
   FROM DESIG       
   WHERE SP_DESG = @PR_DESIG       
   AND SP_LEVEL = @PR_LEVEL      
   AND SP_CURRENT = 'C'       
   AND (SP_BRANCH = @PR_NEW_BRANCH  OR SP_BRANCH IS NULL)      
  END      
      
      
 ELSE IF @ACTIONTYPE = 'ADDMONTH'      
  BEGIN      
     SELECT DATEADD(MONTH, SP_CONFIRM, @PR_JOINING_DATE) AS 'PR_CONFIRM'      
  --   INTO   :PR_CONFIRM      
     FROM   DESIG      
     WHERE  SP_DESG    = @PR_DESIG      
     AND    SP_LEVEL   = @PR_LEVEL      
     AND    SP_CURRENT = 'C'      
  END      
      
 ELSE IF @ACTIONTYPE = 'SP_MIN'      
  BEGIN      
   SELECT SP_MIN       
   FROM DESIG       
   WHERE SP_DESG = @PR_DESIG       
   AND SP_LEVEL = @PR_LEVEL       
   AND SP_CURRENT = 'C'       
   AND (SP_BRANCH = @PR_NEW_BRANCH OR SP_BRANCH IS NULL)      
  END      
      
      
      
 ELSE IF @ACTIONTYPE = 'PROC_INC'      
  BEGIN      
   SELECT 'X' AS 'DUMMY'      
   FROM  INC_PROMOTION      
   WHERE PR_IN_NO = @PR_P_NO;      
  END      
      
 ELSE IF @ACTIONTYPE = 'NAT_TAX_LEAVE'      
  BEGIN      
   SELECT  PR_FIRST_NAME        
   FROM PERSONNEL      
   WHERE PR_NATIONAL_TAX =  @PR_NATIONAL_TAX       
    AND PR_P_NO   <> @PR_P_NO      
         
  END      
      
 ELSE IF @ACTIONTYPE = 'SerialNO'      
  BEGIN      
   SELECT isnull(PR_SNO,0)+1      
   FROM SERIAL_NO      
            WHERE PR_TYPE = 'R'      
  END      
      
 ELSE IF @ACTIONTYPE = 'Dummy'      
  BEGIN      
   SELECT 'X'       
   FROM LEAVE_STATUS       
   WHERE LS_P_NO = @PR_P_NO      
  END      
      
 ELSE IF @ACTIONTYPE = 'Insert_Lev_Status'      
  BEGIN      
   INSERT INTO LEAVE_STATUS (LS_P_NO,LS_CL_BAL,LS_ML_BAL,LS_SL_BAL,LS_PL_BAL,LS_LEV_ADV,LS_SL_HF_BAL )      
   VALUES( @PR_P_NO, @CL , @ML , @SL , @PL , 0, @SLH )      
  END      
      
      
 ELSE IF @ACTIONTYPE = 'AddSerialNum'      
  BEGIN      
   SELECT ISNULL(PR_SNO,0)+1 as SerialNum      
   FROM SERIAL_NO      
            WHERE PR_TYPE   = 'R'      
                 
  END      
      
      
----------------------------------------------------***********  LOV  ***********------------------------------------      
      
 ELSE IF @ACTIONTYPE = 'PERSONNELSINFO'      
 BEGIN      
  SELECT PR_P_NO --AS 'PERSONNEL NO.'      
      ,PR_P_NO AS 'PERSONNEL_NO'      
      ,PR_BRANCH      
      ,PR_FIRST_NAME AS 'FIRST_NAME'      
      ,PR_LAST_NAME AS 'LAST_NAME'      
      ,PR_FIRST_NAME      
      ,PR_LAST_NAME      
      ,PR_DESIG      
      ,PR_LEVEL      
      ,PR_CATEGORY      
      ,PR_FUNC_TITTLE1      
      ,PR_FUNC_TITTLE2      
      ,PR_JOINING_DATE      
      ,PR_CONFIRM      
      ,PR_ANNUAL_PACK      
      ,PR_NATIONAL_TAX      
      ,PR_ACCOUNT_NO      
      ,PR_BANK_ID      
      ,PR_ID_ISSUE      
      ,PR_EXPIRY      
      ,PR_TAX_INC      
      ,PR_TAX_PAID      
      ,PR_TAX_USED      
      ,PR_CLOSE_FLAG      
      ,PR_CONFIRM_ON      
      ,PR_EXPECTED      
      ,PR_TERMIN_TYPE      
      ,PR_TERMIN_DATE      
      ,PR_REASONS      
      ,PR_RESIG_RECV      
      ,PR_APP_RESIG      
      ,PR_EXIT_INTER      
      ,PR_ID_RETURN      
      ,PR_NOTICE      
      ,PR_ARTONY      
      ,PR_DELETION      
      ,PR_SETTLE      
      ,PR_CONF_FLAG      
      ,PR_LAST_INCREMENT      
      ,PR_NEXT_INCREMENT      
      ,PR_TRANSFER_DATE      
      ,PR_PROMOTION_DATE      
      ,PR_NEW_BRANCH      
      ,PR_NEW_ANNUAL_PACK      
      ,PR_TRANSFER      
      ,PR_MONTH_AWARD      
     -- ,PR_RELIGION      
      ,PR_ZAKAT_AMT      
      ,PR_MED_FLAG      
      ,PR_ATT_FLAG      
      ,PR_PAY_FLAG      
      --,PR_REP_NO      
      ,PR_REFUND_AMT      
      ,PR_REFUND_FOR      
      ,PR_NTN_CERT_REC      
      ,PR_NTN_CARD_REC      
      ,PR_EMP_TYPE      
      ,LOCATION      
      ,ID      
	     		  ,PR_EMAIL
    FROM PERSONNEL WITH(NOLOCK)      
  WHERE PR_CLOSE_FLAG IS NULL      
 END      
      
      
ELSE IF @ACTIONTYPE = 'PERSONNELSINFOEXIST'      
 BEGIN      
 IF EXISTS (SELECT ID FROM PERSONNEL WHERE PR_P_NO = @PR_P_NO)       
  BEGIN      
  SELECT PR_P_NO --AS 'PERSONNEL NO.'      
      ,PR_P_NO AS 'PERSONNEL_NO'      
      ,PR_BRANCH      
      ,PR_FIRST_NAME AS 'FIRST_NAME'      
      ,PR_LAST_NAME AS 'LAST_NAME'      
      ,PR_FIRST_NAME      
      ,PR_LAST_NAME      
      ,PR_DESIG      
      ,PR_LEVEL      
      ,PR_CATEGORY      
      ,PR_FUNC_TITTLE1      
      ,PR_FUNC_TITTLE2      
      ,PR_JOINING_DATE      
      ,PR_CONFIRM      
      ,PR_ANNUAL_PACK      
      ,PR_NATIONAL_TAX      
      ,PR_ACCOUNT_NO     
      ,PR_BANK_ID      
      ,PR_ID_ISSUE      
      ,PR_EXPIRY      
      ,PR_TAX_INC      
      ,PR_TAX_PAID      
      ,PR_TAX_USED      
      ,PR_CLOSE_FLAG      
      ,PR_CONFIRM_ON      
      ,PR_EXPECTED      
      ,PR_TERMIN_TYPE      
      ,PR_TERMIN_DATE      
      ,PR_REASONS      
      ,PR_RESIG_RECV      
      ,PR_APP_RESIG      
      ,PR_EXIT_INTER      
      ,PR_ID_RETURN      
      ,PR_NOTICE      
      ,PR_ARTONY      
      ,PR_DELETION      
      ,PR_SETTLE      
      ,PR_CONF_FLAG      
      ,PR_LAST_INCREMENT      
      ,PR_NEXT_INCREMENT      
      ,PR_TRANSFER_DATE      
      ,PR_PROMOTION_DATE      
      ,PR_NEW_BRANCH      
      ,PR_NEW_ANNUAL_PACK      
      ,PR_TRANSFER      
      ,PR_MONTH_AWARD      
      --,PR_RELIGION      
      ,PR_ZAKAT_AMT      
      ,PR_MED_FLAG      
      ,PR_ATT_FLAG      
      ,PR_PAY_FLAG      
      ,PR_REP_NO      
      --,PR_FIRST_NAME+' '+PR_LAST_NAME AS [Name]      
      ,( SELECT PR_FIRST_NAME+' '+PR_LAST_NAME AS [Name]      
     FROM PERSONNEL  WITH(NOLOCK)      
     WHERE PR_P_NO     <> CONVERT( NUMERIC(6,0), @PR_P_NO)      
      AND PR_CLOSE_FLAG   IS NULL       
      AND PR_TERMIN_DATE   IS NULL       
      AND ISNULL(PR_TRANSFER,0) < 6       
      AND PR_P_NO     = (SELECT PR_REP_NO       
              FROM PERSONNEL WITH(NOLOCK)       
              WHERE PR_CLOSE_FLAG IS NULL AND PR_P_NO = @PR_P_NO)      
     ) as [NAME]      
      ,PR_REFUND_AMT      
      ,PR_REFUND_FOR      
      ,PR_NTN_CERT_REC      
      ,PR_NTN_CARD_REC      
      ,PR_EMP_TYPE      
      ,LOCATION      
      ,ID      
				  ,PR_EMAIL	
    FROM PERSONNEL WITH(NOLOCK)      
  WHERE PR_CLOSE_FLAG IS NULL       
   AND PR_P_NO = @PR_P_NO      
      
  END       
      
  ELSE IF EXISTS (SELECT ID FROM PERSONNEL WHERE PR_P_NO LIKE CONVERT(VARCHAR(10),@PR_P_NO) + '%' OR ISNULL(CONVERT(VARCHAR(10),@PR_P_NO), '')= '' )      
   BEGIN      
    SELECT PR_P_NO --AS 'PERSONNEL NO.'      
      ,PR_P_NO AS 'PERSONNEL_NO'      
      ,PR_BRANCH      
      ,PR_FIRST_NAME AS 'FIRST_NAME'      
      ,PR_LAST_NAME AS 'LAST_NAME'      
      ,PR_FIRST_NAME      
      ,PR_LAST_NAME      
      ,PR_DESIG      
      ,PR_LEVEL      
      ,PR_CATEGORY      
      ,PR_FUNC_TITTLE1      
      ,PR_FUNC_TITTLE2      
      ,PR_JOINING_DATE      
      ,PR_CONFIRM      
      ,PR_ANNUAL_PACK      
      ,PR_NATIONAL_TAX      
      ,PR_ACCOUNT_NO      
      ,PR_BANK_ID      
      ,PR_ID_ISSUE      
      ,PR_EXPIRY      
      ,PR_TAX_INC      
      ,PR_TAX_PAID      
      ,PR_TAX_USED      
      ,PR_CLOSE_FLAG      
      ,PR_CONFIRM_ON      
      ,PR_EXPECTED      
      ,PR_TERMIN_TYPE      
      ,PR_TERMIN_DATE      
      ,PR_REASONS      
      ,PR_RESIG_RECV      
      ,PR_APP_RESIG      
      ,PR_EXIT_INTER      
      ,PR_ID_RETURN      
      ,PR_NOTICE      
      ,PR_ARTONY      
      ,PR_DELETION      
      ,PR_SETTLE      
      ,PR_CONF_FLAG      
      ,PR_LAST_INCREMENT      
      ,PR_NEXT_INCREMENT      
      ,PR_TRANSFER_DATE      
      ,PR_PROMOTION_DATE      
      ,PR_NEW_BRANCH      
      ,PR_NEW_ANNUAL_PACK      
      ,PR_TRANSFER      
      ,PR_MONTH_AWARD      
     -- ,PR_RELIGION      
      ,PR_ZAKAT_AMT      
      ,PR_MED_FLAG      
      ,PR_ATT_FLAG      
      ,PR_PAY_FLAG      
      ,PR_REP_NO      
      --,PR_FIRST_NAME+' '+PR_LAST_NAME AS [Name]      
      ,( SELECT PR_FIRST_NAME+' '+PR_LAST_NAME AS [Name]      
     FROM PERSONNEL  WITH(NOLOCK)      
     WHERE PR_P_NO     <> CONVERT( NUMERIC(6,0), @PR_P_NO)      
      AND PR_CLOSE_FLAG   IS NULL       
      AND PR_TERMIN_DATE   IS NULL       
      AND ISNULL(PR_TRANSFER,0) < 6       
      AND PR_P_NO     = (SELECT PR_REP_NO       
              FROM PERSONNEL WITH(NOLOCK)       
              WHERE PR_CLOSE_FLAG IS NULL AND PR_P_NO = @PR_P_NO)      
     ) as [NAME]      
      
      ,PR_REFUND_AMT      
      ,PR_REFUND_FOR      
      ,PR_NTN_CERT_REC      
      ,PR_NTN_CARD_REC      
      ,PR_EMP_TYPE      
      ,LOCATION      
      ,ID      
				  ,PR_EMAIL	
     FROM PERSONNEL WITH(NOLOCK)      
WHERE PR_CLOSE_FLAG IS NULL      
    --AND PR_P_NO = @PR_P_NO      
  END      
       
 END      
      
ELSE IF @ACTIONTYPE = 'BRANCH'      
 BEGIN      
  SELECT BRN_CODE AS PR_BRANCH, BRN_NAME AS BRANCH_NAME FROM BRANCH WITH(NOLOCK)      
 END      
ELSE IF @ACTIONTYPE = 'BRANCHEXIST'      
 BEGIN      
  IF EXISTS (SELECT BRN_CODE FROM BRANCH WHERE BRN_CODE = @PR_BRANCH)      
   BEGIN      
    SELECT BRN_CODE AS PR_BRANCH, BRN_NAME AS BRANCH_NAME       
     FROM BRANCH   WITH(NOLOCK)           
     WHERE BRN_CODE = @PR_BRANCH      
   END      
  IF EXISTS (SELECT BRN_CODE FROM BRANCH WHERE BRN_CODE LIKE @PR_BRANCH + '%' OR ISNULL(@PR_BRANCH ,'')='')      
   BEGIN      
    SELECT BRN_CODE AS PR_BRANCH, BRN_NAME AS BRANCH_NAME       
    FROM BRANCH WITH(NOLOCK)      
    WHERE BRN_CODE = @PR_BRANCH      
   END      
 END      
      
 ELSE IF @ACTIONTYPE = 'REP_NO'      
  BEGIN      
   SELECT PR_FIRST_NAME+' '+PR_LAST_NAME AS [Name], PR_P_NO AS 'Personnels_No', PR_P_NO AS PR_REP_NO      
   FROM PERSONNEL  WITH(NOLOCK)      
   WHERE PR_P_NO     <> CONVERT( NUMERIC(6,0), @SEARCHFILTER)      
    AND PR_CLOSE_FLAG   IS NULL       
    AND PR_TERMIN_DATE   IS NULL       
    AND ISNULL(PR_TRANSFER,0) < 6       
   ORDER BY 1      
      
  END      
      
 ELSE IF @ACTIONTYPE = 'REP_NO_EXIST'      
  BEGIN      
   IF EXISTS (SELECT PR_P_NO FROM PERSONNEL WITH(NOLOCK) WHERE PR_P_NO = @PR_REP_NO)      
    BEGIN      
     SELECT PR_FIRST_NAME+' '+PR_LAST_NAME AS [Name], PR_P_NO AS 'Personnels_No', PR_P_NO AS PR_REP_NO      
     FROM PERSONNEL  WITH(NOLOCK)      
     WHERE PR_P_NO     <> CONVERT( NUMERIC(6,0), @SEARCHFILTER)      
      AND PR_CLOSE_FLAG   IS NULL       
      AND PR_TERMIN_DATE   IS NULL       
      AND ISNULL(PR_TRANSFER,0) < 6       
      --AND PR_P_NO     = @PR_REP_NO      
     ORDER BY 1      
    END      
      
   ELSE IF EXISTS (SELECT PR_P_NO FROM PERSONNEL WHERE PR_P_NO LIKE CONVERT(VARCHAR(10),@PR_REP_NO) + '%' OR ISNULL(CONVERT(VARCHAR(10),@PR_REP_NO),'')='')      
    BEGIN      
     SELECT PR_FIRST_NAME+' '+PR_LAST_NAME AS [Name], PR_P_NO AS 'Personnels_No', PR_P_NO AS PR_REP_NO      
     FROM PERSONNEL  WITH(NOLOCK)      
     WHERE PR_P_NO     <> CONVERT( NUMERIC(6,0), @SEARCHFILTER)      
      AND PR_CLOSE_FLAG   IS NULL       
      AND PR_TERMIN_DATE   IS NULL       
      AND ISNULL(PR_TRANSFER,0) < 6       
      --AND PR_P_NO     = @PR_REP_NO      
     ORDER BY 1      
      
    END      
  END      
      
  ELSE IF @ACTIONTYPE = 'DESIGNATION'      
   BEGIN      
    SELECT SP_DESG AS DESIGNATION, SP_DESG AS PR_DESIG,SP_LEVEL AS [LEVEL],SP_MIN AS [MIN],SP_DESC_DG1 AS [DESCRIPTION],      SP_BRANCH AS BRANCH, SP_LEVEL AS PR_LEVEL --, SP_BRANCH AS PR_NEW_BRANCH      
    FROM DESIG  WITH(NOLOCK)      
    WHERE SP_CURRENT = 'C'       
    ORDER BY SP_CATEGORY,SP_DESG,SP_BRANCH,SP_LEVEL      
   END      
  ELSE IF @ACTIONTYPE = 'DESIGNATION_EXIST'      
   BEGIN      
    IF EXISTS (SELECT SP_DESG FROM DESIG WHERE SP_DESG = @PR_DESIG)      
    BEGIN      
     SELECT SP_DESG AS DESIGNATION, SP_DESG AS PR_DESIG,SP_BRANCH AS BRANCH --,  SP_BRANCH AS PR_NEW_BRANCH      
     FROM DESIG  WITH(NOLOCK)      
     WHERE SP_CURRENT = 'C' AND SP_DESG = @PR_DESIG AND ( [SP_BRANCH] = @SEARCHFILTER OR [SP_BRANCH] IS NULL)      
     ORDER BY SP_CATEGORY,SP_DESG,SP_BRANCH,SP_LEVEL      
    END      
      
      
    else IF EXISTS (SELECT SP_DESG FROM DESIG WHERE SP_DESG LIKE @PR_DESIG + '%' OR ISNULL(@PR_DESIG,'') ='')      
    BEGIN      
     SELECT SP_DESG AS DESIGNATION, SP_DESG AS PR_DESIG,SP_BRANCH AS BRANCH --, SP_BRANCH AS PR_NEW_BRANCH      
     FROM DESIG  WITH(NOLOCK)      
     WHERE SP_CURRENT = 'C' AND SP_DESG = @PR_DESIG AND ( [SP_BRANCH] = @SEARCHFILTER OR [SP_BRANCH] IS NULL)      
     ORDER BY SP_CATEGORY,SP_DESG,SP_BRANCH,SP_LEVEL      
    END      
   END      
  ELSE IF @ACTIONTYPE = 'DESG_LEVEL'      
   BEGIN      
    SELECT SP_LEVEL AS PR_LEVEL, SP_LEVEL AS [LEVEL],SP_MIN AS [MIN],SP_DESG AS [DESIGNATION],SP_DESC_DG1 AS [DESCRIPTION]   
    FROM DESIG       
    WHERE SP_CURRENT = 'C'      
   END      
      
  ELSE IF @ACTIONTYPE = 'DESG_LEVEL_EXIST'      
  BEGIN      
   IF EXISTS (SELECT SP_LEVEL FROM DESIG WHERE SP_LEVEL = @PR_LEVEL AND SP_CURRENT = 'C')      
    BEGIN      
     SELECT SP_LEVEL AS PR_LEVEL, SP_LEVEL AS [LEVEL],SP_MIN AS [MIN],SP_DESG AS [DESIGNATION],SP_DESC_DG1 AS [DESCRIPTION]       
     FROM DESIG  WITH(NOLOCK)      
     WHERE SP_CURRENT = 'C' AND SP_LEVEL = @PR_LEVEL AND SP_DESG = @SEARCHFILTER      
    END      
   ELSE IF EXISTS(SELECT SP_LEVEL FROM DESIG WHERE SP_LEVEL LIKE @PR_LEVEL + '%' OR ISNULL(@PR_LEVEL,'')='')      
    BEGIN      
     SELECT SP_LEVEL AS PR_LEVEL, SP_LEVEL AS [LEVEL],SP_MIN AS [MIN],SP_DESG AS [DESIGNATION],SP_DESC_DG1 AS [DESCRIPTION]       
     FROM DESIG  WITH(NOLOCK)      
     WHERE SP_CURRENT = 'C' AND SP_LEVEL = @PR_LEVEL AND SP_DESG = @SEARCHFILTER      
    END       
         
  END       
  ELSE IF @ACTIONTYPE = 'ACCOUNT_VALIDATE'      
  BEGIN    
  IF EXISTS(SELECT 1 FROM dbo.PERSONNEL where PR_ACCOUNT_NO = @PR_ACCOUNT_NO and PR_P_NO <> @PR_P_NO  )    
  BEGIN    
 SELECT PR_P_NO AS 'PR_P_NO' FROM dbo.PERSONNEL where PR_ACCOUNT_NO = @PR_ACCOUNT_NO and PR_P_NO <> @PR_P_NO  
  --END    
 -- ELSE    
 -- BEGIN    
 --SELECT PR_P_NO AS 'PR_P_NO' FROM dbo.PERSONNEL where PR_ACCOUNT_NO = @PR_ACCOUNT_NO    
  END    
      
  END    
 RETURN            

GO
/****** Object:  StoredProcedure [dbo].[CHRIS_SP_RegStHiEnt_PERSONNEL_GETALL] ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[CHRIS_SP_RegStHiEnt_PERSONNEL_GETALL]
/*<Param>*/  
@oRetVal int Output 

As  
  
Set NoCount On  
 Select ID  
     ,PR_P_NO  
     ,PR_BRANCH  
     ,PR_FIRST_NAME  
     ,PR_LAST_NAME  
     ,PR_DESIG  
     ,PR_LEVEL  
     ,PR_CATEGORY  
     ,PR_FUNC_TITTLE1  
     ,PR_FUNC_TITTLE2  
     ,PR_JOINING_DATE  
     ,PR_CONFIRM  
     ,PR_ANNUAL_PACK  
     ,PR_NATIONAL_TAX  
     ,PR_ACCOUNT_NO  
     ,PR_BANK_ID  
     ,PR_ID_ISSUE  
     ,PR_EXPIRY  
     ,PR_TAX_INC  
     ,PR_TAX_PAID  
     ,PR_TAX_USED  
     ,PR_CLOSE_FLAG  
     ,PR_CONFIRM_ON  
     ,PR_EXPECTED  
     ,PR_TERMIN_TYPE  
     ,PR_TERMIN_DATE  
     ,PR_REASONS  
     ,PR_RESIG_RECV  
     ,PR_APP_RESIG  
     ,PR_EXIT_INTER  
     ,PR_ID_RETURN  
     ,PR_NOTICE  
     ,PR_ARTONY  
     ,PR_DELETION  
     ,PR_SETTLE  
     ,PR_CONF_FLAG  
     ,PR_LAST_INCREMENT  
     ,PR_NEXT_INCREMENT  
     ,PR_TRANSFER_DATE  
     ,PR_PROMOTION_DATE  
     ,PR_NEW_BRANCH  
     ,PR_NEW_ANNUAL_PACK  
     ,PR_TRANSFER  
     ,PR_MONTH_AWARD  
    -- ,PR_RELIGION  
     ,PR_ZAKAT_AMT  
     ,PR_MED_FLAG  
     ,PR_ATT_FLAG  
     ,PR_PAY_FLAG  
     ,PR_REP_NO  
     ,PR_REFUND_AMT  
     ,PR_REFUND_FOR  
     ,PR_NTN_CERT_REC  
     ,PR_NTN_CARD_REC  
     ,PR_EMP_TYPE  
     ,LOCATION  
		,PR_EMAIL	
 From dbo.PERSONNEL WITH(NOLOCK)  
 WHERE PR_CLOSE_FLAG IS NULL AND PR_User_Approved = 1
  
  
 If @@RowCount = 0   
  Set @oRetVal = -40 --Record not found  
 Else  
  Set @oRetVal = -10  --Success  
   
 Return  
 
