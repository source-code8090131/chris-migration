USE [CHRIS_UAT]
GO
IF NOT EXISTS (
  SELECT * 
  FROM   sys.columns 
  WHERE  object_id = OBJECT_ID(N'[dbo].[CONTRACTER]') 
         AND name = 'CONT_MAKER_ID'
)
BEGIN
    ALTER TABLE CONTRACTER 
    ADD CONT_MAKER_ID varchar(50)
END
GO
IF NOT EXISTS (
  SELECT * 
  FROM   sys.columns 
  WHERE  object_id = OBJECT_ID(N'[dbo].[CONTRACTER]') 
         AND name = 'CONT_CHECKER_ID'
)
BEGIN
    ALTER TABLE CONTRACTER 
    ADD CONT_CHECKER_ID varchar(50)
END
GO
IF NOT EXISTS (
  SELECT * 
  FROM   sys.columns 
  WHERE  object_id = OBJECT_ID(N'[dbo].[CONTRACTER]') 
         AND name = 'CONT_ADDED_DATETIME'
)
BEGIN
    ALTER TABLE CONTRACTER 
    ADD CONT_ADDED_DATETIME datetime
END
GO
IF NOT EXISTS (
  SELECT * 
  FROM   sys.columns 
  WHERE  object_id = OBJECT_ID(N'[dbo].[CONTRACTER]') 
         AND name = 'CONT_UPDATED_DATETIME'
)
BEGIN
    ALTER TABLE CONTRACTER 
    ADD CONT_UPDATED_DATETIME datetime
END
GO
IF NOT EXISTS (
  SELECT * 
  FROM   sys.columns 
  WHERE  object_id = OBJECT_ID(N'[dbo].[CONTRACTER]') 
         AND name = 'CONT_IS_AUTH'
)
BEGIN
    ALTER TABLE CONTRACTER 
    ADD CONT_IS_AUTH bit
END
GO
UPDATE CONTRACTER SET CONT_IS_AUTH = 0, CONT_MAKER_ID = 'SYSTEM'
GO