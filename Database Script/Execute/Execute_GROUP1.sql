USE [CHRIS_UAT]
GO
IF NOT EXISTS (
  SELECT * 
  FROM   sys.columns 
  WHERE  object_id = OBJECT_ID(N'[dbo].[GROUP1]') 
         AND name = 'GR1_MAKER_ID'
)
BEGIN
    ALTER TABLE GROUP1 
    ADD GR1_MAKER_ID varchar(50)
END
GO
IF NOT EXISTS (
  SELECT * 
  FROM   sys.columns 
  WHERE  object_id = OBJECT_ID(N'[dbo].[GROUP1]') 
         AND name = 'GR1_CHECKER_ID'
)
BEGIN
    ALTER TABLE GROUP1 
    ADD GR1_CHECKER_ID varchar(50)
END
GO
IF NOT EXISTS (
  SELECT * 
  FROM   sys.columns 
  WHERE  object_id = OBJECT_ID(N'[dbo].[GROUP1]') 
         AND name = 'GR1_ADDED_DATETIME'
)
BEGIN
    ALTER TABLE GROUP1 
    ADD GR1_ADDED_DATETIME datetime
END
GO
IF NOT EXISTS (
  SELECT * 
  FROM   sys.columns 
  WHERE  object_id = OBJECT_ID(N'[dbo].[GROUP1]') 
         AND name = 'GR1_UPDATED_DATETIME'
)
BEGIN
    ALTER TABLE GROUP1 
    ADD GR1_UPDATED_DATETIME datetime
END
GO
IF NOT EXISTS (
  SELECT * 
  FROM   sys.columns 
  WHERE  object_id = OBJECT_ID(N'[dbo].[GROUP1]') 
         AND name = 'GR1_IS_AUTH'
)
BEGIN
    ALTER TABLE GROUP1 
    ADD GR1_IS_AUTH bit
END
GO
UPDATE GROUP1 SET GR1_IS_AUTH = 0, GR1_MAKER_ID = 'SYSTEM'
GO