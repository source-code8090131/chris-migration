USE [CHRIS_UAT]
GO
IF NOT EXISTS (
  SELECT * 
  FROM   sys.columns 
  WHERE  object_id = OBJECT_ID(N'[dbo].[MARGINAL_TAX]') 
         AND name = 'MT_MAKER_ID'
)
BEGIN
    ALTER TABLE MARGINAL_TAX 
    ADD MT_MAKER_ID varchar(50)
END
GO
IF NOT EXISTS (
  SELECT * 
  FROM   sys.columns 
  WHERE  object_id = OBJECT_ID(N'[dbo].[MARGINAL_TAX]') 
         AND name = 'MT_CHECKER_ID'
)
BEGIN
    ALTER TABLE MARGINAL_TAX 
    ADD MT_CHECKER_ID varchar(50)
END
GO
IF NOT EXISTS (
  SELECT * 
  FROM   sys.columns 
  WHERE  object_id = OBJECT_ID(N'[dbo].[MARGINAL_TAX]') 
         AND name = 'MT_ADDED_DATETIME'
)
BEGIN
    ALTER TABLE MARGINAL_TAX 
    ADD MT_ADDED_DATETIME datetime
END
GO
IF NOT EXISTS (
  SELECT * 
  FROM   sys.columns 
  WHERE  object_id = OBJECT_ID(N'[dbo].[MARGINAL_TAX]') 
         AND name = 'MT_UPDATED_DATETIME'
)
BEGIN
    ALTER TABLE MARGINAL_TAX 
    ADD MT_UPDATED_DATETIME datetime
END
GO
IF NOT EXISTS (
  SELECT * 
  FROM   sys.columns 
  WHERE  object_id = OBJECT_ID(N'[dbo].[MARGINAL_TAX]') 
         AND name = 'MT_IS_AUTH'
)
BEGIN
    ALTER TABLE MARGINAL_TAX 
    ADD MT_IS_AUTH bit
END
GO
UPDATE MARGINAL_TAX SET MT_IS_AUTH = 0, MT_MAKER_ID = 'SYSTEM'
GO
/****** Object:  StoredProcedure [dbo].[CHRIS_SP_MARGINAL_TAX_MANAGER] ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* -----------------------------------------------------------------------------------------------------------
 * SP Name		: CHRIS_SP_MARGINAL_TAXMANAGER
 * Created By	: Saad Saleem
 * Created On	: 15/02/2011
 * Purpose		: Manages (MARGINAL_TAX) store procedure depending upon the action type.
 * Module		: CHRISS
 * Called By	: iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.MARGINAL_TAXDAO
 * Comments		: None
 * -----------------------------------------------------------------------------------------------------------
///-------------------------------------Modification History--------------------------------------------------
	[Date]		[Author]			[Purpose]

///----------------------------------------------------------------------------------------------------------- */
 
ALTER PROCEDURE [dbo].[CHRIS_SP_MARGINAL_TAX_MANAGER]
/*<Param>*/
@MT_DATE_FROM DATETIME=NULL, --<descr></descr>
@MT_DATE_TO datetime=NULL, --<descr></descr>
@MT_AMT_FROM numeric(10,0)=NULL, --<descr></descr>
@MT_AMT_TO numeric(10,0)=NULL, --<descr></descr>
@MT_PERCENTAGE numeric(5,2)=NULL, --<descr></descr>
@ID INT =NULL OUTPUT,  --<descr></descr>
@ActionType varchar(50)=NULL,
@MakerId varchar(50) = null,
@CheckerkID varchar(50) = null,
@AddedDatetime datetime = null,
@UpdatedDatetime datetime = null,
@IsAuth bit = null
,@oRetVal int = Null Output
/*</Param>*/
As

Set NoCount On
/*<Comment></Comment>*/


	If @ActionType = 'Save'
	Begin
		Exec CHRIS_SP_MARGINAL_TAX_ADD
	   			@MT_DATE_FROM
	   			,@MT_DATE_TO
	   			,@MT_AMT_FROM
	   			,@MT_AMT_TO
	   			,@MT_PERCENTAGE
	   			,@ID
				,@MakerId
				,@AddedDatetime
				,@IsAuth
				,@oRetVal = @oRetVal OUTPUT
			SET @ID =  @oRetVal
	End
	Else If @ActionType = 'Update'
	Begin
		Exec CHRIS_SP_MARGINAL_TAX_UPDATE
	   			@MT_DATE_FROM
	   			,@MT_DATE_TO
	   			,@MT_AMT_FROM
	   			,@MT_AMT_TO
	   			,@MT_PERCENTAGE
	   			,@ID
				,@MakerId
				,@AddedDatetime
				,@IsAuth
	End
	Else If @ActionType = 'Delete'
	Begin
		Exec CHRIS_SP_MARGINAL_TAX_DELETE
				@ID,@oRetVal
	End
	Else If @ActionType = 'Get'
	Begin
		Exec CHRIS_SP_MARGINAL_TAX_GET
				@ID,@oRetVal
	End
	Else If @ActionType = 'List'
	Begin
		Exec CHRIS_SP_MARGINAL_TAX_GETALL @oRetVal
	End
	ELSE IF @ACTIONTYPE = 'AUTHORIZE_MARGIN_TAX'      
	 BEGIN      
	  UPDATE MARGINAL_TAX SET MT_IS_AUTH = 1, MT_CHECKER_ID = @CheckerkID,MT_UPDATED_DATETIME = @UpdatedDatetime WHERE ID = @ID     
	 END 

	 ELSE IF @ACTIONTYPE = 'REJECT_MARGIN_TAX'      
	 BEGIN      
	  DELETE FROM MARGINAL_TAX WHERE ID = @ID     
	 END 

	ELSE IF @ACTIONTYPE = 'GET_UNAUTHORIZE_MARGIN_TAX'      
	 BEGIN
		Select ID
	   	,MT_DATE_FROM
	   	,MT_DATE_TO
	   	,MT_AMT_FROM
	   	,MT_AMT_TO
	   	,MT_PERCENTAGE
	From dbo.MARGINAL_TAX WITH(NOLOCK) WHERE MT_IS_AUTH = 0 AND MT_MAKER_ID != @MakerId
	order by mt_date_from, mt_amt_from 
	Return
	 END 

	Return
GO
/****** Object:  StoredProcedure [dbo].[CHRIS_SP_MARGINAL_TAX_ADD] ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* -----------------------------------------------------------------------------------------------------------
 * SP Name		: CHRIS_SP_MARGINAL_TAXADD
 * Created By	: Saad Saleem
 * Created On	: 15/02/2011
 * Purpose		: Adds record(s) in the Table(MARGINAL_TAX) according to the parameter(s).
 * Module		: CHRISS
 * Called By	: iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.MARGINAL_TAX.InsertMARGINAL_TAX
 * Comments		: None
 * -----------------------------------------------------------------------------------------------------------
///-------------------------------------Modification History--------------------------------------------------
	[Date]		[Author]			[Purpose]

///----------------------------------------------------------------------------------------------------------- */

ALTER PROCEDURE [dbo].[CHRIS_SP_MARGINAL_TAX_ADD]
/*<Params>*/
@MT_DATE_FROM datetime, --<descr></descr>
@MT_DATE_TO datetime, --<descr></descr>
@MT_AMT_FROM numeric(10,0), --<descr></descr>
@MT_AMT_TO numeric(10,0), --<descr></descr>
@MT_PERCENTAGE numeric(5,2), --<descr></descr>
@ID INT  OUTPUT,  --<descr></descr>
@MakerId varchar(50) = null,
@AddedDatetime datetime = null,
@IsAuth bit = null,
@oRetVal int = Null Output --<descr></descr>
/*</Params>*/
As

Set NoCount On
/*<Comments></Comments>*/
/*<LocalVariables>*/
/*</LocalVariables>*/
 
	Insert Into dbo.MARGINAL_TAX
	(
		MT_DATE_FROM
		,MT_DATE_TO
		,MT_AMT_FROM
		,MT_AMT_TO
		,MT_PERCENTAGE
		,MT_MAKER_ID
		,MT_ADDED_DATETIME
		,MT_IS_AUTH
	)
	Values
	(
		@MT_DATE_FROM
		,@MT_DATE_TO
		,@MT_AMT_FROM
		,@MT_AMT_TO
		,@MT_PERCENTAGE
		,@MakerId
		,@AddedDatetime
		,@IsAuth
	)
	
	If @@RowCount > 0
	BEGIN
		Set @oRetVal = SCOPE_IDENTITY() --Record has been inserted successfully, return generated ID
		Set @ID = @oRetVal
	END
	Else
		Set @oRetVal = -40 --No record inserted
	
	Return
GO
/****** Object:  StoredProcedure [dbo].[CHRIS_SP_MARGINAL_TAX_UPDATE]  ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* -----------------------------------------------------------------------------------------------------------
 * SP Name		: CHRIS_SP_MARGINAL_TAXUPDATE
 * Created By	: Saad Saleem
 * Created On	: 15/02/2011
 * Purpose		: Updates record(s) in the Table(MARGINAL_TAX) according to the parameter(s).
 * Module		: CHRISS
 * Called By	: iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.MARGINAL_TAX.InsertMARGINAL_TAX
 * Comments		: None
 * -----------------------------------------------------------------------------------------------------------
///-------------------------------------Modification History--------------------------------------------------
	[Date]		[Author]			[Purpose]

///----------------------------------------------------------------------------------------------------------- */
 
ALTER PROCEDURE [dbo].[CHRIS_SP_MARGINAL_TAX_UPDATE]
/*<Param>*/
@MT_DATE_FROM datetime, --<descr></descr>
@MT_DATE_TO datetime, --<descr></descr>
@MT_AMT_FROM numeric(10,0), --<descr></descr>
@MT_AMT_TO numeric(10,0), --<descr></descr>
@MT_PERCENTAGE numeric(5,2), --<descr></descr>
@ID INT, --<descr></descr>
@MakerId varchar(50) = null,
@AddedDatetime datetime = null,
@IsAuth bit = null,
@oRetVal int = Null Output
/*</Param>*/
As

Set NoCount On
/*<Comment></Comment>*/
	Update	dbo.MARGINAL_TAX
	Set
	   	MT_DATE_FROM = @MT_DATE_FROM
	   	,MT_DATE_TO = @MT_DATE_TO
	   	,MT_AMT_FROM = @MT_AMT_FROM
	   	,MT_AMT_TO = @MT_AMT_TO
	   	,MT_PERCENTAGE = @MT_PERCENTAGE
		,MT_MAKER_ID = @MakerId
		,MT_ADDED_DATETIME = @AddedDatetime
		,MT_IS_AUTH = @IsAuth
	Where 	 ID = @ID	
	
	Set @oRetVal = -10 --Record updated successfully
	Return
GO
/****** Object:  StoredProcedure [dbo].[CHRIS_SP_MARGINAL_TAX_GETALL] ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* -----------------------------------------------------------------------------------------------------------
 * SP Name		: CHRIS_SP_MARGINAL_TAX_GETALL
 * Created By	: Saad Saleem
 * Created On	: 15/02/2011
 * Purpose		: Retrieve entity(MARGINAL_TAX) records as per filter provided.
 * Module		: CHRISS
 * Called By	: iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.MARGINAL_TAX.GetMARGINAL_TAXRecords
 * Comments		: None
 * -----------------------------------------------------------------------------------------------------------
///-------------------------------------Modification History--------------------------------------------------
	[Date]		[Author]			[Purpose]

///----------------------------------------------------------------------------------------------------------- */
 
ALTER PROCEDURE [dbo].[CHRIS_SP_MARGINAL_TAX_GETALL]
/*<Param>*/
@oRetVal int Output --<descr></descr>
/*</Param>*/
As

Set NoCount On
/*<Comment></Comment>*/
	Select ID
	   	,MT_DATE_FROM
	   	,MT_DATE_TO
	   	,MT_AMT_FROM
	   	,MT_AMT_TO
	   	,MT_PERCENTAGE
	From dbo.MARGINAL_TAX WITH(NOLOCK) WHERE MT_IS_AUTH = 1
	order by mt_date_from, mt_amt_from 


	If @@RowCount = 0 
		Set @oRetVal = -40	--Record not found
	Else
		Set @oRetVal = -10 	--Success
	
	Return
GO


