USE [CHRIS_UAT]
GO
IF NOT EXISTS (
  SELECT * 
  FROM   sys.columns 
  WHERE  object_id = OBJECT_ID(N'[dbo].[DEPT]') 
         AND name = 'DEPT_MAKER_ID'
)
BEGIN
    ALTER TABLE DEPT 
    ADD DEPT_MAKER_ID varchar(50)
END
GO
IF NOT EXISTS (
  SELECT * 
  FROM   sys.columns 
  WHERE  object_id = OBJECT_ID(N'[dbo].[DEPT]') 
         AND name = 'DEPT_CHECKER_ID'
)
BEGIN
    ALTER TABLE DEPT 
    ADD DEPT_CHECKER_ID varchar(50)
END
GO
IF NOT EXISTS (
  SELECT * 
  FROM   sys.columns 
  WHERE  object_id = OBJECT_ID(N'[dbo].[DEPT]') 
         AND name = 'DEPT_ADDED_DATETIME'
)
BEGIN
    ALTER TABLE DEPT 
    ADD DEPT_ADDED_DATETIME datetime
END
GO
IF NOT EXISTS (
  SELECT * 
  FROM   sys.columns 
  WHERE  object_id = OBJECT_ID(N'[dbo].[DEPT]') 
         AND name = 'DEPT_UPDATED_DATETIME'
)
BEGIN
    ALTER TABLE DEPT 
    ADD DEPT_UPDATED_DATETIME datetime
END
GO
IF NOT EXISTS (
  SELECT * 
  FROM   sys.columns 
  WHERE  object_id = OBJECT_ID(N'[dbo].[DEPT]') 
         AND name = 'DEPT_IS_AUTH'
)
BEGIN
    ALTER TABLE DEPT 
    ADD DEPT_IS_AUTH bit
END
GO
UPDATE DEPT SET DEPT_IS_AUTH = 0, DEPT_MAKER_ID = 'SYSTEM'
GO