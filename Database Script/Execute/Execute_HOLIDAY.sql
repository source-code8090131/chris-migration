USE [CHRIS_UAT]
GO
IF NOT EXISTS (
  SELECT * 
  FROM   sys.columns 
  WHERE  object_id = OBJECT_ID(N'[dbo].[HOLIDAY]') 
         AND name = 'SH_MAKER_ID'
)
BEGIN
    ALTER TABLE HOLIDAY 
    ADD SH_MAKER_ID varchar(50)
END
GO
IF NOT EXISTS (
  SELECT * 
  FROM   sys.columns 
  WHERE  object_id = OBJECT_ID(N'[dbo].[HOLIDAY]') 
         AND name = 'SH_CHECKER_ID'
)
BEGIN
    ALTER TABLE HOLIDAY 
    ADD SH_CHECKER_ID varchar(50)
END
GO
IF NOT EXISTS (
  SELECT * 
  FROM   sys.columns 
  WHERE  object_id = OBJECT_ID(N'[dbo].[HOLIDAY]') 
         AND name = 'SH_ADDED_DATETIME'
)
BEGIN
    ALTER TABLE HOLIDAY 
    ADD SH_ADDED_DATETIME datetime
END
GO
IF NOT EXISTS (
  SELECT * 
  FROM   sys.columns 
  WHERE  object_id = OBJECT_ID(N'[dbo].[HOLIDAY]') 
         AND name = 'SH_UPDATED_DATETIME'
)
BEGIN
    ALTER TABLE HOLIDAY 
    ADD SH_UPDATED_DATETIME datetime
END
GO
IF NOT EXISTS (
  SELECT * 
  FROM   sys.columns 
  WHERE  object_id = OBJECT_ID(N'[dbo].[HOLIDAY]') 
         AND name = 'SH_IS_AUTH'
)
BEGIN
    ALTER TABLE HOLIDAY 
    ADD SH_IS_AUTH bit
END
GO
UPDATE HOLIDAY SET SH_IS_AUTH = 0, SH_MAKER_ID = 'SYSTEM'
GO