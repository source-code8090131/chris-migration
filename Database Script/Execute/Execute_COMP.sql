USE [CHRIS_UAT]
GO
IF NOT EXISTS (
  SELECT * 
  FROM   sys.columns 
  WHERE  object_id = OBJECT_ID(N'[dbo].[COMP]') 
         AND name = 'COMP_MAKER_ID'
)
BEGIN
    ALTER TABLE COMP 
    ADD COMP_MAKER_ID varchar(50)
END
GO
IF NOT EXISTS (
  SELECT * 
  FROM   sys.columns 
  WHERE  object_id = OBJECT_ID(N'[dbo].[COMP]') 
         AND name = 'COMP_CHECKER_ID'
)
BEGIN
    ALTER TABLE COMP 
    ADD COMP_CHECKER_ID varchar(50)
END
GO
IF NOT EXISTS (
  SELECT * 
  FROM   sys.columns 
  WHERE  object_id = OBJECT_ID(N'[dbo].[COMP]') 
         AND name = 'COMP_ADDED_DATETIME'
)
BEGIN
    ALTER TABLE COMP 
    ADD COMP_ADDED_DATETIME datetime
END
GO
IF NOT EXISTS (
  SELECT * 
  FROM   sys.columns 
  WHERE  object_id = OBJECT_ID(N'[dbo].[COMP]') 
         AND name = 'COMP_UPDATED_DATETIME'
)
BEGIN
    ALTER TABLE COMP 
    ADD COMP_UPDATED_DATETIME datetime
END
GO
IF NOT EXISTS (
  SELECT * 
  FROM   sys.columns 
  WHERE  object_id = OBJECT_ID(N'[dbo].[COMP]') 
         AND name = 'COMP_IS_AUTH'
)
BEGIN
    ALTER TABLE COMP 
    ADD COMP_IS_AUTH bit
END
GO
UPDATE COMP SET COMP_IS_AUTH = 0, COMP_MAKER_ID = 'SYSTEM'
GO