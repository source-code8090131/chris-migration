USE [CHRIS_UAT]
GO
IF NOT EXISTS (
  SELECT * 
  FROM   sys.columns 
  WHERE  object_id = OBJECT_ID(N'[dbo].[SMTP_Settings]') 
         AND name = 'SMTP_MAKER_ID'
)
BEGIN
    ALTER TABLE SMTP_Settings 
    ADD SMTP_MAKER_ID varchar(50)
END
GO
IF NOT EXISTS (
  SELECT * 
  FROM   sys.columns 
  WHERE  object_id = OBJECT_ID(N'[dbo].[SMTP_Settings]') 
         AND name = 'SMTP_CHECKER_ID'
)
BEGIN
    ALTER TABLE SMTP_Settings 
    ADD SMTP_CHECKER_ID varchar(50)
END
GO
IF NOT EXISTS (
  SELECT * 
  FROM   sys.columns 
  WHERE  object_id = OBJECT_ID(N'[dbo].[SMTP_Settings]') 
         AND name = 'SMTP_ADDED_DATETIME'
)
BEGIN
    ALTER TABLE SMTP_Settings 
    ADD SMTP_ADDED_DATETIME datetime
END
GO
IF NOT EXISTS (
  SELECT * 
  FROM   sys.columns 
  WHERE  object_id = OBJECT_ID(N'[dbo].[SMTP_Settings]') 
         AND name = 'SMTP_UPDATED_DATETIME'
)
BEGIN
    ALTER TABLE SMTP_Settings 
    ADD SMTP_UPDATED_DATETIME datetime
END
GO
IF NOT EXISTS (
  SELECT * 
  FROM   sys.columns 
  WHERE  object_id = OBJECT_ID(N'[dbo].[SMTP_Settings]') 
         AND name = 'SMTP_IS_AUTH'
)
BEGIN
    ALTER TABLE SMTP_Settings 
    ADD SMTP_IS_AUTH bit
END
GO
UPDATE SMTP_Settings SET SMTP_IS_AUTH = 0, SMTP_MAKER_ID = 'SYSTEM'
GO