USE [CHRIS_UAT]
GO
IF NOT EXISTS (
  SELECT * 
  FROM   sys.columns 
  WHERE  object_id = OBJECT_ID(N'[dbo].[DESIG]') 
         AND name = 'SP_D_MAKER_ID'
)
BEGIN
    ALTER TABLE DESIG 
    ADD SP_D_MAKER_ID varchar(50)
END
GO
IF NOT EXISTS (
  SELECT * 
  FROM   sys.columns 
  WHERE  object_id = OBJECT_ID(N'[dbo].[DESIG]') 
         AND name = 'SP_D_CHECKER_ID'
)
BEGIN
    ALTER TABLE DESIG 
    ADD SP_D_CHECKER_ID varchar(50)
END
GO
IF NOT EXISTS (
  SELECT * 
  FROM   sys.columns 
  WHERE  object_id = OBJECT_ID(N'[dbo].[DESIG]') 
         AND name = 'SP_D_ADDED_DATETIME'
)
BEGIN
    ALTER TABLE DESIG 
    ADD SP_D_ADDED_DATETIME datetime
END
GO
IF NOT EXISTS (
  SELECT * 
  FROM   sys.columns 
  WHERE  object_id = OBJECT_ID(N'[dbo].[DESIG]') 
         AND name = 'SP_D_UPDATED_DATETIME'
)
BEGIN
    ALTER TABLE DESIG 
    ADD SP_D_UPDATED_DATETIME datetime
END
GO
IF NOT EXISTS (
  SELECT * 
  FROM   sys.columns 
  WHERE  object_id = OBJECT_ID(N'[dbo].[DESIG]') 
         AND name = 'SP_D_IS_AUTH'
)
BEGIN
    ALTER TABLE DESIG 
    ADD SP_D_IS_AUTH bit
END
GO
UPDATE DESIG SET SP_D_IS_AUTH = 0, SP_D_MAKER_ID = 'SYSTEM'
GO