USE [CHRIS_UAT]
GO
/****** Object:  StoredProcedure [dbo].[CHRIS_SP_AccEnt_ACCOUNT_ADD]    Script Date: 01/02/2024 2:26:35 pm ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* -----------------------------------------------------------------------------------------------------------
 * SP Name		: CHRIS_SP_AccEnt_ACCOUNTADD
 * ALTERd By	: Umair Mufti
 * ALTERd On	: 01/02/2011
 * Purpose		: Adds record(s) in the Table(ACCOUNT) according to the parameter(s).
 * Module		: CHRIS
 * Called By	: iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.ACCOUNT.InsertACCOUNT
 * Comments		: None
 * -----------------------------------------------------------------------------------------------------------
///-------------------------------------Modification History--------------------------------------------------
	[Date]		[Author]			[Purpose]

///----------------------------------------------------------------------------------------------------------- */

ALTER PROCEDURE [dbo].[CHRIS_SP_AccEnt_ACCOUNT_ADD]
/*<Params>*/
@SP_ACC_NO varchar(11), --<descr></descr>
@SP_ACC_DESC varchar(30), --<descr></descr>
@SP_ACC_DESC1 varchar(30), --<descr></descr>
@SP_ACC_TYPE varchar(1), --<descr></descr>
@SP_ACC_GROUP varchar(2), --<descr></descr>
@SP_ACC_AMOUNT numeric(16,2), --<descr></descr>
@ID INT  OUTPUT,  --<descr></descr>
@MakerId varchar(50) = null,
@AddedDatetime datetime = null,
@IsAuth bit = null,
@oRetVal int = Null Output --<descr></descr>
/*</Params>*/
As

Set NoCount On
/*<Comments></Comments>*/
/*<LocalVariables>*/
/*</LocalVariables>*/
 
	Insert Into dbo.ACCOUNT
	(
		SP_ACC_NO
		,SP_ACC_DESC
		,SP_ACC_DESC1
		,SP_ACC_TYPE
		,SP_ACC_GROUP
		,SP_ACC_AMOUNT
		,ACCOUNT_MAKER_ID
		,ACCOUNT_ADDED_DATETIME
		,ACCOUNT_IS_AUTH
	)
	Values
	(
		@SP_ACC_NO
		,@SP_ACC_DESC
		,@SP_ACC_DESC1
		,upper(@SP_ACC_TYPE)
		,@SP_ACC_GROUP
		,@SP_ACC_AMOUNT
		,@MakerId
		,@AddedDatetime
		,@IsAuth
	)
	
	If @@RowCount > 0
	BEGIN
		Set @oRetVal = SCOPE_IDENTITY() --Record has been inserted successfully, return generated ID
		Set @ID = @oRetVal
	END
	Else
		Set @oRetVal = -40 --No record inserted
	
	Return
GO
/****** Object:  StoredProcedure [dbo].[CHRIS_SP_AccEnt_ACCOUNT_GETALL]    Script Date: 01/02/2024 2:26:35 pm ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* -----------------------------------------------------------------------------------------------------------
 * SP Name		: CHRIS_SP_AccEnt_ACCOUNT_GETALL
 * ALTERd By	: Umair Mufti
 * ALTERd On	: 01/02/2011
 * Purpose		: Retrieve entity(ACCOUNT) records as per filter provided.
 * Module		: CHRIS
 * Called By	: iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.ACCOUNT.GetACCOUNTRecords
 * Comments		: None
 * -----------------------------------------------------------------------------------------------------------
///-------------------------------------Modification History--------------------------------------------------
	[Date]		[Author]			[Purpose]

///----------------------------------------------------------------------------------------------------------- */
 
ALTER PROCEDURE [dbo].[CHRIS_SP_AccEnt_ACCOUNT_GETALL]
/*<Param>*/
@oRetVal int Output --<descr></descr>
/*</Param>*/
As

Set NoCount On
/*<Comment></Comment>*/
	Select ID
	   	,SP_ACC_NO
	   	,SP_ACC_DESC
	   	,SP_ACC_DESC1
	   	,SP_ACC_TYPE
	   	,SP_ACC_GROUP
	   	,SP_ACC_AMOUNT
	From dbo.ACCOUNT WITH(NOLOCK) WHERE ACCOUNT_IS_AUTH = 1



	If @@RowCount = 0 
		Set @oRetVal = -40	--Record not found
	Else
		Set @oRetVal = -10 	--Success
	
	Return
GO
/****** Object:  StoredProcedure [dbo].[CHRIS_SP_AccEnt_ACCOUNT_MANAGER]    Script Date: 01/02/2024 2:26:35 pm ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* -----------------------------------------------------------------------------------------------------------
 * SP Name		: CHRIS_SP_AccEnt_ACCOUNTMANAGER
 * ALTERd By	: Umair Mufti
 * ALTERd On	: 01/02/2011
 * Purpose		: Manages (ACCOUNT) store procedure depending upon the action type.
 * Module		: CHRIS
 * Called By	: iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.ACCOUNTDAO
 * Comments		: None
 * -----------------------------------------------------------------------------------------------------------
///-------------------------------------Modification History--------------------------------------------------
	[Date]		[Author]			[Purpose]

///----------------------------------------------------------------------------------------------------------- */
 
ALTER PROCEDURE [dbo].[CHRIS_SP_AccEnt_ACCOUNT_MANAGER]
/*<Param>*/
@SP_ACC_NO varchar(11), --<descr></descr>
@SP_ACC_DESC varchar(30), --<descr></descr>
@SP_ACC_DESC1 varchar(30), --<descr></descr>
@SP_ACC_TYPE varchar(1), --<descr></descr>
@SP_ACC_GROUP varchar(2), --<descr></descr>
@SP_ACC_AMOUNT numeric(16,2), --<descr></descr>
@ID INT  OUTPUT,  --<descr></descr>
@ActionType varchar(50),
@MakerId varchar(50) = null,
@CheckerkID varchar(50) = null,
@AddedDatetime datetime = null,
@UpdatedDatetime datetime = null,
@IsAuth bit = null
,@oRetVal int = Null Output
/*</Param>*/
As

Set NoCount On
/*<Comment></Comment>*/


	If @ActionType = 'Save'
	Begin
		IF(@ID is not NULL)
		BEGIN
			Exec CHRIS_SP_AccEnt_ACCOUNT_UPDATE
	   			@SP_ACC_NO
	   			,@SP_ACC_DESC
	   			,@SP_ACC_DESC1
	   			,@SP_ACC_TYPE
	   			,@SP_ACC_GROUP
	   			,@SP_ACC_AMOUNT
	   			,@ID
				,@MakerId
				,@AddedDatetime
				,@IsAuth
		END
		ELSE
		BEGIN
			Exec CHRIS_SP_AccEnt_ACCOUNT_ADD
	   				@SP_ACC_NO
	   				,@SP_ACC_DESC
	   				,@SP_ACC_DESC1
	   				,@SP_ACC_TYPE
	   				,@SP_ACC_GROUP
	   				,@SP_ACC_AMOUNT
	   				,@ID
					,@MakerId
				    ,@AddedDatetime
				    ,@IsAuth
					,@oRetVal = @oRetVal OUTPUT
				--SET @ID =  @oRetVal
		End
	End
	Else If @ActionType = 'Update'
	Begin
		Exec CHRIS_SP_AccEnt_ACCOUNT_UPDATE
	   			@SP_ACC_NO
	   			,@SP_ACC_DESC
	   			,@SP_ACC_DESC1
	   			,@SP_ACC_TYPE
	   			,@SP_ACC_GROUP
	   			,@SP_ACC_AMOUNT
	   			,@ID
				,@MakerId
				,@AddedDatetime
				,@IsAuth
	End
	Else If @ActionType = 'Delete'
	Begin
		Exec CHRIS_SP_AccEnt_ACCOUNT_DELETE
				@ID,@oRetVal
	End
	Else If @ActionType = 'Get'
	Begin
		Exec CHRIS_SP_AccEnt_ACCOUNT_GET
				@ID,@oRetVal
	End
	Else If @ActionType = 'List'
	Begin
		Exec CHRIS_SP_AccEnt_ACCOUNT_GETALL @oRetVal
	End


	Else If @ActionType = 'CheckAccount'
	Begin
		Select SP_ACC_NO
		From dbo.ACCOUNT 
		where SP_ACC_NO = @SP_ACC_NO
	End



-------------------------------------LOV------------------------------------
	Else If @ActionType = 'ACCNO_LOV'
	Begin
		SELECT SUBSTRING(SP_ACC_NO,1,11) AS SP_ACC_NO, SP_ACC_DESC, SP_ACC_DESC1, SP_ACC_TYPE, ID
		FROM ACCOUNT 
		ORDER BY SP_ACC_NO
	End


	Else If @ActionType = 'ACCNO_LOV_EXIST'
	Begin
		If EXISTS (SELECT SP_ACC_NO FROM ACCOUNT WHERE SP_ACC_NO = @SP_ACC_NO )
		BEGIN
			SELECT SUBSTRING(SP_ACC_NO,1,11) AS SP_ACC_NO, SP_ACC_DESC, SP_ACC_DESC1, SP_ACC_TYPE, ID
			FROM ACCOUNT 
			WHERE SP_ACC_NO = @SP_ACC_NO
			ORDER BY SP_ACC_NO
		END

		If EXISTS (SELECT SP_ACC_NO FROM ACCOUNT WHERE SP_ACC_NO LIKE @SP_ACC_NO + '%' OR ISNULL(SP_ACC_NO, '')='')
		BEGIN
			SELECT SUBSTRING(SP_ACC_NO,1,11) AS SP_ACC_NO, SP_ACC_DESC, SP_ACC_DESC1, SP_ACC_TYPE, ID
			FROM ACCOUNT 
			WHERE SP_ACC_NO = @SP_ACC_NO
			ORDER BY SP_ACC_NO
		END

	End
	---------------------------------------------------------------------------------------------------------------------------------------

	ELSE IF @ACTIONTYPE = 'AUTHORIZE_AccountEntry'      
	 BEGIN      
	  UPDATE ACCOUNT SET ACCOUNT_IS_AUTH = 1, ACCOUNT_CHECKER_ID = @CheckerkID,ACCOUNT_UPDATED_DATETIME = @UpdatedDatetime WHERE ID = @ID     
	 END 

	 ELSE IF @ACTIONTYPE = 'REJECT_AccountEntry'      
	 BEGIN      
	  DELETE FROM ACCOUNT WHERE ID = @ID     
	 END 

	ELSE IF @ACTIONTYPE = 'GET_UNAUTHORIZE_AccountEntry'      
	 BEGIN
		Select ID
	   	,SP_ACC_NO
	   	,SP_ACC_DESC
	   	,SP_ACC_DESC1
	   	,SP_ACC_TYPE
	   	,SP_ACC_GROUP
	   	,SP_ACC_AMOUNT
	From dbo.ACCOUNT WITH(NOLOCK) WHERE ACCOUNT_IS_AUTH = 0 AND ACCOUNT_MAKER_ID != @MakerId
	Return
	 END 



	Return
GO
/****** Object:  StoredProcedure [dbo].[CHRIS_SP_AccEnt_ACCOUNT_UPDATE]    Script Date: 01/02/2024 2:26:35 pm ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* -----------------------------------------------------------------------------------------------------------
 * SP Name		: CHRIS_SP_AccEnt_ACCOUNTUPDATE
 * ALTERd By	: Umair Mufti
 * ALTERd On	: 01/02/2011
 * Purpose		: Updates record(s) in the Table(ACCOUNT) according to the parameter(s).
 * Module		: CHRIS
 * Called By	: iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.ACCOUNT.InsertACCOUNT
 * Comments		: None
 * -----------------------------------------------------------------------------------------------------------
///-------------------------------------Modification History--------------------------------------------------
	[Date]		[Author]			[Purpose]

///----------------------------------------------------------------------------------------------------------- */
 
ALTER PROCEDURE [dbo].[CHRIS_SP_AccEnt_ACCOUNT_UPDATE]
/*<Param>*/
@SP_ACC_NO varchar(11), --<descr></descr>
@SP_ACC_DESC varchar(30), --<descr></descr>
@SP_ACC_DESC1 varchar(30), --<descr></descr>
@SP_ACC_TYPE varchar(1), --<descr></descr>
@SP_ACC_GROUP varchar(2), --<descr></descr>
@SP_ACC_AMOUNT numeric(16,2), --<descr></descr>
@ID INT, --<descr></descr>
@MakerId varchar(50) = null,
@AddedDatetime datetime = null,
@IsAuth bit = null,
@oRetVal int = Null Output
/*</Param>*/
As

Set NoCount On
/*<Comment></Comment>*/
	Update	dbo.ACCOUNT
	Set
	   	SP_ACC_NO = @SP_ACC_NO
	   	,SP_ACC_DESC = @SP_ACC_DESC
	   	,SP_ACC_DESC1 = @SP_ACC_DESC1
	   	,SP_ACC_TYPE = UPPER(@SP_ACC_TYPE)
	   	,SP_ACC_GROUP = @SP_ACC_GROUP
	   	,SP_ACC_AMOUNT = @SP_ACC_AMOUNT
		,ACCOUNT_MAKER_ID = @MakerId
		,ACCOUNT_ADDED_DATETIME = @AddedDatetime
		,ACCOUNT_IS_AUTH = @IsAuth
	Where 	 ID = @ID	
	
	Set @oRetVal = -10 --Record updated successfully
	Return
GO
/****** Object:  StoredProcedure [dbo].[CHRIS_SP_BENCHMARK_ADD]    Script Date: 01/02/2024 2:26:35 pm ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* -----------------------------------------------------------------------------------------------------------
 * SP Name		: CHRIS_SP_BENCHMARKADD
 * ALTERd By	: Nida Nazir
 * ALTERd On	: 08/12/2010
 * Purpose		: Adds record(s) in the Table(BENCHMARK) according to the parameter(s).
 * Module		: CHRIS
 * Called By	: CORE.CHRIS.BUSINESSOBJECTS.ENTITIES.BENCHMARK.InsertBENCHMARK
 * Comments		: None
 * -----------------------------------------------------------------------------------------------------------
///-------------------------------------Modification History--------------------------------------------------
	[Date]		[Author]			[Purpose]

///----------------------------------------------------------------------------------------------------------- */

ALTER PROCEDURE [dbo].[CHRIS_SP_BENCHMARK_ADD]
/*<Params>*/
@BEN_FIN_TYPE varchar(6)=null, --<descr></descr>
@BEN_DATE datetime =null, --<descr></descr>
@BEN_RATE numeric(10,6)=null, --<descr></descr>
@ID INT =null OUTPUT,  --<descr></descr>
@oRetVal int = Null Output --<descr></descr>
/*</Params>*/
As

Set NoCount On
/*<Comments></Comments>*/
/*<LocalVariables>*/
/*</LocalVariables>*/
 
	Insert Into dbo.BENCHMARK
	(
		BEN_FIN_TYPE
		,BEN_DATE
		,BEN_RATE
	)
	Values
	(
		@BEN_FIN_TYPE
		,@BEN_DATE
		,@BEN_RATE
	)
	
	If @@RowCount > 0
	BEGIN
		Set @oRetVal = SCOPE_IDENTITY() --Record has been inserted successfully, return generated ID
		Set @ID = @oRetVal
	END
	Else
		Set @oRetVal = -40 --No record inserted
	
	Return
GO
/****** Object:  StoredProcedure [dbo].[CHRIS_SP_BENCHMARK_GETALL]    Script Date: 01/02/2024 2:26:35 pm ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* -----------------------------------------------------------------------------------------------------------
 * SP Name		: CHRIS_SP_BENCHMARK_GETALL
 * ALTERd By	: Nida Nazir
 * ALTERd On	: 08/12/2010
 * Purpose		: Retrieve entity(BENCHMARK) records as per filter provided.
 * Module		: CHRIS
 * Called By	: CORE.CHRIS.BUSINESSOBJECTS.ENTITIES.BENCHMARK.GetBENCHMARKRecords
 * Comments		: None
 * -----------------------------------------------------------------------------------------------------------
///-------------------------------------Modification History--------------------------------------------------
	[Date]		[Author]			[Purpose]

///----------------------------------------------------------------------------------------------------------- */
 
ALTER PROCEDURE [dbo].[CHRIS_SP_BENCHMARK_GETALL]
/*<Param>*/
@BEN_DATE datetime,
@oRetVal int Output --<descr></descr>
/*</Param>*/
As

Set NoCount On
/*<Comment></Comment>*/
	Select
		BENCHMARK.ID AS ID
	   	,BEN_FIN_TYPE
	   	,BEN_DATE
	   	,BEN_RATE
		,fin_type.FN_DESC w_description
	From dbo.BENCHMARK WITH(NOLOCK)
	LEFT JOIN FIN_TYPE
	ON fin_type.FN_TYPE = BENCHMARK.BEN_FIN_TYPE 
	WHERE (DATEDIFF(dd,BEN_DATE ,@BEN_DATE)=0) AND BEN_IS_AUTH = 1
    order by BEN_FIN_TYPE asc
	

	If @@RowCount = 0 
		Set @oRetVal = -40	--Record not found
	Else
		Set @oRetVal = -10 	--Success
	
	Return
GO
/****** Object:  StoredProcedure [dbo].[CHRIS_SP_BENCHMARK_MANAGER]    Script Date: 01/02/2024 2:26:35 pm ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* -----------------------------------------------------------------------------------------------------------
 * SP Name		: CHRIS_SP_BENCHMARKMANAGER
 * ALTERd By	: Nida Nazir
 * ALTERd On	: 08/12/2010
 * Purpose		: Manages (BENCHMARK) store procedure depending upon the action type.
 * Module		: CHRIS
 * Called By	: CORE.CHRIS.BUSINESSOBJECTS.ENTITIES.BENCHMARKDAO
 * Comments		: None
 * -----------------------------------------------------------------------------------------------------------
///-------------------------------------Modification History--------------------------------------------------
	[Date]		[Author]			[Purpose]

///----------------------------------------------------------------------------------------------------------- */
 
ALTER PROCEDURE [dbo].[CHRIS_SP_BENCHMARK_MANAGER]
/*<Param>*/
@BEN_FIN_TYPE varchar(6)=null, --<descr></descr>
@BEN_DATE datetime=null, --<descr></descr>
@BEN_RATE numeric(10,6)=null, --<descr></descr>
@ID INT =null OUTPUT,  --<descr></descr>
@ActionType varchar(50),
@MakerId varchar(50) = null,
@CheckerkID varchar(50) = null,
@AddedDatetime datetime = null,
@UpdatedDatetime datetime = null,
@IsAuth bit = null
,@oRetVal int = Null Output
/*</Param>*/
As

Set NoCount On
/*<Comment></Comment>*/



   If @ActionType = 'PRC01'
    BEGIN

    	
	DECLARE @Cursor_BenchMark CURSOR

	-----Declare @BEN_FIN_TYPE varchar(6)
	Declare @w_description varchar(20)

	Declare @fn_type varchar(6)
	Declare @fn_desc varchar(20)


	SET @Cursor_BenchMark = CURSOR READ_ONLY  ---FAST_FORWARD 
	FOR 
		select distinct fn_type as BEN_FIN_TYPE,fn_desc as w_description ,NULL as BEN_RATE,@BEN_DATE as BEN_DATE,ID
		from  fin_type order by fn_type;

	OPEN @Cursor_BenchMark 
	FETCH NEXT FROM @Cursor_BenchMark into @fn_type,@fn_desc,@BEN_RATE,@BEN_DATE,@ID

	WHILE (@@FETCH_STATUS = 0)

	BEGIN  
	  
	  SET @BEN_FIN_TYPE = @fn_type
	  SET @w_description = @fn_desc
	 
	FETCH NEXT FROM @Cursor_BenchMark into @fn_type,@fn_desc,@BEN_RATE,@BEN_DATE,@ID

	END

	SELECT FN_TYPE AS BEN_FIN_TYPE,FN_DESC AS w_description  ,Convert(numeric(10,6),NULL) as BEN_RATE ,@BEN_DATE as BEN_DATE,ID FROM FIN_TYPE order by fn_type
	CLOSE @Cursor_BenchMark 
	DEALLOCATE @Cursor_BenchMark 



    END



	ELSE If @ActionType = 'Save'
	Begin
		Exec CHRIS_SP_BENCHMARK_ADD
	   			@BEN_FIN_TYPE
	   			,@BEN_DATE
	   			,@BEN_RATE
	   			,@ID
				,@MakerId
				,@AddedDatetime
				,@IsAuth
				,@oRetVal = @oRetVal OUTPUT
			SET @ID =  @oRetVal
	End
	Else If @ActionType = 'Update'
	Begin
		Exec CHRIS_SP_BENCHMARK_UPDATE
	   			@BEN_FIN_TYPE
	   			,@BEN_DATE
	   			,@BEN_RATE
	   			,@ID
				,@MakerId
				,@AddedDatetime
				,@UpdatedDatetime
				,@IsAuth
	End
	Else If @ActionType = 'Delete'
	Begin
		Exec CHRIS_SP_BENCHMARK_DELETE
				@ID,@oRetVal
	End
	Else If @ActionType = 'Get'
	Begin
		Exec CHRIS_SP_BENCHMARK_GET
				@ID,@oRetVal
	End

	ELSE IF @ACTIONTYPE = 'AUTHORIZE_BENCHMARK'      
	 BEGIN      
	  UPDATE BENCHMARK SET BEN_IS_AUTH = 1, BEN_CHECKER_ID = @CheckerkID,BEN_UPDATED_DATETIME = @UpdatedDatetime WHERE ID = @ID     
	 END 

	 ELSE IF @ACTIONTYPE = 'REJECT_BENCHMARK'      
	 BEGIN      
	  DELETE FROM BENCHMARK WHERE ID = @ID     
	 END 

	ELSE IF @ACTIONTYPE = 'GET_UNAUTHORIZE_BENCHMARK'      
	 BEGIN
		Select
		BENCHMARK.ID AS ID
	   	,BEN_FIN_TYPE
	   	,BEN_DATE
	   	,BEN_RATE
		,fin_type.FN_DESC w_description
	From dbo.BENCHMARK WITH(NOLOCK)
	LEFT JOIN FIN_TYPE
	ON fin_type.FN_TYPE = BENCHMARK.BEN_FIN_TYPE 
	WHERE BEN_IS_AUTH = 0 AND BEN_MAKER_ID != @MakerId
    order by BEN_FIN_TYPE asc	
	Return
	 END 

	Else If @ActionType = 'List'
	Begin
		Exec CHRIS_SP_BENCHMARK_GETALL @BEN_DATE ,@oRetVal
	End

	Else If @ActionType = 'BenchDateLOV'
	Begin
	SELECT DISTINCT CAST(CONVERT(VARCHAR,ben_date,101)  AS DATETIME) BEN_DATE FROM  benchmark  	ORDER BY 1 
	End

	else if @ActionType='BenchDateLOVExists'
	Begin
	IF EXISTS(Select ID FROM  benchmark WHERE BEN_DATE=@BEN_DATE )  
    BEGIN  
   SELECT DISTINCT CAST(CONVERT(VARCHAR,ben_date,101)  AS DATETIME) BEN_DATE FROM  benchmark 
	--WHERE ben_date=@ben_date
	order by 1
    END 
	Else IF EXISTS (Select ID From benchmark  WHERE BEN_DATE  LIKE  @BEN_DATE + '%'  OR ISNULL(@BEN_DATE,'')='')   
		BEGIN
		SELECT DISTINCT CAST(CONVERT(VARCHAR,ben_date,101)  AS DATETIME) as BEN_DATE
		FROM benchmark 
	END  
	

  

 END 

   

Return
GO
/****** Object:  StoredProcedure [dbo].[CHRIS_SP_BENCHMARK_UPDATE]    Script Date: 01/02/2024 2:26:35 pm ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* -----------------------------------------------------------------------------------------------------------
 * SP Name		: CHRIS_SP_BENCHMARKUPDATE
 * ALTERd By	: Nida Nazir
 * ALTERd On	: 08/12/2010
 * Purpose		: Updates record(s) in the Table(BENCHMARK) according to the parameter(s).
 * Module		: CHRIS
 * Called By	: CORE.CHRIS.BUSINESSOBJECTS.ENTITIES.BENCHMARK.InsertBENCHMARK
 * Comments		: None
 * -----------------------------------------------------------------------------------------------------------
///-------------------------------------Modification History--------------------------------------------------
	[Date]		[Author]			[Purpose]

///----------------------------------------------------------------------------------------------------------- */
 
ALTER PROCEDURE [dbo].[CHRIS_SP_BENCHMARK_UPDATE]
/*<Param>*/
@BEN_FIN_TYPE varchar(6)=null, --<descr></descr>
@BEN_DATE datetime=null, --<descr></descr>
@BEN_RATE numeric(10,6)=null, --<descr></descr>
@ID INT=null, --<descr></descr>
@MakerId varchar(50),
@AddedDatetime datetime,
@UpdatedDatetime datetime,
@IsAuth bit,
@oRetVal int = Null Output
/*</Param>*/
As

Set NoCount On
/*<Comment></Comment>*/
if exists (select 1 from dbo.BENCHMARK with (nolock) where 
BEN_FIN_TYPE = @BEN_FIN_TYPE
AND CONVERT(VARCHAR(10), BEN_DATE, 101) = CONVERT(VARCHAR(10), @BEN_DATE, 101)
)--id = @ID)
begin
	Update	dbo.BENCHMARK
	Set
	   	--BEN_FIN_TYPE = @BEN_FIN_TYPE
	   	BEN_DATE = @BEN_DATE,
	   	BEN_RATE = @BEN_RATE,
		BEN_MAKER_ID = @MakerId,
		BEN_ADDED_DATETIME = @AddedDatetime,
		BEN_IS_AUTH = @IsAuth
	Where 	 --ID = @ID	
		BEN_FIN_TYPE = @BEN_FIN_TYPE
		AND CONVERT(VARCHAR(10), BEN_DATE, 101) = CONVERT(VARCHAR(10), @BEN_DATE, 101)
	Set @oRetVal = -10 --Record updated successfully
end
else
begin
	Insert Into dbo.BENCHMARK
	(
		BEN_FIN_TYPE
		,BEN_DATE
		,BEN_RATE
		,BEN_MAKER_ID
		,BEN_ADDED_DATETIME
		,BEN_IS_AUTH
	)
	Values
	(
		@BEN_FIN_TYPE
		,@BEN_DATE
		,@BEN_RATE
		,@MakerId
		,@AddedDatetime
		,@IsAuth
	)
	
	If @@RowCount > 0
	BEGIN
		Set @oRetVal = SCOPE_IDENTITY() --Record has been inserted successfully, return generated ID
		Set @ID = @oRetVal
	END
	Else
		Set @oRetVal = -40 --No record inserted

end

	Return
GO
/****** Object:  StoredProcedure [dbo].[CHRIS_SP_BRANCH_ADD]    Script Date: 01/02/2024 2:26:35 pm ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* -----------------------------------------------------------------------------------------------------------
 * SP Name		: CHRIS_SP_BRANCHADD
 * ALTERd By	: Nida Nazir
 * ALTERd On	: 06/12/2010
 * Purpose		: Adds record(s) in the Table(BRANCH) according to the parameter(s).
 * Module		: CHRIS
 * Called By	: iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.BRANCH.InsertBRANCH
 * Comments		: None
 * -----------------------------------------------------------------------------------------------------------
///-------------------------------------Modification History--------------------------------------------------
	[Date]		[Author]			[Purpose]

///----------------------------------------------------------------------------------------------------------- */

ALTER PROCEDURE [dbo].[CHRIS_SP_BRANCH_ADD]
/*<Params>*/
@BRN_CODE varchar(3), --<descr></descr>
@BRN_NAME varchar(40), --<descr></descr>
@BRN_ADD1 varchar(30), --<descr></descr>
@BRN_ADD2 varchar(30), --<descr></descr>
@BRN_ADD3 varchar(30), --<descr></descr>
@BRN_CR_AC varchar(11), --<descr></descr>
@BRN_DR_AC varchar(11), --<descr></descr>
@ID INT  OUTPUT,  --<descr></descr>
@MakerId varchar(50) = null,
@AddedDatetime datetime = null,
@IsAuth bit = null,
@oRetVal int = Null Output --<descr></descr>
/*</Params>*/
As

Set NoCount On
/*<Comments></Comments>*/
/*<LocalVariables>*/
/*</LocalVariables>*/
 
	Insert Into dbo.BRANCH
	(
		BRN_CODE
		,BRN_NAME
		,BRN_ADD1
		,BRN_ADD2
		,BRN_ADD3
		,BRN_CR_AC
		,BRN_DR_AC
		,BRANCH_MAKER_ID
		,BRANCH_ADDED_DATETIME
		,BRANCH_IS_AUTH
	)
	Values
	(
		@BRN_CODE
		,@BRN_NAME
		,@BRN_ADD1
		,@BRN_ADD2
		,@BRN_ADD3
		,@BRN_CR_AC
		,@BRN_DR_AC
		,@MakerId
		,@AddedDatetime
		,@IsAuth
	)
	
	If @@RowCount > 0
	BEGIN
		Set @oRetVal = SCOPE_IDENTITY() --Record has been inserted successfully, return generated ID
		Set @ID = @oRetVal
	END
	Else
		Set @oRetVal = -40 --No record inserted
	
	Return
GO
/****** Object:  StoredProcedure [dbo].[CHRIS_SP_BRANCH_GETALL]    Script Date: 01/02/2024 2:26:35 pm ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* -----------------------------------------------------------------------------------------------------------
 * SP Name		: CHRIS_SP_BRANCH_GETALL
 * ALTERd By	: Nida Nazir
 * ALTERd On	: 06/12/2010
 * Purpose		: Retrieve entity(BRANCH) records as per filter provided.
 * Module		: CHRIS
 * Called By	: iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.BRANCH.GetBRANCHRecords
 * Comments		: None
 * -----------------------------------------------------------------------------------------------------------
///-------------------------------------Modification History--------------------------------------------------
	[Date]		[Author]			[Purpose]

///----------------------------------------------------------------------------------------------------------- */
 
ALTER PROCEDURE [dbo].[CHRIS_SP_BRANCH_GETALL]
/*<Param>*/
@oRetVal int Output --<descr></descr>
/*</Param>*/
As

Set NoCount On
/*<Comment></Comment>*/
	Select ID
	   	,BRN_CODE
	   	,BRN_NAME
	   	,BRN_ADD1
	   	,BRN_ADD2
	   	,BRN_ADD3
	   	,BRN_CR_AC
	   	,BRN_DR_AC
	From dbo.BRANCH WITH(NOLOCK) WHERE BRANCH_IS_AUTH = 1



	If @@RowCount = 0 
		Set @oRetVal = -40	--Record not found
	Else
		Set @oRetVal = -10 	--Success
	
	Return
GO
/****** Object:  StoredProcedure [dbo].[CHRIS_SP_BRANCH_MANAGER]    Script Date: 01/02/2024 2:26:35 pm ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* -----------------------------------------------------------------------------------------------------------
 * SP Name		: CHRIS_SP_BRANCHMANAGER
 * ALTERd By	: Nida Nazir
 * ALTERd On	: 06/12/2010
 * Purpose		: Manages (BRANCH) store procedure depending upon the action type.
 * Module		: CHRIS
 * Called By	: iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.BRANCHDAO
 * Comments		: None
 * -----------------------------------------------------------------------------------------------------------
///-------------------------------------Modification History--------------------------------------------------
	[Date]		[Author]			[Purpose]

///----------------------------------------------------------------------------------------------------------- */
 
ALTER PROCEDURE [dbo].[CHRIS_SP_BRANCH_MANAGER]
/*<Param>*/
@BRN_CODE varchar(3)=null, --<descr></descr>
@BRN_NAME varchar(40)=null, --<descr></descr>
@BRN_ADD1 varchar(30)=null, --<descr></descr>
@BRN_ADD2 varchar(30)=null, --<descr></descr>
@BRN_ADD3 varchar(30)=null, --<descr></descr>
@BRN_CR_AC varchar(11)=null, --<descr></descr>
@BRN_DR_AC varchar(11)=null, --<descr></descr>
@ID INT =null OUTPUT,  --<descr></descr>
@ActionType varchar(50),
@MakerId varchar(50) = null,
@CheckerkID varchar(50) = null,
@AddedDatetime datetime = null,
@UpdatedDatetime datetime = null,
@IsAuth bit = null
,@oRetVal int = Null Output
/*</Param>*/
As

Set NoCount On
/*<Comment></Comment>*/


	If @ActionType = 'Save'
	Begin
		Exec CHRIS_SP_BRANCH_ADD
	   			@BRN_CODE
	   			,@BRN_NAME
	   			,@BRN_ADD1
	   			,@BRN_ADD2
	   			,@BRN_ADD3
	   			,@BRN_CR_AC
	   			,@BRN_DR_AC
	   			,@ID
				,@MakerId
				,@AddedDatetime
				,@IsAuth
				,@oRetVal = @oRetVal OUTPUT
			SET @ID =  @oRetVal
	End
	Else If @ActionType = 'Update'
	Begin
		Exec CHRIS_SP_BRANCH_UPDATE
	   			@BRN_CODE
	   			,@BRN_NAME
	   			,@BRN_ADD1
	   			,@BRN_ADD2
	   			,@BRN_ADD3
	   			,@BRN_CR_AC
	   			,@BRN_DR_AC
	   			,@ID
				,@MakerId
				,@AddedDatetime
				,@IsAuth
	End
	Else If @ActionType = 'Delete'
	Begin
		Exec CHRIS_SP_BRANCH_DELETE
				@ID,@oRetVal
	End
	Else If @ActionType = 'Get'
	Begin
		Exec CHRIS_SP_BRANCH_GET
				@ID,@oRetVal
	End
	Else If @ActionType = 'List'
	Begin
		Exec CHRIS_SP_BRANCH_GETALL @oRetVal
	End
		
	else if @ActionType='BranchCodeLOV'
	Begin
	select substring(brn_code,1,3) as BRN_CODE ,brn_name , 
			BRN_ADD1  	,BRN_ADD2
	   	,BRN_ADD3 	   	,BRN_CR_AC
	   	,BRN_DR_AC 
		, ID
		from branch with(Nolock) order by brn_code
	End
	else if @ActionType='BranchCodeLOVExists'
	Begin
	IF EXISTS(Select ID FROM  branch   WHERE BRN_CODE=@BRN_CODE )  
    BEGIN  
    select substring(brn_code,1,3) as BRN_CODE ,brn_name , 
			BRN_ADD1  	,BRN_ADD2
	   	,BRN_ADD3 	   	,BRN_CR_AC
	   	,BRN_DR_AC ,
		 ID from branch with(Nolock) 
	WHERE BRN_CODE=@BRN_CODE
	order by brn_code
    END     

    IF EXISTS(Select ID FROM branch  WHERE BRN_CODE LIKE @BRN_CODE +'%' OR ISNULL(@BRN_CODE, '') = '')
    BEGIN  
    select substring(brn_code,1,3) as BRN_CODE ,brn_name , 
			BRN_ADD1  	,BRN_ADD2
	   	,BRN_ADD3 	   	,BRN_CR_AC
	   	,BRN_DR_AC
		,ID  from branch with(Nolock) 
	WHERE BRN_CODE like @BRN_CODE + '%' 
	order by brn_code
    END 


	End
	else if @ActionType='BranchCount'
	Begin
	 select BRN_CODE  from   branch 
   --where  brn_code = @BRN_CODE
		
	End

	else if @ActionType='BranchCountNULL'
	Begin
		select null brn_code
		union all
	 select BRN_CODE  from   branch 
   --where  brn_code = @BRN_CODE
		
	End

	else if @ActionType='BranchCountAll'
	Begin
	select BRN_CODE  from   branch 
	union all
	select 'ALL' 
	order by 1
	END
	--******************added for Salary Arrears Form***************
	ELSE IF @ACTIONTYPE='GetBranchLovExist'
		BEGIN
			 IF EXISTS(SELECT ID FROM BRANCH  WHERE BRN_CODE= @BRN_CODE)
				SELECT BRN_CODE ,BRN_NAME FROM BRANCH WHERE BRN_CODE= @BRN_CODE
		END
		
	ELSE IF @ACTIONTYPE='GetBranchLov'
		BEGIN
			SELECT BRN_CODE ,BRN_NAME FROM BRANCH ORDER BY 1
		END

---------------------------------------------------------------------------------------------------------------------------------------

ELSE IF @ACTIONTYPE = 'AUTHORIZE_BRANCH'      
	 BEGIN      
	  UPDATE BRANCH SET BRANCH_IS_AUTH = 1, BRANCH_CHECKER_ID = @CheckerkID,BRANCH_UPDATED_DATETIME = @UpdatedDatetime WHERE ID = @ID     
	 END 

	 ELSE IF @ACTIONTYPE = 'REJECT_BRANCH'      
	 BEGIN      
	  DELETE FROM BRANCH WHERE ID = @ID     
	 END 

	ELSE IF @ACTIONTYPE = 'GET_UNAUTHORIZE_BRANCH'      
	 BEGIN
		Select ID
	   	,BRN_CODE
	   	,BRN_NAME
	   	,BRN_ADD1
	   	,BRN_ADD2
	   	,BRN_ADD3
	   	,BRN_CR_AC
	   	,BRN_DR_AC
	From dbo.BRANCH WITH(NOLOCK) WHERE BRANCH_IS_AUTH = 0 AND BRANCH_MAKER_ID != @MakerId
	Return
	 END 




	Return
GO
/****** Object:  StoredProcedure [dbo].[CHRIS_SP_BRANCH_UPDATE]    Script Date: 01/02/2024 2:26:35 pm ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* -----------------------------------------------------------------------------------------------------------
 * SP Name		: CHRIS_SP_BRANCHUPDATE
 * ALTERd By	: Nida Nazir
 * ALTERd On	: 06/12/2010
 * Purpose		: Updates record(s) in the Table(BRANCH) according to the parameter(s).
 * Module		: CHRIS
 * Called By	: iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.BRANCH.InsertBRANCH
 * Comments		: None
 * -----------------------------------------------------------------------------------------------------------
///-------------------------------------Modification History--------------------------------------------------
	[Date]		[Author]			[Purpose]

///----------------------------------------------------------------------------------------------------------- */
 
ALTER PROCEDURE [dbo].[CHRIS_SP_BRANCH_UPDATE]
/*<Param>*/
@BRN_CODE varchar(3), --<descr></descr>
@BRN_NAME varchar(40), --<descr></descr>
@BRN_ADD1 varchar(30), --<descr></descr>
@BRN_ADD2 varchar(30), --<descr></descr>
@BRN_ADD3 varchar(30), --<descr></descr>
@BRN_CR_AC varchar(11), --<descr></descr>
@BRN_DR_AC varchar(11), --<descr></descr>
@ID INT, --<descr></descr>
@MakerId varchar(50) = null,
@AddedDatetime datetime = null,
@IsAuth bit = null,
@oRetVal int = Null Output
/*</Param>*/
As

Set NoCount On
/*<Comment></Comment>*/
	Update	dbo.BRANCH
	Set
	   	BRN_CODE = @BRN_CODE
	   	,BRN_NAME = @BRN_NAME
	   	,BRN_ADD1 = @BRN_ADD1
	   	,BRN_ADD2 = @BRN_ADD2
	   	,BRN_ADD3 = @BRN_ADD3
	   	,BRN_CR_AC = @BRN_CR_AC
	   	,BRN_DR_AC = @BRN_DR_AC
		,BRANCH_MAKER_ID = @MakerId
		,BRANCH_ADDED_DATETIME = @AddedDatetime
		,BRANCH_IS_AUTH = @IsAuth
	Where 	 ID = @ID	
	
	Set @oRetVal = -10 --Record updated successfully
	Return
GO
/****** Object:  StoredProcedure [dbo].[CHRIS_SP_CATEGORY_ADD]    Script Date: 01/02/2024 2:26:35 pm ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* -----------------------------------------------------------------------------------------------------------
 * SP Name		: CHRIS_SP_CATEGORYADD
 * ALTERd By	: Faizan Ashraf
 * ALTERd On	: 08/12/2010
 * Purpose		: Adds record(s) in the Table(CATEGORY) according to the parameter(s).
 * Module		: CHRIS
 * Called By	: iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.CATEGORY.InsertCATEGORY
 * Comments		: None
 * -----------------------------------------------------------------------------------------------------------
///-------------------------------------Modification History--------------------------------------------------
	[Date]		[Author]			[Purpose]

///----------------------------------------------------------------------------------------------------------- */

ALTER PROCEDURE [dbo].[CHRIS_SP_CATEGORY_ADD]
/*<Params>*/
@SP_CAT_CODE varchar(1), --<descr></descr>
@SP_DESC varchar(20), --<descr></descr>
@ID INT  OUTPUT,  --<descr></descr>
@MakerId varchar(50) = null,
@AddedDatetime datetime = null,
@IsAuth bit = null,
@oRetVal int = Null Output --<descr></descr>
/*</Params>*/
As

Set NoCount On
/*<Comments></Comments>*/
/*<LocalVariables>*/
/*</LocalVariables>*/
--SET IDENTITY_INSERT dbo.CATEGORY ON



	Insert Into dbo.CATEGORY
	(
		 SP_CAT_CODE
		,SP_DESC
		,CAT_MAKER_ID
		,CAT_ADDED_DATETIME
		,CAT_IS_AUTH
	)
	Values
	(
		 @SP_CAT_CODE
		,@SP_DESC
		,@MakerId
		,@AddedDatetime
		,@IsAuth
	)
	
	If @@RowCount > 0
	BEGIN
		Set @oRetVal = SCOPE_IDENTITY() --Record has been inserted successfully, return generated ID
		Set @ID = @oRetVal
	END
	Else
		Set @oRetVal = -40 --No record inserted
	
	Return


	select * from CATEGORY
GO
/****** Object:  StoredProcedure [dbo].[CHRIS_SP_CATEGORY_GETALL]    Script Date: 01/02/2024 2:26:35 pm ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* -----------------------------------------------------------------------------------------------------------
 * SP Name		: CHRIS_SP_CATEGORY_GETALL
 * ALTERd By	: Faizan Ashraf
 * ALTERd On	: 08/12/2010
 * Purpose		: Retrieve entity(CATEGORY) records as per filter provided.
 * Module		: CHRIS
 * Called By	: iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.CATEGORY.GetCATEGORYRecords
 * Comments		: None
 * -----------------------------------------------------------------------------------------------------------
///-------------------------------------Modification History--------------------------------------------------
	[Date]		[Author]			[Purpose]

///----------------------------------------------------------------------------------------------------------- */
 
ALTER PROCEDURE [dbo].[CHRIS_SP_CATEGORY_GETALL]
/*<Param>*/
@oRetVal int Output --<descr></descr>
/*</Param>*/
As

Set NoCount On
/*<Comment></Comment>*/
	Select ID 
	   	,SP_CAT_CODE
	   	,SP_DESC
	From dbo.CATEGORY WITH(NOLOCK) WHERE CAT_IS_AUTH = 1



	If @@RowCount = 0 
		Set @oRetVal = -40	--Record not found
	Else
		Set @oRetVal = -10 	--Success
	
	Return
GO
/****** Object:  StoredProcedure [dbo].[CHRIS_SP_CATEGORY_MANAGER]    Script Date: 01/02/2024 2:26:35 pm ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* -----------------------------------------------------------------------------------------------------------
 * SP Name		: CHRIS_SP_CATEGORYMANAGER
 * ALTERd By	: Faizan Ashraf
 * ALTERd On	: 08/12/2010
 * Purpose		: Manages (CATEGORY) store procedure depending upon the action type.
 * Module		: CHRIS
 * Called By	: iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.CATEGORYDAO
 * Comments		: None
 * -----------------------------------------------------------------------------------------------------------
///-------------------------------------Modification History--------------------------------------------------
	[Date]		[Author]			[Purpose]

///----------------------------------------------------------------------------------------------------------- */
ALTER PROCEDURE [dbo].[CHRIS_SP_CATEGORY_MANAGER]
/*<Param>*/
@SP_CAT_CODE varchar(1), --<descr></descr>
@SP_DESC varchar(20), --<descr></descr>
@ID INT  OUTPUT,  --<descr></descr>
@ActionType varchar(50),
@MakerId varchar(50) = null,
@CheckerkID varchar(50) = null,
@AddedDatetime datetime = null,
@UpdatedDatetime datetime = null,
@IsAuth bit = null
,@oRetVal int = Null Output
/*</Param>*/
As

Set NoCount On
/*<Comment></Comment>*/


	If @ActionType = 'Save'
	Begin
		Exec CHRIS_SP_CATEGORY_ADD
	   			@SP_CAT_CODE
	   			,@SP_DESC
				,@MakerId
				,@AddedDatetime
				,@IsAuth
	   			,@ID
				,@oRetVal = @oRetVal OUTPUT
			--SET @ID =  @oRetVal
	End
	Else If @ActionType = 'Update'
	Begin
		Exec CHRIS_SP_CATEGORY_UPDATE
	   			@SP_CAT_CODE
	   			,@SP_DESC
	   			,@ID
				,@MakerId
				,@AddedDatetime
				,@IsAuth
	End
	Else If @ActionType = 'Delete'
	Begin
		Exec CHRIS_SP_CATEGORY_DELETE
				@ID,@oRetVal
	End
	Else If @ActionType = 'Get'
	Begin
		Exec CHRIS_SP_CATEGORY_GET
				@ID,@oRetVal
	End
	Else If @ActionType = 'List'
	Begin
		Exec CHRIS_SP_CATEGORY_GETALL @oRetVal
	End

    Else if @ActionType = 'LovCatCode'
    Begin
    select Substring(sp_cat_code,1,1) as sp_cat_code  from category order by sp_cat_code
    End

    Else if @ActionType = 'Proc_mod_del'
    Begin
    SELECT
			 @sp_cat_code  =  sp_cat_code,
			 @sp_desc  =  sp_desc
	FROM  category (UPDLOCK)  
	WHERE	 sp_cat_code  = @sp_cat_code
    End

    else if @ActionType = 'proc_view_add'
    Begin
    SELECT  
			sp_cat_code,
			sp_desc
	FROM  category 
	WHERE	 sp_cat_code  = @sp_cat_code
    End

   else if @ActionType = 'Get_ID'
    Begin
    SELECT  
			ID
	FROM  category 
	WHERE	 sp_cat_code  = @sp_cat_code
    End



   else if @ActionType = 'LovCatCodeExisit'
    Begin
       If Exists(Select ID from category where sp_cat_code =@sp_cat_code) 
       Begin
       select Substring(sp_cat_code,1,1) as sp_cat_code from category WHERE	 sp_cat_code  = @sp_cat_code
       End
    End

else if @ActionType = 'GetCatCode'
		begin
			Select ID 
	   				,SP_CAT_CODE
	   				,SP_DESC
		From dbo.CATEGORY WITH(NOLOCK)
		WHERE	 sp_cat_code  = @sp_cat_code
		end
   

else if @ActionType = 'CAT_CMB'
		begin
			Select DISTINCT ATTR_CATEGORY_DESC
		From ATTR_CATEGORY WITH(NOLOCK)
		
		ORDER BY ATTR_CATEGORY_DESC ASC
		end

else if @ActionType = 'CAT_CMB_null'
		begin
		select null ATTR_CATEGORY_DESC
		union all
			Select DISTINCT ATTR_CATEGORY_DESC
		From ATTR_CATEGORY WITH(NOLOCK)
		
		ORDER BY ATTR_CATEGORY_DESC ASC
		end

else if @ActionType = 'TYP_CMB'
		begin
			Select DISTINCT ATTR_TYPE_DESC
		From ATTR_TYPE WITH(NOLOCK)
		
		ORDER BY ATTR_TYPE_DESC ASC
		end
else if @ActionType = 'TYP_CMB_null'
		begin
			select null ATTR_TYPE_DESC
			union all 
			Select DISTINCT ATTR_TYPE_DESC
		From ATTR_TYPE WITH(NOLOCK)
		ORDER BY ATTR_TYPE_DESC ASC
		end

		--select * from CATEGORY

		ELSE IF @ACTIONTYPE = 'AUTHORIZE_SP_CATEGORY'      
	 BEGIN      
	  UPDATE CATEGORY SET CAT_IS_AUTH = 1, CAT_CHECKER_ID = @CheckerkID,CAT_UPDATED_DATETIME = @UpdatedDatetime WHERE ID = @ID     
	 END 

	 ELSE IF @ACTIONTYPE = 'REJECT_SP_CATEGORY'      
	 BEGIN      
	  DELETE FROM CATEGORY WHERE ID = @ID     
	 END 

	ELSE IF @ACTIONTYPE = 'GET_UNAUTHORIZE_SP_CATEGORY'      
	 BEGIN

	 Select ID 
	   	,SP_CAT_CODE
	   	,SP_DESC
	From dbo.CATEGORY WITH(NOLOCK) WHERE CAT_IS_AUTH = 0 AND CAT_MAKER_ID != @MakerId

		 
	Return
	 END 

	Return
GO
/****** Object:  StoredProcedure [dbo].[CHRIS_SP_CATEGORY_UPDATE]    Script Date: 01/02/2024 2:26:35 pm ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* -----------------------------------------------------------------------------------------------------------
 * SP Name		: CHRIS_SP_CATEGORYUPDATE
 * ALTERd By	: Faizan Ashraf
 * ALTERd On	: 08/12/2010
 * Purpose		: Updates record(s) in the Table(CATEGORY) according to the parameter(s).
 * Module		: CHRIS
 * Called By	: iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.CATEGORY.InsertCATEGORY
 * Comments		: None
 * -----------------------------------------------------------------------------------------------------------
///-------------------------------------Modification History--------------------------------------------------
	[Date]		[Author]			[Purpose]

///----------------------------------------------------------------------------------------------------------- */
 
ALTER PROCEDURE [dbo].[CHRIS_SP_CATEGORY_UPDATE]
/*<Param>*/
@SP_CAT_CODE varchar(1), --<descr></descr>
@SP_DESC varchar(20), --<descr></descr>
@ID INT, --<descr></descr>
@MakerId varchar(50) = null,
@AddedDatetime datetime = null,
@IsAuth bit = null,
@oRetVal int = Null Output
/*</Param>*/
As

Set NoCount On
/*<Comment></Comment>*/
	Update	dbo.CATEGORY
	Set
	   	SP_CAT_CODE = @SP_CAT_CODE
	   	,SP_DESC = @SP_DESC
		,CAT_MAKER_ID = @MakerId
		,CAT_ADDED_DATETIME = @AddedDatetime
		,CAT_IS_AUTH = @IsAuth
	Where 	 ID = @ID	
	
	Set @oRetVal = -10 --Record updated successfully
	Return
GO
/****** Object:  StoredProcedure [dbo].[CHRIS_SP_COM_GLOBAL_PARAM_MAPPING_MANAGER]    Script Date: 01/02/2024 2:26:35 pm ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* -----------------------------------------------------------------------------------------------------------
 * SP Name		: CHRIS_SP_COM_GLOBAL_PARAM_MAPPINGMANAGER
 * ALTERd By	: Azam Farooq
 * ALTERd On	: 10/01/2011
 * Purpose		: Manages (COM_GLOBAL_PARAM_MAPPING) store procedure depending upon the action type.
 * Module		: CHRIS
 * Called By	: iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.COM_GLOBAL_PARAM_MAPPING DAO
 * Comments		: None
 * -----------------------------------------------------------------------------------------------------------
///-------------------------------------Modification History--------------------------------------------------
	[Date]		[Author]			[Purpose]

///----------------------------------------------------------------------------------------------------------- */
 
ALTER PROCEDURE [dbo].[CHRIS_SP_COM_GLOBAL_PARAM_MAPPING_MANAGER]
/*<Param>*/
@ID INT = Null, --<descr></descr>
@SOE_ID varchar(50) = Null, --<descr></descr>
@GLOBAL_PARAM_ID int = Null, --<descr></descr>
@GLOBAL_PARAM_VALUE varchar(100) = Null, --<descr></descr>
@GLOBAL_PARAM_NAME varchar(100) = Null, 
@APP_CD varchar(10) = Null,
@ActionType varchar(50) = Null,
@MakerId varchar(50) = null,
@CheckerkID varchar(50) = null,
@AddedDatetime datetime = null,
@UpdatedDatetime datetime = null,
@IsAuth bit = null,
@oRetVal int = Null Output
/*</Param>*/
As

Set NoCount On
/*<Comment></Comment>*/

	EXEC [COMMON].[DBO].[COM_SP_COM_GLOBAL_PARAM_MAPPING_MANAGER]
			@ID, @SOE_ID, 
			@GLOBAL_PARAM_ID, 
			@GLOBAL_PARAM_VALUE, 
			@GLOBAL_PARAM_NAME, 
			'CHRIS',
			@ActionType, 
			@MakerId,       
			@CheckerkID,      
			@AddedDatetime,   
			@UpdatedDatetime, 
			@IsAuth    ,      
			@oRetVal
	Return
GO
/****** Object:  StoredProcedure [dbo].[CHRIS_SP_CompEnt_COMP_ADD]    Script Date: 01/02/2024 2:26:35 pm ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* -----------------------------------------------------------------------------------------------------------
 * SP Name		: CHRIS_SP_CompEnt_COMPADD
 * ALTERd By	: UMAIR MUFTI
 * ALTERd On	: 30/01/2011
 * Purpose		: Adds record(s) in the Table(COMP) according to the parameter(s).
 * Module		: CHRIS
 * Called By	: iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.COMP.InsertCOMP
 * Comments		: None
 * -----------------------------------------------------------------------------------------------------------
///-------------------------------------Modification History--------------------------------------------------
	[Date]		[Author]			[Purpose]

///----------------------------------------------------------------------------------------------------------- */

ALTER PROCEDURE [dbo].[CHRIS_SP_CompEnt_COMP_ADD]
/*<Params>*/
@LVL_CODE varchar(5), --<descr></descr>
@GRPHD varchar(1), --<descr></descr>
@MIN_SAL numeric(11,2), --<descr></descr>
@MAX_SAL numeric(11,2), --<descr></descr>
@MID_SAL numeric(11,2), --<descr></descr>
@BDGT_PRICE numeric(11,2), --<descr></descr>
@ANL_DEPC numeric(10,2), --<descr></descr>
@PTRL_LTR numeric(8,2), --<descr></descr>
@PTRL_RATE numeric(8,2), --<descr></descr>
@INS_RATE numeric(11,2), --<descr></descr>
@MNTNC_AMT numeric(11,2), --<descr></descr>
@DRVR_COST numeric(11,2), --<descr></descr>
@GRPHD_ALL numeric(11,2), --<descr></descr>
@BO_YEAR numeric(4,0), --<descr></descr>
@ID INT  OUTPUT,  --<descr></descr>
@MakerId varchar(50) = null,
@AddedDatetime datetime = null,
@IsAuth bit = null,
@oRetVal int = Null Output --<descr></descr>
/*</Params>*/
As

Set NoCount On
/*<Comments></Comments>*/
/*<LocalVariables>*/
/*</LocalVariables>*/
 
	Insert Into dbo.COMP
	(
		LVL_CODE
		,GRPHD
		,MIN_SAL
		,MAX_SAL
		,MID_SAL
		,BDGT_PRICE
		,ANL_DEPC
		,PTRL_LTR
		,PTRL_RATE
		,INS_RATE
		,MNTNC_AMT
		,DRVR_COST
		,GRPHD_ALL
		,BO_YEAR
		,COMP_MAKER_ID
		,COMP_ADDED_DATETIME
		,COMP_IS_AUTH
	)
	Values
	(
		@LVL_CODE
		,@GRPHD
		,@MIN_SAL
		,@MAX_SAL
		,@MID_SAL
		,@BDGT_PRICE
		,@ANL_DEPC
		,@PTRL_LTR
		,@PTRL_RATE
		,@INS_RATE
		,@MNTNC_AMT
		,@DRVR_COST
		,@GRPHD_ALL
		,@BO_YEAR
		,@MakerId
		,@AddedDatetime
		,@IsAuth
	)
	
	If @@RowCount > 0
	BEGIN
		Set @oRetVal = SCOPE_IDENTITY() --Record has been inserted successfully, return generated ID
		Set @ID = @oRetVal
	END
	Else
		Set @oRetVal = -40 --No record inserted
	
	Return
GO
/****** Object:  StoredProcedure [dbo].[CHRIS_SP_CompEnt_COMP_GETALL]    Script Date: 01/02/2024 2:26:35 pm ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* -----------------------------------------------------------------------------------------------------------
 * SP Name		: CHRIS_SP_CompEnt_COMP_GETALL
 * ALTERd By	: UMAIR MUFTI
 * ALTERd On	: 30/01/2011
 * Purpose		: Retrieve entity(COMP) records as per filter provided.
 * Module		: CHRIS
 * Called By	: iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.COMP.GetCOMPRecords
 * Comments		: None
 * -----------------------------------------------------------------------------------------------------------
///-------------------------------------Modification History--------------------------------------------------
	[Date]		[Author]			[Purpose]

///----------------------------------------------------------------------------------------------------------- */
 
ALTER PROCEDURE [dbo].[CHRIS_SP_CompEnt_COMP_GETALL]
/*<Param>*/
@oRetVal int Output --<descr></descr>
/*</Param>*/
As

Set NoCount On
/*<Comment></Comment>*/
	Select ID
	   	,CONVERT(VARCHAR ,LVL_CODE) AS LVL_CODE
	   	,GRPHD
	   	,MIN_SAL 
	   	,MAX_SAL 
	   	,MID_SAL
	   	,BDGT_PRICE
	   	,ANL_DEPC
	   	,PTRL_LTR
	   	,PTRL_RATE
	   	,INS_RATE
	   	,MNTNC_AMT
	   	,DRVR_COST
	   	,GRPHD_ALL
	   	,BO_YEAR
	From dbo.COMP WITH(NOLOCK) WHERE COMP_IS_AUTH = 1
	order by lvl_code desc

--
--Select ID
--	   	,LVL_CODE
--	   	,GRPHD
--	   	,CONVERT(numeric, MIN_SAL ) AS MIN_SAL
--,CONVERT(numeric, MAX_SAL ) AS MAX_SAL 
--,MID_SAL
--,CONVERT(numeric, BDGT_PRICE) AS BDGT_PRICE
--,CONVERT(numeric, ANL_DEPC) AS ANL_DEPC
--,CONVERT(numeric, PTRL_LTR) AS PTRL_LTR
--,CONVERT(numeric, PTRL_RATE) AS PTRL_RATE
--,CONVERT(numeric, INS_RATE) AS INS_RATE
--,CONVERT(numeric, MNTNC_AMT) AS MNTNC_AMT
--,CONVERT(numeric, DRVR_COST) AS DRVR_COST
--,CONVERT(numeric, GRPHD_ALL) AS GRPHD_ALL
--	   	,BO_YEAR
--	From dbo.COMP WITH(NOLOCK)
--	order by lvl_code desc



	If @@RowCount = 0 
		Set @oRetVal = -40	--Record not found
	Else
		Set @oRetVal = -10 	--Success
	
	Return
GO
/****** Object:  StoredProcedure [dbo].[CHRIS_SP_CompEnt_COMP_MANAGER]    Script Date: 01/02/2024 2:26:35 pm ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* -----------------------------------------------------------------------------------------------------------
 * SP Name		: CHRIS_SP_CompEnt_COMPMANAGER
 * ALTERd By	: UMAIR MUFTI
 * ALTERd On	: 30/01/2011
 * Purpose		: Manages (COMP) store procedure depending upon the action type.
 * Module		: CHRIS
 * Called By	: iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.COMPDAO
 * Comments		: None
 * -----------------------------------------------------------------------------------------------------------
///-------------------------------------Modification History--------------------------------------------------
	[Date]		[Author]			[Purpose]

///----------------------------------------------------------------------------------------------------------- */
 
ALTER PROCEDURE [dbo].[CHRIS_SP_CompEnt_COMP_MANAGER]
/*<Param>*/
@LVL_CODE varchar(5), --<descr></descr>
@GRPHD varchar(1), --<descr></descr>
@MIN_SAL numeric(11,2), --<descr></descr>
@MAX_SAL numeric(11,2), --<descr></descr>
@MID_SAL numeric(11,2), --<descr></descr>
@BDGT_PRICE numeric(11,2), --<descr></descr>
@ANL_DEPC numeric(10,2), --<descr></descr>
@PTRL_LTR numeric(8,2), --<descr></descr>
@PTRL_RATE numeric(8,2), --<descr></descr>
@INS_RATE numeric(11,2), --<descr></descr>
@MNTNC_AMT numeric(11,2), --<descr></descr>
@DRVR_COST numeric(11,2), --<descr></descr>
@GRPHD_ALL numeric(11,2), --<descr></descr>
@BO_YEAR numeric(4,0), --<descr></descr>
@GRHD_NO numeric(10,0) = null, --<descr></descr>
@ID INT  OUTPUT,  --<descr></descr>
@ActionType varchar(50),
@MakerId varchar(50) = null,
@CheckerkID varchar(50) = null,
@AddedDatetime datetime = null,
@UpdatedDatetime datetime = null,
@IsAuth bit = null
,@oRetVal int = Null Output
/*</Param>*/
As

Set NoCount On
/*<Comment></Comment>*/


	If @ActionType = 'Save'
	Begin
		IF(@GRPHD is null)
		BEGIN
				SET @GRPHD ='N'
		END

		Exec CHRIS_SP_CompEnt_COMP_ADD
	   			@LVL_CODE
	   			,@GRPHD
	   			,@MIN_SAL
	   			,@MAX_SAL
	   			,@MID_SAL
	   			,@BDGT_PRICE
	   			,@ANL_DEPC
	   			,@PTRL_LTR
	   			,@PTRL_RATE
	   			,@INS_RATE
	   			,@MNTNC_AMT
	   			,@DRVR_COST
	   			,@GRPHD_ALL
	   			,@BO_YEAR
	   			,@ID
				,@MakerId
				,@AddedDatetime
				,@IsAuth
				,@oRetVal = @oRetVal OUTPUT
			SET @ID =  @oRetVal
	End
	Else If @ActionType = 'Update'
	Begin
		IF(@GRPHD is null)
		BEGIN
				SET @GRPHD ='N'
		END
		Exec CHRIS_SP_CompEnt_COMP_UPDATE
	   			@LVL_CODE
	   			,@GRPHD
	   			,@MIN_SAL
	   			,@MAX_SAL
	   			,@MID_SAL
	   			,@BDGT_PRICE
	   			,@ANL_DEPC
	   			,@PTRL_LTR
	   			,@PTRL_RATE
	   			,@INS_RATE
	   			,@MNTNC_AMT
	   			,@DRVR_COST
	   			,@GRPHD_ALL
	   			,@BO_YEAR
	   			,@ID
				,@MakerId
				,@AddedDatetime
				,@IsAuth
	End
	Else If @ActionType = 'Delete'
	Begin
		Exec CHRIS_SP_CompEnt_COMP_DELETE
				@ID,@oRetVal
	End
	Else If @ActionType = 'Get'
	Begin
		Exec CHRIS_SP_CompEnt_COMP_GET
				@ID,@oRetVal
	End
	Else If @ActionType = 'List'
	Begin
		Exec CHRIS_SP_CompEnt_COMP_GETALL @oRetVal
	End

	Else If @ActionType = 'LvlCode'
	Begin
		SELECT LVL_CODE 
		FROM COMP
		WHERE LVL_CODE = @LVL_CODE
	End

	Else If @ActionType = 'Group_Head'
	Begin
		SELECT GRHD_NO ,PR_FIRST_NAME + ' '+ PR_LAST_NAME 
		FROM GROUP_HEAD g
		inner join PERSONNEL p
		on g.GRHD_NO = p.PR_P_NO
		
	End
------------------------------------------------------------------------------------------------------------------------------------------
	
	ELSE IF @ACTIONTYPE = 'AUTHORIZE_COMP'      
	 BEGIN      
	  UPDATE COMP SET COMP_IS_AUTH = 1, COMP_CHECKER_ID = @CheckerkID,COMP_UPDATED_DATETIME = @UpdatedDatetime WHERE ID = @ID     
	 END 

	 ELSE IF @ACTIONTYPE = 'REJECT_COMP'      
	 BEGIN      
	  DELETE FROM COMP WHERE ID = @ID     
	 END 

	ELSE IF @ACTIONTYPE = 'GET_UNAUTHORIZE_COMP'      
	 BEGIN
		Select ID
	   	,CONVERT(VARCHAR ,LVL_CODE) AS LVL_CODE
	   	,GRPHD
	   	,MIN_SAL 
	   	,MAX_SAL 
	   	,MID_SAL
	   	,BDGT_PRICE
	   	,ANL_DEPC
	   	,PTRL_LTR
	   	,PTRL_RATE
	   	,INS_RATE
	   	,MNTNC_AMT
	   	,DRVR_COST
	   	,GRPHD_ALL
	   	,BO_YEAR
	From dbo.COMP WITH(NOLOCK) WHERE COMP_IS_AUTH = 0 AND COMP_MAKER_ID != @MakerId
	order by lvl_code desc
	Return
	 END 

	Return
GO
/****** Object:  StoredProcedure [dbo].[CHRIS_SP_CompEnt_COMP_UPDATE]    Script Date: 01/02/2024 2:26:35 pm ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* -----------------------------------------------------------------------------------------------------------
 * SP Name		: CHRIS_SP_CompEnt_COMPUPDATE
 * ALTERd By	: UMAIR MUFTI
 * ALTERd On	: 30/01/2011
 * Purpose		: Updates record(s) in the Table(COMP) according to the parameter(s).
 * Module		: CHRIS
 * Called By	: iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.COMP.InsertCOMP
 * Comments		: None
 * -----------------------------------------------------------------------------------------------------------
///-------------------------------------Modification History--------------------------------------------------
	[Date]		[Author]			[Purpose]

///----------------------------------------------------------------------------------------------------------- */
 
ALTER PROCEDURE [dbo].[CHRIS_SP_CompEnt_COMP_UPDATE]
/*<Param>*/
@LVL_CODE varchar(5), --<descr></descr>
@GRPHD varchar(1), --<descr></descr>
@MIN_SAL numeric(11,2), --<descr></descr>
@MAX_SAL numeric(11,2), --<descr></descr>
@MID_SAL numeric(11,2), --<descr></descr>
@BDGT_PRICE numeric(11,2), --<descr></descr>
@ANL_DEPC numeric(10,2), --<descr></descr>
@PTRL_LTR numeric(8,2), --<descr></descr>
@PTRL_RATE numeric(8,2), --<descr></descr>
@INS_RATE numeric(11,2), --<descr></descr>
@MNTNC_AMT numeric(11,2), --<descr></descr>
@DRVR_COST numeric(11,2), --<descr></descr>
@GRPHD_ALL numeric(11,2), --<descr></descr>
@BO_YEAR numeric(4,0), --<descr></descr>
@ID INT, --<descr></descr>
@MakerId varchar(50) = null,
@AddedDatetime datetime = null,
@IsAuth bit = null,
@oRetVal int = Null Output
/*</Param>*/
As

Set NoCount On
/*<Comment></Comment>*/
	Update	dbo.COMP
	Set
	   	LVL_CODE = @LVL_CODE
	   	,GRPHD = @GRPHD
	   	,MIN_SAL = @MIN_SAL
	   	,MAX_SAL = @MAX_SAL
	   	,MID_SAL = @MID_SAL
	   	,BDGT_PRICE = @BDGT_PRICE
	   	,ANL_DEPC = @ANL_DEPC
	   	,PTRL_LTR = @PTRL_LTR
	   	,PTRL_RATE = @PTRL_RATE
	   	,INS_RATE = @INS_RATE
	   	,MNTNC_AMT = @MNTNC_AMT
	   	,DRVR_COST = @DRVR_COST
	   	,GRPHD_ALL = @GRPHD_ALL
	   	,BO_YEAR = @BO_YEAR
		,COMP_MAKER_ID = @MakerId
		,COMP_ADDED_DATETIME = @AddedDatetime
		,COMP_IS_AUTH = @IsAuth
	Where 	 ID = @ID	
	
	Set @oRetVal = -10 --Record updated successfully
	Return
GO
/****** Object:  StoredProcedure [dbo].[CHRIS_SP_ConCod_CONTRACTER_ADD]    Script Date: 01/02/2024 2:26:35 pm ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* -----------------------------------------------------------------------------------------------------------
 * SP Name		: CHRIS_SP_ConCod_CONTRACTERADD
 * ALTERd By	: Umair Mufti
 * ALTERd On	: 28/01/2011
 * Purpose		: Adds record(s) in the Table(CONTRACTER) according to the parameter(s).
 * Module		: CHRIS
 * Called By	: iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.CONTRACTER.InsertCONTRACTER
 * Comments		: None
 * -----------------------------------------------------------------------------------------------------------
///-------------------------------------Modification History--------------------------------------------------
	[Date]		[Author]			[Purpose]

///----------------------------------------------------------------------------------------------------------- */

ALTER PROCEDURE [dbo].[CHRIS_SP_ConCod_CONTRACTER_ADD]
/*<Params>*/
@SP_CONTRA_CODE varchar(3), --<descr></descr>
@SP_CONTRA_NAME varchar(20), --<descr></descr>
@SP_CONTRA_COMP_NAME varchar(30), --<descr></descr>
@SP_CONTRA_ADD1 varchar(20), --<descr></descr>
@SP_CONTRA_ADD2 varchar(20), --<descr></descr>
@SP_CONTRA_ADD3 varchar(20), --<descr></descr>
@SP_CONTRA_PHONE1 varchar(9), --<descr></descr>
@SP_CONTRA_PHONE2 varchar(9), --<descr></descr>
@SP_CONTRA_AGRE_DATE datetime, --<descr></descr>
@SP_CONTRA_EXP_DATE datetime, --<descr></descr>
@SP_CONTRA_NID numeric(13,0), --<descr></descr>
@ID INT  OUTPUT,  --<descr></descr>
@MakerId varchar(50) = null,
@AddedDatetime datetime = null,
@IsAuth bit = null,
@oRetVal int = Null Output --<descr></descr>
/*</Params>*/
As

Set NoCount On
/*<Comments></Comments>*/
/*<LocalVariables>*/
/*</LocalVariables>*/
 
	Insert Into dbo.CONTRACTER
	(
		SP_CONTRA_CODE
		,SP_CONTRA_NAME
		,SP_CONTRA_COMP_NAME
		,SP_CONTRA_ADD1
		,SP_CONTRA_ADD2
		,SP_CONTRA_ADD3
		,SP_CONTRA_PHONE1
		,SP_CONTRA_PHONE2
		,SP_CONTRA_AGRE_DATE
		,SP_CONTRA_EXP_DATE
		,SP_CONTRA_NID
		,CONT_MAKER_ID
		,CONT_ADDED_DATETIME
		,CONT_IS_AUTH
	)
	Values
	(
		@SP_CONTRA_CODE
		,@SP_CONTRA_NAME
		,@SP_CONTRA_COMP_NAME
		,@SP_CONTRA_ADD1
		,@SP_CONTRA_ADD2
		,@SP_CONTRA_ADD3
		,@SP_CONTRA_PHONE1
		,@SP_CONTRA_PHONE2
		,@SP_CONTRA_AGRE_DATE
		,@SP_CONTRA_EXP_DATE
		,@SP_CONTRA_NID
		,@MakerId
		,@AddedDatetime
		,@IsAuth
	)
	
	If @@RowCount > 0
	BEGIN
		Set @oRetVal = SCOPE_IDENTITY() --Record has been inserted successfully, return generated ID
		Set @ID = @oRetVal
	END
	Else
		Set @oRetVal = -40 --No record inserted
	
	Return
GO
/****** Object:  StoredProcedure [dbo].[CHRIS_SP_ConCod_CONTRACTER_GETALL]    Script Date: 01/02/2024 2:26:35 pm ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* -----------------------------------------------------------------------------------------------------------
 * SP Name		: CHRIS_SP_ConCod_CONTRACTER_GETALL
 * ALTERd By	: Umair Mufti
 * ALTERd On	: 28/01/2011
 * Purpose		: Retrieve entity(CONTRACTER) records as per filter provided.
 * Module		: CHRIS
 * Called By	: iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.CONTRACTER.GetCONTRACTERRecords
 * Comments		: None
 * -----------------------------------------------------------------------------------------------------------
///-------------------------------------Modification History--------------------------------------------------
	[Date]		[Author]			[Purpose]

///----------------------------------------------------------------------------------------------------------- */
 
ALTER PROCEDURE [dbo].[CHRIS_SP_ConCod_CONTRACTER_GETALL]
/*<Param>*/
@oRetVal int Output --<descr></descr>
/*</Param>*/
As

Set NoCount On
/*<Comment></Comment>*/
	Select ID
	   	,SP_CONTRA_CODE
	   	,SP_CONTRA_NAME
	   	,SP_CONTRA_COMP_NAME
	   	,SP_CONTRA_ADD1
	   	,SP_CONTRA_ADD2
	   	,SP_CONTRA_ADD3
	   	,SP_CONTRA_PHONE1
	   	,SP_CONTRA_PHONE2
	   	,SP_CONTRA_AGRE_DATE
	   	,SP_CONTRA_EXP_DATE
	   	,SP_CONTRA_NID
	From dbo.CONTRACTER WITH(NOLOCK) WHERE CONT_IS_AUTH = 1



	If @@RowCount = 0 
		Set @oRetVal = -40	--Record not found
	Else
		Set @oRetVal = -10 	--Success
	
	Return
GO
/****** Object:  StoredProcedure [dbo].[CHRIS_SP_ConCod_CONTRACTER_MANAGER]    Script Date: 01/02/2024 2:26:35 pm ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* -----------------------------------------------------------------------------------------------------------
 * SP Name		: CHRIS_SP_ConCod_CONTRACTERMANAGER
 * ALTERd By	: Umair Mufti
 * ALTERd On	: 28/01/2011
 * Purpose		: Manages (CONTRACTER) store procedure depending upon the action type.
 * Module		: CHRIS
 * Called By	: iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.CONTRACTERDAO
 * Comments		: None
 * -----------------------------------------------------------------------------------------------------------
///-------------------------------------Modification History--------------------------------------------------
	[Date]		[Author]			[Purpose]

///----------------------------------------------------------------------------------------------------------- */
 
ALTER PROCEDURE [dbo].[CHRIS_SP_ConCod_CONTRACTER_MANAGER]
/*<Param>*/
@SP_CONTRA_CODE varchar(3) OUTPUT, --<descr></descr>
@SP_CONTRA_NAME varchar(20), --<descr></descr>
@SP_CONTRA_COMP_NAME varchar(30), --<descr></descr>
@SP_CONTRA_ADD1 varchar(20), --<descr></descr>
@SP_CONTRA_ADD2 varchar(20), --<descr></descr>
@SP_CONTRA_ADD3 varchar(20), --<descr></descr>
@SP_CONTRA_PHONE1 varchar(9), --<descr></descr>
@SP_CONTRA_PHONE2 varchar(9), --<descr></descr>
@SP_CONTRA_AGRE_DATE datetime, --<descr></descr>
@SP_CONTRA_EXP_DATE datetime, --<descr></descr>
@SP_CONTRA_NID numeric(13,0), --<descr></descr>
@ID INT  OUTPUT,  --<descr></descr>
@MakerId varchar(50) = null,
@CheckerkID varchar(50) = null,
@AddedDatetime datetime = null,
@UpdatedDatetime datetime = null,
@IsAuth bit = null,
@ActionType varchar(50)
,@oRetVal int = Null Output
/*</Param>*/
As

Set NoCount On
/*<Comment></Comment>*/


	If @ActionType = 'Save'
	Begin
		Exec CHRIS_SP_ConCod_CONTRACTER_ADD
	   			@SP_CONTRA_CODE
	   			,@SP_CONTRA_NAME
	   			,@SP_CONTRA_COMP_NAME
	   			,@SP_CONTRA_ADD1
	   			,@SP_CONTRA_ADD2
	   			,@SP_CONTRA_ADD3
	   			,@SP_CONTRA_PHONE1
	   			,@SP_CONTRA_PHONE2
	   			,@SP_CONTRA_AGRE_DATE
	   			,@SP_CONTRA_EXP_DATE
	   			,@SP_CONTRA_NID
	   			,@ID
				,@MakerId
				,@AddedDatetime
				,@IsAuth
				,@oRetVal = @oRetVal OUTPUT
			SET @ID =  @oRetVal
	End
	Else If @ActionType = 'Update'
	Begin
		Exec CHRIS_SP_ConCod_CONTRACTER_UPDATE
	   			@SP_CONTRA_CODE
	   			,@SP_CONTRA_NAME
	   			,@SP_CONTRA_COMP_NAME
	   			,@SP_CONTRA_ADD1
	   			,@SP_CONTRA_ADD2
	   			,@SP_CONTRA_ADD3
	   			,@SP_CONTRA_PHONE1
	   			,@SP_CONTRA_PHONE2
	   			,@SP_CONTRA_AGRE_DATE
	   			,@SP_CONTRA_EXP_DATE
	   			,@SP_CONTRA_NID
	   			,@ID
				,@MakerId
				,@AddedDatetime
				,@IsAuth
	End
	Else If @ActionType = 'Delete'
	Begin
		Exec CHRIS_SP_ConCod_CONTRACTER_DELETE
				@ID,@oRetVal
				--@SP_CONTRA_CODE,@oRetVal
	End
	Else If @ActionType = 'Get'
	Begin
		Exec CHRIS_SP_ConCod_CONTRACTER_GET
				@ID,@oRetVal
	End
	Else If @ActionType = 'List'
	Begin
		Exec CHRIS_SP_ConCod_CONTRACTER_GETALL @oRetVal
	End


	Else If @ActionType = 'PROC_MOD_DEL'
	Begin
		Select 
	   	 SP_CONTRA_CODE
	   	,SP_CONTRA_NAME
	   	,SP_CONTRA_COMP_NAME
	   	,SP_CONTRA_ADD1
	   	,SP_CONTRA_ADD2
	   	,SP_CONTRA_ADD3
	   	,SP_CONTRA_PHONE1
	   	,SP_CONTRA_PHONE2
	   	,SP_CONTRA_AGRE_DATE
	   	,SP_CONTRA_EXP_DATE
	   	,SP_CONTRA_NID
	From dbo.CONTRACTER WITH(NOLOCK)
	where SP_CONTRA_CODE = @SP_CONTRA_CODE
	End



	Else If @ActionType = 'CONCODE_EXISTS'
	Begin
		SELECT   SP_CONTRA_CODE  
				,SUBSTRING(SP_CONTRA_NAME,1,20) AS SP_CONTRA_NAME 
				,SP_CONTRA_COMP_NAME
	   			,SP_CONTRA_ADD1
	   			,SP_CONTRA_ADD2
	   			,SP_CONTRA_ADD3
	   			,SP_CONTRA_PHONE1
	   			,SP_CONTRA_PHONE2
	   			,SP_CONTRA_AGRE_DATE
	   			,SP_CONTRA_EXP_DATE
	   			,SP_CONTRA_NID
				,[ID]
		FROM [CONTRACTER]
		WHERE  SP_CONTRA_CODE = @SP_CONTRA_CODE
	End


-----------------------------------LOV----------------------------------

	Else If @ActionType = 'CONTRA_CODE'
	Begin
		SELECT SUBSTRING(SP_CONTRA_CODE,1,3) AS SP_CONTRA_CODE, SUBSTRING(SP_CONTRA_NAME,1,20) AS SP_CONTRA_NAME ,
		SP_CONTRA_COMP_NAME
	   	,SP_CONTRA_ADD1
	   	,SP_CONTRA_ADD2
	   	,SP_CONTRA_ADD3
	   	,SP_CONTRA_PHONE1
	   	,SP_CONTRA_PHONE2
	   	,SP_CONTRA_AGRE_DATE
	   	,SP_CONTRA_EXP_DATE
	   	,SP_CONTRA_NID
		,[ID]
		FROM CONTRACTER 
		ORDER BY SP_CONTRA_CODE
	End

	Else If @ActionType = 'CONTRA_CODE_EXIST'
	Begin
		IF EXISTS(SELECT SP_CONTRA_CODE FROM CONTRACTER WHERE SP_CONTRA_CODE = @SP_CONTRA_CODE)
		BEGIN
			SELECT SUBSTRING(SP_CONTRA_CODE,1,3) AS SP_CONTRA_CODE, SUBSTRING(SP_CONTRA_NAME,1,20) AS SP_CONTRA_NAME,			 SP_CONTRA_COMP_NAME
	   		,SP_CONTRA_ADD1
	   		,SP_CONTRA_ADD2
	   		,SP_CONTRA_ADD3
	   		,SP_CONTRA_PHONE1
	   		,SP_CONTRA_PHONE2
	   		,SP_CONTRA_AGRE_DATE
	   		,SP_CONTRA_EXP_DATE
	   		,SP_CONTRA_NID
			,[ID]
			FROM CONTRACTER 
			WHERE SP_CONTRA_CODE = @SP_CONTRA_CODE
			ORDER BY SP_CONTRA_CODE
		END
		ELSE IF EXISTS (SELECT SP_CONTRA_CODE FROM CONTRACTER WHERE SP_CONTRA_CODE LIKE @SP_CONTRA_CODE + '%' OR							ISNULL(@SP_CONTRA_CODE, '') = '')
		BEGIN
			SELECT SUBSTRING(SP_CONTRA_CODE,1,3) AS SP_CONTRA_CODE, SUBSTRING(SP_CONTRA_NAME,1,20) AS SP_CONTRA_NAME,
			 SP_CONTRA_COMP_NAME
	   		,SP_CONTRA_ADD1
	   		,SP_CONTRA_ADD2
	   		,SP_CONTRA_ADD3
	   		,SP_CONTRA_PHONE1
	   		,SP_CONTRA_PHONE2
	   		,SP_CONTRA_AGRE_DATE
	   		,SP_CONTRA_EXP_DATE
	   		,SP_CONTRA_NID
			,[ID]
			FROM CONTRACTER 
			WHERE SP_CONTRA_CODE = @SP_CONTRA_CODE
			ORDER BY SP_CONTRA_CODE
		END
	End


	------------------------------------------------------------------------------------------------------------

	ELSE IF @ACTIONTYPE = 'AUTHORIZE_Contract_Code_Ent'      
	 BEGIN      
	  UPDATE CONTRACTER SET CONT_IS_AUTH = 1, CONT_CHECKER_ID = @CheckerkID,CONT_UPDATED_DATETIME = @UpdatedDatetime WHERE ID = @ID     
	 END 

	 ELSE IF @ACTIONTYPE = 'REJECT_Contract_Code_Ent'      
	 BEGIN      
	  DELETE FROM CONTRACTER WHERE ID = @ID     
	 END 

	ELSE IF @ACTIONTYPE = 'GET_UNAUTHORIZE_Contract_Code_Ent'      
	 BEGIN
		Select ID
	   	,SP_CONTRA_CODE
	   	,SP_CONTRA_NAME
	   	,SP_CONTRA_COMP_NAME
	   	,SP_CONTRA_ADD1
	   	,SP_CONTRA_ADD2
	   	,SP_CONTRA_ADD3
	   	,SP_CONTRA_PHONE1
	   	,SP_CONTRA_PHONE2
	   	,SP_CONTRA_AGRE_DATE
	   	,SP_CONTRA_EXP_DATE
	   	,SP_CONTRA_NID
	From dbo.CONTRACTER WITH(NOLOCK) WHERE CONT_IS_AUTH = 0 AND CONT_MAKER_ID != @MakerId
	Return
	 END 


	Return
GO
/****** Object:  StoredProcedure [dbo].[CHRIS_SP_ConCod_CONTRACTER_UPDATE]    Script Date: 01/02/2024 2:26:35 pm ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* -----------------------------------------------------------------------------------------------------------
 * SP Name		: CHRIS_SP_ConCod_CONTRACTERUPDATE
 * ALTERd By	: Umair Mufti
 * ALTERd On	: 28/01/2011
 * Purpose		: Updates record(s) in the Table(CONTRACTER) according to the parameter(s).
 * Module		: CHRIS
 * Called By	: iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.CONTRACTER.InsertCONTRACTER
 * Comments		: None
 * -----------------------------------------------------------------------------------------------------------
///-------------------------------------Modification History--------------------------------------------------
	[Date]		[Author]			[Purpose]

///----------------------------------------------------------------------------------------------------------- */
 
ALTER PROCEDURE [dbo].[CHRIS_SP_ConCod_CONTRACTER_UPDATE]
/*<Param>*/
@SP_CONTRA_CODE varchar(3), --<descr></descr>
@SP_CONTRA_NAME varchar(20), --<descr></descr>
@SP_CONTRA_COMP_NAME varchar(30), --<descr></descr>
@SP_CONTRA_ADD1 varchar(20), --<descr></descr>
@SP_CONTRA_ADD2 varchar(20), --<descr></descr>
@SP_CONTRA_ADD3 varchar(20), --<descr></descr>
@SP_CONTRA_PHONE1 varchar(9), --<descr></descr>
@SP_CONTRA_PHONE2 varchar(9), --<descr></descr>
@SP_CONTRA_AGRE_DATE datetime, --<descr></descr>
@SP_CONTRA_EXP_DATE datetime, --<descr></descr>
@SP_CONTRA_NID numeric(13,0), --<descr></descr>
@ID INT, --<descr></descr>
@MakerId varchar(50) = null,
@AddedDatetime datetime = null,
@IsAuth bit = null,
@oRetVal int = Null Output
/*</Param>*/
As

Set NoCount On
/*<Comment></Comment>*/
	Update	dbo.CONTRACTER
	Set
	   	SP_CONTRA_CODE = @SP_CONTRA_CODE
	   	,SP_CONTRA_NAME = @SP_CONTRA_NAME
	   	,SP_CONTRA_COMP_NAME = @SP_CONTRA_COMP_NAME
	   	,SP_CONTRA_ADD1 = @SP_CONTRA_ADD1
	   	,SP_CONTRA_ADD2 = @SP_CONTRA_ADD2
	   	,SP_CONTRA_ADD3 = @SP_CONTRA_ADD3
	   	,SP_CONTRA_PHONE1 = @SP_CONTRA_PHONE1
	   	,SP_CONTRA_PHONE2 = @SP_CONTRA_PHONE2
	   	,SP_CONTRA_AGRE_DATE = @SP_CONTRA_AGRE_DATE
	   	,SP_CONTRA_EXP_DATE = @SP_CONTRA_EXP_DATE
	   	,SP_CONTRA_NID = @SP_CONTRA_NID
		,CONT_MAKER_ID = @MakerId
		,CONT_ADDED_DATETIME = @AddedDatetime
		,CONT_IS_AUTH = @IsAuth
	--Where 	 ID = @ID	
	Where 	 SP_CONTRA_CODE = @SP_CONTRA_CODE
	
	Set @oRetVal = -10 --Record updated successfully
	Return
GO
/****** Object:  StoredProcedure [dbo].[CHRIS_SP_DeducEnt_DEDUCTION_ADD]    Script Date: 01/02/2024 2:26:35 pm ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* -----------------------------------------------------------------------------------------------------------
 * SP Name		: CHRIS_SP_DeducEnt_DEDUCTIONADD
 * ALTERd By	: Umair Mufti
 * ALTERd On	: 02/02/2011
 * Purpose		: Adds record(s) in the Table(DEDUCTION) according to the parameter(s).
 * Module		: CHRIS
 * Called By	: iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.DEDUCTION.InsertDEDUCTION
 * Comments		: None
 * -----------------------------------------------------------------------------------------------------------
///-------------------------------------Modification History--------------------------------------------------
	[Date]		[Author]			[Purpose]

///----------------------------------------------------------------------------------------------------------- */

ALTER PROCEDURE [dbo].[CHRIS_SP_DeducEnt_DEDUCTION_ADD]
/*<Params>*/
@SP_DED_CODE varchar(3), --<descr></descr>
@SP_BRANCH_D varchar(3), --<descr></descr>
@SP_DED_DESC varchar(30), --<descr></descr>
@SP_CATEGORY_D varchar(1), --<descr></descr>
@SP_DESG_D varchar(3), --<descr></descr>
@SP_LEVEL_D varchar(3), --<descr></descr>
@SP_VALID_FROM_D datetime, --<descr></descr>
@SP_VALID_TO_D datetime, --<descr></descr>
@SP_DED_AMOUNT numeric(6,0), --<descr></descr>
@SP_DED_PER numeric(2,0), --<descr></descr>
@SP_ALL_IND varchar(1), --<descr></descr>
@SP_ACOUNT_NO_D varchar(11), --<descr></descr>
@ID INT  OUTPUT,  --<descr></descr>
@MakerId varchar(50) = null,
@AddedDatetime datetime = null,
@IsAuth bit = null,
@oRetVal int = Null Output --<descr></descr>
/*</Params>*/
As

Set NoCount On
/*<Comments></Comments>*/
/*<LocalVariables>*/
/*</LocalVariables>*/
 
	Insert Into dbo.DEDUCTION
	(
		SP_DED_CODE
		,SP_BRANCH_D
		,SP_DED_DESC
		,SP_CATEGORY_D
		,SP_DESG_D
		,SP_LEVEL_D
		,SP_VALID_FROM_D
		,SP_VALID_TO_D
		,SP_DED_AMOUNT
		,SP_DED_PER
		,SP_ALL_IND
		,SP_ACOUNT_NO_D
		,DEDUCTION_MAKER_ID
		,DEDUCTION_ADDED_DATETIME
		,DEDUCTION_IS_AUTH
	)
	Values
	(
		@SP_DED_CODE
		,@SP_BRANCH_D
		,@SP_DED_DESC
		,@SP_CATEGORY_D
		,@SP_DESG_D
		,@SP_LEVEL_D
		,@SP_VALID_FROM_D
		,@SP_VALID_TO_D
		,@SP_DED_AMOUNT
		,@SP_DED_PER
		,@SP_ALL_IND
		,@SP_ACOUNT_NO_D
		,@MakerId
		,@AddedDatetime
		,@IsAuth
	)
	
	If @@RowCount > 0
	BEGIN
		Set @oRetVal = SCOPE_IDENTITY() --Record has been inserted successfully, return generated ID
		Set @ID = @oRetVal
	END
	Else
		Set @oRetVal = -40 --No record inserted
	
	Return
GO
/****** Object:  StoredProcedure [dbo].[CHRIS_SP_DeducEnt_DEDUCTION_GETALL]    Script Date: 01/02/2024 2:26:35 pm ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* -----------------------------------------------------------------------------------------------------------
 * SP Name		: CHRIS_SP_DeducEnt_DEDUCTION_GETALL
 * ALTERd By	: Umair Mufti
 * ALTERd On	: 02/02/2011
 * Purpose		: Retrieve entity(DEDUCTION) records as per filter provided.
 * Module		: CHRIS
 * Called By	: iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.DEDUCTION.GetDEDUCTIONRecords
 * Comments		: None
 * -----------------------------------------------------------------------------------------------------------
///-------------------------------------Modification History--------------------------------------------------
	[Date]		[Author]			[Purpose]

///----------------------------------------------------------------------------------------------------------- */
 
ALTER PROCEDURE [dbo].[CHRIS_SP_DeducEnt_DEDUCTION_GETALL]
/*<Param>*/
@oRetVal int Output --<descr></descr>
/*</Param>*/
As

Set NoCount On
/*<Comment></Comment>*/
	Select ID
	   	,SP_DED_CODE
	   	,SP_BRANCH_D
	   	,SP_DED_DESC
	   	,SP_CATEGORY_D
	   	,SP_DESG_D
	   	,SP_LEVEL_D
	   	,SP_VALID_FROM_D
	   	,SP_VALID_TO_D
	   	,SP_DED_AMOUNT
	   	,SP_DED_PER
	   	,SP_ALL_IND
	   	,SP_ACOUNT_NO_D
	From dbo.DEDUCTION WITH(NOLOCK)  WHERE DEDUCTION_IS_AUTH = 1



	If @@RowCount = 0 
		Set @oRetVal = -40	--Record not found
	Else
		Set @oRetVal = -10 	--Success
	
	Return
GO
/****** Object:  StoredProcedure [dbo].[CHRIS_SP_DeducEnt_DEDUCTION_MANAGER]    Script Date: 01/02/2024 2:26:35 pm ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* -----------------------------------------------------------------------------------------------------------
 * SP NAME		: CHRIS_SP_DEDUCENT_DEDUCTIONMANAGER
 * ALTERD BY	: UMAIR MUFTI
 * ALTERD ON	: 02/02/2011
 * PURPOSE		: MANAGES (DEDUCTION) STORE PROCEDURE DEPENDING UPON THE ACTION TYPE.
 * MODULE		: CHRIS
 * CALLED BY	: ICORE.CHRIS.BUSINESSOBJECTS.ENTITIES.DEDUCTIONDAO
 * COMMENTS		: NONE
 * -----------------------------------------------------------------------------------------------------------
///-------------------------------------MODIFICATION HISTORY--------------------------------------------------
	[DATE]		[AUTHOR]			[PURPOSE]

///----------------------------------------------------------------------------------------------------------- */
 
ALTER PROCEDURE [dbo].[CHRIS_SP_DeducEnt_DEDUCTION_MANAGER]
/*<PARAM>*/
@SP_DED_CODE VARCHAR(3), --<DESCR></DESCR>
@SP_BRANCH_D VARCHAR(3), --<DESCR></DESCR>
@SP_DED_DESC VARCHAR(30), --<DESCR></DESCR>
@SP_CATEGORY_D VARCHAR(1), --<DESCR></DESCR>
@SP_DESG_D VARCHAR(3), --<DESCR></DESCR>
@SP_LEVEL_D VARCHAR(3), --<DESCR></DESCR>
@SP_VALID_FROM_D DATETIME, --<DESCR></DESCR>
@SP_VALID_TO_D DATETIME, --<DESCR></DESCR>
@SP_DED_AMOUNT NUMERIC(6,0), --<DESCR></DESCR>
@SP_DED_PER NUMERIC(2,0), --<DESCR></DESCR>
@SP_ALL_IND VARCHAR(1), --<DESCR></DESCR>
@SP_ACOUNT_NO_D VARCHAR(11), --<DESCR></DESCR>
@ID INT  OUTPUT,  --<DESCR></DESCR>
@ACTIONTYPE VARCHAR(50),
@SEARCHFILTER VARCHAR(MAX),
@MakerId varchar(50) = null,
@CheckerkID varchar(50) = null,
@AddedDatetime datetime = null,
@UpdatedDatetime datetime = null,
@IsAuth bit = null
,@ORETVAL INT = NULL OUTPUT
/*</PARAM>*/
AS

SET NOCOUNT ON
/*<COMMENT></COMMENT>*/


	IF @ACTIONTYPE = 'SAVE'
	BEGIN
		EXEC CHRIS_SP_DEDUCENT_DEDUCTION_ADD
	   			@SP_DED_CODE
	   			,@SP_BRANCH_D
	   			,@SP_DED_DESC
	   			,@SP_CATEGORY_D
	   			,@SP_DESG_D
	   			,@SP_LEVEL_D
	   			,@SP_VALID_FROM_D
	   			,@SP_VALID_TO_D
	   			,@SP_DED_AMOUNT
	   			,@SP_DED_PER
	   			,@SP_ALL_IND
	   			,@SP_ACOUNT_NO_D	   			
				,@MakerId
				,@AddedDatetime
				,@IsAuth
				,@ORETVAL = @ORETVAL OUTPUT
			--SET @ID =  @ORETVAL
	END
	ELSE IF @ACTIONTYPE = 'UPDATE'
	BEGIN
		EXEC CHRIS_SP_DEDUCENT_DEDUCTION_UPDATE
	   			@SP_DED_CODE
	   			,@SP_BRANCH_D
	   			,@SP_DED_DESC
	   			,@SP_CATEGORY_D
	   			,@SP_DESG_D
	   			,@SP_LEVEL_D
	   			,@SP_VALID_FROM_D
	   			,@SP_VALID_TO_D
	   			,@SP_DED_AMOUNT
	   			,@SP_DED_PER
	   			,@SP_ALL_IND
	   			,@SP_ACOUNT_NO_D
	   			,@ID
				,@MakerId
				,@AddedDatetime
				,@IsAuth
	END
	ELSE IF @ACTIONTYPE = 'DELETE'
	BEGIN
		EXEC CHRIS_SP_DEDUCENT_DEDUCTION_DELETE
				@ID,@ORETVAL
	END
	ELSE IF @ACTIONTYPE = 'GET'
	BEGIN
		EXEC CHRIS_SP_DEDUCENT_DEDUCTION_GET
				@ID,@ORETVAL
	END
	ELSE IF @ACTIONTYPE = 'LIST'
	BEGIN
		EXEC CHRIS_SP_DEDUCENT_DEDUCTION_GETALL @ORETVAL
	END


	ELSE IF @ACTIONTYPE = 'DESG'
	BEGIN
		SELECT DISTINCT SP_CATEGORY, SP_LEVEL, SP_DESG 
		FROM   DESIG 
		WHERE  SP_CURRENT	= 'C'
		AND	   SP_DESG		= @SP_DESG_D
	END


	ELSE IF @ACTIONTYPE = 'Valid_From_D'
	BEGIN
	     SELECT *  FROM PAYROLL
		 --WHERE DATEDIFF(day,PA_DATE , @SP_VALID_FROM_D )<= 0
		WHERE @SP_VALID_FROM_D <= PA_DATE 
	END

	ELSE IF @ACTIONTYPE = 'Valid_To_D'
	BEGIN
	     SELECT 'X'  FROM PAYROLL
		--WHERE DATEDIFF(day,PA_DATE , @SP_VALID_TO_D )<= 0
		WHERE @SP_VALID_TO_D <= PA_DATE 
	END

	ELSE IF @ACTIONTYPE = 'Account_No_D'
	BEGIN
	     SELECT SP_ACC_NO,SP_ACC_DESC
		 FROM	ACCOUNT
		 WHERE	SP_ACC_NO = @SP_ACOUNT_NO_D
	END


	Else If @ActionType = 'CATEGORY_VALIDATING'
	Begin
		SELECT SP_DESG, SP_LEVEL, SP_CATEGORY
		FROM DESIG
		WHERE SP_CATEGORY = @SP_CATEGORY_D	AND
			  SP_DESG     = @SP_DESG_D		AND
			  SP_LEVEL    = @SP_LEVEL_D		AND 
			  SP_CURRENT  = 'C'
	End

	Else If @ActionType = 'CATEGORY_FROM_FILL'
	Begin
		SELECT SP_DED_DESC, SP_VALID_FROM_D, SP_VALID_TO_D, SP_ALL_IND, SP_DED_AMOUNT, SP_ACOUNT_NO_D, SP_DED_PER
			 
		FROM deduction
		WHERE SP_CATEGORY_D = @SP_CATEGORY_D	AND
			SP_DESG_D		= @SP_DESG_D		AND
			SP_LEVEL_D		= @SP_LEVEL_D		AND
			SP_DED_CODE		= @SP_DED_CODE		AND
			SP_BRANCH_D		= @SP_BRANCH_D
	End

	ELSE IF @ACTIONTYPE = 'DESIGNATION_FROM_FILL'
	Begin
		SELECT SP_DED_DESC, SP_VALID_FROM_D, SP_VALID_TO_D, SP_ALL_IND, SP_DED_AMOUNT, SP_ACOUNT_NO_D
		FROM deduction
		WHERE SP_DED_CODE		= @SP_DED_CODE		AND
			  SP_BRANCH_D		= @SP_BRANCH_D
	End




	Else If @ActionType = 'CATEGORY_VALIDATED'
	Begin
--	
--if (@SP_BRANCH_D is null)
--	set @SP_BRANCH_D	= 'A'
--if (@SP_BRANCH_D is null)
--	set @SP_DESG_D		= 'A'
--if (@SP_BRANCH_D is null)
--	set @SP_LEVEL_D		= 'A'
--if (@SP_BRANCH_D is null)
--	set @SP_CATEGORY_D	= 'A'
--
--		SELECT  SP_DED_CODE, SP_BRANCH_D, SP_DED_DESC, SP_CATEGORY_D, SP_DESG_D, SP_LEVEL_D, SP_VALID_FROM_D,					SP_VALID_TO_D, SP_DED_AMOUNT, SP_DED_PER, SP_ACOUNT_NO_D, SP_ALL_IND
--		FROM	DEDUCTION
--		WHERE	SP_DED_CODE		=  @SP_DED_CODE		AND
--				(SP_BRANCH_D)	= (@SP_BRANCH_D)	AND
--				(SP_DESG_D)		= (@SP_DESG_D)		AND			  
--				(SP_LEVEL_D)	= (@SP_LEVEL_D)		AND	
--				(SP_CATEGORY_D)	= (@SP_CATEGORY_D)	
--			  
			  
	SELECT
			 SP_DED_CODE,
			 SP_BRANCH_D,
			 SP_DED_DESC,
			 SP_CATEGORY_D,
			 SP_DESG_D,
			 SP_LEVEL_D,
			 SP_VALID_FROM_D,
			 SP_VALID_TO_D,
			 SP_DED_AMOUNT,
			 SP_DED_PER,
			 SP_ACOUNT_NO_D,
			 SP_ALL_IND,
			 ID
	FROM  DEDUCTION 
	WHERE	SP_DED_CODE					= @SP_DED_CODE
	 AND	ISNULL(SP_BRANCH_D, 'A')	= ISNULL(@SP_BRANCH_D, 'A')
	 AND	ISNULL(SP_DESG_D, 'A')		= ISNULL(@SP_DESG_D, 'A')
	 AND	ISNULL(SP_LEVEL_D, 'A')		= ISNULL(@SP_LEVEL_D, 'A')
	 AND	ISNULL(SP_CATEGORY_D, 'A')  = ISNULL(@SP_CATEGORY_D, 'A')
	







	End


	ELSE IF @ACTIONTYPE = 'DEL_DED_DETAIL'
		BEGIN
			DELETE FROM DEDUCTION_DETAILS
			WHERE SP_DEDUC_CODE = @SP_DED_CODE
		End






----------------------------------------------LOV---------------------------------------------

	ELSE IF @ACTIONTYPE = 'DUC_LOV'
	BEGIN
		--SELECT SUBSTRING(SP_DED_CODE,1,3) AS SP_DED_CODE,SUBSTRING(SP_BRANCH_D,1,3) AS SP_BRANCH_D, 
		SELECT SP_DED_CODE,SUBSTRING(SP_BRANCH_D,1,3) AS SP_BRANCH_D, 
			   SUBSTRING(SP_DESG_D,1,3) AS SP_DESG_D , SUBSTRING(SP_LEVEL_D,1,3) AS SP_LEVEL_D,	
			   SUBSTRING(SP_CATEGORY_D,1,1) AS	SP_CATEGORY, SP_DED_DESC  AS [SP_DESCRIPTION], ID
		FROM DEDUCTION



	select SP_DED_CODE
	   			,SP_BRANCH_D
	   			,SP_DED_DESC
	   			,SP_CATEGORY_D
	   			,SP_DESG_D
	   			,SP_LEVEL_D
	   			,SP_VALID_FROM_D
	   			,SP_VALID_TO_D
	   			,SP_DED_AMOUNT
	   			,SP_DED_PER
	   			,SP_ALL_IND
	   			,SP_ACOUNT_NO_D
	   			,ID
from deduction


	END

	ELSE IF @ACTIONTYPE = 'DUC_LOV_EXISTS'
	BEGIN
		IF EXISTS(SELECT SP_DED_CODE FROM DEDUCTION WHERE SP_DED_CODE = @SP_DED_CODE )
		BEGIN
			SELECT SUBSTRING(SP_DED_CODE,1,3) AS SP_DED_CODE
			FROM DEDUCTION
			WHERE SP_DED_CODE  = @SP_DED_CODE 
		END 
		ELSE IF EXISTS(SELECT SP_DED_CODE FROM DEDUCTION WHERE SP_DED_CODE LIKE @SP_DED_CODE + '%' OR ISNULL(SP_DED_CODE ,'') = '')
		BEGIN
			SELECT SUBSTRING(SP_DED_CODE,1,3) AS SP_DED_CODE
			FROM DEDUCTION
			WHERE SP_DED_CODE  = @SP_DED_CODE 
		END
	END



	ELSE IF @ACTIONTYPE = 'DUC_DETAIL_LOV1'
	BEGIN
		SELECT DISTINCT SUBSTRING(SP_DED_CODE,1,3) AS SP_DED_CODE,SP_DED_DESC , SP_VALID_FROM_D,	SP_VALID_TO_D,							SP_DED_AMOUNT, SP_ACOUNT_NO_D AS [SP_ACCOUNT_NO_D]
		FROM DEDUCTION
		WHERE SP_ALL_IND = 'I' 
		ORDER BY SUBSTRING(SP_DED_CODE,1,3)
	END

	ELSE IF @ACTIONTYPE = 'BRANCH_LOV'
	BEGIN
		SELECT	BRN_CODE as SP_BRANCH_D ,BRN_NAME 
		FROM	BRANCH
	END

	ELSE IF @ACTIONTYPE = 'BRANCH_LOV_EXISTS'
	BEGIN
		IF EXISTS(SELECT BRN_CODE FROM BRANCH WHERE BRN_CODE = @SP_BRANCH_D )
		BEGIN		
			SELECT	BRN_CODE as SP_BRANCH_D ,BRN_NAME 
			FROM	BRANCH
			WHERE	BRN_CODE = @SP_BRANCH_D
		END
		IF EXISTS(SELECT BRN_CODE FROM BRANCH WHERE BRN_CODE LIKE @SP_BRANCH_D OR ISNULL(@SP_BRANCH_D,'')= '' )
		BEGIN		
			SELECT	BRN_CODE as SP_BRANCH_D,BRN_NAME 
			FROM	BRANCH
			WHERE	BRN_CODE = @SP_BRANCH_D
		END

	END



	ELSE IF @ACTIONTYPE = 'DESG_LOV'
	BEGIN
		SELECT DISTINCT SP_CATEGORY AS SP_CATEGORY_D , SP_LEVEL AS SP_LEVEL_D, SP_DESG AS SP_DESG_D
		FROM DESIG 
		WHERE SP_CURRENT = 'C'
	END

	ELSE IF @ACTIONTYPE = 'DESG_LOV_EXISTS'
	BEGIN
		IF EXISTS (SELECT SP_DESG FROM DESIG WHERE SP_DESG LIKE @SP_DESG_D OR ISNULL(@SP_DESG_D ,'') = '')
		BEGIN
			SELECT DISTINCT SP_CATEGORY , SP_LEVEL, SP_DESG AS SP_DESG_D
			FROM DESIG 
			WHERE SP_CURRENT = 'C'
			AND	  SP_DESG = @SP_DESG_D
		END
		ELSE IF EXISTS (SELECT SP_DESG FROM DESIG WHERE SP_DESG = @SP_DESG_D)
		BEGIN
			SELECT DISTINCT SP_CATEGORY , SP_LEVEL, SP_DESG AS SP_DESG_D
			FROM DESIG 
			WHERE SP_CURRENT = 'C'
			AND	  SP_DESG = @SP_DESG_D
		END 
	END


	ELSE IF @ACTIONTYPE = 'LEVEL_LOV'
	BEGIN
		SELECT DISTINCT SP_CATEGORY AS SP_CATEGORY_D, SP_LEVEL AS SP_LEVEL_D, SP_DESG 
		FROM DESIG 
		WHERE SP_CURRENT = 'C' 
		AND   SP_DESG	 = @SEARCHFILTER
	END

	ELSE IF @ACTIONTYPE = 'LEVEL_LOV_EXISTS'
	BEGIN
		IF EXISTS(SELECT SP_LEVEL FROM DESIG WHERE SP_LEVEL LIKE @SP_LEVEL_D + '%' OR ISNULL(@SP_LEVEL_D,'')= '')
		BEGIN
			SELECT DISTINCT SP_CATEGORY , SP_LEVEL AS SP_LEVEL_D, SP_DESG 
			FROM DESIG 
			WHERE SP_CURRENT = 'C' 
			AND   SP_DESG	 = @SEARCHFILTER
			AND	  SP_LEVEL   = @SP_LEVEL_D
		END
		ELSE IF EXISTS(SELECT SP_LEVEL FROM DESIG WHERE SP_LEVEL = @SP_LEVEL_D)
		BEGIN
			SELECT DISTINCT SP_CATEGORY, SP_LEVEL AS SP_LEVEL_D, SP_DESG 
			FROM  DESIG 
			WHERE SP_CURRENT = 'C' 
			AND   SP_DESG	 = @SEARCHFILTER
			AND	  SP_LEVEL   = @SP_LEVEL_D
		END
		
	END


	ELSE IF @ACTIONTYPE = 'CATGORY_LOV'
	BEGIN
		SELECT DISTINCT SP_CATEGORY AS SP_CATEGORY_D, SP_LEVEL, SP_DESG 
		FROM DESIG 
		WHERE SP_CURRENT = 'C'
	END

	ELSE IF @ACTIONTYPE = 'CATGORY_LOV_EXISTS'
	BEGIN
		IF EXISTS(SELECT SP_CATEGORY  AS SP_CATEGORY_D FROM DESIG WHERE SP_CATEGORY LIKE @SP_CATEGORY_D OR ISNULL(@SP_CATEGORY_D,'')='')
		BEGIN
			SELECT DISTINCT SP_CATEGORY AS SP_CATEGORY_D, SP_LEVEL, SP_DESG 
			FROM DESIG 
			WHERE SP_CURRENT = 'C'
			AND	  SP_CATEGORY = @SP_CATEGORY_D
		END
		ELSE IF EXISTS (SELECT SP_CATEGORY  AS SP_CATEGORY_D FROM DESIG WHERE SP_CATEGORY = @SP_CATEGORY_D)
		BEGIN
			SELECT DISTINCT SP_CATEGORY AS SP_CATEGORY_D, SP_LEVEL, SP_DESG 
			FROM DESIG 
			WHERE SP_CURRENT = 'C'
			AND	  SP_CATEGORY = @SP_CATEGORY_D
		END 
	END


	ELSE IF @ACTIONTYPE = 'ACCOUNT_LOV'
	BEGIN
		SELECT SP_ACC_NO AS SP_ACOUNT_NO_D, SP_ACC_DESC as W_ACC_DESC FROM ACCOUNT
	END

	ELSE IF @ACTIONTYPE = 'ACCOUNT_LOV_EXIST'
	BEGIN
		IF EXISTS (SELECT SP_ACC_NO AS SP_ACOUNT_NO_D FROM ACCOUNT WHERE SP_ACC_NO = @SP_ACOUNT_NO_D)
		BEGIN
			SELECT SP_ACC_NO  AS SP_ACOUNT_NO_D, SP_ACC_DESC as W_ACC_DESC FROM ACCOUNT
			WHERE SP_ACC_NO = @SP_ACOUNT_NO_D
		END
		ELSE IF EXISTS(SELECT SP_ACC_NO  as SP_ACCOUNT_NO_D FROM ACCOUNT WHERE SP_ACC_NO LIKE @SP_ACOUNT_NO_D + '%' OR ISNULL(
@SP_ACOUNT_NO_D, '') = '')
		BEGIN
			SELECT SP_ACC_NO AS SP_ACOUNT_NO_D, SP_ACC_DESC as W_ACC_DESC FROM ACCOUNT
			WHERE SP_ACC_NO = @SP_ACOUNT_NO_D
		END
	END
-----------------------------------------------------------------------------------------------------------------------------------------------

ELSE IF @ACTIONTYPE = 'AUTHORIZE_Deduct_Ent'      
	 BEGIN      
	  UPDATE DEDUCTION SET DEDUCTION_IS_AUTH = 1, DEDUCTION_CHECKER_ID = @CheckerkID,DEDUCTION_UPDATED_DATETIME = @UpdatedDatetime WHERE ID = @ID     
	 END 

	 ELSE IF @ACTIONTYPE = 'REJECT_Deduct_Ent'      
	 BEGIN      
	  DELETE FROM MARGINAL_TAX WHERE ID = @ID     
	 END 

	ELSE IF @ACTIONTYPE = 'GET_UNAUTHORIZE_Deduct_Ent'      
	 BEGIN
		Select ID
	   	,SP_DED_CODE
	   	,SP_BRANCH_D
	   	,SP_DED_DESC
	   	,SP_CATEGORY_D
	   	,SP_DESG_D
	   	,SP_LEVEL_D
	   	,SP_VALID_FROM_D
	   	,SP_VALID_TO_D
	   	,SP_DED_AMOUNT
	   	,SP_DED_PER
	   	,SP_ALL_IND
	   	,SP_ACOUNT_NO_D
	From dbo.DEDUCTION WITH(NOLOCK)   WHERE DEDUCTION_IS_AUTH = 0 AND DEDUCTION_MAKER_ID != @MakerId
	Return
	 END 





	RETURN
GO
/****** Object:  StoredProcedure [dbo].[CHRIS_SP_DeducEnt_DEDUCTION_UPDATE]    Script Date: 01/02/2024 2:26:35 pm ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* -----------------------------------------------------------------------------------------------------------
 * SP Name		: CHRIS_SP_DeducEnt_DEDUCTIONUPDATE
 * ALTERd By	: Umair Mufti
 * ALTERd On	: 02/02/2011
 * Purpose		: Updates record(s) in the Table(DEDUCTION) according to the parameter(s).
 * Module		: CHRIS
 * Called By	: iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.DEDUCTION.InsertDEDUCTION
 * Comments		: None
 * -----------------------------------------------------------------------------------------------------------
///-------------------------------------Modification History--------------------------------------------------
	[Date]		[Author]			[Purpose]

///----------------------------------------------------------------------------------------------------------- */
 
ALTER PROCEDURE [dbo].[CHRIS_SP_DeducEnt_DEDUCTION_UPDATE]
/*<Param>*/
@SP_DED_CODE varchar(3), --<descr></descr>
@SP_BRANCH_D varchar(3), --<descr></descr>
@SP_DED_DESC varchar(30), --<descr></descr>
@SP_CATEGORY_D varchar(1), --<descr></descr>
@SP_DESG_D varchar(3), --<descr></descr>
@SP_LEVEL_D varchar(3), --<descr></descr>
@SP_VALID_FROM_D datetime, --<descr></descr>
@SP_VALID_TO_D datetime, --<descr></descr>
@SP_DED_AMOUNT numeric(6,0), --<descr></descr>
@SP_DED_PER numeric(2,0), --<descr></descr>
@SP_ALL_IND varchar(1), --<descr></descr>
@SP_ACOUNT_NO_D varchar(11), --<descr></descr>
@ID INT, --<descr></descr>
@MakerId varchar(50) = null,
@AddedDatetime datetime = null,
@IsAuth bit = null,
@oRetVal int = Null Output
/*</Param>*/
As

Set NoCount On
/*<Comment></Comment>*/
	Update	dbo.DEDUCTION
	Set
	   	SP_DED_CODE = @SP_DED_CODE
	   	,SP_BRANCH_D = @SP_BRANCH_D
	   	,SP_DED_DESC = @SP_DED_DESC
	   	,SP_CATEGORY_D = @SP_CATEGORY_D
	   	,SP_DESG_D = @SP_DESG_D
	   	,SP_LEVEL_D = @SP_LEVEL_D
	   	,SP_VALID_FROM_D = @SP_VALID_FROM_D
	   	,SP_VALID_TO_D = @SP_VALID_TO_D
	   	,SP_DED_AMOUNT = @SP_DED_AMOUNT
	   	,SP_DED_PER = @SP_DED_PER
	   	,SP_ALL_IND = @SP_ALL_IND
	   	,SP_ACOUNT_NO_D = @SP_ACOUNT_NO_D
		,DEDUCTION_MAKER_ID = @MakerId
		,DEDUCTION_ADDED_DATETIME = @AddedDatetime
		,DEDUCTION_IS_AUTH = @IsAuth
	Where 	 ID = @ID	
	
	Set @oRetVal = -10 --Record updated successfully
	Return
GO
/****** Object:  StoredProcedure [dbo].[CHRIS_SP_DEPT_ADD]    Script Date: 01/02/2024 2:26:35 pm ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* -----------------------------------------------------------------------------------------------------------
 * SP Name		: CHRIS_SP_DEPTADD
 * ALTERd By	: Faizan Ashraf
 * ALTERd On	: 31/01/2011
 * Purpose		: Adds record(s) in the Table(DEPT) according to the parameter(s).
 * Module		: CHRIS
 * Called By	: iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.DEPT.InsertDEPT
 * Comments		: None
 * -----------------------------------------------------------------------------------------------------------
///-------------------------------------Modification History--------------------------------------------------
	[Date]		[Author]			[Purpose]

///----------------------------------------------------------------------------------------------------------- */

ALTER PROCEDURE [dbo].[CHRIS_SP_DEPT_ADD]
/*<Params>*/
@SP_SEGMENT_D varchar(3), --<descr></descr>
@SP_GROUP_D varchar(5), --<descr></descr>
@SP_DEPT varchar(5), --<descr></descr>
@SP_DESC_D1 varchar(20), --<descr></descr>
@SP_DESC_D2 varchar(20), --<descr></descr>
@SP_DEPT_HEAD varchar(20), --<descr></descr>
@SP_DATE_FROM_D datetime, --<descr></descr>
@SP_DATE_TO_D datetime, --<descr></descr>
@RC varchar(6), --<descr></descr>
@SP_RC_APA varchar(7), --<descr></descr>
@ID INT  OUTPUT,  --<descr></descr>
@MakerId varchar(50) = null,
@AddedDatetime datetime = null,
@IsAuth bit = null,
@oRetVal int = Null Output --<descr></descr>
/*</Params>*/
As

Set NoCount On
/*<Comments></Comments>*/
/*<LocalVariables>*/
/*</LocalVariables>*/
 
	Insert Into dbo.DEPT
	(
		SP_SEGMENT_D
		,SP_GROUP_D
		,SP_DEPT
		,SP_DESC_D1
		,SP_DESC_D2
		,SP_DEPT_HEAD
		,SP_DATE_FROM_D
		,SP_DATE_TO_D
		,RC
		,SP_RC_APA
		,DEPT_MAKER_ID
		,DEPT_ADDED_DATETIME
		,DEPT_IS_AUTH
	)
	Values
	(
		@SP_SEGMENT_D
		,@SP_GROUP_D
		,@SP_DEPT
		,@SP_DESC_D1
		,@SP_DESC_D2
		,@SP_DEPT_HEAD
		,@SP_DATE_FROM_D
		,@SP_DATE_TO_D
		,@RC
		,@SP_RC_APA
		,@MakerId
		,@AddedDatetime
		,@IsAuth
	)
	
	If @@RowCount > 0
	BEGIN
		Set @oRetVal = SCOPE_IDENTITY() --Record has been inserted successfully, return generated ID
		Set @ID = @oRetVal
	END
	Else
		Set @oRetVal = -40 --No record inserted
	
	Return
GO
/****** Object:  StoredProcedure [dbo].[CHRIS_SP_DEPT_GETALL]    Script Date: 01/02/2024 2:26:35 pm ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[CHRIS_SP_DEPT_GETALL]
/*<Param>*/
@oRetVal int Output --<descr></descr>
/*</Param>*/
As

Set NoCount On
/*<Comment></Comment>*/
	Select ID
	   	,SP_SEGMENT_D
	   	,SP_GROUP_D
	   	,SP_DEPT
		,CostDept.CostCode AS SP_COST_CENTER
	   	,SP_DESC_D1
	   	,SP_DESC_D2
	   	,SP_DEPT_HEAD
	   	,SP_DATE_FROM_D
	   	,SP_DATE_TO_D
	   	,RC
	   	,SP_RC_APA
	From dbo.DEPT  Dept
	lEFT join [dbo].[Dept_CostCode] CostDept on Dept.SP_DEPT = CostDept.DeptCode
	WHERE DEPT_IS_AUTH = 1
	If @@RowCount = 0 
		Set @oRetVal = -40	--Record not found
	Else
		Set @oRetVal = -10 	--Success
	
	Return


GO
/****** Object:  StoredProcedure [dbo].[CHRIS_SP_DEPT_MANAGER]    Script Date: 01/02/2024 2:26:35 pm ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[CHRIS_SP_DEPT_MANAGER]
/*<Param>*/
	@SP_SEGMENT_D VARCHAR(3), --<descr></descr>
	@SP_GROUP_D VARCHAR(5), --<descr></descr>
	@SP_DEPT VARCHAR(5), --<descr></descr> 
	@SP_COST_CENTER VARCHAR(15), --<descr></descr>
	@SP_DESC_D1 VARCHAR(20), --<descr></descr>
	@SP_DESC_D2 VARCHAR(20), --<descr></descr>
	@SP_DEPT_HEAD VARCHAR(20), --<descr></descr>
	@SP_DATE_FROM_D DATETIME, --<descr></descr>
	@SP_DATE_TO_D DATETIME, --<descr></descr>
	@RC VARCHAR(6), --<descr></descr>
	@SP_RC_APA VARCHAR(7), --<descr></descr>
	@ID INT OUTPUT, --<descr></descr>
	@MakerId varchar(50) = null,
@CheckerkID varchar(50) = null,
@AddedDatetime datetime = null,
@UpdatedDatetime datetime = null,
@IsAuth bit = null,
	@ActionType VARCHAR(50)
	 ,
	@oRetVal INT = NULL OUTPUT
	 /*</Param>*/
AS
	SET NOCOUNT ON
	/*<Comment></Comment>*/
	
	
	IF @ActionType = 'Save'
	BEGIN
	    EXEC CHRIS_SP_DEPT_ADD
	         @SP_SEGMENT_D,
	         @SP_GROUP_D,
	         @SP_DEPT,
			 @SP_COST_CENTER,
	         @SP_DESC_D1,
	         @SP_DESC_D2,
	         @SP_DEPT_HEAD,
	         @SP_DATE_FROM_D,
	         @SP_DATE_TO_D,
	         @RC,
	         @SP_RC_APA,
	         @ID
			 ,@MakerId
				,@AddedDatetime
				,@IsAuth,
	         @oRetVal = @oRetVal OUTPUT
	    
	    SET @ID = @oRetVal
	END
	ELSE 
	IF @ActionType = 'Update'
	BEGIN
	    EXEC CHRIS_SP_DEPT_UPDATE
	         @SP_SEGMENT_D,
	         @SP_GROUP_D,
	         @SP_DEPT,
			 @SP_COST_CENTER,
	         @SP_DESC_D1,
	         @SP_DESC_D2,
	         @SP_DEPT_HEAD,
	         @SP_DATE_FROM_D,
	         @SP_DATE_TO_D,
	         @RC,
	         @SP_RC_APA,
	         @ID
			  ,@MakerId
				,@AddedDatetime
				,@IsAuth
	END
	ELSE 
	IF @ActionType = 'Delete'
	BEGIN
	    EXEC CHRIS_SP_DEPT_DELETE
	         @ID,
	         @oRetVal
	END
	ELSE 
	IF @ActionType = 'Get'
	BEGIN
	    EXEC CHRIS_SP_DEPT_GET
	         @ID,
	         @oRetVal
	END
	ELSE 
	IF @ActionType = 'List'
	BEGIN
	    EXEC CHRIS_SP_DEPT_GETALL @oRetVal
	END
	------------------------------------------------------------------------------------
   
    IF @ActionType = 'GetByParam'
	BEGIN
	    select count(*) as DeptCount from dept where
		SP_DESC_D1 = @SP_DESC_D1 and SP_DEPT_HEAD= @SP_DEPT_HEAD
        ----and SP_DESC_D2 = @SP_DESC_D2    
	--and SP_DATE_FROM_D =@SP_DATE_FROM_D and @SP_DATE_TO_D=@SP_DATE_TO_D
	END


	--------------------------------------------------------------------------------------------------------------
	ELSE 
	IF @ActionType = 'W_dept_LOVs'
	BEGIN
	    SELECT SUBSTRING(sp_dept, 1, 5) sp_dept,
	           RTRIM(sp_desc_d1) + '' + RTRIM(sp_desc_d2)
	    FROM   dept
	END--------------------------------------------------------------------------------------------------------------
	ELSE 
	IF @ActionType = 'sp_Segment_LOVs'
	BEGIN
	    SELECT SUBSTRING(sp_segment_d, 1, 3)sp_segment_d,
	           SUBSTRING(sp_group_d, 1, 5)sp_group_d,
	           SUBSTRING(sp_dept, 1, 5)sp_dept,
	           MAX(sp_date_from_d) sp_date_from_d
	           --ID
	    FROM   dept
	    GROUP BY
	           sp_segment_d,
	           sp_group_d,
	           sp_dept
	           --ID
	    ORDER BY
	           sp_segment_d,
	           sp_group_d,
	           sp_dept
	END
	ELSE 
	IF @Actiontype = 'sp_Segment_LOVsExists'
	BEGIN
	    IF EXISTS(
	           SELECT id
	           FROM   dept
	           WHERE  sp_segment_d = @SP_SEGMENT_D
	       )
	        SELECT SUBSTRING(sp_segment_d, 1, 3)sp_segment_d
	               --	               ,SUBSTRING(sp_group_d, 1, 5)sp_group_d,
	               --	               SUBSTRING(sp_dept, 1, 5)sp_dept,
	               --	               MAX(sp_date_from_d) sp_date_from_d
	               
	               --ID
	        FROM   dept
	        WHERE  sp_segment_d = @SP_SEGMENT_D
	        GROUP BY
	               sp_segment_d,
	               sp_group_d,
	               sp_dept
	               --ID
	        ORDER BY
	               sp_segment_d,
	               sp_group_d,
	               sp_dept
	END--------------------------------------------------------------------------------------------------------------
	ELSE 
	IF @ActionType = 'SP_Group_LOVs'
	BEGIN
	    SELECT SUBSTRING(sp_group_code, 1, 5)sp_group_d,
	           SUBSTRING(sp_segment, 1, 3)sp_segment_d,
	           ID
	    FROM   group1
	END
	ELSE 
	IF @Actiontype = 'SP_Group_LOVsExists'
	BEGIN
	    IF EXISTS (
	           SELECT ID
	           FROM   group1
	           WHERE  sp_group_code = @SP_GROUP_D
--	                  AND sp_segment = @SP_SEGMENT_D
	       )
	        SELECT SUBSTRING(sp_group_code, 1, 5)sp_group_d,
	               SUBSTRING(sp_segment, 1, 3)sp_segment_d
	        FROM   group1
	        WHERE  sp_group_code = @SP_GROUP_D
--	               AND sp_segment = @SP_SEGMENT_D





	END--------------------------------------------------------------------------------------------------------------
	ELSE 
	IF @ActionType = 'SP_Dept_LOV3'
	BEGIN
	    SELECT SUBSTRING(sp_segment_d, 1, 3) sp_segment_d,
	           SUBSTRING(sp_group_d, 1, 5) sp_group_d,
	           SUBSTRING(sp_dept, 1, 5) sp_dept,
	           MAX(sp_date_from_d) sp_date_from_d,
	           ID
	    FROM   dept
	    GROUP BY
	           sp_segment_d,
	           sp_group_d,
	           sp_dept,
	           ID
	    ORDER BY
	           sp_segment_d,
	           sp_group_d,
	           sp_dept
	END
	ELSE 
	IF @Actiontype = 'SP_Dept_LOV3Exists'
	BEGIN
	    IF EXISTS (
	           SELECT ID
	           FROM   dept
	           WHERE  sp_dept = @SP_DEPT
	       )
	        SELECT SUBSTRING(sp_segment_d, 1, 3) sp_segment_d,
	               SUBSTRING(sp_group_d, 1, 5) sp_group_d,
	               SUBSTRING(sp_dept, 1, 5) sp_dept,
	               MAX(sp_date_from_d) sp_date_from_d,
	               ID
	        FROM   dept
	        WHERE  sp_dept = @SP_DEPT
	        GROUP BY
	               sp_segment_d,
	               sp_group_d,
	               sp_dept,
	               ID
	        ORDER BY
	               sp_segment_d,
	               sp_group_d,
	               sp_dept
	END--------------------------------------------------------------------------------------------------------------
	ELSE 
	IF @ActionType = 'DEPT_CMB_null'
	BEGIN
		select null pr_dept
		union all
	    SELECT DISTINCT PR_DEPT
	    FROM   dept_CONT where pr_dept is not null 
	    ORDER BY
	           PR_DEPT ASC
	END--------------------------------------------------------------------------------------------------------------
	else
	IF @ActionType = 'DEPT_CMB'
	BEGIN
	    SELECT DISTINCT PR_DEPT
	    FROM   dept_CONT
	    ORDER BY
	           PR_DEPT ASC
	END--------------------------------------------------------------------------------------------------------------
	ELSE 
	IF @ActionType = 'GROUP_CMB_null'
	BEGIN
		select null sp_group_d
		union all
	    SELECT DISTINCT SP_GROUP_D
	    FROM   DEPT
	    ORDER BY
	           SP_GROUP_D ASC
	END--------------------------------------------------------------------------------------------------------------
	else
	IF @ActionType = 'GROUP_CMB'
	BEGIN
	    SELECT DISTINCT SP_GROUP_D
	    FROM   DEPT
	    ORDER BY
	           SP_GROUP_D ASC
	END--------------------------------------------------------------------------------------------------------------
	ELSE 
	IF @ActionType = 'CheckRecordAlreadyExist'
	BEGIN
--	    SELECT @sp_segment_d = sp_segment_d,
--	           @sp_group_d = sp_group_d,
--	           @sp_dept = sp_dept,
--	           @sp_desc_d1 = sp_desc_d1,
--	           @sp_desc_d2 = sp_desc_d2,
--	           @sp_dept_head = sp_dept_head,
--	           @sp_date_from_d = sp_date_from_d,
--	           @sp_date_to_d = sp_date_to_d,
--	           @ID = ID
	           SELECT sp_segment_d,
	           sp_group_d,
	           sp_dept,
			   CostDept.CostCode AS SP_COST_CENTER,
	           sp_desc_d1,
	           sp_desc_d2,
	           sp_dept_head,
	           sp_date_from_d,
	           sp_date_to_d,
	           ID
	    FROM   dept Dept
		lEFT join [dbo].[Dept_CostCode] CostDept on Dept.SP_DEPT = CostDept.DeptCode
	    WHERE  sp_segment_d = @sp_segment_d
	           AND sp_group_d = @sp_group_d
	           AND sp_dept = @sp_dept
	           AND sp_date_from_d = (
	                   SELECT MAX(sp_date_from_d)
	                   FROM   dept
	                   WHERE  sp_dept = @sp_dept
	                          AND sp_group_d = @sp_group_d
	                          AND sp_segment_d = @sp_segment_d
	               )
	    
	    
--	    SELECT 
----	           @sp_segment_d AS sp_segment_d,
----	           @sp_group_d AS sp_group_d,
--	           @sp_dept AS sp_dept,
--	           @sp_desc_d1 AS sp_desc_d1,
--	           @sp_desc_d2 AS sp_desc_d2,
--	           @sp_dept_head AS sp_dept_head,
--	           @sp_date_from_d AS sp_date_from_d,
--	           @sp_date_to_d AS sp_date_to_d,
--	           @ID AS ID
	           
	END
	-------------------------------------------------------------------------------------------
	ELSE IF @ACTIONTYPE = 'AUTHORIZE_DEPT'      
	 BEGIN      
	  UPDATE DEPT SET DEPT_IS_AUTH = 1, DEPT_CHECKER_ID = @CheckerkID,DEPT_UPDATED_DATETIME = @UpdatedDatetime WHERE ID = @ID     
	 END 

	 ELSE IF @ACTIONTYPE = 'REJECT_DEPT'      
	 BEGIN      
	  DELETE FROM DEPT WHERE ID = @ID     
	 END 

	ELSE IF @ACTIONTYPE = 'GET_UNAUTHORIZE_DEPT'      
	 BEGIN
		Select ID
	   	,SP_SEGMENT_D
	   	,SP_GROUP_D
	   	,SP_DEPT
		,CostDept.CostCode AS SP_COST_CENTER
	   	,SP_DESC_D1
	   	,SP_DESC_D2
	   	,SP_DEPT_HEAD
	   	,SP_DATE_FROM_D
	   	,SP_DATE_TO_D
	   	,RC
	   	,SP_RC_APA
	From dbo.DEPT  Dept
	lEFT join [dbo].[Dept_CostCode] CostDept on Dept.SP_DEPT = CostDept.DeptCode
 WHERE DEPT_IS_AUTH = 0 AND DEPT_MAKER_ID != @MakerId
	
	Return
	 END 





	RETURN

GO
/****** Object:  StoredProcedure [dbo].[CHRIS_SP_DEPT_UPDATE]    Script Date: 01/02/2024 2:26:35 pm ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* -----------------------------------------------------------------------------------------------------------
 * SP Name		: CHRIS_SP_DEPTUPDATE
 * ALTERd By	: Faizan Ashraf
 * ALTERd On	: 31/01/2011
 * Purpose		: Updates record(s) in the Table(DEPT) according to the parameter(s).
 * Module		: CHRIS
 * Called By	: iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.DEPT.InsertDEPT
 * Comments		: None
 * -----------------------------------------------------------------------------------------------------------
///-------------------------------------Modification History--------------------------------------------------
	[Date]		[Author]			[Purpose]

///----------------------------------------------------------------------------------------------------------- */
 
ALTER PROCEDURE [dbo].[CHRIS_SP_DEPT_UPDATE]
/*<Param>*/
@SP_SEGMENT_D varchar(3), --<descr></descr>
@SP_GROUP_D varchar(5), --<descr></descr>
@SP_DEPT varchar(5), --<descr></descr>
@SP_DESC_D1 varchar(20), --<descr></descr>
@SP_DESC_D2 varchar(20), --<descr></descr>
@SP_DEPT_HEAD varchar(20), --<descr></descr>
@SP_DATE_FROM_D datetime, --<descr></descr>
@SP_DATE_TO_D datetime, --<descr></descr>
@RC varchar(6), --<descr></descr>
@SP_RC_APA varchar(7), --<descr></descr>
@ID INT, --<descr></descr>
@MakerId varchar(50) = null,
@CheckerkID varchar(50) = null,
@AddedDatetime datetime = null,
@UpdatedDatetime datetime = null,
@IsAuth bit = null,
@oRetVal int = Null Output
/*</Param>*/
As

Set NoCount On
/*<Comment></Comment>*/
	Update	dbo.DEPT
	Set
	   	SP_SEGMENT_D = @SP_SEGMENT_D
	   	,SP_GROUP_D = @SP_GROUP_D
	   	,SP_DEPT = @SP_DEPT
	   	,SP_DESC_D1 = @SP_DESC_D1
	   	,SP_DESC_D2 = @SP_DESC_D2
	   	,SP_DEPT_HEAD = @SP_DEPT_HEAD
	   	,SP_DATE_FROM_D = @SP_DATE_FROM_D
	   	,SP_DATE_TO_D = @SP_DATE_TO_D
	   	,RC = @RC
	   	,SP_RC_APA = @SP_RC_APA
		,DEPT_MAKER_ID = @MakerId
		,DEPT_ADDED_DATETIME = @AddedDatetime
		,DEPT_IS_AUTH = @IsAuth
	Where 	 ID = @ID	
	
	Set @oRetVal = -10 --Record updated successfully
	Return
GO
/****** Object:  StoredProcedure [dbo].[CHRIS_SP_DESIG_ADD]    Script Date: 01/02/2024 2:26:35 pm ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* -----------------------------------------------------------------------------------------------------------
 * SP Name		: CHRIS_SP_DESIGADD
 * ALTERd By	: Faizan Ashraf
 * ALTERd On	: 26/01/2011
 * Purpose		: Adds record(s) in the Table(DESIG) according to the parameter(s).
 * Module		: CHRIS
 * Called By	: iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.DESIG.InsertDESIG
 * Comments		: None
 * -----------------------------------------------------------------------------------------------------------
///-------------------------------------Modification History--------------------------------------------------
	[Date]		[Author]			[Purpose]

///----------------------------------------------------------------------------------------------------------- */

ALTER PROCEDURE [dbo].[CHRIS_SP_DESIG_ADD]
/*<Params>*/
@SP_DESG varchar(3), --<descr></descr>
@SP_BRANCH varchar(3), --<descr></descr>
@SP_LEVEL varchar(3), --<descr></descr>
@SP_CATEGORY varchar(1), --<descr></descr>
@SP_DESC_DG1 varchar(20), --<descr></descr>
@SP_DESC_DG2 varchar(20), --<descr></descr>
@SP_CONFIRM numeric(1,0), --<descr></descr>
@SP_MIN numeric(11,0), --<descr></descr>
@SP_MID numeric(11,0), --<descr></descr>
@SP_MAX numeric(11,0), --<descr></descr>
@SP_INCREMENT numeric(4,0), --<descr></descr>
@SP_EFFECTIVE datetime, --<descr></descr>
@SP_CURRENT varchar(1), --<descr></descr>
@ID INT  OUTPUT,  --<descr></descr>
@MakerId varchar(50) = null,
@CheckerkID varchar(50) = null,
@AddedDatetime datetime = null,
@UpdatedDatetime datetime = null,
@IsAuth bit = null,
@oRetVal int = Null Output --<descr></descr>
/*</Params>*/
As

Set NoCount On
/*<Comments></Comments>*/
/*<LocalVariables>*/
/*</LocalVariables>*/
 
	Insert Into dbo.DESIG
	(
		SP_DESG
		,SP_BRANCH
		,SP_LEVEL
		,SP_CATEGORY
		,SP_DESC_DG1
		,SP_DESC_DG2
		,SP_CONFIRM
		,SP_MIN
		,SP_MID
		,SP_MAX
		,SP_INCREMENT
		,SP_EFFECTIVE
		,SP_CURRENT
		,SP_D_MAKER_ID
		,SP_D_ADDED_DATETIME
		,SP_D_IS_AUTH
	)
	Values
	(
		@SP_DESG
		,@SP_BRANCH
		,@SP_LEVEL
		,@SP_CATEGORY
		,@SP_DESC_DG1
		,@SP_DESC_DG2
		,@SP_CONFIRM
		,@SP_MIN
		,@SP_MID
		,@SP_MAX
		,@SP_INCREMENT
		,@SP_EFFECTIVE
		,@SP_CURRENT
		,@MakerId
		,@AddedDatetime
		,@IsAuth
	)
	
	If @@RowCount > 0
	BEGIN
		Set @oRetVal = SCOPE_IDENTITY() --Record has been inserted successfully, return generated ID
		Set @ID = @oRetVal
	END
	Else
		Set @oRetVal = -40 --No record inserted
	
	Return
GO
/****** Object:  StoredProcedure [dbo].[CHRIS_SP_DESIG_GETALL]    Script Date: 01/02/2024 2:26:35 pm ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* -----------------------------------------------------------------------------------------------------------
 * SP Name		: CHRIS_SP_DESIG_GETALL
 * ALTERd By	: Faizan Ashraf
 * ALTERd On	: 26/01/2011
 * Purpose		: Retrieve entity(DESIG) records as per filter provided.
 * Module		: CHRIS
 * Called By	: iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.DESIG.GetDESIGRecords
 * Comments		: None
 * -----------------------------------------------------------------------------------------------------------
///-------------------------------------Modification History--------------------------------------------------
	[Date]		[Author]			[Purpose]

///----------------------------------------------------------------------------------------------------------- */
 
ALTER PROCEDURE [dbo].[CHRIS_SP_DESIG_GETALL]
/*<Param>*/
@oRetVal int Output --<descr></descr>
/*</Param>*/
As

Set NoCount On
/*<Comment></Comment>*/
	Select ID
	   	,SP_DESG
	   	,SP_BRANCH
	   	,SP_LEVEL
	   	,SP_CATEGORY
	   	,SP_DESC_DG1
	   	,SP_DESC_DG2
	   	,SP_CONFIRM
	   	,SP_MIN
	   	,SP_MID
	   	,SP_MAX
	   	,SP_INCREMENT
	   	,SP_EFFECTIVE
	   	,SP_CURRENT
	From dbo.DESIG WITH(NOLOCK) WHERE SP_D_IS_AUTH = 1



	If @@RowCount = 0 
		Set @oRetVal = -40	--Record not found
	Else
		Set @oRetVal = -10 	--Success
	
	Return
GO
/****** Object:  StoredProcedure [dbo].[CHRIS_SP_DESIG_MANAGER]    Script Date: 01/02/2024 2:26:35 pm ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* -----------------------------------------------------------------------------------------------------------
 * SP Name		: CHRIS_SP_DESIGMANAGER
 * ALTERd By	: Faizan Ashraf
 * ALTERd On	: 26/01/2011
 * Purpose		: Manages (DESIG) store procedure depending upon the action type.
 * Module		: CHRIS
 * Called By	: iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.DESIGDAO
 * Comments		: None
 * -----------------------------------------------------------------------------------------------------------
///-------------------------------------Modification History--------------------------------------------------
	[Date]		[Author]			[Purpose]

///----------------------------------------------------------------------------------------------------------- */
                       
ALTER PROCEDURE [dbo].[CHRIS_SP_DESIG_MANAGER]
/*<Param>*/
@SP_DESG varchar(3), --<descr></descr>
@SP_BRANCH varchar(3), --<descr></descr>
@SP_LEVEL varchar(3), --<descr></descr>
@SP_CATEGORY varchar(1), --<descr></descr>
@SP_DESC_DG1 varchar(20), --<descr></descr>
@SP_DESC_DG2 varchar(20), --<descr></descr>
@SP_CONFIRM numeric(1,0), --<descr></descr>
@SP_MIN numeric(11,0), --<descr></descr>
@SP_MID numeric(11,0), --<descr></descr>
@SP_MAX numeric(11,0), --<descr></descr>
@SP_INCREMENT numeric(4,0), --<descr></descr>
@SP_EFFECTIVE datetime, --<descr></descr>
@SP_CURRENT varchar(1), --<descr></descr>
@SearchFilter varchar(50),
@ID INT  OUTPUT,  --<descr></descr>
@ActionType varchar(50),
@MakerId varchar(50) = null,
@CheckerkID varchar(50) = null,
@AddedDatetime datetime = null,
@UpdatedDatetime datetime = null,
@IsAuth bit = null
,@oRetVal int = Null Output
/*</Param>*/
As

Set NoCount On
/*<Comment></Comment>*/


	If @ActionType = 'Save'
	Begin
		Exec CHRIS_SP_DESIG_ADD
	   			@SP_DESG
	   			,@SP_BRANCH
	   			,@SP_LEVEL
	   			,@SP_CATEGORY
	   			,@SP_DESC_DG1
	   			,@SP_DESC_DG2
	   			,@SP_CONFIRM
	   			,@SP_MIN
	   			,@SP_MID
	   			,@SP_MAX
	   			,@SP_INCREMENT
	   			,@SP_EFFECTIVE
	   			,@SP_CURRENT
	   			,@ID
				,@MakerId
				,@AddedDatetime
				,@IsAuth
				,@oRetVal = @oRetVal OUTPUT
--			SET @ID =  @oRetVal
	End
	Else If @ActionType = 'Update'
	Begin
		Exec CHRIS_SP_DESIG_UPDATE
	   			@SP_DESG
	   			,@SP_BRANCH
	   			,@SP_LEVEL
	   			,@SP_CATEGORY
	   			,@SP_DESC_DG1
	   			,@SP_DESC_DG2
	   			,@SP_CONFIRM
	   			,@SP_MIN
	   			,@SP_MID
	   			,@SP_MAX
	   			,@SP_INCREMENT
	   			,@SP_EFFECTIVE
	   			,@SP_CURRENT
	   			,@ID
				,@MakerId
				,@AddedDatetime
				,@IsAuth
	End
	Else If @ActionType = 'Delete'
	Begin
		Exec CHRIS_SP_DESIG_DELETE
				@ID,@oRetVal
	End
	Else If @ActionType = 'Get'
	Begin
		Exec CHRIS_SP_DESIG_GET
				@ID,@oRetVal
	End
	Else If @ActionType = 'List'
	Begin
		Exec CHRIS_SP_DESIG_GETALL @oRetVal
	End
-----------------------------------------------------------------------------

Else if @Actiontype = 'Lov_DSG'
Begin
SELECT ALL ID, DESIG.SP_DESG, DESIG.SP_BRANCH, DESIG.SP_LEVEL, DESIG.SP_CATEGORY
FROM DESIG 
End

Else if @Actiontype = 'Lov_DSG_Exists'
Begin

If Exists (Select ID from DESIG  Where SP_DESG = (select Data  from  dbo.fn_Distinct_Split(@SearchFilter,'|') where id = 1)
AND SP_BRANCH= (select Data  from  dbo.fn_Distinct_Split(@SearchFilter,'|') where id = 3)
AND SP_LEVEL= (select Data  from  dbo.fn_Distinct_Split(@SearchFilter,'|') where id = 2)
--where SP_DESG = @SP_DESG AND SP_BRANCH= @SP_BRANCH AND SP_LEVEL=@SP_LEVEL)
)
Begin
SELECT ALL ID,DESIG.SP_DESG, DESIG.SP_BRANCH, DESIG.SP_LEVEL, DESIG.SP_CATEGORY
FROM DESIG 
Where SP_DESG = (select Data  from  dbo.fn_Distinct_Split(@SearchFilter,'|') where id = 1)
AND SP_BRANCH= (select Data  from  dbo.fn_Distinct_Split(@SearchFilter,'|') where id = 3)
AND SP_LEVEL= (select Data  from  dbo.fn_Distinct_Split(@SearchFilter,'|') where id = 2)


End

--If Exists (Select ID from DESIG where SP_DESG Like @SP_DESG + '%' AND SP_BRANCH like @SP_BRANCH + '%' AND SP_LEVEL like @SP_LEVEL + '%' )
--Begin
--SELECT ALL ID,DESIG.SP_DESG, DESIG.SP_BRANCH, DESIG.SP_LEVEL, DESIG.SP_CATEGORY
--FROM DESIG 
--where SP_DESG Like @SP_DESG + '%' AND SP_BRANCH Like @SP_BRANCH + '%' AND SP_LEVEL Like @SP_LEVEL + '%'
--End

End
-----------------------------------------------------------------------------
Else if @Actiontype = 'Lov_BRN'
Begin
select brn_code SP_BRANCH,brn_name  BRN_NAME from branch
End

Else if @Actiontype = 'Lov_BRN_Exists'
Begin
	Begin
	IF Exists (Select Id From branch where brn_code = @SP_BRANCH )
	select brn_code SP_BRANCH,brn_name  BRN_NAME from branch
	where brn_code = @SP_BRANCH
	End
	Begin
	IF Exists (Select Id From branch where brn_code Like @SP_BRANCH + '%' )
	select brn_code SP_BRANCH,brn_name  BRN_NAME from branch
	where brn_code Like @SP_BRANCH + '%'
	End
End

-----------------------------------------------------------------------------
Else if @Actiontype = 'Lov_CAT'
Begin
select sp_cat_code SP_CATEGORY,sp_desc  CAT_DISC from category
End

Else if @Actiontype = 'Lov_CAT_Exists'
Begin
	If Exists (Select Id from category where sp_cat_code = @SP_CATEGORY )
	Begin
	select sp_cat_code SP_CATEGORY,sp_desc  CAT_DISC from category
	where sp_cat_code = @SP_CATEGORY
	End 
   
   If Exists (Select Id from category where sp_cat_code Like @SP_CATEGORY + '%' )
	Begin
	select sp_cat_code SP_CATEGORY,sp_desc  CAT_DISC from category
	where sp_cat_code Like @SP_CATEGORY + '%'
	End 

End


Else if @Actiontype = 'GetDesc'
Begin
	select * from category where sp_cat_code = @SP_CATEGORY
	
End


Else if @Actiontype = 'GetBranchName'
Begin
	select * from branch where brn_code = @SP_BRANCH
End

-----------------------------------------------------------------------------
Else if @Actiontype = 'GetAll_Columns'
Begin
		select   SP_DESG
                ,SP_BRANCH
                ,(select brn_name  BRN_NAME from branch where brn_code = SP_BRANCH)BRN_NAME
				,SP_LEVEL
				,SP_CATEGORY
				,(select sp_desc  CAT_DISC from category where sp_cat_code = SP_CATEGORY)CAT_DISC
                ,SP_DESC_DG1
				,SP_DESC_DG2
				,SP_CONFIRM
				,SP_MIN
				,SP_MID
				,SP_MAX
				,SP_INCREMENT
				,SP_EFFECTIVE
				,SP_CURRENT
                ,ID
		 from  desig
		--where SP_DESG = @SP_DESG
END

ELSE IF @ACTIONTYPE = 'AUTHORIZE_DESIG'      
	 BEGIN      
	  UPDATE DESIG SET SP_D_IS_AUTH = 1, SP_D_CHECKER_ID = @CheckerkID,SP_D_UPDATED_DATETIME = @UpdatedDatetime WHERE ID = @ID     
	 END 

	 ELSE IF @ACTIONTYPE = 'REJECT_DESIG'      
	 BEGIN      
	  DELETE FROM DESIG WHERE ID = @ID     
	 END 

	ELSE IF @ACTIONTYPE = 'GET_UNAUTHORIZE_DESIG'      
	 BEGIN
		Select ID
	   	,SP_DESG
	   	,SP_BRANCH
	   	,SP_LEVEL
	   	,SP_CATEGORY
	   	,SP_DESC_DG1
	   	,SP_DESC_DG2
	   	,SP_CONFIRM
	   	,SP_MIN
	   	,SP_MID
	   	,SP_MAX
	   	,SP_INCREMENT
	   	,SP_EFFECTIVE
	   	,SP_CURRENT
	From dbo.DESIG WITH(NOLOCK) WHERE SP_D_IS_AUTH = 0 AND SP_D_MAKER_ID != @MakerId
	
	Return
	 END 



	Return
--select  * from desig
--where sp_Desg ='CTT'
--and SP_LEVEL = 'C'
--and SP_CATEGORY = 'C'
--and SP_MIN =51168
--and SP_MID =88452
--and SP_INCREMENT = 239
GO
/****** Object:  StoredProcedure [dbo].[CHRIS_SP_DESIG_UPDATE]    Script Date: 01/02/2024 2:26:35 pm ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* -----------------------------------------------------------------------------------------------------------
 * SP Name		: CHRIS_SP_DESIGUPDATE
 * ALTERd By	: Faizan Ashraf
 * ALTERd On	: 26/01/2011
 * Purpose		: Updates record(s) in the Table(DESIG) according to the parameter(s).
 * Module		: CHRIS
 * Called By	: iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.DESIG.InsertDESIG
 * Comments		: None
 * -----------------------------------------------------------------------------------------------------------
///-------------------------------------Modification History--------------------------------------------------
	[Date]		[Author]			[Purpose]

///----------------------------------------------------------------------------------------------------------- */
 
ALTER PROCEDURE [dbo].[CHRIS_SP_DESIG_UPDATE]
/*<Param>*/
@SP_DESG varchar(3), --<descr></descr>
@SP_BRANCH varchar(3), --<descr></descr>
@SP_LEVEL varchar(3), --<descr></descr>
@SP_CATEGORY varchar(1), --<descr></descr>
@SP_DESC_DG1 varchar(20), --<descr></descr>
@SP_DESC_DG2 varchar(20), --<descr></descr>
@SP_CONFIRM numeric(1,0), --<descr></descr>
@SP_MIN numeric(11,0), --<descr></descr>
@SP_MID numeric(11,0), --<descr></descr>
@SP_MAX numeric(11,0), --<descr></descr>
@SP_INCREMENT numeric(4,0), --<descr></descr>
@SP_EFFECTIVE datetime, --<descr></descr>
@SP_CURRENT varchar(1), --<descr></descr>
@ID INT, --<descr></descr>
@MakerId varchar(50) = null,
@AddedDatetime datetime = null,
@IsAuth bit = null,
@oRetVal int = Null Output
/*</Param>*/
As

Set NoCount On
/*<Comment></Comment>*/
	Update	dbo.DESIG
	Set
	   	SP_DESG = @SP_DESG
	   	,SP_BRANCH = @SP_BRANCH
	   	,SP_LEVEL = @SP_LEVEL
	   	,SP_CATEGORY = @SP_CATEGORY
	   	,SP_DESC_DG1 = @SP_DESC_DG1
	   	,SP_DESC_DG2 = @SP_DESC_DG2
	   	,SP_CONFIRM = @SP_CONFIRM
	   	,SP_MIN = @SP_MIN
	   	,SP_MID = @SP_MID
	   	,SP_MAX = @SP_MAX
	   	,SP_INCREMENT = @SP_INCREMENT
	   	,SP_EFFECTIVE = @SP_EFFECTIVE
	   	,SP_CURRENT = @SP_CURRENT
		,SP_D_MAKER_ID = @MakerId
		,SP_D_ADDED_DATETIME = @AddedDatetime
		,SP_D_IS_AUTH = @IsAuth
	Where 	 ID = @ID	
	
	Set @oRetVal = -10 --Record updated successfully
	Return
GO
/****** Object:  StoredProcedure [dbo].[CHRIS_SP_GROUP_INSC_ADD]    Script Date: 01/02/2024 2:26:35 pm ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* -----------------------------------------------------------------------------------------------------------
 * SP Name		: CHRIS_SP_GROUP_INSCADD
 * ALTERd By	: Faizan Ashraf
 * ALTERd On	: 24/01/2011
 * Purpose		: Adds record(s) in the Table(GROUP_INSC) according to the parameter(s).
 * Module		: CHRIS
 * Called By	: iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.GROUP_INSC.InsertGROUP_INSC
 * Comments		: None
 * -----------------------------------------------------------------------------------------------------------
///-------------------------------------Modification History--------------------------------------------------
	[Date]		[Author]			[Purpose]

///----------------------------------------------------------------------------------------------------------- */

ALTER PROCEDURE [dbo].[CHRIS_SP_GROUP_INSC_ADD]
/*<Params>*/
@SP_TYPE_CODE varchar(1), --<descr></descr>
@SP_NAME_1 varchar(20), --<descr></descr>
@SP_NAME_2 varchar(20), --<descr></descr>
@SP_STAFF_TYPE varchar(1), --<descr></descr>
@SP_RENEWAL datetime, --<descr></descr>
@SP_POLICY_NO varchar(20), --<descr></descr>
@SP_L_COVERAGE numeric(7,0), --<descr></descr>
@SP_H_COVERAGE numeric(7,0), --<descr></descr>
@SP_DESIG_IN varchar(3), --<descr></descr>
@SP_LEVEL_IN varchar(3), --<descr></descr>
@SP_MULT_PACK numeric(1,0), --<descr></descr>
@ID INT  OUTPUT,  --<descr></descr>
@MakerId varchar(50) = null,
@AddedDatetime datetime = null,
@IsAuth bit = null,
@oRetVal int = Null Output --<descr></descr>
/*</Params>*/
As

Set NoCount On
/*<Comments></Comments>*/
/*<LocalVariables>*/
/*</LocalVariables>*/
 
	Insert Into dbo.GROUP_INSC
	(
		SP_TYPE_CODE
		,SP_NAME_1
		,SP_NAME_2
		,SP_STAFF_TYPE
		,SP_RENEWAL
		,SP_POLICY_NO
		,SP_L_COVERAGE
		,SP_H_COVERAGE
		,SP_DESIG_IN
		,SP_LEVEL_IN
		,SP_MULT_PACK
		,SP_MAKER_ID
		,SP_ADDED_DATETIME
		,SP_IS_AUTH
	)
	Values
	(
		@SP_TYPE_CODE
		,@SP_NAME_1
		,@SP_NAME_2
		,@SP_STAFF_TYPE
		,@SP_RENEWAL
		,@SP_POLICY_NO
		,@SP_L_COVERAGE
		,@SP_H_COVERAGE
		,@SP_DESIG_IN
		,@SP_LEVEL_IN
		,@SP_MULT_PACK
		,@MakerId
		,@AddedDatetime
		,@IsAuth
	)
	
	If @@RowCount > 0
	BEGIN
		Set @oRetVal = SCOPE_IDENTITY() --Record has been inserted successfully, return generated ID
		Set @ID = @oRetVal
	END
	Else
		Set @oRetVal = -40 --No record inserted
	
	Return
GO
/****** Object:  StoredProcedure [dbo].[CHRIS_SP_GROUP_INSC_GETALL]    Script Date: 01/02/2024 2:26:35 pm ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* -----------------------------------------------------------------------------------------------------------
 * SP Name		: CHRIS_SP_GROUP_INSC_GETALL
 * ALTERd By	: Faizan Ashraf
 * ALTERd On	: 24/01/2011
 * Purpose		: Retrieve entity(GROUP_INSC) records as per filter provided.
 * Module		: CHRIS
 * Called By	: iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.GROUP_INSC.GetGROUP_INSCRecords
 * Comments		: None
 * -----------------------------------------------------------------------------------------------------------
///-------------------------------------Modification History--------------------------------------------------
	[Date]		[Author]			[Purpose]

///----------------------------------------------------------------------------------------------------------- */
 
ALTER PROCEDURE [dbo].[CHRIS_SP_GROUP_INSC_GETALL]
/*<Param>*/
@oRetVal int Output --<descr></descr>
/*</Param>*/
As

Set NoCount On
/*<Comment></Comment>*/
	Select ID
	   	,SP_TYPE_CODE
	   	,SP_NAME_1
	   	,SP_NAME_2
	   	,SP_STAFF_TYPE
	   	,SP_RENEWAL
	   	,SP_POLICY_NO
	   	,SP_L_COVERAGE
	   	,SP_H_COVERAGE
	   	,SP_DESIG_IN
	   	,SP_LEVEL_IN
	   	,SP_MULT_PACK
	From dbo.GROUP_INSC WITH(NOLOCK) WHERE SP_IS_AUTH = 1



	If @@RowCount = 0 
		Set @oRetVal = -40	--Record not found
	Else
		Set @oRetVal = -10 	--Success
	
	Return
GO
/****** Object:  StoredProcedure [dbo].[CHRIS_SP_GROUP_INSC_MANAGER]    Script Date: 01/02/2024 2:26:35 pm ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* -----------------------------------------------------------------------------------------------------------
 * SP Name		: CHRIS_SP_GROUP_INSCMANAGER
 * ALTERd By	: Faizan Ashraf
 * ALTERd On	: 24/01/2011
 * Purpose		: Manages (GROUP_INSC) store procedure depending upon the action type.
 * Module		: CHRIS
 * Called By	: iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.GROUP_INSCDAO
 * Comments		: None
 * -----------------------------------------------------------------------------------------------------------
///-------------------------------------Modification History--------------------------------------------------
	[Date]		[Author]			[Purpose]

///----------------------------------------------------------------------------------------------------------- */
 
ALTER PROCEDURE [dbo].[CHRIS_SP_GROUP_INSC_MANAGER]
/*<Param>*/
@SP_TYPE_CODE varchar(1), --<descr></descr>
@SP_NAME_1 varchar(20), --<descr></descr>
@SP_NAME_2 varchar(20), --<descr></descr>
@SP_STAFF_TYPE varchar(1), --<descr></descr>
@SP_RENEWAL datetime, --<descr></descr>
@SP_POLICY_NO varchar(20), --<descr></descr>
@SP_L_COVERAGE numeric(7,0), --<descr></descr>
@SP_H_COVERAGE numeric(7,0), --<descr></descr>
@SP_DESIG_IN varchar(3), --<descr></descr>
@SP_LEVEL_IN varchar(3), --<descr></descr>
@SP_MULT_PACK numeric(1,0), --<descr></descr>
@ID INT  OUTPUT,  --<descr></descr>
@ActionType varchar(50),
@MakerId varchar(50) = null,
@CheckerkID varchar(50) = null,
@AddedDatetime datetime = null,
@UpdatedDatetime datetime = null,
@IsAuth bit = null
,@oRetVal int = Null Output
/*</Param>*/
As

Set NoCount On
/*<Comment></Comment>*/


	If @ActionType = 'Save'
	Begin
		Exec CHRIS_SP_GROUP_INSC_ADD
	   			@SP_TYPE_CODE
	   			,@SP_NAME_1
	   			,@SP_NAME_2
	   			,@SP_STAFF_TYPE
	   			,@SP_RENEWAL
	   			,@SP_POLICY_NO
	   			,@SP_L_COVERAGE
	   			,@SP_H_COVERAGE
	   			,@SP_DESIG_IN
	   			,@SP_LEVEL_IN
	   			,@SP_MULT_PACK
	   			,@ID
				,@oRetVal = @oRetVal OUTPUT
			SET @ID =  @oRetVal
	End
	Else If @ActionType = 'Update'
	Begin
		Exec CHRIS_SP_GROUP_INSC_UPDATE
	   			@SP_TYPE_CODE
	   			,@SP_NAME_1
	   			,@SP_NAME_2
	   			,@SP_STAFF_TYPE
	   			,@SP_RENEWAL
	   			,@SP_POLICY_NO
	   			,@SP_L_COVERAGE
	   			,@SP_H_COVERAGE
	   			,@SP_DESIG_IN
	   			,@SP_LEVEL_IN
	   			,@SP_MULT_PACK
	   			,@ID
	End
	Else If @ActionType = 'Delete'
	Begin
		Exec CHRIS_SP_GROUP_INSC_DELETE
				@ID,@oRetVal
	End
	Else If @ActionType = 'Get'
	Begin
		Exec CHRIS_SP_GROUP_INSC_GET
				@ID,@oRetVal
	End
	Else If @ActionType = 'List'
	Begin
		Exec CHRIS_SP_GROUP_INSC_GETALL @oRetVal
	End
---------------------------------------------------------------------------------------------------------------------
Else if @actiontype ='Type_code' 
Begin
select ID,substring(sp_type_code,1,1)sp_type_code 
		, substring(sp_staff_type,1,1)sp_staff_type
		, substring(sp_desig_in,1,3)sp_desig_in
		, substring(sp_level_in,1,3)sp_level_in 
		from group_insc
End


Else if @actiontype ='Type_code_exists' 
Begin

if Exists (select ID from group_insc where sp_type_code = @SP_TYPE_CODE )
begin
select ID,substring(sp_type_code,1,1)sp_type_code 
		, substring(sp_staff_type,1,1)sp_staff_type
		, substring(sp_desig_in,1,3)sp_desig_in
		, substring(sp_level_in,1,3)sp_level_in 
		from group_insc
      where  sp_type_code = @SP_TYPE_CODE
end

End
---------------------------------------------------------------------------------------------------------------------
Else if @Actiontype= 'StaffType'
Begin
select substring(sp_type_code,1,1), substring(sp_staff_type,1,1), substring(sp_desig_in,1,3), substring(sp_level_in,1,3) from group_insc
End


Else if @Actiontype= 'StaffType_Exists'
Begin
if Exists (select Id from group_insc where  sp_staff_type = @sp_staff_type )
select substring(sp_type_code,1,1), substring(sp_staff_type,1,1), substring(sp_desig_in,1,3), substring(sp_level_in,1,3)
 from group_insc
where  sp_staff_type = @sp_staff_type
End



---------------------------------------------------------------------------------------------------------------------
Else if @Actiontype = 'sp_desig'
Begin
select distinct sp_desg SP_DESIG_IN, sp_level SP_LEVEL_IN from desig where sp_current = 'C'
End

Else if @Actiontype = 'sp_desig_Exists'
Begin
If Exists ( Select Id from  desig  where sp_desg = @SP_DESIG_IN)

select distinct sp_desg SP_DESIG_IN , sp_level SP_LEVEL_IN from desig where sp_current = 'C'
and sp_desg = @SP_DESIG_IN
End


---------------------------------------------------------------------------------------------------------------------
Else if @Actiontype = 'Level_In' 
Begin
select distinct sp_desg SP_DESIG_IN , sp_level  SP_LEVEL_IN from desig where sp_current = 'C'
End

Else if @Actiontype = 'Level_In_Exists' 
Begin
If Exists (Select Id from  desig  where sp_level = @SP_LEVEL_IN)
 
select distinct sp_desg SP_DESIG_IN , sp_level  SP_LEVEL_IN from desig where sp_current = 'C'
and sp_level = @SP_LEVEL_IN
End


---------------------------------------------------------------------------------------------------------------------
 Else if @actiontype = 'proc_delete'
 

	Begin
            Declare @Count_delete int 
			select @Count_delete = count(*) from GROUP_INSC
			where sp_type_code  = @sp_type_code and
				  sp_staff_type = @sp_staff_type and
				  sp_desig_in   = @sp_desig_in and
				  sp_level_in   = @sp_level_in

			if (@Count_delete > 0)
			Begin
				   delete from GROUP_INSC 
          where sp_type_code  = @sp_type_code and
				sp_staff_type = @sp_staff_type and
				sp_desig_in   = @sp_desig_in and
				sp_level_in   = @sp_level_in
			End 
	  
	End
---------------------------------------------------------------------------------------------------------------------
Else if @Actiontype = 'proc_modify'
Begin

		declare @count_Modify int

	 select  @count_Modify = count(*)  from GROUP_INSC
	where sp_type_code = @sp_type_code and  sp_staff_type= @sp_staff_type and  sp_desig_in = @sp_desig_in and sp_level_in = @sp_level_in



			Begin

select @sp_type_code = sp_type_code from GROUP_INSC where sp_type_code = @sp_type_code and sp_staff_type= @sp_staff_type and sp_desig_in  = @sp_desig_in and sp_level_in  = @sp_level_in
select @sp_staff_type from GROUP_INSC where sp_type_code = @sp_type_code and   sp_staff_type= @sp_staff_type and sp_desig_in  = @sp_desig_in and  sp_level_in  = @sp_level_in
select @sp_name_1  from GROUP_INSC where sp_type_code = @sp_type_code and   sp_staff_type= @sp_staff_type and sp_desig_in  = @sp_desig_in and  sp_level_in  = @sp_level_in		 
select @sp_name_2  from GROUP_INSC where sp_type_code = @sp_type_code and   sp_staff_type= @sp_staff_type and  sp_desig_in  = @sp_desig_in and sp_level_in  = @sp_level_in
select @sp_renewal from GROUP_INSC where sp_type_code = @sp_type_code and   sp_staff_type= @sp_staff_type and  sp_desig_in  = @sp_desig_in and  sp_level_in  = @sp_level_in
select @sp_policy_no from GROUP_INSC  where sp_type_code = @sp_type_code and   sp_staff_type= @sp_staff_type and sp_desig_in  = @sp_desig_in and sp_level_in  = @sp_level_in
select @sp_l_coverage from GROUP_INSC where sp_type_code = @sp_type_code and   sp_staff_type= @sp_staff_type and  sp_desig_in  = @sp_desig_in and sp_level_in  = @sp_level_in
select @sp_h_coverage from GROUP_INSC  where sp_type_code = @sp_type_code and   sp_staff_type= @sp_staff_type and sp_desig_in  = @sp_desig_in and sp_level_in  = @sp_level_in
select @sp_desig_in from GROUP_INSC  where sp_type_code = @sp_type_code and   sp_staff_type= @sp_staff_type and  sp_desig_in  = @sp_desig_in and sp_level_in  = @sp_level_in
select @sp_level_in from GROUP_INSC where sp_type_code = @sp_type_code and   sp_staff_type= @sp_staff_type and  sp_desig_in  = @sp_desig_in and sp_level_in  = @sp_level_in
select @sp_mult_pack from GROUP_INSC where sp_type_code = @sp_type_code and   sp_staff_type= @sp_staff_type and  sp_desig_in  = @sp_desig_in and sp_level_in  = @sp_level_in


               update GROUP_INSC set   sp_type_code      =  @sp_type_code,
									   sp_staff_type     =  @sp_staff_type,
									   sp_name_1         =  @sp_name_1 ,
									   sp_name_2         =  @sp_name_2,
									   sp_renewal        =  @sp_renewal,
									   sp_policy_no      =  @sp_policy_no,
									   sp_l_coverage     =  @sp_l_coverage,
									   sp_h_coverage     =  @sp_h_coverage,
									   sp_desig_in       =  @sp_desig_in,
									   sp_level_in       =  @sp_level_in,
									   sp_mult_pack      =  @sp_mult_pack


			End 
End
---------------------------------------------------------------------------------------------------------------------
Else if @Actiontype = 'proc_mod_Del'

begin
 select sp_type_code,    sp_staff_type,
        sp_name_1,       sp_name_2,
        sp_renewal,      sp_policy_no,
        sp_l_coverage,   sp_h_coverage,
        sp_desig_in,     sp_level_in,
        sp_mult_pack
    from  GROUP_INSC
  where sp_type_code  = @sp_type_code and
        sp_staff_type = @sp_staff_type and
        sp_desig_in   = @sp_desig_in and
        sp_level_in   = @sp_level_in
End

---------------------------------------------------------------------------------------------------------------------
Else if @Actiontype = 'proc_view_add'
Begin 

	select sp_type_code,    sp_staff_type,
			sp_name_1,       sp_name_2,
			sp_renewal,      sp_policy_no,
			sp_l_coverage,   sp_h_coverage,
			sp_desig_in,     sp_level_in,
			sp_mult_pack
	  
	  from  GROUP_INSC
	  where sp_type_code  = @sp_type_code and
			sp_staff_type = @sp_staff_type and
			sp_desig_in   = @sp_desig_in and
			sp_level_in   = @sp_level_in
End
---------------------------------------------------------------------------------------------------------------------
Else if @Actiontype = 'sp_desig_IfClause'
Begin 
select sp_desg 
 from desig
 where sp_desg = @sp_desig_in and sp_current = 'C' and sp_category = 'C';
End

---------------------------------------------------------------------------------------------------------------------
Else if @Actiontype = 'sp_desig_ElseClause'
Begin  
select sp_desg 
 from desig 
where sp_desg = @sp_desig_in
 and sp_current = 'C' 
and sp_category != 'C';
End
---------------------------------------------------------------------------------------------------------------------
Else if @Actiontype = 'sp_Level_IfClause'
Begin
 select sp_desg, sp_level 
 from desig
 where sp_desg = @sp_desig_in and sp_level = @sp_level_in and
       sp_current = 'C' and sp_category = 'C';

End
---------------------------------------------------------------------------------------------------------------------
Else if @Actiontype = 'sp_Level_ElseClause'
Begin
select sp_desg, sp_level
 from desig
 where sp_desg = @sp_desig_in and sp_level = @sp_level_in and
 sp_current = 'C' and sp_category != 'C';
End
---------------------------------------------------------------------------------------------------------------------

ELSE IF @ACTIONTYPE = 'AUTHORIZE_GROUP_INSC'      
	 BEGIN      
	  UPDATE GROUP_INSC SET SP_IS_AUTH = 1, SP_CHECKER_ID = @CheckerkID,SP_UPDATED_DATETIME = @UpdatedDatetime WHERE ID = @ID     
	 END 
------------------------------------------------------------------------------------------------------------------------------------------
	 ELSE IF @ACTIONTYPE = 'REJECT_GROUP_INSC'      
	 BEGIN      
	  DELETE FROM GROUP_INSC WHERE ID = @ID     
	 END 
------------------------------------------------------------------------------------------------------------------------------------------

	ELSE IF @ACTIONTYPE = 'GET_UNAUTHORIZE_GROUP_INSC'      
	 BEGIN
		Select ID
	   	,SP_TYPE_CODE
	   	,SP_NAME_1
	   	,SP_NAME_2
	   	,SP_STAFF_TYPE
	   	,SP_RENEWAL
	   	,SP_POLICY_NO
	   	,SP_L_COVERAGE
	   	,SP_H_COVERAGE
	   	,SP_DESIG_IN
	   	,SP_LEVEL_IN
	   	,SP_MULT_PACK
	From dbo.GROUP_INSC WITH(NOLOCK) WHERE SP_IS_AUTH = 0 AND SP_MAKER_ID != @MakerId
	
	Return
	 END 
------------------------------------------------------------------------------------------------------------------------------------------
Return


GO
/****** Object:  StoredProcedure [dbo].[CHRIS_SP_GROUP_INSC_UPDATE]    Script Date: 01/02/2024 2:26:35 pm ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* -----------------------------------------------------------------------------------------------------------
 * SP Name		: CHRIS_SP_GROUP_INSCUPDATE
 * ALTERd By	: Faizan Ashraf
 * ALTERd On	: 24/01/2011
 * Purpose		: Updates record(s) in the Table(GROUP_INSC) according to the parameter(s).
 * Module		: CHRIS
 * Called By	: iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.GROUP_INSC.InsertGROUP_INSC
 * Comments		: None
 * -----------------------------------------------------------------------------------------------------------
///-------------------------------------Modification History--------------------------------------------------
	[Date]		[Author]			[Purpose]

///----------------------------------------------------------------------------------------------------------- */
 
ALTER PROCEDURE [dbo].[CHRIS_SP_GROUP_INSC_UPDATE]
/*<Param>*/
@SP_TYPE_CODE varchar(1), --<descr></descr>
@SP_NAME_1 varchar(20), --<descr></descr>
@SP_NAME_2 varchar(20), --<descr></descr>
@SP_STAFF_TYPE varchar(1), --<descr></descr>
@SP_RENEWAL datetime, --<descr></descr>
@SP_POLICY_NO varchar(20), --<descr></descr>
@SP_L_COVERAGE numeric(7,0), --<descr></descr>
@SP_H_COVERAGE numeric(7,0), --<descr></descr>
@SP_DESIG_IN varchar(3), --<descr></descr>
@SP_LEVEL_IN varchar(3), --<descr></descr>
@SP_MULT_PACK numeric(1,0), --<descr></descr>
@ID INT, --<descr></descr>
@MakerId varchar(50) = null,
@AddedDatetime datetime = null,
@IsAuth bit = null,
@oRetVal int = Null Output
/*</Param>*/
As

Set NoCount On
/*<Comment></Comment>*/
	Update	dbo.GROUP_INSC
	Set
	   	SP_TYPE_CODE = @SP_TYPE_CODE
	   	,SP_NAME_1 = @SP_NAME_1
	   	,SP_NAME_2 = @SP_NAME_2
	   	,SP_STAFF_TYPE = @SP_STAFF_TYPE
	   	,SP_RENEWAL = @SP_RENEWAL
	   	,SP_POLICY_NO = @SP_POLICY_NO
	   	,SP_L_COVERAGE = @SP_L_COVERAGE
	   	,SP_H_COVERAGE = @SP_H_COVERAGE
	   	,SP_DESIG_IN = @SP_DESIG_IN
	   	,SP_LEVEL_IN = @SP_LEVEL_IN
	   	,SP_MULT_PACK = @SP_MULT_PACK
		,SP_MAKER_ID = @MakerId
		,SP_ADDED_DATETIME = @AddedDatetime
		,SP_IS_AUTH = @IsAuth
	Where 	 ID = @ID	
	
	Set @oRetVal = -10 --Record updated successfully
	Return
GO
/****** Object:  StoredProcedure [dbo].[CHRIS_SP_HOLIDAY_ADD]    Script Date: 01/02/2024 2:26:35 pm ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* -----------------------------------------------------------------------------------------------------------
 * SP Name		: CHRIS_SP_HOLIDAYADD
 * ALTERd By	: Nida Nazir
 * ALTERd On	: 10/12/2010
 * Purpose		: Adds record(s) in the Table(HOLIDAY) according to the parameter(s).
 * Module		: CHRIS
 * Called By	: iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.HOLIDAY.InsertHOLIDAY
 * Comments		: None
 * -----------------------------------------------------------------------------------------------------------
///-------------------------------------Modification History--------------------------------------------------
	[Date]		[Author]			[Purpose]

///----------------------------------------------------------------------------------------------------------- */

ALTER PROCEDURE [dbo].[CHRIS_SP_HOLIDAY_ADD]
/*<Params>*/
@SP_HOL_DATE datetime, --<descr></descr>
@SP_H_DESC varchar(20), --<descr></descr>
@ID INT  OUTPUT,  --<descr></descr>
@MakerId varchar(50) = null,
@AddedDatetime datetime = null,
@IsAuth bit = null,
@oRetVal int = Null Output --<descr></descr>
/*</Params>*/
As

Set NoCount On
/*<Comments></Comments>*/
/*<LocalVariables>*/
/*</LocalVariables>*/
 
	Insert Into dbo.HOLIDAY
	(
		SP_HOL_DATE
		,SP_H_DESC
		,SH_MAKER_ID
		,SH_ADDED_DATETIME
		,SH_IS_AUTH
	)
	Values
	(
		@SP_HOL_DATE
		,@SP_H_DESC
		,@MakerId
		,@AddedDatetime
		,@IsAuth
	)
	
	If @@RowCount > 0
	BEGIN
		Set @oRetVal = SCOPE_IDENTITY() --Record has been inserted successfully, return generated ID
		Set @ID = @oRetVal
	END
	Else
		Set @oRetVal = -40 --No record inserted
	
	Return
GO
/****** Object:  StoredProcedure [dbo].[CHRIS_SP_HOLIDAY_GETALL]    Script Date: 01/02/2024 2:26:35 pm ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* -----------------------------------------------------------------------------------------------------------
 * SP Name		: CHRIS_SP_HOLIDAY_GETALL
 * ALTERd By	: Nida Nazir
 * ALTERd On	: 10/12/2010
 * Purpose		: Retrieve entity(HOLIDAY) records as per filter provided.
 * Module		: CHRIS
 * Called By	: iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.HOLIDAY.GetHOLIDAYRecords
 * Comments		: None
 * -----------------------------------------------------------------------------------------------------------
///-------------------------------------Modification History--------------------------------------------------
	[Date]		[Author]			[Purpose]

///----------------------------------------------------------------------------------------------------------- */
 
ALTER PROCEDURE [dbo].[CHRIS_SP_HOLIDAY_GETALL]
/*<Param>*/
@oRetVal int Output --<descr></descr>
/*</Param>*/
As

Set NoCount On
/*<Comment></Comment>*/
	Select ID
	   	,CONVERT(VARCHAR, SP_HOL_DATE, 103) AS SP_HOL_DATE
	   	,SP_H_DESC
	From dbo.HOLIDAY WITH(NOLOCK) WHERE SH_IS_AUTH = 1 order by  Year(SP_HOL_DATE) asc



	If @@RowCount = 0 
		Set @oRetVal = -40	--Record not found
	Else
		Set @oRetVal = -10 	--Success
	
	Return
	
GO
/****** Object:  StoredProcedure [dbo].[CHRIS_SP_HOLIDAY_MANAGER]    Script Date: 01/02/2024 2:26:35 pm ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* -----------------------------------------------------------------------------------------------------------
 * SP Name		: CHRIS_SP_HOLIDAYMANAGER
 * ALTERd By	: Nida Nazir
 * ALTERd On	: 10/12/2010
 * Purpose		: Manages (HOLIDAY) store procedure depending upon the action type.
 * Module		: CHRIS
 * Called By	: iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.HOLIDAYDAO
 * Comments		: None
 * -----------------------------------------------------------------------------------------------------------
///-------------------------------------Modification History--------------------------------------------------
	[Date]		[Author]			[Purpose]

///----------------------------------------------------------------------------------------------------------- */
 
ALTER PROCEDURE [dbo].[CHRIS_SP_HOLIDAY_MANAGER]
/*<Param>*/
@SP_HOL_DATE datetime=null, --<descr></descr>
@SP_H_DESC varchar(20)=null, --<descr></descr>
@ID INT =null  OUTPUT,  --<descr></descr>
@SP_HOL_DATE_string varchar(10)=null,
@ActionType varchar(50),
@MakerId varchar(50) = null,
@CheckerkID varchar(50) = null,
@AddedDatetime datetime = null,
@UpdatedDatetime datetime = null,
@IsAuth bit = null
,@oRetVal int = Null Output
/*</Param>*/
As

Set NoCount On
/*<Comment></Comment>*/


	If @ActionType = 'Save'
	Begin
		Exec CHRIS_SP_HOLIDAY_ADD
	   			@SP_HOL_DATE
	   			,@SP_H_DESC
	   			,@ID
				,@MakerId
				,@AddedDatetime
				,@IsAuth
				,@oRetVal = @oRetVal OUTPUT
			SET @ID =  @oRetVal
	End
	Else If @ActionType = 'Update'
	Begin
		Exec CHRIS_SP_HOLIDAY_UPDATE
	   			@SP_HOL_DATE
	   			,@SP_H_DESC
	   			,@ID
				,@MakerId
				,@AddedDatetime
				,@IsAuth
	End
	Else If @ActionType = 'Delete'
	Begin
		Exec CHRIS_SP_HOLIDAY_DELETE
				@ID,@oRetVal
	End
	Else If @ActionType = 'Get'
	Begin
		Exec CHRIS_SP_HOLIDAY_GET
				@ID,@oRetVal
	End
	Else If @ActionType = 'List'
	Begin
		Exec CHRIS_SP_HOLIDAY_GETALL @oRetVal
	End
	else if @ActionType = 'proc_view_add'
	begin
	SELECT
				 @sp_hol_date  =  sp_hol_date,
				 @sp_h_desc  =  sp_h_desc
		FROM  HOLIDAY 
		WHERE	 sp_hol_date  = @sp_hol_date
	end
 Else if @ActionType = 'Proc_mod_del'
    Begin
   	SELECT
				 @sp_hol_date  =  sp_hol_date,
				 @sp_h_desc  =  sp_h_desc
		FROM  HOLIDAY (UPDLOCK)  
		WHERE	 sp_hol_date  = @sp_hol_date
    End
	Else if @ActionType = 'HolidayLOV'
    Begin
--	select  CAST(CONVERT(VARCHAR(20),sp_hol_date,101) AS DATETIME) SP_HOL_DATE,SP_H_DESC  from holiday 
--	order by sp_hol_date
	select  CONVERT(VARCHAR(20),sp_hol_date,103) as SP_HOL_DATE,SP_H_DESC  from holiday 
	order by sp_hol_date

	End
	Else if @ActionType = 'HolidayLOVExists'
    Begin
		IF EXISTS (SELECT ID FROM holiday WHERE CAST(CONVERT(VARCHAR(10),sp_hol_date,101) AS DATETIME) =CAST(CONVERT(VARCHAR(10),@SP_HOL_DATE_string,101) AS DATETIME))
		BEGIN
--		select  CAST(CONVERT(VARCHAR(10),sp_hol_date,101) AS DATETIME) SP_HOL_DATE,SP_H_DESC  from holiday 
--		WHERE CAST(CONVERT(VARCHAR(10),sp_hol_date,101) AS DATETIME) = CAST(CONVERT(VARCHAR(10),@SP_HOL_DATE_string,101) AS DATETIME)
--		order by sp_hol_date
--		
		select CONVERT(VARCHAR(10),sp_hol_date,103) SP_HOL_DATE,SP_H_DESC from holiday 
		WHERE CONVERT(VARCHAR(10),sp_hol_date,103) = CONVERT(VARCHAR(10),@SP_HOL_DATE_string,103)
		order by sp_hol_date
		

		END
	End

	Else if @ActionType = 'LovDateExist'
	Begin
	
	---select  Count(*) as CountDate from holiday 
	---WHERE CAST(CONVERT(VARCHAR(10),sp_hol_date,101) AS DATETIME) =CAST(CONVERT(VARCHAR(10),@SP_HOL_DATE_string,101) AS DATETIME)
	select  Count(*) as CountDate from holiday 
	WHERE CONVERT(VARCHAR(10),sp_hol_date,103) = CONVERT(VARCHAR(10),@SP_HOL_DATE_string,103)

	End



	Else if @ActionType = 'RecordMatch'
	Begin
		select count(*) as HolCount from holiday 
		WHERE CONVERT(VARCHAR(20),sp_hol_date,103) = CONVERT(VARCHAR(20),@SP_HOL_DATE,103) 
--		select count(*) as HolCount from holiday 
--		WHERE year(sp_hol_date) = year(@SP_HOL_DATE) 
	End



	Else if @ActionType = 'FridayMatch'
	Begin
		---select datename(dw,SP_HOL_DATE) FROM HOLIDAY WHERE convert(varchar(20),SP_HOL_DATE,103) = CONVERT(VARCHAR(20),@SP_HOL_DATE,103); 
		SELECT CONVERT(VARCHAR(23), datename(dw,@SP_HOL_DATE),103) as DAYNAME
   End



   	ELSE IF @ACTIONTYPE = 'AUTHORIZE_SP_SH'      
	 BEGIN      
	  UPDATE HOLIDAY SET SH_IS_AUTH = 1, SH_CHECKER_ID = @CheckerkID,SH_UPDATED_DATETIME = @UpdatedDatetime WHERE ID = @ID     
	 END 

	 ELSE IF @ACTIONTYPE = 'REJECT_SP_SH'      
	 BEGIN      
	  DELETE FROM HOLIDAY WHERE ID = @ID     
	 END 

	ELSE IF @ACTIONTYPE = 'GET_UNAUTHORIZE_SP_SH'      
	 BEGIN

		 Select ID
	   	,CONVERT(VARCHAR, SP_HOL_DATE, 103) AS SP_HOL_DATE
	   	,SP_H_DESC
	From dbo.HOLIDAY WITH(NOLOCK) WHERE SH_IS_AUTH = 0 AND SH_MAKER_ID != @MakerId 
	order by  Year(SP_HOL_DATE) asc



	Return
	 END 


	Return
GO
/****** Object:  StoredProcedure [dbo].[CHRIS_SP_HOLIDAY_UPDATE]    Script Date: 01/02/2024 2:26:35 pm ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* -----------------------------------------------------------------------------------------------------------
 * SP Name		: CHRIS_SP_HOLIDAYUPDATE
 * ALTERd By	: Nida Nazir
 * ALTERd On	: 10/12/2010
 * Purpose		: Updates record(s) in the Table(HOLIDAY) according to the parameter(s).
 * Module		: CHRIS
 * Called By	: iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.HOLIDAY.InsertHOLIDAY
 * Comments		: None
 * -----------------------------------------------------------------------------------------------------------
///-------------------------------------Modification History--------------------------------------------------
	[Date]		[Author]			[Purpose]

///----------------------------------------------------------------------------------------------------------- */
 
ALTER PROCEDURE [dbo].[CHRIS_SP_HOLIDAY_UPDATE]
/*<Param>*/
@SP_HOL_DATE datetime, --<descr></descr>
@SP_H_DESC varchar(20), --<descr></descr>
@ID INT, --<descr></descr>
@MakerId varchar(50) = null,
@AddedDatetime datetime = null,
@IsAuth bit = null,
@oRetVal int = Null Output
/*</Param>*/
As

Set NoCount On
/*<Comment></Comment>*/
	Update	dbo.HOLIDAY
	Set
	   	SP_HOL_DATE = @SP_HOL_DATE
	   	,SP_H_DESC = @SP_H_DESC
		,SH_MAKER_ID = @MakerId
		,SH_ADDED_DATETIME = @AddedDatetime
		,SH_IS_AUTH = @IsAuth
	Where 	 ID = @ID	
	
	Set @oRetVal = -10 --Record updated successfully
	Return


	
GO
/****** Object:  StoredProcedure [dbo].[CHRIS_SP_INCOME_ADD]    Script Date: 01/02/2024 2:26:35 pm ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[CHRIS_SP_INCOME_ADD]
/*<Params>*/
@SP_AMT_FROM numeric(15,0), --<descr></descr>
@SP_AMT_TO numeric(15,0), --<descr></descr>
@SP_REBATE numeric(15,0), --<descr></descr>
@SP_PERCEN numeric(8,2), --<descr></descr>
@SP_TAX_AMT numeric(15,0), --<descr></descr>
@SP_SURCHARGE numeric(2,0), --<descr></descr>
@SP_FREBATE numeric(7,0), --<descr></descr>
@SP_FPERCEN numeric(4,2), --<descr></descr>
@SP_FTAX_AMT numeric(7,0), --<descr></descr>
@SP_FSURCHARGE numeric(2,0), --<descr></descr>
@SP_ADD_TAX numeric(7,0), --<descr></descr>
@SP_ADD_TAX_AMT numeric(7,0), --<descr></descr>
@TAX_RED_PER numeric(5,2), --<descr></descr>
@FTAX_RED_PER numeric(5,2), --<descr></descr>
@ID INT  OUTPUT,  --<descr></descr>
@MakerId varchar(50) = null,
@AddedDatetime datetime = null,
@IsAuth bit = null,
@oRetVal int = Null Output --<descr></descr>
/*</Params>*/
As

Set NoCount On
/*<Comments></Comments>*/
/*<LocalVariables>*/
/*</LocalVariables>*/
 
	Insert Into dbo.INCOME
	(
		SP_AMT_FROM
		,SP_AMT_TO
		,SP_REBATE
		,SP_PERCEN
		,SP_TAX_AMT
		,SP_SURCHARGE
		,SP_FREBATE
		,SP_FPERCEN
		,SP_FTAX_AMT
		,SP_FSURCHARGE
		,SP_ADD_TAX
		,SP_ADD_TAX_AMT
		,TAX_RED_PER
		,FTAX_RED_PER
		,INCOME_MAKER_ID
		,INCOME_ADDED_DATETIME
		,INCOME_IS_AUTH
	)
	Values
	(
		@SP_AMT_FROM
		,@SP_AMT_TO
		,@SP_REBATE
		,@SP_PERCEN
		,@SP_TAX_AMT
		,@SP_SURCHARGE
		,@SP_FREBATE
		,@SP_FPERCEN
		,@SP_FTAX_AMT
		,@SP_FSURCHARGE
		,@SP_ADD_TAX
		,@SP_ADD_TAX_AMT
		,@TAX_RED_PER
		,@FTAX_RED_PER
		,@MakerId
		,@AddedDatetime
		,@IsAuth
	)
	
	If @@RowCount > 0
	BEGIN
		Set @oRetVal = SCOPE_IDENTITY() --Record has been inserted successfully, return generated ID
		Set @ID = @oRetVal
	END
	Else
		Set @oRetVal = -40 --No record inserted
	
	Return



GO
/****** Object:  StoredProcedure [dbo].[CHRIS_SP_INCOME_GETALL]    Script Date: 01/02/2024 2:26:35 pm ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* -----------------------------------------------------------------------------------------------------------
 * SP Name		: CHRIS_SP_INCOME_GETALL
 * ALTERd By	: Faizan Ashraf
 * ALTERd On	: 25/01/2011
 * Purpose		: Retrieve entity(INCOME) records as per filter provided.
 * Module		: CHRIS
 * Called By	: iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.INCOME.GetINCOMERecords
 * Comments		: None
 * -----------------------------------------------------------------------------------------------------------
///-------------------------------------Modification History--------------------------------------------------
	[Date]		[Author]			[Purpose]

///----------------------------------------------------------------------------------------------------------- */
 
ALTER PROCEDURE [dbo].[CHRIS_SP_INCOME_GETALL]
/*<Param>*/
@oRetVal int Output --<descr></descr>
/*</Param>*/
As

Set NoCount On
/*<Comment></Comment>*/
	Select ID
	   	,SP_AMT_FROM
	   	,SP_AMT_TO
	   	,SP_REBATE
	   	,SP_PERCEN
	   	,SP_TAX_AMT
	   	,SP_SURCHARGE
	   	,SP_FREBATE
	   	,SP_FPERCEN
	   	,SP_FTAX_AMT
	   	,SP_FSURCHARGE
	   	,SP_ADD_TAX
	   	,SP_ADD_TAX_AMT
	   	,TAX_RED_PER
	   	,FTAX_RED_PER
	From dbo.INCOME WITH(NOLOCK) WHERE INCOME_IS_AUTH = 1
    ORDER BY SP_AMT_FROM
    

	If @@RowCount = 0 
		Set @oRetVal = -40	--Record not found
	Else
		Set @oRetVal = -10 	--Success
	
	Return
GO
/****** Object:  StoredProcedure [dbo].[CHRIS_SP_INCOME_MANAGER]    Script Date: 01/02/2024 2:26:35 pm ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[CHRIS_SP_INCOME_MANAGER]
/*<Param>*/
@SP_AMT_FROM numeric(15,0), --<descr></descr>
@SP_AMT_TO numeric(15,0), --<descr></descr>
@SP_REBATE numeric(15,0), --<descr></descr>
@SP_PERCEN numeric(8,2), --<descr></descr>
@SP_TAX_AMT numeric(15,0), --<descr></descr>
@SP_SURCHARGE numeric(8,0), --<descr></descr>
@SP_FREBATE numeric(7,0), --<descr></descr>
@SP_FPERCEN numeric(4,2), --<descr></descr>
@SP_FTAX_AMT numeric(7,0), --<descr></descr>
@SP_FSURCHARGE numeric(2,0), --<descr></descr>
@SP_ADD_TAX numeric(7,0), --<descr></descr>
@SP_ADD_TAX_AMT numeric(7,0), --<descr></descr>
@TAX_RED_PER numeric(5,2), --<descr></descr>
@FTAX_RED_PER numeric(5,2), --<descr></descr>
@ID INT  OUTPUT,  --<descr></descr>
@MakerId varchar(50) = null,
@CheckerkID varchar(50) = null,
@AddedDatetime datetime = null,
@UpdatedDatetime datetime = null,
@IsAuth bit = null,
@ActionType varchar(50)
,@oRetVal int = Null Output
/*</Param>*/
As
--select * from INCOME
Set NoCount On
/*<Comment></Comment>*/


	If @ActionType = 'Save'
	Begin
		Exec CHRIS_SP_INCOME_ADD
	   			@SP_AMT_FROM
	   			,@SP_AMT_TO
	   			,@SP_REBATE
	   			,@SP_PERCEN
	   			,@SP_TAX_AMT
	   			,@SP_SURCHARGE
	   			,@SP_FREBATE
	   			,@SP_FPERCEN
	   			,@SP_FTAX_AMT
	   			,@SP_FSURCHARGE
	   			,@SP_ADD_TAX
	   			,@SP_ADD_TAX_AMT
	   			,@TAX_RED_PER
	   			,@FTAX_RED_PER
	   			,@ID
				,@MakerId
				,@AddedDatetime
				,@IsAuth
				,@oRetVal = @oRetVal OUTPUT
			SET @ID =  @oRetVal
	End
	Else If @ActionType = 'Update'
	Begin
		Exec CHRIS_SP_INCOME_UPDATE
	   			@SP_AMT_FROM
	   			,@SP_AMT_TO
	   			,@SP_REBATE
	   			,@SP_PERCEN
	   			,@SP_TAX_AMT
	   			,@SP_SURCHARGE
	   			,@SP_FREBATE
	   			,@SP_FPERCEN
	   			,@SP_FTAX_AMT
	   			,@SP_FSURCHARGE
	   			,@SP_ADD_TAX
	   			,@SP_ADD_TAX_AMT
	   			,@TAX_RED_PER
	   			,@FTAX_RED_PER
	   			,@ID
				,@MakerId
				,@AddedDatetime
				,@IsAuth
	End
	Else If @ActionType = 'Delete'
	Begin
		Exec CHRIS_SP_INCOME_DELETE
				@ID,@oRetVal
	End
	Else If @ActionType = 'Get'
	Begin
		Exec CHRIS_SP_INCOME_GET
				@ID,@oRetVal
	End
	Else If @ActionType = 'List'
	Begin
		Exec CHRIS_SP_INCOME_GETALL @oRetVal 
	End
--------------------------------------------------------------------------------------------------------
Else if @actiontype= 'proc_delete' 
	Begin

declare @count_delete int 
	select @count_delete =count(*)
	   from   income
	   where  sp_amt_from = @sp_amt_from
	   and    sp_amt_to   = @sp_amt_to 
	   if (@count_delete > 0)
       begin	      
			  delete from income
				where  sp_amt_from = @sp_amt_from
				and    sp_amt_to   = @sp_amt_to
		  end
	End
--------------------------------------------------------------------------------------------------------

Else if @Actiontype= 'proc_modify'
begin
   declare @count_modify int 
      
      select  @count_modify = count(*)
      from   income
      where  sp_amt_from = @sp_amt_from
      and    sp_amt_to   = @sp_amt_to

   if (@count_modify > 0)
   begin
    
select @sp_amt_from  =  sp_amt_from from income   where  sp_amt_from = @sp_amt_from  and    sp_amt_to   = @sp_amt_to
select @sp_amt_to    =     sp_amt_to from income  where  sp_amt_from = @sp_amt_from  and    sp_amt_to   = @sp_amt_to
select @sp_rebate    =     sp_rebate from income  where  sp_amt_from = @sp_amt_from  and    sp_amt_to   = @sp_amt_to
select @sp_percen    =     sp_percen from income  where  sp_amt_from = @sp_amt_from  and    sp_amt_to   = @sp_amt_to
select @sp_tax_amt   =     sp_tax_amt from  income  where  sp_amt_from = @sp_amt_from  and    sp_amt_to   = @sp_amt_to
select @sp_surcharge =     sp_surcharge from income  where  sp_amt_from = @sp_amt_from  and    sp_amt_to   = @sp_amt_to
select @sp_frebate   =     sp_frebate from income where  sp_amt_from = @sp_amt_from  and    sp_amt_to   = @sp_amt_to
select @sp_fpercen   =     sp_fpercen from  income where  sp_amt_from = @sp_amt_from  and    sp_amt_to   = @sp_amt_to
select @sp_ftax_amt  =     sp_ftax_amt from  income where  sp_amt_from = @sp_amt_from  and    sp_amt_to   = @sp_amt_to
select @sp_fsurcharge =     sp_fsurcharge from income  where  sp_amt_from = @sp_amt_from  and    sp_amt_to   = @sp_amt_to
select @sp_add_tax    =     sp_add_tax from income  where   sp_amt_from = @sp_amt_from  and    sp_amt_to   = @sp_amt_to
select @sp_add_tax    =     sp_add_tax from income where  sp_amt_from = @sp_amt_from  and    sp_amt_to   = @sp_amt_to
	 
 
  
         update income
         set sp_amt_from   = @sp_amt_from,
             sp_amt_to     = @sp_amt_to,
             sp_rebate     = @sp_rebate,
             sp_percen     = @sp_percen,
             sp_tax_amt    = @sp_tax_amt,
             sp_surcharge  = @sp_surcharge,
             sp_frebate    = @sp_frebate,
             sp_fpercen    = @sp_fpercen,
             sp_ftax_amt   = @sp_ftax_amt,
             sp_fsurcharge = @sp_fsurcharge,
             sp_add_tax    = @sp_add_tax,
             sp_add_tax_amt= @sp_add_tax_amt

      end 


End
--------------------------------------------------------------------------------------------------------
Else if @actiontype = 'proc_mod_Del'
Begin
      select sp_amt_from,  sp_amt_to,
             sp_rebate,    sp_frebate,
             sp_percen,    sp_fpercen,
             sp_tax_amt,   sp_ftax_amt,
             sp_surcharge, sp_fsurcharge,
             sp_add_tax,   sp_add_tax_amt
      from   income
      where  sp_amt_from = @sp_amt_from
      and    sp_amt_to   = @sp_amt_to
End
--------------------------------------------------------------------------------------------------------
Else if @actiontype = 'proc_view_add'
Begin
   select sp_amt_from,sp_amt_to,sp_rebate,sp_percen,sp_tax_amt,
          sp_surcharge,sp_frebate,sp_fpercen,sp_ftax_amt,sp_fsurcharge,
          sp_add_tax,sp_add_tax_amt
    from   income
   where  sp_amt_from = @sp_amt_from
   and    sp_amt_to   = @sp_amt_to;
End
--------------------------------------------------------------------------------------------------------
Else if @Actiontype = 'Lov_Amt'
Begin
select 
substring(convert( varchar(20),sp_amt_from),1,7) SP_AMT_FROM
, sp_amt_to SP_AMT_TO
from income 
order by sp_amt_from
End
--------------------------------------------------------------------------------------------------------
Else if @Actiontype = 'Lov_Amt_Exists'
Begin
		If Exists (select Id from  income where SP_AMT_FROM = @SP_AMT_FROM)
       Begin
		select 
		substring(convert( varchar(20),sp_amt_from),1,7) SP_AMT_FROM
		, sp_amt_to SP_AMT_TO
		from income 
		 where SP_AMT_FROM = @SP_AMT_FROM
		order by sp_amt_from
      End


--     If Exists (select Id from  income where cast(SP_AMT_FROM as varchar) like cast(@SP_AMT_FROM as varchar) + '%')
--       Begin
--		select 
--		substring(convert( varchar(20),sp_amt_from),1,7) SP_AMT_FROM
--		, sp_amt_to SP_AMT_TO
--		from income 
--		 Where cast(SP_AMT_FROM as varchar) like cast(@SP_AMT_FROM as varchar) + '%'
--		order by sp_amt_from
--      End
   


End

------------------------------------------------------------------------------------------------------
ELSE IF @ACTIONTYPE = 'AUTHORIZE_ITSE'      
	 BEGIN      
	  UPDATE INCOME SET INCOME_IS_AUTH = 1, INCOME_CHECKER_ID = @CheckerkID,INCOME_UPDATED_DATETIME = @UpdatedDatetime WHERE ID = @ID     
	 END 

	 ELSE IF @ACTIONTYPE = 'REJECT_ITSE'      
	 BEGIN      
	  DELETE FROM INCOME WHERE ID = @ID     
	 END 

	ELSE IF @ACTIONTYPE = 'GET_UNAUTHORIZE_ITSE'      
	 BEGIN
		Select ID
	   	,SP_AMT_FROM
	   	,SP_AMT_TO
	   	,SP_REBATE
	   	,SP_PERCEN
	   	,SP_TAX_AMT
	   	,SP_SURCHARGE
	   	,SP_FREBATE
	   	,SP_FPERCEN
	   	,SP_FTAX_AMT
	   	,SP_FSURCHARGE
	   	,SP_ADD_TAX
	   	,SP_ADD_TAX_AMT
	   	,TAX_RED_PER
	   	,FTAX_RED_PER
	From dbo.INCOME WITH(NOLOCK) WHERE INCOME_IS_AUTH = 0 AND INCOME_MAKER_ID != @MakerId
	ORDER BY SP_AMT_FROM
	Return
	 END 





	Return

GO
/****** Object:  StoredProcedure [dbo].[CHRIS_SP_INCOME_UPDATE]    Script Date: 01/02/2024 2:26:35 pm ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[CHRIS_SP_INCOME_UPDATE]
/*<Param>*/
@SP_AMT_FROM numeric(15,0), --<descr></descr>
@SP_AMT_TO numeric(15,0), --<descr></descr>
@SP_REBATE numeric(15,0), --<descr></descr>
@SP_PERCEN numeric(8,2), --<descr></descr>
@SP_TAX_AMT numeric(15,0), --<descr></descr>
@SP_SURCHARGE numeric(2,0), --<descr></descr>
@SP_FREBATE numeric(7,0), --<descr></descr>
@SP_FPERCEN numeric(4,2), --<descr></descr>
@SP_FTAX_AMT numeric(7,0), --<descr></descr>
@SP_FSURCHARGE numeric(2,0), --<descr></descr>
@SP_ADD_TAX numeric(7,0), --<descr></descr>
@SP_ADD_TAX_AMT numeric(7,0), --<descr></descr>
@TAX_RED_PER numeric(5,2), --<descr></descr>
@FTAX_RED_PER numeric(5,2), --<descr></descr>
@ID INT, --<descr></descr>
@MakerId varchar(50) = null,
@AddedDatetime datetime = null,
@IsAuth bit = null,
@oRetVal int = Null Output
/*</Param>*/
As

Set NoCount On
/*<Comment></Comment>*/
	Update	dbo.INCOME
	Set
	   	SP_AMT_FROM = @SP_AMT_FROM
	   	,SP_AMT_TO = @SP_AMT_TO
	   	,SP_REBATE = @SP_REBATE
	   	,SP_PERCEN = @SP_PERCEN
	   	,SP_TAX_AMT = @SP_TAX_AMT
	   	,SP_SURCHARGE = @SP_SURCHARGE
	   	,SP_FREBATE = @SP_FREBATE
	   	,SP_FPERCEN = @SP_FPERCEN
	   	,SP_FTAX_AMT = @SP_FTAX_AMT
	   	,SP_FSURCHARGE = @SP_FSURCHARGE
	   	,SP_ADD_TAX = @SP_ADD_TAX
	   	,SP_ADD_TAX_AMT = @SP_ADD_TAX_AMT
	   	,TAX_RED_PER = @TAX_RED_PER
	   	,FTAX_RED_PER = @FTAX_RED_PER
		,INCOME_MAKER_ID = @MakerId
		,INCOME_ADDED_DATETIME = @AddedDatetime
		,INCOME_IS_AUTH = @IsAuth
	Where 	 ID = @ID	
	
	Set @oRetVal = -10 --Record updated successfully
	Return


GO
/****** Object:  StoredProcedure [dbo].[CHRIS_SP_LOAN_TAX_ADD]    Script Date: 01/02/2024 2:26:35 pm ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* -----------------------------------------------------------------------------------------------------------
 * SP Name		: CHRIS_SP_LOAN_TAXADD
 * ALTERd By	: Saad Saleem
 * ALTERd On	: 15/02/2011
 * Purpose		: Adds record(s) in the Table(LOAN_TAX) according to the parameter(s).
 * Module		: CHRISS
 * Called By	: iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.LOAN_TAX.InsertLOAN_TAX
 * Comments		: None
 * -----------------------------------------------------------------------------------------------------------
///-------------------------------------Modification History--------------------------------------------------
	[Date]		[Author]			[Purpose]

///----------------------------------------------------------------------------------------------------------- */

ALTER PROCEDURE [dbo].[CHRIS_SP_LOAN_TAX_ADD]
/*<Params>*/
@TAX_YEAR_FROM datetime, --<descr></descr>
@TAX_YEAR_TO datetime, --<descr></descr>
@TAX_PER numeric(3,1), --<descr></descr>
@ID INT  OUTPUT,  --<descr></descr>
@MakerId varchar(50) = null,
@AddedDatetime datetime = null,
@IsAuth bit = null,
@oRetVal int = Null Output --<descr></descr>
/*</Params>*/
As

Set NoCount On
/*<Comments></Comments>*/
/*<LocalVariables>*/
/*</LocalVariables>*/
 
	Insert Into dbo.LOAN_TAX
	(
		TAX_YEAR_FROM
		,TAX_YEAR_TO
		,TAX_PER
		,LT_MAKER_ID
		,LT_ADDED_DATETIME
		,LT_IS_AUTH
	)
	Values
	(
		@TAX_YEAR_FROM
		,@TAX_YEAR_TO
		,@TAX_PER
		,@MakerId
		,@AddedDatetime
		,@IsAuth
	)
	
	If @@RowCount > 0
	BEGIN
		Set @oRetVal = SCOPE_IDENTITY() --Record has been inserted successfully, return generated ID
		Set @ID = @oRetVal
	END
	Else
		Set @oRetVal = -40 --No record inserted
	
	Return
GO
/****** Object:  StoredProcedure [dbo].[CHRIS_SP_LOAN_TAX_GETALL]    Script Date: 01/02/2024 2:26:35 pm ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* -----------------------------------------------------------------------------------------------------------
 * SP Name		: CHRIS_SP_LOAN_TAX_GETALL
 * ALTERd By	: Saad Saleem
 * ALTERd On	: 15/02/2011
 * Purpose		: Retrieve entity(LOAN_TAX) records as per filter provided.
 * Module		: CHRISS
 * Called By	: iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.LOAN_TAX.GetLOAN_TAXRecords
 * Comments		: None
 * -----------------------------------------------------------------------------------------------------------
///-------------------------------------Modification History--------------------------------------------------
	[Date]		[Author]			[Purpose]

///----------------------------------------------------------------------------------------------------------- */
 
ALTER PROCEDURE [dbo].[CHRIS_SP_LOAN_TAX_GETALL]
/*<Param>*/
@oRetVal int Output --<descr></descr>
/*</Param>*/
As

Set NoCount On
/*<Comment></Comment>*/
	Select ID
	   	,TAX_YEAR_FROM
	   	,TAX_YEAR_TO
	   	,TAX_PER
	From dbo.LOAN_TAX WITH(NOLOCK) WHERE LT_IS_AUTH = 1



	If @@RowCount = 0 
		Set @oRetVal = -40	--Record not found
	Else
		Set @oRetVal = -10 	--Success
	
	Return
GO
/****** Object:  StoredProcedure [dbo].[CHRIS_SP_LOAN_TAX_MANAGER]    Script Date: 01/02/2024 2:26:35 pm ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* -----------------------------------------------------------------------------------------------------------
 * SP Name		: CHRIS_SP_LOAN_TAXMANAGER
 * ALTERd By	: Saad Saleem
 * ALTERd On	: 15/02/2011
 * Purpose		: Manages (LOAN_TAX) store procedure depending upon the action type.
 * Module		: CHRISS
 * Called By	: iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.LOAN_TAXDAO
 * Comments		: None
 * -----------------------------------------------------------------------------------------------------------
///-------------------------------------Modification History--------------------------------------------------
	[Date]		[Author]			[Purpose]

///----------------------------------------------------------------------------------------------------------- */
 
ALTER PROCEDURE [dbo].[CHRIS_SP_LOAN_TAX_MANAGER]
/*<Param>*/
@TAX_YEAR_FROM datetime, --<descr></descr>
@TAX_YEAR_TO datetime, --<descr></descr>
@TAX_PER numeric(3,1), --<descr></descr>
@ID INT  OUTPUT,  --<descr></descr>
@ActionType varchar(50),
@MakerId varchar(50) = null,
@CheckerkID varchar(50) = null,
@AddedDatetime datetime = null,
@UpdatedDatetime datetime = null,
@IsAuth bit = null
,@oRetVal int = Null Output
/*</Param>*/
As

Set NoCount On
/*<Comment></Comment>*/


	If @ActionType = 'Save'
	Begin
		Exec CHRIS_SP_LOAN_TAX_ADD
	   			@TAX_YEAR_FROM
	   			,@TAX_YEAR_TO
	   			,@TAX_PER
	   			,@ID
				,@MakerId
				,@AddedDatetime
				,@IsAuth
				,@oRetVal = @oRetVal OUTPUT
			SET @ID =  @oRetVal
	End
	Else If @ActionType = 'Update'
	Begin
		Exec CHRIS_SP_LOAN_TAX_UPDATE
	   			@TAX_YEAR_FROM
	   			,@TAX_YEAR_TO
	   			,@TAX_PER
	   			,@ID
				,@MakerId
				,@AddedDatetime
				,@IsAuth
	End
	Else If @ActionType = 'Delete'
	Begin
		Exec CHRIS_SP_LOAN_TAX_DELETE
				@ID,@oRetVal
	End
	Else If @ActionType = 'Get'
	Begin
		Exec CHRIS_SP_LOAN_TAX_GET
				@ID,@oRetVal
	End
	Else If @ActionType = 'List'
	Begin
		Exec CHRIS_SP_LOAN_TAX_GETALL @oRetVal
	END
	
	Else If @ActionType = 'EnterQuery'
	Begin
		Exec CHRIS_SP_LOAN_TAX_ENTERQUERY @TAX_YEAR_FROM,@TAX_YEAR_TO,@TAX_PER,@oRetVal OUTPUT
	End

	ELSE IF @ACTIONTYPE = 'AUTHORIZE_LSBM'      
	 BEGIN      
	  UPDATE LOAN_TAX SET LT_IS_AUTH = 1, LT_CHECKER_ID = @CheckerkID,LT_UPDATED_DATETIME = @UpdatedDatetime WHERE ID = @ID     
	 END 

	 ELSE IF @ACTIONTYPE = 'REJECT_LSBM'      
	 BEGIN      
	  DELETE FROM LOAN_TAX WHERE ID = @ID     
	 END 

	ELSE IF @ACTIONTYPE = 'GET_UNAUTHORIZE_LSBM'      
	 BEGIN
		Select ID
	   	,TAX_YEAR_FROM
	   	,TAX_YEAR_TO
	   	,TAX_PER
	From dbo.LOAN_TAX WITH(NOLOCK)  WHERE LT_IS_AUTH = 0 AND LT_MAKER_ID != @MakerId
	Return
	 END 

	Return
GO
/****** Object:  StoredProcedure [dbo].[CHRIS_SP_LOAN_TAX_UPDATE]    Script Date: 01/02/2024 2:26:35 pm ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* -----------------------------------------------------------------------------------------------------------
 * SP Name		: CHRIS_SP_LOAN_TAXUPDATE
 * ALTERd By	: Saad Saleem
 * ALTERd On	: 15/02/2011
 * Purpose		: Updates record(s) in the Table(LOAN_TAX) according to the parameter(s).
 * Module		: CHRISS
 * Called By	: iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.LOAN_TAX.InsertLOAN_TAX
 * Comments		: None
 * -----------------------------------------------------------------------------------------------------------
///-------------------------------------Modification History--------------------------------------------------
	[Date]		[Author]			[Purpose]

///----------------------------------------------------------------------------------------------------------- */
 
ALTER PROCEDURE [dbo].[CHRIS_SP_LOAN_TAX_UPDATE]
/*<Param>*/
@TAX_YEAR_FROM datetime, --<descr></descr>
@TAX_YEAR_TO datetime, --<descr></descr>
@TAX_PER numeric(3,1), --<descr></descr>
@ID INT, --<descr></descr>
@MakerId varchar(50) = null,
@AddedDatetime datetime = null,
@IsAuth bit = null,
@oRetVal int = Null Output
/*</Param>*/
As

Set NoCount On
/*<Comment></Comment>*/
	Update	dbo.LOAN_TAX
	Set
	   	TAX_YEAR_FROM = @TAX_YEAR_FROM
	   	,TAX_YEAR_TO = @TAX_YEAR_TO
	   	,TAX_PER = @TAX_PER
		,LT_MAKER_ID = @MakerId
		,LT_ADDED_DATETIME = @AddedDatetime
		,LT_IS_AUTH = @IsAuth
	Where 	 ID = @ID	
	
	Set @oRetVal = -10 --Record updated successfully
	Return
GO
/****** Object:  StoredProcedure [dbo].[CHRIS_SP_MARGINAL_TAX_ADD]    Script Date: 01/02/2024 2:26:35 pm ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* -----------------------------------------------------------------------------------------------------------
 * SP Name		: CHRIS_SP_MARGINAL_TAXADD
 * ALTERd By	: Saad Saleem
 * ALTERd On	: 15/02/2011
 * Purpose		: Adds record(s) in the Table(MARGINAL_TAX) according to the parameter(s).
 * Module		: CHRISS
 * Called By	: iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.MARGINAL_TAX.InsertMARGINAL_TAX
 * Comments		: None
 * -----------------------------------------------------------------------------------------------------------
///-------------------------------------Modification History--------------------------------------------------
	[Date]		[Author]			[Purpose]

///----------------------------------------------------------------------------------------------------------- */

ALTER PROCEDURE [dbo].[CHRIS_SP_MARGINAL_TAX_ADD]
/*<Params>*/
@MT_DATE_FROM datetime, --<descr></descr>
@MT_DATE_TO datetime, --<descr></descr>
@MT_AMT_FROM numeric(10,0), --<descr></descr>
@MT_AMT_TO numeric(10,0), --<descr></descr>
@MT_PERCENTAGE numeric(5,2), --<descr></descr>
@ID INT  OUTPUT,  --<descr></descr>
@MakerId varchar(50) = null,
@AddedDatetime datetime = null,
@IsAuth bit = null,
@oRetVal int = Null Output --<descr></descr>
/*</Params>*/
As

Set NoCount On
/*<Comments></Comments>*/
/*<LocalVariables>*/
/*</LocalVariables>*/
 
	Insert Into dbo.MARGINAL_TAX
	(
		MT_DATE_FROM
		,MT_DATE_TO
		,MT_AMT_FROM
		,MT_AMT_TO
		,MT_PERCENTAGE
		,MT_MAKER_ID
		,MT_ADDED_DATETIME
		,MT_IS_AUTH
	)
	Values
	(
		@MT_DATE_FROM
		,@MT_DATE_TO
		,@MT_AMT_FROM
		,@MT_AMT_TO
		,@MT_PERCENTAGE
		,@MakerId
		,@AddedDatetime
		,@IsAuth
	)
	
	If @@RowCount > 0
	BEGIN
		Set @oRetVal = SCOPE_IDENTITY() --Record has been inserted successfully, return generated ID
		Set @ID = @oRetVal
	END
	Else
		Set @oRetVal = -40 --No record inserted
	
	Return

GO
/****** Object:  StoredProcedure [dbo].[CHRIS_SP_MARGINAL_TAX_GETALL]    Script Date: 01/02/2024 2:26:35 pm ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* -----------------------------------------------------------------------------------------------------------
 * SP Name		: CHRIS_SP_MARGINAL_TAX_GETALL
 * ALTERd By	: Saad Saleem
 * ALTERd On	: 15/02/2011
 * Purpose		: Retrieve entity(MARGINAL_TAX) records as per filter provided.
 * Module		: CHRISS
 * Called By	: iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.MARGINAL_TAX.GetMARGINAL_TAXRecords
 * Comments		: None
 * -----------------------------------------------------------------------------------------------------------
///-------------------------------------Modification History--------------------------------------------------
	[Date]		[Author]			[Purpose]

///----------------------------------------------------------------------------------------------------------- */
 
ALTER PROCEDURE [dbo].[CHRIS_SP_MARGINAL_TAX_GETALL]
/*<Param>*/
@oRetVal int Output --<descr></descr>
/*</Param>*/
As

Set NoCount On
/*<Comment></Comment>*/
	Select ID
	   	,MT_DATE_FROM
	   	,MT_DATE_TO
	   	,MT_AMT_FROM
	   	,MT_AMT_TO
	   	,MT_PERCENTAGE
	From dbo.MARGINAL_TAX WITH(NOLOCK) WHERE MT_IS_AUTH = 1
	order by mt_date_from, mt_amt_from 


	If @@RowCount = 0 
		Set @oRetVal = -40	--Record not found
	Else
		Set @oRetVal = -10 	--Success
	
	Return

GO
/****** Object:  StoredProcedure [dbo].[CHRIS_SP_MARGINAL_TAX_MANAGER]    Script Date: 01/02/2024 2:26:35 pm ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* -----------------------------------------------------------------------------------------------------------
 * SP Name		: CHRIS_SP_MARGINAL_TAXMANAGER
 * ALTERd By	: Saad Saleem
 * ALTERd On	: 15/02/2011
 * Purpose		: Manages (MARGINAL_TAX) store procedure depending upon the action type.
 * Module		: CHRISS
 * Called By	: iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.MARGINAL_TAXDAO
 * Comments		: None
 * -----------------------------------------------------------------------------------------------------------
///-------------------------------------Modification History--------------------------------------------------
	[Date]		[Author]			[Purpose]

///----------------------------------------------------------------------------------------------------------- */
 
ALTER PROCEDURE [dbo].[CHRIS_SP_MARGINAL_TAX_MANAGER]
/*<Param>*/
@MT_DATE_FROM DATETIME=NULL, --<descr></descr>
@MT_DATE_TO datetime=NULL, --<descr></descr>
@MT_AMT_FROM numeric(10,0)=NULL, --<descr></descr>
@MT_AMT_TO numeric(10,0)=NULL, --<descr></descr>
@MT_PERCENTAGE numeric(5,2)=NULL, --<descr></descr>
@ID INT =NULL OUTPUT,  --<descr></descr>
@ActionType varchar(50)=NULL,
@MakerId varchar(50) = null,
@CheckerkID varchar(50) = null,
@AddedDatetime datetime = null,
@UpdatedDatetime datetime = null,
@IsAuth bit = null
,@oRetVal int = Null Output
/*</Param>*/
As

Set NoCount On
/*<Comment></Comment>*/


	If @ActionType = 'Save'
	Begin
		Exec CHRIS_SP_MARGINAL_TAX_ADD
	   			@MT_DATE_FROM
	   			,@MT_DATE_TO
	   			,@MT_AMT_FROM
	   			,@MT_AMT_TO
	   			,@MT_PERCENTAGE
	   			,@ID
				,@MakerId
				,@AddedDatetime
				,@IsAuth
				,@oRetVal = @oRetVal OUTPUT
			SET @ID =  @oRetVal
	End
	Else If @ActionType = 'Update'
	Begin
		Exec CHRIS_SP_MARGINAL_TAX_UPDATE
	   			@MT_DATE_FROM
	   			,@MT_DATE_TO
	   			,@MT_AMT_FROM
	   			,@MT_AMT_TO
	   			,@MT_PERCENTAGE
	   			,@ID
				,@MakerId
				,@AddedDatetime
				,@IsAuth
	End
	Else If @ActionType = 'Delete'
	Begin
		Exec CHRIS_SP_MARGINAL_TAX_DELETE
				@ID,@oRetVal
	End
	Else If @ActionType = 'Get'
	Begin
		Exec CHRIS_SP_MARGINAL_TAX_GET
				@ID,@oRetVal
	End
	Else If @ActionType = 'List'
	Begin
		Exec CHRIS_SP_MARGINAL_TAX_GETALL @oRetVal
	End
	ELSE IF @ACTIONTYPE = 'AUTHORIZE_MARGIN_TAX'      
	 BEGIN      
	  UPDATE MARGINAL_TAX SET MT_IS_AUTH = 1, MT_CHECKER_ID = @CheckerkID,MT_UPDATED_DATETIME = @UpdatedDatetime WHERE ID = @ID     
	 END 

	 ELSE IF @ACTIONTYPE = 'REJECT_MARGIN_TAX'      
	 BEGIN      
	  DELETE FROM MARGINAL_TAX WHERE ID = @ID     
	 END 

	ELSE IF @ACTIONTYPE = 'GET_UNAUTHORIZE_MARGIN_TAX'      
	 BEGIN
		Select ID
	   	,MT_DATE_FROM
	   	,MT_DATE_TO
	   	,MT_AMT_FROM
	   	,MT_AMT_TO
	   	,MT_PERCENTAGE
	From dbo.MARGINAL_TAX WITH(NOLOCK) WHERE MT_IS_AUTH = 0 AND MT_MAKER_ID != @MakerId
	order by mt_date_from, mt_amt_from 
	Return
	 END 

	Return

GO
/****** Object:  StoredProcedure [dbo].[CHRIS_SP_MARGINAL_TAX_UPDATE]    Script Date: 01/02/2024 2:26:35 pm ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* -----------------------------------------------------------------------------------------------------------
 * SP Name		: CHRIS_SP_MARGINAL_TAXUPDATE
 * ALTERd By	: Saad Saleem
 * ALTERd On	: 15/02/2011
 * Purpose		: Updates record(s) in the Table(MARGINAL_TAX) according to the parameter(s).
 * Module		: CHRISS
 * Called By	: iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.MARGINAL_TAX.InsertMARGINAL_TAX
 * Comments		: None
 * -----------------------------------------------------------------------------------------------------------
///-------------------------------------Modification History--------------------------------------------------
	[Date]		[Author]			[Purpose]

///----------------------------------------------------------------------------------------------------------- */
 
ALTER PROCEDURE [dbo].[CHRIS_SP_MARGINAL_TAX_UPDATE]
/*<Param>*/
@MT_DATE_FROM datetime, --<descr></descr>
@MT_DATE_TO datetime, --<descr></descr>
@MT_AMT_FROM numeric(10,0), --<descr></descr>
@MT_AMT_TO numeric(10,0), --<descr></descr>
@MT_PERCENTAGE numeric(5,2), --<descr></descr>
@ID INT, --<descr></descr>
@MakerId varchar(50) = null,
@AddedDatetime datetime = null,
@IsAuth bit = null,
@oRetVal int = Null Output
/*</Param>*/
As

Set NoCount On
/*<Comment></Comment>*/
	Update	dbo.MARGINAL_TAX
	Set
	   	MT_DATE_FROM = @MT_DATE_FROM
	   	,MT_DATE_TO = @MT_DATE_TO
	   	,MT_AMT_FROM = @MT_AMT_FROM
	   	,MT_AMT_TO = @MT_AMT_TO
	   	,MT_PERCENTAGE = @MT_PERCENTAGE
		,MT_MAKER_ID = @MakerId
		,MT_ADDED_DATETIME = @AddedDatetime
		,MT_IS_AUTH = @IsAuth
	Where 	 ID = @ID	
	
	Set @oRetVal = -10 --Record updated successfully
	Return

GO
/****** Object:  StoredProcedure [dbo].[CHRIS_SP_SETUP_ALLOWANCE_ADD]    Script Date: 01/02/2024 2:26:35 pm ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* -----------------------------------------------------------------------------------------------------------
 * SP Name		: CHRIS_SP_SETUP_ALLOWANCEADD
 * ALTERd By	: Nida Nazir
 * ALTERd On	: 29/01/2011
 * Purpose		: Adds record(s) in the Table(ALLOWANCE) according to the parameter(s).
 * Module		: CHRIS
 * Called By	: iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.ALLOWANCE.InsertALLOWANCE
 * Comments		: None
 * -----------------------------------------------------------------------------------------------------------
///-------------------------------------Modification History--------------------------------------------------
	[Date]		[Author]			[Purpose]

///----------------------------------------------------------------------------------------------------------- */

ALTER PROCEDURE [dbo].[CHRIS_SP_SETUP_ALLOWANCE_ADD]
/*<Params>*/
@SP_ALL_CODE varchar(3), --<descr></descr>
@SP_BRANCH_A varchar(3), --<descr></descr>
@SP_DESC varchar(30), --<descr></descr>
@SP_CATEGORY_A varchar(1), --<descr></descr>
@SP_DESG_A varchar(3), --<descr></descr>
@SP_LEVEL_A varchar(3), --<descr></descr>
@SP_VALID_FROM datetime, --<descr></descr>
@SP_VALID_TO datetime, --<descr></descr>
@SP_ALL_AMOUNT numeric(6,0), --<descr></descr>
@SP_PER_ASR numeric(2,0), --<descr></descr>
@SP_TAX varchar(1), --<descr></descr>
@SP_ATT_RELATED varchar(1), --<descr></descr>
@SP_RELATED_LEAV varchar(1), --<descr></descr>
@SP_ASR_BASIC varchar(1), --<descr></descr>
@SP_MEAL_CONV varchar(1), --<descr></descr>
@SP_ALL_IND varchar(1), --<descr></descr>
@SP_ACOUNT_NO varchar(11), --<descr></descr>
@SP_FORECAST_TAX varchar(1), --<descr></descr>
@ID INT  OUTPUT,  --<descr></descr>
@MakerId varchar(50) = null,
@AddedDatetime datetime = null,
@IsAuth bit = null,
@oRetVal int = Null Output --<descr></descr>
/*</Params>*/
As

Set NoCount On
/*<Comments></Comments>*/
/*<LocalVariables>*/
/*</LocalVariables>*/
 
	Insert Into dbo.ALLOWANCE
	(
		SP_ALL_CODE
		,SP_BRANCH_A
		,SP_DESC
		,SP_CATEGORY_A
		,SP_DESG_A
		,SP_LEVEL_A
		,SP_VALID_FROM
		,SP_VALID_TO
		,SP_ALL_AMOUNT
		,SP_PER_ASR
		,SP_TAX
		,SP_ATT_RELATED
		,SP_RELATED_LEAV
		,SP_ASR_BASIC
		,SP_MEAL_CONV
		,SP_ALL_IND
		,SP_ACOUNT_NO
		,SP_FORECAST_TAX
		,ALLOWANCE_MAKER_ID
		,ALLOWANCE_ADDED_DATETIME
		,ALLOWANCE_IS_AUTH
	)
	Values
	(
		@SP_ALL_CODE
		,@SP_BRANCH_A
		,@SP_DESC
		,@SP_CATEGORY_A
		,@SP_DESG_A
		,@SP_LEVEL_A
		,@SP_VALID_FROM
		,@SP_VALID_TO
		,@SP_ALL_AMOUNT
		,@SP_PER_ASR
		,@SP_TAX
		,@SP_ATT_RELATED
		,@SP_RELATED_LEAV
		,@SP_ASR_BASIC
		,@SP_MEAL_CONV
		,@SP_ALL_IND
		,@SP_ACOUNT_NO
		,@SP_FORECAST_TAX
		,@MakerId
		,@AddedDatetime
		,@IsAuth
	)
	
	If @@RowCount > 0
	BEGIN
		Set @oRetVal = SCOPE_IDENTITY() --Record has been inserted successfully, return generated ID
		Set @ID = @oRetVal
	END
	Else
		Set @oRetVal = -40 --No record inserted
	
	Return
GO
/****** Object:  StoredProcedure [dbo].[CHRIS_SP_SETUP_ALLOWANCE_DETAILS_GETALL]    Script Date: 01/02/2024 2:26:35 pm ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* -----------------------------------------------------------------------------------------------------------
 * SP Name		: CHRIS_SP_SETUP_ALLOWANCE_DETAILS_GETALL
 * ALTERd By	: Nida Nazir
 * ALTERd On	: 31/01/2011
 * Purpose		: Retrieve entity(ALLOWANCE_DETAILS) records as per filter provided.
 * Module		: CHRIS
 * Called By	: iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.ALLOWANCE_DETAILS.GetALLOWANCE_DETAILSRecords
 * Comments		: None
 * -----------------------------------------------------------------------------------------------------------
///-------------------------------------Modification History--------------------------------------------------
	[Date]		[Author]			[Purpose]

///----------------------------------------------------------------------------------------------------------- */
 
ALTER PROCEDURE [dbo].[CHRIS_SP_SETUP_ALLOWANCE_DETAILS_GETALL]
/*<Param>*/
@SP_ALLOW_CODE varchar(3),
@oRetVal int Output --<descr></descr>
/*</Param>*/
As

Set NoCount On
/*<Comment></Comment>*/
	Select ALLOWANCE_DETAILS.ID
	   	,SP_ALLOW_CODE SP_ALL_CODE
	   	,SP_P_NO
	   	,SP_REMARKS
	   	,SP_ALL_AMT
		,ISNULL(PR_FIRST_NAME,'') + ' ' + ISNULL(PR_LAST_NAME,'') as Name
	From dbo.ALLOWANCE_DETAILS WITH(NOLOCK)
INNER JOIN PERSONNEL
ON PERSONNEL.PR_P_NO=ALLOWANCE_DETAILS.SP_P_NO

WHERE SP_ALLOW_CODE=@SP_ALLOW_CODE


	If @@RowCount = 0 
		Set @oRetVal = -40	--Record not found
	Else
		Set @oRetVal = -10 	--Success
	
	Return
GO
/****** Object:  StoredProcedure [dbo].[CHRIS_SP_SETUP_ALLOWANCE_DETAILS_MANAGER]    Script Date: 01/02/2024 2:26:35 pm ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* -----------------------------------------------------------------------------------------------------------
 * SP Name		: CHRIS_SP_SETUP_ALLOWANCE_DETAILSMANAGER
 * ALTERd By	: Nida Nazir
 * ALTERd On	: 31/01/2011
 * Purpose		: Manages (ALLOWANCE_DETAILS) store procedure depending upon the action type.
 * Module		: CHRIS
 * Called By	: iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.ALLOWANCE_DETAILSDAO
 * Comments		: None
 * -----------------------------------------------------------------------------------------------------------
///-------------------------------------Modification History--------------------------------------------------
	[Date]		[Author]			[Purpose]

///----------------------------------------------------------------------------------------------------------- */
 
ALTER PROCEDURE [dbo].[CHRIS_SP_SETUP_ALLOWANCE_DETAILS_MANAGER]
/*<Param>*/
@SP_ALL_CODE varchar(3), --<descr></descr>
@SP_P_NO numeric(6,0), --<descr></descr>
@SP_REMARKS varchar(30), --<descr></descr>
@SP_ALL_AMT numeric(12,2), --<descr></descr>
@ID INT  OUTPUT,  --<descr></descr>
@ActionType varchar(50)
,@oRetVal int = Null Output
/*</Param>*/
As

Set NoCount On
/*<Comment></Comment>*/


	If @ActionType = 'Save'
	Begin
		Exec CHRIS_SP_SETUP_ALLOWANCE_DETAILS_ADD
	   			@SP_ALL_CODE
	   			,@SP_P_NO
	   			,@SP_REMARKS
	   			,@SP_ALL_AMT
	   			,@ID
				,@oRetVal = @oRetVal OUTPUT
			SET @ID =  @oRetVal
	End
	Else If @ActionType = 'Update'
	Begin
		Exec CHRIS_SP_SETUP_ALLOWANCE_DETAILS_UPDATE
	   			@SP_ALL_CODE
	   			,@SP_P_NO
	   			,@SP_REMARKS
	   			,@SP_ALL_AMT
	   			,@ID
	End
	Else If @ActionType = 'Delete'
	Begin
		Exec CHRIS_SP_SETUP_ALLOWANCE_DETAILS_DELETE
				@ID,@oRetVal
	End
	Else If @ActionType = 'Get'
	Begin
		Exec CHRIS_SP_SETUP_ALLOWANCE_DETAILS_GET
				@ID,@oRetVal
	End
	Else If @ActionType = 'List'
	Begin
		Exec CHRIS_SP_SETUP_ALLOWANCE_DETAILS_GETALL @SP_ALL_CODE,@oRetVal
	End
	Else If @ActionType = 'Pr_PNO_LOV'
	Begin
	SELECT
			 CAST(RTRIM(pr_first_name) AS VARCHAR) + ' ' + CAST(RTRIM(pr_last_name) AS VARCHAR) PR_Name,
			 pr_p_no as SP_P_NO
	FROM  personnel 
	WHERE	 pr_close_flag  is null
	 AND	pr_termin_date  is null
	 AND	(pr_transfer  <= 5
	 OR	pr_transfer  is null)
	
	End

	Else If @ActionType = 'Pr_PNO_LOVExists'
	Begin
	IF EXISTS(Select ID FROM  personnel  WHERE pr_p_no = @SP_P_NO )  
	BEGIN 
			SELECT
			 CAST(RTRIM(pr_first_name) AS VARCHAR) + ' ' + CAST(RTRIM(pr_last_name) AS VARCHAR) PR_Name,
			 pr_p_no as SP_P_NO
	FROM  personnel 
	WHERE	 pr_close_flag  is null
	 AND	pr_termin_date  is null
	 AND	(pr_transfer  <= 5
	 OR	pr_transfer  is null)
	AND  pr_p_no = @SP_P_NO
		
	End
    IF EXISTS(Select ID FROM  personnel  WHERE  CAST (pr_p_no AS VARCHAR) LIKE  CAST(@SP_P_NO AS VARCHAR) + '%' OR  ISNULL(CAST(@SP_P_NO AS VARCHAR),'') = '')   
		BEGIN 
		SELECT
			 CAST(RTRIM(pr_first_name) AS VARCHAR) + ' ' + CAST(RTRIM(pr_last_name) AS VARCHAR) PR_Name,
			 pr_p_no as SP_P_NO
	FROM  personnel 
	WHERE	 pr_close_flag  is null
	 AND	pr_termin_date  is null
	 AND	(pr_transfer  <= 5
	 OR	pr_transfer  is null)
	AND    (CAST(pr_p_no AS VARCHAR) LIKE  CAST(@SP_P_NO AS VARCHAR) + '%')
	
		
                 
		End
	End


	Return
GO
/****** Object:  StoredProcedure [dbo].[CHRIS_SP_SETUP_ALLOWANCE_DETAILS_UPDATE]    Script Date: 01/02/2024 2:26:35 pm ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* -----------------------------------------------------------------------------------------------------------
 * SP Name		: CHRIS_SP_SETUP_ALLOWANCE_DETAILSUPDATE
 * ALTERd By	: Nida Nazir
 * ALTERd On	: 31/01/2011
 * Purpose		: Updates record(s) in the Table(ALLOWANCE_DETAILS) according to the parameter(s).
 * Module		: CHRIS
 * Called By	: iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.ALLOWANCE_DETAILS.InsertALLOWANCE_DETAILS
 * Comments		: None
 * -----------------------------------------------------------------------------------------------------------
///-------------------------------------Modification History--------------------------------------------------
	[Date]		[Author]			[Purpose]

///----------------------------------------------------------------------------------------------------------- */
 
ALTER PROCEDURE [dbo].[CHRIS_SP_SETUP_ALLOWANCE_DETAILS_UPDATE]
/*<Param>*/
@SP_ALLOW_CODE varchar(3), --<descr></descr>
@SP_P_NO numeric(6,0), --<descr></descr>
@SP_REMARKS varchar(30), --<descr></descr>
@SP_ALL_AMT numeric(12,2), --<descr></descr>
@ID INT, --<descr></descr>
@oRetVal int = Null Output
/*</Param>*/
As

Set NoCount On
/*<Comment></Comment>*/
	Update	dbo.ALLOWANCE_DETAILS
	Set
	   	SP_ALLOW_CODE = @SP_ALLOW_CODE
	   	,SP_P_NO = @SP_P_NO
	   	,SP_REMARKS = @SP_REMARKS
	   	,SP_ALL_AMT = @SP_ALL_AMT
	Where 	 ID = @ID	
	
	Set @oRetVal = -10 --Record updated successfully
	Return
GO
/****** Object:  StoredProcedure [dbo].[CHRIS_SP_Setup_ATTR_CATEGORY_ADD]    Script Date: 01/02/2024 2:26:35 pm ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* -----------------------------------------------------------------------------------------------------------
 * SP Name		: CHRIS_SP_Setup_ATTR_CATEGORYADD
 * ALTERd By	: Tahnia
 * ALTERd On	: 24/01/2011
 * Purpose		: Adds record(s) in the Table(ATTR_CATEGORY) according to the parameter(s).
 * Module		: CHRIS
 * Called By	: iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.ATTR_CATEGORY.InsertATTR_CATEGORY
 * Comments		: None
 * -----------------------------------------------------------------------------------------------------------
///-------------------------------------Modification History--------------------------------------------------
	[Date]		[Author]			[Purpose]

///----------------------------------------------------------------------------------------------------------- */

ALTER PROCEDURE [dbo].[CHRIS_SP_Setup_ATTR_CATEGORY_ADD]
/*<Params>*/
@ATTR_CATEGORY_CODE numeric(3,0), --<descr></descr>
@ATTR_CATEGORY_DESC varchar(20), --<descr></descr>
@ID INT  OUTPUT,  --<descr></descr>
@MakerId varchar(50) = null,
@AddedDatetime datetime = null,
@IsAuth bit = null,
@oRetVal int = Null Output --<descr></descr>
/*</Params>*/
As

Set NoCount On
/*<Comments></Comments>*/
/*<LocalVariables>*/
/*</LocalVariables>*/

SELECT @ATTR_CATEGORY_CODE=ISNULL(MAX(ATTR_CATEGORY_CODE),0)+1 
FROM ATTR_CATEGORY


	Insert Into dbo.ATTR_CATEGORY
	(
		ATTR_CATEGORY_CODE
		,ATTR_CATEGORY_DESC
		,ATTR_CATE_MAKER_ID
		,ATTR_CATE_ADDED_DATETIME
		,ATTR_CATE_IS_AUTH
	)
	Values
	(
		@ATTR_CATEGORY_CODE
		,@ATTR_CATEGORY_DESC
		,@MakerId
		,@AddedDatetime
		,@IsAuth
	)
	
	If @@RowCount > 0
	BEGIN
		Set @oRetVal = SCOPE_IDENTITY() --Record has been inserted successfully, return generated ID
		Set @ID = @oRetVal
	END
	Else
		Set @oRetVal = -40 --No record inserted
	
	Return
GO
/****** Object:  StoredProcedure [dbo].[CHRIS_SP_Setup_ATTR_CATEGORY_GETALL]    Script Date: 01/02/2024 2:26:35 pm ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* -----------------------------------------------------------------------------------------------------------
 * SP Name		: CHRIS_SP_Setup_ATTR_CATEGORY_GETALL
 * ALTERd By	: Tahnia
 * ALTERd On	: 24/01/2011
 * Purpose		: Retrieve entity(ATTR_CATEGORY) records as per filter provided.
 * Module		: CHRIS
 * Called By	: iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.ATTR_CATEGORY.GetATTR_CATEGORYRecords
 * Comments		: None
 * -----------------------------------------------------------------------------------------------------------
///-------------------------------------Modification History--------------------------------------------------
	[Date]		[Author]			[Purpose]

///----------------------------------------------------------------------------------------------------------- */
 
ALTER PROCEDURE [dbo].[CHRIS_SP_Setup_ATTR_CATEGORY_GETALL]
/*<Param>*/
@oRetVal int Output --<descr></descr>
/*</Param>*/
As

Set NoCount On
/*<Comment></Comment>*/
	Select ID
	   	,ATTR_CATEGORY_CODE
	   	,ATTR_CATEGORY_DESC
	From dbo.ATTR_CATEGORY WITH(NOLOCK) WHERE ATTR_CATE_IS_AUTH = 1



	If @@RowCount = 0 
		Set @oRetVal = -40	--Record not found
	Else
		Set @oRetVal = -10 	--Success
	
	Return


	Select * from ATTR_CATEGORY
GO
/****** Object:  StoredProcedure [dbo].[CHRIS_SP_Setup_ATTR_CATEGORY_MANAGER]    Script Date: 01/02/2024 2:26:35 pm ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* -----------------------------------------------------------------------------------------------------------
 * SP Name		: CHRIS_SP_Setup_ATTR_CATEGORYMANAGER
 * ALTERd By	: Tahnia
 * ALTERd On	: 24/01/2011
 * Purpose		: Manages (ATTR_CATEGORY) store procedure depending upon the action type.
 * Module		: CHRIS
 * Called By	: iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.ATTR_CATEGORYDAO
 * Comments		: None
 * -----------------------------------------------------------------------------------------------------------
///-------------------------------------Modification History--------------------------------------------------
	[Date]		[Author]			[Purpose]

///----------------------------------------------------------------------------------------------------------- */
 
ALTER PROCEDURE [dbo].[CHRIS_SP_Setup_ATTR_CATEGORY_MANAGER]
/*<Param>*/
@ATTR_CATEGORY_CODE numeric(3,0)=null, --<descr></descr>
@ATTR_CATEGORY_DESC varchar(20)=null, --<descr></descr>
@ID INT=null  OUTPUT,  --<descr></descr>
@ActionType varchar(50),
@MakerId varchar(50) = null,
@CheckerkID varchar(50) = null,
@AddedDatetime datetime = null,
@UpdatedDatetime datetime = null,
@IsAuth bit = null
,@oRetVal int = Null Output
/*</Param>*/
As

Set NoCount On
/*<Comment></Comment>*/


	If @ActionType = 'Save'
	Begin
		Exec CHRIS_SP_Setup_ATTR_CATEGORY_ADD
	   			@ATTR_CATEGORY_CODE
	   			,@ATTR_CATEGORY_DESC
	   			,@ID
				,@MakerId
				,@AddedDatetime
				,@IsAuth
				,@oRetVal = @oRetVal OUTPUT
			SET @ID =  @oRetVal
	End
	Else If @ActionType = 'Update'
	Begin
		Exec CHRIS_SP_Setup_ATTR_CATEGORY_UPDATE
	   			@ATTR_CATEGORY_CODE
	   			,@ATTR_CATEGORY_DESC
	   			,@ID
				,@MakerId
				,@AddedDatetime
				,@IsAuth
	End
	Else If @ActionType = 'Delete'
	Begin
		Exec CHRIS_SP_Setup_ATTR_CATEGORY_DELETE
				@ID,@oRetVal
	End
	Else If @ActionType = 'Get'
	Begin
		Exec CHRIS_SP_Setup_ATTR_CATEGORY_GET
				@ID,@oRetVal
	End
	Else If @ActionType = 'List'
	Begin
		Exec CHRIS_SP_Setup_ATTR_CATEGORY_GETALL @oRetVal
	End
	Else If @ActionType = 'Category_LOV'
	Begin
	select attr_category_desc ATTR_CATEGORY_DESC, 
			attr_category_code 
	from attr_category 
	order by 1
	End
	Else If @ActionType = 'Category_LOVExists'
	Begin
	IF EXISTS(Select ID FROM  attr_category  WHERE attr_category_code = @ATTR_CATEGORY_CODE )  
	BEGIN 
		select attr_category_desc ATTR_CATEGORY_DESC, 
			attr_category_code 
	from attr_category 
	WHERE attr_category_code=@ATTR_CATEGORY_CODE
	order by 1
	End
    IF EXISTS(Select ID FROM  attr_category  WHERE CAST (attr_category_code AS VARCHAR) LIKE  CAST(@ATTR_CATEGORY_CODE AS VARCHAR) + '%' OR  ISNULL(CAST(@ATTR_CATEGORY_CODE AS VARCHAR),'') = '')   
		BEGIN 
			select attr_category_desc ATTR_CATEGORY_DESC, 
			attr_category_code 
	from attr_category 
	WHERE   (CAST(attr_category_code AS VARCHAR) LIKE  CAST(@ATTR_CATEGORY_CODE AS VARCHAR) + '%')
	order by 1
		
		
                 
		End

   End

   ----------------------------------------------------------------------------------------------

   ELSE IF @ACTIONTYPE = 'AUTHORIZE_ATTR_CATE'      
	 BEGIN      
	  UPDATE ATTR_CATEGORY SET ATTR_CATE_IS_AUTH = 1, ATTR_CATE_CHECKER_ID = @CheckerkID,ATTR_CATE_UPDATED_DATETIME = @UpdatedDatetime WHERE ID = @ID     
	 END 

	 ELSE IF @ACTIONTYPE = 'REJECT_ATTR_CATE'      
	 BEGIN      
	  DELETE FROM ATTR_CATEGORY WHERE ID = @ID     
	 END 

	ELSE IF @ACTIONTYPE = 'GET_UNAUTHORIZE_ATTR_CATE'      
	 BEGIN
		Select ID
	   	,ATTR_CATEGORY_CODE
	   	,ATTR_CATEGORY_DESC
	From dbo.ATTR_CATEGORY WITH(NOLOCK) WHERE ATTR_CATE_IS_AUTH = 0 AND ATTR_CATE_MAKER_ID != @MakerId
	Return
	 END 



	Return
GO
/****** Object:  StoredProcedure [dbo].[CHRIS_SP_Setup_ATTR_CATEGORY_UPDATE]    Script Date: 01/02/2024 2:26:35 pm ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* -----------------------------------------------------------------------------------------------------------
 * SP Name		: CHRIS_SP_Setup_ATTR_CATEGORYUPDATE
 * ALTERd By	: Tahnia
 * ALTERd On	: 24/01/2011
 * Purpose		: Updates record(s) in the Table(ATTR_CATEGORY) according to the parameter(s).
 * Module		: CHRIS
 * Called By	: iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.ATTR_CATEGORY.InsertATTR_CATEGORY
 * Comments		: None
 * -----------------------------------------------------------------------------------------------------------
///-------------------------------------Modification History--------------------------------------------------
	[Date]		[Author]			[Purpose]

///----------------------------------------------------------------------------------------------------------- */
 
ALTER PROCEDURE [dbo].[CHRIS_SP_Setup_ATTR_CATEGORY_UPDATE]
/*<Param>*/
@ATTR_CATEGORY_CODE numeric(3,0), --<descr></descr>
@ATTR_CATEGORY_DESC varchar(20), --<descr></descr>
@ID INT, --<descr></descr>
@MakerId varchar(50) = null,
@AddedDatetime datetime = null,
@IsAuth bit = null,
@oRetVal int = Null Output

/*</Param>*/
As

Set NoCount On
/*<Comment></Comment>*/
	Update	dbo.ATTR_CATEGORY
	Set
	   	ATTR_CATEGORY_CODE = @ATTR_CATEGORY_CODE
	   	,ATTR_CATEGORY_DESC = @ATTR_CATEGORY_DESC
		,ATTR_CATE_MAKER_ID = @MakerId
		,ATTR_CATE_ADDED_DATETIME = @AddedDatetime
		,ATTR_CATE_IS_AUTH = @IsAuth
	Where 	 ID = @ID	
	
	Set @oRetVal = -10 --Record updated successfully
	Return
GO
/****** Object:  StoredProcedure [dbo].[CHRIS_SP_SETUP_GROUP1_ADD]    Script Date: 01/02/2024 2:26:35 pm ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* -----------------------------------------------------------------------------------------------------------
 * SP Name		: CHRIS_SP_SETUP_GROUP1ADD
 * ALTERd By	: Nida Nazir
 * ALTERd On	: 28/01/2011
 * Purpose		: Adds record(s) in the Table(GROUP1) according to the parameter(s).
 * Module		: CHRIS
 * Called By	: iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.GROUP1.InsertGROUP1
 * Comments		: None
 * -----------------------------------------------------------------------------------------------------------
///-------------------------------------Modification History--------------------------------------------------
	[Date]		[Author]			[Purpose]

///----------------------------------------------------------------------------------------------------------- */

ALTER PROCEDURE [dbo].[CHRIS_SP_SETUP_GROUP1_ADD]
/*<Params>*/
@SP_SEGMENT varchar(3), --<descr></descr>
@SP_GROUP_CODE varchar(5), --<descr></descr>
@SP_DESC_1 varchar(20), --<descr></descr>
@SP_DESC_2 varchar(20), --<descr></descr>
@SP_GROUP_HEAD varchar(20), --<descr></descr>
@SP_DATE_FROM datetime, --<descr></descr>
@SP_DATE_TO datetime, --<descr></descr>
@ID INT  OUTPUT,  --<descr></descr>
@MakerId varchar(50) = null,
@AddedDatetime datetime = null,
@IsAuth bit = null,
@oRetVal int = Null Output --<descr></descr>
/*</Params>*/
As

Set NoCount On
/*<Comments></Comments>*/
/*<LocalVariables>*/
/*</LocalVariables>*/
 
	Insert Into dbo.GROUP1
	(
		SP_SEGMENT
		,SP_GROUP_CODE
		,SP_DESC_1
		,SP_DESC_2
		,SP_GROUP_HEAD
		,SP_DATE_FROM
		,SP_DATE_TO
		,GR1_MAKER_ID
		,GR1_ADDED_DATETIME
		,GR1_IS_AUTH
	)
	Values
	(
		@SP_SEGMENT
		,@SP_GROUP_CODE
		,@SP_DESC_1
		,@SP_DESC_2
		,@SP_GROUP_HEAD
		,@SP_DATE_FROM
		,@SP_DATE_TO
		,@MakerId
		,@AddedDatetime
		,@IsAuth
	)
	
	If @@RowCount > 0
	BEGIN
		Set @oRetVal = SCOPE_IDENTITY() --Record has been inserted successfully, return generated ID
		Set @ID = @oRetVal
	END
	Else
		Set @oRetVal = -40 --No record inserted
	
	Return
GO
/****** Object:  StoredProcedure [dbo].[CHRIS_SP_SETUP_GROUP1_GETALL]    Script Date: 01/02/2024 2:26:35 pm ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* -----------------------------------------------------------------------------------------------------------
 * SP Name		: CHRIS_SP_SETUP_GROUP1_GETALL
 * ALTERd By	: Nida Nazir
 * ALTERd On	: 28/01/2011
 * Purpose		: Retrieve entity(GROUP1) records as per filter provided.
 * Module		: CHRIS
 * Called By	: iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.GROUP1.GetGROUP1Records
 * Comments		: None
 * -----------------------------------------------------------------------------------------------------------
///-------------------------------------Modification History--------------------------------------------------
	[Date]		[Author]			[Purpose]

///----------------------------------------------------------------------------------------------------------- */
 
ALTER PROCEDURE [dbo].[CHRIS_SP_SETUP_GROUP1_GETALL]
/*<Param>*/
@oRetVal int Output --<descr></descr>
/*</Param>*/
As

Set NoCount On
/*<Comment></Comment>*/
	Select ID
	   	,SP_SEGMENT
	   	,SP_GROUP_CODE
	   	,SP_DESC_1
	   	,SP_DESC_2
	   	,SP_GROUP_HEAD
	   	,SP_DATE_FROM
	   	,SP_DATE_TO
	From dbo.GROUP1 WITH(NOLOCK)  WHERE GR1_IS_AUTH = 1



	If @@RowCount = 0 
		Set @oRetVal = -40	--Record not found
	Else
		Set @oRetVal = -10 	--Success
	
	Return
GO
/****** Object:  StoredProcedure [dbo].[CHRIS_SP_SETUP_GROUP1_MANAGER]    Script Date: 01/02/2024 2:26:35 pm ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* -----------------------------------------------------------------------------------------------------------
 * SP Name		: CHRIS_SP_SETUP_GROUP1MANAGER
 * ALTERd By	: Nida Nazir
 * ALTERd On	: 28/01/2011
 * Purpose		: Manages (GROUP1) store procedure depending upon the action type.
 * Module		: CHRIS
 * Called By	: iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.GROUP1DAO
 * Comments		: None
 * -----------------------------------------------------------------------------------------------------------
///-------------------------------------Modification History--------------------------------------------------
	[Date]		[Author]			[Purpose]

///----------------------------------------------------------------------------------------------------------- */
 
ALTER PROCEDURE [dbo].[CHRIS_SP_SETUP_GROUP1_MANAGER]
/*<Param>*/
@SP_SEGMENT varchar(3), --<descr></descr>
@SP_GROUP_CODE varchar(5), --<descr></descr>
@SP_DESC_1 varchar(20), --<descr></descr>
@SP_DESC_2 varchar(20), --<descr></descr>
@SP_GROUP_HEAD varchar(20), --<descr></descr>
@SP_DATE_FROM datetime, --<descr></descr>
@SP_DATE_TO datetime, --<descr></descr>
@ID INT  OUTPUT,  --<descr></descr>
@ActionType varchar(50),
@MakerId varchar(50) = null,
@CheckerkID varchar(50) = null,
@AddedDatetime datetime = null,
@UpdatedDatetime datetime = null,
@IsAuth bit = null
,@oRetVal int = Null Output
/*</Param>*/
As

Set NoCount On
/*<Comment></Comment>*/


	If @ActionType = 'Save'
	Begin
		Exec CHRIS_SP_SETUP_GROUP1_ADD
	   			@SP_SEGMENT
	   			,@SP_GROUP_CODE
	   			,@SP_DESC_1
	   			,@SP_DESC_2
	   			,@SP_GROUP_HEAD
	   			,@SP_DATE_FROM
	   			,@SP_DATE_TO
	   			,@ID
				,@MakerId
				,@AddedDatetime
				,@IsAuth
				,@oRetVal = @oRetVal OUTPUT
			SET @ID =  @oRetVal
	End
	Else If @ActionType = 'Update'
	Begin
		Exec CHRIS_SP_SETUP_GROUP1_UPDATE
	   			@SP_SEGMENT
	   			,@SP_GROUP_CODE
	   			,@SP_DESC_1
	   			,@SP_DESC_2
	   			,@SP_GROUP_HEAD
	   			,@SP_DATE_FROM
	   			,@SP_DATE_TO
	   			,@ID
				,@MakerId
				,@AddedDatetime
				,@IsAuth
	End
	Else If @ActionType = 'Delete'
	Begin
		Exec CHRIS_SP_SETUP_GROUP1_DELETE
				@ID,@oRetVal
	End
	Else If @ActionType = 'Get'
	Begin
		Exec CHRIS_SP_SETUP_GROUP1_GET
				@ID,@oRetVal
	End
	Else If @ActionType = 'List'
	Begin
		Exec CHRIS_SP_SETUP_GROUP1_GETALL @oRetVal
	End
	Else If @ActionType = 'Segment_LOVs'
	Begin
		SELECT
			 SUBSTRING(sp_segment, 1, 3) sp_segment,
			 SUBSTRING(sp_group_code, 1, 5) sp_group_code,
			CONVERT(VARCHAR(23), MAX(sp_date_from), 103) AS date_from
	FROM  group1 WITH(NOLOCK)
	GROUP BY sp_segment,
		  sp_group_code 
	ORDER BY sp_segment,
		 sp_group_code 
	
	End
	Else If @ActionType = 'Segment_LOVsExists'
	Begin
		
	IF EXISTS(Select ID FROM  group1  WHERE sp_group_code = @SP_GROUP_CODE )  
	BEGIN 
			SELECT
			 SUBSTRING(sp_segment, 1, 3) sp_segment,
			 SUBSTRING(sp_group_code, 1, 5) sp_group_code,
			 CONVERT(VARCHAR(23), MAX(sp_date_from), 103)  AS date_from --sp_date_from
	FROM  group1 WITH(NOLOCK)
	WHERE sp_group_code = @SP_GROUP_CODE
	GROUP BY sp_segment,
		  sp_group_code 
	ORDER BY sp_segment,
		 sp_group_code 
	End
    IF EXISTS(Select ID FROM  group1  WHERE CAST (sp_group_code AS VARCHAR) LIKE  CAST(@SP_GROUP_CODE AS VARCHAR) + '%' OR  ISNULL(CAST(@SP_GROUP_CODE AS VARCHAR),'') = '')   
		BEGIN 
		SELECT
			 SUBSTRING(sp_segment, 1, 3) sp_segment,
			 SUBSTRING(sp_group_code, 1, 5) sp_group_code,
			 CONVERT(VARCHAR(23), MAX(sp_date_from), 103)  AS date_from--sp_date_from
	FROM  group1 WITH(NOLOCK)
	
	WHERE   (CAST(sp_group_code AS VARCHAR) LIKE  CAST(@SP_GROUP_CODE AS VARCHAR) + '%')
	GROUP BY sp_segment,
		  sp_group_code 
	ORDER BY sp_segment,
		 sp_group_code 
	
	
		
                 
		End

	End


	
	ELSE IF @ACTIONTYPE = 'AUTHORIZE_GroupEntry'      
	 BEGIN      
	  UPDATE GROUP1 SET GR1_IS_AUTH = 1, GR1_CHECKER_ID = @CheckerkID,GR1_UPDATED_DATETIME = @UpdatedDatetime WHERE ID = @ID     
	 END 

	 ELSE IF @ACTIONTYPE = 'REJECT_GroupEntry'      
	 BEGIN      
	  DELETE FROM GROUP1 WHERE ID = @ID     
	 END 

	ELSE IF @ACTIONTYPE = 'GET_UNAUTHORIZE_GroupEntry'      
	 BEGIN
		Select ID
	   	,SP_SEGMENT
	   	,SP_GROUP_CODE
	   	,SP_DESC_1
	   	,SP_DESC_2
	   	,SP_GROUP_HEAD
	   	,SP_DATE_FROM
	   	,SP_DATE_TO
	From dbo.GROUP1 WITH(NOLOCK) WHERE GR1_IS_AUTH = 0 AND GR1_MAKER_ID != @MakerId
	
	Return
	 END 


	Return
GO
/****** Object:  StoredProcedure [dbo].[CHRIS_SP_SETUP_GROUP1_UPDATE]    Script Date: 01/02/2024 2:26:35 pm ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* -----------------------------------------------------------------------------------------------------------
 * SP Name		: CHRIS_SP_SETUP_GROUP1UPDATE
 * ALTERd By	: Nida Nazir
 * ALTERd On	: 28/01/2011
 * Purpose		: Updates record(s) in the Table(GROUP1) according to the parameter(s).
 * Module		: CHRIS
 * Called By	: iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.GROUP1.InsertGROUP1
 * Comments		: None
 * -----------------------------------------------------------------------------------------------------------
///-------------------------------------Modification History--------------------------------------------------
	[Date]		[Author]			[Purpose]

///----------------------------------------------------------------------------------------------------------- */
 
ALTER PROCEDURE [dbo].[CHRIS_SP_SETUP_GROUP1_UPDATE]
/*<Param>*/
@SP_SEGMENT varchar(3), --<descr></descr>
@SP_GROUP_CODE varchar(5), --<descr></descr>
@SP_DESC_1 varchar(20), --<descr></descr>
@SP_DESC_2 varchar(20), --<descr></descr>
@SP_GROUP_HEAD varchar(20), --<descr></descr>
@SP_DATE_FROM datetime, --<descr></descr>
@SP_DATE_TO datetime, --<descr></descr>
@ID INT, --<descr></descr>
@MakerId varchar(50) = null,
@AddedDatetime datetime = null,
@IsAuth bit = null,
@oRetVal int = Null Output
/*</Param>*/
As

Set NoCount On
/*<Comment></Comment>*/
	Update	dbo.GROUP1
	Set
	   	SP_SEGMENT = @SP_SEGMENT
	   	,SP_GROUP_CODE = @SP_GROUP_CODE
	   	,SP_DESC_1 = @SP_DESC_1
	   	,SP_DESC_2 = @SP_DESC_2
	   	,SP_GROUP_HEAD = @SP_GROUP_HEAD
	   	,SP_DATE_FROM = @SP_DATE_FROM
	   	,SP_DATE_TO = @SP_DATE_TO
		,GR1_MAKER_ID = @MakerId
		,GR1_ADDED_DATETIME = @AddedDatetime
		,GR1_IS_AUTH = @IsAuth
	Where 	 ID = @ID	
	
	Set @oRetVal = -10 --Record updated successfully
	Return
GO
/****** Object:  StoredProcedure [dbo].[CHRIS_SP_SMTP_SETTINGS]    Script Date: 01/02/2024 2:26:35 pm ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* -----------------------------------------------------------------------------------------------------------  
 * SP Name  : CHRIS_SP_PAYROLL_SAL  
 * ALTERd By : Syed Fahad Abbas 
 * ALTERd On : 28/08/2012  
 * Purpose  : Get SMTP mail settings and employee email address based on action type.
 * Module  : CHRIS  
 * Called By : iCORE.CHRIS.PRESENTATIONOBJECTS.Cmn.BaseRptForm  
 * Comments  : None  
 * -----------------------------------------------------------------------------------------------------------  
///-------------------------------------Modification History--------------------------------------------------  
 [Date]  [Author]   [Purpose]  
  
///----------------------------------------------------------------------------------------------------------- */  
ALTER PROCEDURE [dbo].[CHRIS_SP_SMTP_SETTINGS]      
     
 @ProfileName varchar(50), --<descr></descr>    
 @ActionType varchar(50),
 @SharedFolderPath varchar(MAX),      
 @Subject varchar(MAX),    
 @Body varchar(MAX),    
 @ID INT  OUTPUT,  --<descr></descr>  
 @MakerId varchar(50) = null,
 @CheckerkID varchar(50) = null,
 @AddedDatetime datetime = null,
 @UpdatedDatetime datetime = null,
 @IsAuth bit = null,
 @oRetVal int = Null Output      
AS    
    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
    
If @ActionType = 'List'      
Begin      
 Select ProfileName,SharedFolderPath,[Subject],Body       
 From dbo.SMTP_Settings WITH(NOLOCK)    WHERE SMTP_IS_AUTH = 1 
    
END    
    
ELSE If @ActionType = 'Save'      
    
Begin      
    
 DELETE from dbo.SMTP_Settings    
     
 INSERT INTO dbo.SMTP_Settings    
 Values(@ProfileName,@SharedFolderPath,@Subject,@Body,@MakerId,@CheckerkID,@AddedDatetime,@UpdatedDatetime,@IsAuth)    
END    

ELSE IF @ACTIONTYPE = 'AUTHORIZE_SMTP_Settings'      
	 BEGIN      
	  UPDATE SMTP_Settings SET SMTP_IS_AUTH = 1, SMTP_CHECKER_ID = @CheckerkID,SMTP_UPDATED_DATETIME = @UpdatedDatetime WHERE ID = @ID     
	 END 

	 ELSE IF @ACTIONTYPE = 'REJECT_SMTP_Settings'      
	 BEGIN      
	  DELETE FROM SMTP_Settings WHERE ID = @ID     
	 END 

	ELSE IF @ACTIONTYPE = 'GET_UNAUTHORIZE_SMTP_Settings'      
	 BEGIN
		Select ID, ProfileName,SharedFolderPath,[Subject],Body       
 From dbo.SMTP_Settings WITH(NOLOCK)     
 WHERE SMTP_IS_AUTH = 0 AND SMTP_MAKER_ID != @MakerId
	
	Return
	 END 
GO
/****** Object:  StoredProcedure [dbo].[CHRIS_SP_TIREnt_TIR_ADD]    Script Date: 01/02/2024 2:26:35 pm ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* -----------------------------------------------------------------------------------------------------------
 * SP Name		: CHRIS_SP_TIREnt_TIRADD
 * ALTERd By	: Umair Mufti
 * ALTERd On	: 01/02/2011
 * Purpose		: Adds record(s) in the Table(TIR) according to the parameter(s).
 * Module		: CHRIS
 * Called By	: iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.TIR.InsertTIR
 * Comments		: None
 * -----------------------------------------------------------------------------------------------------------
///-------------------------------------Modification History--------------------------------------------------
	[Date]		[Author]			[Purpose]

///----------------------------------------------------------------------------------------------------------- */

ALTER PROCEDURE [dbo].[CHRIS_SP_TIREnt_TIR_ADD]
/*<Params>*/
@SP_YEAR numeric(4,0), --<descr></descr>
@SP_TIR_PER numeric(4,2), --<descr></descr>
@SP_LEVEL varchar(3), --<descr></descr>
@SP_RANK numeric(2,0), --<descr></descr>
@SP_MIN_PER numeric(2,0), --<descr></descr>
@SP_MAX_PER numeric(2,0), --<descr></descr>
@SP_AVERAGE numeric(2,0), --<descr></descr>
@SP_LOCAL_TIR numeric(2,0), --<descr></descr>
@ID INT  OUTPUT,  --<descr></descr>
@MakerId varchar(50) = null,
@AddedDatetime datetime = null,
@IsAuth bit = null,
@oRetVal int = Null Output --<descr></descr>
/*</Params>*/
As

Set NoCount On
/*<Comments></Comments>*/
/*<LocalVariables>*/
/*</LocalVariables>*/
 
	Insert Into dbo.TIR
	(
		SP_YEAR
		,SP_TIR_PER
		,SP_LEVEL
		,SP_RANK
		,SP_MIN_PER
		,SP_MAX_PER
		,SP_AVERAGE
		,SP_LOCAL_TIR
		,TIR_MAKER_ID
		,TIR_ADDED_DATETIME
		,TIR_IS_AUTH
	)
	Values
	(
		@SP_YEAR
		,@SP_TIR_PER
		,@SP_LEVEL
		,@SP_RANK
		,@SP_MIN_PER
		,@SP_MAX_PER
		,@SP_AVERAGE
		,@SP_LOCAL_TIR
		,@MakerId
		,@AddedDatetime
		,@IsAuth
	)
	
	If @@RowCount > 0
	BEGIN
		Set @oRetVal = SCOPE_IDENTITY() --Record has been inserted successfully, return generated ID
		Set @ID = @oRetVal
	END
	Else
		Set @oRetVal = -40 --No record inserted
	
	Return
GO
/****** Object:  StoredProcedure [dbo].[CHRIS_SP_TIREnt_TIR_GETALL]    Script Date: 01/02/2024 2:26:35 pm ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* -----------------------------------------------------------------------------------------------------------
 * SP Name		: CHRIS_SP_TIREnt_TIR_GETALL
 * ALTERd By	: Umair Mufti
 * ALTERd On	: 01/02/2011
 * Purpose		: Retrieve entity(TIR) records as per filter provided.
 * Module		: CHRIS
 * Called By	: iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.TIR.GetTIRRecords
 * Comments		: None
 * -----------------------------------------------------------------------------------------------------------
///-------------------------------------Modification History--------------------------------------------------
	[Date]		[Author]			[Purpose]

///----------------------------------------------------------------------------------------------------------- */
 
ALTER PROCEDURE [dbo].[CHRIS_SP_TIREnt_TIR_GETALL]
/*<Param>*/
@oRetVal int Output --<descr></descr>
/*</Param>*/
As

Set NoCount On
/*<Comment></Comment>*/
	Select ID
	   	,SP_YEAR
	   	,SP_TIR_PER
	   	,SP_LEVEL
	   	,SP_RANK
	   	,SP_MIN_PER
	   	,SP_MAX_PER
	   	,SP_AVERAGE
	   	,SP_LOCAL_TIR
	From dbo.TIR WITH(NOLOCK) WHERE TIR_IS_AUTH = 1



	If @@RowCount = 0 
		Set @oRetVal = -40	--Record not found
	Else
		Set @oRetVal = -10 	--Success
	
	Return
GO
/****** Object:  StoredProcedure [dbo].[CHRIS_SP_TIREnt_TIR_MANAGER]    Script Date: 01/02/2024 2:26:35 pm ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* -----------------------------------------------------------------------------------------------------------
 * SP Name		: CHRIS_SP_TIREnt_TIRMANAGER
 * ALTERd By	: Umair Mufti
 * ALTERd On	: 01/02/2011
 * Purpose		: Manages (TIR) store procedure depending upon the action type.
 * Module		: CHRIS
 * Called By	: iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.TIRDAO
 * Comments		: None
 * -----------------------------------------------------------------------------------------------------------
///-------------------------------------Modification History--------------------------------------------------
	[Date]		[Author]			[Purpose]

///----------------------------------------------------------------------------------------------------------- */
 
ALTER PROCEDURE [dbo].[CHRIS_SP_TIREnt_TIR_MANAGER]
/*<Param>*/
@SP_YEAR numeric(4,0), --<descr></descr>
@SP_TIR_PER numeric(4,2), --<descr></descr>
@SP_LEVEL varchar(3), --<descr></descr>
@SP_RANK numeric(2,0), --<descr></descr>
@SP_MIN_PER numeric(2,0), --<descr></descr>
@SP_MAX_PER numeric(2,0), --<descr></descr>
@SP_AVERAGE numeric(2,0), --<descr></descr>
@SP_LOCAL_TIR numeric(2,0), --<descr></descr>
@ID INT  OUTPUT,  --<descr></descr>
@ActionType varchar(50),
@MakerId varchar(50) = null,
@CheckerkID varchar(50) = null,
@AddedDatetime datetime = null,
@UpdatedDatetime datetime = null,
@IsAuth bit = null
,@oRetVal int = Null Output
/*</Param>*/
As

Set NoCount On
/*<Comment></Comment>*/


	If @ActionType = 'Save'
	Begin
		Exec CHRIS_SP_TIREnt_TIR_ADD
	   			@SP_YEAR
	   			,@SP_TIR_PER
	   			,@SP_LEVEL
	   			,@SP_RANK
	   			,@SP_MIN_PER
	   			,@SP_MAX_PER
	   			,@SP_AVERAGE
	   			,@SP_LOCAL_TIR
	   			,@ID
				,@MakerId
				,@AddedDatetime
				,@IsAuth
				,@oRetVal = @oRetVal OUTPUT
			SET @ID =  @oRetVal
	End
	Else If @ActionType = 'Update'
	Begin
		Exec CHRIS_SP_TIREnt_TIR_UPDATE
	   			@SP_YEAR
	   			,@SP_TIR_PER
	   			,@SP_LEVEL
	   			,@SP_RANK
	   			,@SP_MIN_PER
	   			,@SP_MAX_PER
	   			,@SP_AVERAGE
	   			,@SP_LOCAL_TIR
	   			,@ID
				,@MakerId
				,@AddedDatetime
				,@IsAuth
	End
	Else If @ActionType = 'Delete'
	Begin
		Exec CHRIS_SP_TIREnt_TIR_DELETE
				@ID,@oRetVal
	End
	Else If @ActionType = 'Get'
	Begin
		Exec CHRIS_SP_TIREnt_TIR_GET
				@ID,@oRetVal
	End
	Else If @ActionType = 'List'
	Begin
		Exec CHRIS_SP_TIREnt_TIR_GETALL @oRetVal
	End

	Else If @ActionType = 'LEVEL_LOV'
	Begin
		SELECT DISTINCT SP_LEVEL FROM DESIG WHERE SP_CURRENT = 'C'
	End

	Else If @ActionType = 'LEVEL_LOV_EXIST'
	Begin
		IF EXISTS (SELECT DISTINCT SP_LEVEL FROM DESIG WHERE SP_CURRENT = 'C' AND SP_LEVEL = @SP_LEVEL)
		BEGIN
			SELECT DISTINCT SP_LEVEL FROM DESIG WHERE SP_CURRENT = 'C' AND SP_LEVEL = @SP_LEVEL 
		END

		ELSE IF EXISTS (SELECT DISTINCT SP_LEVEL FROM DESIG WHERE SP_CURRENT = 'C' AND SP_LEVEL LIKE @SP_LEVEL + '%' OR ISNULL(@SP_LEVEL,'' )= '')
		BEGIN
			SELECT DISTINCT SP_LEVEL FROM DESIG WHERE SP_CURRENT = 'C' AND SP_LEVEL = @SP_LEVEL 
		END
	End

	Else If @ActionType = 'W_YEAR'
	Begin
		Select ID
	   	,SP_YEAR
	   	,SP_TIR_PER
	   	,SP_LEVEL
	   	,SP_RANK
	   	,SP_MIN_PER
	   	,SP_MAX_PER
	   	,SP_AVERAGE
	   	,SP_LOCAL_TIR
	From dbo.TIR WITH(NOLOCK)
	Where 	 SP_YEAR = @SP_YEAR	

	End

--	Else If @ActionType = 'SP_LEVEL'
--	Begin
--		SELECT 'X' 
--		FROM DESIG 
--		WHERE SP_LEVEL = @SP_LEVEL
--		AND SP_CURRENT = 'C';
--
--	End

ELSE IF @ACTIONTYPE = 'AUTHORIZE_TIR'      
	 BEGIN      
	  UPDATE TIR SET TIR_IS_AUTH = 1, TIR_CHECKER_ID = @CheckerkID,TIR_UPDATED_DATETIME = @UpdatedDatetime WHERE ID = @ID     
	 END 

	 ELSE IF @ACTIONTYPE = 'REJECT_TIR'      
	 BEGIN      
	  DELETE FROM TIR WHERE ID = @ID     
	 END 

	ELSE IF @ACTIONTYPE = 'GET_UNAUTHORIZE_TIR'      
	 BEGIN
		Select ID
	   	,SP_YEAR
	   	,SP_TIR_PER
	   	,SP_LEVEL
	   	,SP_RANK
	   	,SP_MIN_PER
	   	,SP_MAX_PER
	   	,SP_AVERAGE
	   	,SP_LOCAL_TIR
	From dbo.TIR WITH(NOLOCK) WHERE TIR_IS_AUTH = 0 AND TIR_MAKER_ID != @MakerId
	
	Return
	 END 

	Return
GO
/****** Object:  StoredProcedure [dbo].[CHRIS_SP_TIREnt_TIR_UPDATE]    Script Date: 01/02/2024 2:26:35 pm ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* -----------------------------------------------------------------------------------------------------------
 * SP Name		: CHRIS_SP_TIREnt_TIRUPDATE
 * ALTERd By	: Umair Mufti
 * ALTERd On	: 01/02/2011
 * Purpose		: Updates record(s) in the Table(TIR) according to the parameter(s).
 * Module		: CHRIS
 * Called By	: iCORE.CHRIS.BUSINESSOBJECTS.ENTITIES.TIR.InsertTIR
 * Comments		: None
 * -----------------------------------------------------------------------------------------------------------
///-------------------------------------Modification History--------------------------------------------------
	[Date]		[Author]			[Purpose]

///----------------------------------------------------------------------------------------------------------- */
 
ALTER PROCEDURE [dbo].[CHRIS_SP_TIREnt_TIR_UPDATE]
/*<Param>*/
@SP_YEAR numeric(4,0), --<descr></descr>
@SP_TIR_PER numeric(4,2), --<descr></descr>
@SP_LEVEL varchar(3), --<descr></descr>
@SP_RANK numeric(2,0), --<descr></descr>
@SP_MIN_PER numeric(2,0), --<descr></descr>
@SP_MAX_PER numeric(2,0), --<descr></descr>
@SP_AVERAGE numeric(2,0), --<descr></descr>
@SP_LOCAL_TIR numeric(2,0), --<descr></descr>
@ID INT, --<descr></descr>
@MakerId varchar(50) = null,
@AddedDatetime datetime = null,
@IsAuth bit = null,
@oRetVal int = Null Output
/*</Param>*/
As

Set NoCount On
/*<Comment></Comment>*/
	Update	dbo.TIR
	Set
	   	SP_YEAR = @SP_YEAR
	   	,SP_TIR_PER = @SP_TIR_PER
	   	,SP_LEVEL = @SP_LEVEL
	   	,SP_RANK = @SP_RANK
	   	,SP_MIN_PER = @SP_MIN_PER
	   	,SP_MAX_PER = @SP_MAX_PER
	   	,SP_AVERAGE = @SP_AVERAGE
	   	,SP_LOCAL_TIR = @SP_LOCAL_TIR
		,TIR_MAKER_ID = @MakerId
		,TIR_ADDED_DATETIME = @AddedDatetime
		,TIR_IS_AUTH = @IsAuth
	Where 	 ID = @ID	
	
	Set @oRetVal = -10 --Record updated successfully
	Return
GO
