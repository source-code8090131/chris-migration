USE [CHRIS_UAT]
GO
IF NOT EXISTS (
  SELECT * 
  FROM   sys.columns 
  WHERE  object_id = OBJECT_ID(N'[dbo].[ATTR_CATEGORY]') 
         AND name = 'ATTR_CATE_MAKER_ID'
)
BEGIN
    ALTER TABLE ATTR_CATEGORY 
    ADD ATTR_CATE_MAKER_ID varchar(50)
END
GO
IF NOT EXISTS (
  SELECT * 
  FROM   sys.columns 
  WHERE  object_id = OBJECT_ID(N'[dbo].[ATTR_CATEGORY]') 
         AND name = 'ATTR_CATE_CHECKER_ID'
)
BEGIN
    ALTER TABLE ATTR_CATEGORY 
    ADD ATTR_CATE_CHECKER_ID varchar(50)
END
GO
IF NOT EXISTS (
  SELECT * 
  FROM   sys.columns 
  WHERE  object_id = OBJECT_ID(N'[dbo].[ATTR_CATEGORY]') 
         AND name = 'ATTR_CATE_ADDED_DATETIME'
)
BEGIN
    ALTER TABLE ATTR_CATEGORY 
    ADD ATTR_CATE_ADDED_DATETIME datetime
END
GO
IF NOT EXISTS (
  SELECT * 
  FROM   sys.columns 
  WHERE  object_id = OBJECT_ID(N'[dbo].[ATTR_CATEGORY]') 
         AND name = 'ATTR_CATE_UPDATED_DATETIME'
)
BEGIN
    ALTER TABLE ATTR_CATEGORY 
    ADD ATTR_CATE_UPDATED_DATETIME datetime
END
GO
IF NOT EXISTS (
  SELECT * 
  FROM   sys.columns 
  WHERE  object_id = OBJECT_ID(N'[dbo].[ATTR_CATEGORY]') 
         AND name = 'ATTR_CATE_IS_AUTH'
)
BEGIN
    ALTER TABLE ATTR_CATEGORY 
    ADD ATTR_CATE_IS_AUTH bit
END
GO
UPDATE ATTR_CATEGORY SET ATTR_CATE_IS_AUTH = 0, ATTR_CATE_MAKER_ID = 'SYSTEM'
GO