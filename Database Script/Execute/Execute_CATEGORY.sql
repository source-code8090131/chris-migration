USE [CHRIS_UAT]
GO
IF NOT EXISTS (
  SELECT * 
  FROM   sys.columns 
  WHERE  object_id = OBJECT_ID(N'[dbo].[CATEGORY]') 
         AND name = 'CAT_MAKER_ID'
)
BEGIN
    ALTER TABLE CATEGORY 
    ADD CAT_MAKER_ID varchar(50)
END
GO
IF NOT EXISTS (
  SELECT * 
  FROM   sys.columns 
  WHERE  object_id = OBJECT_ID(N'[dbo].[CATEGORY]') 
         AND name = 'CAT_CHECKER_ID'
)
BEGIN
    ALTER TABLE CATEGORY 
    ADD CAT_CHECKER_ID varchar(50)
END
GO
IF NOT EXISTS (
  SELECT * 
  FROM   sys.columns 
  WHERE  object_id = OBJECT_ID(N'[dbo].[CATEGORY]') 
         AND name = 'CAT_ADDED_DATETIME'
)
BEGIN
    ALTER TABLE CATEGORY 
    ADD CAT_ADDED_DATETIME datetime
END
GO
IF NOT EXISTS (
  SELECT * 
  FROM   sys.columns 
  WHERE  object_id = OBJECT_ID(N'[dbo].[CATEGORY]') 
         AND name = 'CAT_UPDATED_DATETIME'
)
BEGIN
    ALTER TABLE CATEGORY 
    ADD CAT_UPDATED_DATETIME datetime
END
GO
IF NOT EXISTS (
  SELECT * 
  FROM   sys.columns 
  WHERE  object_id = OBJECT_ID(N'[dbo].[CATEGORY]') 
         AND name = 'CAT_IS_AUTH'
)
BEGIN
    ALTER TABLE CATEGORY 
    ADD CAT_IS_AUTH bit
END
GO
UPDATE CATEGORY SET CAT_IS_AUTH = 0, CAT_MAKER_ID = 'SYSTEM'
GO