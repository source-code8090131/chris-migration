USE [CHRIS_UAT]
GO
IF NOT EXISTS (
  SELECT * 
  FROM   sys.columns 
  WHERE  object_id = OBJECT_ID(N'[dbo].[GROUP_INSC]') 
         AND name = 'SP_MAKER_ID'
)
BEGIN
    ALTER TABLE GROUP_INSC 
    ADD SP_MAKER_ID varchar(50)
END
GO
IF NOT EXISTS (
  SELECT * 
  FROM   sys.columns 
  WHERE  object_id = OBJECT_ID(N'[dbo].[GROUP_INSC]') 
         AND name = 'SP_CHECKER_ID'
)
BEGIN
    ALTER TABLE GROUP_INSC 
    ADD SP_CHECKER_ID varchar(50)
END
GO
IF NOT EXISTS (
  SELECT * 
  FROM   sys.columns 
  WHERE  object_id = OBJECT_ID(N'[dbo].[GROUP_INSC]') 
         AND name = 'SP_ADDED_DATETIME'
)
BEGIN
    ALTER TABLE GROUP_INSC 
    ADD SP_ADDED_DATETIME datetime
END
GO
IF NOT EXISTS (
  SELECT * 
  FROM   sys.columns 
  WHERE  object_id = OBJECT_ID(N'[dbo].[GROUP_INSC]') 
         AND name = 'SP_UPDATED_DATETIME'
)
BEGIN
    ALTER TABLE GROUP_INSC 
    ADD SP_UPDATED_DATETIME datetime
END
GO
IF NOT EXISTS (
  SELECT * 
  FROM   sys.columns 
  WHERE  object_id = OBJECT_ID(N'[dbo].[GROUP_INSC]') 
         AND name = 'SP_IS_AUTH'
)
BEGIN
    ALTER TABLE GROUP_INSC 
    ADD SP_IS_AUTH bit
END
GO
UPDATE GROUP_INSC SET SP_IS_AUTH = 0, SP_MAKER_ID = 'SYSTEM'
GO