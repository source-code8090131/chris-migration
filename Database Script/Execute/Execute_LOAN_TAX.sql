USE [CHRIS_UAT]
GO
IF NOT EXISTS (
  SELECT * 
  FROM   sys.columns 
  WHERE  object_id = OBJECT_ID(N'[dbo].[LOAN_TAX]') 
         AND name = 'LT_MAKER_ID'
)
BEGIN
    ALTER TABLE LOAN_TAX 
    ADD LT_MAKER_ID varchar(50)
END
GO
IF NOT EXISTS (
  SELECT * 
  FROM   sys.columns 
  WHERE  object_id = OBJECT_ID(N'[dbo].[LOAN_TAX]') 
         AND name = 'LT_CHECKER_ID'
)
BEGIN
    ALTER TABLE LOAN_TAX 
    ADD LT_CHECKER_ID varchar(50)
END
GO
IF NOT EXISTS (
  SELECT * 
  FROM   sys.columns 
  WHERE  object_id = OBJECT_ID(N'[dbo].[LOAN_TAX]') 
         AND name = 'LT_ADDED_DATETIME'
)
BEGIN
    ALTER TABLE LOAN_TAX 
    ADD LT_ADDED_DATETIME datetime
END
GO
IF NOT EXISTS (
  SELECT * 
  FROM   sys.columns 
  WHERE  object_id = OBJECT_ID(N'[dbo].[LOAN_TAX]') 
         AND name = 'LT_UPDATED_DATETIME'
)
BEGIN
    ALTER TABLE LOAN_TAX 
    ADD LT_UPDATED_DATETIME datetime
END
GO
IF NOT EXISTS (
  SELECT * 
  FROM   sys.columns 
  WHERE  object_id = OBJECT_ID(N'[dbo].[LOAN_TAX]') 
         AND name = 'LT_IS_AUTH'
)
BEGIN
    ALTER TABLE LOAN_TAX 
    ADD LT_IS_AUTH bit
END
GO
UPDATE LOAN_TAX SET LT_IS_AUTH = 0, LT_MAKER_ID = 'SYSTEM'
GO