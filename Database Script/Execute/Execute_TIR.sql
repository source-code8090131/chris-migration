USE [CHRIS_UAT]
GO
IF NOT EXISTS (
  SELECT * 
  FROM   sys.columns 
  WHERE  object_id = OBJECT_ID(N'[dbo].[TIR]') 
         AND name = 'TIR_MAKER_ID'
)
BEGIN
    ALTER TABLE TIR 
    ADD TIR_MAKER_ID varchar(50)
END
GO
IF NOT EXISTS (
  SELECT * 
  FROM   sys.columns 
  WHERE  object_id = OBJECT_ID(N'[dbo].[TIR]') 
         AND name = 'TIR_CHECKER_ID'
)
BEGIN
    ALTER TABLE TIR 
    ADD TIR_CHECKER_ID varchar(50)
END
GO
IF NOT EXISTS (
  SELECT * 
  FROM   sys.columns 
  WHERE  object_id = OBJECT_ID(N'[dbo].[TIR]') 
         AND name = 'TIR_ADDED_DATETIME'
)
BEGIN
    ALTER TABLE TIR 
    ADD TIR_ADDED_DATETIME datetime
END
GO
IF NOT EXISTS (
  SELECT * 
  FROM   sys.columns 
  WHERE  object_id = OBJECT_ID(N'[dbo].[TIR]') 
         AND name = 'TIR_UPDATED_DATETIME'
)
BEGIN
    ALTER TABLE TIR 
    ADD TIR_UPDATED_DATETIME datetime
END
GO
IF NOT EXISTS (
  SELECT * 
  FROM   sys.columns 
  WHERE  object_id = OBJECT_ID(N'[dbo].[TIR]') 
         AND name = 'TIR_IS_AUTH'
)
BEGIN
    ALTER TABLE TIR 
    ADD TIR_IS_AUTH bit
END
GO
UPDATE TIR SET TIR_IS_AUTH = 0, TIR_MAKER_ID = 'SYSTEM'
GO