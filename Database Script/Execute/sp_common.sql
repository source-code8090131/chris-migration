USE [common]
GO
/****** Object:  StoredProcedure [dbo].[COM_SP_COM_GLOBAL_PARAM_MAPPING_ADD]    Script Date: 01/02/2024 4:07:28 pm ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* -----------------------------------------------------------------------------------------------------------
 * SP Name		: COM_SP_COM_GLOBAL_PARAM_MAPPINGADD
 * Created By	: Azam Farooq
 * Created On	: 10/01/2011
 * Purpose		: Adds record(s) in the Table(COM_GLOBAL_PARAM_MAPPING) according to the parameter(s).
 * Module		: COMMON
 * Called By	: iCORE.DDS.BUSINESSOBJECTS.ENTITIES.COM_GLOBAL_PARAM_MAPPING.InsertCOM_GLOBAL_PARAM_MAPPING
 * Comments		: None
 * -----------------------------------------------------------------------------------------------------------
///-------------------------------------Modification History--------------------------------------------------
	[Date]		[Author]			[Purpose]

///----------------------------------------------------------------------------------------------------------- */

CREATE PROCEDURE [dbo].[COM_SP_COM_GLOBAL_PARAM_MAPPING_ADD]
/*<Params>*/
@ID INT, --<descr></descr>
@SOE_ID varchar(50), --<descr></descr>
@GLOBAL_PARAM_ID int, --<descr></descr>
@GLOBAL_PARAM_VALUE varchar(100), --<descr></descr>
@MakerId varchar(50) = null,
@AddedDatetime datetime = null,
@IsAuth bit = null,
@oRetVal int = Null Output --<descr></descr>
/*</Params>*/
As

Set NoCount On
/*<Comments></Comments>*/
/*<LocalVariables>*/
/*</LocalVariables>*/
 
	Insert Into dbo.COM_GLOBAL_PARAM_MAPPING
	(
		SOE_ID
		,GLOBAL_PARAM_ID
		,GLOBAL_PARAM_VALUE
		,GPM_MAKER_ID
		,GPM_ADDED_DATETIME
		,GPM_IS_AUTH
	)
	Values
	(
		@SOE_ID
		,@GLOBAL_PARAM_ID
		,@GLOBAL_PARAM_VALUE
		,@MakerId
		,@AddedDatetime
		,@IsAuth
	)
	
	If @@RowCount > 0
		Set @oRetVal = SCOPE_IDENTITY() --Record has been inserted successfully, return generated ID
	Else
		Set @oRetVal = -40 --No record inserted
	
	Return
GO
/****** Object:  StoredProcedure [dbo].[COM_SP_COM_GLOBAL_PARAM_MAPPING_GETALL]    Script Date: 01/02/2024 4:07:28 pm ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* -----------------------------------------------------------------------------------------------------------
 * SP Name		: COM_SP_COM_GLOBAL_PARAM_MAPPING_GETALL
 * Created By	: Azam Farooq
 * Created On	: 10/01/2011
 * Purpose		: Retrieve entity(COM_GLOBAL_PARAM_MAPPING) records as per filter provided.
 * Module		: COMMON
 * Called By	: iCORE.DDS.BUSINESSOBJECTS.ENTITIES.COM_GLOBAL_PARAM_MAPPING.GetCOM_GLOBAL_PARAM_MAPPINGRecords
 * Comments		: None
 * -----------------------------------------------------------------------------------------------------------
///-------------------------------------Modification History--------------------------------------------------
	[Date]		[Author]			[Purpose]

///----------------------------------------------------------------------------------------------------------- */
 
CREATE PROCEDURE [dbo].[COM_SP_COM_GLOBAL_PARAM_MAPPING_GETALL]
/*<Param>*/
@GLOBAL_PARAM_ID int,
@oRetVal int Output --<descr></descr>
/*</Param>*/
As

Set NoCount On
/*<Comment></Comment>*/
	Select ID
	   	,SOE_ID
	   	,GLOBAL_PARAM_ID
	   	,GLOBAL_PARAM_VALUE
		,'' As ActionType 
	From dbo.COM_GLOBAL_PARAM_MAPPING
	Where GLOBAL_PARAM_ID = @GLOBAL_PARAM_ID AND  GPM_IS_AUTH = 1



	If @@RowCount = 0 
		Set @oRetVal = -40	--Record not found
	Else
		Set @oRetVal = -10 	--Success
	
	Return
GO
/****** Object:  StoredProcedure [dbo].[COM_SP_COM_GLOBAL_PARAM_MAPPING_MANAGER]    Script Date: 01/02/2024 4:07:28 pm ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* -----------------------------------------------------------------------------------------------------------
 * SP Name		: COM_SP_COM_GLOBAL_PARAM_MAPPINGMANAGER
 * Created By	: Azam Farooq
 * Created On	: 10/01/2011
 * Purpose		: Manages (COM_GLOBAL_PARAM_MAPPING) store procedure depending upon the action type.
 * Module		: COMMON
 * Called By	: iCORE.DDS.BUSINESSOBJECTS.ENTITIES.COM_GLOBAL_PARAM_MAPPINGDAO
 * Comments		: None
 * -----------------------------------------------------------------------------------------------------------
///-------------------------------------Modification History--------------------------------------------------
	[Date]		[Author]			[Purpose]

///----------------------------------------------------------------------------------------------------------- */
 
CREATE PROCEDURE [dbo].[COM_SP_COM_GLOBAL_PARAM_MAPPING_MANAGER]
/*<Param>*/
@ID INT = Null, --<descr></descr>
@SOE_ID varchar(50) = Null, --<descr></descr>
@GLOBAL_PARAM_ID int = Null, --<descr></descr>
@GLOBAL_PARAM_VALUE varchar(100) = Null, --<descr></descr>
@GLOBAL_PARAM_NAME varchar(100) = Null, 
@APP_CD varchar(10) = Null,
@ActionType varchar(50) = Null,
@MakerId varchar(50) = null,
@CheckerkID varchar(50) = null,
@AddedDatetime datetime = null,
@UpdatedDatetime datetime = null,
@IsAuth bit = null,
@oRetVal int = Null Output
/*</Param>*/
As

Set NoCount On
/*<Comment></Comment>*/


	If @ActionType = 'Save'
	Begin
		Exec COM_SP_COM_GLOBAL_PARAM_MAPPING_ADD
	   			@ID
	   			,@SOE_ID
	   			,@GLOBAL_PARAM_ID
	   			,@GLOBAL_PARAM_VALUE
				,@MakerId
				,@AddedDatetime
				,@IsAuth
,@oRetVal = @oRetVal OUTPUT
	End
	Else If @ActionType = 'Update'
	Begin
		Exec COM_SP_COM_GLOBAL_PARAM_MAPPING_UPDATE
	   			@ID
	   			,@SOE_ID
	   			,@GLOBAL_PARAM_ID
	   			,@GLOBAL_PARAM_VALUE
				,@MakerId
				,@AddedDatetime
				,@IsAuth
	End
	Else If @ActionType = 'Delete'
	Begin
		Exec COM_SP_COM_GLOBAL_PARAM_MAPPING_DELETE
				@ID,@oRetVal
	End
	Else If @ActionType = 'Get'
	Begin
		Exec COM_SP_COM_GLOBAL_PARAM_MAPPING_GET
				@ID,@oRetVal
	End
	Else If @ActionType = 'List'
	Begin
		Exec COM_SP_COM_GLOBAL_PARAM_MAPPING_GETALL @GLOBAL_PARAM_ID, @oRetVal
	End
	Else If @ActionType = 'GetValue'
	Begin
		SELECT  dbo.udf_GetGlobalParamValue(@GLOBAL_PARAM_NAME, @SOE_ID) AS GLOBAL_PARAM_VALUE
	End
	Else If @ActionType = 'GetGlobalParametersList'
	Begin
		SELECT	ID AS GLOBAL_PARAM_ID,
				GLOBAL_PARAM_NAME
		FROM	COM_GLOBAL_PARAMETERS
		WHERE	APP_CD = @APP_CD
	End

	ELSE IF @ACTIONTYPE = 'AUTHORIZE_GPM'      
	 BEGIN      
	  UPDATE COM_GLOBAL_PARAM_MAPPING SET GPM_IS_AUTH = 1, GPM_CHECKER_ID = @CheckerkID,GPM_UPDATED_DATETIME = @UpdatedDatetime WHERE ID = @ID     
	 END 

	 ELSE IF @ACTIONTYPE = 'REJECT_GPM'      
	 BEGIN      
	  DELETE FROM COM_GLOBAL_PARAM_MAPPING WHERE ID = @ID     
	 END 

	ELSE IF @ACTIONTYPE = 'GET_UNAUTHORIZE_GPM'      
	 BEGIN
		Select ID
	   	,SOE_ID
	   	,GLOBAL_PARAM_ID
	   	,GLOBAL_PARAM_VALUE
		,'' As ActionType 
	From dbo.COM_GLOBAL_PARAM_MAPPING WHERE GPM_IS_AUTH = 0 AND GPM_MAKER_ID != @MakerId AND GLOBAL_PARAM_ID=0
	
	Return
	 END 


	Return
GO
/****** Object:  StoredProcedure [dbo].[COM_SP_COM_GLOBAL_PARAM_MAPPING_UPDATE]    Script Date: 01/02/2024 4:07:28 pm ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* -----------------------------------------------------------------------------------------------------------
 * SP Name		: COM_SP_COM_GLOBAL_PARAM_MAPPINGUPDATE
 * Created By	: Azam Farooq
 * Created On	: 10/01/2011
 * Purpose		: Updates record(s) in the Table(COM_GLOBAL_PARAM_MAPPING) according to the parameter(s).
 * Module		: COMMON
 * Called By	: iCORE.DDS.BUSINESSOBJECTS.ENTITIES.COM_GLOBAL_PARAM_MAPPING.InsertCOM_GLOBAL_PARAM_MAPPING
 * Comments		: None
 * -----------------------------------------------------------------------------------------------------------
///-------------------------------------Modification History--------------------------------------------------
	[Date]		[Author]			[Purpose]

///----------------------------------------------------------------------------------------------------------- */
 
CREATE PROCEDURE [dbo].[COM_SP_COM_GLOBAL_PARAM_MAPPING_UPDATE]
/*<Param>*/
@ID INT, --<descr></descr>
@SOE_ID varchar(50), --<descr></descr>
@GLOBAL_PARAM_ID int, --<descr></descr>
@GLOBAL_PARAM_VALUE varchar(100), --<descr></descr>
@MakerId varchar(50) = null,
@AddedDatetime datetime = null,
@IsAuth bit = null,
@oRetVal int = Null Output
/*</Param>*/
As

Set NoCount On
/*<Comment></Comment>*/
	Update	dbo.COM_GLOBAL_PARAM_MAPPING
	Set
	   	SOE_ID = @SOE_ID
	   	,GLOBAL_PARAM_ID = @GLOBAL_PARAM_ID
	   	,GLOBAL_PARAM_VALUE = @GLOBAL_PARAM_VALUE
		,GPM_MAKER_ID = @MakerId
		,GPM_ADDED_DATETIME = @AddedDatetime
		,GPM_IS_AUTH = @IsAuth
	Where 	 ID = @ID	
	
	Set @oRetVal = -10 --Record updated successfully
	Return
GO
